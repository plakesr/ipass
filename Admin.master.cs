﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                lbl_admin.Text = Session["Login"].ToString();
            }
        }
        catch
        {
            Response.Redirect("Default.aspx");
        }
    }
    protected void lnkbtn_Logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
        Response.Cache.SetNoStore();
        Response.Redirect("Default.aspx");
     //   string loggedOutPageUrl = "Default.aspx";
     //Response.Write("<script language='javascript'>");
     //Response.Write("function ClearHistory()");
     //Response.Write("{");
     //Response.Write(" var backlen=history.length;");
     //Response.Write(" history.go(-backlen);");
     //Response.Write(" window.location.href='" + loggedOutPageUrl + "'; ");
     //Response.Write("}");
     //Response.Write("</script>");

    }

    
}

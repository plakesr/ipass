﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Data.SqlClient;

public partial class Admin_DatewiseCorporateIndividual : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            BindChart();
        }
    }
    StringBuilder str = new StringBuilder();
    SqlConnection conn = new SqlConnection("Data source=localhost; Initial catalog=db_IPASS; Integrated security=true");

    DataTable dt_countdate = new DataTable();
    DataTable dt_countindividual = new DataTable();
    DataTable dt_countcorporate = new DataTable();
    private DataTable GetData()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Date", typeof(string));
        dt.Columns.Add("individual", typeof(string));
        dt.Columns.Add("corporate", typeof(string));

        dt_countdate = new clsInsert().countdate();
        for (int i = 0; i < dt_countdate.Rows.Count; i++)
        {
            DateTime d1 = DateTime.Parse(dt_countdate.Rows[i]["dateofregistration"].ToString());
            DateTime d2 = DateTime.Parse(dt_countdate.Rows[i]["dateofregistration"].ToString()).AddDays(1);
            DateTime dateOnly = d1.Date;
            DateTime dateOnly1 = d2.Date;
           

            DataRow dr = dt.NewRow();
            dt_countindividual = new clsInsert().countdates_individual(dateOnly, dateOnly1);
            dt_countcorporate = new clsInsert().countdates_Corporate(dateOnly, dateOnly1);

           


                dr[0] = dateOnly.ToString("dd/MM/yyyy");
                dr[1] = dt_countindividual.Rows[0]["totalparkerindividual"].ToString();
                dr[2] = dt_countcorporate.Rows[0]["totalparkercorporate"].ToString();
                dt.Rows.Add(dr);
            
        }

        //DataTable dt = new DataTable();
        //string cmd = "select * from Sales";

        return dt;
    }

    private void BindChart()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = GetData();

            str.Append(@"<script type=text/javascript> google.load( *visualization*, *1*, {packages:[*corechart*]});
                       google.setOnLoadCallback(drawChart);
                       function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Dateofregistration');
        data.addColumn('number', 'Individual');
        data.addColumn('number', 'Corporate');       

        data.addRows(" + dt.Rows.Count + ");");

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                str.Append("data.setValue( " + i + "," + 0 + "," + "'" + dt.Rows[i][0].ToString() + "');");
                str.Append("data.setValue(" + i + "," + 1 + "," + dt.Rows[i][1].ToString() + ") ;");
                str.Append("data.setValue(" + i + "," + 2 + "," + dt.Rows[i][2].ToString() + ") ;");
            }

            str.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));");
            str.Append(" chart.draw(data, {width: 850, height: 500, title: 'Number Of Registration',");
            str.Append("hAxis: {title: 'Date Of Registration', titleTextStyle: {color: 'green'}}");
            str.Append("}); }");
            str.Append("</script>");
            lt.Text = str.ToString().TrimEnd(',').Replace('*', '"');
        }
        catch
        {
        }
    }
}
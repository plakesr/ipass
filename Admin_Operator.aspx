﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator.master" AutoEventWireup="true" CodeFile="Admin_Operator.aspx.cs" Inherits="Admin_Operator" %>

<%--<%@ Register src="WUC/wuc_Operator_registration.ascx" tagname="wuc_Operator_registration" tagprefix="uc1" %>--%>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<div class="body-right"><h1>Add New Parkers</h1>
 
      <%-- <uc1:wuc_Operator_registration ID="wuc_Operator_registration1" 
        runat="server" />--%>


<%--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>--%>
<%--<script type ="text/javascript">
    var map;
    function initialize() {
        var lat = '<%= Session["Lot_Latitude"]%>';
        var long = '<%= Session["Lot_Longitude"] %>';

        var latlng = new google.maps.LatLng(lat, long);
        var myOptions = {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        var marker = new google.maps.Marker
        (
            {
                position: new google.maps.LatLng(lat, long),
                map: map,
                title: 'Click me'
            }
        );
        var infowindow = new google.maps.InfoWindow({
            content: 'Location info:<br/>Country Name:<br/>LatLng:'
        });
        google.maps.event.addListener(marker, 'click', function () {
            // Calling the open method of the infoWindow 
            infowindow.open(map, marker);
        });
    }
    window.onload = initialize;
</script>--%>
 
                    
<link href="../stylesheet/main.css" rel="stylesheet" type="text/css" />

                
  <script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />
<div class="container">

   
 <table class="registration-area" width="100%" >
 <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </cc1:ToolkitScriptManager>
      <tr>
        <td>
        <div class="tab-border">
        <asp:Button Text="Lot Search" BorderStyle="None" ID="btn_lotsearch" 
                CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Personal Details" BorderStyle="None" ID="btn_personaldetails" 
                CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Add Vehicle Details" BorderStyle="None" 
                ID="btn_addvehicledetails" CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
          <asp:Button Text="Total Charges" BorderStyle="None" ID="btn_totalcharges" 
                CssClass="Initial" runat="server"
              OnClick="Tab4_Click" />

            <asp:Label ID="Label1" runat="server" Text="You Purchase The Planrate Of $" 
                Visible="False" ForeColor="#3333FF"   Font-Bold="True"></asp:Label>
             
            <asp:Label ID="lbl_amount" runat="server" Visible="False" ForeColor="#3333FF"   Font-Bold="True"></asp:Label>    </div>
          <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
       
              <asp:View ID="View1" runat="server">
                  <asp:Panel ID="Panel12" runat="server" CssClass="panalclass" Width="100%">
                  
              <table class="reg-form" width="100%" >
<tr>
<td valign="top" class="style8">
<table class="form-table" width="100%">
<tr>
<td class="style17">
    <asp:Panel ID="Panel1" runat="server">
    <fieldset><legend>Lot Wise</legend>
<table width="100%">
<tr><td class="style16" width="30%"> 

    <asp:Label ID="lbl_lotname" runat="server" Text="Lot Name" Visible="True"></asp:Label>
    </td>
<td class="style7">
    
    <asp:TextBox ID="txtbox_lotname" runat="server"></asp:TextBox>
    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
        CompletionInterval="1" CompletionSetCount="1" EnableCaching="true" 
        MinimumPrefixLength="1" ServiceMethod="GetCountries" 
        TargetControlID="txtbox_lotname" UseContextKey="True">
    </cc1:AutoCompleteExtender>
    
  <%--  <cc1:AutoCompleteExtender ID="TextBox1_AutoCompleteExtender" 
        runat="server" TargetControlID="txtbox_lotname"
       MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" 
        CompletionInterval="1" ServiceMethod="GetCountries" UseContextKey="True">
    </cc1:AutoCompleteExtender>--%>
    </td>
</tr>

<tr><td class="style16">&nbsp;</td>
<td>
    <asp:Button ID="btn_ok" runat="server" onclick="btn_ok_Click" 
        Text="OK" Width="78px" CssClass="button" />
    </td>
</tr>
<tr><td colspan="2" valign=" middle">
<span style=" background:#deeeff; padding:5px 10px 10px;float:left;width:96%;">
    <asp:Label ID="lbl_availablesapce" runat="server" Text="Available Space" 
        Visible="False"></asp:Label>
        <span style=" font-size:25px; font-weight:bold;color:#173866 !important;">
 <asp:Label ID="Lbl_TotalSpace" runat="server" Text="" Visible="false"></asp:Label>   <asp:Label ID="lbl_availablevalue" runat="server" Visible="False"></asp:Label></span>
    </span></td>
</tr>
</table>
</fieldset>
    </asp:Panel>
</td>
<td  style="text-align:center; border:2" class="style18">|<br />|<br />|<br />|<br />|<br />|<br />|<br />|<br />|<br />|<br />|<br />|</td>

<td>
    <asp:Panel ID="Panel10" runat="server">
    <fieldset><legend>Coupon Wise</legend>
<table width="100%">
<tr><td class="style7" width="30%"> 

    <asp:Label ID="Label2" runat="server" Text="Coupon Number" Visible="True"></asp:Label>
    </td>
<td class="style7">
    
    <asp:TextBox ID="coupon_number" runat="server"></asp:TextBox>
   
    
  
    </td>
</tr>

<tr><td>&nbsp;</td>
<td>
    <asp:Button ID="c_ok" runat="server" Text="OK" Width="78px" CssClass="button" 
        onclick="c_ok_Click" />
    </td>
</tr>
</table>
</fieldset>
    </asp:Panel>
</td>
</tr>
</table>   
    <asp:Panel ID="Panel11" runat="server" Visible="false">
    <fieldset>
<table class="form-table">
    





<tr><td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
   
<tr>
<td valign="top" colspan="2"> 
<asp:Panel ID="randompanel" runat="server" Visible="False" >
        <fieldset >
        <legend> Random</legend>
        
           <div class="parking-price">
            <table >
                <tr>
                    <td >
                       </td>
                    <td >
                        <asp:Repeater ID="rpt_RandomDaily" runat="server" 
                            onitemcommand="rpt_RandomDaily_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th width="30%">
                         Daily
                    </th>
                      <th width="30%">
                      
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td >$  <%#Eval("Amount")%> </td>
                <td >   <asp:Label ID="Label1" runat="server"   Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>  <%#Eval("chargebyname")%> </td> 

                  
               
                <td >
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/Buy01.png" width="86" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                    
                    
                </td>
               <asp:Label ID="random_daily_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="random_daily_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="chargeby_random_daily" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                        </asp:Repeater>

                    </td>
                </tr>
                <tr>
                    <td >
                        </td>
                    <td >
                        <asp:Repeater ID="rpt_RandomMonthly" runat="server" 
                            onitemcommand="rpt_RandomMonthly_ItemCommand">
                        <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th width="30%">
                       Monthly 
                    </th>
                      <th width="30%">
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td>  $   <%#Eval("Amount")%> </td>
                <td>   <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>'  runat="server">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/Buy01.png" width="86" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="random_month_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="random_month_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>

                                          <asp:Label ID="chargeby_random_monthly" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                        </asp:Repeater>
                     </td>
                </tr>
               
            </table></div>
            </fieldset>
         </asp:Panel>
            </td>
<td colspan="2">

<asp:Panel ID="reservedpanel" runat="server" Visible="False">
         <fieldset >
         <legend style="width: 57px">Reserved</legend>
         <div class="parking-price">
             <table>
                 <tr>
                     <td>
                        </td>
                     <td >
                         <asp:Repeater ID="rpt_ReservedDaily" runat="server" 
                             onitemcommand="rpt_ReservedDaily_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th width="30%">
                         Daily
                    </th>
                      <th width="30%">
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td>    $ <%#Eval("Amount")%> </td>
                <td>   
                    <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/Buy01.png" width="86" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="reserved_daily_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="reserved_daily_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>
                                          <asp:Label ID="chargeby_reserved_daily" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
                 <tr>
                     <td >
                        </td>
                     <td >
                         <asp:Repeater ID="rpt_ReservedMonthly" runat="server" 
                             onitemcommand="rpt_ReservedMonthly_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th width="30%">
                        Monthly
                    </th>
                      <th width="30%">
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td>  $   <%#Eval("Amount")%> </td>
                <td>   <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="images/Buy01.png" width="86" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="reserved_month_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                         <asp:Label ID="reserved_month_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>
                                          <asp:Label ID="chargeby_reserved_monthly" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
             </table>
             </div>
         </fieldset>
         </asp:Panel>




</td>
</tr>

</table>
</fieldset>
    </asp:Panel>

</td>

</tr>
  
</table>

                  </asp:Panel>
              </asp:View>

            <asp:View ID="View2" runat="server">
              <table class="reg-form">
                <tr>
                  <td>
             
                      
  

<asp:Panel ID="Panel2" runat="server">
<fieldset>
<legend>Personal Information</legend>
    <table class="form-table" width="100%">
        <tr>
            <td class="style34">
          
                <%--<asp:RequiredFieldValidator ID="selectaccount" runat="server" 
                    ControlToValidate="ddl_country" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>--%>Select Account</td>
            <td style="width:250px; height:50;">
                <asp:RadioButton ID="rb_account" runat="server" Text="Individual" GroupName="a" 
                     ValidationGroup="b"  AutoPostBack="true" 
                    oncheckedchanged="rb_account_CheckedChanged"/> &nbsp; &nbsp; &nbsp;
                <asp:RadioButton ID="rb_account1" runat="server" Text="Corporate" GroupName="a"  
                     ValidationGroup="b" oncheckedchanged="rb_account1_CheckedChanged" AutoPostBack="true" Enabled="false"/>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td valign="top" width="150">
               First Name:</td>
            <td class="style41">
                <asp:TextBox ID="txtBox_firstname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" placeholder="Enter the First Name"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfv_firstname" runat="server" 
                    ControlToValidate="txtBox_firstname" ErrorMessage="Enter First Name" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td width="50"></td>
            <td valign="top" width="150">
               
                
                Last Name:</td>
            <td class="style39">
                <asp:TextBox ID="txtbox_lastname" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" placeholder="Enter the Last Name" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_lastname" runat="server" 
                    ControlToValidate="txtbox_lastname" ErrorMessage="Enter Last Name" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
      
        </tr>
        <tr>
            <td class="style34">
                <span ID="Grp_AccountInformation_lblContactName">Card Number(If Issued)</span></td>
            <td class="style41">
                <asp:TextBox ID="txtbox_coupannumber" runat="server" CssClass="twitterStyleTextbox" placeholder="Enter the Card Number" ></asp:TextBox>
            </td>
           <td width="50"></td>
            <td class="style39">
                Country</td>
            <td class="style30">

             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <asp:DropDownList ID="ddl_Parkercountry" runat="server" 
                    DataSourceID="SqlDataSource5" DataTextField="Country_name"  CssClass="twitterStyleTextbox"
                    DataValueField="Country_id" AutoPostBack="True" Height="37px">
                </asp:DropDownList>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="ddl_Parkercountry" ErrorMessage="Enter Country Name" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country]">
                </asp:SqlDataSource>
                </ContentTemplate>
                </asp:UpdatePanel>
                </td>
           
        </tr>
        <tr>
            <td class="style34">
                
                 ST#</td>
            <td class="style41">
                <asp:TextBox ID="txtbox_address1" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"
                    ValidationGroup="a" placeholder="Enter the Street Address" style="resize: none;"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_address2" runat="server" 
                    ControlToValidate="txtbox_address1" ErrorMessage="Enter the address" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
             <td width="50"></td>
            <td class="style39">
                Province</td>
            <td class="style30">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="True" 
                            CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource1" 
                            DataTextField="ProvinceName" DataValueField="ProvinceId" ValidationGroup="a" Height="37px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            
                            SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddl_Parkercountry" Name="Country_id" 
                                    PropertyName="SelectedValue" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:RequiredFieldValidator ID="rfv_state0" runat="server" 
                            ControlToValidate="ddlstate" ErrorMessage="Enter Province Name" ForeColor="#CC0000" 
                            ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                Street Name</td>
            <td class="style41">
                <asp:TextBox ID="txtbox_address2" runat="server"  TextMode="MultiLine" CssClass="twitterStyleTextbox" style="resize: none;" placeholder="Enter The Area"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtbox_address2" ErrorMessage="Enter the address" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                City</td>
            <td class="style30">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddl_Parkercity" runat="server" 
                            CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource6" 
                            DataTextField="City_name" DataValueField="City_id" Height="37px">
                        </asp:DropDownList>
                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="ddl_Parkercity" ErrorMessage="Enter City Name" ForeColor="#CC0000" 
                            ValidationGroup="a"></asp:RequiredFieldValidator>
                        <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            
                            SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id) order by City_name">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlstate" Name="Province_id" 
                                    PropertyName="SelectedValue" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
     
        </tr>
        <tr>
            <td class="style34">
                
                
                &nbsp;Postal Code /Zip Code</td>
            <td class="style41">
                <asp:TextBox ID="txtbox_postal" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" placeholder="Enter The Zip"></asp:TextBox><br />
              <%--  <asp:RequiredFieldValidator ID="rfv_postal" runat="server" 
                    ControlToValidate="txtbox_postal" 
                    ErrorMessage="Enter the Postal Code/Zip Code " ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator><br />--%>
                    <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_postal" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator>
            </td>
            <td width="50"></td>
            <td class="style39">
                
                Phone No.</td>
            <td class="style30">
                <asp:TextBox ID="txtbox_phone" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox><br />
                <%--<asp:RequiredFieldValidator ID="rfv_phoneno" runat="server" 
                    ControlToValidate="txtbox_phone" ErrorMessage="Enter Phone Number" 
                    ForeColor="#CC0000" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator><br />--%>
                    <asp:RegularExpressionValidator    ID="RegularExpressionValidator1" runat="server"  ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_phone" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_phone" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
            </td>
   
        </tr>

           
        <tr>
            <td class="style34">
                Cellular No.</td>
            <td class="style41">
                <asp:TextBox ID="txtbox_cellular" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" >


</asp:TextBox><br />
 
                <asp:RequiredFieldValidator ID="rfv_cellular" runat="server" 
                    ControlToValidate="txtbox_cellular" ErrorMessage="Enter Cellular Number" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:RegularExpressionValidator         ID="RegularExpressionValidator2" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_cellular" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                 runat="server" Enabled="True" TargetControlID="txtbox_cellular" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>

   
            </td>
              <td width="50"></td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                
            </td>
           
        </tr>

        <tr>
           <td class="style30">
                Fax No.</td>
            <td class="style41">
                <asp:TextBox ID="txt_fax" runat="server" CssClass="twitterStyleTextbox" Placeholder="Enter The Fax Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_fax" runat="server" 
                    ControlToValidate="txt_fax" ErrorMessage="Enter Fax Number" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
              <td width="50"></td>
            <td class="style15">
                <asp:Label ID="Delivery" runat="server" Text="Select card Delivery Method" Visible="false"></asp:Label>
            </td>
            <td class="style39">
                <asp:DropDownList ID="ddl_deliverymethod_corporate" runat="server" 
                    CssClass="twitterStyleTextbox" Height="38px" Width="300px" Visible="false" 
                    >
                </asp:DropDownList>
            </td>
           
        </tr>


        <tr>
            <td class="style34" valign="top">
             Email Id</td>
            <td class="style41">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                <asp:TextBox ID="txtbox_Email" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" ></asp:TextBox><br />
                      </ContentTemplate>
                </asp:UpdatePanel>
                <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                    ControlToValidate="txtbox_Email" ErrorMessage="Enter The Email ID" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="rgexp_email" runat="server" 
                    ErrorMessage="*Please Enter Email In The Correct Format" ControlToValidate="txtbox_Email" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                   
                  
            </td>
              <td width="50"></td>
            <td class="style38" valign="top">
                <asp:Label ID="lbl_corporate_activationdate" runat="server" 
                    Text="Activation Date :" Visible="False"></asp:Label>
            </td>
            <td class="style39">
                
                <asp:TextBox ID="txtbox_corporateactivationdate" runat="server" 
                    CssClass="disable_past_dates" Visible="False"></asp:TextBox>
                 <cc1:CalendarExtender ID="CalendarExtender1" 
                            runat="server" Enabled="True" TargetControlID="txtbox_corporateactivationdate" ></cc1:CalendarExtender>
                
            </td>
                  </tr>
    </table>

    </fieldset>
</asp:Panel>
<br />
<div class="clearfix">&nbsp;</div>

<asp:Panel ID="Panel3" runat="server" Height="201px">
<fieldset>
<legend>&nbsp;Login Information</legend>
    <table class="form-table">
        <tr>
            <td width="27%">
                
                
                User Name:</td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                <asp:TextBox ID="txtBox_username" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" Placeholder="Enter The User Name" ontextchanged="txtBox_username_TextChanged1" AutoPostBack="true" ></asp:TextBox>
                
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtBox_username" ErrorMessage="Enter User Name"  ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:Label ID="lbl_usercheck" runat="server" Visible="False" 
                    ForeColor="#CC0000"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td width="27%">
              
                Password:</td>
            <td>
                <asp:TextBox ID="txtBox_Passwrd" runat="server" ValidationGroup="a" TextMode="Password" CssClass="twitterStyleTextbox" Placeholder="Enter The Password"></asp:TextBox>
                  <cc1:PasswordStrength ID="txtBox_Passwrd_PasswordStrength0" runat="server" 
                    Enabled="True" MinimumUpperCaseCharacters="1" PreferredPasswordLength="10" 
                    StrengthIndicatorType="BarIndicator" TargetControlID="txtBox_Passwrd"  
                    TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"></cc1:PasswordStrength>
                  <asp:RequiredFieldValidator ID="rfv_password" runat="server" 
                    ControlToValidate="txtBox_Passwrd" ErrorMessage="Enter password" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="27%">
                Confirm Password:</td>
            <td>
                <asp:TextBox ID="txtBox_cnfrmpasswrd" runat="server" ValidationGroup="a" TextMode="Password" CssClass="twitterStyleTextbox" Placeholder="Retype the Password"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfv_confirmpassword" runat="server" 
                    ControlToValidate="txtBox_cnfrmpasswrd" 
                    ErrorMessage="Enter Confirm Password" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cmp_confirmpwd" runat="server" 
                    ControlToCompare="txtBox_Passwrd" ControlToValidate="txtBox_cnfrmpasswrd" 
                    ErrorMessage="Password Does Not Match" ForeColor="#CC0000" ValidationGroup="a"></asp:CompareValidator>
                    
                &nbsp;&nbsp;
            </td>
        </tr>
    </table>
    </fieldset>
</asp:Panel>



                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div>


                                    <asp:Panel ID="Panel4" runat="server">
                                        <fieldset>
                                            <legend>Billing Information</legend>
                                            <br />
                                          
                                          
                                                <table class="style10">
                                                    <tr>
                                                        <td class="style13">
                                                            <asp:RadioButton ID="rbtn_creditcard" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_creditcard_CheckedChanged" 
                                                                Text="Auto Charge to Credit Card " />
                                                        </td>
                                                        <td class="style12">
                                                            <asp:RadioButton ID="rbtn_invoicebymail" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_invoicebymail0_CheckedChanged" 
                                                                Text="Invoice By Email" />
                                                        </td>
                                                        <td class="style11">
                                                            <asp:RadioButton ID="rbtn_invoicebyemail" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_invoicebyemail0_CheckedChanged" 
                                                                Text="Invoice By Mail " />
                                                        </td>
                                                        <td>
                                                            <asp:RadioButton ID="rbtn_ach" runat="server" AutoPostBack="true" GroupName="s" 
                                                                oncheckedchanged="rbtn_ach_CheckedChanged" Text="Auto Charge to ACH" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style13">
                                                        <br />
                                                            <asp:Button ID="btn_entercreditcard" runat="server" CssClass="button"  
                                                                onclick="btn_entercreditcard_Click" Text="Enter Credit Card" Visible="False" />
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;</td>
                                                        <td class="style11">
                                                            &nbsp;</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                           
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                 
                                    <asp:Panel ID="Panel5" runat="server"
                                        Visible="False">
                                        <fieldset>
                                            <legend>Direct&nbsp; Debit Bank Information</legend>
                                            <br />
                                            <table class="style10">
                                                <tr>
                                                    <td width="15%">
                                                        Customer Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_customername" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        Bank Account Type</td>
                                                    <td>
                                                    
                                                        <asp:DropDownList ID="ddl_accounttype" runat="server" CssClass="twitterStyleTextbox">
                                                         <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Saving</asp:ListItem>
                        <asp:ListItem Value="2">Current</asp:ListItem>
                        
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Bank Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        Branch</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_branch" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Zip/Postal</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankzip" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        City</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_city_bank" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        State/Province</td>
                                                    <td class="style15">
                                                        <asp:DropDownList ID="ddl_bankstate" runat="server" DataSourceID="SqlDataSource4" 
                                                            DataTextField="ProvinceName" DataValueField="ProvinceId">
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                            SelectCommand="SELECT * FROM [tbl_Province]"></asp:SqlDataSource>
                                                    </td>
                                                    <td width="15%">
                                                        Bank Routing No</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_routingno" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Bank Account No</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankaccountno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <br />


                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

<asp:Button ID="btn_next2" runat="server" Text="Next"  onclick="btn_next2_Click" CssClass="button"  />

<asp:Button ID="btn_finish_corporate" runat="server" Text="Finish"  CssClass="button" Visible="false" 
                          onclick="btn_finish_corporate_Click" />


                        


                      
                      <asp:Button ID="btn_pre" runat="server" Text="Previous" CssClass=button 
                          onclick="btn_pre_Click" />


                        


                      
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View3" runat="server">
              <table class="reg-form" width="100%">
                <tr>
                  <td>
                    <h3>
                      <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 150px;
    }
    .style3
    {
        width: 180px;
    }
    .style4
    {
        width: 181px;
    }
</style>

<div style="font-size:13px; color:#707070; font-weight:normal;">
    <table width="90%">
        <tr>
            <td width="12%">
                First Name :</td>
            <td  align="left">
                <asp:TextBox ID="txtbox_thirdFirstname" runat="server" ReadOnly="True" CssClass="twitterStyleTextbox"  Enabled="false"></asp:TextBox>
            </td>
            <td width="5%"></td>
            <td width="12%" align="left">
                Last Name:</td>
            <td align="left">
                <asp:TextBox ID="txtbox_thirdLastname" runat="server" ReadOnly="True" CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
            </td>
            <td>  <asp:Button ID="btn_addVehicle" runat="server" Text="Add/Edit vehicle" CssClass="button" onclick="btn_addVehicle_Click"/></td>
        </tr>
    </table>
</div>
<asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table style="font-size:13px; color:#707070; font-weight:normal;"  width="100%">
                <tr>
                    <td width="12%">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                 

                </tr>
                <tr>
                    <td width="12%">
                        Activation Date :</td>
                    <td>
                        <asp:TextBox ID="txtbox_ActicationDate" runat="server" CssClass="disable_past_dates twitterStyleTextbox" Width="150" Height="30"></asp:TextBox>
                        <cc1:CalendarExtender ID="txtbox_ActicationDate_CalendarExtender0" 
                            runat="server" Enabled="True" TargetControlID="txtbox_ActicationDate"></cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                            ControlToValidate="txtbox_ActicationDate" ErrorMessage="Activation Date" 
                            ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>

      
      </ContentTemplate>
    </asp:UpdatePanel>
<asp:Panel ID="Panel6" runat="server">
<fieldset>
<legend>
    Select card Delivery Method
</legend>
    <br />
    <asp:DropDownList ID="ddl_deliverymethod" runat="server" Height="38px" CssClass="twitterStyleTextbox" 
        Width="300px">
    </asp:DropDownList>
    
</fieldset></asp:Panel>
    
<asp:Button ID="btn_prev3" runat="server" Text="Previous" onclick="btn_prev3_Click" CssClass="button" /> &nbsp; &nbsp; 
 <asp:Button ID="btn_next3" runat="server" Text="Next" onclick="btn_next3_Click" CssClass="button" />

                       
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View4" runat="server">
              <table class="reg-form">
                <tr>
                  <td>
                    <h3>
                      <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 129px;
    }
    .style3
    {
        width: 149px;
    }
    .style4
    {
        width: 77px;
    }
    .style5
    {
        width: 151px;
    }
    .style6
    {
        width: 68px;
    }
</style>
<asp:Panel ID="Panel7" runat="server">
<fieldset>
<legend>
    Total Charges</legend>


    <table width="100%" class="form-table">
        <tr>
            <td>
                <asp:Label ID="lbl_activationcharge" runat="server" CssClass="normaltext" 
                    Text="Total Activation Charges"></asp:Label>
            </td>
            <td>
                <asp:Label ID="activationcharge" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_totaldeliverycharge" runat="server" CssClass="normaltext" 
                    Text="Total Delivery Charges"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_deliverycharge" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="total" runat="server" CssClass="normaltext" Text="Sub-Total"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_total" runat="server"></asp:Label>
                <asp:Label ID="discount" runat="server" Text="Discount" Visible="false"></asp:Label>
                <asp:Label ID="lbl_discount" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_tax" runat="server" CssClass="normaltext" Text="Tax" 
                    Visible="True"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_taxmoney" runat="server"></asp:Label>
                <asp:Label ID="Label3" runat="server" Text="%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                             <asp:Label ID="lbl_tax_money" runat="server" Visible="false"></asp:Label></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lb_grandtotal" runat="server" CssClass="normaltext" 
                    Text="Total" Visible="false"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_granttotal" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>

<asp:Panel ID="Panel8" runat="server">
<fieldset>
<legend>
    Add Old Card Detail</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="normaltext">
                Credit Reference #</td>
            <td class="style3">
                <asp:TextBox ID="txtbox_creditreference" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="rfv_creditreference" runat="server" 
                    ControlToValidate="txtbox_creditreference" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>--%>
            </td>
            <td class="normaltext">
                Old Card #</td>
            <td class="style5">
                <asp:TextBox ID="txtbox_oldcard" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td class="normaltext">
                Balance</td>
            <td class="style5">
                <asp:TextBox ID="txtbox_balance" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_Add" runat="server" Text="Add" CssClass="button" 
                    onclick="btn_Add_Click" />
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>

<asp:Panel ID="Panel9" runat="server">
<fieldset>
<legend>
    Old Card And Balances</legend>
    <asp:Repeater ID="rpt_oldcard" runat="server">
    </asp:Repeater>
    <br />
</fieldset>
    
</asp:Panel>

<br />
<asp:CheckBox ID="chbk_accept" runat="server" CssClass="normaltext" 
    
    Text="I confirm I have read and accept the terms &amp; conditions of service. I also accept the non-refundable fee detailed in the registration and the billing option selected." 
    ValidationGroup="a" />
                        <asp:Label ID="lbl_error" runat="server" ForeColor="Red" 
                            Text="* Please Accept Terms &amp; condition" Visible="False"></asp:Label>
                            <div class="clearfix">&nbsp;</div>
                            <asp:Button ID="btn_pre4" runat="server" Text="Previous" onclick="btn_pre4_Click" CssClass="button"   />

<asp:Button ID="btn_add1" runat="server" Text="Submit" CssClass="button"  
    ValidationGroup="a" onclick="btn_add1_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                        </td>
                </tr>
              </table>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>
   
    </div>
<%#Eval("Amount")%>             
       </div>
</asp:Content>



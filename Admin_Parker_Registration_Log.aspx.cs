﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Text;

public partial class Show_Graph : System.Web.UI.Page
{
    StringBuilder str = new StringBuilder();
    //Get connection string from web.config
    //SqlConnection conn = new SqlConnection("Data source=.; Initial catalog=db_IPASS; Integrated security=true");

    //QueryStringAccess conn1 = new QueryStringAccess();
    //SqlConnection conn = new SqlConnection(conn);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            BindChart();
        }
    }




    private DataTable GetData()
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        DataTable dt = new DataTable();
        string cmd = "select modifiedby, COUNT(*) as totalparker from tbl_UserLogin where operator_id='operator'  group by modifiedby";
        SqlDataAdapter adp = new SqlDataAdapter(cmd, con);
        adp.Fill(dt);
        return dt;
    }

    private void BindChart()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = GetData();

            str.Append(@"<script type=text/javascript> google.load( *visualization*, *1*, {packages:[*corechart*]});
                       google.setOnLoadCallback(drawChart);
                       function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Operator Name');
        data.addColumn('number', 'Registraion Done');      

        data.addRows(" + dt.Rows.Count + ");");

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                str.Append("data.setValue( " + i + "," + 0 + "," + "'" + dt.Rows[i]["modifiedby"].ToString() + "');");
                str.Append("data.setValue(" + i + "," + 1 + "," + dt.Rows[i]["totalparker"].ToString() + ") ;");
            }

            str.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));");
            str.Append(" chart.draw(data, {width: 1000, height: 600, title: 'No. of Parkers',");
            str.Append("hAxis: {title: 'Operator Name', titleTextStyle: {color: 'green'}}");
            str.Append("}); }");
            str.Append("</script>");
            lt.Text = str.ToString().TrimEnd(',').Replace('*', '"');
        }
        catch
        {
        }
    }
     
}


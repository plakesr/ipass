﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="Admin_Registration_Of_Corporate.aspx.cs" Inherits="Admin_Registration_Of_Corporate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link href="stylesheet/multiple-form.css" rel="stylesheet" type="text/css" />
   <%-- <asp:Label ID="Label2" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%>
  <script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />
    <link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
   
   
    <script type="text/javascript">

        function IsOneDecimalPoint(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
            var parts = evt.srcElement.value.split('.');
            if (parts.length > 1 && charCode == 46)
                return false;
            return true;
        }
</script>
<div class="body-right"><h1>Add New Corporate Parkers</h1>
                     

<div class="registration-area">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>


    <asp:Label ID="Label14" runat="server" style="text-align:right; float:right" Text="* Represents Mandatory Fields" ForeColor="Red" ></asp:Label>
 
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="3" 
        AutoPostBack="True" OnActiveTabChanged="TabContainer1_ActiveTabChanged" >
    <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"  CssClass="Clicked"><HeaderTemplate>
Lot Search
</HeaderTemplate>
    
<ContentTemplate>
    
          <asp:Panel ID="Panel1" runat="server">
    <fieldset><legend>Lot Wise</legend>
                
              <table class="reg-form" >
<tr>
<td valign="top" class="style8">
<table class="form-table01">
<tr>
<td class="style17">
 
<table style="width: 710px">
<tr><td class="style16" style="width: 276px; text-align: right"> 

    <asp:Label ID="lbl_lotname" runat="server" Text="Lot Name"></asp:Label>&nbsp;<asp:Label ID="Label3" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
    </td>
<td class="style7">
    
    <asp:TextBox ID="txtbox_lotname" runat="server"></asp:TextBox>
    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
        CompletionInterval="1" CompletionSetCount="1" 
        MinimumPrefixLength="1" ServiceMethod="GetCountries" 
        TargetControlID="txtbox_lotname" UseContextKey="True" 
        DelimiterCharacters="" Enabled="True" ServicePath="">
    </cc1:AutoCompleteExtender>
    
    </td>
</tr>

<tr><td class="style16" style="width: 276px">&nbsp;</td>
<td>
    <asp:Button ID="btn_ok" runat="server" onclick="btn_ok_Click" 
        Text="OK" Width="78px" CssClass="button" />
    </td>
</tr>
</table>

</td>


</tr>
</table>   
   

</td>

</tr>
  
</table>
</fieldset>
                
                  </asp:Panel>
                 
        
</ContentTemplate>

</cc1:TabPanel>
   

  


    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2"  CssClass="Initial" Enabled="false"><HeaderTemplate>
Contact Details
</HeaderTemplate>
    
<ContentTemplate>


       <table class="reg-form" width="100%">
                <tr>
                  <td>
             
                      
  

<asp:Panel ID="Panel2" class="pn2" runat="server">
<fieldset>
<legend>Personal Information</legend>
    <table class="form-table01" width="100%">
        <tr>
            <td valign="top" width="150">
                Corporation Name</td>
            <td class="style41" style="width: 296px"><asp:Label ID="Label4" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtBox_Companyname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the Company Name"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfv_firstname" runat="server" 
                    ControlToValidate="txtBox_Companyname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td valign="top" width="40" >
               
                
                &nbsp;</td>
            <td class="style39">
            </td>
      
        </tr>
        <tr>
            <td valign="top" width="150">
                Activation Date
               </td>
            <td class="style41" style="width: 296px"><asp:Label ID="Label5" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                  <asp:TextBox ID="txtbox_ActicationDate" runat="server" CssClass="disable_past_dates"></asp:TextBox>
                        <cc1:CalendarExtender ID="txtbox_ActicationDate_CalendarExtender" 
                            runat="server" Enabled="True" TargetControlID="txtbox_ActicationDate" ></cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rfv_activation_date" runat="server" 
                            ControlToValidate="txtbox_ActicationDate" Display="Dynamic" 
                            ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator> 
            </td>
            <td valign="top"></td>
            <td valign="top" width="150">
               
                
              Contact's  Name</td>
            <td class="style39"><asp:Label ID="Label13" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtbox_Firstname" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the First Name" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_lname" runat="server" 
                    ControlToValidate="txtbox_Firstname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
      
        </tr>
        <tr>
            <td class="style34">
                Designation</td>
            <td class="style41" style="width: 296px">
               <asp:Label ID="Label6" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtbox_designation" runat="server"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtbox_designation" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td valign="top"></td>
            <td class="style39">
                Corporation Address</td>
            <td class="style30" valign="top">
           <asp:Label ID="Label12" runat="server" Text="*" ForeColor="#CC0000" style=" vertical-align:top;"></asp:Label>
                <asp:TextBox ID="txtbox_address1" runat="server" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the Street Address" style="resize: none;" 
                    TextMode="MultiLine" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_address2" runat="server" 
                    ControlToValidate="txtbox_address1" Display="Dynamic" 
                    ErrorMessage="*" ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
           
        </tr>
        <tr>
            <td class="style34">
                
                 Country</td>
            <td class="style41" style="width: 296px">
               
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                       &nbsp;&nbsp; <asp:DropDownList ID="ddl_Parkercountry" runat="server" AutoPostBack="True" 
                            CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource5" 
                            DataTextField="Country_name" DataValueField="Country_id">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country]">
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
               
            </td>
            <td valign="top"></td>
            <td class="style39">
                Province</td>
            <td class="style30">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                          &nbsp;&nbsp; <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="True" 
                            CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource1" 
                            DataTextField="ProvinceName" DataValueField="ProvinceId" ValidationGroup="a">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddl_Parkercountry" Name="Country_id" 
                                    PropertyName="SelectedValue" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                City</td>
            <td class="style41" style="width: 296px">
                
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                         &nbsp;&nbsp;  <asp:DropDownList ID="ddl_Parkercity" runat="server" 
                            CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource6" 
                            DataTextField="City_name" DataValueField="City_id">
                        </asp:DropDownList>
                        &nbsp;<asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id) order by City_name">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlstate" Name="Province_id" 
                                    PropertyName="SelectedValue" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </td>
            <td valign="top"></td>
            <td class="style39">
                Email Id</td>
            <td class="style30">
            <asp:Label ID="Label11" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtbox_Email" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" ontextchanged="txtbox_Email_TextChanged" ></asp:TextBox>
                 
                <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                    ControlToValidate="txtbox_Email" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rgexp_email" runat="server" 
                    ErrorMessage="*Please Enter Email In The Correct Format" 
                    ControlToValidate="txtbox_Email" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    CssClass="validation"></asp:RegularExpressionValidator>
                     
            </td>
     
        </tr>
        <tr>
            <td class="style34">
                
                
                Postal Code </td>
            <td class="style41" style="width: 296px"><asp:Label ID="Label7" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtbox_postal" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" placeholder="Enter The Zip"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_postal" runat="server" 
                    ControlToValidate="txtbox_postal" 
                    ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_postal" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300" CssClass="validation"></asp:RegularExpressionValidator>
            </td>
            <td valign="top"></td>
            <td class="style39">
                
                Phone No.</td>
            <td class="style30"><asp:Label ID="Label10" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtbox_phone" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_phoneno" runat="server" 
                    ControlToValidate="txtbox_phone" ErrorMessage="*" 
                    ForeColor="#CC0000" Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator    ID="RegularExpressionValidator1" 
                    runat="server"  ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_phone" ValidationGroup="b" ForeColor="#FF3300" 
                    CssClass="validation" ></asp:RegularExpressionValidator>
                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_phone" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
            </td>
   
        </tr>

           
        <tr>
            <td class="style34">
                Cellular No.</td>
            <td class="style41" style="width: 296px"><asp:Label ID="Label8" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txtbox_cellular" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" ></asp:TextBox>
 
                <asp:RequiredFieldValidator ID="rfv_cellular" runat="server" 
                    ControlToValidate="txtbox_cellular" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator         ID="RegularExpressionValidator2" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_cellular" ValidationGroup="b" ForeColor="#FF3300" 
                    CssClass="validation" ></asp:RegularExpressionValidator>
                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                 runat="server" Enabled="True" TargetControlID="txtbox_cellular" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>

   
            </td>
            <td valign="top"></td>
            <td class="style38">
                Fax No.</td>
            <td class="style39">
                <asp:Label ID="Label9" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label>
                <asp:TextBox ID="txt_fax" runat="server" CssClass="twitterStyleTextbox" 
                    Placeholder="Enter The Fax Number"></asp:TextBox>
                 <asp:RegularExpressionValidator         ID="RegularExpressionValidator4" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txt_fax" ValidationGroup="b" ForeColor="#FF3300" 
                    CssClass="validation" ></asp:RegularExpressionValidator>
                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" 
                 runat="server" Enabled="True" TargetControlID="txt_fax" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>

                
            </td>
           
        </tr>


   
    </table>

    </fieldset>
</asp:Panel>
<br />
<div class="clearfix">&nbsp;</div>
<asp:UpdatePanel runat="server">
<ContentTemplate>
<asp:Panel ID="Panel3" runat="server">
<fieldset>
<legend>&nbsp;Login Information</legend>
    <table class="form-table01">

    
        <tr>
            <td width="27%">
                
                
                User Name<asp:Label ID="Label15" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label></td>
            <td style="width: 431px">
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate> 
                <asp:TextBox ID="txtBox_username" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" Placeholder="Enter The User Name" ontextchanged="txtBox_username_TextChanged1" AutoPostBack="true" ></asp:TextBox>
                
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtBox_username" ErrorMessage="*"  ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:Label ID="lbl_usercheck" runat="server" Visible="False" 
                    ForeColor="#CC0000" CssClass="validation"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td width="27%">
              
                Password &nbsp;<asp:Label ID="Label16" runat="server" Text="  *" ForeColor="#CC0000"></asp:Label></td>
            <td style="width: 431px">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                <asp:TextBox ID="txtBox_Passwrd" runat="server" ValidationGroup="a" 
                    TextMode="SingleLine" CssClass="twitterStyleTextbox" 
                    Placeholder="Enter The Password" 
                    ontextchanged="txtBox_Passwrd_TextChanged" AutoPostBack="True"></asp:TextBox>
                 
                    </ContentTemplate>
                      <Triggers>
      
         <asp:AsyncPostBackTrigger ControlID="txtBox_Passwrd" EventName="TextChanged" />
         
                </Triggers>
                    
                    </asp:UpdatePanel>
                 <%-- <asp:RequiredFieldValidator ID="rfv_password" runat="server" 
                    ControlToValidate="txtBox_Passwrd" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
       <%-- <tr>
            <td width="27%">
                Confirm Password:</td>
            <td>
                <asp:TextBox ID="txtBox_cnfrmpasswrd" runat="server" ValidationGroup="a" TextMode="Password" CssClass="twitterStyleTextbox" Placeholder="Retype the Password"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfv_confirmpassword" runat="server" 
                    ControlToValidate="txtBox_cnfrmpasswrd" 
                    ErrorMessage="Enter Confirm Password" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cmp_confirmpwd" runat="server" 
                    ControlToCompare="txtBox_Passwrd" ControlToValidate="txtBox_cnfrmpasswrd" 
                    ErrorMessage="Password Does Not Match" ForeColor="#CC0000" ValidationGroup="a"></asp:CompareValidator>
                    
                &nbsp;&nbsp;
            </td>
        </tr>--%>
    </table>
    </fieldset>
</asp:Panel>

</ContentTemplate>
</asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div>


                                    <asp:Panel ID="Panel4" runat="server">
                                        <fieldset>
                                            <legend>Billing Information</legend>
                                            <br />
                                          
                                          
                                                <table width="100%">
                                                <tr>
    <td><asp:Label ID="Label1" runat="server" Text="Billing Type"></asp:Label></td>
    <td><asp:DropDownList ID="DropDownList1" runat="server">
                       <asp:ListItem Value="1">Full </asp:ListItem>
                       <asp:ListItem Value="2">Pay-per use</asp:ListItem>
                   </asp:DropDownList></td>
                   <td>
                   </td>
                   <td></td>
    </tr>
                                                    <tr>
                                                        <td class="style13">
                                                            <asp:RadioButton ID="rbtn_creditcard" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_creditcard_CheckedChanged" 
                                                                Text="Auto Charge to Credit Card " />
                                                        </td>
                                                        <td class="style12">
                                                            <asp:RadioButton ID="rbtn_invoicebymail" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_invoicebymail0_CheckedChanged" 
                                                                Text="Invoice By Email" />
                                                        </td>
                                                        <td class="style11">
                                                            <asp:RadioButton ID="rbtn_invoicebyemail" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_invoicebyemail0_CheckedChanged" 
                                                                Text="Invoice By Mail " />
                                                        </td>
                                                        <td>
                                                            <asp:RadioButton ID="rbtn_ach" runat="server" AutoPostBack="true" GroupName="s" 
                                                                oncheckedchanged="rbtn_ach_CheckedChanged" Text="Auto Charge to ACH" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style13">
                                                        <br />
                                                            <asp:Button ID="btn_entercreditcard" runat="server" CssClass="button"  
                                                                onclick="btn_entercreditcard_Click" Text="Enter Credit Card" Visible="False" />
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;</td>
                                                        <td class="style11">
                                                            &nbsp;</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                           
                                            
                                        </fieldset>
                                    </asp:Panel>
                                    <br />
                                    <br />
                                    <asp:Panel ID="Panel5" runat="server"
                                        Visible="False">
                                        <fieldset>
                                            <legend>Direct&nbsp; Debit Bank Information</legend>
                                            <br />
                                            <table class="form-table01">
                                                <tr>
                                                    <td width="15%">
                                                        Customer Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_customername" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        Bank Account Type</td>
                                                    <td>
                                                    
                                                        <asp:DropDownList ID="ddl_accounttype" runat="server" CssClass="twitterStyleTextbox">
                                                         <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Saving</asp:ListItem>
                        <asp:ListItem Value="2">Current</asp:ListItem>
                        
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Bank Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        Branch</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_branch" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Zip/Postal</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankzip" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        City</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_city_bank" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        State/Province</td>
                                                    <td class="style15">
                                                        <asp:DropDownList ID="ddl_bankstate" runat="server" DataSourceID="SqlDataSource4" 
                                                            DataTextField="ProvinceName" DataValueField="ProvinceId">
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                            SelectCommand="SELECT * FROM [tbl_Province]"></asp:SqlDataSource>
                                                    </td>
                                                    <td width="15%">
                                                        Bank Routing No</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_routingno" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Bank Account No</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankaccountno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <br />

<%--ValidationGroup="a" --%>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
<asp:Button ID="btn_next" runat="server" Text="Next"  CssClass="button" onclick="btn_next_Click"   />





                      

                      
                  </td>
                </tr>
              </table>



</ContentTemplate>

</cc1:TabPanel>



    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3"  CssClass="Initial" Enabled="false"><HeaderTemplate>
Parking Type
</HeaderTemplate>
<ContentTemplate>
   <table  style="width: 941px" >
   <tr>
       <th>
       </th>
       <th>
           Number Of Space</th>
       <th>
           Plan Name</th>
       <th>
           Monthly Rate</th>
       <th>
           Activation Fees</th>
       <th>
           Deposit</th>
       <th style="width: 134px">
           Delivery Fees</th>
       <th>
           &nbsp;</th>
   </tr>
       <tr>
           <td>
               Reserved</td>
           <td>
               <asp:TextBox ID="txtbox_space_reserved" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="txtbox_space_reserved_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_space_reserved" ValidChars="0123456789"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                   <ContentTemplate>
                       <asp:TextBox ID="txtbox_reserved_planname" runat="server" 
                     CssClass="twitterStyleTextbox" 
                     ontextchanged="txtbox_reserved_planname_TextChanged" AutoPostBack="true"></asp:TextBox>
                   
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="txtbox_reserved_planname" 
                       EventName="TextChanged" ></asp:AsyncPostBackTrigger>
</Triggers>
</asp:UpdatePanel>

           </td>
           <td>
               <asp:TextBox ID="txtbox_reserved_monthly" runat="server" CssClass="twitterStyleTextbox"  onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_reserved_monthly" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:TextBox ID="txtbox_reserved_activation" runat="server" CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_reserved_activation" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:TextBox ID="txtbox_reserved_deposite" runat="server" CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_reserved_deposite" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td style="width: 134px">
               <asp:TextBox ID="txtbox_reserved_delivery" runat="server" CssClass="twitterStyleTextbox"  onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_reserved_delivery" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:Button ID="add_reserved" runat="server" Text="Add" CssClass="button" 
                onclick="add_reserved_Click" />

           </td>
       </tr>
       <tr>
           <td>
               Random</td>
           <td>
               <asp:TextBox ID="txtbox_space_random" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" 
                runat="server" Enabled="True" TargetControlID="txtbox_space_random" ValidChars="0123456789"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                   <ContentTemplate>
                       <asp:TextBox ID="txtbox_randomplanname" runat="server" 
                     CssClass="twitterStyleTextbox"  AutoPostBack="true" 
                     ontextchanged="txtbox_randomplanname_TextChanged"></asp:TextBox>
                   
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="txtbox_randomplanname" 
                       EventName="TextChanged" ></asp:AsyncPostBackTrigger>
</Triggers>
</asp:UpdatePanel>

           </td>
           <td>
               <asp:TextBox ID="txtbox_random_monthly" runat="server" CssClass="twitterStyleTextbox"  onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_random_monthly" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:TextBox ID="txtbox_random_activation" runat="server" CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_random_activation" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:TextBox ID="txtbox_random_deposite" runat="server" CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_random_deposite" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td style="width: 134px">
               <asp:TextBox ID="txtbox_random_delivery" runat="server" CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
  ValidChars="01234567890." TargetControlID="txtbox_random_delivery" Enabled="True"></cc1:FilteredTextBoxExtender>

           </td>
           <td>
               <asp:Button ID="btn_add_random" runat="server" Text="Add" CssClass="button" 
                onclick="btn_add_random_Click" />

           </td>
       </tr>
   </table>

    <asp:Panel ID="Panel6" runat="server" Visible="False">
    <fieldset><legend>Reserved</legend>
        <asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand1" 
                                    Visible="False" >
            <FooterTemplate>
                </table>
            
</FooterTemplate>
<HeaderTemplate>
                <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <th>
                            Number Of Parker
                        </th>
                        <th>
                            Plan Name
                        </th>
                        <th>
                            Monthly Rate
                        </th>
                        <th>
                            Activation Fees
                        </th>
                        <th>
                            Deposit
                        </th>
                        <th>
                            Delivery Fees
                        </th>
                        <th>
                            Remove
                        </th>
                    </tr>
            
</HeaderTemplate>
<ItemTemplate>
                <tr>
                    <td>
                        <%-- dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                //dt_charge_Random.Columns.Add("monthlyrate", typeof(decimal));
                //dt_charge_Random.Columns.Add("activationfess", typeof(decimal));
                //dt_charge_Random.Columns.Add("deposit", typeof(decimal));
                //dt_charge_Random.Columns.Add("deliveryfees", typeof(decimal));
                //dt_charge_Random.Columns.Add("planname", typeof(string));
                //dt_charge_Random.Columns.Add("Username", typeof(string));
                //dt_charge_Random.Columns.Add("PlanID", typeof(int));--%><%#Eval("Total_Parker")%>
                    </td>
                    <asp:Label ID="totalparker" runat="server" Text='<%# Eval("Total_Parker") %>' 
                                        Visible="False"></asp:Label>
                    <td>
                        <%#Eval("planname")%>
                    </td>
                    <td>
                        <%#Eval("monthlyrate")%>
                    </td>
                    <td>
                        <%-- <asp:Label ID="Label2" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%><%#Eval("activationfess")%>
                    </td>
                    <td>
                        <%--<asp:Label ID="Label17" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%><%#Eval("deposit")%>
                    </td>
                    <td>
                        <%--  <asp:Label ID="Label18" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%><%#Eval("deliveryfees")%>
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton1" runat="server" 
                            CommandArgument='<%#Eval("PlanID") %>' CommandName="remove" CssClass="button">Remove</asp:LinkButton>
                    </td>
                </tr>
            
</ItemTemplate>
</asp:Repeater>


    </fieldset>
    </asp:Panel>


    <br />
<asp:Panel ID="Panel7" runat="server" Visible="False">
    <fieldset>
        <legend>Random</legend>
        <asp:Repeater ID="Repeater2" runat="server" onitemcommand="Repeater2_ItemCommand">
            <FooterTemplate>
                </table>
            
</FooterTemplate>
<HeaderTemplate>
                <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <th>
                            Number Of Parker
                        </th>
                        <th>
                            Plan Name
                        </th>
                        <th>
                            Monthly Rate
                        </th>
                        <th>
                            Activation Fees
                        </th>
                        <th>
                            Deposit
                        </th>
                        <th>
                            Delivery Fees
                        </th>
                        <th>
                            Remove
                        </th>
                    </tr>
            
</HeaderTemplate>
<ItemTemplate>
                <tr>
                    <td>
                        <%#Eval("Total_Parker")%>
                    </td>
                    <asp:Label ID="totalparker" runat="server" Text='<%# Eval("Total_Parker") %>' 
                                        Visible="False"></asp:Label>
                    <td>
                        <%#Eval("planname")%>
                    </td>
                    <td>
                        <%#Eval("monthlyrate")%>
                    </td>
                    <td>
                        <%-- <asp:Label ID="Label2" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%><%#Eval("activationfess")%>
                    </td>
                    <td>
                        <%--<asp:Label ID="Label17" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%><%#Eval("deposit")%>
                    </td>
                    <td>
                        <%--  <asp:Label ID="Label18" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>--%><%#Eval("deliveryfees")%>
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton1" runat="server" 
                            CommandArgument='<%#Eval("PlanID") %>' CommandName="remove" CssClass="button">Remove</asp:LinkButton>
                    </td>
                </tr>
            
</ItemTemplate>
</asp:Repeater>

    </fieldset>
</asp:Panel>

<asp:Button ID="btn_submit" runat="server" Text="Next" CssClass="button" 
            onclick="btn_submit_Click" />

</ContentTemplate>
</cc1:TabPanel>

       
       
       <cc1:TabPanel runat="server" HeaderText="TabPanel4" ID="TabPanel4"  CssClass="Initial" Visible="false" ><HeaderTemplate>
Add Parker Detail
</HeaderTemplate>
    

<ContentTemplate>
   <table class="registration-area">
   <tr>
   <td>
       <asp:Panel ID="reserved" runat="server" Width="360px"  ScrollBars="Vertical">
       <fieldset><legend>Reserved</legend>
   <asp:Repeater ID="resrevd" runat="server" Visible="False" onitemcommand="rpt_emp_ItemCommand" 
                >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                    Number Of Parker
                    </th>

                     <th>
              Plan Name
                    </th>
                
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("Total_Parker")%></td>
             
 
               <td>

            <%#Eval("planname")%></td>
            
           
        
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
                </fieldset>
       </asp:Panel>
   </td>
 
   <td>
   <asp:Panel ID="random" runat="server" Width="360px" ScrollBars="Vertical">
       <fieldset><legend>Random</legend>
   <asp:Repeater ID="randm" runat="server" Visible="False" onitemcommand="rpt_emp_ItemCommand" 
                >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                 <th>
                    Number Of Parker
                    </th>

                     <th>
              Plan Name
                    </th>
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("Total_Parker")%></td>
             
 
               <td>

            <%#Eval("planname")%></td>
           
           
        
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
   </fieldset>
       </asp:Panel>
   </td>
   </tr>
   </table>

    <asp:Panel ID="Panel10" runat="server" Width="698px">
    <fieldset><legend>Select Plan</legend>
        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
        <ContentTemplate>
    <table class="registration-area" style="width: 697px" >
    <tr>
    <td style="width: 71px" >
    Plan Type
    </td>
    <td style="width: 186px" >
        <asp:DropDownList ID="ddl_plantype" runat="server" 
            CssClass="twitterStyleTextbox" 
            onselectedindexchanged="ddl_plantype_SelectedIndexChanged" 
            AutoPostBack="True">
            <asp:ListItem Value="0">--Select--</asp:ListItem>
            <asp:ListItem Value="1">Reserved</asp:ListItem>
            <asp:ListItem Value="2">Random</asp:ListItem>
        </asp:DropDownList>
    </td>
     
    <td style="width: 111px">
        <asp:Label ID="lbl_plan" runat="server" Visible="False"></asp:Label>
        </td>
    <td>
        <asp:DropDownList ID="ddl_plan" runat="server" CssClass="twitterStyleTextbox" 
            onselectedindexchanged="ddl_plan_SelectedIndexChanged1" 
            AutoPostBack="true" Visible="False" >
        </asp:DropDownList>
        <asp:Label ID="lbl_reserved_totalparker" runat="server" Text="0" 
            Visible="False"></asp:Label>
        <asp:Label ID="lbl_random_totalparker" runat="server" Text="0" Visible="False"></asp:Label>
    </td>
    </tr></table>
    </ContentTemplate>
     <Triggers>
      
       <asp:AsyncPostBackTrigger ControlID="ddl_plantype" EventName="SelectedIndexChanged" />
       <asp:AsyncPostBackTrigger ControlID="ddl_plan" EventName="SelectedIndexChanged" />
         
                </Triggers>
        </asp:UpdatePanel>
    </fieldset>
    </asp:Panel>
    <asp:Panel ID="Panel11" runat="server" Width="718px">
    <fieldset><legend>Parker Detail</legend>
        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
        <ContentTemplate>
<table class="form-table" style="width: 699px">
    <tr><td class="style12" style="width: 77px" >Employee ID </td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="#CC0000"></asp:Label></td>
        <td class="style3">
            <asp:TextBox ID="txtboxcode_employeecode" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Employee Code" 
        ontextchanged="txtboxcode_employeecode_TextChanged" AutoPostBack="True"></asp:TextBox>
            
</td>
        <td class="style3"><asp:RequiredFieldValidator ID="rfv_code" runat="server" 
                ControlToValidate="txtboxcode_employeecode" ErrorMessage="*" 
                ForeColor="#CC0000" ValidationGroup="m"></asp:RequiredFieldValidator></td><td class="style11">&nbsp;</td>
        <td class="style11">
            &nbsp;</td>
        <td class="style5">

        <asp:Label ID="lblerror" runat="server" ForeColor="#FF3300" Visible="False"></asp:Label>

</td><td class="style14">
            <asp:Label ID="lbl_invoicenumber" runat="server" Text="" Visible="False"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style12" style="width: 77px">
            Name</td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label17" runat="server" ForeColor="#CC0000" Text="*"></asp:Label>
        </td>
        <td class="style3">
           <asp:TextBox ID="txtbox_emp_firstname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  First Name"></asp:TextBox>
            
        </td>
        <td class="style3">
           <asp:RequiredFieldValidator ID="rfv_code0" runat="server" 
                ControlToValidate="txtbox_emp_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator></td>
        <td class="style11">
           <asp:Label ID="Label19" runat="server" ForeColor="#CC0000" Text="*"></asp:Label></td>
        <td class="style11">
            
            Email</td>
        <td class="style5">
           <asp:TextBox ID="txtBox_emp_emailid" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Email Id" 
                ValidationGroup="a"></asp:TextBox>
            
        </td>
        <td class="style14">
        <asp:RequiredFieldValidator ID="rfv_code2" runat="server" 
                ControlToValidate="txtBox_emp_emailid" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ErrorMessage="Enter Correct Format" ControlToValidate="txtBox_emp_emailid" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
    </tr>
    <tr><td class="style12" style="width: 77px" >Address</td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label18" runat="server" ForeColor="#CC0000" Text="*"></asp:Label>
        </td>
        <td class="style3">
            <asp:TextBox ID="txtBox_emp_address1" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the St#" 
                    ValidationGroup="a"></asp:TextBox>

            

</td><td class="style3">
           <asp:RequiredFieldValidator ID="rfv_code1" runat="server" 
                ControlToValidate="txtBox_emp_address1" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator></td><td class="style11" >
            <asp:Label ID="Label20" runat="server" ForeColor="#CC0000" Text="*"></asp:Label></td>
        <td class="style11">
            
            Cell#</td>
        <td class="style5">
            <asp:TextBox ID="txtbox_emp_cell" runat="server" CssClass="twitterStyleTextbox" 
            placeholder="Enter the  Cell Number" ValidationGroup="a"></asp:TextBox>


            


</td><td class="style14" >
<asp:RequiredFieldValidator ID="rfv_code3" runat="server" 
                ControlToValidate="txtbox_emp_cell" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
            ControlToValidate="txtbox_emp_cell" ErrorMessage="Enter Correct Format" 
            ForeColor="#FF3300" 
            ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
            ValidationGroup="b"></asp:RegularExpressionValidator></td>
    </tr>
    </table>
    </ContentTemplate>
     <Triggers>
      
         <asp:AsyncPostBackTrigger ControlID="txtboxcode_employeecode" EventName="TextChanged" />
         
                </Triggers>
        </asp:UpdatePanel>
    </fieldset>
    </asp:Panel>
                 <table class="form-table" style="width: 1252px" ><tr><td class="style7" >
                         <asp:Button ID="btnclose1" runat="server" CssClass="button" 
                onclick="btnclose1_Click" style="text-align: center; margin-bottom: 0px;" 
                             Text="Finish" />

</td><td><asp:Button ID="btn_save" runat="server" Text="Save" CssClass="button" 
                  onclick="btn_save_Click" ValidationGroup="m" />

</td></tr></table><asp:Panel ID="Panel8" runat="server">
            <fieldset>
            
            <asp:Repeater ID="rpt_emp" runat="server" Visible="False" onitemcommand="rpt_emp_ItemCommand" 
                >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                     Parker's Code
                    </th>

                     <th>
                Parker Name
                    </th>
                   <%-- <th>Plan
                    </th>--%>
                    <th>
               Parker Email-Id
                    </th>
                   
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("empcode")%></td>
             
 
               <td>

            <%#Eval("emp_name")%></td>
                   <td>
          
            <%#Eval("emp_emailid")%></td>
           
        
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
            </fieldset> </asp:Panel>


                 
                  
                  
</ContentTemplate>

</cc1:TabPanel>
  

    <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="TabPanel5" Visible="false">
    <HeaderTemplate>
Add Vehicle
</HeaderTemplate>

    



<ContentTemplate>













                    <asp:Panel ID="Panel9" runat="server"  Width="100%" CssClass="parker-table">
                      <fieldset>
                    
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        DeleteCommand="DELETE FROM [tbl_CorporateEmployeeVehicleDetails] WHERE [EmployeeVehicleID] = @EmployeeVehicleID" 
        InsertCommand="INSERT INTO [tbl_CorporateEmployeeVehicleDetails] ([EmployeeID], [MakeID], [ModelID], [Year], [LicenseNo], [ColorID]) VALUES (@Parker_id, @Make, @Model, @Year, @LicenseNo, @Color)" 
        SelectCommand="SELECT tbl_CorporateEmployeeVehicleDetails.*,tbl_Vehicle_color.*,tbl_VehicleMakes.* FROM [tbl_CorporateEmployeeVehicleDetails] inner join tbl_Vehicle_color on tbl_Vehicle_color.VehiColorid=tbl_CorporateEmployeeVehicleDetails.ColorID inner join tbl_VehicleMakes on tbl_VehicleMakes.VehiMakeID=tbl_CorporateEmployeeVehicleDetails.MakeID WHERE [EmployeeID]=@Parker_id " 
         UpdateCommand="UPDATE [tbl_CorporateEmployeeVehicleDetails] SET  [MakeID] = @Make, [ModelID] = @Model, [Year] = @Year,  [LicenseNo] = @LicenseNo ,[ColorID] = @Color WHERE [EmployeeVehicleID] = @EmployeeVehicleID" 
                              onselecting="SqlDataSource2_Selecting">
        <DeleteParameters>
            <asp:Parameter Name="EmployeeVehicleID" Type="Int64" />
        </DeleteParameters>
       <InsertParameters>
            
            <asp:Parameter Name="Make" Type="Int64" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
              <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="Color" Type="Int64" />
          
           
            <asp:SessionParameter Name="Parker_id" SessionField="Parker_id" 
                Type="Int64" />
        
        </InsertParameters>
        <SelectParameters>
             <asp:SessionParameter Name="Parker_id" SessionField="Parker_id" 
                Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Parker_Id" Type="Int64" />
            <asp:Parameter Name="Make" Type="Int64" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="Int64" />
            <asp:Parameter Name="LicenseNo" Type="String" />
          
            <asp:Parameter Name="EmployeeVehicleID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource9" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
        SelectCommand="SELECT * FROM [tbl_VehicleMakes] ORDER BY [VehiMake]" 
                              onselecting="SqlDataSource9_Selecting">
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource10" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_Vehicle_color]" 
                              onselecting="SqlDataSource10_Selecting">
        </asp:SqlDataSource>
                    
                      <asp:ListView ID="ListView1" runat="server" DataKeyNames="EmployeeVehicleID" 
        DataSourceID="SqlDataSource2" InsertItemPosition="LastItem" 
        onselectedindexchanged="ListView1_SelectedIndexChanged">
        <AlternatingItemTemplate>
            <tr style="background-color: #FAFAD2;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button"  
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button" />
                </td>
                
                <td >
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="background-color: #FFCC66;color: #000080;">
                <td> 
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" CssClass="button"  
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="button" 
                        Text="Cancel" />
                </td>
                
                <td>
               <%-- <asp:Label ID="ParkerVehicleIdLabel1" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' Visible="false" />
                        <asp:TextBox ID="Parker_IdTextBox" runat="server" 
                        Text='<%# Bind("Parker_Id") %>' Visible="false" />--%>
                 <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource9" DataTextField="VehiMake" 
                DataValueField="VehiMakeID" SelectedValue='<%# Bind("VehiMakeID") %>'> </asp:DropDownList>

                    
                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("ModelID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
              

                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource10" DataTextField="VehiColor" 
                DataValueField="VehiColorid" SelectedValue='<%# Bind("ColorID") %>'> </asp:DropDownList>
                  
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table id="Table1" runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" CssClass="add"  
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="cancel"  
                        Text="Clear" />
                </td>
                 
                <td>
                    
                    <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource9" DataTextField="VehiMake" 
                DataValueField="VehiMakeID" SelectedValue='<%# Bind("Make", "{0}") %>'> </asp:DropDownList>
                    

                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource10" DataTextField="VehiColor" 
                DataValueField="VehiColorid" SelectedValue='<%# Bind("Color", "{0}") %>'> </asp:DropDownList>
                  <%--  <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />--%>
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
              
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="background-color: #FFFBD6;color: #333333;">
                <td>
             
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button"  />
                </td>
               
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td >
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td >
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
               
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table id="Table2" runat="server">
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="1"
                            style=" background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family:Open Sans; width:100%;">
                            <tr id="Tr2" runat="server" style="background-color: #FFFBD6;color: #333333;">
                                <th id="Th1" runat="server">
                                </th>
                               <%-- <th id="Th2" runat="server">
                                    ParkerVehicleId</th>
                                <th id="Th3" runat="server">
                                    Parker_Id</th>--%>
                                <th id="Th4"  runat="server">
                                    Make</th>
                                <th id="Th5" runat="server">
                                    Model</th>
                                <th id="Th6" runat="server">
                                    Year</th>
                                <th id="Th7" runat="server">
                                    Color</th>
                                <th id="Th8" runat="server">
                                    LicenseNo</th>
                                
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td2" runat="server" 
                        style="text-align: center;background-color: #6699ff;font-family: Verdana, Arial, Helvetica, sans-serif;color: #333333;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                    ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #FFCC66;font-weight: bold;color: #000080;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit" />
                </td>
                <td>
                    <%--<asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />--%>
                </td>
                <td>
                   <%-- <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />--%>
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                   <%-- <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />--%>
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
     </fieldset>
                      
                      </asp:Panel>
    <asp:Button ID="Button1" runat="server" Text="Add More Parker" CssClass="button" 
                        onclick="Button1_Click" />
    &nbsp;<asp:Button ID="btn_close" runat="server" Text="Close" CssClass="button" 
                        onclick="btn_close_Click" Visible="False"  />
   
    
</ContentTemplate>
    

</cc1:TabPanel>
</cc1:TabContainer>
    


    </div>
</div>
</asp:Content>


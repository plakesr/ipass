﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Drawing;

public partial class Admin_Registration_Of_Corporate : System.Web.UI.Page
{
   
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    int idrecently_reserved = 0;
    int idrecently_random = 0;

    protected void Page_Load(object sender, EventArgs e)
    {



        if (!IsPostBack == true)
        {
            try
            {



                dt_charge.Columns.Add("Total_Parker", typeof(int));

                dt_charge.Columns.Add("monthlyrate", typeof(decimal));
                dt_charge.Columns.Add("activationfess", typeof(decimal));
                dt_charge.Columns.Add("deposit", typeof(decimal));
                dt_charge.Columns.Add("deliveryfees", typeof(decimal));
                dt_charge.Columns.Add("planname", typeof(string));
                dt_charge.Columns.Add("Username", typeof(string));
                dt_charge.Columns.Add("PlanID", typeof(int));

                Session["Chargetable"] = dt_charge;



                dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                dt_charge_Random.Columns.Add("monthlyrate", typeof(decimal));
                dt_charge_Random.Columns.Add("activationfess", typeof(decimal));
                dt_charge_Random.Columns.Add("deposit", typeof(decimal));
                dt_charge_Random.Columns.Add("deliveryfees", typeof(decimal));
                dt_charge_Random.Columns.Add("planname", typeof(string));
                dt_charge_Random.Columns.Add("Username", typeof(string));
                dt_charge_Random.Columns.Add("PlanID", typeof(int));

                Session["ChargetableRandom"] = dt_charge_Random;



                empshow.Columns.Add("empcode", typeof(string));
                empshow.Columns.Add("emp_name", typeof(string));
                empshow.Columns.Add("emp_emailid", typeof(string));
                Session["empshow"] = empshow;







                //dt_charge.Columns.Add("Total_Parker", typeof(int));
                //dt_charge.Columns.Add("PlanType", typeof(string));
                //dt_charge.Columns.Add("TotalAmount", typeof(decimal));
                //dt_charge.Columns.Add("Planid", typeof(int));
                //dt_charge.Columns.Add("plandetail", typeof(string));
                //dt_charge.Columns.Add("Username", typeof(string));
                

                //Session["Chargetable"] = dt_charge;



                //dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                //dt_charge_Random.Columns.Add("PlanType", typeof(string));
                //dt_charge_Random.Columns.Add("TotalAmount", typeof(decimal));
                //dt_charge_Random.Columns.Add("Planid", typeof(int));
                //dt_charge_Random.Columns.Add("plandetail", typeof(string));
                //dt_charge_Random.Columns.Add("Username", typeof(string));

                //Session["ChargetableRandom"] = dt_charge_Random;


            
            }
            catch
            {

            }

        }
    }
 
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCountries(string prefixText)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
        con.Open();
        SqlCommand cmd = new SqlCommand("select LotName from tbl_LotMaster where LotName like ''+@Name+'%'", con);
        cmd.Parameters.AddWithValue("@Name", prefixText);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }
    DataTable empshow = new DataTable();
    DataTable res = new DataTable();
    public void Fillddlsites(int id)
    {

        //List<clsData> lst = new clsInsert().Getlot_chargeby1(id);
        //if (lst.Count() > 0)
        //{
        //    ddl_reservedtype.DataSource = lst;
        //    ddl_reservedtype.DataTextField = "Charge";
        //    ddl_reservedtype.DataValueField = "LotRateid";
        //    ddl_reservedtype.DataBind();



        //}
        //ddl_reservedtype.Items.Insert(0, new ListItem("Select Sites", "0"));
       // ddl_Randomparking.Items.Insert(0, new ListItem("Select Sites", "0"));

    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
      

        try
        {
            

            if (TabContainer1.ActiveTabIndex == 1)
            {
                if (Session["lotid"].ToString() == "" || Session["lotid"].ToString() == null)
                {
                   

                }
            }

            if (TabContainer1.ActiveTabIndex == 2)
            {
                if (Session["lotid"].ToString() == "" || Session["lotid"].ToString() == null)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select Lot')", true);

                }
            }

        }
        catch
        {
            TabContainer1.ActiveTabIndex = 0;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select Lot')", true);
        }
       

    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        try
        {
          
        DataTable dt_lotid = new clsInsert().Gelotid(txtbox_lotname.Text);
        Session["lotid"] = dt_lotid.Rows[0][1].ToString();
        TabContainer1.Tabs[1].Enabled = true;
        TabContainer1.ActiveTabIndex = 1;
        //Fillddlsites(Convert.ToInt32(Session["lotid"].ToString()));
       // bind_DeliveryMaster();
      
        //clsInsert onj = new clsInsert();
        //DataSet rec = onj.fetchrec("select  chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"].ToString()) + " and Parkingtype=1 and ISSpecial=0 and chargeby=3 ");
        //DataTable t = new DataTable("chargeadd");
        //t.Columns.Add("LotRateid", typeof(int));
        //t.Columns.Add("Charge", typeof(string));
        //if (rec.Tables[0].Rows.Count > 0)
        //{
        //    Panel6.Visible = true;
        //    Panel8.Visible = true;
        //    bindcharge_reserved();
        //    for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
        //    {
        //        DataRow dr = t.NewRow();

        //        if (rec.Tables[0].Rows[i][0].ToString() == "1")
        //        {
        //            dr[0] = rec.Tables[0].Rows[i][0].ToString();

        //            dr[1] = "Hourly";
        //            t.Rows.Add(dr);
        //        }
        //        if (rec.Tables[0].Rows[i][0].ToString() == "2")
        //        {
        //            dr[0] = rec.Tables[0].Rows[i][0].ToString();

        //            dr[1] = "Daily";
        //            t.Rows.Add(dr);
        //        }
        //        if (rec.Tables[0].Rows[i][0].ToString() == "3")
        //        {
        //            dr[0] = rec.Tables[0].Rows[i][0].ToString();

        //            dr[1] = "Monthly";
        //            t.Rows.Add(dr);
        //        }
        //        if (rec.Tables[0].Rows[i][0].ToString() == "4")
        //        {
        //            dr[0] = rec.Tables[0].Rows[i][0].ToString();

        //            dr[1] = "Weekly";
        //            t.Rows.Add(dr);
        //        }



        //    }

        //    RemoveDuplicateRows(t, "Charge");
        //    ddl_reservedtype.DataSource = t;
        //    ddl_reservedtype.DataTextField = "Charge";
        //    ddl_reservedtype.DataValueField = "LotRateid";
        //    ddl_reservedtype.DataBind();


            


        //}
        //else
        //{
        //    Panel6.Visible = false;
        //    Panel8.Visible = false;

        //}
        //DataSet rec1 = onj.fetchrec("select  chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"].ToString()) + " and Parkingtype=2 and ISSpecial=0 and chargeby=3");
        //DataTable t1 = new DataTable("chargeadd");
        //t1.Columns.Add("LotRateid", typeof(int));
        //t1.Columns.Add("Charge", typeof(string));
        //if (rec1.Tables[0].Rows.Count > 0)
        //{
        //    Panel7.Visible = true;
        //    Panel9.Visible = true;
        //    bindcharge_random();
        //    for (int i = 0; i < rec1.Tables[0].Rows.Count; i++)
        //    {
        //        DataRow dr1 = t1.NewRow();

        //        if (rec1.Tables[0].Rows[i][0].ToString() == "1")
        //        {
        //            dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

        //            dr1[1] = "Hourly";
        //            t1.Rows.Add(dr1);
        //        }
        //        if (rec1.Tables[0].Rows[i][0].ToString() == "2")
        //        {
        //            dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

        //            dr1[1] = "Daily";
        //            t1.Rows.Add(dr1);
        //        }
        //        if (rec1.Tables[0].Rows[i][0].ToString() == "3")
        //        {
        //            dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

        //            dr1[1] = "Monthly";
        //            t1.Rows.Add(dr1);
        //        }
        //        if (rec1.Tables[0].Rows[i][0].ToString() == "4")
        //        {
        //            dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

        //            dr1[1] = "Weekly";
        //            t1.Rows.Add(dr1);
        //        }



        //    }

        //    RemoveDuplicateRows(t1, "Charge");
        //    ddl_Randomparking.DataSource = t1;
        //    ddl_Randomparking.DataTextField = "Charge";
        //    ddl_Randomparking.DataValueField = "LotRateid";
        //    ddl_Randomparking.DataBind();
        //}
        //else
        //{
        //    Panel7.Visible = false;
        //    Panel9.Visible = false;

        //}
    }
    catch
     {
     }
    }
    
    //public void bind_DeliveryMaster()
    //{
    //    try
    //    {
    //        // int lotiddd = Convert.ToInt32(ddl_lotname.SelectedItem.Value);
    //        //clsInsert cs = new clsInsert();
    //        //DataSet ds1 = cs.select_operation("select tbl_SiteMaster.SiteId ,tbl_LotMaster.Lot_id from tbl_LotMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_LotMaster.Site_Id where tbl_LotMaster.Lot_id='" + lotiddd + "'");

    //        //int site_id_carddelivery = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
            

    //        DataSet ds = new clsInsert().fetchrec("select tbl_CardDeliveryID.*,tbl_CardDeliveryMaster.* from tbl_CardDeliveryMaster inner join tbl_CardDeliveryID on tbl_CardDeliveryID.card_delivery_id=tbl_CardDeliveryMaster.CardDeliveryId where tbl_CardDeliveryID.Lotid=" + Convert.ToInt32(Session["lotid"].ToString()));

    //        ddl_deliverymethod_corporate.Items.Clear();

    //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //        {
          
    //            ListItem l = new ListItem(ds.Tables[0].Rows[i][4].ToString() + " [Price :" + ds.Tables[0].Rows[i][6].ToString() + "]", ds.Tables[0].Rows[i][3].ToString());
    //            // ddl_deliverymethod.Items.Add(l);
    //            ddl_deliverymethod_corporate.Items.Add(l);
    //        }
    //    }
    //    catch
    //    {

    //    }



    //}
    protected void txtbox_Email_TextChanged(object sender, EventArgs e)
    {
        //DataTable dt = new clsInsert().alreadyexist("emailverify", txtbox_Email.Text.Trim());

        //if (dt.Rows.Count > 0)
        //{
        //    lbl_emailcheck.Visible = true;
        //    lbl_emailcheck.Text = "email already exist";
        //    txtbox_Email.Text = "";
        //}
        //else
        //{
        //    lbl_emailcheck.Visible = false;
        //    txtbox_Email.Text = txtbox_Email.Text;
        //}
    }
    protected void txtBox_username_TextChanged1(object sender, EventArgs e)
    {
        DataTable dt = new clsInsert().alreadyexist("usernamecheck", txtBox_username.Text.Trim());

        if (dt.Rows.Count > 0)
        {
            lbl_usercheck.Visible = true;
            lbl_usercheck.Text = "UserName already exist";
            txtBox_username.Text = "";
        }
        else
        {
            lbl_usercheck.Visible = false;
            txtBox_username.Text = txtBox_username.Text;
        }
    }
    int total_reserved_parker = 0;
    int total_random_parker = 0;

    //protected void btn_finish_corporate_Click(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        //if (txtbox_random_numberofparker.Text.Trim() == "" && TextBox1.Text.Trim() == "")
    //        //{
    //        //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Reserved Space OR Random Space.')", true);

    //        //}
    //        //else
    //        //{
    //        //    //for (int p = 0; p < Repeater1.Items.Count; p++)
    //            //{
    //            //    Label lbl = Repeater1.Items[p].FindControl("totalparker") as Label;
    //            //    total_reserved_parker = total_reserved_parker + Convert.ToInt32(lbl.Text);
    //            //}
              

    //            //if (total_reserved_parker == Convert.ToInt32(TextBox1.Text) )
    //            //{
    //            if (DropDownList1.SelectedValue == "1")
    //            {

    //                bool b = objData.mail(txtbox_Email.Text, "User Name :" + txtBox_username.Text + "\nPassword :" + Session["password"].ToString(), "Successfully Registerd on ipass" + System.DateTime.Now.ToString());
    //                if (b == true)
    //                {
    //                    try
    //                    {

    //                        insertrec_corporate();
    //                    }
    //                    catch
    //                    {
    //                    }
    //                    try
    //                    {
    //                        insertrec_corporate_billingdetails();
    //                    }
    //                    catch
    //                    {
    //                    }
    //                    try
    //                    {
    //                        insertrec_corporate_spacedetails();
    //                    }
    //                    catch
    //                    {

    //                    }


    //                    Response.Redirect("Admin_FillDetail_Of_Coporate_Employee.aspx");

    //                }
    //                else
    //                {
    //                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Email Id not exist.')", true);

    //                }





    //            }
    //            else
    //            {
    //                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select Check Counter As Full Charge for now ')", true);

    //            }

    //    //    }
    //    }
    //    catch
    //    {


    //    }
    //}
    //protected void btn_pre_Click(object sender, EventArgs e)
    //{
    //    TabContainer1.ActiveTabIndex = 0;
    //}

    int parkingtype = 0;
    int invoicetype_corporate;
    string customername = "";
    string bankaccounttype = "";
    string bankname = "";
    string branch = "";
    string bank_zip = "";
    string bank_city = "";
    string state_bank = "";
    string routingno = "";
    string bankaccountno = "";
    decimal total_randommoney = Convert.ToDecimal(0.00);
    decimal total_resrvedmoney =Convert.ToDecimal(0.00);

    decimal activationcharge_reserved = Convert.ToDecimal(0.00);
    decimal activationcharge_random = Convert.ToDecimal(0.00);
    decimal activationcharge = Convert.ToDecimal(0.00);


    decimal carddeliverycharges = Convert.ToDecimal(0.00);
    decimal carddeliverycharges_reserved = Convert.ToDecimal(0.00);
    decimal carddeliverycharges_random = Convert.ToDecimal(0.00);

    decimal monthlycharges = Convert.ToDecimal(0.00);
    decimal monthlycharges_reserved = Convert.ToDecimal(0.00);
    decimal monthlycharges_random= Convert.ToDecimal(0.00);

    decimal depositcharges = Convert.ToDecimal(0.00);
    decimal depositcharges_reserved = Convert.ToDecimal(0.00);
    decimal depositcharges_random = Convert.ToDecimal(0.00);


    string stat = "true";

    string stat1="False";
    decimal taxcharge = Convert.ToDecimal(0.00);
    int totalspace;
    int reservedspace=0;
    int randomsapce=0;
    decimal defaultvalue = Convert.ToDecimal(0.00);
    DataTable dt_reserveddetails;
    DataTable dt_randomdetails;
    DataTable dt_reserveddetails_billing;
    DataTable dt_randomdetails_billing;
    string billingtypeentry = "";
    string invoicenumber = "";
    decimal monthly_totalcharge =Convert.ToDecimal( 0.00);
    int dateformatch = 15;
    public void insertrec_corporate_billingdetails_dependonuseofparking()
    {




        try
        {

            if (Session["Chargetable"] != null)
            {
                dt_reserveddetails_billing = (DataTable)Session["Chargetable"];
            }
        }
        catch
        {
        }

        try
        {

            if (Session["ChargetableRandom"] != null)
            {
                dt_randomdetails_billing = (DataTable)Session["ChargetableRandom"];
            }
        }
        catch
        {

        }


        try
        {
            DataSet dsplanid=new clsInsert().fetchrec("select * from tbl_CorporateLotRatePlan where Corporateusername='"+txtBox_username.Text+"'");
            if (dsplanid.Tables[0].Rows.Count > 0)
            {
                for(int i=0;i<dsplanid.Tables[0].Rows.Count;i++)
                {
                  DataSet employeecount=new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where PlanRateID="+Convert.ToInt32(dsplanid.Tables[0].Rows[i][0].ToString()));
                  if (employeecount.Tables[0].Rows.Count > 0)
                  {
                      activationcharge_reserved = activationcharge_reserved + (Convert.ToDecimal(dsplanid.Tables[0].Rows[i]["ActivationFees"].ToString()) * Convert.ToInt32(employeecount.Tables[0].Rows.Count));
                      carddeliverycharges_reserved = carddeliverycharges_reserved + (Convert.ToDecimal(dsplanid.Tables[0].Rows[i]["DeliveryFees"].ToString()) * Convert.ToInt32(employeecount.Tables[0].Rows.Count));

                      monthlycharges_reserved = monthlycharges_reserved + (Convert.ToDecimal(dsplanid.Tables[0].Rows[i]["MonthlyRate"].ToString()) * Convert.ToInt32(employeecount.Tables[0].Rows.Count));
                    //  depositcharges_reserved = depositcharges_reserved + Convert.ToDecimal(dsplanid.Tables[0].Rows[i]["deposit"].ToString());
                  }
                }
            }
        }
        catch
        {
        }

        if (dt_reserveddetails_billing.Rows.Count > 0)
        {
            for (int i = 0; i < dt_reserveddetails_billing.Rows.Count; i++)
            {
                //activationcharge_reserved = activationcharge_reserved + (Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["activationfess"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                //carddeliverycharges_reserved = carddeliverycharges_reserved + (Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["deliveryfees"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));

                //monthlycharges_reserved = monthlycharges_reserved + (Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["monthlyrate"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                depositcharges_reserved = depositcharges_reserved + Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["deposit"].ToString());
            }
        }
        else
        {
            //activationcharge_reserved = Convert.ToDecimal("0.00");
            //carddeliverycharges_reserved = Convert.ToDecimal("0.00");
            //monthlycharges_reserved = Convert.ToDecimal("0.00");
            depositcharges_reserved = Convert.ToDecimal("0.00");
        }

        if (dt_randomdetails_billing.Rows.Count > 0)
        {
            for (int i = 0; i < dt_randomdetails_billing.Rows.Count; i++)
            {
                //activationcharge_random = activationcharge_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["activationfess"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                //carddeliverycharges_random = carddeliverycharges_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["deliveryfees"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                //monthlycharges_random = monthlycharges_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["monthlyrate"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                depositcharges_random = depositcharges_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["deposit"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
            }
        }
        else
        {
            //activationcharge_random = Convert.ToDecimal("0.00");
            //carddeliverycharges_random = Convert.ToDecimal("0.00");
            //monthlycharges_random = Convert.ToDecimal("0.00");
            depositcharges_random = Convert.ToDecimal("0.00");
        }

        depositcharges = depositcharges_random + depositcharges_reserved;

        if (monthlycharges_reserved>0)
        {
            int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());

            try
            {


                DataSet ds_billingentrytype = new clsInsert().fetchrec("select Billing_entry from tbl_LotMaster where Lot_id=" + LOTIDForInserting);
                if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "1")
                {
                    billingtypeentry = "Full Charge";
                }

                if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "2")
                {
                    billingtypeentry = "Prorate";

                }

                if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "3")
                {
                    billingtypeentry = "Prorate By Fort Night";

                }




                hst.Clear();
                // dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                //dt_charge_Random.Columns.Add("monthlyrate", typeof(decimal));
                //dt_charge_Random.Columns.Add("activationfess", typeof(decimal));
                //dt_charge_Random.Columns.Add("deposit", typeof(decimal));
                //dt_charge_Random.Columns.Add("deliveryfees", typeof(decimal));
                //dt_charge_Random.Columns.Add("planname", typeof(string));
                //dt_charge_Random.Columns.Add("Username", typeof(string));
                //dt_charge_Random.Columns.Add("PlanID", typeof(int));--%>
                activationcharge = activationcharge_reserved;
                carddeliverycharges = carddeliverycharges_reserved;

                monthlycharges = monthlycharges_reserved;
                if (billingtypeentry == "Full Charge")
                {
                    monthly_totalcharge = monthlycharges;
                }
                if (billingtypeentry == "Prorate")
                {
                    DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
                    int y = active.Year;
                    int m = active.Month;
                    int d = active.Day;
                    int days = DateTime.DaysInMonth(y, m);
                    int daysinmonthremaining = days - d;
                    double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days);
                    monthly_totalcharge = monthlycharges * Convert.ToDecimal(daystocharge);


                }
                if (billingtypeentry == "Prorate By Fort Night")
                {
                    DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
                    int d = active.Day;
                    if (d > dateformatch)
                    {
                        monthly_totalcharge = monthlycharges;

                    }
                    else
                    {
                        monthly_totalcharge = monthlycharges / 2;
                    }
                }



                taxcharge = Convert.ToDecimal("0.13");
                decimal subtotal = (activationcharge + carddeliverycharges + monthly_totalcharge);
                decimal total = (activationcharge + carddeliverycharges + monthly_totalcharge) * taxcharge;



                decimal grandtotal = activationcharge + carddeliverycharges + monthly_totalcharge + total+depositcharges;

                DateTime activedate = Convert.ToDateTime(txtbox_ActicationDate.Text);
                int year1 = activedate.Year;
                int month1 = activedate.Month;
                string month = "";
                if (month1 < 10)
                {
                    month = "0" + Convert.ToString(month1);
                }
                if (month1 >= 10)
                {
                    month = Convert.ToString(month1);

                }
                string yearmonth = Convert.ToString(year1) + Convert.ToString(month);
                DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
                if (dsinvoice.Tables[0].Rows.Count > 0)
                {
                    string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                    string[] strArr = oFName.Split('_');
                    int sLength = strArr.Length;
                    int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                    if (number < 10)
                    {
                        invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                    }
                    if (number >= 10 && number < 100)
                    {
                        invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                    }
                    if (number >= 100 && number < 1000)
                    {
                        invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                    }
                    if (number >= 1000 && number < 10000)
                    {
                        invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                    }
                }
                else
                {
                    invoicenumber = yearmonth + "_" + "000001";
                }

                //@invoicenumber,@startdate,@duedate,@subtotal,@tax,@totalamount,@dueamount,@billingtype,@Parkerusername
                hst.Add("action", "insert_billing_corporate");
                hst.Add("invoicenumber", invoicenumber);

                hst.Add("Parkerusername", txtBox_username.Text);

                hst.Add("startdate", Convert.ToDateTime(txtbox_ActicationDate.Text));

                DateTime today = Convert.ToDateTime(txtbox_ActicationDate.Text);
                int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                DateTime endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);


                // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
                hst.Add("duedate", endOfMonth);
                hst.Add("subtotal", subtotal);
                hst.Add("tax", Convert.ToDecimal(total));
                hst.Add("details", "Invoice At The Time Of Registration");

                hst.Add("totalamount", Convert.ToDecimal(grandtotal));

                decimal dueamount = Convert.ToDecimal(grandtotal);
                hst.Add("dueamount ", dueamount);
                hst.Add("billingtype", DropDownList1.SelectedValue);


                int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                if (result_billing > 0)
                {
                    // Sending Values in tbl_Charge Table

                    hst.Clear();
                    DataSet ds2 = new clsInsert().select_operation("select Inv_num from TBL_Invoice where Acct_Num='" + txtBox_username.Text + "'order by  ID desc");

                    string lastid_bill = ds2.Tables[0].Rows[0][0].ToString();
                    if (Convert.ToString(carddeliverycharges) != "0.00")
                    {
                        // @billid,@billtitle,@amountparticular,@date,@accountnumber
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Card Delivery Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(carddeliverycharges));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                    if (Convert.ToString(carddeliverycharges) == "0.00")
                    {
                        // @billid,@billtitle,@amountparticular,@date,@accountnumber
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Card Delivery Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(carddeliverycharges));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(activationcharge) != "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Activation Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(activationcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                    if (Convert.ToString(activationcharge) == "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Activation Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(activationcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(monthlycharges) != "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Monthly Parking Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(monthlycharges) == "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Monthly Parking Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(depositcharges) != "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Security Amount");
                        hst.Add("amountparticular", Convert.ToDecimal(depositcharges));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                    if (Convert.ToString(depositcharges) == "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Security Amount");
                        hst.Add("amountparticular", Convert.ToDecimal(depositcharges));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    //if (Convert.ToString(total) != "0.00")
                    //{
                    //    hst.Clear();
                    //    hst.Add("action", "fulldetailsaboutbill");
                    //    hst.Add("billid", lastid_bill);
                    //    hst.Add("billtitle", "Tax Charges");
                    //    hst.Add("amountparticular", Convert.ToDecimal(total));
                    //    hst.Add("date", DateTime.Now);
                    //    hst.Add("accountnumber", txtBox_username.Text);
                    //    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    //    if (result_billing_full > 0)
                    //    {
                    //    }
                    //}
                    //if (Convert.ToString(total) == "0.00")
                    //{
                    //    hst.Clear();
                    //    hst.Add("action", "fulldetailsaboutbill");
                    //    hst.Add("billid", lastid_bill);
                    //    hst.Add("billtitle", "Tax Charges");
                    //    hst.Add("amountparticular", Convert.ToDecimal(total));
                    //    hst.Add("date", DateTime.Now);
                    //    hst.Add("accountnumber", txtBox_username.Text);
                    //    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    //    if (result_billing_full > 0)
                    //    {
                    //    }
                    //}
                    //For Bill
                    lbl_invoicenumber.Text = invoicenumber;
                    // Sending Values in Payment Table

                    if (Convert.ToString(depositcharges) != "0.00")
                    {

                        //hst.Clear();
                        //hst.Add("action", "paymentinfo");
                        //hst.Add("acctnum", txtBox_username.Text);
                        //hst.Add("bocmmnt", "On the Time of Registration Deposit Charges");
                        //hst.Add("amountpaid", Convert.ToDecimal(depositcharges));
                        //hst.Add("reconciled", stat1);
                        //hst.Add("status", stat);
                        //hst.Add("transactiondate", DateTime.Now);
                        //hst.Add("paymntmenthod", "Cash");
                        ////hst.Add("dateofpayment", DateTime.Now);
                        ////@paymntmenthod,@dateofpayment
                        //int result_payment = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        //if (result_payment > 0)
                        //{
                        //    //upadate tbl_invoice amount_outstanding amount



                        //    if (Convert.ToDecimal(depositcharges) < Convert.ToDecimal(grandtotal))
                        //    {
                        //        hst.Clear();
                        //        hst.Add("action", "update_billing_corporate");
                        //        hst.Add("invoicenumber1", invoicenumber);
                        //        decimal dueamount1 = Convert.ToDecimal(grandtotal) - depositcharges;

                        //        hst.Add("Amt_Outstanding", Convert.ToDecimal(dueamount1));



                        //        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        //        if (result_billing_full > 0)
                        //        {
                                    hst.Clear();
                                    hst.Add("action", "insert");
                                    hst.Add("acctnumber", txtBox_username.Text);
                                    hst.Add("reason", "Advance At the Time Of Registration");
                                    hst.Add("creditamt", Convert.ToDecimal("0.00"));
                                    hst.Add("dateoftransaction", DateTime.Now);
                                    hst.Add("status", status);

                                    int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                                    {
                                    }
                        //        }
                        //    }

                        //    if (Convert.ToDecimal(depositcharges) > Convert.ToDecimal(grandtotal))
                        //    {
                        //        hst.Clear();
                        //        hst.Add("action", "update_billing_corporate");
                        //        hst.Add("invoicenumber1", invoicenumber);
                        //        decimal dueamount1 = depositcharges - Convert.ToDecimal(grandtotal);

                        //        hst.Add("Amt_Outstanding", Convert.ToDecimal("0.00"));



                        //        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        //        if (result_billing_full > 0)
                        //        {
                        //            hst.Clear();
                        //            hst.Add("action", "insert");
                        //            hst.Add("acctnumber", txtBox_username.Text);
                        //            hst.Add("reason", "Advance At the Time Of Registration");
                        //            hst.Add("creditamt", Convert.ToDecimal(dueamount1));
                        //            hst.Add("dateoftransaction", DateTime.Now);
                        //            hst.Add("status", status);

                        //            int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                        //            {
                        //            }
                        //        }
                        //    }
                        //}
                    }






                }

            }
            catch
            {

            }
            countofparker = 1;
        }

        else
        {
            //if (Convert.ToString(depositcharges) != "0.00")
            //{

            ////    hst.Clear();
            ////    hst.Add("action", "paymentinfo");
            ////    hst.Add("acctnum", txtBox_username.Text);
            ////    hst.Add("bocmmnt", "On the Time of Registration Deposit Charges");
            ////    hst.Add("amountpaid", Convert.ToDecimal(depositcharges));
            ////    hst.Add("reconciled", stat1);
            ////    hst.Add("status", stat);
            ////    hst.Add("transactiondate", DateTime.Now);
            ////    hst.Add("paymntmenthod", "Cash");
            ////    //hst.Add("dateofpayment", DateTime.Now);
            ////    //@paymntmenthod,@dateofpayment
            ////    int result_payment = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
            ////    if (result_payment > 0)
            ////    {

            ////        hst.Clear();
            ////        hst.Add("action", "insert");
            ////        hst.Add("acctnumber", txtBox_username.Text);
            ////        hst.Add("reason", "Advance At the Time Of Registration");
            ////        hst.Add("creditamt", Convert.ToDecimal(depositcharges));
            ////        hst.Add("dateoftransaction", DateTime.Now);
            ////        hst.Add("status", status);

            ////        int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
            ////        {
            ////        }

            ////    }

            //}
                   
                    lbl_invoicenumber.Text = "";
                    countofparker = 0;
        }

    }
    public void insertrec_corporate()
    {


        //Hashtable second = (Hashtable)Session["hasht2"];
        
       // Session["fullname"] = firstname + "   " + lastname;
        
        //if (t.Text == "")
        //{
        //    cardnumber = "0";
        //}
        //else
        //{

        //    cardnumber = txtbox_coupannumber.Text;
        //}
        //  Session["Full Address"] = address1 + adress2;
        string account_type = "Corporate";
        int PlanrateID = Convert.ToInt32(parkingtype);
        int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());
        int Parkingtype = Convert.ToInt32(parkingtype);

        string Compnayname = txtBox_Companyname.Text;
        string Contactpersonname = txtbox_Firstname.Text;
       
        string cardnumber = "0";


        string address1 = txtbox_address1.Text;
        string adress2 = txtbox_address1.Text;

      
        string postal_parker = txtbox_postal.Text;
        string country_parker = ddl_Parkercountry.SelectedItem.Text;
        string state_parker = ddlstate.SelectedItem.Text;
        string city_parker = ddl_Parkercity.SelectedItem.Text;
        string cellular_parker = txtbox_cellular.Text;
        string phone_parker = txtbox_phone.Text;
        string fax_parker = txt_fax.Text;
        string email_parker = txtbox_Email.Text;
        string loginusername_parker = txtBox_username.Text;
        string pass_parker = Session["password"].ToString();
        string type = "Corporate";
        string userstatus = "true";
        string oprator = txtbox_designation.Text;
        string modifiedby = "Admin";
        DateTime activationdate;
        int carddeliveryid = Convert.ToInt32(0);

          if (txtbox_ActicationDate.Text != "")
          {
              activationdate = Convert.ToDateTime(txtbox_ActicationDate.Text);
          }
          else
          {
              activationdate = DateTime.Now;
          }


        if (rbtn_invoicebymail.Checked == true)
        {
            invoicetype_corporate = 3;
        }
        if (rbtn_creditcard.Checked == true)
        {
            invoicetype_corporate = 1;


        }

        if (rbtn_invoicebyemail.Checked == true)
        {
            invoicetype_corporate = 2;


        }

        if (rbtn_ach.Checked == true)
        {
            invoicetype_corporate = 4;

        }



        if (invoicetype_corporate == 4)
        {

            customername = txtbox_customername.Text;
            bankaccounttype = ddl_accounttype.SelectedItem.Text;
            bankname = txtbox_bankname.Text;
            branch = txtbox_branch.Text;
            bank_zip = txtbox_bankzip.Text;
            bank_city = txtbox_city_bank.Text;
            state_bank = ddl_bankstate.SelectedItem.Text;
            routingno = txtbox_routingno.Text;
            bankaccountno = txtbox_bankaccountno.Text;
        }

       



        clsInsert cs = new clsInsert();
        //DataSet ds1 = cs.select_operation("select * from tbl_carddeliverymaster where carddeliveryid='" + carddeliveryid + "'");
        //   string chargedfor = ds1.Tables[0].Rows[0]["carddeliveryname"].ToString();
        // string charges = ds1.Tables[0].Rows[0]["charges"].ToString();

        string deposite = "false";


        try
        {
            hst.Clear();


            hst.Add("action", "insert");
            hst.Add("Lotid", LOTIDForInserting);
            hst.Add("parkingtype", Parkingtype);
            hst.Add("planrateid", parkingtype);

            hst.Add("accounttype", account_type);
            hst.Add("UserName", loginusername_parker);
            hst.Add("CardNo ", cardnumber);
            hst.Add("firstname", Compnayname);
            hst.Add("lastname", Contactpersonname);
            hst.Add("AdressLine1", address1);
            hst.Add("AddressLine2", adress2);
            hst.Add("Zip_Parker", postal_parker);
            hst.Add("Country_parker", country_parker);
            hst.Add("State_Parker", state_parker);
            hst.Add("city_parker", city_parker);

            hst.Add("cell_parker", cellular_parker);
            hst.Add("phone_parker", phone_parker);
            hst.Add("fax", fax_parker);
            hst.Add("email", email_parker);
            hst.Add("billing", invoicetype_corporate);
            //hst.Add("tariffplanid", tariifid);
             hst.Add("activationdate",Convert.ToDateTime(activationdate));
            hst.Add("carddeliveryid", carddeliveryid);

            hst.Add("login_username", loginusername_parker);
            hst.Add("pass", pass_parker);
            hst.Add("type", type);
            hst.Add("userstatus", userstatus);
            hst.Add("operatorid", oprator);
            hst.Add("modifiedby", modifiedby);
            hst.Add("dateofregis ", DateTime.Now);
           
            if (invoicetype_corporate == 4)
            {
                hst.Add("AccountUserName", loginusername_parker);
                hst.Add("CustomerName", customername);
                hst.Add("BankAccountType", bankaccounttype);
                hst.Add("BranchName", branch);
                hst.Add("BankName", bankname);
                hst.Add("Bank_zip", bank_zip);
                hst.Add("Bank_city", bank_city);
                hst.Add("Bank_state", state_bank);
                hst.Add("bankrouting", routingno);
                hst.Add("bankAccountNo", bankaccountno);

            }


            int result = objData.ExecuteNonQuery("[sp_Parker_Corporate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {


        }
        }
        catch
        {

        }




    }
    string status = "true";
    public void insertrec_corporate_billingdetails()
    {



       
        try
        {

            if (Session["Chargetable"] != null)
            {
                dt_reserveddetails_billing = (DataTable)Session["Chargetable"];
            }
        }
        catch
        {
        }

        try
        {

            if (Session["ChargetableRandom"] != null)
            {
                dt_randomdetails_billing = (DataTable)Session["ChargetableRandom"];
            }
        }
        catch
        {

        }
        int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());

        try
        {


            DataSet ds_billingentrytype = new clsInsert().fetchrec("select Billing_entry from tbl_LotMaster where Lot_id=" + LOTIDForInserting);
            if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "1")
            {
                billingtypeentry = "Full Charge";
            }

            if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "2")
            {
                billingtypeentry = "Prorate";

            }

            if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "3")
            {
                billingtypeentry = "Prorate By Fort Night";

            }




            hst.Clear();
           // dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
            //dt_charge_Random.Columns.Add("monthlyrate", typeof(decimal));
            //dt_charge_Random.Columns.Add("activationfess", typeof(decimal));
            //dt_charge_Random.Columns.Add("deposit", typeof(decimal));
            //dt_charge_Random.Columns.Add("deliveryfees", typeof(decimal));
            //dt_charge_Random.Columns.Add("planname", typeof(string));
            //dt_charge_Random.Columns.Add("Username", typeof(string));
            //dt_charge_Random.Columns.Add("PlanID", typeof(int));--%>
            if (dt_reserveddetails_billing.Rows.Count > 0)
            {
                for (int i = 0; i < dt_reserveddetails_billing.Rows.Count; i++)
                {
                    activationcharge_reserved = activationcharge_reserved + (Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["activationfess"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                    carddeliverycharges_reserved = carddeliverycharges_reserved + (Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["deliveryfees"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));

                    monthlycharges_reserved = monthlycharges_reserved + (Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["monthlyrate"].ToString()) * Convert.ToInt32(dt_reserveddetails_billing.Rows[i]["Total_Parker"].ToString()));
                    depositcharges_reserved = depositcharges_reserved + Convert.ToDecimal(dt_reserveddetails_billing.Rows[i]["deposit"].ToString()) ;
                }
            }
            else
            {
                activationcharge_reserved = Convert.ToDecimal("0.00");
                carddeliverycharges_reserved = Convert.ToDecimal("0.00");
                monthlycharges_reserved = Convert.ToDecimal("0.00");
                depositcharges_reserved = Convert.ToDecimal("0.00");
            }

            if (dt_randomdetails_billing.Rows.Count > 0)
            {
                for (int i = 0; i < dt_randomdetails_billing.Rows.Count; i++)
                {
                    activationcharge_random = activationcharge_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["activationfess"].ToString()) * Convert.ToInt32(dt_randomdetails_billing.Rows[i]["Total_Parker"].ToString()));
                    carddeliverycharges_random = carddeliverycharges_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["deliveryfees"].ToString()) * Convert.ToInt32(dt_randomdetails_billing.Rows[i]["Total_Parker"].ToString()));
                    monthlycharges_random = monthlycharges_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["monthlyrate"].ToString()) * Convert.ToInt32(dt_randomdetails_billing.Rows[i]["Total_Parker"].ToString()));
                    depositcharges_random = depositcharges_random + (Convert.ToDecimal(dt_randomdetails_billing.Rows[i]["deposit"].ToString()));
                }
            }
            else
            {
                activationcharge_random = Convert.ToDecimal("0.00");
                carddeliverycharges_random = Convert.ToDecimal("0.00");
                monthlycharges_random = Convert.ToDecimal("0.00");
                depositcharges_random = Convert.ToDecimal("0.00");
            }

            depositcharges=depositcharges_random+depositcharges_reserved;
            activationcharge = activationcharge_random + activationcharge_reserved;
            carddeliverycharges = carddeliverycharges_reserved+carddeliverycharges_random;

            monthlycharges = monthlycharges_reserved + monthlycharges_random;
            if (billingtypeentry == "Full Charge")
            {
                monthly_totalcharge = monthlycharges;
            }
            if (billingtypeentry == "Prorate")
            {
                DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
                int y = active.Year;
                int m = active.Month;
                int d = active.Day;
                int days = DateTime.DaysInMonth(y, m);
                int daysinmonthremaining = days - d;
                double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days); 
                monthly_totalcharge = monthlycharges *Convert.ToDecimal( daystocharge);

 
            }
            if (billingtypeentry == "Prorate By Fort Night")
            {
                DateTime active=Convert.ToDateTime(txtbox_ActicationDate.Text);
                int d=active.Day;
                if (d > dateformatch)
                {
                    monthly_totalcharge = monthlycharges;

                }
                else
                {
                    monthly_totalcharge = monthlycharges / 2;
                }
            }
          


            taxcharge = Convert.ToDecimal("0.13");
            decimal subtotal = (activationcharge + carddeliverycharges + monthly_totalcharge);
            decimal total = (activationcharge + carddeliverycharges + monthly_totalcharge) * taxcharge;



            decimal grandtotal = activationcharge + carddeliverycharges + monthly_totalcharge + total+depositcharges;

            DateTime activedate = Convert.ToDateTime(txtbox_ActicationDate.Text);
            int year1 = activedate.Year;
            int month1 = activedate.Month;
            string month = "";
            if (month1 < 10)
            {
                month = "0" + Convert.ToString(month1);
            }
            if (month1 >= 10)
            {
                month = Convert.ToString(month1);

            }
            string yearmonth = Convert.ToString(year1) + Convert.ToString(month);
            DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
            if (dsinvoice.Tables[0].Rows.Count > 0)
            {
                string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                string[] strArr = oFName.Split('_');
                int sLength = strArr.Length;
                int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                if (number < 10)
                {
                    invoicenumber = yearmonth + "_" +"00000"+ Convert.ToString(number);
                }
                if (number >= 10 && number<100)
                {
                    invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                }
                if (number >= 100 && number < 1000)
                {
                    invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                }
                if (number >= 1000 && number < 10000)
                {
                    invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                }
            }
            else
            {
                invoicenumber = yearmonth + "_" + "000001";
            }
           
            //@invoicenumber,@startdate,@duedate,@subtotal,@tax,@totalamount,@dueamount,@billingtype,@Parkerusername
            hst.Add("action", "insert_billing_corporate");
            hst.Add("invoicenumber", invoicenumber);

            hst.Add("Parkerusername", txtBox_username.Text);

            hst.Add("startdate", Convert.ToDateTime(txtbox_ActicationDate.Text));

            DateTime today = Convert.ToDateTime(txtbox_ActicationDate.Text);
            int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);             
            DateTime endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);


           // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
            hst.Add("duedate", endOfMonth);
            hst.Add("subtotal", subtotal);
            hst.Add("tax",Convert.ToDecimal( total));
            hst.Add("details", "Invoice At The Time Of Registration");

            hst.Add("totalamount", Convert.ToDecimal(grandtotal));
          
            decimal dueamount = Convert.ToDecimal(grandtotal) ;
            hst.Add("dueamount ", dueamount);
            hst.Add("billingtype", DropDownList1.SelectedValue);


            int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
            if (result_billing > 0)
            {
                // Sending Values in tbl_Charge Table
                
                hst.Clear();
                DataSet ds2 = new clsInsert().select_operation("select Inv_num from TBL_Invoice where Acct_Num='" + txtBox_username.Text + "'order by  ID desc");

                string lastid_bill = ds2.Tables[0].Rows[0][0].ToString();
                if (Convert.ToString(carddeliverycharges) != "0.00")
                {
                   // @billid,@billtitle,@amountparticular,@date,@accountnumber
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Card Delivery Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(carddeliverycharges));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);

                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }

                if (Convert.ToString(carddeliverycharges) == "0.00")
                {
                    // @billid,@billtitle,@amountparticular,@date,@accountnumber
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Card Delivery Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(carddeliverycharges));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);

                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }
                if (Convert.ToString(activationcharge) != "0.00")
                {
                    hst.Clear();
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Activation Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(activationcharge));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);
                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }
                if (Convert.ToString(activationcharge) == "0.00")
                {
                    hst.Clear();
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Activation Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(activationcharge));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);
                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }
                if (Convert.ToString(monthlycharges) != "0.00")
                {
                    hst.Clear();
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Monthly Parking Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);
                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }
                if (Convert.ToString(monthlycharges) == "0.00")
                {
                    hst.Clear();
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Monthly Parking Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);
                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }

                if (Convert.ToString(depositcharges) != "0.00")
                {
                    hst.Clear();
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Security Amount");
                    hst.Add("amountparticular", Convert.ToDecimal(depositcharges));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);
                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }

                if (Convert.ToString(depositcharges) == "0.00")
                {
                    hst.Clear();
                    hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Security Amount");
                    hst.Add("amountparticular", Convert.ToDecimal(depositcharges));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);
                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    if (result_billing_full > 0)
                    {
                    }
                }





                //if (Convert.ToString(total) != "0.00")
                //{
                //    hst.Clear();
                //    hst.Add("action", "fulldetailsaboutbill");
                //    hst.Add("billid", lastid_bill);
                //    hst.Add("billtitle", "Tax Charges");
                //    hst.Add("amountparticular", Convert.ToDecimal(total));
                //    hst.Add("date", DateTime.Now);
                //    hst.Add("accountnumber", txtBox_username.Text);
                //    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                //    if (result_billing_full > 0)
                //    {
                //    }
                //}


                //if (Convert.ToString(total) == "0.00")
                //{
                //    hst.Clear();
                //    hst.Add("action", "fulldetailsaboutbill");
                //    hst.Add("billid", lastid_bill);
                //    hst.Add("billtitle", "Tax Charges");
                //    hst.Add("amountparticular", Convert.ToDecimal(total));
                //    hst.Add("date", DateTime.Now);
                //    hst.Add("accountnumber", txtBox_username.Text);
                //    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                //    if (result_billing_full > 0)
                //    {
                //    }
                //}
                // For Bill
                lbl_invoicenumber.Text = invoicenumber;

                // Sending Values in Payment Table

                if (Convert.ToString(depositcharges) != "0.00")
                {
                   
                    //hst.Clear();
                    //hst.Add("action", "paymentinfo");
                    //hst.Add("acctnum", txtBox_username.Text);
                    //hst.Add("bocmmnt", "On the Time of Registration Deposit Charges");
                    //hst.Add("amountpaid", Convert.ToDecimal(depositcharges));
                    //hst.Add("reconciled", stat1);
                    //hst.Add("status", stat);
                    //hst.Add("transactiondate", DateTime.Now);
                    //hst.Add("paymntmenthod", "Cash");
                    ////hst.Add("dateofpayment", DateTime.Now);
                    ////@paymntmenthod,@dateofpayment
                    //int result_payment = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                    //if (result_payment > 0)
                    //{
                    //    //upadate tbl_invoice amount_outstanding amount

                    //    if (Convert.ToDecimal(depositcharges) < Convert.ToDecimal(grandtotal))
                    //    {
                            //hst.Clear();
                            //hst.Add("action", "update_billing_corporate");
                            //hst.Add("invoicenumber1", invoicenumber);
                            //decimal dueamount1 = Convert.ToDecimal(grandtotal) - depositcharges;

                            //hst.Add("Amt_Outstanding", Convert.ToDecimal(dueamount1));



                            //int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                            //if (result_billing_full > 0)
                            //{
                                hst.Clear();
                                hst.Add("action", "insert");
                                hst.Add("acctnumber", txtBox_username.Text);
                                hst.Add("reason", "Advance At the Time Of Registration");
                                hst.Add("creditamt", Convert.ToDecimal("0.00"));
                                hst.Add("dateoftransaction", DateTime.Now);
                                hst.Add("status", status);

                                int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                                {
                                }
                        //    }
                        //}

                        //if (Convert.ToDecimal(depositcharges) > Convert.ToDecimal(grandtotal))
                        //{
                        //    hst.Clear();
                        //    hst.Add("action", "update_billing_corporate");
                        //    hst.Add("invoicenumber1", invoicenumber);
                        //    decimal dueamount1 =depositcharges- Convert.ToDecimal(grandtotal);

                        //    hst.Add("Amt_Outstanding", Convert.ToDecimal("0.00"));



                        //    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        //    if (result_billing_full > 0)
                        //    {
                        //        hst.Clear();
                        //        hst.Add("action", "insert");
                        //        hst.Add("acctnumber", txtBox_username.Text);
                        //        hst.Add("reason", "Advance At the Time Of Registration");
                        //        hst.Add("creditamt", Convert.ToDecimal(dueamount1));
                        //        hst.Add("dateoftransaction", DateTime.Now);
                        //        hst.Add("status", status);

                        //        int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                        //        {
                        //        }
                        //    }
                        //}
                    }
               // }






            }

        }
        catch
        {

        }  

        }
       
    public void insertrec_corporate_spacedetails()
    {
        try
        {

            if(Session["Chargetable"]!=null)
            {
                dt_reserveddetails=(DataTable)Session["Chargetable"];
            }
        }
        catch
        {
        }

         try
        {

            if(Session["ChargetableRandom"]!=null)
            {
                dt_randomdetails=(DataTable)Session["ChargetableRandom"];
            }
        }
        catch
        {

        }
         if (dt_reserveddetails.Rows.Count > 0)
         {
             for (int i = 0; i < dt_reserveddetails.Rows.Count; i++)
             {

                 reservedspace =reservedspace + Convert.ToInt32(dt_reserveddetails.Rows[i]["Total_Parker"].ToString());
                
                 
             }
         }
         else
         {
             reservedspace = Convert.ToInt32("0");
         }




         if (dt_randomdetails.Rows.Count > 0)
         {
             for (int i = 0; i < dt_randomdetails.Rows.Count; i++)
             {

                 randomsapce = randomsapce + Convert.ToInt32(dt_randomdetails.Rows[i]["Total_Parker"].ToString());
                

             }
         }

         else
         {
             randomsapce = Convert.ToInt32("0");
         }



        //if(TextBox1.Text!="")
        //{
        //    reservedspace=Convert.ToInt32(TextBox1.Text);
        //}
        // if(TextBox1.Text.Trim()=="")
        //{
        //    reservedspace=Convert.ToInt32("0");

        //}

        // if(txtbox_totalrandomspace.Text!="")
        //{
        //    randomsapce=Convert.ToInt32(txtbox_totalrandomspace.Text);
        //}
        // if(txtbox_totalrandomspace.Text.Trim()=="")
        //{
        //    randomsapce=Convert.ToInt32("0");

        //}
        totalspace=reservedspace+randomsapce;
        int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());
         try
        {
            hst.Clear();
           
                hst.Add("action", "totalspaceinsert");
                hst.Add("parkerusername", txtBox_username.Text);
                hst.Add("randomspace", Convert.ToDecimal(randomsapce));
                hst.Add("totalspace", totalspace);
                hst.Add("reservedspace ", reservedspace);
                hst.Add("lotid ", LOTIDForInserting);

              

                int result_totalspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
                if (result_totalspace > 0)
                {
                    try
                    {
                        if (dt_reserveddetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt_reserveddetails.Rows.Count; i++)
                            {
                                hst.Clear();
                                hst.Add("action", "reservedspaceinsert");

                                hst.Add("numberofparker_reserved", dt_reserveddetails.Rows[i]["Total_Parker"].ToString());
                                hst.Add("planid_reserved", dt_reserveddetails.Rows[i]["Planid"].ToString());
                                hst.Add("parkerusername_reserved", txtBox_username.Text);

                                int result_reservedspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
                                if (result_reservedspace > 0)
                                {

                                    //Response.Redirect("UserManager.aspx");

                                }
                                else
                                {
                                    // lbl_error.Visible = true;

                                }
                            }
                        }
                    }
                    catch
                    {
                    }

                    try
                    {
                        if (dt_randomdetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt_randomdetails.Rows.Count; i++)
                            {
                                hst.Clear();
                                hst.Add("action", "randomspaceinsert");

                                hst.Add("numberofparker_random", dt_randomdetails.Rows[i]["Total_Parker"].ToString());
                                hst.Add("planid_random", dt_randomdetails.Rows[i]["Planid"].ToString());
                                hst.Add("parkerusername_random", txtBox_username.Text);

                                int result_randomspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
                                if (result_randomspace > 0)
                                {

                                    //Response.Redirect("UserManager.aspx");

                                }
                                else
                                {
                                    // lbl_error.Visible = true;

                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                }

            }
           catch
        {

        }  

        }

        


   
    public void clear()
    {
        //txtBox_firstname.Text = "";
        //txtbox_lastname.Text = "";
        //txtbox_Email.Text = "";
        //txtbox_coupannumber.Text = "";
        //txt_fax.Text = "";
        //txtbox_address1.Text = "";
        //txtbox_address2.Text = "";
        //txtbox_balance.Text = "";
        //txtbox_bankaccountno.Text = "";
        //// ddl_Parkercity.SelectedIndex = 0;
        //txtbox_cellular.Text = "";
        //txtbox_postal.Text = "";
        //txtbox_phone.Text = "";
        //txtBox_username.Text = "";
        //txtbox_bankname.Text = "";
        //txtbox_bankzip.Text = "";
        //txtbox_city_bank.Text = "";


    }

    protected void rbtn_creditcard_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_creditcard.Checked == true)
        {
            btn_entercreditcard.Visible = true;
            Panel5.Visible = false;

            //Panel4.Visible = false;
        }
        else
        {
            btn_entercreditcard.Visible = false;
            // Panel4.Visible = false;
        }
        if (rbtn_ach.Checked == true)
        {
            Panel5.Visible = true;
            btn_entercreditcard.Visible = false;

        }
        else
        {

            // Panel4.Visible = false;
        }

        UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel2.Update();
    }
    protected void rbtn_invoicebymail0_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_creditcard.Checked == true)
        {
            btn_entercreditcard.Visible = true;
            Panel5.Visible = false;

            //Panel4.Visible = false;
        }
        else
        {
            Panel5.Visible = false;
            btn_entercreditcard.Visible = false;
        }
        if (rbtn_ach.Checked == true)
        {
            Panel5.Visible = true;
            btn_entercreditcard.Visible = false;

        }
        else
        {
            Panel5.Visible = false;
            btn_entercreditcard.Visible = false;

            // Panel4.Visible = false;
        }
        UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel2.Update();
    }
    protected void rbtn_invoicebyemail0_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (rbtn_creditcard.Checked == true)
            {
                btn_entercreditcard.Visible = true;
                Panel5.Visible = false;


                //Panel4.Visible = false;
            }
            else
            {
                Panel5.Visible = false;
                btn_entercreditcard.Visible = false;
            }
            if (rbtn_ach.Checked == true)
            {
                Panel5.Visible = true;
                btn_entercreditcard.Visible = false;

            }
            else
            {
                Panel5.Visible = false;
                btn_entercreditcard.Visible = false;

                // Panel4.Visible = false;
            }
            UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
            UpdatePanel2.Update();
        }
        catch
        {
        }
    }
    protected void rbtn_ach_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (rbtn_creditcard.Checked == true)
            {
                btn_entercreditcard.Visible = true;
                Panel5.Visible = false;

                //Panel4.Visible = false;
            }
            else
            {
                btn_entercreditcard.Visible = false;
                // Panel4.Visible = false;
            }
            if (rbtn_ach.Checked == true)
            {
                Panel5.Visible = true;
                btn_entercreditcard.Visible = false;

                UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
                UpdatePanel2.Update();

            }
            else
            {

                // Panel4.Visible = false;
            }
        }
        catch
        {
        }
    }
    protected void btn_entercreditcard_Click(object sender, EventArgs e)
    {

    }
   
    protected void Repeater1_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            //if (e.CommandName == "cmdDelete")
            //{
                
            //    if (Session["Type"].ToString() == "admin")
            //    {
            //        delCountry(ID);
            //        Response.Redirect("Admin_CountryMaster.aspx");
            //    }
            //    if (Session["Type"].ToString() == "operator")
            //    {
            //        if (Session["lbldelete"].ToString() == "False")
            //        {
            //            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

            //        }
            //        else
            //        {
            //            delCountry(ID);
            //            Response.Redirect("Operator_CountryMaster.aspx");
            //        }
            //    }

            //}
            if (e.CommandName == "remove")
            {
                Label l = (Label)e.Item.FindControl("Label2");
                int ID = Convert.ToInt32(e.CommandArgument.ToString());
                delete(ID);
                dt_charge = (DataTable)Session["Chargetable"];

                dt_charge.Rows.RemoveAt(e.Item.ItemIndex);

                Repeater1.DataSource = dt_charge;
                Repeater1.DataBind();

                Session["Chargetable"] = dt_charge;

            }
        }
        catch
        {
        }
    }
    public void delete(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "deleteplan");
            hst.Add("id", id);
            int result_lotratedelete = objData.ExecuteNonQuery("[sp_CorporateLotPlanRate]", CommandType.StoredProcedure, hst);
            if (result_lotratedelete > 0)
            {
            }


        }
        catch
        {
        }
    }

    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    DataTable dt_charge=  new DataTable("ChargeByReserved");
    DataTable dt_charge_Random = new DataTable("ChargeByRandom");
    DataTable dt_planreserved = new DataTable();
    protected void btn_add_Click(object sender, EventArgs e)
    {
        //{
        //    if (txtbox_numberofparker.Text != "0" || txtbox_numberofparker.Text != "00" || txtbox_numberofparker.Text != "000"||txtbox_numberofparker.Text != "0000")
        //    {

        //        try
        //        {
        //            dt_charge = (DataTable)Session["Chargetable"];

        //            DataRow dr = dt_charge.NewRow();
        //            dr[0] = Convert.ToInt32(txtbox_numberofparker.Text);
        //            dr[1] = ddl_reservedtype.SelectedItem.Text;
        //            dr[2] = Convert.ToDecimal(lbl_total.Text);
        //            dr[3] = Convert.ToInt32(ddl_chargeby.SelectedItem.Value);
        //            dr[4] = ddl_reservedtype.SelectedItem.Text + ddl_chargeby.SelectedItem.Text + "of $" + lbl_charge.Text;
        //            dr[5] = txtBox_username.Text;

        //            dt_charge.Rows.Add(dr);
        //            Session["Chargetable"] = dt_charge;



        //            RemoveDuplicateRows(dt_charge, "Planid");
        //            //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

        //            if (dt_charge.Rows.Count > 0)
        //            {
        //                Repeater1.Visible = true;
        //                Repeater1.DataSource = dt_charge;
        //                Repeater1.DataBind();
        //                //ddl_reservedtype.Visible = false;
        //                //ddl_chargeby.Visible = false;
        //                //lbl_total.Visible = false;
        //                //lbl_text.Visible = false;
        //                //lbl_charge.Visible = false;
        //                //btn_add.Visible = false;
        //                txtbox_numberofparker.Text = "";
        //                // lbl_alltotal.Visible = true;


        //            }
        //            decimal grandtotal = Convert.ToDecimal(lbl_alltotal.Text) + Convert.ToDecimal(lbl_total.Text);
        //            lbl_alltotal.Text = Convert.ToString(grandtotal);
        //        }
        //        catch
        //        {
        //            //lbl_error.Visible = true;

        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Enter Number More Than 0')", true);
        //    }
    }
    
    protected void  btn_next_Click(object sender, EventArgs e)
{
    TabContainer1.Tabs[2].Enabled = true;
  
      
    TabContainer1.ActiveTabIndex = 2;
}

    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "remove")
            {
                Label l = (Label)e.Item.FindControl("Label2");

                int ID = Convert.ToInt32(e.CommandArgument.ToString());
                delete(ID);
                dt_charge_Random = (DataTable)Session["ChargetableRandom"];

                dt_charge_Random.Rows.RemoveAt(e.Item.ItemIndex);

                Repeater2.DataSource = dt_charge_Random;
                Repeater2.DataBind();

                Session["ChargetableRandom"] = dt_charge_Random;

            }
        }
        catch
        {
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        //Panel8.Visible = true;
        //TextBox1.Enabled = false;
      
    }
    protected void txtbox_totalrandomspace_TextChanged(object sender, EventArgs e)
    {

        //Panel9.Visible = true;
        //txtbox_totalrandomspace.Enabled = false;
    }
 
    //protected void ddl_chargeby_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    try
    //    {
    //        if (txtbox_numberofparker.Text.Trim() != "" && txtbox_numberofparker.Text != "0" && txtbox_numberofparker.Text != "00" && txtbox_numberofparker.Text != "000" && txtbox_numberofparker.Text != "0000")
    //        {
    //            if (ddl_chargeby.SelectedItem.Text != "Select")
    //            {
    //                int a = 0;
    //                DataTable dt = (DataTable)Session["Chargetable"];
    //                for (int j = 0; j < dt.Rows.Count; j++)
    //                {
    //                    if (Convert.ToString(ddl_chargeby.SelectedItem.Value) == dt.Rows[j][3].ToString())
    //                    {
    //                        a++;
    //                        break;

    //                    }

    //                }
    //                if (a > 0)
    //                {
    //                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //                    ddl_chargeby.Visible = false;
    //                    lbl_charge.Visible = false;
    //                    lbl_text.Visible = false;
    //                    lbl_total.Visible = false;
    //                    //  lbl_totaltext.Visible = false;
    //                    btn_add.Visible = false;

    //                }
    //                else
    //                {

    //                    DataSet chargetype = onj.fetchrec("select  * from tbl_LotRateMaster where LotRateid=" + ddl_chargeby.SelectedValue);
    //                    decimal total_charge = Convert.ToDecimal(chargetype.Tables[0].Rows[0][3]) * Convert.ToDecimal(txtbox_numberofparker.Text);


    //                    lbl_charge.Visible = true;
    //                    lbl_text.Visible = true;
    //                    lbl_total.Visible = true;
    //                    // lbl_totaltext.Visible = true;

    //                    lbl_total.Text = Convert.ToString(total_charge);


    //                    btn_add.Visible = true;
    //                    lbl_charge.Text = chargetype.Tables[0].Rows[0][3].ToString();
    //                }
    //            }
    //            else
    //            {
    //                lbl_charge.Visible = true;
    //                lbl_charge.Text = "Select Atleast One Plan";
    //            }
    //        }
    //        else
    //        {
    //            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Enter Number of parker )", true);

    //        }
    //    }
    //    catch
    //    {
    //    }
    //    //try
    //    //{
    //    //    if (ddl_chargeby.SelectedItem.Text != "Select")
    //    //    {
               
    //    //         DataSet chargetype = onj.fetchrec("select  * from tbl_LotRateMaster where LotRateid=" + ddl_chargeby.SelectedValue);
    //    //         decimal total_charge = Convert.ToDecimal(chargetype.Tables[0].Rows[0][3]) * Convert.ToDecimal(txtbox_numberofparker.Text);

    //    //         int a = 0;
    //    //         DataTable dt = (DataTable)Session["Chargetable"];
    //    //         for (int j = 0; j < dt.Rows.Count; j++)
    //    //         {
    //    //             if (ddl_chargeby.SelectedItem.Value == dt.Rows[j][3].ToString())
    //    //             {
    //    //                 a++;
    //    //                 break;

    //    //             }

    //    //         }
    //    //         if (a > 0)
    //    //         {
    //    //             ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //    //             ddl_chargeby.Visible = false;
    //    //             lbl_charge.Visible = false;
    //    //             lbl_text.Visible = false;
    //    //             lbl_total.Visible = false;
    //    //             btn_add.Visible = false;

    //    //         }
    //    //         else
    //    //         {

    //    //             lbl_charge.Visible = true;
    //    //             lbl_text.Visible = true;
    //    //             lbl_total.Visible = true;

    //    //             lbl_total.Text = Convert.ToString(total_charge);


    //    //             btn_add.Visible = true;
    //    //             lbl_charge.Text = chargetype.Tables[0].Rows[0][3].ToString();
    //    //         }
    //    //    }
    //    //    else
    //    //    {
    //    //        lbl_charge.Visible = true;
    //    //        lbl_charge.Text = "Select Atleast One Plan";
    //    //    }
    //    //}
    //    //catch
    //    //{
    //    //}
       
    //}

    clsInsert onj = new clsInsert();
    //public void bindcharge_reserved()
    //{
    //                ddl_chargeby.Items.Clear();
    //                ddl_chargeby.Visible = true;

    //                DataSet rec = onj.fetchrec("select LotRateid,Quantity,chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"]) + " and Parkingtype=1 and ISSpecial=0 and chargeby=3 " );
    //                DataTable t = new DataTable("chargeadd");
    //                t.Columns.Add("LotRateid", typeof(int));
    //                t.Columns.Add("Charge", typeof(string));

    //                for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
    //                {
    //                    DataRow dr = t.NewRow();
    //                    string charge = rec.Tables[0].Rows[i][1].ToString().Trim();
    //                    string[] strArr = charge.Split('.');

    //                    if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "3")
    //                    {
    //                        dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                        dr[1] = "For " + strArr[0] + " [Months]";
    //                        t.Rows.Add(dr);
    //                    }
                       

    //                }


    //                ddl_chargeby.DataSource = t;
    //                ddl_chargeby.DataTextField = "Charge";
    //                ddl_chargeby.DataValueField = "LotRateid";
    //                ddl_chargeby.DataBind();
    //            //}
            


            
    //        ddl_chargeby.Items.Insert(0, new ListItem("Select Charge", "0"));
    //}
    //public void bindcharge_random()
    //{
    //    ddl_chargebyrandom.Items.Clear();
    //    ddl_chargebyrandom.Visible = true;

    //    DataSet rec = onj.fetchrec("select LotRateid,Quantity,chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"]) + " and Parkingtype=2 and ISSpecial=0 and chargeby=3 ");
    //    DataTable t = new DataTable("chargeadd");
    //    t.Columns.Add("LotRateid", typeof(int));
    //    t.Columns.Add("Charge", typeof(string));

    //    for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
    //    {
    //        DataRow dr = t.NewRow();
    //        string charge = rec.Tables[0].Rows[i][1].ToString().Trim();
    //        string[] strArr = charge.Split('.');

    //        if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "3")
    //        {
    //            dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            dr[1] = "For " + strArr[0] + " [Months]";
    //            t.Rows.Add(dr);
    //        }


    //    }


    //    ddl_chargebyrandom.DataSource = t;
    //    ddl_chargebyrandom.DataTextField = "Charge";
    //    ddl_chargebyrandom.DataValueField = "LotRateid";
    //    ddl_chargebyrandom.DataBind();
    //    //}




    //    ddl_chargebyrandom.Items.Insert(0, new ListItem("Select Charge", "0"));
    //}
    //protected void ddl_reservedtype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {

    //       // ddl_chargeby.Items.Clear();
    //        if (ddl_reservedtype.SelectedItem.Text == "Select")
    //        {
    //            ddl_chargeby.Visible = false;
    //            lbl_charge.Visible = true;
    //            lbl_charge.Text = "Select atleast One Type";

    //        }
    //        else
    //        {
    //            lbl_charge.Visible = false;
    //            ddl_chargeby.Visible = true;
    //            //int a = 0;
    //            //DataTable dt = (DataTable)Session["Chargetable"];
    //            //for (int j = 0; j < dt.Rows.Count; j++)
    //            //{
    //            //    if (ddl_reservedtype.SelectedItem.Text == dt.Rows[j][1].ToString())
    //            //    {
    //            //        a++;
    //            //        break;

    //            //    }

    //            //}
    //            //if (a > 0)
    //            //{
    //            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //            //    ddl_chargeby.Visible = false;
    //            //    lbl_charge.Visible = false;
    //            //    lbl_text.Visible = false;
    //            //    lbl_total.Visible = false;
    //            //    btn_add.Visible = false;

    //            //}
    //            //else
    //            //{

    //            //        ddl_chargeby.Items.Clear();
    //            //        ddl_chargeby.Visible = true;

    //            //        DataSet rec = onj.fetchrec("select LotRateid,Quantity,chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"]) + " and Parkingtype=1 and ISSpecial=0 and chargeby= " + ddl_reservedtype.SelectedValue);
    //            //        DataTable t = new DataTable("chargeadd");
    //            //        t.Columns.Add("LotRateid", typeof(int));
    //            //        t.Columns.Add("Charge", typeof(string));

    //            //        for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
    //            //        {
    //            //            DataRow dr = t.NewRow();
    //            //            string charge = rec.Tables[0].Rows[i][1].ToString().Trim();
    //            //            string[] strArr = charge.Split('.');

    //            //            if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "1")
    //            //            {
    //            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //                dr[1] = "For " + strArr[0] + " [Hours]";
    //            //                t.Rows.Add(dr);
    //            //            }
    //            //            if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "2")
    //            //            {
    //            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //                dr[1] = "For " + strArr[0] + " [Days]";
    //            //                t.Rows.Add(dr);
    //            //            }
    //            //            if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "3")
    //            //            {
    //            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //                dr[1] = "For " + strArr[0] + " [Months]";
    //            //                t.Rows.Add(dr);
    //            //            }
    //            //            if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "4")
    //            //            {
    //            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //                dr[1] = "For " + strArr[0] + " [Weeks]";
    //            //                t.Rows.Add(dr);
    //            //            }

    //            //        }


    //            //        ddl_chargeby.DataSource = t;
    //            //        ddl_chargeby.DataTextField = "Charge";
    //            //        ddl_chargeby.DataValueField = "LotRateid";
    //            //        ddl_chargeby.DataBind();
    //            //    //}



    //            //}
    //            //ddl_chargeby.Items.Insert(0, new ListItem("Select Charge", "0"));
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}
    protected void txtbox_numberofparker_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    int total_parker = 0;
        //    for (int i = 0; i< Repeater1.Items.Count; i++)
        //    {

        //        Label Countparker = Repeater1.Items[i].FindControl("totalparker") as Label;
        //          int parker = Convert.ToInt32(Countparker.Text);
        //       total_parker = parker + total_parker;
              

        //    }
        //    if (Convert.ToInt32(txtbox_numberofparker.Text) > ((Convert.ToInt32(TextBox1.Text)) - total_parker))
        //    {
        //        txtbox_numberofparker.Text = "";
        //        lbl_check_noofparker.Visible = true;
        //        lbl_check_noofparker.ForeColor = Color.Red;
        //        lbl_check_noofparker.Text = "Exceed Than "+ TextBox1.Text;
        //    }
        //    else
        //    {
        //        lbl_check_noofparker.Visible = false;
        //        ddl_reservedtype.Visible = true;
        //    }
        //}
        //catch
        //{
        //}
    }

    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    int total_parker = 0;
        //    for (int i = 0; i < Repeater2.Items.Count; i++)
        //    {

        //        Label Countparker = Repeater2.Items[i].FindControl("totalparker") as Label;
        //        int parker = Convert.ToInt32(Countparker.Text);
        //        total_parker = parker + total_parker;


        //    }
        //    if (Convert.ToInt32(txtbox_random_numberofparker.Text) > ((Convert.ToInt32(txtbox_totalrandomspace.Text)) - total_parker))
        //    {
        //        txtbox_random_numberofparker.Text = "";
        //        lbl_check_noofparkerRandom.Visible = true;
        //        lbl_check_noofparkerRandom.ForeColor = Color.Red;
        //        lbl_check_noofparkerRandom.Text = "Exceed Than " + txtbox_totalrandomspace.Text;
        //    }
        //    else
        //    {
        //        lbl_check_noofparkerRandom.Visible = false;
        //        ddl_Randomparking.Visible = true;
        //    }
        //}
        //catch
        //{
        //}
    }
    //protected void ddl_Randomparking_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        // ddl_chargeby.Items.Clear();
    //        if (ddl_Randomparking.SelectedItem.Text == "Select")
    //        {
    //            ddl_chargebyrandom.Visible = false;
    //            lbl_randome_charge.Visible = true;
    //            lbl_randome_charge.Text = "Select atleast One Type";

    //        }
    //        else
    //        {
    //            lbl_randome_charge.Visible = false;

    //            ddl_chargebyrandom.Visible = true;
    //            // int a = 0;
    //            //    DataTable dt = (DataTable)Session["Chargetable"];
    //            //    //for (int j = 0; j < dt.Rows.Count; j++)
    //            //    //{
    //            //    //    if (ddl_reservedtype.SelectedItem.Text == dt.Rows[j][1].ToString())
    //            //    //    {
    //            //    //        a++;
    //            //    //        break;

    //            //    //    }

    //            //    //}
    //            //    //if (a > 0)
    //            //    //{
    //            //    //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //            //    //    ddl_chargeby.Visible = false;
    //            //    //    lbl_charge.Visible = false;
    //            //    //    lbl_text.Visible = false;
    //            //    //    lbl_total.Visible = false;
    //            //    //    btn_add.Visible = false;

    //            //    //}
    //            //    //else
    //            //    //{

    //            //    ddl_chargebyrandom.Items.Clear();
    //            //    ddl_chargebyrandom.Visible = true;

    //            //    DataSet rec = onj.fetchrec("select LotRateid,Quantity,chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"]) + " and Parkingtype=2 and ISSpecial=0 and chargeby= " + ddl_Randomparking.SelectedValue);
    //            //    DataTable t = new DataTable("chargeadd");
    //            //    t.Columns.Add("LotRateid", typeof(int));
    //            //    t.Columns.Add("Charge", typeof(string));

    //            //    for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
    //            //    {
    //            //        DataRow dr = t.NewRow();
    //            //        string charge = rec.Tables[0].Rows[i][1].ToString().Trim();
    //            //        string[] strArr = charge.Split('.');

    //            //        if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "1")
    //            //        {
    //            //            dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //            dr[1] = "For " + strArr[0] + " [Hours]";
    //            //            t.Rows.Add(dr);
    //            //        }
    //            //        if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "2")
    //            //        {
    //            //            dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //            dr[1] = "For " + strArr[0] + " [Days]";
    //            //            t.Rows.Add(dr);
    //            //        }
    //            //        if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "3")
    //            //        {
    //            //            dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //            dr[1] = "For " + strArr[0] + " [Months]";
    //            //            t.Rows.Add(dr);
    //            //        }
    //            //        if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "4")
    //            //        {
    //            //            dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //            //            dr[1] = "For " + strArr[0] + " [Weeks]";
    //            //            t.Rows.Add(dr);
    //            //        }

    //            //    }


    //            //    ddl_chargebyrandom.DataSource = t;
    //            //    ddl_chargebyrandom.DataTextField = "Charge";
    //            //    ddl_chargebyrandom.DataValueField = "LotRateid";
    //            //    ddl_chargebyrandom.DataBind();
    //            //}



    //            ////}
    //            //ddl_chargebyrandom.Items.Insert(0, new ListItem("Select Charge", "0"));
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}
    protected void txtbox_random_numberofparker_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    int total_parker = 0;
        //    for (int i = 0; i < Repeater2.Items.Count; i++)
        //    {

        //        Label Countparker = Repeater2.Items[i].FindControl("totalparker") as Label;
        //        int parker = Convert.ToInt32(Countparker.Text);
        //        total_parker = parker + total_parker;


        //    }
        //    if (Convert.ToInt32(txtbox_random_numberofparker.Text) > ((Convert.ToInt32(txtbox_totalrandomspace.Text)) - total_parker))
        //    {
        //        txtbox_random_numberofparker.Text = "";
        //        lbl_check_noofparkerRandom.Visible = true;
        //        lbl_check_noofparkerRandom.ForeColor = Color.Red;
        //        lbl_check_noofparkerRandom.Text = "Exceed Than " + txtbox_totalrandomspace.Text;
        //    }
        //    else
        //    {
        //        lbl_check_noofparkerRandom.Visible = false;
        //        ddl_Randomparking.Visible = true;
        //    }
        //}
        //catch
        //{
        //}
    }
    //protected void ddl_chargebyrandom_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (ddl_chargebyrandom.SelectedItem.Text != "Select")
    //        {
    //            int a = 0;
    //            DataTable dt = (DataTable)Session["ChargetableRandom"];
    //            for (int j = 0; j < dt.Rows.Count; j++)
    //            {
    //                if (Convert.ToString(ddl_chargebyrandom.SelectedItem.Value) == dt.Rows[j][3].ToString())
    //                {
    //                    a++;
    //                    break;

    //                }

    //            }
    //            if (a > 0)
    //            {
    //                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //                ddl_chargebyrandom.Visible = false;
    //                lbl_randome_charge.Visible = false;
    //                lbl_randome_text.Visible = false;
    //                lbl_randome_total.Visible = false;
    //               // lbl_random_totaltext.Visible = false;
    //                Add_random.Visible = false;

    //            }
    //            else
    //            {

    //                DataSet chargetype = onj.fetchrec("select  * from tbl_LotRateMaster where LotRateid=" + ddl_chargebyrandom.SelectedValue);
    //                decimal total_charge = Convert.ToDecimal(chargetype.Tables[0].Rows[0][3]) * Convert.ToDecimal(txtbox_random_numberofparker.Text);


    //                lbl_randome_charge.Visible = true;
    //                lbl_randome_text.Visible = true;
    //                lbl_randome_total.Visible = true;
    //             //   lbl_random_totaltext.Visible = true;

    //                lbl_randome_total.Text = Convert.ToString(total_charge);


    //                Add_random.Visible = true;
    //                lbl_randome_charge.Text = chargetype.Tables[0].Rows[0][3].ToString();
    //            }
    //        }
    //        else
    //        {
    //            lbl_randome_charge.Visible = true;
    //            lbl_randome_charge.Text = "Select Atleast One Plan";
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}
    //protected void Add_random_Click(object sender, EventArgs e)
    //{
    //    if (txtbox_random_numberofparker.Text != "0" || txtbox_random_numberofparker.Text != "00" || txtbox_random_numberofparker.Text != "000" || txtbox_random_numberofparker.Text != "0000")
    //    {
    //        try
    //        {
    //            dt_charge_Random = (DataTable)Session["ChargetableRandom"];

    //            DataRow dr = dt_charge_Random.NewRow();
    //            dr[0] = Convert.ToInt32(txtbox_random_numberofparker.Text);
    //            dr[1] = ddl_Randomparking.SelectedItem.Text;
    //            dr[2] = Convert.ToDecimal(lbl_randome_total.Text);
    //            dr[3] = Convert.ToInt32(ddl_chargebyrandom.SelectedItem.Value);
    //            dr[4] = ddl_Randomparking.SelectedItem.Text + ddl_chargebyrandom.SelectedItem.Text + "of $" + lbl_randome_charge.Text;
    //            dr[5] = txtBox_username.Text;

    //            dt_charge_Random.Rows.Add(dr);
    //            Session["ChargetableRandom"] = dt_charge_Random;
    //            RemoveDuplicateRows(dt_charge_Random, "Planid");
    //            //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

    //            if (dt_charge_Random.Rows.Count > 0)
    //            {
    //                Repeater2.Visible = true;
    //                Repeater2.DataSource = dt_charge_Random;
    //                Repeater2.DataBind();
    //                //ddl_Randomparking.Visible = false;
    //                //ddl_chargebyrandom.Visible = false;
    //                //lbl_randome_total.Visible = false;
    //                //lbl_randome_text.Visible = false;
    //                //lbl_randome_charge.Visible = false;
    //                //  lbl_random_totaltext.Visible = false;
    //                Add_random.Visible = false;
    //                txtbox_random_numberofparker.Text = "";
    //                // lbl_alltotal.Visible = true;


    //            }
    //            else
    //            {
    //                Repeater2.Visible = false;
    //            }
    //            decimal grandtotal = Convert.ToDecimal(lbl_alltotalRandome.Text) + Convert.ToDecimal(lbl_randome_total.Text);
    //            lbl_alltotalRandome.Text = Convert.ToString(grandtotal);
    //        }
    //        catch
    //        {
    //            //lbl_error.Visible = true;

    //        }
    //        //try
    //        //{
    //        //    dt_charge_Random = (DataTable)Session["ChargetableRandom"];

    //        //    DataRow dr = dt_charge_Random.NewRow();
    //        //    dr[0] = Convert.ToInt32(txtbox_random_numberofparker.Text);
    //        //    dr[1] = ddl_Randomparking.SelectedItem.Text;
    //        //    dr[2] = Convert.ToDecimal(lbl_randome_total.Text);
    //        //    dr[3] = Convert.ToInt32(ddl_Randomparking.SelectedItem.Value);

    //        //    dt_charge_Random.Rows.Add(dr);
    //        //    Session["ChargetableRandom"] = dt_charge_Random;
    //        //    RemoveDuplicateRows(dt_charge_Random, "Planid");
    //        //    //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

    //        //    if (dt_charge_Random.Rows.Count > 0)
    //        //    {
    //        //        Repeater2.Visible = true;
    //        //        Repeater2.DataSource = dt_charge_Random;
    //        //        Repeater2.DataBind();
    //        //        ddl_Randomparking.Visible = false;
    //        //        ddl_chargebyrandom.Visible = false;
    //        //        lbl_randome_total.Visible = false;
    //        //        lbl_randome_text.Visible = false;
    //        //        lbl_randome_charge.Visible = false;
    //        //        Add_random.Visible = false;
    //        //        txtbox_random_numberofparker.Text = "";
    //        //        // lbl_alltotal.Visible = true;


    //        //    }
    //        //    decimal grandtotal = Convert.ToDecimal(lbl_alltotalRandome.Text) + Convert.ToDecimal(lbl_randome_total.Text);
    //        //    lbl_alltotalRandome.Text = Convert.ToString(grandtotal);
    //        //}
    //        //catch
    //        //{
    //        //    //lbl_error.Visible = true;

    //        //}
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Enter Number More Than 0')", true);

    //    }
    //}

  
    protected void txtBox_Passwrd_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Session["password"] = txtBox_Passwrd.Text;
        }
        catch
        {
        }
    }
    protected void rbtn_filldetail_CheckedChanged(object sender, EventArgs e)
    {
      //  pfill_skip.Visible = false;
        //panel_reserved.Visible = true;
        //DataTable reserved = (DataTable)Session["Chargetable"];

        //rpt_reserved.DataSource = reserved;
        //rpt_reserved.DataBind();

    }
    decimal defaultvalueforinsert = Convert.ToDecimal("0.00");
    protected void rbtn_skip_CheckedChanged(object sender, EventArgs e)
    {
        Response.Redirect("Admin_Registration_Of_Corporate.aspx");
    }

    protected void add_reserved_Click(object sender, EventArgs e)
    {
        if (txtbox_reserved_planname.Text.Trim() != "")
        {

            try
            {
                hst.Clear();
                hst.Add("action", "insertrateplan");
                hst.Add("Lotid", Convert.ToInt32(Session["lotid"].ToString()));
                if (txtbox_reserved_monthly.Text.Trim() != "")
                {
                    hst.Add("monthlyrate", Convert.ToDecimal(txtbox_reserved_monthly.Text));
                }
                else
                {
                    hst.Add("monthlyrate", Convert.ToDecimal(defaultvalueforinsert));

                }
                if (txtbox_reserved_activation.Text.Trim() != "")
                {
                    hst.Add("activation", Convert.ToDecimal(txtbox_reserved_activation.Text));

                }
                else
                {
                    hst.Add("activation", Convert.ToDecimal(defaultvalueforinsert));


                }
                if (txtbox_reserved_delivery.Text.Trim() != "")
                {
                    hst.Add("delivery", Convert.ToDecimal(txtbox_reserved_delivery.Text));


                }
                else
                {
                    hst.Add("delivery", Convert.ToDecimal(defaultvalueforinsert));

                }
                hst.Add("parkingtype", Convert.ToInt32(1));
                hst.Add("corporateusername", txtBox_username.Text);
                hst.Add("planname", txtbox_reserved_planname.Text);
                int result_lotratemanage = objData.ExecuteNonQuery("[sp_CorporateLotPlanRate]", CommandType.StoredProcedure, hst);
                if (result_lotratemanage > 0)
                {
                    DataSet idrecently = new clsInsert().fetchrec("select PlanID_Corporate from tbl_CorporateLotRatePlan where ParkingType=1 and PlanName='"+txtbox_reserved_planname.Text+"' and Corporateusername='"+txtBox_username.Text+"'");
                    if (idrecently.Tables[0].Rows.Count > 0)
                    {
                        idrecently_reserved = Convert.ToInt32(idrecently.Tables[0].Rows[0]["PlanID_Corporate"].ToString());
                    }
                }

            }
            catch
            {

            }
            
                
                //dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                //dt_charge_Random.Columns.Add("monthlyrate", typeof(decimal));
                //dt_charge_Random.Columns.Add("activationfess", typeof(decimal));
                //dt_charge_Random.Columns.Add("deposit", typeof(decimal));
                //dt_charge_Random.Columns.Add("deliveryfees", typeof(decimal));
                //dt_charge_Random.Columns.Add("planname", typeof(string));
                //dt_charge_Random.Columns.Add("Username", typeof(string));
                //dt_charge_Random.Columns.Add("PlanID", typeof(int));
            try
            {
                dt_charge = (DataTable)Session["Chargetable"];

                DataRow dr = dt_charge.NewRow();

                if (txtbox_reserved_monthly.Text.Trim() != "")
                {
                    //hst.Add("monthlyrate", Convert.ToDecimal(txtbox_reserved_monthly.Text));
                    dr[1] = Convert.ToDecimal(txtbox_reserved_monthly.Text);

                }
                else
                {
                   // hst.Add("monthlyrate", Convert.ToDecimal(defaultvalueforinsert));
                    dr[1] = Convert.ToDecimal(defaultvalueforinsert);

                }
                if (txtbox_reserved_activation.Text.Trim() != "")
                {
                   // hst.Add("activation", Convert.ToDecimal(txtbox_reserved_activation.Text));
                    dr[2] = Convert.ToDecimal(txtbox_reserved_activation.Text);

                }
                else
                {
                    //hst.Add("activation", Convert.ToDecimal(defaultvalueforinsert));

                    dr[2] = Convert.ToDecimal(defaultvalueforinsert);

                }
                if (txtbox_reserved_delivery.Text.Trim() != "")
                {
                   // hst.Add("delivery", Convert.ToDecimal(txtbox_reserved_delivery.Text));
                    dr[4] = Convert.ToDecimal(txtbox_reserved_delivery.Text);


                }
                else
                {
                    //hst.Add("delivery", Convert.ToDecimal(defaultvalueforinsert));
                    dr[4] = Convert.ToDecimal(defaultvalueforinsert);


                }

                if (txtbox_reserved_deposite.Text.Trim() != "")
                {
                    // hst.Add("delivery", Convert.ToDecimal(txtbox_reserved_delivery.Text));
                    dr[3] = Convert.ToDecimal(txtbox_reserved_deposite.Text);


                }
                else
                {
                    //hst.Add("delivery", Convert.ToDecimal(defaultvalueforinsert));
                    dr[3] = Convert.ToDecimal(defaultvalueforinsert);


                }
                dr[0] = Convert.ToInt32(txtbox_space_reserved.Text);
              //  dr[3] = Convert.ToDecimal(txtbox_reserved_deposite.Text);
                dr[5] = txtbox_reserved_planname.Text;
                dr[6] = txtBox_username.Text;
                dr[7] = idrecently_reserved;

                dt_charge.Rows.Add(dr);
                Session["Chargetable"] = dt_charge;
                // RemoveDuplicateRows(dt_charge, "Planid");
                //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

                if (dt_charge.Rows.Count > 0)
                {
                    Panel6.Visible = true;
                    Repeater1.Visible = true;
                    Repeater1.DataSource = dt_charge;
                    Repeater1.DataBind();
                }

              
            }
            catch
            {
            }
            txtbox_reserved_activation.Text = "";
            txtbox_reserved_delivery.Text = "";
            txtbox_reserved_deposite.Text = "";
            txtbox_reserved_monthly.Text = "";
            txtbox_space_reserved.Text = "";
            txtbox_reserved_planname.Text = "";
        }
        else
        {
               ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Enter Plan Name')", true);

        }

    }
    protected void btn_add_random_Click(object sender, EventArgs e)
    {

        if (txtbox_randomplanname.Text.Trim() != "")
        {


            try
            {
                hst.Clear();
                hst.Add("action", "insertrateplan");
                hst.Add("Lotid", Convert.ToInt32(Session["lotid"].ToString()));
                if (txtbox_random_monthly.Text.Trim() != "")
                {
                    hst.Add("monthlyrate", Convert.ToDecimal(txtbox_random_monthly.Text));
                }
                else
                {
                    hst.Add("monthlyrate", Convert.ToDecimal(defaultvalueforinsert));

                }
                if (txtbox_random_activation.Text.Trim() != "")
                {
                    hst.Add("activation", Convert.ToDecimal(txtbox_random_activation.Text));

                }
                else
                {
                    hst.Add("activation", Convert.ToDecimal(defaultvalueforinsert));


                }
                if (txtbox_random_delivery.Text.Trim() != "")
                {
                    hst.Add("delivery", Convert.ToDecimal(txtbox_random_delivery.Text));


                }
                else
                {
                    hst.Add("delivery", Convert.ToDecimal(defaultvalueforinsert));

                }
                hst.Add("parkingtype", Convert.ToInt32(2));
                hst.Add("corporateusername", txtBox_username.Text);
                hst.Add("planname", txtbox_randomplanname.Text);
                int result_lotratemanage = objData.ExecuteNonQuery("[sp_CorporateLotPlanRate]", CommandType.StoredProcedure, hst);
                if (result_lotratemanage > 0)
                {
                    DataSet idrecently = new clsInsert().fetchrec("select PlanID_Corporate from tbl_CorporateLotRatePlan where ParkingType=2 and PlanName='" + txtbox_randomplanname.Text + "' and Corporateusername='" + txtBox_username.Text + "'");
                    if (idrecently.Tables[0].Rows.Count > 0)
                    {
                        idrecently_random = Convert.ToInt32(idrecently.Tables[0].Rows[0]["PlanID_Corporate"].ToString());
                    }
                }

            }
            catch
            {

            }


            try
            {

                dt_charge_Random = (DataTable)Session["ChargetableRandom"];

                DataRow dr = dt_charge_Random.NewRow();

                if (txtbox_random_monthly.Text.Trim() != "")
                {
                    //hst.Add("monthlyrate", Convert.ToDecimal(txtbox_random_monthly.Text));
                    dr[1] = Convert.ToDecimal(txtbox_random_monthly.Text);

                }
                else
                {
                   // hst.Add("monthlyrate", Convert.ToDecimal(defaultvalueforinsert));
                    dr[1] = Convert.ToDecimal(defaultvalueforinsert);

                }
                if (txtbox_random_activation.Text.Trim() != "")
                {
                   // hst.Add("activation", Convert.ToDecimal(txtbox_random_activation.Text));
                    dr[2] = Convert.ToDecimal(txtbox_random_activation.Text);

                }
                else
                {
                  //  hst.Add("activation", Convert.ToDecimal(defaultvalueforinsert));
                    dr[2] = Convert.ToDecimal(defaultvalueforinsert);


                }
                if (txtbox_random_delivery.Text.Trim() != "")
                {
                   // hst.Add("delivery", Convert.ToDecimal(txtbox_random_delivery.Text));

                    dr[4] = Convert.ToDecimal(txtbox_random_delivery.Text);

                }
                else
                {
                   // hst.Add("delivery", Convert.ToDecimal(defaultvalueforinsert));
                    dr[4] = Convert.ToDecimal(defaultvalueforinsert);

                }

                if (txtbox_random_deposite.Text.Trim() != "")
                {
                    // hst.Add("delivery", Convert.ToDecimal(txtbox_random_delivery.Text));

                    dr[3] = Convert.ToDecimal(txtbox_random_deposite.Text);

                }
                else
                {
                    // hst.Add("delivery", Convert.ToDecimal(defaultvalueforinsert));
                    dr[3] = Convert.ToDecimal(defaultvalueforinsert);

                }
                dr[0] = Convert.ToInt32(txtbox_space_random.Text);
               // dr[3] = Convert.ToDecimal(txtbox_random_deposite.Text);
                dr[5] = txtbox_randomplanname.Text;
                dr[6] = txtBox_username.Text;
                dr[7] = idrecently_random;

                dt_charge_Random.Rows.Add(dr);
                Session["ChargetableRandom"] = dt_charge_Random;
                // RemoveDuplicateRows(dt_charge, "Planid");
                //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

                if (dt_charge_Random.Rows.Count > 0)
                {
                    Panel7.Visible = true;
                    Repeater2.Visible = true;
                    Repeater2.DataSource = dt_charge_Random;
                    Repeater2.DataBind();
                }
               
            }
            catch
            {
            }
            txtbox_random_activation.Text = "";
            txtbox_random_delivery.Text = "";
            txtbox_random_deposite.Text = "";
            txtbox_random_monthly.Text = "";
            txtbox_space_random.Text = "";
            txtbox_randomplanname.Text = "";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Enter Plan Name')", true);

        }
    }
    protected void txtbox_randomplanname_TextChanged(object sender, EventArgs e)
  {
        try
        {
            DataSet dschek = new clsInsert().fetchrec("select PlanName from tbl_CorporateLotRatePlan where ParkingType=2 and PlanName='" + txtbox_randomplanname.Text + "'and Corporateusername='" + txtBox_username.Text + "' ");
            if (dschek.Tables[0].Rows.Count > 0)
            {
                txtbox_randomplanname.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Already Exist exist.')", true);

            }
            else
            {
                txtbox_randomplanname.Text = txtbox_randomplanname.Text;
            }
        }
        catch
        {
        }
    }
    
    protected void txtbox_reserved_planname_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataSet dschek = new clsInsert().fetchrec("select PlanName from tbl_CorporateLotRatePlan where ParkingType=1 and PlanName='" + txtbox_reserved_planname.Text + "'and Corporateusername='" + txtBox_username.Text + "' ");
            if (dschek.Tables[0].Rows.Count > 0)
            {
                txtbox_reserved_planname.Text = "";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Already Exist exist.')", true);

            }
            else
            {
                txtbox_reserved_planname.Text = txtbox_reserved_planname.Text;
            }
        }
        catch
        {
        }
    }
    DataTable show_reserved = new DataTable();
    DataTable show_random =new DataTable();
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {

            //if (txtbox_random_numberofparker.Text.Trim() == "" && TextBox1.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Reserved Space OR Random Space.')", true);

            //}
            //else
            //{
            //    //for (int p = 0; p < Repeater1.Items.Count; p++)
            //{
            //    Label lbl = Repeater1.Items[p].FindControl("totalparker") as Label;
            //    total_reserved_parker = total_reserved_parker + Convert.ToInt32(lbl.Text);
            //}


            //if (total_reserved_parker == Convert.ToInt32(TextBox1.Text) )
            //{
            if (DropDownList1.SelectedValue == "2" || DropDownList1.SelectedValue == "1")
            {

                //bool b = objData.mail(txtbox_Email.Text, "User Name :" + txtBox_username.Text + "\nPassword :" + Session["password"].ToString(), "Successfully Registerd on ipass" + System.DateTime.Now.ToString());
                //if (b == true)
                //{
                    try
                    {

                        insertrec_corporate();
                    }
                    catch
                    {
                    }
                    try
                    {
                        if (DropDownList1.SelectedValue == "1")
                        {
                            insertrec_corporate_billingdetails();
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        insertrec_corporate_spacedetails();
                    }
                    catch
                    {

                    }


                    // Response.Redirect("Admin_Registration_Of_Corporate.aspx");
                    show_reserved = (DataTable)Session["Chargetable"];
                    show_random = (DataTable)Session["ChargetableRandom"];
                    if (show_reserved.Rows.Count > 0)
                    {
                        reserved.Visible = true;
                        resrevd.Visible = true;
                        resrevd.DataSource = show_reserved;
                        resrevd.DataBind();
                    }
                    else
                    {
                        reserved.Visible = false;
                        resrevd.Visible = false;
                    }

                    if (show_random.Rows.Count > 0)
                    {
                        random.Visible = true;
                        randm.Visible = true;
                        randm.DataSource = show_random;
                        randm.DataBind();
                    }

                    else
                    {

                        random.Visible = false;
                        randm.Visible = false;
                    }
                    TabContainer1.Tabs[0].Visible = false;
                    TabContainer1.Tabs[1].Visible = false;
                    TabContainer1.Tabs[2].Visible = false;
                    TabContainer1.Tabs[3].Visible = true;
                    TabContainer1.Tabs[4].Visible = true;
                    TabContainer1.Tabs[4].Enabled = false;
                    TabContainer1.ActiveTabIndex = 3;

                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Email Id not exist.')", true);

                //}





            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select Check Counter As Full Charge for now ')", true);

            }

            //    }
        }
        catch
        {


        }
    }
    int number_totalreseved = 0;
    int number_totalrandom = 0;
    int regis_reserved = 0;
    int regis_random = 0;

    protected void ddl_plantype_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if(ddl_plantype.SelectedItem.Value=="1")
            {

                //DataSet ds_bind_plan = new clsInsert().fetchrec("select * from tbl_CorporateLotRatePlan where ParkingType=1 and Corporateusername='" + txtBox_username.Text + "'order by MonthlyRate ");
                //if (ds_bind_plan.Tables[0].Rows.Count > 0)
                //{
                //    lbl_plan.Visible = true;
                //    ddl_plan.Visible = true;
                //    ddl_plan.Items.Clear();
                //    for (int i = 0; i < ds_bind_plan.Tables[0].Rows.Count; i++)
                //    {

                //        ListItem l = new ListItem(ds_bind_plan.Tables[0].Rows[i][7].ToString() + " [Monthly Charge $" + ds_bind_plan.Tables[0].Rows[i][2].ToString() + "]", ds_bind_plan.Tables[0].Rows[i][0].ToString());
                //        // ddl_deliverymethod.Items.Add(l);
                //        ddl_plan.Items.Add(l);
                //    }
                //    ddl_plan.Items.Insert(0, new ListItem("Select Plan Name", "0"));
                //    //ddl_plan.Items = ds_bind_plan;
                //}
                //else
                //{
                //    lbl_plan.Visible = false;
                //    ddl_plan.Visible = false;
                //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Plan For Reserved Parking')", true);

                //}
             
                DataSet plan = new clsInsert().fetchrec("select tbl_CorporateReservedParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateReservedParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateReservedParkingDetails.Planid where ParkingType=1  and tbl_CorporateLotRatePlan.Corporateusername='"+txtBox_username.Text+"' order by tbl_CorporateLotRatePlan.MonthlyRate ");

                for (int l = 0; l < plan.Tables[0].Rows.Count; l++)
                {
                    total_reserved_parker = total_reserved_parker + Convert.ToInt32(plan.Tables[0].Rows[l][1].ToString());
                    for (int r = l; r <= l; r++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan.Tables[0].Rows[l]["Planid"].ToString()));
                        regis_reserved = regis_reserved + Convert.ToInt32(registration.Tables[0].Rows[0][0].ToString());
                    }
                }
                int exist = total_reserved_parker - regis_reserved;
                if (exist != 0)
                {

                    res.Columns.Add("planid", typeof(int));

                    res.Columns.Add("total_p", typeof(int));
                    res.Columns.Add("regi", typeof(int));


                    DataSet total_plan = new clsInsert().fetchrec("select tbl_CorporateReservedParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateReservedParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateReservedParkingDetails.Planid where ParkingType=1  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");


                    //  res = (DataTable)Session["res"];
                    for (int i = 0; i < total_plan.Tables[0].Rows.Count; i++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString()));
                        DataRow dr = res.NewRow();
                        dr[0] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString());
                        dr[1] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Numberofparker"].ToString());
                        dr[2] = Convert.ToDecimal(registration.Tables[0].Rows[0][0].ToString());

                        res.Rows.Add(dr);
                    }
                    //Session["res"] = res;

                    for (int j = 0; j < res.Rows.Count; j++)
                    {
                        if (Convert.ToInt32(res.Rows[j][1].ToString()) != Convert.ToInt32(res.Rows[j][2].ToString()))
                        {
                            lbl_plan.Text = res.Rows[j][0].ToString();
                            break;
                        }
                    }


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Registration Is Completed In Reserved Parking')", true);
                    ddl_plantype.SelectedValue = "0";

                }





            }

            if (ddl_plantype.SelectedItem.Value == "2")
            {

                //DataSet ds_bind_plan = new clsInsert().fetchrec("select * from tbl_CorporateLotRatePlan where ParkingType=2 and Corporateusername='" + txtBox_username.Text + "'");
                //if (ds_bind_plan.Tables[0].Rows.Count > 0)
                //{
                //    lbl_plan.Visible = true;
                //    ddl_plan.Visible = true;
                //    ddl_plan.Items.Clear();
                //    for (int i = 0; i < ds_bind_plan.Tables[0].Rows.Count; i++)
                //    {

                //        ListItem l = new ListItem(ds_bind_plan.Tables[0].Rows[i][7].ToString() + " [Monthly Charge $" + ds_bind_plan.Tables[0].Rows[i][2].ToString() + "]", ds_bind_plan.Tables[0].Rows[i][0].ToString());
                //        // ddl_deliverymethod.Items.Add(l);
                //        ddl_plan.Items.Add(l);
                //    }
                //    ddl_plan.Items.Insert(0, new ListItem("Select Plan Name", "0"));

                //}
                //else
                //{
                //    lbl_plan.Visible = false;
                //    ddl_plan.Visible = false;
                //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Plan For Random Parking')", true);

                //}

                DataSet plan = new clsInsert().fetchrec("select tbl_CorporateRandomParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateRandomParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateRandomParkingDetails.Planid where ParkingType=2  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");

                for (int l = 0; l < plan.Tables[0].Rows.Count; l++)
                {
                    number_totalrandom = number_totalrandom + Convert.ToInt32(plan.Tables[0].Rows[l][1].ToString());
                    for (int r = l; r <= l; r++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan.Tables[0].Rows[l]["Planid"].ToString()));
                        regis_random = regis_random + Convert.ToInt32(registration.Tables[0].Rows[0][0].ToString());
                    }
                }
                int exist = number_totalrandom - regis_random;
                if (exist != 0)
                {
                res.Columns.Add("planid", typeof(int));

                res.Columns.Add("total_p", typeof(int));
                res.Columns.Add("regi", typeof(int));


                DataSet total_plan = new clsInsert().fetchrec("select tbl_CorporateRandomParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateRandomParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateRandomParkingDetails.Planid where ParkingType=2  and tbl_CorporateLotRatePlan.Corporateusername='"+txtBox_username.Text+"' order by tbl_CorporateLotRatePlan.MonthlyRate ");


                //  res = (DataTable)Session["res"];
                for (int i = 0; i < total_plan.Tables[0].Rows.Count; i++)
                {
                    DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString()));
                    DataRow dr = res.NewRow();
                    dr[0] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString());
                    dr[1] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Numberofparker"].ToString());
                    dr[2] = Convert.ToDecimal(registration.Tables[0].Rows[0][0].ToString());

                    res.Rows.Add(dr);
                }
                //Session["res"] = res;

                for (int j = 0; j < res.Rows.Count; j++)
                {
                    if (Convert.ToInt32(res.Rows[j][1].ToString()) != Convert.ToInt32(res.Rows[j][2].ToString()))
                    {
                        lbl_plan.Text = res.Rows[j][0].ToString();
                        break;
                    }
                }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Registration Is Completed In Random Parking')", true);
                    ddl_plantype.SelectedValue = "0";

                }
            }
            if (ddl_plantype.SelectedItem.Value == "0")
            {

                lbl_plan.Visible = false;
                ddl_plan.Visible = false;
               ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('PLease Select Plan')", true);

               
            }
            
        }
        catch
        {
        }
    }
    int Planid = 0;
    public void insertrec_parkerdetail()
    {

        try
        {
           // Planid = Convert.ToInt32(ddl_plan.SelectedValue);

            Planid = Convert.ToInt32(lbl_plan.Text);
        }
        catch
        {
        }


        try
        {
            hst.Clear();
           // DataSet   dscorporationid = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='" + txtBox_username.Text + "'");
            //@CorporationID,@planrateID,@emailid,@fname,@address1,@cell,@EmployeeCode,@dateofregis)
            hst.Add("action", "insert");
            hst.Add("CorporationID", txtBox_username.Text);
            // hst.Add("CorporationID", Convert.ToInt32(5));

            hst.Add("planrateID", Convert.ToInt32(Planid));
            hst.Add("emailid", txtBox_emp_emailid.Text);

            hst.Add("fname", txtbox_emp_firstname.Text);
           

            hst.Add("address1", txtBox_emp_address1.Text);
           

          
            hst.Add("cell", txtbox_emp_cell.Text);
            hst.Add("EmployeeCode", txtboxcode_employeecode.Text);
            hst.Add("dateofregis",DateTime.Now);






            int result = objData.ExecuteNonQuery("[Sp_employeeDetailsCorporate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
            

                int i = 1;

                if (i >= 1)
                {

                    //TabContainer1.Tabs[1].Enabled = true;
                   // DataSet code = new clsInsert().fetchrec("select EmployeeCode,Corporate_EmployeeID from tbl_CorporateEmployeeDetails where CorporationID=" + Convert.ToInt32(5) + "and PlanRateID=" + Planid + "order by Corporate_EmployeeID DESC");

                   DataSet code = new clsInsert().fetchrec("select EmployeeCode,Corporate_EmployeeID from tbl_CorporateEmployeeDetails where CorporationID='" + txtBox_username.Text +"' and PlanRateID=" + Planid + "order by Corporate_EmployeeID DESC");
                    if (code.Tables[0].Rows.Count > 0)
                    {
                        //ddl_emp_code.DataSource = code.Tables[0];
                        //ddl_emp_code.DataTextField = "EmployeeCode";
                        //ddl_emp_code.DataValueField = "Corporate_EmployeeID";
                        //ddl_emp_code.DataBind();
                        Session["Parker_id"] = code.Tables[0].Rows[0]["Corporate_EmployeeID"].ToString();
                    }
                    

                }

            }
        }
        catch
        {

        }





    }
    protected void txtboxcode_employeecode_TextChanged(object sender, EventArgs e)
    {

        try
        {
            DataSet mainhead = new clsInsert().fetchrec("select UserName from tbl_ParkerRegis where UserName='" +txtBox_username.Text + "'");
            DataSet codereturn = new clsInsert().fetchrec("select EmployeeCode from tbl_CorporateEmployeeDetails where  CorporationID='"+mainhead.Tables[0].Rows[0][0].ToString()+ "'and EmployeeCode='" + txtboxcode_employeecode.Text + "'");
            if (codereturn.Tables[0].Rows.Count > 0)
            {
                lblerror.Visible = true;
                lblerror.Text = "Already Exist... Try Another";
                txtboxcode_employeecode.Text = "";
            }
            else
            {
                lblerror.Visible = false;
                lblerror.Text = "";
            }
        }
        catch
        {
        }
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            //if (lbl_number.Text != "0")
            //{
            if (ddl_plantype.SelectedValue != "0")
            {
                if (lblerror.Visible == false)
                {
                    insertrec_parkerdetail();
                    ddl_plan.SelectedValue = "0";
                    ddl_plantype.SelectedValue = "0";
                    // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);

                    empshow = (DataTable)Session["empshow"];
                    DataRow dr = empshow.NewRow();
                    dr[0] = txtboxcode_employeecode.Text;
                    dr[1] = txtbox_emp_firstname.Text;
                    dr[2] = txtBox_emp_emailid.Text;

                    empshow.Rows.Add(dr);
                    rpt_emp.Visible = true;
                    rpt_emp.DataSource = empshow;
                    rpt_emp.DataBind();

                    TabContainer1.Tabs[4].Enabled = true;
                    TabContainer1.ActiveTabIndex = 4;
                    TabContainer1.Tabs[3].Enabled = false;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Fill Correct Employee Code.')", true);

                }
                txtBox_emp_address1.Text = "";
                // txtBox_emp_address2.Text = "";
                txtbox_emp_cell.Text = "";
                txtBox_emp_emailid.Text = "";
                txtbox_emp_firstname.Text = "";
                // txtbox_emp_lastname.Text = "";
                //  txtbox_emp_middlename.Text = "";
                // txtbox_emp_phone.Text = "";
                // txtbox_emp_postal.Text = "";
                txtboxcode_employeecode.Text = "";

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select The Plan.')", true);

            }

        }
        catch
        {

        }
    }
    int countofparker = 0;
    protected void btnclose1_Click(object sender, EventArgs e)
    {
        
       
            if (DropDownList1.SelectedValue == "2")
            {
                try
                {
                    insertrec_corporate_billingdetails_dependonuseofparking();
                }
                catch
                {
                }
            }
            if (countofparker <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Add At Least One Parker To The Account')", true);

            }
            if (countofparker > 0)
            {
            try
            {
                Session["empshow"] = null;
                Session["Parker_id"] = null;

                Session["Chargetable"] = null;

                Session["ChargetableRandom"] = null;
                Session["password"] = null;
                Session["lotid"] = null;
                Session["registersucceess"] = "true";
                Session["Billusername"] = txtBox_username.Text;
                if (lbl_invoicenumber.Text != "")
                {
                    Session["InvoiceNumber"] = lbl_invoicenumber.Text;
                }
                else
                {
                    Session["InvoiceNumber"] = "Invoice Not Generated";

                }
                Response.Redirect("Admin_Search_Corporate_Parker.aspx");



            }
            catch
            {
            }
        }
        
    }
    protected void rpt_emp_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void SqlDataSource2_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSource9_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSource10_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            TabContainer1.Tabs[4].Enabled = false;
            TabContainer1.Tabs[3].Enabled = true;
            TabContainer1.ActiveTabIndex = 3;
        }
        catch
        {
        }
    }
    protected void btn_close_Click(object sender, EventArgs e)
    {
        try
        {
            TabContainer1.Tabs[4].Enabled = false;
            TabContainer1.Tabs[3].Enabled = true;
            TabContainer1.ActiveTabIndex = 3;
        }
        catch
        {
        }
    }
 
    protected void ddl_plan_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            DataSet space_complete = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where PlanRateID=" + ddl_plan.SelectedValue);
            int total_reg = space_complete.Tables[0].Rows.Count;
            DataSet space_reserved = new clsInsert().fetchrec("select * from tbl_CorporateReservedParkingDetails where Planid=" + ddl_plan.SelectedValue);
            DataSet space_random = new clsInsert().fetchrec("select * from tbl_CorporateRandomParkingDetails where Planid=" + ddl_plan.SelectedValue);
            if (space_reserved.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt32(space_reserved.Tables[0].Rows[0][1]) == total_reg)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Registration Is Complete In This Plan.')", true);
                    lbl_plan.Visible = false;
                    ddl_plan.Visible = false;
                  //  ddl_plan.SelectedValue = "0";

                }
            }
            if (space_random.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt32(space_random.Tables[0].Rows[0][1]) == total_reg)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Registration Is Complete In This Plan.')", true);
                    lbl_plan.Visible = false;
                    ddl_plan.Visible = false;
                  //  ddl_plan.SelectedValue = "0";


                }
            }



        }
        catch
        {
        }
    }
}
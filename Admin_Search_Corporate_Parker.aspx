﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="Admin_Search_Corporate_Parker.aspx.cs" Inherits="Admin_Search_Corporate_Parker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <div class="body-right">
 <h1>Search Corporate Parkers</h1>
 <div>
  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
    <table class="style4">
        <tr>
            <td class="style7" width="150">
                <asp:CheckBox ID="chk_site" runat="server" Text=" Select  Customer" 
                    AutoPostBack="true" oncheckedchanged="chk_site_CheckedChanged" />
            </td>
            <td >
                <asp:DropDownList ID="ddl_sitename" runat="server" 
                    DataSourceID="SqlDataSource2" DataTextField="SiteName" DataValueField="SiteId" 
                    Enabled="False" AutoPostBack="True" CssClass="twitterStyleTextbox" AppendDataBoundItems="True" Width="305px">
                    <asp:ListItem Selected = "True" Text = "--Select Site--" Value = "0"></asp:ListItem>
                                
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [SiteName], [SiteId] FROM [tbl_SiteMaster] order by SiteName">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox ID="chk_campus" runat="server" Text=" Select Campus" 
                    oncheckedchanged="chk_campus_CheckedChanged"  AutoPostBack="true" />
            </td>
            <td>
                <asp:DropDownList ID="ddl_campusname" runat="server" 
                    DataSourceID="SqlDataSource3" DataTextField="Campus_Name" 
                    DataValueField="CampusID" Enabled="False" AutoPostBack="True" CssClass="twitterStyleTextbox" AppendDataBoundItems="True" Width="305px">
                    <asp:ListItem Selected = "True" Text = "--Select Campus--" Value = "0"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [CampusID], [Campus_Name] FROM [tbl_CampusMaster] order by Campus_Name">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox ID="chk_lot" runat="server" Text=" Select Lot" AutoPostBack="true" 
                    oncheckedchanged="chk_lot_CheckedChanged" />
            </td>
            <td>
                <asp:DropDownList ID="ddl_lotname" runat="server" DataSourceID="SqlDataSource4" 
                    DataTextField="LotName" DataValueField="Lot_id" Enabled="False" 
                    onselectedindexchanged="ddl_lotname_SelectedIndexChanged" 
                    AutoPostBack="True" AppendDataBoundItems="True" CssClass="twitterStyleTextbox" Width="305px">
                    <asp:ListItem Selected = "True" Text = "--Select Lot--" Value = "0"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [Lot_id], [LotName] FROM [tbl_LotMaster]  order by LotName">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox ID="chk_username" runat="server" Text="Corporation Name" 
                    oncheckedchanged="chk_username_CheckedChanged" AutoPostBack= "true"/>
            </td>
            <td>
                <asp:TextBox ID="txtbox_username" runat="server" Enabled="False" 
                    CssClass="twitterStyleTextbox" Width="285px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style7">
               <asp:Label ID="Result" runat="server" Text="" Visible="false"></asp:Label></td>  
            </td>
            <td>
               
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>

    </div>
    <table class="style4" width="100%">
    <tr>
        <td class="style8" width="150"></td>
           
        <td align="left">
           <asp:Button ID="btn_search" runat="server" onclick="btn_search_Click" 
                    Text="Search" CssClass="button" />
</td>
    </tr>
</table>
       <asp:Panel ID="Panel1" runat="server">
       <fieldset>
       
       <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource1" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Lot&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Corporation&nbsp;&nbsp;Name
                    </th>
                    <th width="5%">
                        Contact Person&nbsp;&nbsp;Name
                    </th>
                    <th>
                    User &nbsp;&nbsp; Name
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                    <th width="5%">
                        Deactivate
                    </th>
                    <%--<th width="5%">
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("LotName")%>
                    </td><td>
                    <%#Eval("FirstName")%>
                </td>
<td>
                    <%#Eval("LastName")%>
    
                </td>

                <td>
                    <%#Eval("UserName")%>
    
                </td>
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Parker_id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                    <asp:LinkButton ID="lnkinactive" CommandName="cmdInactive" CommandArgument='<%#Eval("UserName") %>' runat="server"><img src="images/inactive1.png" />
                    </asp:LinkButton>
                    
                </td>
                  <asp:Label ID="lotid" runat="server" Text='<%# Eval("LotId") %>' 
                                        Visible="False" />
                    <asp:Label ID="UserName" runat="server" Text='<%# Eval("UserName") %>' 
                                        Visible="False" />
                                   
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
           <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
               ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
               SelectCommand="SELECT top 10 tbl_LotMaster.LotName, tbl_ParkerRegis.Parker_id, tbl_ParkerRegis.LotId, tbl_ParkerRegis.AccountType, tbl_ParkerRegis.ParkingType, tbl_ParkerRegis.UserName, tbl_ParkerRegis.CardNo, tbl_ParkerRegis.FirstName, tbl_ParkerRegis.LastName, tbl_ParkerRegis.AdressLine1, tbl_ParkerRegis.AdressLine2, tbl_ParkerRegis.Zip, tbl_ParkerRegis.Country, tbl_ParkerRegis.State, tbl_ParkerRegis.City, tbl_ParkerRegis.CellNo, tbl_ParkerRegis.Phoneno, tbl_ParkerRegis.Fax, tbl_ParkerRegis.Email, tbl_ParkerRegis.Billingwith, tbl_ParkerRegis.PlanRateid, tbl_ParkerRegis.ActivationDate, tbl_ParkerRegis.CardDeliveryID FROM tbl_ParkerRegis INNER JOIN tbl_LotMaster ON tbl_ParkerRegis.LotId = tbl_LotMaster.Lot_id WHERE (tbl_ParkerRegis.AccountType = 'corporate') order by Parker_id desc">
           </asp:SqlDataSource>
       </fieldset>
       </asp:Panel>

       <asp:Panel ID="Panel2" runat="server" >
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
            <br />
            
            <asp:Repeater runat="server" id="rpt_searchparker" 
                onitemcommand="rpt_searchparker_ItemCommand1"   >
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Lot&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Corporation&nbsp;&nbsp;Name
                    </th>
                    <th width="5%">
                        Contact Person&nbsp;&nbsp;Name
                    </th>
                    <th>
                    User &nbsp;&nbsp; Name
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                    <th width="5%">
                        Deactivate
                    </th>
                    <%--<th width="5%">
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("LotName")%>
                    </td><td>
                    <%#Eval("FirstName")%>
                </td>
<td>
                    <%#Eval("LastName")%>
    
                </td>

                <td>
                    <%#Eval("UserName")%>
    
                </td>
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Parker_id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>

                <td>
                    <asp:LinkButton ID="lnkinactive1" CommandName="cmdInactive" CommandArgument='<%#Eval("UserName") %>' runat="server"><img src="images/inactive1.png" />
                    </asp:LinkButton>
                    
                </td>
                  <asp:Label ID="lotid" runat="server" Text='<%# Eval("LotId") %>' 
                                        Visible="False" />
                    <asp:Label ID="UserName" runat="server" Text='<%# Eval("UserName") %>' 
                                        Visible="False" />
                                   
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
    
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
       </div>
   
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class Admin_Search_Corporate_Parker : System.Web.UI.Page
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    decimal monthlyreservedcharges = Convert.ToDecimal("0.00");
    decimal monthlyrandomcharges = Convert.ToDecimal("0.00");
    decimal monthlycharges = Convert.ToDecimal("0.00");
    decimal monthly_totalcharge = Convert.ToDecimal("0.00");
    decimal creditamount = Convert.ToDecimal("0.00");
    decimal extramount = Convert.ToDecimal("0.00");

    string status = "true";
    string invoicenumber = "";
    string monthforinvoice = "";
    int dateformatch = 15;
    decimal amountoutstanding = Convert.ToDecimal("0.00");
    string billingtypeentry = "";
    string generatebill = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                if (Session["registersucceess"].ToString() == "true")
                {
                   // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Registered Successfully')", true);
                    if (Session["InvoiceNumber"].ToString() == "Invoice Not Generated")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Registered Successfully')", true);

                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('InvoiceCorporate.aspx','New Windows','height=800, width=600,location=yes');", true);
                    }
                    Session["registersucceess"] = null;
                }

                if (Session["registersucceess"].ToString() == "Update")
                {
                    // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Registered Successfully')", true);
                    if (Session["InvoiceNumber"].ToString() == "Invoice Not Generated")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Updated Successfully')", true);

                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('InvoiceCorporate.aspx','New Windows','height=800, width=600,location=yes');", true);
                    }
                    Session["registersucceess"] = null;
                }
            }
            catch
            {

            }
        }
    }

    
    public void countuserbill(string user)
    {
        DataSet dsuserlot = new clsInsert().fetchrec("select tbl_ParkerRegis.LotId ,tbl_LotMaster.Billing_exit from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId where UserName='"+user+"'");
        if (dsuserlot.Tables[0].Rows[0][1].ToString() == "1")
        {
            billingtypeentry = "Full Charge";
        }

        if (dsuserlot.Tables[0].Rows[0][1].ToString() == "2")
        {
            billingtypeentry = "Prorate";

        }

        if (dsuserlot.Tables[0].Rows[0][1].ToString() == "3")
        {
            billingtypeentry = "Prorate By Fort Night";

        }


        DataSet dsplan_reserved = new clsInsert().fetchrec("select tbl_CorporateLotRatePlan.MonthlyRate,tbl_CorporateReservedParkingDetails.NumberOfParker from tbl_CorporateLotRatePlan inner join tbl_CorporateReservedParkingDetails on tbl_CorporateReservedParkingDetails.Planid=tbl_CorporateLotRatePlan.PlanID_Corporate where tbl_CorporateLotRatePlan.Corporateusername='"+user+"' and tbl_CorporateLotRatePlan.ParkingType=1");
        if (dsplan_reserved.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsplan_reserved.Tables[0].Rows.Count; i++)
            {
                decimal totalmoney_reserved = Convert.ToDecimal(dsplan_reserved.Tables[0].Rows[i][0].ToString()) * Convert.ToDecimal(dsplan_reserved.Tables[0].Rows[i][1].ToString());
                monthlyreservedcharges = monthlyreservedcharges + totalmoney_reserved;
            }
        }
        DataSet dsplan_random = new clsInsert().fetchrec("select tbl_CorporateLotRatePlan.MonthlyRate,tbl_CorporateRandomParkingDetails.NumberOfParker from tbl_CorporateLotRatePlan inner join tbl_CorporateRandomParkingDetails on tbl_CorporateRandomParkingDetails.Planid=tbl_CorporateLotRatePlan.PlanID_Corporate where tbl_CorporateLotRatePlan.Corporateusername='"+user+"' and tbl_CorporateLotRatePlan.ParkingType=2");

        if (dsplan_random.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsplan_random.Tables[0].Rows.Count; i++)
            {
                decimal totalmoney_random = Convert.ToDecimal(dsplan_random.Tables[0].Rows[i][0].ToString()) * Convert.ToDecimal(dsplan_random.Tables[0].Rows[i][1].ToString());

                monthlyrandomcharges = monthlyrandomcharges + totalmoney_random;
            }
        }
        monthlycharges = monthlyrandomcharges + monthlyreservedcharges;
        if (billingtypeentry == "Full Charge")
        {
            monthly_totalcharge = monthlycharges;
        }
        if (billingtypeentry == "Prorate")
        {
            DateTime active = DateTime.Now;
            int y = active.Year;
            int m = active.Month;
            int d = active.Day;
            int days = DateTime.DaysInMonth(y, m);
            int daysinmonthremaining = days - d;
            double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days);
            monthly_totalcharge = monthlycharges * Convert.ToDecimal(daystocharge);
            generatebill = "yes";

        }
        if (billingtypeentry == "Prorate By Fort Night")
        {
            DateTime active = DateTime.Now;
            int d = active.Day;
            if (d > dateformatch)
            {
                monthly_totalcharge = monthlycharges;

            }
            else
            {

                monthly_totalcharge = monthlycharges / 2;
                generatebill = "yes";
            }
        }
        if (generatebill == "yes")
        {
            decimal taxcharge = Convert.ToDecimal("0.13");
            decimal subtotal = monthly_totalcharge;
            decimal total = monthly_totalcharge * taxcharge;
            DataSet ds_creditamount = new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='" + user + "' order by CreditNoteID desc");
            if (ds_creditamount.Tables[0].Rows.Count > 0)
            {
                creditamount = Convert.ToDecimal(ds_creditamount.Tables[0].Rows[0][2].ToString());

            }
            DataSet ds_amountoutsatnding = new clsInsert().fetchrec("select Amt_Outstanding,Biliing_type from TBL_Invoice where Acct_Num='" + user + "' order by ID desc");
            if (ds_amountoutsatnding.Tables[0].Rows.Count > 0)
            {
                if (creditamount > 0)
                {
                    extramount = Convert.ToDecimal(ds_amountoutsatnding.Tables[0].Rows[0][0].ToString());
                    creditamount = creditamount + extramount;
                }
               
                for (int i = 0; i < ds_amountoutsatnding.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                        continue;
                    else
                    amountoutstanding = amountoutstanding + Convert.ToDecimal(ds_amountoutsatnding.Tables[0].Rows[i][0].ToString());
                }

            }

            decimal grandtotal = monthly_totalcharge + total + amountoutstanding;



            DateTime activedate = DateTime.Now;
            int year1 = activedate.Year;
            int month1 = activedate.Month;
            string month = "";
            if (month1 < 10)
            {
                month = "0" + Convert.ToString(month1);
            }
            if (month1 >= 10)
            {
                month = Convert.ToString(month1);

            }
            string yearmonth = Convert.ToString(year1) + Convert.ToString(month);
            DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
            if (dsinvoice.Tables[0].Rows.Count > 0)
            {
                string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                string[] strArr = oFName.Split('_');
                int sLength = strArr.Length;
                int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                if (number < 10)
                {
                    invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                }
                if (number >= 10 && number < 100)
                {
                    invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                }
                if (number >= 100 && number < 1000)
                {
                    invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                }
                if (number >= 1000 && number < 10000)
                {
                    invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                }
            }
            else
            {
                invoicenumber = yearmonth + "_" + "000001";
            }

            hst.Add("action", "insert_billing_corporate");
            hst.Add("invoicenumber", invoicenumber);

            hst.Add("Parkerusername", user);

            DateTime today = DateTime.Now;
            DateTime startOfMonth = new DateTime(today.Year, today.Month, 1);
            hst.Add("startdate", Convert.ToDateTime(startOfMonth));



            // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
            hst.Add("duedate", DateTime.Now);
            hst.Add("subtotal", subtotal);
            hst.Add("tax", Convert.ToDecimal(total));
            hst.Add("details", "Invoice At The Time Of Account Deactivate");

            hst.Add("totalamount", Convert.ToDecimal(grandtotal));
            if (creditamount < grandtotal)
            {
                decimal dueamount = Convert.ToDecimal(grandtotal) - creditamount;
                hst.Add("dueamount ", dueamount);

            }
            if (creditamount >= grandtotal)
            {
                decimal dueamount = Convert.ToDecimal("0.00");
                hst.Add("dueamount ", dueamount);

            }
            hst.Add("billingtype", Convert.ToInt32(ds_amountoutsatnding.Tables[0].Rows[0][1].ToString()));


            int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
            {
                hst.Clear();
                hst.Add("action", "insert");
                hst.Add("acctnumber", user);
                hst.Add("reason", "At The Time Of Terminating The Account");
                if (creditamount > grandtotal)
                {
                    decimal credit = creditamount - grandtotal;
                    hst.Add("creditamt", Convert.ToDecimal(credit));
                }
                if (creditamount <= grandtotal)
                {
                    // decimal credit = creditamount - grandtotal;
                    hst.Add("creditamt", Convert.ToDecimal("0.00"));
                }
                hst.Add("dateoftransaction", DateTime.Now);
                hst.Add("status", status);

                int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                {
                    if (monthly_totalcharge > 0)
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", invoicenumber);
                        hst.Add("billtitle", "Monthly Charges Of Current Month At The Time Of Termination Of Account");
                        hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", user);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (total > 0)
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", invoicenumber);
                        hst.Add("billtitle", "Tax Charges Of Current Month At The Time Of Termination Of Account");
                        hst.Add("amountparticular", Convert.ToDecimal(total));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", user);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (amountoutstanding > 0)
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", invoicenumber);
                        hst.Add("billtitle", "Total OutStanding Charges Of Previous Months At The Time Of Termination Of Account");
                        hst.Add("amountparticular", Convert.ToDecimal(amountoutstanding));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", user);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                }
            }
        }
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        try
        {
            if (e.CommandName == "cmdEdit")
            {

                Label UserName = rpt1.Items[i].FindControl("UserName") as Label;
                Label Lotid = rpt1.Items[i].FindControl("Lotid") as Label;

                Session["parkerUsername"] = UserName.Text;
                Session["lotid"] = Lotid.Text;
                DataSet dscheckactive = new clsInsert().fetchrec("select [status] from tbl_UserLogin where Username='" + UserName.Text + "'");

                if (dscheckactive.Tables[0].Rows[0][0].ToString() == "True")
                {
                    Response.Redirect("Admin_Edit_Corporate_Parker.aspx");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Not Activated')", true);

                }

            }
            if (e.CommandName == "cmdInactive")
            {
                string user = e.CommandArgument.ToString();
                DataSet dscheckactive = new clsInsert().fetchrec("select [status] from tbl_UserLogin where Username='" + user + "'");
                if (dscheckactive.Tables[0].Rows[0][0].ToString() == "True")
                {
                    countuserbill(user);
                    updateuserstatus(user);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Deactivated Successfully')", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Already Deactivated')", true);

                }

            }
        }
        catch
        {
        }

    }
    string site, campus, lot, name;
    public void updateuserstatus(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "updatestatus");
            //hst.Add("ustatus", userstattus1);
            hst.Add("username", username);



            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                // Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                //  Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
            // Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (chk_username.Checked == true)
        {
            name = txtbox_username.Text;

        }
        else
        {
            name = "";

        }

        if (chk_lot.Checked == true)
        {
            lot = ddl_lotname.SelectedValue;

        }
        else
        {
            lot = "";
        }

        if (chk_campus.Checked == true)
        {
            campus = ddl_campusname.SelectedValue;
        }
        else
        {
            campus = "";
        }

        if (chk_site.Checked == true)
        {
            site = ddl_sitename.SelectedValue;

        }
        else
        {
            site = "";
        }




        DataTable dt = new clsInsert().getparkersbysite_Corporate(name, lot, campus, site);
        int count = dt.Rows.Count;
        if (dt.Rows.Count > 0)
        {
            rpt1.Visible = false;
            rpt_searchparker.Visible = true;

            Result.Visible = true;
            Result.Text = count.ToString() + " " + "Records Found";

            rpt_searchparker.DataSource = dt;
            rpt_searchparker.DataBind();

        }
        else
        {
            rpt1.Visible = true;
            Result.Visible = false;
            rpt_searchparker.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);

        }

    }
    protected void chk_site_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_site.Checked == true)
        {
            ddl_sitename.Enabled = true;
        }
        else
        {
            ddl_sitename.Enabled = false;
        }
    }
    protected void chk_campus_CheckedChanged(object sender, EventArgs e)
    {

        if (chk_campus.Checked == true)
        {
            ddl_campusname.Enabled = true;
        }
        else
        {
            ddl_campusname.Enabled = false;
        }
    }
    protected void chk_lot_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_lot.Checked == true)
        {
            ddl_lotname.Enabled = true;
        }
        else
        {
            ddl_lotname.Enabled = false;
        }
    }
    protected void chk_username_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_username.Checked == true)
        {
            txtbox_username.Enabled = true;
        }
        else
        {
            txtbox_username.Text = "";
            txtbox_username.Enabled = false;
        }
    }
    protected void ddl_lotname_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rpt_searchparker_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void rpt_searchparker_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        try
        {
            if (e.CommandName == "cmdEdit")
            {

                Label UserName = rpt_searchparker.Items[i].FindControl("UserName") as Label;
                Label Lotid = rpt_searchparker.Items[i].FindControl("Lotid") as Label;

                Session["parkerUsername"] = UserName.Text;
                Session["lotid"] = Lotid.Text;
                DataSet dscheckactive=new clsInsert().fetchrec("select [status] from tbl_UserLogin where Username='"+UserName.Text+"'");
                if (dscheckactive.Tables[0].Rows[0][0].ToString() == "True")
                {
                    Response.Redirect("Admin_Edit_Corporate_Parker.aspx");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Not Activated')", true);

                }
            }
            if (e.CommandName == "cmdInactive")
            {
                string user = e.CommandArgument.ToString();
                DataSet dscheckactive = new clsInsert().fetchrec("select [status] from tbl_UserLogin where Username='" + user + "'");
                if (dscheckactive.Tables[0].Rows[0][0].ToString() == "True")
                {
                    countuserbill(user);
                    updateuserstatus(user);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Deactivated Successfully')", true);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Already Deactivated')", true);

                }

               
            }
        }
        catch
        {
        }
    }
}
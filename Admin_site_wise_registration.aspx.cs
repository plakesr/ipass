﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections;

public partial class Admin_site_wise_registration : System.Web.UI.Page
{
    StringBuilder str = new StringBuilder();
    //Get connection string from web.config
    SqlConnection conn = new SqlConnection("Data source=localhost; Initial catalog=db_IPASS; Integrated security=true");
    DataTable getsite = new DataTable();

   // DataTable getlots = new DataTable();
    DataTable dt_countindividual = new DataTable();
    DataTable dt_countcorporate = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            BindChart();
        }
    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    private DataTable GetData()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("site_name", typeof(string));
        dt.Columns.Add("individual", typeof(string));
        dt.Columns.Add("corporate", typeof(string));

        getsite = new clsInsert().getsite();
        RemoveDuplicateRows(getsite, "Site_Id");
        for (int i = 0; i < getsite.Rows.Count; i++)
        {
            
            DataRow dr = dt.NewRow();
            dt_countindividual = new clsInsert().sitecountindividual(Convert.ToInt32(getsite.Rows[i]["Site_Id"].ToString()));
            dt_countcorporate = new clsInsert().sitecountcorporate(Convert.ToInt32(getsite.Rows[i]["Site_Id"].ToString()));

            dr[0] = getsite.Rows[i]["SiteName"].ToString();
            dr[1] = dt_countindividual.Rows[0]["totalparkerIndividual"].ToString();
            dr[2] = dt_countcorporate.Rows[0]["totalparkercorporate"].ToString();
            dt.Rows.Add(dr);
        }

        //DataTable dt = new DataTable();
        //string cmd = "select * from Sales";

        return dt;
    }

    private void BindChart()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = GetData();

            str.Append(@"<script type=text/javascript> google.load( *visualization*, *1*, {packages:[*corechart*]});
                       google.setOnLoadCallback(drawChart);
                       function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'site_name');
        data.addColumn('number', 'Individual');
        data.addColumn('number', 'Corporate');       

        data.addRows(" + dt.Rows.Count + ");");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                str.Append("data.setValue( " + i + "," + 0 + "," + "'" + dt.Rows[i][0].ToString() + "');");
                str.Append("data.setValue(" + i + "," + 1 + "," + dt.Rows[i][1].ToString() + ") ;");
                str.Append("data.setValue(" + i + "," + 2 + "," + dt.Rows[i][2].ToString() + ") ;");

                //str.Append("data.setValue( " + i + "," + 0 + "," + "'" + dt.Rows[i][0].ToString() + "');");
                //str.Append("data.setValue(" + i + "," + 0 + "," + dt.Rows[i][1].ToString() + ") ;");
                //str.Append("data.setValue(" + i + "," + 0 + "," + dt.Rows[i][2].ToString() + ") ;");

            }

            str.Append(" var chart = new google.visualization.BarChart(document.getElementById('chart_div'));");
            str.Append(" chart.draw(data, {width: 850, height: 900, title: 'Number Of Registration',");
            str.Append("vAxis: {title: 'Customer Name', titleTextStyle: {color: 'green'}}");
            // str.Append("hAxis: {title: 'OperatorName', titleTextStyle: {color: 'green'}}");

            str.Append("}); }");
            str.Append("</script>");
            lt.Text = str.ToString().Replace('*', '"');
            //lt.Text = str.ToString().TrimEnd(',').Replace('*', '"');
        }
        catch
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;

/// <summary>
/// Summary description for clsInsert
/// </summary>
public class clsInsert
{
	public clsInsert()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public string connString()
    {
        return ConfigurationManager.ConnectionStrings["constr"].ToString();
    }
    public DataTable sitename(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getsitename");
        adp.SelectCommand.Parameters.AddWithValue("@lot_id", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable billingtransact(string username)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_BillingTransact", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "get");
        adp.SelectCommand.Parameters.AddWithValue("@username", username);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }



    //public DataTable Coupanactive(string series)
    //{

    //    QueryStringAccess com = new QueryStringAccess();
    //    SqlConnection con = new SqlConnection(com.connString());
    //    SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
    //    adp.SelectCommand.CommandType = CommandType.StoredProcedure;
    //    adp.SelectCommand.Parameters.AddWithValue("@action", "active");
    //    adp.SelectCommand.Parameters.AddWithValue("@coupan", series);



    //    DataSet ds = new DataSet();
    //    adp.Fill(ds);
    //    return ds.Tables[0];
    //}
    public DataTable lotid(string lotname)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getlotid");
        adp.SelectCommand.Parameters.AddWithValue("@series", lotname);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getparker(string name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_CashAccount", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");

        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getparker_corporate(string name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_CashAccount", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchcorporate");

        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable fullcoupandetails(string coupanseries)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getcoupanfulldetails");
        adp.SelectCommand.Parameters.AddWithValue("@coupan", coupanseries);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable countoperator()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "alloperator");



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getlots()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getlots");



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getsite()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getsite");



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getcampus()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getcampus");



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable selectsitelotcampus(string lotname)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("[sp_Refund_Customer]", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "selectcampussite");
        adp.SelectCommand.Parameters.AddWithValue("@lotname", lotname);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable Gelotid(string lotname)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("[sp_Refund_Customer]", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "Gelotid");
        adp.SelectCommand.Parameters.AddWithValue("@lotname", lotname);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    
    public DataTable dueamount(string username)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billingfullinfo", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdueamount");
        adp.SelectCommand.Parameters.AddWithValue("@username", username);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable dueamount1(string username)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billingfullinfo", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdueamount1");
        adp.SelectCommand.Parameters.AddWithValue("@username", username);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable AllCoupan()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "allcoupan");
        



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable CoupanCount(string coupan)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getquantity");
        adp.SelectCommand.Parameters.AddWithValue("@coupan", coupan);
        



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable countuserbill()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billingfullinfo", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getuser");
       // adp.SelectCommand.Parameters.AddWithValue("@username", username);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable countuserbill_corporate_payperuse()
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billingfullinfo", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getuser_corporate_payperuse");
        // adp.SelectCommand.Parameters.AddWithValue("@username", username);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    
    public DataTable countuserbill_corporate()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billingfullinfo", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getuser_corporate");
        // adp.SelectCommand.Parameters.AddWithValue("@username", username);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable countdate()
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "alldates");



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable GetAllCountry()
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getcountrystatecity", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getcountry");
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable GetAllstate(int id)
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getcountrystatecity", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getstate");
        adp.SelectCommand.Parameters.AddWithValue("@id", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable GetAllCity(int id)
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getcountrystatecity", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getcity");
        adp.SelectCommand.Parameters.AddWithValue("@id", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable countdates_individual(DateTime date1, DateTime date2)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "allindividual_dates");
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable countdates_Corporate(DateTime date1, DateTime date2)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "allCorporate_dates");
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable countindividual(string modifiedby)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "individualReg");
        adp.SelectCommand.Parameters.AddWithValue("@modifiedby", modifiedby);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable operatorcountindividual(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "individualRegfoeoperator");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable sitecountindividual(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "individualRegforsite");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable campuscountindividual(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "individualRegforcampus");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable operatorcountcorporate(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "corporateRegfoeoperator");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable sitecountcorporate(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "corporateRegforsite");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable campuscountcorporate(int lotid)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "corporateRegforcampus");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable countcorporate(string modifiedby)
    {

        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "corporateReg");
        adp.SelectCommand.Parameters.AddWithValue("@modifiedby", modifiedby);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }













    public void InsertCountry(EntInsert entrv)
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter dap = new SqlDataAdapter("sp_country", con);
        dap.SelectCommand.CommandType = CommandType.StoredProcedure;
        dap.SelectCommand.Parameters.AddWithValue("@action", entrv.action);
        dap.SelectCommand.Parameters.AddWithValue("@country_name", entrv.countryname);
        dap.SelectCommand.Parameters.AddWithValue("@country_code", entrv.countrycode);
        dap.SelectCommand.Parameters.AddWithValue("@country_flag", entrv.countryflag);
        dap.SelectCommand.Parameters.AddWithValue("@country_abbrvtion", entrv.countryabbr);

        DataSet ds = new DataSet();
        dap.Fill(ds);

    }

    public void InsertCity(EntInsert entrv)
    {
        QueryStringAccess com = new QueryStringAccess();
        SqlConnection con = new SqlConnection(com.connString());
        SqlDataAdapter dap = new SqlDataAdapter("sp_city", con);
        dap.SelectCommand.CommandType = CommandType.StoredProcedure;
        dap.SelectCommand.Parameters.AddWithValue("@action", entrv.action);
        dap.SelectCommand.Parameters.AddWithValue("@countyid", entrv.countryid);
        dap.SelectCommand.Parameters.AddWithValue("@city_name", entrv.cityname);
        dap.SelectCommand.Parameters.AddWithValue("@city_code", entrv.citycode);
        dap.SelectCommand.Parameters.AddWithValue("@city_desc", entrv.citydesc);
        dap.SelectCommand.Parameters.AddWithValue("@city_abbrvtion", entrv.cityabbr);
        dap.SelectCommand.Parameters.AddWithValue("@zip", entrv.zip);

        DataSet ds = new DataSet();
        dap.Fill(ds);

    }

    
    public DataTable getvalue(string id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_forrepeater", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@name", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getduepaymentparker(string name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_search_duepayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");

        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getduepaymentparker_corporate(string name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_search_duepayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search_corporate");

        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }








    public DataTable getduepaymentparkerbyname(int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billbyoperator", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchall");

       // adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getduepaymentparker(string name,int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billbyoperator", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbytextbox");

         adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getcampus(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_forsearchofcampus", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "get");

        adp.SelectCommand.Parameters.AddWithValue("@id", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getvalue1()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_forrepeater1", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        

        //adp.SelectCommand.Parameters.AddWithValue("@name", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getsiteforparker(string cityname,string zip)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getsiteForParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@city", cityname);
        adp.SelectCommand.Parameters.AddWithValue("@zip", "%" + zip + "%");

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getrefunddetails()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetails");
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
       
    }


    public DataTable getdetailsforpmo_pending()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforpmo_pending");
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }

    public DataTable getrefunddetailsforpmbylot(int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforpm");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }
    public DataTable getdueparkerbylot(int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_billbyoperator", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "serach");
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }

    public DataTable getlotsforPm(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getlotsforpm");
        adp.SelectCommand.Parameters.AddWithValue("@username", username);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }
    public DataTable getlots(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getlotname");
        adp.SelectCommand.Parameters.AddWithValue("@username", username);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }
    public DataTable binddata(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "binddata");
        adp.SelectCommand.Parameters.AddWithValue("@username", username);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }
    public DataTable allbind()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "allbind");
       


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }
    public DataTable getdetailsforclientmaneger()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforclientmaneger");
       
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
        
    }


    public DataTable getdetailsforclientmanegerbylot(int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforclientmaneger");
        adp.SelectCommand.Parameters.AddWithValue("@lotid",lotid);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }

    public DataTable getdetailsforAccounting()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforAccounting");
       
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
        
    }

     public DataTable getdetailsforAccounting_pending()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "accountingpending");
       
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
        
    }
    public DataTable getdetailsforDatacenter()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforDatacenter");
       
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
        
    }



    public DataTable getdetailsforPMO()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdetailsforPMO");
       
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
        
    }


    public DataTable getstatus_redirect(int refundtrackingid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getstatus_redirect");
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid", refundtrackingid);

        


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];

    }

    public DataTable getparkers(string Name )
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");
       
        adp.SelectCommand.Parameters.AddWithValue("@name", "%" + Name + "%");
        
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable Getsites(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter dap = new SqlDataAdapter("spGetallsites", con);
        dap.SelectCommand.CommandType = CommandType.StoredProcedure;
        dap.SelectCommand.Parameters.AddWithValue("@site_id", id);
        DataSet ds = new DataSet();
        dap.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable Getlot_chargeby(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter dap = new SqlDataAdapter("sp_getchargebytype", con);
        dap.SelectCommand.CommandType = CommandType.StoredProcedure;
        dap.SelectCommand.Parameters.AddWithValue("@lotid", id);
        DataSet ds = new DataSet();
        dap.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable RemoveDuplicateRows1(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    public List<clsData> Getlot_chargeby1(int Id)
    {
        List<clsData> lst = new List<clsData>();
        DataTable dt = new clsInsert().Getlot_chargeby(Id);
        RemoveDuplicateRows1(dt, "chargeby");
        foreach (DataRow rw in dt.Rows)
        {
            clsData obj = new clsData();
            obj.LotRateid = Convert.ToInt32(rw["chargeby"]);
            if (rw["chargeby"].ToString() == "1")
            {
                obj.Charge = "Hourly";

            }
            if (rw["chargeby"].ToString() == "2")
            {
                obj.Charge ="Daily";
            }
            if (rw["chargeby"].ToString() == "3")
            {
                obj.Charge = "Monthly";
            }
            if (rw["chargeby"].ToString() == "4")
            {
                obj.Charge = "Weekly";
            }
            lst.Add(obj);
        }
        return lst;
    }

    public DataTable GetCampus(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter dap = new SqlDataAdapter("spGetallcampus", con);
        dap.SelectCommand.CommandType = CommandType.StoredProcedure;
        dap.SelectCommand.Parameters.AddWithValue("@site_id", id);
        DataSet ds = new DataSet();
        dap.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable GetLots(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter dap = new SqlDataAdapter("spGetalllots", con);
        dap.SelectCommand.CommandType = CommandType.StoredProcedure;
        dap.SelectCommand.Parameters.AddWithValue("@campus_id", id);
        DataSet ds = new DataSet();
        dap.Fill(ds);
        return ds.Tables[0];
    }
    public List<clsData> GetSiteslist(int Id)
    {
        List<clsData> lst = new List<clsData>();
        DataTable dt = new clsInsert().Getsites(Id);
        foreach (DataRow rw in dt.Rows)
        {
            clsData obj = new clsData();
            obj.siteid = Convert.ToInt32(rw["SiteId"]);
            obj.sitename = rw["SiteName"].ToString();

            lst.Add(obj);
        }
        return lst;
    }

   
    public List<clsData> GetCampuslist(int Id)
    {
        List<clsData> lst = new List<clsData>();
        DataTable dt = new clsInsert().GetCampus(Id);
        foreach (DataRow rw in dt.Rows)
        {
            clsData obj = new clsData();
            obj.campusid = Convert.ToInt32(rw["CampusID"]);
            obj.campusname = rw["Campus_Name"].ToString();

            lst.Add(obj);
        }
        return lst;
    }
    public List<clsData> Getlotslist(int Id)
    {
        List<clsData> lst = new List<clsData>();
        DataTable dt = new clsInsert().GetLots(Id);
        foreach (DataRow rw in dt.Rows)
        {
            clsData obj = new clsData();
            obj.lotid = Convert.ToInt32(rw["Lot_id"]);
            obj.lotname = rw["LotName"].ToString();

            lst.Add(obj);
        }
        return lst;
    }



    public DataTable getparkersbylot(int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbylot");

       
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getparkersbycampus(int campusid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbycampus");


        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getparkersbysite(string name, string lotid, string campusid, string siteid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbysite");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getparkersbysite_Corporate(string name, string lotid, string campusid, string siteid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Parker_Corporate", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbysite");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid_search", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getparkersbysite_Corporate_operator(string name, string lotid, string campusid, string siteid,string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Parker_Corporate", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbysite_operator");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid_search", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@UserName", username);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getrefundparker(string name, string lotid, string campusid, string siteid, string username, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);

      //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getrefundparker_pmo(string name, string lotid, string campusid, string siteid,  string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbypmopending");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);

        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    
    public DataTable getrefundparkeronlyrefundtrackingid(int id,string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "refundonlytrackingid");
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid", id);
        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

     public DataTable getrefundparkeronlyrefundtrackingidbypmo(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "refundonlytrackingidbypmo");
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid", id);
       
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getrefundparkercm(string name, string lotid, string campusid, string siteid, string username, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "CMsearch");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getrefundparkeraccounting(string name, string lotid, string campusid, string siteid, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "Accountingsearch");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getrefundparkeraccounting1(string name, string lotid, string campusid, string siteid, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "Accountingsearch1");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);
       
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getrefundparkeraccounting2(string name, string lotid, string campusid, string siteid, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "Accountingsearch2");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }







    public DataTable getrefundparkerdatacenter(string name, string lotid, string campusid, string siteid, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "DataCentersearch");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);

        //adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }




    public DataTable getrefundparkerdatacenter1(string name, string lotid, string campusid, string siteid, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "DataCentersearch1");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);

        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getrefundparkerdatacenter2(string name, string lotid, string campusid, string siteid, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "DataCentersearch2");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);

        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        //  adp.SelectCommand.Parameters.AddWithValue("@dateofsubmition", dateofsubmition);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    
    public DataTable getrefundparker1(string name, string lotid, string campusid, string siteid, string username, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search1");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);
       
        
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getrefundparker_pmo1(string name, string lotid, string campusid, string siteid, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbypmopending1");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
       
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);
       
        
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getrefundparkercm1(string name, string lotid, string campusid, string siteid, string username, string reqstatus, DateTime date1, DateTime date2, string refundtrackingid_search)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "CMsearch1");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        adp.SelectCommand.Parameters.AddWithValue("@reqstatus", reqstatus);
        adp.SelectCommand.Parameters.AddWithValue("@date1", date1);
        adp.SelectCommand.Parameters.AddWithValue("@date2", date2);
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid_search", refundtrackingid_search);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getallrefundparker(string name, string lotid, string campusid, string siteid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchRefund", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchall");
        adp.SelectCommand.Parameters.AddWithValue("@siteid", siteid);
        adp.SelectCommand.Parameters.AddWithValue("@campusid", campusid);
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@name", name);
      

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getcoupan(string coupan)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");

        adp.SelectCommand.Parameters.AddWithValue("@coupan", coupan);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getparkersvehicles(string Name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_VehiclesofIndividualParkers", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;

        adp.SelectCommand.Parameters.AddWithValue("@username", Name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getparkersdetails(string Name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("profileupdate", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;

        adp.SelectCommand.Parameters.AddWithValue("@username", Name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getparkersBankAccountDetails(string Name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "info");

        adp.SelectCommand.Parameters.AddWithValue("@name", Name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getparkersotherDetails(string Name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "details");

        adp.SelectCommand.Parameters.AddWithValue("@name", Name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getparkersplan(string Name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_SearchParker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "planinfo");

        adp.SelectCommand.Parameters.AddWithValue("@name", Name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getCoupanseries(string series)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Coupan", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getquantity1");

        adp.SelectCommand.Parameters.AddWithValue("@coupan", series);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable alreadyexist(string action,string check)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_check", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", action);
        if (action.ToString().Trim() == "usernamecheck")
        {
            adp.SelectCommand.Parameters.AddWithValue("@username", check);
        }
        if (action.ToString().Trim() == "emailverify")
        {
            adp.SelectCommand.Parameters.AddWithValue("@email", check);
        }
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getsiteforparker1(int id )
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getsiteForParker1", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@id", id);
       
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getdetailsofrefundcustomer(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "refundcustomerdetail");

        adp.SelectCommand.Parameters.AddWithValue("@refundcustomerid", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getdetailsofrefundcustomercm(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "refundcustomerdetailforcm");

        adp.SelectCommand.Parameters.AddWithValue("@refundcustomerid", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable refundcustomerdetailforrejection(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "refundcustomerdetailforrejection");

        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getpmsign(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getpmsign");

        adp.SelectCommand.Parameters.AddWithValue("@refundcustomerid", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getcuntryflag(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getFlag", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@id", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getsitelotdetailsforparker(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getsitelotdetailsfroparker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@id", id);
        

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getsiteTariffPlandetailsforparker(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_getsitetariffdetailsfroparker", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@id", id);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataSet select_operation(String qry)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        con.Open();

        SqlCommand cmd = new SqlCommand(qry, con);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        con.Close();
        return ds;
    }

    public DataTable LoginCheck(string username,string pass)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("spGetLoginCheck", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "logincheck");

        adp.SelectCommand.Parameters.AddWithValue("@username", username);
        adp.SelectCommand.Parameters.AddWithValue("@pass", pass);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable CorporateRegistrationcheck(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("spGetLoginCheck", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@type", "corpratecheckdetail");

        adp.SelectCommand.Parameters.AddWithValue("@username", username);
     
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable CorporateParkerParentDetail(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("spGetLoginCheck", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "corprateparkerparentdetail");

        adp.SelectCommand.Parameters.AddWithValue("@username", username);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }



   public DataTable OperatorProfile(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_OperatorProfile", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");

        adp.SelectCommand.Parameters.AddWithValue("@username", username);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
 public DataTable authenticatedlots(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());

        SqlDataAdapter adp = new SqlDataAdapter("fetchmenu", con);

        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@username", username);

   
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }



    public DataTable getoperator(string Name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_OperatorSearch", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbyname");

        adp.SelectCommand.Parameters.AddWithValue("@username", "%" + Name + "%");

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getoperatorbylot(int lotid,string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_OperatorSearch", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchbylot");
        adp.SelectCommand.Parameters.AddWithValue("@username", "%" + username + "%");

        adp.SelectCommand.Parameters.AddWithValue("@lotid",  lotid );

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable refundparkersearchbyname(string name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchrefundbyname");
      

        adp.SelectCommand.Parameters.AddWithValue("@name", name );

       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable refundparkersearchbyticket(string ticketno)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchrefundbyticket");
        adp.SelectCommand.Parameters.AddWithValue("@ticketno", ticketno);


        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable refundparkersearchbybusiness(string business)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchrefundbybusiness");


        adp.SelectCommand.Parameters.AddWithValue("@businessunit", business);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable refundparkersearchbydate(DateTime date)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "searchrefundbybusiness");


        adp.SelectCommand.Parameters.AddWithValue("@transariondate", date);



        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable getoperatorbylotandname(string name, int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_OperatorSearch", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");
        adp.SelectCommand.Parameters.AddWithValue("@username", "%" + name + "%");

        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid );

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getrefundparkerstatus(string lname, int requestid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
         
        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "statusdetailbyparker");
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid", requestid);

        adp.SelectCommand.Parameters.AddWithValue("@lname", lname);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


    public DataTable getdateandamount(int requestid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());

        SqlDataAdapter adp = new SqlDataAdapter("sp_RefundPayment", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getdateandamount");
        adp.SelectCommand.Parameters.AddWithValue("@refundtrackingid", requestid);

       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable usercheck(string username)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("spGetLoginCheck", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "getuser");

        adp.SelectCommand.Parameters.AddWithValue("@username", username);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataSet fetchrec(string query)
    {
        QueryStringAccess obj = new QueryStringAccess();
         SqlConnection con = new SqlConnection(obj.connString());
         SqlDataAdapter da = new SqlDataAdapter(query, con);
         DataSet ds = new DataSet();
         da.Fill(ds);
         return ds;
    }
    

    public DataTable get_Lotdetails_lotsearch(int id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Lotdetails_lotsearch", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@lotid", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

    public DataTable get_LotRatedetails_lotsearch(int lotid,int chargebyid,int parkingtypeid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Ratedetailsforlots", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@chargeby", chargebyid);
        adp.SelectCommand.Parameters.AddWithValue("@parkingtype", parkingtypeid);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }


     public DataTable get_LotRatedetails_lotsearchmonth(int lotid,int chargebyid,int parkingtypeid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_Ratedetailsforlotsmonth", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);
        adp.SelectCommand.Parameters.AddWithValue("@chargeby", chargebyid);
        adp.SelectCommand.Parameters.AddWithValue("@parkingtype", parkingtypeid);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
     public DataTable LoginHistory(string username)
     {
         QueryStringAccess obj = new QueryStringAccess();
         SqlConnection con = new SqlConnection(obj.connString());
         SqlDataAdapter adp = new SqlDataAdapter("sp_loginhistory", con);
         adp.SelectCommand.CommandType = CommandType.StoredProcedure;
         adp.SelectCommand.Parameters.AddWithValue("@action", "fetch");

         adp.SelectCommand.Parameters.AddWithValue("@username", username);
        

         DataSet ds = new DataSet();
         adp.Fill(ds);
         return ds.Tables[0];
     }

     DataSet ds;
     public DataSet fetchdatea(string comd)
     {
         try
         {
             QueryStringAccess obj = new QueryStringAccess();
               SqlConnection con = new SqlConnection(obj.connString());
               SqlDataAdapter da = new SqlDataAdapter(comd, con);
               ds = new DataSet();
               da.Fill(ds);

             return ds;
         }
         catch
         {
             return ds;
         }
     }
}
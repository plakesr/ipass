﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntInsert
/// </summary>
public class EntInsert
{
	public EntInsert()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int countryid { get; set; }
    public string action { get; set; }
    public string countryname { get; set; }
    public string countrycode { get; set; }
    public string countryflag { get; set; }
    public string countryabbr { get; set; }


    public int cityid { get; set; }
    public string cityname { get; set; }
    public string citycode { get; set; }
    public string citydesc { get; set; }
    public string cityabbr { get; set; }
    public string zip { get; set; }

}
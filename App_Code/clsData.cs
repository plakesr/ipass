﻿using System;
using System.Data;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using System.Net.Mail;
using System.Net;
//using System.Windows.Forms;

public enum Actions
{
    Select,
    Insert,
    Update,
    Delete
}


    public class clsData : DbProviderFactory
    {
        static string ename = "";
    #region Private Variables
    String strConnection = "";
    String providerName = "";
    DbProviderFactory provider;
    string strtbls;
    public  string strcon;
   
    string sql;
    public static DbConnection conn;
    Hashtable hstParameters1 = new Hashtable();
    DataSet ds = new DataSet();
#endregion Private Variables

    public clsData()
	{
        provider = GetProvider();
        funcon();
	}
    ///<Author>Shalu Madan</Author>
    /// <Date>24-03-07</Date>
    /// <summary>
    /// Function to get the provider of the connection to the DB.
    /// </summary>
    /// <returns>Provider (Sql, Oracle)</returns>
    /// 
    public int siteid { get; set; }
    public string sitename { get; set; }

    public int campusid { get; set; }
    public string campusname { get; set; } 
        
        public int lotid { get; set; }
    public string lotname { get; set; }
    public int LotRateid { get; set; }
    public string Charge { get; set; }
    public static string NumberToText(long number)
    {
        StringBuilder wordNumber = new StringBuilder();

        string[] powers = new string[] { "Thousand ", "Million ", "Billion " };
        string[] tens = new string[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        string[] ones = new string[] { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", 
                                       "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };

        if (number == 0) { return "Zero"; }
        if (number < 0)
        {
            wordNumber.Append("Negative ");
            number = -number;
        }

        long[] groupedNumber = new long[] { 0, 0, 0, 0 };
        int groupIndex = 0;

        while (number > 0)
        {
            groupedNumber[groupIndex++] = number % 1000;
            number /= 1000;
        }

        for (int i = 3; i >= 0; i--)
        {
            long group = groupedNumber[i];

            if (group >= 100)
            {
                wordNumber.Append(ones[group / 100 - 1] + " Hundred ");
                group %= 100;

                if (group == 0 && i > 0)
                    wordNumber.Append(powers[i - 1]);
            }

            if (group >= 20)
            {
                if ((group % 10) != 0)
                    wordNumber.Append(tens[group / 10 - 2] + " " + ones[group % 10 - 1] + " ");
                else
                    wordNumber.Append(tens[group / 10 - 2] + " ");
            }
            else if (group > 0)
                wordNumber.Append(ones[group - 1] + " ");

            if (group != 0 && i > 0)
                wordNumber.Append(powers[i - 1]);
        }

        return wordNumber.ToString().Trim();
    }    
    public DbProviderFactory GetProvider()
    {
        providerName = "System.Data.SqlClient";        
        provider = DbProviderFactories.GetFactory(providerName);
        return provider;
    }

    public bool mail(string to, string msg, string subject)
    {
        bool i;
        try
        {
            SmtpClient send = new SmtpClient("smtp.gmail.com", 587);
            MailMessage mail = new MailMessage(new MailAddress("projectipass@gmail.com"), new MailAddress(to));
            mail.Subject = subject;
            mail.Body = msg;
            mail.IsBodyHtml = true;
            send.Credentials = new System.Net.NetworkCredential("projectipass@gmail.com", "ipass_2014");
            send.EnableSsl = true;
            send.Send(mail);

            return i = true;
        }
        catch
        {
            return i = false;
        }
    }
    public DataTable getoperator()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "findoperator");
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable countregistration()
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_graph", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "countregistration");
        
       

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable getlotsforediting(string name)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("lotsearch", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "search");

        adp.SelectCommand.Parameters.AddWithValue("@name", name);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    public DataTable deletelot(int lotid)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("lotsearch", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@action", "DELETE");

        adp.SelectCommand.Parameters.AddWithValue("@lotid", lotid);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }
    ///<Author>Shalu Madan</Author>
    /// <Date>24-03-07</Date>
    /// <summary>
    /// Function to Create a connection with the database.
    /// </summary>
    /// <returns>Boolean value depending upon the state of the connection</returns>
    public bool funcon()
    {
  // strcon = @"Data Source=70.38.37.199;Initial Catalog=dhschool5;Integrated Security=false;user=ashu;password=minix#123";
        
    //    strcon = @"Data Source=70.38.37.199;Initial Catalog=dbschool6v;Integrated Security=false;user=vivek;password=minix#1234";
        strcon = ConfigurationManager.ConnectionStrings["constr"].ToString();
    //   strcon = @"Data Source=.;Initial Catalog=db_IPASS;Integrated Security=true;";
       // strcon =@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\dbAavaniInvoice.mdf;Integrated Security=True;Connect Timeout=30";    
        
        conn = new SqlConnection(strcon);
         
            if (conn.State == ConnectionState.Closed)
                    conn.Open();
            return (true);
    }
    
    #region Utility Functions

    /// <summary>
    /// Function to Attach Parameters and their values to the command to be executed
    /// </summary>
    /// <param name="command">Command to be executed</param>
    /// <param name="parameterNames">Parameters to be attached to the command</param>
    /// <param name="parameterValues">Values of the parameters attached to the command</param>
    private void AttachParameters(DbCommand command, Hashtable hstParameters)
    {
        try
        {
            IEnumerator parameters = hstParameters.Keys.GetEnumerator();
                     
                while (parameters.MoveNext())
                {
                    DbParameter param = provider.CreateParameter();
                    param.ParameterName = "@" + parameters.Current;
                    param.Value = hstParameters[parameters.Current];
                    command.Parameters.Add(param);
                  }
      
        }
        catch
        {
            //HttpContext.Current.Response.Write("Error : " + exception.Message + " </br> Source : " + exception.Source);
            //System.Windows.Forms.MessageBox.Show(exception.Message + " Source = " + exception.Source);
        }
    }


    //public void ResetForm(Control frm)
    //{
    //    try
    //    {
    //        foreach (Control ctl in frm.Controls)
    //        {
    //            ResetForm(ctl);
    //            if (ctl.GetType().ToString() == typeof(TextBox).ToString())
    //            {
    //                ((TextBox)ctl).Text = string.Empty;
    //            }
    //            if (ctl.GetType().ToString() == typeof(CheckedListBox).ToString())
    //            {
    //                ((CheckedListBox)ctl).SelectedIndex = 0;
    //            }
    //            if (ctl.GetType().ToString() == typeof(ComboBox).ToString())
    //            {
    //                 ((ComboBox)ctl).SelectedIndex = -1;
    //            }
    //            if (ctl.GetType().ToString() == typeof(DateTimePicker).ToString())
    //            {
    //                ((DateTimePicker)ctl).Text = "";
    //            }
    //            if (ctl.GetType().ToString() == typeof(RichTextBox).ToString())
    //            {
    //                ((RichTextBox)ctl).Text = string.Empty;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    //    }
    //}

    // public void mOpenFile(string URL)
    //{
    //    Screen scr = Screen.PrimaryScreen;
    //    int oldWidth = scr.Bounds.Width;
    //    int oldHeight = scr.Bounds.Height;
    //    Process proc = new Process();
    //    proc.StartInfo.FileName = URL;
    //    // put full path in here
    //    proc.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
    //    proc.Start();
    //}

    /// <summary>
    /// Function to Attach Parameters to be passed to the database with Dataadapters
    /// </summary>
    /// <param name="command">Command to be created</param>
    /// <param name="Parameters">Parameters to be attached</param>

    public DbCommand AttachParameters(DbCommand command, Hashtable Parameters, String strCommand)
    {
        IEnumerator ienumParams = Parameters.GetEnumerator();
        while (ienumParams.MoveNext())
        {
            DbParameter param = provider.CreateParameter();
            param.ParameterName = "@" + ienumParams.Current;
            param.SourceColumn = Parameters[ienumParams.Current].ToString();
            param.SourceVersion = DataRowVersion.Original;
            command.Parameters.Add(param);
        }
        return command;
    }
    
    /// <summary>
    /// Function to Create DbCommands
    /// </summary>
    /// <param name="commandText">Query/ Stored Procedure to be executed</param>
    /// <param name="commandType">Type of Commands to be executed</param>
    public DbCommand CreateDbCommand(String commandText, CommandType commandType)
    {
        DbCommand command = provider.CreateCommand();
        try
        {
            command.Connection = conn;
            command.CommandText = commandText;
            command.CommandType = commandType;
        }
        catch (Exception exception)
        {
           // HttpContext.Current.Response.Write("Error : " + exception.Message + " </br> Source : " + exception.Source);
          
        }
        return command;
    }

    /// <summary>
    /// Function to create DbCommand to be executed
    /// </summary>
    /// <param name="hstCommands">
    /// Commands to be executed
    /// ex. hstCommands("SelectCommand", "Command")
    /// </param>
    /// <param name="hstCommandTypes">
    /// Command Type of each of the commands
    /// ex. hstCommandTypes("SelectCommand", CommandType.StoredProcedure)
    /// </param>
    private void CreateDbCommand(Hashtable hstCommands, Hashtable hstCommandTypes)
    {
        Hashtable hstDbCommands = new Hashtable();
        IEnumerator ienum = hstCommands.Keys.GetEnumerator();
        try
        {
            while (ienum.MoveNext())
            {
                DbCommand command = provider.CreateCommand();
                command.Connection = conn;
                command.CommandText = hstCommands[ienum.Current].ToString();
                command.CommandType = (CommandType)hstCommandTypes[ienum.Current];
                hstDbCommands.Add(ienum.Current, command);
            }
        }
        catch(Exception exception)
        {
          //  HttpContext.Current.Response.Write("Error : " + exception.Message + " </br> Source : " + exception.Source);
           
        }

    }
   
    public DbDataAdapter SetDataAdapter(String SelectCommand, String InsertCommand, String UpdateCommand, String DeleteCommand, Hashtable Parameters)
    {
        DbDataAdapter daResult = provider.CreateDataAdapter(); //new SqlDataAdapter(SelectCommand, conn);
        try
        {
            daResult.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            daResult.SelectCommand = (DbCommand)AttachParameters(CreateDbCommand(SelectCommand, CommandType.Text), Parameters, "");
            daResult.InsertCommand = (DbCommand)AttachParameters(CreateDbCommand(InsertCommand, CommandType.Text), Parameters, "");
            daResult.UpdateCommand = (DbCommand)AttachParameters(CreateDbCommand(UpdateCommand, CommandType.Text), Parameters, "");
            daResult.DeleteCommand = (DbCommand)AttachParameters(CreateDbCommand(DeleteCommand, CommandType.Text), Parameters, "");
            

         
            return daResult;
        }
        catch (Exception exception)
        {
           // HttpContext.Current.Response.Write("Error : " + exception.Message + " </br> Source : " + exception.Source);
        }
        return daResult;
    }

   
    #endregion Utility Functions


    #region Execute Dataset

    ///<Author>Shalu Madan</Author>
    /// <Date>24-03-07</Date>
    /// <summary>
    /// Function to get the required resultant into a dataset
    /// </summary>
    /// <returns>Dataset containing required resultant</returns>
    private DataSet GetDataSet( String commandText, CommandType commandType, Hashtable hstParameters , String tableName)
    {
        //try
        //{
           
            DbCommand command = (DbCommand)CreateDbCommand(commandText, commandType);

            if (hstParameters != null)
            {
                if (hstParameters.Count > 0)
                    AttachParameters(command, hstParameters);
            }

            using (DataSet dataset = new DataSet())
            using (DbDataAdapter adapter = provider.CreateDataAdapter())
            {
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                adapter.SelectCommand = command;
                if (tableName != "" && tableName != null )
                    adapter.Fill(dataset, tableName);
                else
                    adapter.Fill(dataset);
                return dataset;
            }
            
        //}
        //        catch (Exception exception)
        //{
           
        //}
    }

    public void ExecuteDataset(ref DataSet dsResult, Hashtable hstCommands, Hashtable hstCommandTypes, Actions action, String tableName, Hashtable hstParameters )
    {
    }

    
    
    ///<Author>Shalu Madan</Author>
    /// <Date>24-03-07</Date>
    /// <summary>
    /// Function to get the resultant dataset after execution of Query/Stored Procedure
    /// </summary>
    /// <param name="CommandText">Query/ Stored Procedure to be executed</param>
    /// <param name="CommandType">Command Type(query/stored procedure</param>
    /// <returns>Dataset containing required resultant after execution of Query or Stored Procedure</returns>
    public DataSet GetDataset(String commandText, CommandType commandType)
    {
        return GetDataSet(commandText, commandType, null, null);
    }

    public DataSet GetDataset(String commandText, CommandType commandType, String tableName)
   {
    return GetDataSet(commandText, commandType, null, tableName);
    }
    public bool GetRight(string strRight,string strUser)
    {
        if (!strUser.Contains("Admin"))
        {
            ds = GetDataset("select * from tbluser where User_UserName='" + strUser + "' and User_Rights like '%" + strRight + "%'", CommandType.Text, "dsRight");
            if (ds.Tables["dsRight"].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    ///<Author>Shalu Madan</Author>
    /// <Date>24-03-07</Date>
    /// <summary>
    /// Overload of GetDataset function for execution of queries with parameters
    /// </summary>
    /// <returns>Dataset containing required result set</returns>
    public DataSet GetDataset(String commandText, CommandType commandType, Hashtable hstParameters)
    {
        return GetDataSet(commandText, commandType, hstParameters, null);
    }


    /// <summary>
    /// Overloaded Function to fetch data from database
    /// </summary>
    /// <param name="commandText">Stored Procedure/Query to be executed</param>
    /// <param name="commandType">Command Type (Stroed Procedure / Query)</param>
    /// <param name="hstParameters">Parameters to be passed to the database</param>
    /// <param name="tableName">Name of the table into which data needs to be fetched</param>
    /// <returns>Dataset containing reuqired values</returns>
    public DataSet GetDataset(String commandText, CommandType commandType, Hashtable hstParameters, String tableName)
    {
        return GetDataSet(commandText, commandType, hstParameters, tableName);
    }
 

    #endregion Execute Dataset


    #region ExecuteReader

    /// <summary>
    /// Function to Execute the query/ stored procedure and return a data reader 
    /// </summary>
    /// <param name="CommandText">Comand to be executed ( Query/ Stored Procedure)</param>
    /// <param name="CommandType">Command Type ( Query/Stored Procedure) </param>
    /// <param name="hstParameters">Parameters to be passed with the Query/Stored Procedure to be executed</param>
    /// <returns>DataReader</returns>
    public DbDataReader GetDataReader(String commandText, CommandType commandType, Hashtable hstParameters)
    {
        if (conn.State.ToString().Equals("Closed"))
            conn.Open();
        DbCommand command = provider.CreateCommand();
        command.Connection = conn;
        command.CommandText = commandText;
        command.CommandType = commandType;


        if (hstParameters.Count != 0)
            AttachParameters(command, hstParameters);

        return command.ExecuteReader(CommandBehavior.CloseConnection);
    }

    ///<author>Vishnu Sharma</author>
    /// <date> 15-05-07 </date>
    /// <summary>
    /// Overloaded Function to fetch data from database
    /// </summary>
    /// <param name="commandText">Query to be executed</param>
    /// <param name="commandType">Command Type (Query)</param>
    /// <returns>DataReader</returns>

    public DbDataReader GetDataReader(String commandText, CommandType commandType)
    {
        if (conn.State.ToString().Equals("Closed"))
            conn.Open();
        DbCommand command = provider.CreateCommand();
        command.Connection = conn;
        command.CommandText = commandText;
        command.CommandType = commandType;

        return command.ExecuteReader(CommandBehavior.CloseConnection);

    }



    #endregion ExecuteReader


    #region ExecuteNonQuery

    /// <summary>
    /// Function to Execute NonQuery
    /// </summary>
    /// <param name="commandText">Query or Stored Procedure to be executed</param>
    /// <param name="commandType">Query/Stored Procedure</param>
    /// <param name="parameters">parameters to be passed along with the Query/Stored Procedure to be executed</param>
    /// <returns>no. of rows affected</returns>
    public Int32 ExecuteNonQuery(String commandText, CommandType commandType, Hashtable parameters)
    {
        DbCommand command = provider.CreateCommand();
        if (conn.State == ConnectionState.Closed)
            conn.Open();
        command.Connection = conn;
        command.CommandText = commandText;
        command.CommandType = commandType;
        if (parameters != null)
        {
            if (parameters.Count > 0)
                AttachParameters(command, parameters);
        }
        Int32 iRowsAffected = 0;
        try
        {
            iRowsAffected = command.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            if(ex.Number.ToString()=="2601")
            {
            //    MessageBox.Show("Canon't Insert Duplicate Record");
            }
            if (conn.State == ConnectionState.Open)
                conn.Close();
            return iRowsAffected;
        }
        if (conn.State == ConnectionState.Open)
            conn.Close();
        return iRowsAffected;
    }

    #endregion ExecuteNonQuery

   
   #region ExecuteScalar

    /// <summary>
    /// Function to Execute Scalar
    /// </summary>
    /// <param name="commandText">Query or Stored Procedure to be executed</param>
    /// <param name="commandType">Query/Stored Procedure</param>
    /// <param name="parameters">parameters to be passed along with the Query/Stored Procedure to be executed</param>
    /// <returns>The first column of the first row in the resultset returned by the Query</returns>
    public Object ExecuteScalar(String commandText, CommandType commandType, Hashtable parameters)
    {
        DbCommand command = provider.CreateCommand();
        if (conn.State == ConnectionState.Closed)
            conn.Open();
        command.Connection = conn;
        command.CommandText = commandText;
        command.CommandType = commandType;
        if (parameters != null)
        {
            if (parameters.Count > 0)
                AttachParameters(command, parameters);
        }
        Object result = command.ExecuteScalar();
        if (conn.State == ConnectionState.Open)
            conn.Close();
        return result;
    }

    #endregion ExecuteScalar

    public void CreateAdpt(DataSet ds, string qry,string tblMap)
    {
        DbDataAdapter adpt = provider.CreateDataAdapter();
        DbCommand cmd = provider.CreateCommand();
        DbCommandBuilder builder = provider.CreateCommandBuilder();
        
        if (conn.State == ConnectionState.Closed)
            conn.Open();       

        cmd.CommandType = CommandType.Text;
        cmd.CommandText=qry;
        cmd.Connection=conn;
        
        adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey;

        adpt.SelectCommand = cmd;     
        
        builder.DataAdapter=adpt;
        adpt.InsertCommand = builder.GetInsertCommand();
        adpt.UpdateCommand = builder.GetUpdateCommand();
        adpt.DeleteCommand = builder.GetDeleteCommand();

        adpt.Update(ds,tblMap);

    }

    //public string fnpath()
    //{
    //    string strcon;
    //    string path = System.Windows.Forms.Application.StartupPath.ToString();
    //    FileInfo fi = new FileInfo(path + "\\filepath.txt");
    //    StreamReader sr = fi.OpenText();
    //    strcon = sr.ReadToEnd();
    //    sr.Close();
    //    fi = null;

    //    return strcon;
    //}
    public DbDataAdapter ExecuteDataAdapter(DataSet ds ,String qry, String qry_Type, Hashtable parameters,String TableName)
    {
        if (conn.State == ConnectionState.Closed)
            conn.Open();
        
        DbDataAdapter adapter = provider.CreateDataAdapter();
        DbCommand cmd = provider.CreateCommand();

        IEnumerator ienumParams = parameters.Keys.GetEnumerator();

        while (ienumParams.MoveNext())
        {
            if (providerName == "System.Data.SqlClient")
            {
                DbParameter param1 = provider.CreateParameter();
                param1.ParameterName = "@" + ienumParams.Current;
                param1.Value = parameters[ienumParams.Current];
                cmd.Parameters.Add(param1);
            }
            else { }
        }

        cmd.CommandType = CommandType.Text;
        cmd.CommandText = qry;
        cmd.Connection = conn;

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

        if (qry_Type == "Insert")
        {
            adapter.InsertCommand = (DbCommand)cmd.ExecuteScalar();
        }
        else if (qry_Type == "Update")
        {
            adapter.UpdateCommand = (DbCommand)cmd.ExecuteScalar();
        }
        else if (qry_Type == "Delete")
        {
            adapter.DeleteCommand = (DbCommand)cmd.ExecuteScalar();
        }
        else
        {
            adapter.SelectCommand = (DbCommand)cmd.ExecuteScalar();
        }

         adapter.Update(ds,TableName);

         DbDataAdapter DT = provider.CreateDataAdapter();
         return DT;
        }
    }


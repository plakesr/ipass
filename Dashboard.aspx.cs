﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Net;
using System.Web.Script.Serialization;

public partial class Dashboard : System.Web.UI.Page
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //if (string.IsNullOrEmpty(ipAddress))
            //{
            //    ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            //}

            //string APIKey = "17bb829c281ef78ada7e87c94b61c5e96f205412853dca616c563c8f0b7aef71";
            //string url = string.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, ipAddress);
            //using (WebClient client = new WebClient())
            //{
            //    string json = client.DownloadString(url);
            //    Location1 location = new JavaScriptSerializer().Deserialize<Location1>(json);
            //    List<Location1> locations = new List<Location1>();
            //    location.times = DateTime.Now;

            //    hst.Clear();


            //    hst.Add("action", "insert");
            //    hst.Add("IPAddress", location.IPAddress);
            //    hst.Add("CountryName", location.CountryName);

            //    hst.Add("CountryCode", location.CountryCode);
            //    hst.Add("CityName", location.CityName);
            //    hst.Add("RegionName", location.RegionName);
            //    hst.Add("ZipCode", location.ZipCode);
            //    hst.Add("Latitude", location.Latitude);
            //    hst.Add("Longitude", location.Longitude);
            //    hst.Add("times", location.times);
            //    hst.Add("Username", Session["Login"].ToString());

            //    hst.Add("TimeZone", location.TimeZone);

            //    int result = objData.ExecuteNonQuery("[sp_loginhistory]", CommandType.StoredProcedure, hst);
            //    if (result > 0)
            //    {
            //        //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            //        //clear();
            //        //Response.Redirect("Admin_CityMaster.aspx");

            //    }
            //    else
            //    {
            //        // lbl_error.Visible = true;

            //    }

                if (Request.QueryString["message"] == "1")
                {
                    Session["datatable"] = null;
                    Session["datatable_random"] = null;
                    Session["datatable_reserved"] = null;
                    Session["dataaddress"] = null;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                }


           // }

        }
        catch
        {
            Response.Redirect("Default.aspx");
        }
    }
    

}

   

 public class Location1
{
    public string IPAddress { get; set; }
    public string CountryName { get; set; }
    public string CountryCode { get; set; }
    public string CityName { get; set; }
    public string RegionName { get; set; }
    public string ZipCode { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public string TimeZone { get; set; }
    public string username { get; set; }

    public DateTime times { get; set; }
}
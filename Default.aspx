﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Index" %>

<%@ Register src="WUC/wuc_index.ascx" tagname="wuc_index" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link href="stylesheet/font-awesome.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type='text/javascript' src="js/jquery.cookie.js"></script>
<script type='text/javascript' src="js/jquery.dcjqaccordion.2.7.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {
        $('#accordion-1').dcAccordion({
            eventType: 'click',
            autoClose: true,
            saveState: true,
            disableLink: true,
            speed: 'slow',
            showCount: false,
            autoExpand: true,
            cookie: 'dcjq-accordion-1',
            classExpand: 'dcjq-current-parent'
        });

    });
</script>
    <title></title>
   
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"> </script>
      <script type="text/javascript" src="js/script.js"> </script>
<style type="text/css">
#backgroundPopup { 
	z-index:1;
	position: fixed;
	display:none;
	height:100%;
	width:100%;
	background:#000000;	
	top:0px;  
	left:0px;
}
.toPopup {
	font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
    background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #ccc;
    border-radius: 3px 3px 3px 3px;
    color: #333333;
    display: none;
	font-size: 14px;
    left:60%;
    margin-left: -402px;
    position: fixed;
    top: 20%;
    width:400px;
    z-index: 2;
	padding:20px;
}
.toPopup input{ padding:5px 10px; margin-bottom:10px;float:left}
.toPopup label{ padding:5px 10px; margin-bottom:10px; width:137px; float:left}


div.loader {
    background: url("img/loading.gif") no-repeat scroll 0 0 transparent;
    height: 32px;
    width: 32px;
	display: none;
	z-index: 9999;
	top: 40%;
	left: 50%;
	position: absolute;
	margin-left: -10px;
}
div.close {
  background: url("img/closebox.png") no-repeat scroll 0 0 transparent;
  bottom: 38px;
  cursor: pointer;
  float: right;
  height: 30px;
  left: 39px;
  position: relative;
  width: 30px;
}
span.ecs_tooltip {
    background: none repeat scroll 0 0 #000000;
    border-radius: 2px 2px 2px 2px;
    color: #FFFFFF;
    display: none;
    font-size: 11px;
    height: 16px;
    opacity: 0.7;
    padding: 4px 3px 2px 5px;
    position: absolute;
    right: -62px;
    text-align: center;
    top: -51px;
    width: 93px;
}
span.arrow {
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 7px solid #000000;
    display: block;
    height: 1px;
    left: 40px;
    position: relative;
    top: 3px;
    width: 1px;
}
div#popup_content {
    margin: 4px 7px;
}
</style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:wuc_index ID="wuc_index1" runat="server" />
    
    </div>
    </form>
</body>
</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllParkersMaster.master" AutoEventWireup="true" CodeFile="Deshboard.aspx.cs" Inherits="Deshboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row dash-box">
    <div class="col-md-3 col-sm-6">
    <div class="green">
      <div class="detail-area"> NEW USER<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-user"></i></div>
      <div class="clearfix"></div>
      <div class="green-strip bottom-strip">44% Higher Than Yesterday</div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6">
    <div class="purple">
      <div class="detail-area"> SALES<br />
        <span>+50</span> </div>
      <div class="icon-area"><i class="fa fa-tags"></i></div>
      <div class="clearfix"></div>
      <div class="purple-strip bottom-strip">20% Higher Than Yesterday</div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6">
    <div class="light-red">
      <div class="detail-area">COMMENTS<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-comments"></i></div>
      <div class="clearfix"></div>
      <div class="light-red-strip bottom-strip">44% Higher Than Yesterday</div>
    </div>
    </div>
    <div class="col-md-3 col-sm-6">
    <div class="orange">
      <div class="detail-area">NEW ORDER<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-shopping-cart"></i></div>
      <div class="clearfix"></div>
      <div class="orange-strip bottom-strip">44% Higher Than Yesterday</div>
      </div>
    </div>

    <div class="col-md-3 col-sm-6">
    <div class="red">
      <div class="detail-area">Message<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-envelope"></i></div>
      <div class="clearfix"></div>
      <div class="red-strip bottom-strip">44% Higher Than Yesterday</div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6">
    <div class="blue">
      <div class="detail-area">PENDING ORDER<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-tasks"></i></div>
      <div class="clearfix"></div>
      <div class="blue-strip bottom-strip">44% Higher Than Yesterday</div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6">
    <div class="light-green">
      <div class="detail-area">UPDATE VISITOR<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-eye"></i></div>
      <div class="clearfix"></div>
      <div class="light-green-strip bottom-strip">44% Higher Than Yesterday</div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6">
    <div class="grey">
      <div class="detail-area">TOTAL PROFIT<br />
        <span>61</span> </div>
      <div class="icon-area"><i class="fa fa-external-link"></i></div>
      <div class="clearfix"></div>
      <div class="grey-strip bottom-strip">44% Higher Than Yesterday</div>
      </div>
    </div>
      </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div class="body-section">
    <h2>Messages :</h2>
    <img src="images/message.jpg" width="503" height="257" /></div>
  <div class="body-section">
    <h2>Overview :</h2>
    <img src="images/graph.jpg" width="341" height="258" /></div>
  
</asp:content>


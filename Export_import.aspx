﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Export_import.aspx.cs" Inherits="Export_import" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Webblgosforyou.com | Export Grid Data to Word, Excel, CSV, Pdf, Text File</title>
</head>
<body>
<form id="form1" runat="server">
<div>
<table>
<tr><td colspan="3">
<h4>Export Grid Data to Word, Excel, CSV, Pdf File Examples</h4>
</td></tr>
<tr><td colspan="3">
<asp:Button ID="btnExportToWord" runat="server" Text="ExportToWord" 
OnClick="btnExportToWord_Click" />&nbsp;&nbsp;
<asp:Button ID="btnExportToExcel" runat="server" Text="ExportToExcel" 
OnClick="btnExportToExcel_Click" />&nbsp;&nbsp;
<asp:Button ID="btnExportToCSV" runat="server" Text="ExportToCSV" 
OnClick="btnExportToCSV_Click" />&nbsp;&nbsp;
<asp:Button ID="btnExportToText" runat="server" Text="ExportToText" 
OnClick="btnExportToText_Click" />&nbsp;&nbsp;
<asp:Button ID="btnExportToPdf" runat="server" Text="ExportToPdf" 
OnClick="btnExportToPdf_Click" />
</td></tr>
<tr><td colspan="3">

    <%--<asp:GridView ID="grdResultDetails1" runat="server" AutoGenerateColumns="false"
PageSize="5"AllowPaging="true"
OnPageIndexChanging="grdResultDetails_PageIndexChanging">
<HeaderStyle BackColor="#9a9a9a" ForeColor="White" Font-Bold="true" Height="30" />
<PagerStyle HorizontalAlign="Center" />
<AlternatingRowStyle BackColor="#f5f5f5" />
<Columns>
<asp:BoundField DataField="SubjectName" HeaderText="SubjectName"
ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
<asp:BoundField DataField="Marks" HeaderText="Marks"
ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
<asp:BoundField DataField="Grade" HeaderText="Grade"
ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
</Columns>
    </asp:GridView>

<asp:GridView ID="grdResultDetails" runat="server" AutoGenerateColumns="false"
PageSize="5"AllowPaging="true"
OnPageIndexChanging="grdResultDetails_PageIndexChanging">
<HeaderStyle BackColor="#9a9a9a" ForeColor="White" Font-Bold="true" Height="30" />
<PagerStyle HorizontalAlign="Center" />
<AlternatingRowStyle BackColor="#f5f5f5" />
<Columns>
<asp:BoundField DataField="SubjectName" HeaderText="SubjectName"
ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
<asp:BoundField DataField="Marks" HeaderText="Marks"
ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
<asp:BoundField DataField="Grade" HeaderText="Grade"
ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
</Columns>
</asp:GridView>
--%>

  <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="SELECT * FROM [tbl_country]">
            </asp:SqlDataSource>
            <br />

<asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource1" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Country&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Country&nbsp;&nbsp;Abbreviation
                    </th>
                    <th>
                    Country &nbsp;&nbsp; Flag
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                    <%--<th width="5%">
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("Country_name")%>
                    </td><td>
                    <%#Eval("Country_Abbreviation")%>
                </td>
<td>
    <asp:Image ID="Image1" runat="server" ImageUrl='<%#"~/FlagImage/Resized/"+Eval("Country_flag") %>' />
                </td>
                <td>
                   <%-- <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Country_id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>--%>
                    
                </td>
                <td>
                <%--<asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("Country_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>--%>
                    </td>
                    <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Country_name") %>' 
                                        Visible="False" />
                                   
                    <asp:Label ID="idlabel2" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>
                                       <asp:Label ID="idlabel3" runat="server" Text='<%# Eval("Country_flag") %>' 
                                        Visible="False"></asp:Label>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>




</td></tr>
</table>
</div>
</form>
</body>
</html>
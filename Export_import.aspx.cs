﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
//using iTextSharp;
//using iTextSharp.text;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text.pdf;
using System.Data.SqlClient;
public partial class Export_import : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    private void ExportFile(string fileName, string contentType)
    {
        //disable paging to export all data and make sure to bind griddata before begin
       // grdResultDetails.AllowPaging = false;
      //  LoadGridData();
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}",
        fileName));
        Response.ContentType = contentType;
        StringWriter objSW = new StringWriter();
        HtmlTextWriter objTW = new HtmlTextWriter(objSW);
        rpt1.RenderControl(objTW);
        Response.Write(objSW);
        Response.End();
    }

    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void btnExportToText_Click(object sender, EventArgs e)
    {
//        string fileName = "ExportToText_" + DateTime.Now.ToShortDateString() + ".txt",
//contentType = "application/text";

//        //call 2nd export method with fileName and contentType
//        ExportTextBasedFile(fileName, contentType);
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        string fileName = "ExportToExcel_" + DateTime.Now.ToShortDateString() + ".xls",
contentType = "application/vnd.ms-excel";

        //call 1st export method with fileName and contentType
        ExportFile(fileName, contentType);
    }
    protected void btnExportToWord_Click(object sender, EventArgs e)
    {
        string fileName = "ExportToWord_" + DateTime.Now.ToShortDateString() + ".doc",
contentType = "application/ms-word";
        //call 1st export method with fileName and contentType
        ExportFile(fileName, contentType);
    }
    protected void btnExportToCSV_Click(object sender, EventArgs e)
    {
        string fileName = "ExportToCSV_" + DateTime.Now.ToShortDateString() + ".csv",
contentType = "application/text";

        //call 2nd export method with fileName and contentType
        ExportTextBasedFile(fileName, contentType);
    }
    protected void btnExportToPdf_Click(object sender, EventArgs e)
    {
        //grdResultDetails.AllowPaging = false;
        //LoadGridData();
        //string fileName = "ExportToPdf_" + DateTime.Now.ToShortDateString();
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}",
        //fileName + ".pdf"));
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter objSW = new StringWriter();
        //HtmlTextWriter objTW = new HtmlTextWriter(objSW);
        //grdResultDetails.RenderControl(objTW);
        //StringReader objSR = new StringReader(objSW.ToString());
        //Document objPDF = new Document(PageSize.A4, 100f, 100f, 100f, 100f);
        //HTMLWorker objHW = new HTMLWorker(objPDF);
        //PdfWriter.GetInstance(objPDF, Response.OutputStream);
        //objPDF.Open();
        //objHW.Parse(objSR);
        //objPDF.Close();
        //Response.Write(objPDF);
        //Response.End();
    }

    private void ExportTextBasedFile(string fileName, string contentType)
    {
        //disable paging to export all data and make sure to bind griddata before begin
       // grdResultDetails.AllowPaging = false;
        //LoadGridData();

        //Response.ClearContent();
        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}",
        //fileName));
        //Response.ContentType = contentType;
        //StringBuilder objSB = new StringBuilder();
        //for (int i = 0; i < rpt1.Items.Count; i++)
        //{
        //    objSB.Append(rpt1.Items[i].ItemType + ',');
        //}
        //objSB.Append("\n");
        //for (int j = 0; j < grdResultDetails.Rows.Count; j++)
        //{
        //    for (int k = 0; k < grdResultDetails.Columns.Count; k++)
        //    {
        //        objSB.Append(grdResultDetails.Rows[j].Cells[k].Text + ',');
        //    }
        //    objSB.Append("\n");
        //}
        //Response.Write(objSB.ToString());
        //Response.End();
    }
}
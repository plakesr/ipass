﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoiceGenerate.aspx.cs" Inherits="InvoiceGenerate" %>

<%@ Register src="WUC/wuc_invoice_generate.ascx" tagname="wuc_invoice_generate" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

  <head id="Head1" runat="server">
  <style type="text/css">
      html, body, #map-canvas {
        margin: 0px;
        padding: 0px
        }
    </style>
  <title>Print Page</title>
    <script type="text/javascript">
        function print_page() {
            var ButtonControl = document.getElementById("btnprint");
            ButtonControl.style.visibility = "hidden";
            window.print();
        }
    </script>

</head>
<body >

    <form id="form1" runat="server">
   
    
    <br />
    <br />
    <uc1:wuc_invoice_generate ID="wuc_invoice_generate1" runat="server" />
    <div class="invoice-table" style="border:none;">
  <input type="button" id="btnprint" value="Print this Page" onclick="print_page()" style="float:right" class="button"/>
  </div>
    </form>
    
</body>
</html>

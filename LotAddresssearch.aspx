﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LotAddresssearch.aspx.cs" Inherits="LotAddresssearch" %>

<%@ Register src="WUC/Wuc_lotaddress.ascx" tagname="Wuc_lotaddress" tagprefix="uc1" %>

<%@ Register src="WUC/wuc_CoupanSearch.ascx" tagname="wuc_CoupanSearch" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <style>
      html, body, #map-canvas {
        margin: 0px;
        padding: 0px
        }
    </style>
    
     
   
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script type="text/javascript">
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initialize() {
            // Create the autocomplete object, restricting the search
            // to geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(document.getElementById('Wuc_lotaddress1_autocomplete')),
      { types: ['geocode'] });
            // When the user selects an address from the dropdown,
            // populate the address fields in the form.
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                fillInAddress();
            });
        }

        // [START region_fillform]
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
        // [END region_fillform]

        // [START region_geolocation]
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
                    autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
                });
            }
        }
        // [END region_geolocation]
       
       </script>
    <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
</head>
<body onload="initialize()">
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
 <div class="body-container">
 <div class="left-slider"><img border="0" alt="Salesforce" src="images/lot-search-main.jpg" /></div>
  <div class="search-area"><form id="form1" runat="server">
    <div id="locationField" class="search-bg" >
    <label>Lot Search</label>
        <uc1:Wuc_lotaddress ID="Wuc_lotaddress1" runat="server" />
    <asp:Label ID="lblresult" runat="server" ForeColor="Red"></asp:Label>
    </div>
  
    <div class="search-bg">

        <label>Coupon Number</label>
        <uc2:wuc_CoupanSearch ID="wuc_CoupanSearch1" runat="server" />

        
      </div>

    </form></div>
  </div>
    </div>

    </div>
    <br /> &nbsp;   <br />
        <div class="footer-container"></div>
</body>
</html>
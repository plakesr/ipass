﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="LotMaster.aspx.cs" Inherits="LotMaster" %>

<%@ Register src="WUC/wuc_LotMaster.ascx" tagname="wuc_LotMaster" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">

    function IsOneDecimalPoint(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
        var parts = evt.srcElement.value.split('.');
        if (parts.length > 1 && charCode == 46)
            return false;
        return true;
    }
</script>



   <script>
       // JQUERY ".Class" SELECTOR.
       $(document).ready(function () {
           $(".groupOfTexbox").keypress(function (event) { return isNumber(event) });
       });

       // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
       function isNumber(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) &&
                (charCode < 48 || charCode > 57))
               return false;

           return true;
       }    
</script>
    <link href="style/main.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
      <script src="js/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/LotMap.js"></script>
  <div class="body-right"><h1>Lot
      </h1>
 <uc1:wuc_LotMaster ID="wuc_LotMaster1" 
          runat="server" />
       </div>
</asp:Content>


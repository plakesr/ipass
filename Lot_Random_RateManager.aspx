﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Lot_Random_RateManager.aspx.cs" Inherits="Lot_Random_RateManager" %>

<%@ Register src="WUC/wuc_randomrate.ascx" tagname="wuc_randomrate" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
 <link href="style/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    function IsOneDecimalPoint(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
        var parts = evt.srcElement.value.split('.');
        if (parts.length > 1 && charCode == 46)
            return false;
        return true;
    }
</script>



   <script>
       // JQUERY ".Class" SELECTOR.
       $(document).ready(function () {
           $(".groupOfTexbox").keypress(function (event) { return isNumber(event) });
       });

       // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
       function isNumber(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) &&
                (charCode < 48 || charCode > 57))
               return false;

           return true;
       }    
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:wuc_randomrate ID="wuc_randomrate1" runat="server" />
    
    </div>
    </form>
</body>
</html>

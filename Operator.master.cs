﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Operator : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Login"] != null)
        {

        }
        else
        {
            Response.Redirect("default.aspx");
        }


        try
        {
            if (!IsPostBack)
            {
                lbl_admin.Text = Session["Login"].ToString();
                

            }
        }
        catch
        {
            Response.Redirect("Default.aspx");
        }
    }
    protected void lnkbtn_Logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("Default.aspx");
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {
            Label redirect = Repeater1.Items[i].FindControl("idLabel") as Label;
             //<asp:Label ID="idLabel" runat="server" Text='<%# Eval("navigationurl") %>' Visible="false"></asp:Label>
             //       <asp:Label ID="lbladd" runat="server" Text='<%# Eval("Addpermssn") %>' Visible="false"></asp:Label>
             //       <asp:Label ID="lbledit" runat="server" Text='<%# Eval("Editpermssn") %>' Visible="false"></asp:Label>
             //       <asp:Label ID="lbldelete" runat="server" Text='<%# Eval("Deletepermssn") %>' Visible="false"></asp:Label>
             //       <asp:Label ID="lblread" runat="server" Text='<%# Eval("Readpermssn") %>' Visible="false"></asp:Label>
             //       <asp:Label ID="lblfull" runat="server" Text='<%# Eval("Fullpermssn") %>' Visible="false"></asp:Label>

            Label lbladd = Repeater1.Items[i].FindControl("lbladd") as Label;
            Label lbledit = Repeater1.Items[i].FindControl("lbledit") as Label;
            Label lbldelete = Repeater1.Items[i].FindControl("lbldelete") as Label;
            Label lblread = Repeater1.Items[i].FindControl("lblread") as Label;
            Label lblfull = Repeater1.Items[i].FindControl("lblfull") as Label;
            Session["lbladd"] = lbladd.Text;
            Session["lbledit"] = lbledit.Text;
            Session["lbldelete"] = lbldelete.Text;
            Session["lblread"] = lblread.Text;

            Session["lblfull"] = lblfull.Text;

            Response.Redirect(redirect.Text);
        }

    }
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {
            Label redirect = Repeater2.Items[i].FindControl("idLabel1") as Label;
            Label lbladd = Repeater2.Items[i].FindControl("lbladd1") as Label;
            Label lbledit = Repeater2.Items[i].FindControl("lbledit1") as Label;
            Label lbldelete = Repeater2.Items[i].FindControl("lbldelete1") as Label;
            Label lblread = Repeater2.Items[i].FindControl("lblread1") as Label;
            Label lblfull = Repeater2.Items[i].FindControl("lblfull1") as Label;
            Session["lbladd"] = lbladd.Text;
            Session["lbledit"] = lbledit.Text;
            Session["lbldelete"] = lbldelete.Text;
            Session["lblread"] = lblread.Text;

            Session["lblfull"] = lblfull.Text;
            Response.Redirect(redirect.Text);
        }

    }
    protected void Repeater3_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {
            Label redirect = Repeater3.Items[i].FindControl("idLabel2") as Label;


            Label lbladd = Repeater3.Items[i].FindControl("lbladd2") as Label;
            Label lbledit = Repeater3.Items[i].FindControl("lbledit2") as Label;
            Label lbldelete = Repeater3.Items[i].FindControl("lbldelete2") as Label;
            Label lblread = Repeater3.Items[i].FindControl("lblread2") as Label;
            Label lblfull = Repeater3.Items[i].FindControl("lblfull2") as Label;
            Session["lbladd"] = lbladd.Text;
            Session["lbledit"] = lbledit.Text;
            Session["lbldelete"] = lbldelete.Text;
            Session["lblread"] = lblread.Text;

            Session["lblfull"] = lblfull.Text;
            Response.Redirect(redirect.Text);
        }

    }
    protected void search_refund_Click(object sender, EventArgs e)
    {
        //if (Session["operatorid"].ToString() == "PM" || Session["operatorid"].ToString() == "Client Manager")
        //{

            Response.Redirect("Operator_Search_RefundableByParker.aspx");
       // }
       //if(Session["operatorid"].ToString() == "PM"||Session["operatorid"].ToString() == "PM")
       // {
       //     Response.Redirect("Operator_Search_Refundable.aspx");

       // }
    }
   
    protected void search_bill_Click(object sender, EventArgs e)
    {


        Response.Redirect("operator_search_bill.aspx");
       
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Refund_Request_For_Parker.aspx.cs" Inherits="Refund_Request_For_Parker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Refund Request</title>
    
    <%--<script src="Extension.min.js" type="text/javascript"></script>
    <link href="CSS.css" rel="stylesheet" type="text/css" />--%>
     <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
<link href="stylesheet/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
 

    <link href="style/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.8.2.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        var date = new Date();
        var currentMonth = date.getMonth();
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
        $('#txtbox_transcationdate').datepicker({
            maxDate: new Date(currentYear, currentMonth, currentDate)
        });
    });
</script>


   <script>
       // JQUERY ".Class" SELECTOR.
       $(document).ready(function () {
           $(".groupOfTexbox").keypress(function (event) { return isNumber(event) });
       });

       // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
       function isNumber(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) &&
                (charCode < 48 || charCode > 57))
               return false;

           return true;
       }    
</script>
 <style>
        .groupOfTexbox {border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
    margin-bottom: 0px;}
    
    .groupOfTexbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
     .style1
     {
         width: 149px;
     }
     .style4
     {
         width: 123px;
     }
     .style5
     {
         width: 125px;
     }
     .style6
     {
         width: 127px;
     }
     .style7
     {
         width: 131px;
     }
     .style8
     {
         width: 134px;
     }
     .style11
     {
         width: 150px;
     }
     .style16
     {
         width: 206px;
     }
     .style17
     {
         width: 124px;
     }
     .style19
     {
         width: 330px;
     }
     .style21
     {
         width: 151px;
     }
     .style22
     {
         width: 147px;
     }
     .style27
     {
         width: 201px;
     }
     .style28
     {
         width: 141px;
     }
     .style32
     {
         width: 3px;
     }
     .style34
     {
         width: 145px;
     }
     .style35
     {
         width: 216px;
     }
     .style37
     {
         width: 189px;
     }
     .style38
     {
         width: 186px;
     }
    </style>

     <script type="text/javascript">

         function IsOneDecimalPoint(evt) {
             var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
             var parts = evt.srcElement.value.split('.');
             if (parts.length > 1 && charCode == 46)
                 return false;
             return true;
         }
</script>
</head>

<body>
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
 <div class="body-container">
    <form id="form1" runat="server" >
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
        <asp:Panel ID="Panel1" runat="server">
        <fieldset><legend style="margin-left:-5px;">Refund Detail</legend>
        
        <table class="refund-table" cellpadding="0" cellspacing="0" >
            <tr>
                <td class="style1">
                    Enter Facility
                </td>
                <td class="style16">
                    <asp:TextBox ID="search_lot" runat="server" 
                        ontextchanged="search_lot_TextChanged" CssClass="twitterStyleTextbox"  ></asp:TextBox>
                   <cc1:AutoCompleteExtender ID="TextBox1_AutoCompleteExtender" runat="server" TargetControlID="search_lot"
       MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1" ServiceMethod="GetCountries">
    </cc1:AutoCompleteExtender>
                </td>
                <td class="style17">
                    <asp:RequiredFieldValidator ID="rfv_facility" runat="server" 
                        ControlToValidate="search_lot" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style38">
                    &nbsp;</td>
                <td class="style34">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    Paystation #</td>
                <td class="style16">
                     <asp:TextBox ID="txtbox_machine" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                    <td class="style17">
                        <asp:RequiredFieldValidator ID="rfv_machine0" runat="server" 
                            ControlToValidate="txtbox_machine" ErrorMessage="*" ForeColor="#CC0000" 
                            ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style38">
                    Receipt #
                </td>
                <td class="style34">
                    <asp:TextBox ID="txtbox_ticketno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    
                </td>
                <td>
                <asp:RequiredFieldValidator ID="rfv_ticketno" runat="server" 
                        ControlToValidate="txtbox_ticketno" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            </table>
            
                   <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table  class="refund-table" cellpadding="0" cellspacing="0">

            <tr>
     
                <td class="style1">
                    Amount Paid</td>
                <td class="style16">
                      
                    <asp:TextBox ID="txtbox_givenamopunt" runat="server" 
                       CssClass="groupOfTexbox" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom"
  ValidChars="01234567890." TargetControlID="txtbox_givenamopunt"></cc1:FilteredTextBoxExtender><br />
                    
                   </td>
                   <td width="50" class="style17">
                       <asp:RequiredFieldValidator ID="rfv_totalamountgiven0" runat="server" 
                           ControlToValidate="txtbox_ticketno" ErrorMessage="*" ForeColor="#CC0000" 
                           ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style37">
                    Amount Requested To Be Refunded #</td>
                <td class="style28">
                     <asp:TextBox ID="txtbox_refundamount" runat="server" CssClass="twitterStyleTextbox" 
                        ontextchanged="txtbox_refundamount_TextChanged" AutoPostBack="true" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom"
  ValidChars="01234567890." TargetControlID="txtbox_refundamount"></cc1:FilteredTextBoxExtender><br />
                   
                    </td>
                    <td class="style35">
                     <asp:RequiredFieldValidator ID="rfv_refundamount" runat="server" 
                        ControlToValidate="txtbox_refundamount" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblerr" runat="server" Text="" Visible="false"></asp:Label>
                    </td>

 


            </tr>
            </table>
       
            </ContentTemplate>
                     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="txtbox_refundamount" EventName="TextChanged" />
                </Triggers>
   </asp:UpdatePanel>
   <table  class="refund-table" cellpadding="0" cellspacing="0">
            <tr>
                <td class="style11">
                    Date Of Transaction #</td>
                <td class="style16">
                    <asp:TextBox ID="txtbox_transcationdate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <%--<cc1:CalendarExtender ID="txtbox_transcationdate_CalendarExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_transcationdate">
                    </cc1:CalendarExtender>--%>
                </td>
                    <td class="style4">
                        <asp:RequiredFieldValidator ID="rfv_date0" runat="server" 
                            ControlToValidate="txtbox_transcationdate" ErrorMessage="*" ForeColor="#CC0000" 
                            ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style37">
                    Attach Original Receipt</td>
                <td class="style27">
                     <asp:FileUpload ID="originalrecepit" runat="server" /><br />
             </td>
                    <td>
                       <asp:RequiredFieldValidator ID="rfv_attachoriginal" runat="server" 
                    ControlToValidate="originalrecepit" ErrorMessage="*Upload Original Receipt" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                    </td>
            </tr>
            </table>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="refund-table" cellpadding="0" cellspacing="0">
          
              <tr>

                <td class="style21">
                    Method Of Payment</td>
  
                <td class="style19">
              
                    <asp:RadioButton ID="rbt_cash" runat="server" AutoPostBack="true" GroupName="r" 
                    oncheckedchanged="rbt_cash_CheckedChanged" Text="CASH" />
                &nbsp;
                <asp:RadioButton ID="rbt_debit" runat="server" AutoPostBack="true" 
                    GroupName="r" oncheckedchanged="rbt_debit_CheckedChanged" Text="DEBIT" />
                &nbsp;
                <asp:RadioButton ID="rbt_credit" runat="server" AutoPostBack="true" 
                    GroupName="r" oncheckedchanged="rbt_credit_CheckedChanged" Text="CREDIT" />
       
                    
                    
                    </td>

        

                  
                <td class="style37">
                    <asp:Label ID="lbl_credit_authorization" runat="server" 
                    Text="Credit Card Authorization Code" Visible="False"></asp:Label></td>
                   
                <td class="style6">
                   <asp:TextBox ID="txtbox_authorisation" runat="server" 
                    CssClass="twitterStyleTextbox" Visible="False" 
                        ontextchanged="txtbox_authorisation_TextChanged" AutoPostBack="true" ></asp:TextBox>
                    
         
                    
                    </td>
                     <td class="style37">
                    <asp:Label ID="lbl_cc" runat="server" 
                    Text="Credit Card Digit#" Visible="False"></asp:Label></td>
                   
                <td class="style6">
                   <asp:TextBox ID="txtbox_cc" runat="server" 
                    CssClass="twitterStyleTextbox" Visible="False" 
                        AutoPostBack="true" ontextchanged="txtbox_cc_TextChanged" MaxLength='4' ></asp:TextBox>
                    
         
                    
                    </td>
            </tr>
          
            </table>
            
        </ContentTemplate>
                     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rbt_credit" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="txtbox_authorisation" EventName="TextChanged" />

                </Triggers>
   </asp:UpdatePanel>
            
            
             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table class="form-table44" cellpadding="0" cellspacing="0">
            <tr>
       <td class="style22">
           Reason For Refund Request</td>
       <td class="style32" >
                 <asp:DropDownList ID="ddl_reason" runat="server" AutoPostBack="True"  CssClass="twitterStyleTextbox"
                    onselectedindexchanged="ddl_reason_SelectedIndexChanged">
                     <asp:ListItem> Appointment Cancelled</asp:ListItem>
                     <asp:ListItem> Bought Mistakenly wrong pass</asp:ListItem>
                       <asp:ListItem> Buying multiple tickets at pay and display</asp:ListItem>
                        <asp:ListItem> Holiday rate not in effect / Programming error i.e.Holiday rate not programmed</asp:ListItem>
                         <asp:ListItem> Paid an old Ticket</asp:ListItem>

                    <asp:ListItem> Patient Passed away / Discharged / Staff no longer work</asp:ListItem>
                    <asp:ListItem> P&amp;D Machines -Parker wrongly usese Max Button</asp:ListItem>
                       <asp:ListItem>Paystation Short changes with or with out credit note, hopper/dispenser error</asp:ListItem>
                  
                    <asp:ListItem>Using Credit Cards on way in but not on way out</asp:ListItem>
                   
                 
                    
                    <asp:ListItem>Visitor wrongly purchased multi-use pass, but wanted to buy short term ticket</asp:ListItem>
                   
                    <asp:ListItem>Waited the pay station to be serviced then went over time limit</asp:ListItem>
                   
                    <asp:ListItem>Other</asp:ListItem>
                </asp:DropDownList>
       
           </td>
          
           <td >
       
              <asp:TextBox ID="txtbox_reason" runat="server"   CssClass="twitterStyleTextbox" 
                   Height="34px" Width="210px"
                    style=" resize:none; margin-left: 100px;" TextMode="MultiLine" 
                   Visible="False" ></asp:TextBox>
                     <cc1:TextBoxWatermarkExtender ID="txtbox_vehicleType_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_reason" 
                        WatermarkCssClass="watermark" WatermarkText="Enter Reason">
                    </cc1:TextBoxWatermarkExtender>
       
           </td>
           <td></td>
       </tr>
        
        </table>
                                                        </ContentTemplate>
                     <Triggers>
      
        <asp:AsyncPostBackTrigger ControlID="ddl_reason" EventName="SelectedIndexChanged" />
       

    </Triggers>
                </asp:UpdatePanel>
        </fieldset>
        </asp:Panel>
        <br />
        <asp:Panel ID="Panel2" runat="server">
        <fieldset><legend>Personal Detail </legend>

        <table class="form-table">
            <tr>
                <td class="style5" width="150">
                    First Name</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_firstname" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="100">
                    <asp:RequiredFieldValidator ID="rfv_f_name0" runat="server" 
                        ControlToValidate="txtbox_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style8" width="150">
                    Last Name</td>
                <td>
                    <asp:TextBox ID="txtbox_lastname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv__l_name" runat="server" 
                        ControlToValidate="txtbox_lastname" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Apt/Suite#</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_apt" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="50">
                    
                </td>
                <td class="style8">
                    Street Address</td>
                <td>
                    <asp:TextBox ID="txtbox_address" runat="server" CssClass="twitterStyleTextbox" 
                        style=" resize:none;" TextMode="MultiLine" Height="34px" Width="222px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_address" runat="server" 
                        ControlToValidate="txtbox_address" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    City</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_city" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="50">
                    <asp:RequiredFieldValidator ID="rfv_city" runat="server" 
                        ControlToValidate="txtbox_city" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style8">
                    Prov</td>
                <td>
                    <asp:TextBox ID="txtbox_province" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_province" runat="server" 
                        ControlToValidate="txtbox_province" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Postal Code</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_postalcode" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="50">
                    <asp:RequiredFieldValidator ID="rfv_postalcoad" runat="server" 
                        ControlToValidate="txtbox_postalcode" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_postalcode" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator>

                </td>
                <td class="style8">
                    Email</td>
                <td>
                    <asp:TextBox ID="txtbox_email" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ErrorMessage="*Enter Correct Format" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ControlToValidate="txtbox_email" ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Phone#</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_cellnumber" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_cellnumber" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                </td>
                <td width="50">
                    <asp:RequiredFieldValidator ID="rfv_cellno0" runat="server" 
                        ControlToValidate="txtbox_cellnumber" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>


                        <asp:RegularExpressionValidator
                                                           ID="rgv_phone" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_cellnumber" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                </td>
                <td class="style8">
                    Licence Plate#</td>
                <td>
                    <asp:TextBox ID="txtbox_licence" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_licence" runat="server" 
                        ControlToValidate="txtbox_licence" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        </fieldset>        </asp:Panel>
           <table align="center">
       <tr>
       <td class="style5">
          
           <asp:Button ID="btn_send" runat="server" Text="Send Request" CssClass="button btn-center" 
               ValidationGroup="a" onclick="btn_send_Click" /></td>
       <td class="style4">
       <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" 
               CssClass="button btn-center" onclick="btn_Cancel_Click" />
           </td>
       </tr>
       </table>
    </form>
    </div>

    </div>

    </div>

        <div class="footer-container"></div>
</body>
</html>

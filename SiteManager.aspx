﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SiteManager.aspx.cs" Inherits="SiteManager" %>

<%@ Register src="WUC/wuc_SiteManger.ascx" tagname="wuc_SiteManger" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <title>Find Latitude and Longitude of Draggable Marker Google Maps API</title>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="js/mapUtility.js" type="text/javascript"></script>

</head>
<body onload="initialize();">
    <form id="form1" runat="server">
    <div>
    
        <uc1:wuc_SiteManger ID="wuc_SiteManger1" runat="server" />
    
    </div>
    </form>
</body>
</html>

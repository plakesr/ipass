﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StateManager.aspx.cs" Inherits="StateManager" %>

<%@ Register src="WUC/wuc_statemaster.ascx" tagname="wuc_statemaster" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
  .Initial
  {
    display: block;
    padding: 4px 18px 4px 18px;
    float: left;
    background: url("../Images/InitialImage.png") no-repeat right top;
    color: Black;
    font-weight: bold;
  }
  .Initial:hover
  {
    color: White;
    background: url("../Images/SelectedButton.png") no-repeat right top;
  }
  .Clicked
  {
    float: left;
    display: block;
    background: url("../Images/SelectedButton.png") no-repeat right top;
    padding: 4px 18px 4px 18px;
    color: Black;
    font-weight: bold;
    color: White;
  }
  </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:wuc_statemaster ID="wuc_statemaster1" runat="server" />
    
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabTest.aspx.cs" Inherits="TabTest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <style>
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style1
    {
        width: 341px;
    }
    .style2
    {
        width: 360px;
    }
    .style4
    {
        width: 240px;
    }
    .style5
    {
        width: 248px;
    }
    .style6
    {
        width: 295px;
    }
    .form-table
    {
        width: 872px;
    }
    .style10
    {
        height: 62px;
    }
    .style11
    {
        width: 138px;
    }
    .style12
    {
        width: 159px;
    }
    .style13
    {
        width: 281px;
    }
    .style14
    {
        width: 283px;
    }
    .style15
    {
        width: 285px;
    }
    .style17
    {
        width: 304px;
    }
    .style19
    {
        width: 384px;
    }
    .style20
    {
        width: 382px;
    }
    .style21
    {
        width: 381px;
    }
    .style23
    {
        width: 321px;
    }
    .style27
    {
        width: 641px;
    }
    .style28
    {
        width: 485px;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
<div>
    <asp:Panel ID="Panel1" runat="server" BorderWidth="2px">
    
    <table>
      <tr>
            <td class="style28">Billing Entry</td><td class="style14"><asp:DropDownList ID="ddl_billingEntry" runat="server"><asp:ListItem Value="1">First Month Full</asp:ListItem><asp:ListItem Value="2">Prorate</asp:ListItem><asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem></asp:DropDownList></td>
      </tr> 
      <tr>
            <td class="style28">Payment Reminder Days</td><td class="style14"><asp:TextBox ID="txtbox_paymentreminderdays" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
            <td class="style28">Interest Rate Payment</td><td class="style14"><asp:TextBox ID="txtbox_interestrate" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
            <td class="style28">Late Fee($)</td><td class="style14"><asp:TextBox ID="txtbox_latefee" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
            <td class="style28">Activation Charge</td><td class="style14"><asp:TextBox ID="txtbox_Activation" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
            <td class="style28">Display Activation Charge</td><td class="style14"><asp:RadioButton ID="rbtn_yes" runat="server" GroupName="r"  oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" />&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rbtn_no" runat="server" GroupName="r" Text="No" /></td>
      </tr>
      <tr>
            <td class="style28">Display Deposit</td><td class="style14"><asp:RadioButton ID="rbtn_yes_deposite" runat="server" GroupName="r1" oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" />&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rbtn_no_deposite" runat="server" GroupName="r1" Text="No" /></td>
      </tr>
      <tr>
            <td class="style28">Late Payment Action</td><td class="style14"><asp:DropDownList ID="ddl_latepaymentaction" runat="server"><asp:ListItem Value="0">Disable Card</asp:ListItem><asp:ListItem Value="1">Send Email by Notification</asp:ListItem></asp:DropDownList></td>
      </tr>
      <tr>
        <td class="style28">Credit Card Payment Failed</td><td class="style14"><asp:DropDownList ID="ddl_creditcardpayment" runat="server"><asp:ListItem Value="0">Disable Card</asp:ListItem><asp:ListItem Value="1">Send Email by Notification</asp:ListItem></asp:DropDownList></td>
      </tr>
      <tr>
        <td class="style28">Transaction Type </td><td><asp:CheckBox ID="chk_monthly" runat="server" Text="Monthly" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox ID="chk_daily" runat="server" Text="Daily" /></td> 
      </tr>

    </table> 
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel2" runat="server" BorderWidth="2px">
  
    <table>
    <tr>
            <td class="style20"><asp:CheckBox ID="chbk_Reserved" runat="server" Text="Reserved"  /></td>
            <td class="style17">Total Space: <asp:TextBox ID="txtbox_reservedtotalspace" runat="server" Height="20px"  Width="91px"></asp:TextBox></td>
            <td class="style21">Unable Space: <asp:TextBox ID="txtbox_reservedunablespace" runat="server" Height="19px" Width="83px"></asp:TextBox></td>
            <td class="style19"><asp:Button ID="Rate_for_reserevd" runat="server" Text="Rate_reserved"  onclick="Rate_for_reserevd_Click" /></td>
    </tr>   
                                                
    <tr>
            <td class="style20"><asp:CheckBox ID="chbk_Random" runat="server"  Text="Random" /></td>
            <td class="style17">Total Space: <asp:TextBox ID="txtbox_randomtotalspace" runat="server" ontextchanged="TextBox2_TextChanged" Height="21px" Width="89px"></asp:TextBox></td>
            <td class="style21">Unable Space <asp:TextBox ID="txtbox_randomunablespace" runat="server" Height="20px"  Width="88px"></asp:TextBox></td>
            <td class="style19"><asp:Button ID="_Random" runat="server" Text="Rate_Random" onclick="_Random_Click" /></td>
   </tr>
    </table>
     </asp:Panel>
     <br />
    <asp:Panel ID="Panel3" runat="server" BorderWidth="2px">
   
     <table>
      <tr>
             <td colspan="3">TAXES</td></tr><tr><td class="style5"><span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Corporate Tax ID #<span class="Apple-converted-space">&nbsp;</span></span></td>
             <td class="style15"><asp:TextBox ID="txtbox_corporate_taxid" runat="server"></asp:TextBox></td>
             <td rowspan="5"><asp:DataList ID="dl_tax" runat="server" Width="629px" onselectedindexchanged="DataList1_SelectedIndexChanged" Height="181px"><FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" /><HeaderStyle BackColor="#333333" Font-Bold="True" Font-Size="Large" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" /><HeaderTemplate><b>Tax Name</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Tax(%)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Sequence</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Delete</b></HeaderTemplate><ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" /><ItemTemplate><asp:LinkButton ID="lnkDelete" CommandName="cmdDelete"  runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" /> </asp:LinkButton></ItemTemplate></asp:DataList></td>
      </tr>
      <tr>
              <td class="style5"><span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Tax Name</span></td>
              <td class="style15"><asp:TextBox ID="txtbox_taxname" runat="server"></asp:TextBox></td></tr><tr><td class="style5"><span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Tax %</span></td><td class="style15"><asp:TextBox ID="txtbox_tax" runat="server"></asp:TextBox></td></tr><tr><td class="style5"><span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">&#160;Sequence for Calculation</span></td>
              <td class="style15"><asp:DropDownList ID="ddl_squencecal" runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        </asp:DropDownList>
               </td>
        </tr>
        <tr>
                <td>&#160;</td>
                <td class="style15">
                    <asp:Button ID="btn_tax" runat="server" Text="ADD TAX"  CssClass="button" onclick="btn_tax_Click" />
                </td>
        </tr>

     </table>
    </asp:Panel>
    <asp:Button ID="btn_MainAdd" runat="server" onclick="btn_MainAdd_Click"  CssClass="button" Text="Next" Width="101px" Visible="False" />
    <asp:Button ID="btn_FINISH_FINAL" runat="server" Height="43px"  CssClass="button"  onclick="btn_FINISH_FINAL_Click" style="margin-top: 0px" Text="Finish"/>
</div>
    </form>
</body>
</html>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUserManager.ascx.cs" Inherits="WUC_EditUserManager" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
   
                    </cc1:ToolkitScriptManager>
                    <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style3
    {
        width: 101px;
    }
      .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
                      .tab-border{border-bottom:solid 2px #3970dd}
                      .form-button{ float:right; margin-top:20px;}
                    </style>

<table width="100%" align="center">
      <tr>
        <td>
         <div class="tab-border">
          <asp:Button Text="User Details" BorderStyle="None" ID="Tab1" 
                CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Lots Details" BorderStyle="None" 
                ID="Tab2" CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Menu To Access" BorderStyle="None" ID="Tab3" 
                CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
              </div>
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
              <table style="width: 100%;">
                <tr>
                  <td>
                    <h3>
                     

    <table width="100%">
    <tr>
    <td valign="top" width="60%">
    <table  class="form-table" width="100%">
        

            <tr>
                <td class="style3">
                    Name</td>
                <td>
                    <asp:TextBox ID="txtbox_Name" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ownername" runat="server" 
                        ControlToValidate="txtbox_Name" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                
            </tr>
            <tr>
                <td class="style3">
                    Adress1
                </td>
                <td>
                    <asp:TextBox ID="txtbox_adress" runat="server" ValidationGroup="a" TextMode="MultiLine" Height="70" style=" resize:none;" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_owneradress1" runat="server" 
                        ControlToValidate="txtbox_adress" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                 </tr>
            <tr>
                <td class="style3">
                    DOB</td>
                <td>
                    <asp:TextBox ID="txtbox_dob" runat="server" CssClass=twitterStyleTextbox  ></asp:TextBox>
                   
                    
                    <cc1:CalendarExtender ID="txtbox_owneradress2_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtbox_dob">
                    </cc1:CalendarExtender>
                   
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Cellular
                </td>
                <td>
                    <asp:TextBox ID="txtbox_Cellular" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_OCellular" runat="server" 
                        ControlToValidate="txtbox_Cellular" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td class="style3">
                    City</td>
                <td>
                    <asp:TextBox ID="txtbox_city" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ownercity" runat="server" 
                        ControlToValidate="txtbox_city" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                   Email</td>
                <td>
                    <asp:TextBox ID="txtbox_email" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_Oemail" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style3">
                    Designation</td>
                <td>
                    <asp:DropDownList ID="ddl_designation" runat="server" 
                        CssClass="twitterStyleTextbox" Height="36px" Width="237px">
                        <asp:ListItem>PM</asp:ListItem>
                        <asp:ListItem>Client Manager</asp:ListItem>
                        <asp:ListItem>Accounting</asp:ListItem>
                        <asp:ListItem>Data Center</asp:ListItem>
                        <asp:ListItem>PMO</asp:ListItem>
                    </asp:DropDownList>
                </td>
              
            </tr>

            <tr>
                <td class="style3">
                    Gender</td>
                <td>
                     <asp:RadioButton ID="rbMale" runat="server" GroupName="gender" Checked="true" />Male
                    <asp:RadioButton ID="rbFemale" runat="server"  GroupName="gender" />Female
                </td>
              
            </tr>

            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtbox_ownerzip" ErrorMessage="*Please Enter the Zipcode" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>--%>
                </td>
             
            </tr>
        </table>
    </td>
    <td valign="top">
              
                    <asp:Panel ID="Panel4" runat="server">
                  
    <fieldset >
    <legend>
        User Login Detail 
    </legend>
        <table class="style1" cellpadding="5">
            <tr>
                <td class="style3">
                    UserName</td>
                <td>
                    <asp:TextBox ID="txtbox_username" runat="server" ValidationGroup="b"  Width="150"
                        Enabled="false" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtbox_username" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="b"></asp:RequiredFieldValidator>
                </td>
            </tr>
            
            <tr>
                <td class="style3">
                     Password</td>
                <td>
                    <asp:TextBox ID="txtbox_pass" runat="server" ValidationGroup="b"  Height="24" Width="162"
                        CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtbox_pass" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="b"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
      <%-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" CssClass=button 
            ValidationGroup="b"></asp:Button>--%>
    </fieldset>
     </asp:Panel></td>
    </tr>
    </table>
        
  <div class="form-button">
<asp:Button ID="btn1_next" runat="server" Text="Next" onclick="btn1_next_Click" 
            ValidationGroup="a" CssClass=button></asp:Button>
   
    
    </div>

                    
                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View2" runat="server">
              <table width="100%" >
                <tr>
                  <td>
                    
                      
                        &nbsp;<asp:Panel ID="Panel1" runat="server">
                            <fieldset>
                                <legend>Alredy Autentication Lot </legend>
                                <asp:Repeater ID="Repeater3" runat="server" DataSourceID="SqlDataSource1">
                                <HeaderTemplate>
                                <div style="max-height:420px; overflow-y:auto;">
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        User Name
                    </th>
                     <th>
                        Lot Name
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
        <tr>
            <td>
                    <%#Eval("username")%>
                    </td><td>
                    <%#Eval("LotName")%>
                    

                </td>
        </ItemTemplate>
      
                                </asp:Repeater>
                                   <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                    SelectCommand="SELECT tbl_UserAuthenticatedlot.*,tbl_LotMaster.* FROM tbl_UserAuthenticatedlot INNER JOIN tbl_LotMaster ON tbl_UserAuthenticatedlot.lotid=tbl_LotMaster.Lot_id  WHERE ([username] = @username)">
                                       <SelectParameters>
                                           <asp:SessionParameter Name="username" SessionField="username" />
                                       </SelectParameters>
                                </asp:SqlDataSource>
                                   </fieldset>

                        </asp:Panel>
          
      
        <div style="clear:both">&nbsp;</div>
        <asp:Panel ID="Panel2" runat="server">
        <table><tr><td></td></tr></table>
         <fieldset><legend>Add Lots</legend>
         <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
            <table class="style1" width="100%">
                <tr>
                    <td class="style9">
                        &nbsp;Select Customer Name
                        </td>
                    <td class="style8">
                        <asp:DropDownList ID="ddl_customername" runat="server" 
                           CssClass="twitterStyleTextbox" AutoPostBack="True" 
                            Width="244px" 
                            onselectedindexchanged="ddl_customername_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
                        </asp:SqlDataSource>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        Select Campus</td>
                    <td class="style8">
                        <asp:DropDownList ID="ddl_campus" runat="server" AutoPostBack="True" CssClass="twitterStyleTextbox" Width="244" 
                            onselectedindexchanged="ddl_campus_SelectedIndexChanged">
                        </asp:DropDownList>
                        
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT [CampusID], [Campus_Name] FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID) order by Campus_Name">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddl_customername" Name="SiteID" 
                                    PropertyName="SelectedValue" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        Select Lots</td>
                    <td class="style8">
                        &nbsp;<asp:CheckBox ID="CheckBox1" runat="server" 
                            oncheckedchanged="CheckBox1_CheckedChanged" Text="All" AutoPostBack="true" Visible="false"  />
                        <asp:CheckBoxList ID="chk_lots" runat="server">
                        </asp:CheckBoxList>
                    </td>
                    <td>
                    
                       </td>
                </tr>

                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td colspan="2">
                       <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                            Text="Authenticate Lots" CssClass="button" /></td>
                </tr>

                <tr>
                    <td class="style4" colspan="3">
                        <asp:Repeater ID="Repeater1" runat="server" 
                            onitemcommand="Repeater1_ItemCommand1" >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                       Lot Name
                    </th>
                    <th>
                       Remove
                    </th>
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("username")%>
                   
           <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="remove" 
                        CssClass="button">Remove</asp:LinkButton>
                    </td>
        </ItemTemplate>
       <FooterTemplate></table></FooterTemplate>
                </asp:Repeater></td>
                </tr>
            </table>
         </ContentTemplate>
        <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddl_customername" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddl_campus" EventName="SelectedIndexChanged" />
         <%--<asp:AsyncPostBackTrigger ControlID="CheckBox1" EventName="SelectedIndexChanged" /> --%>
        </Triggers>
        </asp:UpdatePanel>
         </fieldset></asp:Panel>
        <div class="form-button">
<asp:Button ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" 
            ValidationGroup="b" CssClass="button"></asp:Button>
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="btn_pre" runat="server" onclick="btn_pre_Click" CssClass="button" 
            Text="Previous" />
            </div>
    
          
                 

                       
                    </td>
                    </tr>                   
                     </table>
                     </div>
                 </asp:View>
            <asp:View ID="View3" runat="server">
               <table width="100%">
                <tr>
                  <td>
                   
                   &nbsp;<asp:Panel ID="Panel3" runat="server">
                            <fieldset>
                                <legend>Menu To access </legend>
                                <asp:Repeater ID="Repeater2" runat="server" 
                                    onitemcommand="Repeater2_ItemCommand">
                                    <HeaderTemplate>
                                    <div style="max-height:320px; overflow-y:auto;">
                                        <table cellpadding="5" cellspacing="0" class="timezone-table" width="100%">
                                            <tr>
                                                <th>
                                                    Select
                                                </th>
                                                <th>
                                                    Menu Name
                                                </th>
                                                <th>
                                                    Add
                                                </th>
                                                <th>
                                                    Edit
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                                <th>
                                                    Read
                                                </th>
                                                <th>
                                                    Full
                                                </th>
                                            </tr>
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chbkselect" runat="server" Checked='<%#Eval("access")%>' />
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("id")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <%#Eval("Menu")%>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkadd" runat="server" Checked='<%#Eval("add")%>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkedit" runat="server" Checked='<%#Eval("edit")%>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkdelete" runat="server" Checked='<%#Eval("delete")%>' />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="chbkread" runat="server" Checked='<%#Eval("read")%>' 
                                                    GroupName="chk" />
                                                <%--<asp:CheckBox ID="chbkread" runat="server"  />--%>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="chbkfull" runat="server" Checked='<%#Eval("full")%>' 
                                                    GroupName="chk" />
                                                <%--<asp:CheckBox ID="chbkfull" runat="server" Checked='<%#Eval("full")%>' />--%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <%-- <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                                    SelectCommand="SELECT * FROM [tbl_menus]"></asp:SqlDataSource>--%>
                                    <div class="form-button">
                                <asp:Button ID="Button2" runat="server" CssClass="button" 
                                    onclick="Button2_Click" Text="Finish" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               
                                <asp:Button ID="btn_cancel" runat="server" CssClass="button" 
                                    onclick="btn_cancel_Click" Text="Cancel" ValidationGroup="b" />
                            </fieldset>
                        </asp:Panel>
                        <asp:Button ID="btn_save" runat="server" CssClass="button" Height="39px" 
                            onclick="btn_save_Click" Text="Update" ValidationGroup="a" Visible="False" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <h3>
   



                    </h3>
                  </td>
                  </tr>
                <%--</tr>--%>
              </table>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>

 
   
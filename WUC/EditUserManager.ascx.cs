﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class WUC_EditUserManager : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    string stat;
    string userstat;
    string gender;
    string str;
    DataTable dtmenucheck = new DataTable();
    DataTable dtlot;
    DataTable dtmenusinsert;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            clsInsert onj = new clsInsert();
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
            Fillddlsites();
            this.PopulateMenus();
            DataSet dsdetails = onj.fetchrec("select * from tbl_menus where parentid!=''");
            DataSet db = new DataSet();
            
            
            DataTable dt = new DataTable("datatable");
            dt.Columns.Add("access", typeof(bool));
            dt.Columns.Add("id", typeof(int));
            dt.Columns.Add("Menu", typeof(string));
            dt.Columns.Add("add", typeof(bool));
            dt.Columns.Add("edit", typeof(bool));
            dt.Columns.Add("delete", typeof(bool));
            dt.Columns.Add("read", typeof(bool));
            dt.Columns.Add("full", typeof(bool));
            db.Tables.Add(dt );


            for (int i = 0; i < dsdetails.Tables[0].Rows.Count; i++)
            {

                DataRow dr = db.Tables[0].NewRow();

                DataSet dh = onj.fetchdatea("select * from tblUserMenu where username='" + Session["username"].ToString() + "' and Menuname=" + dsdetails.Tables[0].Rows[i]["id"].ToString());

                try
                {
                    if (dh.Tables[0].Rows.Count > 0)
                    {
                        dr[0] = "True";
                        dr[1] = int.Parse(dsdetails.Tables[0].Rows[i]["id"].ToString());
                        dr[2] = dsdetails.Tables[0].Rows[i]["menuname"].ToString();
                        dr[3] = bool.Parse(dh.Tables[0].Rows[0][3].ToString());
                        dr[4] = bool.Parse(dh.Tables[0].Rows[0][4].ToString());
                        dr[5] = bool.Parse(dh.Tables[0].Rows[0][5].ToString());
                        dr[6] = bool.Parse(dh.Tables[0].Rows[0][6].ToString());
                        dr[7] = bool.Parse(dh.Tables[0].Rows[0][7].ToString());
                    }
                    else
                    {
                        dr[0] = "false";
                        dr[1] = int.Parse(dsdetails.Tables[0].Rows[i]["id"].ToString());
                        dr[2] = dsdetails.Tables[0].Rows[i]["menuname"].ToString();
                        dr[3] = "False";
                        dr[4] = "False";
                        dr[5] = "False";
                        dr[6] = "False";
                        dr[7] = "False";
                    }
                }
                catch
               {
                   dr[0] = "false";
                   dr[1] = int.Parse(dsdetails.Tables[0].Rows[i]["id"].ToString());
                   dr[2] = dsdetails.Tables[0].Rows[i]["menuname"].ToString();
                   dr[3] = "False";
                   dr[4] = "False";
                   dr[5] = "False";
                   dr[6] = "False";
                   dr[7] = "False";
                
                }
            

                db.Tables[0].Rows.Add(dr);



            }

            Repeater2.DataSource = db.Tables[0];
            Repeater2.DataBind();
                




            txtbox_adress.Text = Session["Address"].ToString();
            txtbox_Cellular.Text = Session["Mob"].ToString();
            txtbox_city.Text = Session["city"].ToString();
            ddl_designation.SelectedItem.Text = Session["Designation"].ToString();
            txtbox_dob.Text = Session["Dob"].ToString();
            txtbox_email.Text = Session["Email"].ToString();
            txtbox_Name.Text = Session["Name"].ToString();
            txtbox_pass.Text = Session["Pass"].ToString();
            // txtbox_type.Text = Session["type"].ToString();
            txtbox_username.Text = Session["loginame"].ToString();
            if (Session["Gender"].ToString() == "male")
            {
                rbMale.Checked = true;
            }
            if (Session["Gender"].ToString() == "Female")
            {
                rbFemale.Checked = true;
            }


            dtmenuauth.Columns.Add("lotid", typeof(int));
            dtmenuauth.Columns.Add("username", typeof(string));


            Session["datatable"] = dtmenuauth;

            dtmenus.Columns.Add("menuid", typeof(int));
            dtmenus.Columns.Add("add", typeof(string));
            dtmenus.Columns.Add("edit", typeof(string));
            dtmenus.Columns.Add("delete", typeof(string));
            dtmenus.Columns.Add("read", typeof(string));
            dtmenus.Columns.Add("full", typeof(string));
            Session["datatablemenu"] = dtmenus;

        }
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        //try
        //{

        //    if (rbMale.Checked == true)
        //    {
        //        gender = "male";
        //    }
        //    if (rbFemale.Checked == true)
        //    {
        //        gender = "Female";
        //    }
        //    UpdateUser();





        //    dtlot = (DataTable)Session["datatable"];


        //    try
        //    {



        //        for (int j = 0; j < dtlot.Rows.Count; j++)
        //        {

        //            hst.Clear();

        //            hst.Add("action", "insert");

        //            hst.Add("lotid", Convert.ToInt32(dtlot.Rows[j]["lotid"].ToString()));

        //            hst.Add("status", stat);
        //            hst.Add("userid", txtbox_username.Text);

        //            int result = objData.ExecuteNonQuery("[sp_Authenticatedlots]", CommandType.StoredProcedure, hst);
        //            if (result > 0)
        //            {

        //                //Response.Redirect("UserManager.aspx");

        //            }
        //            else
        //            {
        //                // lbl_error.Visible = true;

        //            }
        //        }

        //    }
        //    catch
        //    {


        //    }


        //    dtmenusinsert = (DataTable)Session["datatablemenu"];


        //    try
        //    {
        //        for (int k = 0; k < dtmenusinsert.Rows.Count; k++)
        //        {

        //            hst.Clear();

        //            hst.Add("action", "insert");
        //            hst.Add("menuusername", txtbox_username.Text);
        //            hst.Add("menuname", Convert.ToInt32(dtmenusinsert.Rows[k]["menuid"].ToString()));

        //            hst.Add("addpermssn", dtmenusinsert.Rows[k]["add"].ToString());
        //            hst.Add("editpermssn", dtmenusinsert.Rows[k]["edit"].ToString());
        //            hst.Add("deletepermssn", dtmenusinsert.Rows[k]["delete"].ToString());

        //            hst.Add("readpermssn", dtmenusinsert.Rows[k]["read"].ToString());
        //            hst.Add("fullpermssn", dtmenusinsert.Rows[k]["full"].ToString());

        //            int result = objData.ExecuteNonQuery("[sp_usermenu]", CommandType.StoredProcedure, hst);
        //            if (result > 0)
        //            {

        //                //Response.Redirect("UserManager.aspx");

        //            }
        //            else
        //            {
        //                // lbl_error.Visible = true;

        //            }
        //        }

        //    }
        //    catch
        //    {
        //    }
        //    Session["datatablemenu"] = null;
        //    Session["datatable"] = null;
        //    Response.Redirect("Admin_UserPermissionMaster.aspx?mesg=1");
        //}
        //catch
        //{
        //}
    }
    private void PopulateMenus()
    {
        //using (SqlConnection conn = new SqlConnection())
        //{
        //    conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        //    using (SqlCommand cmd = new SqlCommand())
        //    {
        //        cmd.CommandText = "select * from tbl_menus";
        //        cmd.Connection = conn;
        //        conn.Open();
        //        using (SqlDataReader sdr = cmd.ExecuteReader())
        //        {
        //            while (sdr.Read())
        //            {
        //                ListItem item = new ListItem();
        //                item.Text = sdr["menuname"].ToString();
        //                item.Value = sdr["id"].ToString();
        //                //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
        //                chkmenu.Items.Add(item);
        //            }
        //        }
        //        conn.Close();
        //    }
        //}
    }

    DataTable dtmenus = new DataTable();
    public void UpdateUser()
    {
        try
        {
            hst.Clear();
            



            string user = Session["username"].ToString();
            hst.Add("action", "update");
            hst.Add("dusername", user);
            hst.Add("user", user);
            hst.Add("luser", user);
            hst.Add("name", txtbox_Name.Text);
            hst.Add("dob", Convert.ToDateTime(txtbox_dob.Text));
            hst.Add("email ", txtbox_email.Text);
            hst.Add("city", txtbox_city.Text);
            hst.Add("mob", txtbox_Cellular.Text);
            hst.Add("gender", gender.ToString());
            hst.Add("address", txtbox_adress.Text);
            hst.Add("designation", ddl_designation.SelectedItem.Text);
            hst.Add("pass", txtbox_pass.Text);
            hst.Add("operator_id", ddl_designation.SelectedItem.Text);
            hst.Add("modifiedby", "Admin");
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                

            }
            else
            {
               

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
    protected void btn1_next_Click(object sender, EventArgs e)
    {
        Tab2.CssClass = "Clicked";
        Tab1.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }

    protected void btn2_next_Click(object sender, EventArgs e)
    {

        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;
    }
    protected void Tab1_Click(object sender, EventArgs e)
    {

        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void Tab2_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
    protected void Tab3_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;
    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    DataTable dtmenuauth = new DataTable();
   
    clsInsert cs = new clsInsert();
    protected void Button1_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < chk_lots.Items.Count; i++)
        {

            if (chk_lots.Items[i].Selected)
            {

                try
                {
                    int ss = Convert.ToInt32(chk_lots.Items[i].Value);
                    string ss1 = chk_lots.Items[i].Text;
                    dtmenuauth = (DataTable)Session["datatable"];

                    DataRow dr = dtmenuauth.NewRow();
                    dr[0] = ss;
                    dr[1] = ss1;

                    clsInsert obj1 = new clsInsert();
                    DataSet dddds = obj1.fetchrec("select * from tbl_UserAuthenticatedlot where username ='" + Session["loginame"].ToString() + "' and lotid =" + ss);
                    if (dddds.Tables[0].Rows.Count > 0)
                    {

                    }
                    else
                    {

                        dtmenuauth.Rows.Add(dr);
                    }
                    RemoveDuplicateRows(dtmenuauth, "lotid");


                    Session["datatable"] = dtmenuauth;
                    if (dtmenuauth.Rows.Count > 0)
                    {

                        Repeater1.DataSource = dtmenuauth;
                        Repeater1.DataBind();
                    }
                    

                 
                }
                catch
                {
                    //lbl_error.Visible = true;

                }



            }
        }

    }
    protected void Repeater1_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "remove")
        {
            dtmenuauth = (DataTable)Session["datatable"];

            dtmenuauth.Rows.RemoveAt(e.Item.ItemIndex);

            Repeater1.DataSource = dtmenuauth;
            Repeater1.DataBind();

            Session["datatable"] = dtmenuauth;

        }
    }


    private void PopulateLots()
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_LotMaster where Campus_id=" + ddl_campus.SelectedValue;
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["LotName"].ToString();
                        item.Value = sdr["Lot_id"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_lots.Items.Add(item);
                    }
                }
                conn.Close();
            }
        }
    }

    string statusadd = "";
    string statusedit = "";
    string statusdelete = "";
    string statusread = "";
    string statusfull = "";
    protected void Button2_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < Repeater2.Items.Count; i++)
        {
            CheckBox chkselect = Repeater2.Items[i].FindControl("chbkselect") as CheckBox;
            if (chkselect.Checked == true)
            {
                Label menuid = Repeater2.Items[i].FindControl("lblid") as Label;

                CheckBox chkadd = Repeater2.Items[i].FindControl("chbkadd") as CheckBox;
                if (chkadd.Checked == true)
                {
                    statusadd = "true";
                }
                else
                {
                    statusadd = "false";
                }

                CheckBox chkedit = Repeater2.Items[i].FindControl("chbkedit") as CheckBox;
                if (chkedit.Checked == true)
                {
                    statusedit = "true";
                }
                else
                {
                    statusedit = "false";
                }

                CheckBox chkdelete = Repeater2.Items[i].FindControl("chbkdelete") as CheckBox;
                if (chkdelete.Checked == true)
                {
                    statusdelete = "true";
                }
                else
                {
                    statusdelete = "false";
                }




               
                CheckBox chkread = Repeater2.Items[i].FindControl("chbkread") as CheckBox;

                if (chkread.Checked == true)
                {
                    statusread = "true";
                    statusadd = "false";
                    statusedit = "false";
                    statusdelete = "false";
                    statusfull = "false";
                }
                else
                {
                    statusread = "false";
                }

                CheckBox chkfull = Repeater2.Items[i].FindControl("chbkfull") as CheckBox;
                if (chkfull.Checked == true)
                {
                    statusread = "true";
                    statusadd = "true";
                    statusdelete = "true";
                    statusedit = "true";

                    statusfull = "true";
                }
                else
                {
                    statusfull = "false";
                }





                //dtmenus.Columns.Add("menuid", typeof(int));
                //dtmenus.Columns.Add("add", typeof(string));
                //dtmenus.Columns.Add("edit", typeof(string));
                //dtmenus.Columns.Add("delete", typeof(string));
                //dtmenus.Columns.Add("read", typeof(string));
                //dtmenus.Columns.Add("full", typeof(string));

                dtmenus = (DataTable)Session["datatablemenu"];

                DataRow dr1 = dtmenus.NewRow();
                dr1[0] = Convert.ToInt32(menuid.Text);
                dr1[1] = statusadd;
                dr1[2] = statusedit;
                dr1[3] = statusdelete;
                dr1[4] = statusread;
                dr1[5] = statusfull;

                dtmenus.Rows.Add(dr1);
                Session["datatablemenu"] = dtmenus;
            }
        }
       // btn_save.Visible = true;
        try
        {

            if (rbMale.Checked == true)
            {
                gender = "male";
            }
            if (rbFemale.Checked == true)
            {
                gender = "Female";
            }
            UpdateUser();





            dtlot = (DataTable)Session["datatable"];


            try
            {



                for (int j = 0; j < dtlot.Rows.Count; j++)
                {

                    hst.Clear();

                    hst.Add("action", "insert");

                    hst.Add("lotid", Convert.ToInt32(dtlot.Rows[j]["lotid"].ToString()));

                    hst.Add("status", stat);
                    hst.Add("userid", txtbox_username.Text);

                    int result = objData.ExecuteNonQuery("[sp_Authenticatedlots]", CommandType.StoredProcedure, hst);
                    if (result > 0)
                    {

                        //Response.Redirect("UserManager.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }

            }
            catch
            {


            }


            dtmenusinsert = (DataTable)Session["datatablemenu"];


            try
            {
                for (int k = 0; k < dtmenusinsert.Rows.Count; k++)
                {

                    hst.Clear();

                    hst.Add("action", "insert");
                    hst.Add("menuusername", txtbox_username.Text);
                    hst.Add("menuname", Convert.ToInt32(dtmenusinsert.Rows[k]["menuid"].ToString()));

                    hst.Add("addpermssn", dtmenusinsert.Rows[k]["add"].ToString());
                    hst.Add("editpermssn", dtmenusinsert.Rows[k]["edit"].ToString());
                    hst.Add("deletepermssn", dtmenusinsert.Rows[k]["delete"].ToString());

                    hst.Add("readpermssn", dtmenusinsert.Rows[k]["read"].ToString());
                    hst.Add("fullpermssn", dtmenusinsert.Rows[k]["full"].ToString());

                    int result = objData.ExecuteNonQuery("[sp_usermenu]", CommandType.StoredProcedure, hst);
                    if (result > 0)
                    {

                        //Response.Redirect("UserManager.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }

            }
            catch
            {
            }
            Session["datatablemenu"] = null;
            Session["datatable"] = null;
            Response.Redirect("Admin_UserPermissionMaster.aspx?mesg=1");
        }
        catch
        {
        }
    }
    public void Fillddlsites()
    {

        List<clsData> lst = new clsInsert().GetSiteslist(0);
        if (lst.Count() > 0)
        {
            ddl_customername.DataSource = lst;
            ddl_customername.DataTextField = "SiteName";
            ddl_customername.DataValueField = "SiteId";
            ddl_customername.DataBind();
        }
        ddl_customername.Items.Insert(0, new ListItem("Select Sites", "0"));

    }
    public void Fillddlcampus(int siteid)
    {

        List<clsData> lst = new clsInsert().GetCampuslist(siteid);
        if (lst.Count() > 0)
        {
            ddl_campus.DataSource = lst;
            ddl_campus.DataTextField = "campusname";
            ddl_campus.DataValueField = "campusid";
            ddl_campus.DataBind();
        }
        ddl_campus.Items.Insert(0, new ListItem("Select Campus", "0"));

    }
    protected void ddl_customername_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Fillddlcampus(Convert.ToInt32(ddl_customername.SelectedValue));
        }
        catch
        {
        }
    }
    protected void ddl_campus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           
            chk_lots.Items.Clear();
            CheckBox1.Visible = true;
            CheckBox1.Checked = false;
            PopulateLots();
        }
        catch
        {
        }
    }
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void btn_pre_Click(object sender, EventArgs e)
    {

        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Admin_UserPermissionMaster.aspx");
    }
    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBox1.Checked == true)
        {
            bool b = (sender as CheckBox).Checked;
            for (int i = 0; i < chk_lots.Items.Count; i++)
            {
                chk_lots.Items[i].Selected = b;
            }
        }
    }
}
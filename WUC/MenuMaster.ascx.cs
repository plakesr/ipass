﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_MenuMaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {

            addMenu();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void clear()
    {
        txtBox_Menu.Text = "";
        
    }
    public void addMenu()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("menu", txtBox_Menu.Text);
            hst.Add("user", txtBox_Menu.Text);
            
            int result = objData.ExecuteNonQuery("[sp_Menu]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                Response.Redirect("MenuMaster.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }

}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserManager.ascx.cs" Inherits="WUC_UserManager" %>
 <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                    </cc1:ToolkitScriptManager>
<%-- <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />--%>

<style type="text/css">
.form-button{ float:right; margin-top:20px;}
</style>
<table width="100%" align="center" class="registration-area" >
      <tr>
        <td >
            
          <asp:Button Text="Operator Details" BorderStyle="None" ID="Tab1" 
                CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Operator Login Details" BorderStyle="None" 
                ID="Tab2" CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Menu Authentication" BorderStyle="None" ID="Tab3" 
                CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
              
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #b5daff; border-style: solid">
                <tr>
                  <td>
                    <h3>
                      <span>

                       <panel>
    <fieldset>
    <legend>
       Operator Details 
    </legend>
        <table  class="form-table">
        

            <tr>
                <td class="style3" width="150">
                    Name</td>
                <td>
                    <asp:TextBox ID="txtbox_Name" runat="server" ValidationGroup="a" 
                        CssClass=twitterStyleTextbox ontextchanged="txtbox_Name_TextChanged"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender0" 
                runat="server" Enabled="True" TargetControlID="txtbox_Name" WatermarkText="Enter Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_ownername" runat="server" 
                        ControlToValidate="txtbox_Name" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Adress1
                </td>
                <td>
                    <asp:TextBox ID="txtbox_adress" runat="server" ValidationGroup="a" TextMode="MultiLine" style="resize: none;" CssClass=twitterStyleTextbox></asp:TextBox>
                      <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                runat="server" Enabled="True" TargetControlID="txtbox_adress" WatermarkText="Enter Address" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_owneradress1" runat="server" 
                        ControlToValidate="txtbox_adress" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    DOB</td>
                <td>
                    <asp:TextBox ID="txtbox_dob" runat="server" CssClass=twitterStyleTextbox  ></asp:TextBox>
                     <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                runat="server" Enabled="True" TargetControlID="txtbox_dob" WatermarkText="Enter Date Of Birth" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    
                    <cc1:CalendarExtender ID="txtbox_owneradress2_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtbox_dob">
                    </cc1:CalendarExtender>
                   
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Cellular
                </td>
                <td>
                    <asp:TextBox ID="txtbox_Cellular" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                      <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" 
                runat="server" Enabled="True" TargetControlID="txtbox_Cellular" WatermarkText="Enter Cellular Number" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_OCellular" runat="server" 
                        ControlToValidate="txtbox_Cellular" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td class="style3">
                    City</td>
                <td>
                    <asp:TextBox ID="txtbox_city" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" 
                runat="server" Enabled="True" TargetControlID="txtbox_city" WatermarkText="Enter City" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_ownercity" runat="server" 
                        ControlToValidate="txtbox_city" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                   Email</td>
                <td>
                    <asp:TextBox ID="txtbox_email" runat="server" ValidationGroup="a" 
                        CssClass="twitterStyleTextbox" ></asp:TextBox>
                      <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" 
                runat="server" Enabled="True" TargetControlID="txtbox_email" WatermarkText="Enter E-Mail" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_Oemail" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                    <asp:Label ID="lbl_emailcheck" runat="server" Text="Label" Visible="false"></asp:Label>
                </td>
            </tr>

            <tr>
                <td class="style3">
                    Designation</td>
                <td>
                    <asp:DropDownList ID="ddl_designation" runat="server" Height="36px" 
                        Width="237px" CssClass="twitterStyleTextbox">
                         <asp:ListItem>Accounting</asp:ListItem>
                          <asp:ListItem>Client Manager</asp:ListItem>
                            <asp:ListItem>Data Center</asp:ListItem>
                        <asp:ListItem>PM</asp:ListItem>
                       
                       
                      
                        <asp:ListItem>PMO</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td class="style3">
                    Gender</td>
                <td>
                     <asp:RadioButton ID="rbMale" runat="server" GroupName="gender" Checked="true" />Male
                    <asp:RadioButton ID="rbFemale" runat="server"  GroupName="gender" />Female
                </td>
            </tr>

            
        </table>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn1_next" runat="server" Text="Next" onclick="btn1_next_Click" 
            ValidationGroup="a" CssClass=button></asp:Button>
        &nbsp;&nbsp;&nbsp;
    </fieldset>
    </panel>
    <asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource1" 
    onitemcommand="Repeater1_ItemCommand" >
<HeaderTemplate>
<div style="max-height:420px; overflow-y:auto;">
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        User Name
                    </th>
                     <th>
                        Name
                    </th>
                     <th>
                      Designation
                    </th>
                    <th>
                        Edit
                    </th>
                    <%--<th>
                      Delete
                    </th>--%>
                    <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
           <tr>
            <td>
                    <%#Eval("username")%>
                    </td><td>
                    <%#Eval("Name")%>
                    

                </td>
                <td><%#Eval("Designation")%></td>
               <td>
                   <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("username") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td> 
             <%--   <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("username") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>--%>
                    <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("username") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("username") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td>
                    
                    
                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("username") %>' 
                                        Visible="False" />
                     <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("Name") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="idLabel2" runat="server" Text='<%# Eval("Dob") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel3" runat="server" Text='<%# Eval("Email") %>' 
                                        Visible="False"></asp:Label>  
                      <asp:Label ID="idLabel4" runat="server" Text='<%# Eval("Mob") %>' 
                                        Visible="False" />
                    <asp:Label ID="idLabel5" runat="server" Text='<%# Eval("Gender") %>' 
                                        Visible="False"></asp:Label>
                    <asp:Label ID="idLabel6" runat="server" Text='<%# Eval("Address") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel7" runat="server" Text='<%# Eval("Designation") %>' 
                                        Visible="False"></asp:Label>    
                    <asp:Label ID="idLabel8" runat="server" Text='<%# Eval("Status") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel9" runat="server" Text='<%# Eval("city") %>' 
                                        Visible="False"></asp:Label>    
                    <asp:Label ID="idLabel10" runat="server" Text='<%# Eval("loginame") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel11" runat="server" Text='<%# Eval("Pass") %>' 
                                        Visible="False"></asp:Label>    
                    <asp:Label ID="idLabel12" runat="server" Text='<%# Eval("userstatus") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel13" runat="server" Text='<%# Eval("type") %>' 
                                        Visible="False"></asp:Label>                         

            </tr>
        </ItemTemplate>
        
        <FooterTemplate>
            </table>
            </div>
        </FooterTemplate>
</asp:Repeater>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
    SelectCommand="select tblUser.*,tbl_UserLogin.Username as loginame ,tbl_UserLogin.Pass,tbl_UserLogin.[status] as userstatus,tbl_UserLogin.[type] from tblUser
inner join tbl_UserLogin on tblUser.username=tbl_UserLogin.Username"></asp:SqlDataSource>


                      </span>
                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View2" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #b5daff; border-style: solid">
                <tr>
                  <td>
                    <h3>
                      
                       <panel>
    <fieldset>
    <legend>
        Operator Login Detail 
    </legend>
     <fieldset>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <ContentTemplate>
     
        <table class="form-table">
            <tr>
                <td class="style10" width="150">
                    UserName</td>
                <td>
                    <asp:TextBox ID="txtbox_username" runat="server" ValidationGroup="b" 
                        CssClass=twitterStyleTextbox ontextchanged="txtbox_username_TextChanged" AutoPostBack="true"></asp:TextBox>
                     <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" 
                runat="server" Enabled="True" TargetControlID="txtbox_username" WatermarkText="Enter Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_OPhome" runat="server" 
                        ControlToValidate="txtbox_username" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="b"></asp:RequiredFieldValidator>
                    <span>
                    <asp:Label ID="lbl_usercheck" runat="server" Visible="False"></asp:Label>
                    </span>
                </td>
            </tr>
            
            <tr>
                <td class="style10">
                     Password</td>
                <td>
                    <asp:TextBox ID="txtbox_pass" runat="server" ValidationGroup="b" CssClass="twitterStyleTextbox" 
                        ></asp:TextBox>
                     <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" 
                runat="server" Enabled="True" TargetControlID="txtbox_pass" WatermarkText="Enter Password" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_OPoffice" runat="server" 
                        ControlToValidate="txtbox_pass" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="b"></asp:RequiredFieldValidator>
                </td>
            </tr>
         
            
        </table>
        </ContentTemplate>
         <Triggers>
       
        <asp:AsyncPostBackTrigger ControlID="txtbox_username" EventName="TextChanged" />
       

    </Triggers>
     </asp:UpdatePanel>
        </fieldset>
        <asp:Panel ID="Panel1" runat="server">
        <fieldset><legend>Add Lots</legend>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
            <table class="style1" width="100%">
                <tr>
                    <td class="style9" width="150">
                        Select Customer Name
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT * FROM [tbl_SiteMaster] order by SiteName"></asp:SqlDataSource>
                    </td>
                    <td class="style8">
                        <asp:DropDownList ID="ddl_customername" runat="server"     
                            CssClass="twitterStyleTextbox" 
                            onselectedindexchanged="ddl_customername_SelectedIndexChanged" Width="244"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        Select Campus<asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT * FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID) order by Campus_Name">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddl_customername" Name="SiteID" 
                                    PropertyName="SelectedValue" Type="Int64" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                    <td class="style8">
                        <asp:DropDownList ID="ddl_campus" runat="server"  CssClass="twitterStyleTextbox" 
                            onselectedindexchanged="ddl_campus_SelectedIndexChanged" Width="244"
                            AutoPostBack="True">
                        </asp:DropDownList>
                        
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        Select Lots</td>
                    <td class="style8">
                   &nbsp;<asp:CheckBox ID="CheckBox1" runat="server" 
                            oncheckedchanged="CheckBox1_CheckedChanged" Text="All" AutoPostBack="true" Visible="false"  />
                        <asp:CheckBoxList ID="chk_lots" runat="server">
                        </asp:CheckBoxList>
                        
                    </td>
                    <td>
                    
                       </td>
                </tr>

                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td colspan="2" align="left"><br />
                       <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                            Text="Authenticate Lots" CssClass="button" /></td>
                </tr>

                <tr>
                    <td class="style4" colspan="3">
                        <asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand1" >
                   <HeaderTemplate><br />
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                       Lot Name
                    </th>
                    <th>
                       Remove
                    </th>
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("username")%>
                   
           <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="remove" CssClass="button">Remove</asp:LinkButton>
                    </td>
        </ItemTemplate>
                </asp:Repeater></td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddl_customername" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddl_campus" EventName="SelectedIndexChanged" />
         <%--<asp:AsyncPostBackTrigger ControlID="CheckBox1" EventName="SelectedIndexChanged" /> --%>
        </Triggers>
        </asp:UpdatePanel>
        </fieldset></asp:Panel>
      <div class="form-button">
       <asp:Button ID="btn_pre" runat="server" onclick="btn_pre_Click" CssClass=button 
            Text="Previous" />&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" 
            ValidationGroup="b" CssClass=button></asp:Button>
        
       
            </div>
    </fieldset>
    </panel>
                 </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View3" runat="server">
            <div style="max-height:420px; overflow-y:auto;">
              <table style="width: 100%; border-width: 1px; border-color: #b5daff; border-style: solid">
                <tr>
                  <td>
                    <h3>
                        &nbsp;<asp:Panel ID="Panel2" runat="server">
                            <fieldset>
                                <legend>Menu To Access </legend>
                                <asp:Repeater ID="Repeater2" runat="server" DataSourceID="SqlDataSource4" 
                                    onitemcommand="Repeater2_ItemCommand">
                                    <HeaderTemplate>
                                        <table cellpadding="5" cellspacing="0" class="timezone-table" width="100%">
                                            <tr>
                                                <th>
                                                    Select
                                                </th>
                                                <th>
                                                    Menu Name
                                                </th>
                                                <th>
                                                    Add
                                                </th>
                                                <th>
                                                    Edit
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                                <th>
                                                    Read
                                                </th>
                                                <th>
                                                    Full
                                                </th>
                                            </tr>
                                     
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chbkselect" runat="server" />
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("id")%>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <%#Eval("menuname")%>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkadd" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkedit" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkdelete" runat="server" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="chbkread" runat="server" GroupName="chk" />
                                                <%--  <asp:CheckBox ID="chbkread" runat="server" />--%>
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="chbkfull" runat="server" GroupName="chk" />
                                                <%-- <asp:CheckBox ID="chbkfull" runat="server" />--%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                                    SelectCommand="SELECT * FROM [tbl_menus] where parentid!=''">
                                </asp:SqlDataSource>
                                <br />
                                <asp:Button ID="Button2" runat="server" CssClass="button" 
                                    onclick="Button2_Click" Text="Ok" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <%--  <table>
    <tr>
                <td class="style3">
                    Menu</td>
                <td>
                   <asp:CheckBoxList ID="chkmenu" runat="server">
</asp:CheckBoxList>
        
                </td>
                
            </tr>

            <tr>
            <td>
           
        <asp:Button ID="btn_save" runat="server" Text="Submit" ValidationGroup="a" 
    onclick="btn_save_Click"  CssClass=button/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_pre1" runat="server" Text="Previous" ValidationGroup="b" onclick="btn_pre1_Click" CssClass=button
                    />
        
            </td>
            </tr>
            
    </table>--%>
                            </fieldset>
                        </asp:Panel>
                        </h3>
                        <h3>
                         <asp:Button ID="btn_pre1" runat="server" CssClass="button" 
                                onclick="btn_pre1_Click" Text="Previous" ValidationGroup="b" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btn_save" runat="server" CssClass="button" 
                                onclick="btn_save_Click" Text="Submit" ValidationGroup="a" />
                           
                        </h3>
        



                  </td>
                </tr>
              </table>
              </div>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>




   


   

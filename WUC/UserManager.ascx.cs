﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;

public partial class WUC_UserManager : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
         //   this.PopulateMenus();
            Fillddlsites();
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["mesg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }


              dt.Columns.Add("lotid", typeof(int));
                dt.Columns.Add("username", typeof(string));
               

                Session["datatable"] = dt;
                dtmenus.Columns.Add("menuid", typeof(int));
                dtmenus.Columns.Add("add", typeof(string));
                dtmenus.Columns.Add("edit", typeof(string));
                dtmenus.Columns.Add("delete", typeof(string));
                dtmenus.Columns.Add("read", typeof(string));
                dtmenus.Columns.Add("full", typeof(string));
                Session["datatablemenu"] = dtmenus;





        }
    }
    string stat="true";
    string userstat = "true";
    string gender;
    string str;
    DataTable dt = new DataTable();
    DataTable dtlot;
    DataTable dtmenus = new DataTable();
    DataTable dtmenusinsert;

    public void Fillddlsites()
    {

        List<clsData> lst = new clsInsert().GetSiteslist(0);
        if (lst.Count() > 0)
        {
            ddl_customername.DataSource = lst;
            ddl_customername.DataTextField = "SiteName";
            ddl_customername.DataValueField = "SiteId";
            ddl_customername.DataBind();
        }
        ddl_customername.Items.Insert(0, new ListItem("Select Sites", "0"));

    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
           
            if (rbMale.Checked == true)
            {
                gender = "male";
            }
            if (rbFemale.Checked == true)
            {
                gender = "Female";
            }

           
             

            dtlot = (DataTable)Session["datatable"];


            try
            {



                for (int j = 0; j < dtlot.Rows.Count; j++)
                {

                    hst.Clear();
                   
                    hst.Add("action","insert");

                    hst.Add("lotid", Convert.ToInt32(dtlot.Rows[j]["lotid"].ToString()));

                    hst.Add("status", stat);
                    hst.Add("userid",txtbox_username.Text);
                    
                    int result = objData.ExecuteNonQuery("[sp_Authenticatedlots]", CommandType.StoredProcedure, hst);
                    if (result > 0)
                    {

                        //Response.Redirect("UserManager.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }

            }
            catch
            {


            }

            dtmenusinsert = (DataTable)Session["datatablemenu"];
           

            try
            {
                for (int k = 0; k < dtmenusinsert.Rows.Count; k++)
                {
                    
                    hst.Clear();

                    hst.Add("action", "insert");
                    hst.Add("menuusername", txtbox_username.Text);
                    hst.Add("menuname", Convert.ToInt32(dtmenusinsert.Rows[k]["menuid"].ToString()));

                    hst.Add("addpermssn", dtmenusinsert.Rows[k]["add"].ToString());
                    hst.Add("editpermssn", dtmenusinsert.Rows[k]["edit"].ToString());
                    hst.Add("deletepermssn", dtmenusinsert.Rows[k]["delete"].ToString());

                    hst.Add("readpermssn", dtmenusinsert.Rows[k]["read"].ToString());
                    hst.Add("fullpermssn", dtmenusinsert.Rows[k]["full"].ToString());

                    int result = objData.ExecuteNonQuery("[sp_usermenu]", CommandType.StoredProcedure, hst);
                    if (result > 0)
                    {

                        //Response.Redirect("UserManager.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }

            }
            catch
            {
            }





            string msg = "1";
            addUser();
            Session["datatablemenu"] = null;
            Session["datatable"] = null;
            Response.Redirect("UserManager.aspx?message=" + msg);
           
            // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            //clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }

    public void addUser()
    {
        try
        {
            hst.Clear();

          
            hst.Add("action", "insert");
            hst.Add("username", txtbox_username.Text);
            hst.Add("name", txtbox_Name.Text);
            hst.Add("dob", Convert.ToDateTime(txtbox_dob.Text));
            hst.Add("email ", txtbox_email.Text);
            hst.Add("city", txtbox_city.Text);
            hst.Add("mob", txtbox_Cellular.Text);
            hst.Add("gender",gender.ToString() );
            hst.Add("address", txtbox_adress.Text);
            hst.Add("designation",ddl_designation.SelectedItem.Text);
            hst.Add("status",stat);
            hst.Add("operator_id", ddl_designation.SelectedItem.Text);
            hst.Add("modifiedby","admin");
            hst.Add("dateofchanging",DateTime.Now);


            hst.Add("userloginname", txtbox_username.Text);
            hst.Add("pass", Session["password"] .ToString());
            

            hst.Add("type", "operator");
            hst.Add("ustatus",userstat );
            hst.Add("dateofregis",DateTime.Now);
            
            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
    //private void PopulateMenus()
    //{
    //    using (SqlConnection conn = new SqlConnection())
    //    {
    //        conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
    //        using (SqlCommand cmd = new SqlCommand())
    //        {
    //            cmd.CommandText = "select * from tbl_menus";
    //            cmd.Connection = conn;
    //            conn.Open();
    //            using (SqlDataReader sdr = cmd.ExecuteReader())
    //            {
    //                while (sdr.Read())
    //                {
    //                    ListItem item = new ListItem();
    //                    item.Text = sdr["menuname"].ToString();
    //                    item.Value = sdr["id"].ToString();
    //                    //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
    //                    chkmenu.Items.Add(item);
    //                }
    //            }
    //            conn.Close();
    //        }
    //    }
    //}



    private void PopulateLots()
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_LotMaster where Campus_id=" + ddl_campus.SelectedValue;
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["LotName"].ToString();
                        item.Value = sdr["Lot_id"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_lots.Items.Add(item);
                    }
                }
                conn.Close();
            }
        }
    }






    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        //if (e.CommandName == "cmdDelete")
        //{
        //    string user = e.CommandArgument.ToString();

        //    delUser(user);
        //}
        if (e.CommandName == "cmdEdit")
        {
           Label l = rpt1.Items[i].FindControl("idLabel") as Label;
           Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

           Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
           Label l3 = rpt1.Items[i].FindControl("idLabel3") as Label;

           Label l4 = rpt1.Items[i].FindControl("idLabel4") as Label;
           Label l5 = rpt1.Items[i].FindControl("idLabel5") as Label;

           Label l6 = rpt1.Items[i].FindControl("idLabel6") as Label;
           Label l7 = rpt1.Items[i].FindControl("idLabel7") as Label;


           Label l8 = rpt1.Items[i].FindControl("idLabel8") as Label;
           Label l9 = rpt1.Items[i].FindControl("idLabel9") as Label;

           Label l10 = rpt1.Items[i].FindControl("idLabel10") as Label;
           Label l11 = rpt1.Items[i].FindControl("idLabel11") as Label;

        //    Label l12 = rpt1.Items[i].FindControl("idLabel12") as Label;
        //    Label l13 = rpt1.Items[i].FindControl("idLabel13") as Label;

            


           Session["username"] = l.Text;
           Session["Name"] = l1.Text;
           Session["Dob"] = l2.Text;
           Session["Email"] = l3.Text;

           Session["Mob"] = l4.Text;
           Session["Gender"] = l5.Text;
           Session["Address"] = l6.Text;
           Session["Designation"] = l7.Text;

           Session["Status"] = l8.Text;
           Session["city"] = l9.Text;
           Session["loginame"] = l10.Text;
           Session["Pass"] = l11.Text;

         //  Session["userstatus"] = l12.Text;
        //    Session["type"] = l13.Text;
           Response.Redirect("Admin_EditUserPermissionMaster.aspx");

        }
        if (e.CommandName == "cmdinactive")
        {
            string user = e.CommandArgument.ToString();

            updateuserstatus(user);
            
        }

        if (e.CommandName == "cmdactive")
        {
            string user = e.CommandArgument.ToString();

            activeuserstatus(user);

        }
    }
    string userstattus1 = "false";
    public void updateuserstatus(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "updatestatus");
            //hst.Add("ustatus", userstattus1);
            hst.Add("username", username);
            


            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }


    public void activeuserstatus(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "activestatus");
            //hst.Add("ustatus", userstattus1);
            hst.Add("username", username);



            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }

    public void delUser(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("userloginname", username);
            hst.Add("username", username);
            hst.Add("menuusername", username);


            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }
    protected void btn1_next_Click(object sender, EventArgs e)
    {

        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }

    protected void btn2_next_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        Session["password"] = txtbox_pass.Text;
        MainView.ActiveViewIndex = 2;
    }
    protected void Tab1_Click(object sender, EventArgs e)
    {

        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void Tab2_Click(object sender, EventArgs e)
    {
        Session["password"] = txtbox_pass.Text;
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
    protected void Tab3_Click(object sender, EventArgs e)
    {
        Session["password"] = txtbox_pass.Text;
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;
    }
    protected void btn_pre_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 0;
    }

    protected void btn_pre1_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 1;
    }
    clsInsert cs = new clsInsert();
    protected void txtbox_Name_TextChanged(object sender, EventArgs e)
    {
    }
    protected void txtbox_username_TextChanged(object sender, EventArgs e)
    {

        DataTable dt = new clsInsert().alreadyexist("usernamecheck", txtbox_username.Text.Trim());

        if (dt.Rows.Count > 0)
        {
            lbl_usercheck.Visible = true;
            lbl_usercheck.Text = "UserName already exist";
            txtbox_username.Text = "";
        }
        else
        {
            lbl_usercheck.Visible = false;
            txtbox_username.Text = txtbox_username.Text;
        }
    }
    //protected void txtbox_email_TextChanged(object sender, EventArgs e)
    //{
    //    DataTable dt = new clsInsert().alreadyexist("emailverify", txtbox_email.Text.Trim());

    //    if (dt.Rows.Count > 0)
    //    {
    //        lbl_emailcheck.Visible = true;
    //        lbl_emailcheck.Text = "email already exist";
    //        txtbox_email.Text = "";
    //    }
    //    else
    //    {
    //        lbl_emailcheck.Visible = false;
    //        txtbox_email.Text = txtbox_email.Text;
    //    }
    //}
    protected void ddl_campus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            chk_lots.Items.Clear();
            CheckBox1.Visible = true;
            CheckBox1.Checked = false;
            PopulateLots();
        }
        catch
        {
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < chk_lots.Items.Count; i++)
        {

            if (chk_lots.Items[i].Selected)
            {

                try
                {
                    int ss =Convert.ToInt32( chk_lots.Items[i].Value);
                   string ss1=chk_lots.Items[i].Text;
                dt = (DataTable)Session["datatable"];
                
                    DataRow dr = dt.NewRow();
                    dr[0] = ss;
                    dr[1] = ss1;
                    
                    dt.Rows.Add(dr);
                    RemoveDuplicateRows(dt, "lotid");
                    Session["datatable"] = dt;
                    if (dt.Rows.Count > 0)
                    {
                        
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();
                    }

                    //int result = objData.ExecuteNonQuery("[sp_usermenu]", CommandType.StoredProcedure, hst);
                    //if (result > 0)
                    //{

                    //    //Response.Redirect("UserManager.aspx");

                    //}
                    //else
                    //{
                    //    // lbl_error.Visible = true;

                    //}
                }
                catch
                {
                    //lbl_error.Visible = true;

                }



            }
        }

    }


    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    protected void Repeater1_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "remove")
        {
            dt =(DataTable )  Session["datatable"];

            dt.Rows.RemoveAt(e.Item.ItemIndex);

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Session["datatable"] = dt;

        }
    }
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    string statusadd = "";
    string statusedit = "";
    string statusdelete = "";
    string statusread = "";
    string statusfull = "";


    protected void Button2_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < Repeater2.Items.Count; i++)
        {
            CheckBox chkselect = Repeater2.Items[i].FindControl("chbkselect") as CheckBox;
            if (chkselect.Checked == true)
            {
                Label menuid = Repeater2.Items[i].FindControl("lblid") as Label;

                CheckBox chkadd = Repeater2.Items[i].FindControl("chbkadd") as CheckBox;
                if (chkadd.Checked == true)
                {
                    statusadd = "true";
                }
                else
                {
                    statusadd="false";
                }
                
                CheckBox chkedit = Repeater2.Items[i].FindControl("chbkedit") as CheckBox;
                if (chkedit.Checked == true)
                {
                    statusedit = "true";
                }
                else
                {
                    statusedit = "false";
                }


                CheckBox chkdelete = Repeater2.Items[i].FindControl("chbkdelete") as CheckBox;
                if (chkdelete.Checked == true)
                {
                    statusdelete = "true";
                }
                else
                {
                    statusdelete = "false";
                }

                CheckBox chkread = Repeater2.Items[i].FindControl("chbkread") as CheckBox;

                if (chkread.Checked == true)
                {
                    statusread = "true";
                    statusadd = "false";
                    statusedit = "false";
                    statusdelete = "false";
                    statusfull = "false";
                }
                else
                {
                    statusread = "false";
                }
                
                CheckBox chkfull = Repeater2.Items[i].FindControl("chbkfull") as CheckBox;
                if (chkfull.Checked == true)
                {
                    statusread = "true";
                    statusadd = "true";
                    statusdelete = "true";
                    statusedit = "true";
                    
                    statusfull = "true";
                }
                else
                {
                    statusfull = "false";
                }

                //dtmenus.Columns.Add("menuid", typeof(int));
                //dtmenus.Columns.Add("add", typeof(string));
                //dtmenus.Columns.Add("edit", typeof(string));
                //dtmenus.Columns.Add("delete", typeof(string));
                //dtmenus.Columns.Add("read", typeof(string));
                //dtmenus.Columns.Add("full", typeof(string));

                dtmenus = (DataTable)Session["datatablemenu"];

                DataRow dr1 = dtmenus.NewRow();
                dr1[0] =Convert.ToInt32(menuid.Text);
                dr1[1] = statusadd;
                dr1[2] = statusedit;
                dr1[3] = statusdelete;
                dr1[4] = statusread;
                dr1[5] = statusfull;

                dtmenus.Rows.Add(dr1);
                Session["datatablemenu"] = dtmenus;
            }
        }
    }


    public void Fillddlcampus(int siteid)
    {

        List<clsData> lst = new clsInsert().GetCampuslist(siteid);
        if (lst.Count() > 0)
        {
            ddl_campus.DataSource = lst;
            ddl_campus.DataTextField = "campusname";
            ddl_campus.DataValueField = "campusid";
            ddl_campus.DataBind();
        }
        ddl_campus.Items.Insert(0, new ListItem("Select Campus", "0"));

    }
    protected void txtbox_designation_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddl_customername_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Fillddlcampus(Convert.ToInt32(ddl_customername.SelectedValue));
        }
        catch
        {
        }
    }
    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBox1.Checked == true)
        {
            bool b = (sender as CheckBox).Checked;
            for (int i = 0; i < chk_lots.Items.Count; i++)
            {
                chk_lots.Items[i].Selected = b;
            }
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_CoupanMaster.ascx.cs" Inherits="WUC_Wuc_CoupanMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .style2
    {
        height: 23px;
    }
     .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    .style3
    {
        width: 93px;
    }
    .style4
    {
        height: 23px;
        width: 93px;
    }
</style>
<%--<script>
    // JQUERY ".Class" SELECTOR.
    $(document).ready(function () {
        $(".groupOfTexbox").keypress(function (event) { return isNumber(event) });
    });

    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) &&
                (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>
 <style>
        .groupOfTexbox {border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
    margin-bottom: 0px;}
    
    .groupOfTexbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    
    </style>--%>

    <script type="text/javascript">

        function IsOneDecimalPoint(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
            var parts = evt.srcElement.value.split('.');
            if (parts.length > 1 && charCode == 46)
                return false;
            return true;
        }
</script>
 

 

<%--<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Custom"
  ValidChars="01234567890." TargetControlID="TextBoxValue"></ajax:FilteredTextBoxExtender>

--%>


<asp:Panel ID="Panel1" runat="server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table class="form-table">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<tr>
<td>
    Customer Name</td>
<td>
    <asp:DropDownList ID="ddl_sitename" runat="server" AutoPostBack="True" 
        DataSourceID="SqlDataSource1" DataTextField="SiteName" 
        DataValueField="SiteId" CssClass="twitterStyleTextbox" 
        AppendDataBoundItems="true" ondatabinding="ddl_sitename_DataBinding" 
        ondatabound="ddl_sitename_SelectedIndexChanged" onunload="ddl_sitename_Unload" >
       
    </asp:DropDownList>
  
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
    </asp:SqlDataSource>
</td>
    <td class="style3">
        &nbsp;</td>
</tr>
<tr>
<td>
    Campus Name</td>
<td>
    <asp:DropDownList ID="ddl_campusname" runat="server"  AutoPostBack="True" 
        CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource2" 
        DataTextField="Campus_Name" DataValueField="CampusID" >
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID) order by Campus_Name">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddl_sitename" Name="SiteID" 
                PropertyName="SelectedValue" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>
    <%--<asp:DropDownList ID="ddl_campusname" runat="server" AutoPostBack="True" 
        DataSourceID="SqlDataSource2" DataTextField="Campus_Name" CssClass=twitterStyleTextbox 
        DataValueField="CampusID" AppendDataBoundItems="True" 
        ondatabound="ddl_campusname_SelectedIndexChanged" >
       
    </asp:DropDownList>--%>
   
    
</td>
    <td class="style3">
        &nbsp;</td>
</tr>
<tr>
<td>
    Lot Name</td>
<td>
    <asp:DropDownList ID="ddl_lotname" runat="server"  CssClass=twitterStyleTextbox 
        DataSourceID="SqlDataSource3" DataTextField="LotName" DataValueField="Lot_id">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_LotMaster] WHERE ([Campus_id] = @Campus_id) order by LotName">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddl_campusname" Name="Campus_id" 
                PropertyName="SelectedValue" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>
   <%-- <asp:DropDownList ID="ddl_lotname" runat="server" 
        DataSourceID="SqlDataSource3"  CssClass=twitterStyleTextbox
        DataTextField="LotName" DataValueField="Lot_id" 
        AppendDataBoundItems="true" 
        onselectedindexchanged="ddl_lotname_SelectedIndexChanged" >
      
    </asp:DropDownList>--%>
   
   
</td>
    <td class="style3">
        &nbsp;</td>
</tr>

<tr>
        <td>
            Coupon Type</td>
        <td>
            <asp:DropDownList ID="ddl_CoupanType" runat="server" 
                CssClass="twitterStyleTextbox" >
               <asp:ListItem Value="2">Random Parking</asp:ListItem>
                        <asp:ListItem Value="1">Reserved Parking</asp:ListItem>
                       
                        
                    </asp:DropDownList>
                   
        </td>
        <td class="style3">
           </td>
    </tr>
    <tr>
    
        <td class="style2">
            Coupon Series Name</td>
        <td class="style2">
            <asp:TextBox ID="txtbox_Coupanseries" runat="server" AutoPostBack="True" 
                ontextchanged="txtbox_Coupanseries_TextChanged" CssClass="twitterStyleTextbox" ValidationGroup="gp_country"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender0" 
                runat="server" Enabled="True" TargetControlID="txtbox_Coupanseries" WatermarkText="Enter Coupan Series Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:Label ID="lbl_rec" runat="server" ForeColor="Red" ></asp:Label>
        </td>
        <td class="style4">
             <asp:RequiredFieldValidator ID="rfv_cntryname" runat="server" 
                ControlToValidate="txtbox_Coupanseries" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator></td>
    </tr>
    
    <tr>
        <td>
            Card Quantity</td>
        <td>
            <asp:TextBox ID="txtbox_Quantity" runat="server" CssClass="twitterStyleTextbox" 
                ValidationGroup="gp_country" ontextchanged="txtbox_Quantity_TextChanged"></asp:TextBox>
            
            <cc1:FilteredTextBoxExtender ID="txtbox_Quantity_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_Quantity" FilterMode="ValidChars" FilterType="Numbers">
            </cc1:FilteredTextBoxExtender>
            
            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                runat="server" Enabled="True" TargetControlID="txtbox_Quantity" WatermarkText="Enter Quantity" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
        </td>
        <td class="style3">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="txtbox_Quantity" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td>
            Amount</td>
        <td>
            <asp:TextBox ID="txtbox_CoupanDiscount" runat="server" CssClass="twitterStyleTextbox" ValidationGroup="gp_country" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom"
  ValidChars="01234567890." TargetControlID="txtbox_CoupanDiscount"></cc1:FilteredTextBoxExtender>
            <cc1:TextBoxWatermarkExtender ID="txtbox_CoupanDiscount_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_CoupanDiscount" 
                WatermarkCssClass="watermark" WatermarkText="Enter Coupan Discount">
            </cc1:TextBoxWatermarkExtender>
        </td>
        <td class="style3">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="txtbox_CoupanDiscount" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td>
            Type</td>
        <td>
            <asp:DropDownList ID="ddlchargeby" runat="server" AutoPostBack="True"  onselectedindexchanged="DropDownList1_SelectedIndexChanged">


                        <asp:ListItem Value="0">Select</asp:ListItem>
                 
                       
                        <asp:ListItem Value="2">Daily</asp:ListItem>
                         <asp:ListItem Value="1">Hourly</asp:ListItem>
                        <asp:ListItem Value="3">Monthly</asp:ListItem>

                        <asp:ListItem Value="4">Weekly</asp:ListItem>

            </asp:DropDownList>
           
        </td>
        <td class="style3">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lbl_type" runat="server" Visible="False"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtbox_type" runat="server" Visible="False"></asp:TextBox>

            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                runat="server" Enabled="True" TargetControlID="txtbox_type" FilterMode="ValidChars" FilterType="Numbers" >
            </cc1:FilteredTextBoxExtender>
        </td>
        <td class="style3">
            <asp:RequiredFieldValidator ID="rfv_type" runat="server" 
                ControlToValidate="txtbox_type" ErrorMessage="*" ForeColor="#FF0066" 
                ValidationGroup="gp_country"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Start Date</td>
        <td>
            <asp:TextBox ID="txtbox_StartDate" runat="server" CssClass="disable_past_dates" ValidationGroup="gp_country"></asp:TextBox>
            <cc1:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtbox_StartDate">
            </cc1:CalendarExtender>
          <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" 
                runat="server" Enabled="True" TargetControlID="txtbox_StartDate" WatermarkText="Select Start Date" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>--%>
        </td>
        <td class="style3">
           <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="txtbox_StartDate" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator></td>
    </tr>
    
    
</table>
</ContentTemplate>
</asp:UpdatePanel>
 <asp:Button ID="btn_submit" runat="server" Text="Submit" CssClass="button"
                onclick="btn_submit_Click" ValidationGroup="gp_country" />

</asp:Panel>




<asp:Panel ID="Panel2" runat="server"  Height="300px" ScrollBars="Vertical">
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
           
            <br />
            
            <asp:Repeater runat="server" id="Coupan_Repeater" onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Coupon&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Coupon&nbsp;&nbsp;Quantity
                    </th>
                    <th>
                    Lot &nbsp;&nbsp; Name
                    </th>
                    <th>
                    Coupon Type
                    </th>
                    <th>
                   Start Date
                    </th>
                    
                    <th>
                    End Date
                    </th>

                   <%-- <th>
                    Type
                    </th>--%>
                    <th width="5%">
                        Edit
                    </th>
                    <th width="5%">
                    Inactive
                    </th>
                     <th width="5%">
                    Active
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("CoupanSeries")%>
                    </td><td>
                    <%#Eval("Quantity")%>
                </td>
<td>
    <%#Eval("LotName") %>
                </td>
                <td>
               <%#Eval("CoupanType") %>
                </td>
                <td>
               <%#Eval("StartDate")%>
                </td>
                <td>
               <%#Eval("EndDate")%>
                </td>
               <%-- <td>
                <%#Eval("Type")%>
                
                </td>--%>
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("CoupanSeries") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdInactive" CommandArgument='<%#Eval("CoupanSeries") %>' runat="server" OnClientClick="return confirmation();"><img src="images/inactive1.png" />
                    </asp:LinkButton>
                    <cc1:ConfirmButtonExtender ConfirmText="Are You sure?" TargetControlID="lnkDelete" ID="ConfirmButtonExtender1" runat="server">
                    </cc1:ConfirmButtonExtender>
                    </td>

                    <td>
                <asp:LinkButton ID="lnkActive" CommandName="cmdActive" CommandArgument='<%#Eval("CoupanSeries") %>' runat="server" OnClientClick="return confirmation();"><img src="images/active1.png" />
                    </asp:LinkButton>
                    <cc1:ConfirmButtonExtender ConfirmText="Are You sure?" TargetControlID="lnkDelete" ID="ConfirmButtonExtender2" runat="server">
                    </cc1:ConfirmButtonExtender>
                    </td>
                    <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Quantity") %>' 
                                        Visible="False" />
                                   
                    <asp:Label ID="Typeofcoupan" runat="server" Text='<%# Eval("Type") %>' 
                                        Visible="False"></asp:Label>
                                       <asp:Label ID="StartDate" runat="server" Text='<%# Eval("StartDate") %>' 
                                        Visible="False"></asp:Label>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_Wuc_CoupanMaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            binddataviews();

            if (Request.QueryString["msg"] == "true")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Card Updated succesfully.')", true);

            }
            else
            {
            }

        }
    }
    string isspecial = "True";

    public void binddataviews()
    {
        try
        {
            DataTable dr_record1 = new DataTable();
            dr_record1.Columns.Add("CoupanSeries", typeof(string));
            dr_record1.Columns.Add("Quantity", typeof(string));
            dr_record1.Columns.Add("CoupanType", typeof(string));
            dr_record1.Columns.Add("LotName", typeof(string));
            dr_record1.Columns.Add("CoupanCodeID", typeof(int));
            dr_record1.Columns.Add("StartDate", typeof(DateTime));
            dr_record1.Columns.Add("EndDate", typeof(DateTime));
            dr_record1.Columns.Add("Type", typeof(string));


            DataTable dtrecords = new clsInsert().AllCoupan();
            if (dtrecords.Rows.Count > 0)
            {
                RemoveDuplicateRows(dtrecords, "CoupanSeries");
                for (int i = 0; i < dtrecords.Rows.Count; i++)
                {
                    DataRow drnew = dr_record1.NewRow();
                    drnew[0] = dtrecords.Rows[i]["CoupanSeries"].ToString();
                    drnew[2] = dtrecords.Rows[i]["CoupanType"].ToString();
                    drnew[3] = dtrecords.Rows[i]["LotName"].ToString();
                    drnew[4] = Convert.ToInt32(dtrecords.Rows[i]["CoupanCodeID"].ToString());
                    drnew[5] = Convert.ToDateTime(dtrecords.Rows[i]["starting_date"].ToString());
                    drnew[6] = Convert.ToDateTime(dtrecords.Rows[i]["ending_date"].ToString());
                    if (dtrecords.Rows[i]["chargeby"].ToString() == "1")
                    {

                        drnew[7] = "Hourly";

                    }

                    if (dtrecords.Rows[i]["chargeby"].ToString() == "2")
                    {
                        drnew[7] = "Daily";

                    }


                    if (dtrecords.Rows[i]["chargeby"].ToString() == "3")
                    {
                        drnew[7] = "Monthly";

                    }

                    if (dtrecords.Rows[i]["chargeby"].ToString() == "4")
                    {
                        drnew[7] = "Weekly";

                    }
                    DataTable dtrt = new clsInsert().CoupanCount(dtrecords.Rows[i]["CoupanSeries"].ToString());
                    drnew[1] = dtrt.Rows[0]["Quantity"].ToString();
                    dr_record1.Rows.Add(drnew);
                }


                Coupan_Repeater.DataSource = dr_record1;
                Coupan_Repeater.DataBind();
            }
        }
        catch
        {
        }
    }

    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        //if (lbl_rec.Text == "1" &&  btn_submit.Text=="update")
        //{
        //   // deleteCoupan();
            
        //    updateCoupan();
        //    clear();

        //}
        //else if (lbl_rec.Text == "0" && btn_submit.Text == "Add New")
        
        //{

            try
            {
                if (ddlchargeby.SelectedItem.Text == "Select")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select Type')", true);

                }
                else
                {
                    addCoupan();
                    clear();
                    binddataviews();
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Card Created succesfully.')", true);
                }
            }
            catch
            {
            }
        //}
    }
    int number = 0;
    string stat = "true";
    
    public void addCoupan()
    {


        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("lotid", ddl_lotname.SelectedValue);
            hst.Add("parkingtype", ddl_CoupanType.SelectedItem.Value);
            hst.Add("amount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));
            if (txtbox_type.Text != "")
            {
                hst.Add("quantity", Convert.ToDecimal(txtbox_type.Text));
            }
            if (txtbox_type.Text == "")
            {
                decimal defaultvalue = Convert.ToDecimal(1.00);
                hst.Add("quantity", defaultvalue);

            }

            hst.Add("chargeby", ddlchargeby.SelectedValue);
            hst.Add("isspecial", isspecial);



            int result_lotratemaster = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
            if (result_lotratemaster > 0)
            {

            }
        }
        catch
        {

        }




        DataSet ds = new clsInsert().fetchrec("select top 1 LotRateid from tbl_LotRateMaster order by LotRateid desc");






        try
        {
             
            
            for (int i = 0; i < Convert.ToInt32(txtbox_Quantity.Text.ToString()); i++)
            {
                hst.Clear();
                number = number + 1;
                string coupan=txtbox_Coupanseries.Text+number.ToString();
                hst.Add("action", "insert");
                hst.Add("lot_id", ddl_lotname.SelectedValue);
                hst.Add("coupan",coupan );

                hst.Add("series", txtbox_Coupanseries.Text);
                hst.Add("number", number);
                hst.Add("type", ddl_CoupanType.SelectedItem.Text);
                hst.Add("discount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));
                hst.Add("startdate",Convert.ToDateTime( txtbox_StartDate.Text));
                hst.Add("id", Convert.ToInt32(ds.Tables[0].Rows[0]["LotRateid"].ToString()));

                if (ddlchargeby.SelectedItem.Text == "Monthly")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddMonths(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                    else
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddMonths(Convert.ToInt32(1));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Hourly")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddHours(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                    else
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddHours(Convert.ToInt32(1));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Daily")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                    else
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(1));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Weekly")
                {
                    if (txtbox_type.Text != "")
                    {
                        int multi = Convert.ToInt32(txtbox_type.Text);
                        int daysofweek = 7;
                        int weekdays = daysofweek * multi;
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(weekdays));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                    else
                    {
                        int multi = Convert.ToInt32(1);
                        int daysofweek = 7;
                        int weekdays = daysofweek * multi;
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(weekdays));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                }

               
              //  hst.Add("enddate", Convert.ToDateTime(txtbox_EndDate.Text));

                hst.Add("status", stat);
                hst.Add("activestatus", stat);

                int result = objData.ExecuteNonQuery("[sp_Coupan]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {
              
                    
                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }






        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }
    DataTable dt;
    DataTable dt1;
    int count;
    protected void txtbox_Coupanseries_TextChanged(object sender, EventArgs e)
    {
       dt  = new clsInsert().getCoupanseries(txtbox_Coupanseries.Text);
       
        if (dt.Rows.Count > 0)
        {
            
            lbl_rec.Text = "Already Used";
            txtbox_Coupanseries.Text = "";
            

           
        }
        else
        {
            lbl_rec.Text = "";
            txtbox_Coupanseries.Text = txtbox_Coupanseries.Text;
          
        }

    }

    public void updateCoupan()
    {
        try
        {
            int val = Convert.ToInt32(txtbox_Quantity.Text.ToString());
            int runfor = val- Convert.ToInt32(Session["number"].ToString());
            for (int i = 1; i <= runfor; i++)
            {
                hst.Clear();
                number = Convert.ToInt32(Session["number"].ToString()) +i;
                string coupan = txtbox_Coupanseries.Text + number.ToString();
                hst.Add("action", "update");
                hst.Add("lot_id", ddl_lotname.SelectedValue);
                hst.Add("coupan", coupan);
                //  @coupan,@lot_id,@series,@number,@type,@discount,@status,@startdate,@enddate
                hst.Add("series", txtbox_Coupanseries.Text);
                hst.Add("number", number);
                hst.Add("type", ddl_CoupanType.SelectedItem.Text);
                hst.Add("discount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));
                hst.Add("startdate", Convert.ToDateTime(txtbox_StartDate.Text));
          //      hst.Add("enddate", Convert.ToDateTime(txtbox_EndDate.Text));
                if (ddlchargeby.SelectedItem.Text == "Monthly")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddMonths(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Hourly")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddHours(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Daily")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Weekly")
                {
                    if (txtbox_type.Text != "")
                    {
                        int multi = Convert.ToInt32(txtbox_type.Text);
                        int daysofweek = 7;
                        int weekdays = daysofweek * multi;
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(weekdays));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                }
                hst.Add("status", stat);

                int result = objData.ExecuteNonQuery("[sp_Coupan]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                    //clear();
                    //Response.Redirect("Admin_CityMaster.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }



            try
            {
                dt1 = (DataTable)Session["dbdet"];
                hst.Clear();

                hst.Add("action", "update");
                hst.Add("lotrateid", Convert.ToInt32(dt1.Rows[0]["LotRateid"].ToString()));

                
                hst.Add("lotid", ddl_lotname.SelectedValue);
                hst.Add("parkingtype", ddl_CoupanType.SelectedItem.Value);
                hst.Add("amount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));
                if (txtbox_type.Text != "")
                {
                    hst.Add("quantity", Convert.ToDecimal(txtbox_type.Text));
                }
                if (txtbox_type.Text == "")
                {
                    decimal defaultvalue = Convert.ToDecimal(0.00);
                    hst.Add("quantity", defaultvalue);

                }

                hst.Add("chargeby", ddlchargeby.SelectedValue);
                //hst.Add("isspecial", isspecial);



                int result_lotratemaster = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
                if (result_lotratemaster > 0)
                {

                }
            }
            catch
            {
            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }
    public void CoupanInactive(string series)
    {
        try
        {


            hst.Clear();

            hst.Add("action", "inactive");
            hst.Add("coupan", series);
            int result = objData.ExecuteNonQuery("[sp_Coupan]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              

            }
            else
            {
               

            }
        }


        catch
        {
            

        }

    }

    public void Coupanactive(string series)
    {
        try
        {


            hst.Clear();

            hst.Add("action", "active");
            hst.Add("coupan", series);
            int result = objData.ExecuteNonQuery("[sp_Coupan]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {


            }
            else
            {


            }
        }


        catch
        {


        }

    }
    
    
    
    public void clear()
    {
        txtbox_CoupanDiscount.Text = "";
        txtbox_Coupanseries.Text = "";
       // txtbox_EndDate.Text = "";
        txtbox_Quantity.Text = "";
        txtbox_StartDate.Text = "";
        txtbox_type.Text = "";
        txtbox_type.Visible = false;
        lbl_type.Visible = false;

      
    }
    protected void ddl_sitename_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (int.Parse(Session["coupancampusid"].ToString()) > 0)
            {
                ddl_campusname.SelectedValue = Session["coupancampusid"].ToString();

            }
        }
        catch
        {

        }
    }
    protected void ddl_campusname_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (int.Parse(Session["coupanlot"].ToString()) > 0)
            {
                ddl_lotname.SelectedValue = Session["coupanlot"].ToString();
            }
        }
        catch
        {

        }
    }
    protected void ddl_sitename_Unload(object sender, EventArgs e)
    {
      
    }
    protected void ddl_sitename_DataBinding(object sender, EventArgs e)
    {
        try
        {
            if (int.Parse(Session["coupansiteid"].ToString()) > 0)
            {
                ddl_sitename.SelectedValue = Session["coupansiteid"].ToString();
            }
        }
        catch
        {

        }
    }
    protected void ddl_lotname_SelectedIndexChanged(object sender, EventArgs e)
    {
      
       
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlchargeby.SelectedItem.Text == "Select")
        {
            lbl_type.Visible = true;
          txtbox_type.Visible = false;
            lbl_type.Text = "Please Select at least one Value";
        }
        if (ddlchargeby.SelectedItem.Text == "Monthly")
        {
            lbl_type.Visible = true;
            txtbox_type.Visible = true;
            lbl_type.Text = "Number Of Months";
        }
        if (ddlchargeby.SelectedItem.Text == "Hourly")
        {
            lbl_type.Visible = true;
            txtbox_type.Visible = true;
            lbl_type.Text = "Number Of Hours";
        }
        if (ddlchargeby.SelectedItem.Text == "Daily")
        {
            lbl_type.Visible = true;
            txtbox_type.Visible = true;
            lbl_type.Text = "Number Of Days";
        }
        if (ddlchargeby.SelectedItem.Text == "Weekly")
        {
            lbl_type.Visible = true;
            txtbox_type.Visible = true;
            lbl_type.Text = "Number Of Weeks";
        }
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdInactive")
        {
            string series = e.CommandArgument.ToString();
            if (Session["Type"].ToString() == "admin")
            {
                CoupanInactive(series);
                Response.Redirect("Admin_CoupanMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbldelete"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                }
                else
                {
                    //  delCountry(ID);
                    //Response.Redirect("Operator_CountryMaster.aspx");
                }
            }

        }

        if (e.CommandName == "cmdActive")
        {
            string series = e.CommandArgument.ToString();
            if (Session["Type"].ToString() == "admin")
            {
                Coupanactive(series);
                Response.Redirect("Admin_CoupanMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbldelete"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                }
                else
                {
                    //  delCountry(ID);
                    Response.Redirect("Operator_CountryMaster.aspx");
                }
            }

        }


        if (e.CommandName == "cmdEdit")
        {
           
                string coupan_series = e.CommandArgument.ToString();
            Label l = Coupan_Repeater.Items[i].FindControl("idLabel") as Label;
            Label Typeofcoupan = Coupan_Repeater.Items[i].FindControl("Typeofcoupan") as Label;

            Label StartDate = Coupan_Repeater.Items[i].FindControl("StartDate") as Label;

                Session["coupan_series"] = coupan_series;
                Session["number"] = l.Text;
                Session["StartDate"] = StartDate.Text;
                Session["Typeofcoupan"] = Typeofcoupan.Text;

                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_EditCoupanMaster.aspx");
                }
          
              
           
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbledit"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                    }
                    else
                    {
                        Response.Redirect("Operator_EditCoupanMaster.aspx");
                    }

                }


            }
           
        }
    protected void txtbox_Quantity_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_CoupanDiscount_TextChanged(object sender, EventArgs e)
    {
        try
        {
            decimal b = Convert.ToDecimal(txtbox_CoupanDiscount.Text);
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please insert only decimal value')", true);

        }


    }
}

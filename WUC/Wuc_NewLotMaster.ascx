﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_NewLotMaster.ascx.cs" Inherits="WUC_Wuc_NewLotMaster" %>
<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
        margin-left: 0px;
    }

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style1
    {
        height: 803px;
    }
    .style2
    {
        height: 34px;
    }
    .form-table
    {
        margin-right: 136px;
    }
    .style7
    {
        height: 34px;
        width: 152px;
    }
    .style8
    {
        width: 152px;
    }
</style>

<table width="100%" align="center">
      <tr>
        <td>
         
         
         
              <table style="width: 100%">
                <tr>
                  <td class="style1">
                    <h3>
                      <span>
                      
    <asp:Panel ID="Panel1" runat="server">
    <fieldset >
    <legend>
        Site Details</legend>
        <table class="form-table" width="100%">
            <tr>
                <td class="style7">
                    Select Site Name:</td>
                <td>
                    <asp:DropDownList ID="ddl_sitename" runat="server" ValidationGroup="a" 
                        CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource1" 
                        DataTextField="SiteName" DataValueField="SiteId">
                    </asp:DropDownList>
                    
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT * FROM [tbl_SiteMaster]"></asp:SqlDataSource>
                    
                    <asp:RequiredFieldValidator ID="rfv_sitename" runat="server" 
                        ControlToValidate="ddl_sitename" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style7">
                    Select Campus:</td>
                <td>
                    <asp:DropDownList ID="ddl_campus" runat="server" ValidationGroup="a"  CssClass="twitterStyleTextbox">
                    
                        <asp:ListItem Value="1">Campus 1</asp:ListItem>
                        <asp:ListItem Value="2">Campus 2</asp:ListItem>
                        <asp:ListItem Value="3">Campus 3</asp:ListItem>
                        <asp:ListItem Value="4">Campus 4</asp:ListItem>
                    
                                        </asp:DropDownList>
                    
                    <asp:RequiredFieldValidator ID="rfv_sitetype" runat="server" 
                        ControlToValidate="ddl_campus" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style7">
                    Lot Name<span>:</span></td>
                <td>
                    <asp:TextBox ID="txtbox_LotName" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_LotName" runat="server" 
                        ControlToValidate="txtbox_LotName" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="m"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style7">
                    Lot Number<span>:</span></td>
                <td>
                    <asp:TextBox ID="txtbox_lotnumber" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_lotnumber" runat="server" 
                        ControlToValidate="txtbox_lotnumber" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="m"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style7">
                    Lot Type
                <span>:</span></td>
                <td>
                    <asp:TextBox ID="txtbox_Lottype" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_Lottype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="m"></asp:RequiredFieldValidator>
                </td>
            </tr>



            <tr>
                  <td class="style7">Enterance Information<span>:</span></td>
                       <td>
                                            <asp:TextBox ID="txtbox_enteranceinformation" runat="server" 
                                                ValidationGroup="n" Width="162px" CssClass="twitterStyleTextbox" 
                                                Height="23px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_enterance_information" runat="server" 
                                                ControlToValidate="txtbox_exitinformation" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style8">
                                            Exit Information<span>:</span></td>
                                        <td>
                                            <asp:TextBox ID="txtbox_exitinformation" runat="server" ValidationGroup="n" CssClass="twitterStyleTextbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_exit_information" runat="server" 
                                                ControlToValidate="txtbox_exitinformation" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style8">
                                            Restriction<span>:</span></td>
                                        <td>
                                            <asp:TextBox ID="txtbox_restriction" runat="server" ValidationGroup="n" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_restriction" runat="server" 
                                                ControlToValidate="txtbox_restriction" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                   
                                        <tr>
                                            <td class="style8">
                                                Lot Address
                                            <span>:</span></td>
                                            <td>
                                                <asp:TextBox ID="txtbox_lotAddress" runat="server" 
                                                    CssClass="twitterStyleTextbox" ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                                                    ControlToValidate="txtbox_lotAddress" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>

                                        <tr>
                                        <td class="style8">
                                            Parking Billing Type<span>:</span></td>
                                        <td>
                                            <asp:CheckBoxList ID="chbk_Billtype" runat="server">
                                            </asp:CheckBoxList>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td class="style8">
                                            Random Rate /tariff<span>:</span></td>
                                        <td>
                                            <asp:TextBox ID="txtbox_random" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td class="style8">
                                            Reserved Rate/ Tariff
                                        <span>:</span></td>
                                        <td>
                                            <asp:TextBox ID="txtbox_reserved" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                                        </td>
                                        </tr>
                                         <tr>
                                            <td class="style8">
                                                Street #
                                            <span>:</span></td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Streetid" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                    ControlToValidate="txtbox_Streetid" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Street Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbox_StreetName" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                    ControlToValidate="txtbox_StreetName" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Direction:</td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Direction" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                    ControlToValidate="txtbox_Direction" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                City:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbox_City" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                    ControlToValidate="txtbox_City" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Province :</td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Prov" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                    ControlToValidate="txtbox_Prov" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Postal Code:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Postal" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                    ControlToValidate="txtbox_Postal" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Longitude:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Longitude" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                                    ControlToValidate="txtbox_Longitude" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style7">
                                                Latitude:
                                            </td>
                                            <td class="style2">
                                                <asp:TextBox ID="txtbox_Latitude" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                                    ControlToValidate="txtbox_Latitude" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Ph#:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Phone" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                                    ControlToValidate="txtbox_Phone" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style8">
                                                Email:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbox_Email" runat="server" CssClass="twitterStyleTextbox" 
                                                    ValidationGroup="n"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                                    ControlToValidate="txtbox_Email" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="n"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style7">
                                                Total Space<span>:</span></td>
                                            <td>
                                                <asp:TextBox ID="txtbox_spaceavailable" runat="server" 
                                                    CssClass="twitterStyleTextbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv_spaceavailable" runat="server" 
                                                    ControlToValidate="txtbox_spaceavailable" ErrorMessage="*" ForeColor="#CC0000" 
                                                    ValidationGroup="a"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
       
        </table><br />
           <asp:Button ID="btn_add" runat="server" Text="Add" onclick="btn_add_Click" CssClass="button"
                            ValidationGroup="n" />

                        
                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="button" />
    </fieldset>
</asp:Panel>
<br />



                       
           
              </td>
      </tr>
    </table>
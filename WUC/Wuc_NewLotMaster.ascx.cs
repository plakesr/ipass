﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class WUC_Wuc_NewLotMaster : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            PopulateBillingType();
        }
    }

    protected void btn_add_Click(object sender, EventArgs e)
    {
        
    }

    private void PopulateBillingType()
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_CategoryMaster";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["CategoryName"].ToString();
                        item.Value = sdr["CategoryId"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chbk_Billtype.Items.Add(item);
                        
                    }
                }
                conn.Close();
            }
        }
    }
}
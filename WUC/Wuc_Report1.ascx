﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_Report1.ascx.cs" Inherits="WUC_Wuc_Report1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 304px;
    }
</style>
<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(function () {
        $("#dvAccordian").accordion();
    });
</script>--%>
<table class="style1">
    <tr>
        <td class="style2">
            Select Customer</td>
        <td>
            <asp:DropDownList ID="ddlsite" runat="server" DataSourceID="Site" DataTextField="SiteName" DataValueField="SiteId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="Site" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] ORDER BY [SiteName]">
            </asp:SqlDataSource>
        </td>
    </tr>
     <tr>
        <td class="style2">
            Select Period</td>
        <td>
            <asp:DropDownList ID="ddlperiod" runat="server" >
            <asp:ListItem Value="1" Text="This Month"></asp:ListItem>
            <asp:ListItem Value="2" Text="Last Month"></asp:ListItem>
            <asp:ListItem Value="3" Text="Last Three Month"></asp:ListItem>
            <asp:ListItem Value="4" Text="Last Six Month"></asp:ListItem>
          <%--  <asp:ListItem Value="5" Text="Last One Year"></asp:ListItem>--%>

            </asp:DropDownList>
           
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td>
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                Text="Preview" />
        </td>
    </tr>
</table>
<div  id="dvAccordian">
<asp:Repeater ID="rptSite" runat="server" OnItemCommand="rptsiteinfo_ItemCommand"
        OnItemDataBound="rptsiteinfo_ItemDataBound">
        <HeaderTemplate>
            <ul >
                          <asp:Label ID="lbl1" runat="server" Text="Site Name" Visible="True"></asp:Label>
                       

        </HeaderTemplate>
        <AlternatingItemTemplate>
            <li >
                <asp:Label ID="lblCId"  runat="server" Text='<%#Eval("SiteId")%>' Visible="false"></asp:Label>
                <span>
                   <div > <%#Eval("SiteName")%><%#Eval("Revenue") %></div></span>
                <asp:Repeater ID="rptCampus" runat="server" OnItemDataBound="rptcampusInfo_ItemDataBound">
                    <HeaderTemplate>
                        <ol >
                          <asp:Label ID="lbl" runat="server" Text="Campus Name" Visible="true"></asp:Label>
                         
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSId" runat="server" Text='<%#Eval("CampusID")%>' Visible="false"></asp:Label>
                        <li ><span>
                            <%#Eval("Campus_Name")%><%#Eval("RevenueCampus")%></span> </li>
                        <asp:Repeater ID="rptLot" runat="server" OnItemDataBound="rptLotInfo_ItemDataBound">
                            <HeaderTemplate>
                                <ol class="clsT">
                          <asp:Label ID="lbl_lot" runat="server" Text="Lot Name" Visible="true"></asp:Label>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <li ><a><span>
                        <asp:Label ID="lblLId" runat="server" Text='<%#Eval("LotID")%>' Visible="false"></asp:Label>

                                    <%#Eval("LotName")%></span></a></li>

                                    <asp:Repeater ID="rptAccount" runat="server">
                            <HeaderTemplate>
                                <ol >
                          <asp:Label ID="lbl_Account" runat="server" Text="Account Name" Visible="true"></asp:Label>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <li ><a><span>
                      <%--  <asp:Label ID="lblLId" runat="server" Text='<%#Eval("")%>' Visible="false"></asp:Label>--%>

                                    <%#Eval("Account_Name")%><%#Eval("TransactionDate")%> <%#Eval("RevenueAccount") %></span>
                                    </a></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ol>
                            </FooterTemplate>
                        </asp:Repeater>


                            </ItemTemplate>
                            <FooterTemplate>
                                </ol>
                            </FooterTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ol>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </AlternatingItemTemplate>
        <ItemTemplate>
            <li >
                <asp:Label ID="lblCId" runat="server" Text='<%#Eval("SiteId")%>' Visible="false"></asp:Label>
                <span>
                    <div><%#Eval("SiteName")%> <%#Eval("Revenue") %></div></span>
                <asp:Repeater ID="rptCampus" runat="server" OnItemDataBound="rptcampusInfo_ItemDataBound">
                    <HeaderTemplate>
                        <ol >
                          <asp:Label ID="lbl" runat="server" Text="Campus Name" Visible="true"></asp:Label>

                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSId" runat="server" Text='<%#Eval("CampusID")%>' Visible="false"></asp:Label>
                        <li ><span>
                            <%#Eval("Campus_Name")%> <%#Eval("RevenueCampus")%></span> </li>
                        <asp:Repeater ID="rptLot" runat="server" OnItemDataBound="rptLotInfo_ItemDataBound">
                            <HeaderTemplate>
                                <ol >
                          <asp:Label ID="lbl_lot" runat="server" Text="Lot Name" Visible="true"></asp:Label>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <li ><a><span>
                        <asp:Label ID="lblLId" runat="server" Text='<%#Eval("LotID")%>' Visible="false"></asp:Label>

                                    <%#Eval("LotName")%><%#Eval("RevenueLot")%></span></a></li>

                                     <asp:Repeater ID="rptAccount" runat="server">
                            <HeaderTemplate>
                                <ol >
                          <asp:Label ID="lbl_Account" runat="server" Text="Account Name" Visible="true"></asp:Label>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <li ><a><span>
                      <%--  <asp:Label ID="lblLId" runat="server" Text='<%#Eval("")%>' Visible="false"></asp:Label>--%>

                                    <%#Eval("Account_Name")%><%#Eval("TransactionDate")%> <%#Eval("RevenueAccount") %></span>
                                    </a></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ol>
                            </FooterTemplate>
                        </asp:Repeater>


                            </ItemTemplate>
                            <FooterTemplate>
                                </ol>
                            </FooterTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ol>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
    </asp:Repeater>
    </div>
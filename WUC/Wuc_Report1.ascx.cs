﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_Wuc_Report1 : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    sitename1();
        //}
        //catch
        //{
        //}
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            sitename();
        }
        catch
        {
        }
    }
    protected void rptsiteinfo_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    decimal revenueaccount = Convert.ToDecimal("0.00");
    protected void rptsiteinfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      
        Label cid = e.Item.FindControl("lblCId") as Label;
        Repeater rptCampus = e.Item.FindControl("rptCampus") as Repeater;
        DataTable dt_campus = new DataTable();

        dt_campus.Columns.Add("Campus_Name", typeof(string));
        dt_campus.Columns.Add("RevenueCampus", typeof(decimal));
        dt_campus.Columns.Add("CampusID", typeof(int));
        
            if (cid != null && rptCampus != null)
            {

                DataSet ds = new clsInsert().fetchrec("select * from tbl_CampusMaster where SiteID=" + Convert.ToInt32(cid.Text));
                if (ds.Tables[0].Rows.Count > 0)
                {//0,2
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = dt_campus.NewRow();
                        dr[0] = ds.Tables[0].Rows[i][2].ToString();
                        dr[2] = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                        DataSet ds_userget = new clsInsert().fetchrec("select tbl_LotMaster.LotName,tbl_CampusMaster.Campus_Name,tbl_CampusMaster.CampusID,tbl_ParkerRegis.UserName from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId inner join tbl_CampusMaster on tbl_CampusMaster.CampusID=tbl_LotMaster.Campus_id  where tbl_CampusMaster.CampusID=" + Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
                        if (ds_userget.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds_userget.Tables[0].Rows.Count; j++)
                            {
                                DataSet dscurrentmonth_revenue = new clsInsert().fetchrec("select * from TBL_Invoice where ([Date Issued] between '"+dateissuestart+"' and '"+dateisssueend+"')and Acct_Num='" + ds_userget.Tables[0].Rows[j][3].ToString() + "'");
                                if (dscurrentmonth_revenue.Tables[0].Rows.Count > 0)
                                {
                                    for (int k = 0; k < dscurrentmonth_revenue.Tables[0].Rows.Count; k++)
                                    {
                                        revenue_campus = revenue_campus + Convert.ToDecimal(dscurrentmonth_revenue.Tables[0].Rows[k][4].ToString());
                                    }
                                }
                                else
                                {
                                    revenue_campus = revenue_campus + Convert.ToDecimal("0.00");
                                }
                            }
                            dr[1] = revenue_campus;
                        }
                        else
                        {
                            dr[1] = revenue_campus;
                        }

                        dt_campus.Rows.Add(dr);

                        revenue_campus = Convert.ToDecimal("0.00");
                    }
                }
                rptCampus.DataSource = dt_campus;
                rptCampus.DataBind();
            }
       
    }
    protected void rptLotInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lid = e.Item.FindControl("lblLId") as Label;
        Repeater rptAccount = e.Item.FindControl("rptAccount") as Repeater;
        DataTable dt_Account = new DataTable();

        dt_Account.Columns.Add("Account_Name", typeof(string));
        dt_Account.Columns.Add("RevenueAccount", typeof(decimal));
        dt_Account.Columns.Add("TransactionDate", typeof(DateTime));
       
            if (lid != null && rptAccount != null)
            {

                DataSet dtp = new clsInsert().fetchrec("select tbl_LotMaster.LotName,tbl_LotMaster.Lot_id,tbl_ParkerRegis.UserName from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId where tbl_LotMaster.Lot_id=" + Convert.ToInt32(lid.Text));
                if (dtp.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i < dtp.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = dt_Account.NewRow();
                        dr[0] = dtp.Tables[0].Rows[i][2].ToString();
                        DataSet dscurrentmonth_revenue = new clsInsert().fetchrec("select * from TBL_Invoice where ([Date Issued] between '" + dateissuestart + "' and '" + dateisssueend + "')and Acct_Num='" + dtp.Tables[0].Rows[i][2].ToString() + "'");
                        if (dscurrentmonth_revenue.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < dscurrentmonth_revenue.Tables[0].Rows.Count; j++)
                            {
                                revenueaccount =revenueaccount+ Convert.ToDecimal(dscurrentmonth_revenue.Tables[0].Rows[j][4].ToString());
                            }
                            dr[2] = Convert.ToDateTime(dscurrentmonth_revenue.Tables[0].Rows[0][2].ToString());
                            dr[1] = Convert.ToDecimal(revenueaccount);
                            revenueaccount = Convert.ToDecimal("0.00");
                        }
                        else
                        {

                            revenueaccount = Convert.ToDecimal("0.00");
                            dr[2] = DateTime.Now;
                            dr[1] = Convert.ToDecimal(revenueaccount);
                        }
                        dt_Account.Rows.Add(dr);
                    }
                    rptAccount.DataSource = dt_Account;
                    rptAccount.DataBind();
                }
                else
                {
                }
            }
       
    }
    protected void rptcampusInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
       
        Label sid = e.Item.FindControl("lblSId") as Label;
        Repeater rptLot = e.Item.FindControl("rptLot") as Repeater;
        DataTable dt_Lot = new DataTable();

        dt_Lot.Columns.Add("LotName", typeof(string));
        dt_Lot.Columns.Add("RevenueLot", typeof(decimal));
        dt_Lot.Columns.Add("LotID", typeof(int));
       
            if (sid != null && rptLot != null)
            {

                DataSet dtp = new clsInsert().fetchrec("select * from tbl_LotMaster where Campus_id=" + Convert.ToInt32(sid.Text));
                if (dtp.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i < dtp.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = dt_Lot.NewRow();
                        dr[0] = dtp.Tables[0].Rows[i][3].ToString();
                        dr[2] = Convert.ToInt32(dtp.Tables[0].Rows[i][0].ToString());
                        DataSet ds_userget = new clsInsert().fetchrec("select tbl_LotMaster.LotName,tbl_LotMaster.Lot_id,tbl_ParkerRegis.UserName from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId  where tbl_LotMaster.Lot_id=" + Convert.ToInt32(dtp.Tables[0].Rows[i][0].ToString()));
                        if (ds_userget.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds_userget.Tables[0].Rows.Count; j++)
                            {
                                DataSet dscurrentmonth_revenue = new clsInsert().fetchrec("select * from TBL_Invoice where ([Date Issued] between '" + dateissuestart + "' and '" + dateisssueend + "')and Acct_Num='" + ds_userget.Tables[0].Rows[j][2].ToString() + "'");
                                if (dscurrentmonth_revenue.Tables[0].Rows.Count > 0)
                                {
                                    for (int k = 0; k < dscurrentmonth_revenue.Tables[0].Rows.Count; k++)
                                    {
                                        revenue_lot = revenue_lot + Convert.ToDecimal(dscurrentmonth_revenue.Tables[0].Rows[k][4].ToString());
                                    }
                                }
                                else
                                {
                                    revenue_lot = revenue_lot + Convert.ToDecimal("0.00");
                                }
                            }
                            dr[1] = revenue_lot;
                        }
                        else
                        {
                            dr[1] = revenue_lot;
                        }

                        dt_Lot.Rows.Add(dr);

                        revenue_lot = Convert.ToDecimal("0.00");

                    }
                    rptLot.DataSource = dt_Lot;
                    rptLot.DataBind();
                }
            }
      
    }
    decimal revenue = Convert.ToDecimal("0.00");
    decimal revenue_campus = Convert.ToDecimal("0.00");
    decimal revenue_lot = Convert.ToDecimal("0.00");
    DateTime dateissuestart;
    DateTime dateisssueend;
    public void sitename()
    {
        DataTable dt_site = new DataTable();

        dt_site.Columns.Add("SiteName", typeof(string));
        dt_site.Columns.Add("Revenue", typeof(decimal));
        dt_site.Columns.Add("SiteId", typeof(int));

        if (ddlperiod.SelectedItem.Value == "1")
        {
            DateTime datetoday = DateTime.Now;
            int month = datetoday.Month;
            int year = datetoday.Year;
            int numberofdaysinthemonth = DateTime.DaysInMonth(datetoday.Year, datetoday.Month);
            dateissuestart = new DateTime(year, month, 1);
            dateisssueend = new DateTime(year, month, numberofdaysinthemonth);
        }
        if (ddlperiod.SelectedItem.Value == "2")
        {
            //lastmonth
            DateTime datetoday = DateTime.Now;
            int month = datetoday.Month;
            int year = datetoday.Year;
            month = month - 1;
           
            int numberofdaysinthemonth = DateTime.DaysInMonth(datetoday.Year, month);
            dateissuestart = new DateTime(year, month, 1);
            dateisssueend = new DateTime(year, month, numberofdaysinthemonth);
        }

        if (ddlperiod.SelectedItem.Value == "3")
        {
            //Last Three Month
            DateTime datetoday = DateTime.Now;
            int month = datetoday.Month;
            int year = datetoday.Year;
            month = month - 1;

            int numberofdaysinthemonth = DateTime.DaysInMonth(datetoday.Year, month);
            dateisssueend = new DateTime(year, month, numberofdaysinthemonth);
            month = month - 2;
            dateissuestart = new DateTime(year, month, 1);
           
        }
        if (ddlperiod.SelectedItem.Value == "4")
        {
            //Last Six Month
            DateTime datetoday = DateTime.Now;
            int month = datetoday.Month;
            int year = datetoday.Year;
            month = month - 1;

            int numberofdaysinthemonth = DateTime.DaysInMonth(datetoday.Year, month);
            dateisssueend = new DateTime(year, month, numberofdaysinthemonth);
            month = month - 5;
            dateissuestart = new DateTime(year, month, 1);
        }
        //if (ddlperiod.SelectedItem.Value == "5")
        //{
        //    //Last one year
        //    DateTime datetoday = DateTime.Now;
        //    int month = datetoday.Month;
        //    int year = datetoday.Year;
        //    int numberofdaysinthemonth = DateTime.DaysInMonth(datetoday.Year, datetoday.Month);
        //    dateissuestart = new DateTime(year, month, 1);
        //    dateisssueend = new DateTime(year, month, numberofdaysinthemonth);
        //}
            DataSet dssite = new clsInsert().fetchrec("select tbl_LotMaster.LotName,tbl_CampusMaster.Campus_Name,tbl_SiteMaster.SiteName,tbl_ParkerRegis.UserName,tbl_SiteMaster.SiteId from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId inner join tbl_CampusMaster on tbl_CampusMaster.CampusID=tbl_LotMaster.Campus_id inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_CampusMaster.SiteID where tbl_SiteMaster.SiteId=" + ddlsite.SelectedValue);
            if (dssite.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dt_site.NewRow();
                dr[0] = dssite.Tables[0].Rows[0][2].ToString();
                dr[2] = Convert.ToInt32(dssite.Tables[0].Rows[0][4].ToString());

                for (int i = 0; i < dssite.Tables[0].Rows.Count; i++)
                {
                    DataSet dscurrentmonth_revenue = new clsInsert().fetchrec("select * from TBL_Invoice where ([Date Issued] between '"+dateissuestart+"' and '"+dateisssueend+"')and Acct_Num='" + dssite.Tables[0].Rows[i][3].ToString() + "'");
                    if (dscurrentmonth_revenue.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < dscurrentmonth_revenue.Tables[0].Rows.Count; j++)
                        {
                            revenue = revenue + Convert.ToDecimal(dscurrentmonth_revenue.Tables[0].Rows[j][4].ToString());
                        }
                    }
                    else
                    {
                        revenue = revenue + Convert.ToDecimal("0.00");
                    }

                }

                dr[1] = Convert.ToDecimal(revenue);
                dt_site.Rows.Add(dr);
            }
            else
            {
                DataSet ds_none = new clsInsert().fetchrec("select tbl_SiteMaster.SiteId,tbl_SiteMaster.SiteName from tbl_SiteMaster where SiteId=" + ddlsite.SelectedValue);
                DataRow dr = dt_site.NewRow();
                dr[0] = ds_none.Tables[0].Rows[0][1].ToString();
                dr[2] = Convert.ToInt32(ds_none.Tables[0].Rows[0][0].ToString());
                dr[1] = Convert.ToDecimal(revenue);
                dt_site.Rows.Add(dr);
            }
            rptSite.DataSource = dt_site;
            rptSite.DataBind();
        

       
    }

    public void sitename1()
    {
        DataSet dssite = new clsInsert().fetchrec("select * from tbl_sitemaster ");
        if (dssite.Tables[0].Rows.Count > 0)
        {
            rptSite.DataSource = dssite.Tables[0];
            rptSite.DataBind();
        }
    }
}
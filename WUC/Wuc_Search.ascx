﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_Search.ascx.cs" Inherits="WUC_Wuc_Search" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 189px;
    }
    .style3
    {
        width: 237px;
    }
</style>

<table class="style1">
    <tr>
        <td class="style2">
            Select Site</td>
        <td class="style3">
            <asp:DropDownList ID="DropDownList1" runat="server" 
                DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:Button ID="Button1" runat="server" Text="Search Sites" />
        </td>
    </tr>
    <tr>
        <td class="style2">
            Select Campus</td>
        <td class="style3">
            <asp:DropDownList ID="DropDownList2" runat="server" 
                DataSourceID="SqlDataSource2" DataTextField="Campus_Name" 
                DataValueField="CampusID">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                SelectCommand="SELECT [CampusID], [Campus_Name], [SiteID] FROM [tbl_CampusMaster]">
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:Button ID="Button2" runat="server" Text="Search Campus" />
        </td>
    </tr>
    <tr>
        <td class="style2">
            Select Lots</td>
        <td class="style3">
            <asp:DropDownList ID="DropDownList3" runat="server" 
                DataSourceID="SqlDataSource3" DataTextField="LotName" DataValueField="Lot_id">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                SelectCommand="SELECT [Lot_id], [LotName] FROM [tbl_LotMaster]">
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:Button ID="Button3" runat="server" Text="Search Lots" />
        </td>
    </tr>
</table>


﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_SearchParker.ascx.cs" Inherits="WUC_Wuc_SearchParker" %>
<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
      padding:8px 10px;}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
    padding:8px 10px;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

    .search-box{ margin:0px; padding:0px;} 
   .search-box label{ width:100%; margin-bottom:20px; float:left;}
     .search-box .twitterStyleTextbox{padding:10px; margin-bottom:20px; width:300px;}
   
</style>

<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
    <table class="style4">
        <tr>
            <td class="style7" width="150">
                <asp:CheckBox ID="chk_site" runat="server" Text=" Select  Customer" 
                    AutoPostBack="true" oncheckedchanged="chk_site_CheckedChanged" />
            </td>
            <td >
                <asp:DropDownList ID="ddl_sitename" runat="server" 
                    DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId" 
                    Enabled="False" AutoPostBack="True" CssClass="twitterStyleTextbox" AppendDataBoundItems="True" Width="305px">
                    <asp:ListItem Selected = "True" Text = "--Select Site--" Value = "0"></asp:ListItem>
                                
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [SiteName], [SiteId] FROM [tbl_SiteMaster] order by SiteName">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox ID="chk_campus" runat="server" Text=" Select Campus" 
                    oncheckedchanged="chk_campus_CheckedChanged"  AutoPostBack="true" />
            </td>
            <td>
                <asp:DropDownList ID="ddl_campusname" runat="server" 
                    DataSourceID="SqlDataSource2" DataTextField="Campus_Name" 
                    DataValueField="CampusID" Enabled="False" AutoPostBack="True" CssClass="twitterStyleTextbox" AppendDataBoundItems="True" Width="305px">
                    <asp:ListItem Selected = "True" Text = "--Select Campus--" Value = "0"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [CampusID], [Campus_Name] FROM [tbl_CampusMaster] order by Campus_Name">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox ID="chk_lot" runat="server" Text=" Select Lot" AutoPostBack="true" 
                    oncheckedchanged="chk_lot_CheckedChanged" />
            </td>
            <td>
                <asp:DropDownList ID="ddl_lotname" runat="server" DataSourceID="SqlDataSource3" 
                    DataTextField="LotName" DataValueField="Lot_id" Enabled="False" 
                    onselectedindexchanged="ddl_lotname_SelectedIndexChanged" 
                    AutoPostBack="True" AppendDataBoundItems="True" CssClass="twitterStyleTextbox" Width="305px">
                    <asp:ListItem Selected = "True" Text = "--Select Lot--" Value = "0"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [Lot_id], [LotName] FROM [tbl_LotMaster]  order by LotName">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox ID="chk_username" runat="server" Text="Name" 
                    oncheckedchanged="chk_username_CheckedChanged" AutoPostBack= "true"/>
            </td>
            <td>
                <asp:TextBox ID="txtbox_username" runat="server" Enabled="False" 
                    CssClass="twitterStyleTextbox" Width="285px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style7">
               <asp:Label ID="Result" runat="server" Text="" Visible="false"></asp:Label></td>  
            </td>
            <td>
               
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    
</div>


<table class="style4" width="100%">
    <tr>
        <td class="style8" width="150"></td>
           
        <td align="left">
           <asp:Button ID="btn_search" runat="server" onclick="btn_search_Click" 
                    Text="Search" CssClass="button" />
</td>
    </tr>
</table>

<br />
<%--<div class="search-box">

<label>Enter Parker Name To Search</label><br />
<asp:TextBox ID="txtbox_ParkerSearch" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
<br />
 <asp:Button ID="Btn_Submit" runat="server" onclick="Btn_Submit_Click" 
                Text="Search Parker" CssClass="button" />
                </div>--%>

<asp:Panel ID="Panel2" runat="server" >
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
            <br />
            
            <asp:Repeater runat="server" id="rpt_searchparker" 
                onitemcommand="rpt_searchparker_ItemCommand" Visible="False" 
                 >
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Parkers&nbsp;&nbsp;Name
                    </th>
                     <th>
                        User&nbsp;&nbsp;Name
                    </th>
                    <th>
                        Country
                    </th>
                    
                    <th>
                      City
                    </th>
                    <th>
                    Type
                    </th>
                     <th>
                     Details
                    </th>
                    <th>
                     Deactivate
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("FirstName")%> &nbsp; <%# Eval("LastName") %>
                    </td><td>
                    <%#Eval("UserName")%>
                </td>

                <td>
                    <%#Eval("Country")%>
                </td>
                <td>
                    <%#Eval("City")%>
                </td>
                <td>
                    <%#Eval("type")%> &nbsp Parker
                </td>

                <td>
                   <asp:LinkButton ID="lnkDetails" CommandName="cmdDetails" CommandArgument='<%#Eval("FirstName") %>' runat="server" OnClientClick="return confirmation();" CssClass="button">Details
                    </asp:LinkButton>
                    </td>
                    <td>
                     <asp:LinkButton ID="lnkDeactivate" CommandName="cmdInactive" CommandArgument='<%#Eval("UserName") %>' runat="server" ><img src="images/inactive.png" />
                    </asp:LinkButton>
                    <asp:Label ID="UserName" runat="server" Text='<%# Eval("UserName") %>' 
                                        Visible="False"></asp:Label>
                    <asp:Label ID="Billingwith" runat="server" Text='<%# Eval("Billingwith") %>' 
                                        Visible="False"></asp:Label>
                                         <asp:Label ID="AccountType" runat="server" Text='<%# Eval("AccountType") %>' 
                                        Visible="False" />

                                        <asp:Label ID="Cardno" runat="server" Text='<%# Eval("CardNo") %>' 
                                        Visible="False" />
                     <asp:Label ID="FirstName" runat="server" Text='<%# Eval("FirstName") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="LastName" runat="server" Text='<%# Eval("LastName") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="AdressLine1" runat="server" Text='<%# Eval("AdressLine1") %>' 
                                        Visible="False" />
                     <asp:Label ID="AdressLine2" runat="server" Text='<%# Eval("AdressLine2") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Zip" runat="server" Text='<%# Eval("Zip") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Country" runat="server" Text='<%# Eval("Country") %>' 
                                        Visible="False" />
                     <asp:Label ID="State" runat="server" Text='<%# Eval("State") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="City" runat="server" Text='<%# Eval("City") %>' 
                                        Visible="False" />
                     <asp:Label ID="CellNo" runat="server" Text='<%# Eval("CellNo") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Phoneno" runat="server" Text='<%# Eval("Phoneno") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Fax" runat="server" Text='<%# Eval("Fax") %>' 
                                        Visible="False" />
                     <asp:Label ID="Email" runat="server" Text='<%# Eval("Email") %>' 
                                        Visible="False"></asp:Label>
                     
                     <asp:Label ID="Pass" runat="server" Text='<%# Eval("Pass") %>' 
                                        Visible="False" />
                                        <asp:Label ID="ActivationDate" runat="server" Text='<%# Eval("ActivationDate") %>' 
                                        Visible="False" />
                                        <asp:Label ID="Parker_id" runat="server" Text='<%# Eval("Parker_id") %>' 
                                        Visible="False" />
                     
                     
                </td>
              
                   
                  <%--   <asp:Label ID="SiteName" runat="server" Text='<%# Eval("SiteName") %>' 
                                        Visible="False"></asp:Label>
                    --%>
                     <%--<asp:Label ID="TariffPlanNametoSite" runat="server" Text='<%# Eval("TariffPlanNametoSite") %>' 
                                        Visible="False"></asp:Label>--%>
                   
                    <%-- <asp:Label ID="CardDeliveryName" runat="server" Text='<%# Eval("CardDeliveryName") %>' 
                                        Visible="False"></asp:Label>  --%>                     
                  <%-- <asp:Label ID="CustomerName" runat="server" Text='<%# Eval("CustomerName") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="BankAccType" runat="server" Text='<%# Eval("BankAccType") %>' 
                                        Visible="False" />
                     <asp:Label ID="BranchName" runat="server" Text='<%# Eval("BranchName") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="BankName" runat="server" Text='<%# Eval("BankName") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Bank_Zipcode" runat="server" Text='<%# Eval("Bank_Zipcode") %>' 
                                        Visible="False" />
                     <asp:Label ID="Bank_city" runat="server" Text='<%# Eval("Bank_city") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Bank_State" runat="server" Text='<%# Eval("Bank_State") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="BankRoutingNo" runat="server" Text='<%# Eval("BankRoutingNo") %>' 
                                        Visible="False" />
                     <asp:Label ID="BankAccountNo" runat="server" Text='<%# Eval("BankAccountNo") %>' 
                                        Visible="False"></asp:Label>--%>

 
         
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
    
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
<br /><br />
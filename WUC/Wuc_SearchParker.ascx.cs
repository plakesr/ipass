﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_Wuc_SearchParker : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    decimal monthlyreservedcharges = Convert.ToDecimal("0.00");
    decimal monthlyrandomcharges = Convert.ToDecimal("0.00");
    decimal monthlycharges = Convert.ToDecimal("0.00");
    decimal monthly_totalcharge = Convert.ToDecimal("0.00");
    decimal creditamount = Convert.ToDecimal("0.00");
    decimal extramount = Convert.ToDecimal("0.00");

    string status = "true";
    string invoicenumber = "";
    string monthforinvoice = "";
    int dateformatch = 15;
    decimal amountoutstanding = Convert.ToDecimal("0.00");
    string billingtypeentry = "";
    string generatebill = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddl_sitename.SelectedIndex = 0;

            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('updated successfully done')", true);

            }
        }
    }
    public void updateuserstatus(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "updatestatus");
            //hst.Add("ustatus", userstattus1);
            hst.Add("username", username);



            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                // Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                //  Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
            // Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }
    public void countuserbill(string user)
    {
        DataSet dsuserlot = new clsInsert().fetchrec("select tbl_ParkerRegis.LotId ,tbl_LotMaster.Billing_exit from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId where UserName='" + user + "'");
        if (dsuserlot.Tables[0].Rows[0][1].ToString() == "1")
        {
            billingtypeentry = "Full Charge";
        }

        if (dsuserlot.Tables[0].Rows[0][1].ToString() == "2")
        {
            billingtypeentry = "Prorate";

        }

        if (dsuserlot.Tables[0].Rows[0][1].ToString() == "3")
        {
            billingtypeentry = "Prorate By Fort Night";

        }


        DataSet dsplan_reserved = new clsInsert().fetchrec("select tbl_ParkerRegis. PlanRateid ,tbl_LotRateMaster.Amount from tbl_ParkerRegis inner join tbl_LotRateMaster on tbl_LotRateMaster.LotRateid=tbl_ParkerRegis.PlanRateid where UserName='"+user+"'");
        if (dsplan_reserved.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsplan_reserved.Tables[0].Rows.Count; i++)
            {
                decimal totalmoney_reserved = Convert.ToDecimal(dsplan_reserved.Tables[0].Rows[i][1].ToString());
                monthlyreservedcharges = monthlyreservedcharges + totalmoney_reserved;
            }
        }
        //DataSet dsplan_random = new clsInsert().fetchrec("select tbl_CorporateLotRatePlan.MonthlyRate,tbl_CorporateRandomParkingDetails.NumberOfParker from tbl_CorporateLotRatePlan inner join tbl_CorporateRandomParkingDetails on tbl_CorporateRandomParkingDetails.Planid=tbl_CorporateLotRatePlan.PlanID_Corporate where tbl_CorporateLotRatePlan.Corporateusername='" + user + "' and tbl_CorporateLotRatePlan.ParkingType=2");

        //if (dsplan_random.Tables[0].Rows.Count > 0)
        //{
        //    for (int i = 0; i < dsplan_random.Tables[0].Rows.Count; i++)
        //    {
        //        decimal totalmoney_random = Convert.ToDecimal(dsplan_random.Tables[0].Rows[i][0].ToString()) * Convert.ToDecimal(dsplan_random.Tables[0].Rows[i][1].ToString());

        //        monthlyrandomcharges = monthlyrandomcharges + totalmoney_random;
        //    }
        //}
        monthlycharges = monthlyreservedcharges;
        if (billingtypeentry == "Full Charge")
        {
            monthly_totalcharge = monthlycharges;
        }
        if (billingtypeentry == "Prorate")
        {
            DateTime active = DateTime.Now;
            int y = active.Year;
            int m = active.Month;
            int d = active.Day;
            int days = DateTime.DaysInMonth(y, m);
            int daysinmonthremaining = days - d;
            double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days);
            monthly_totalcharge = monthlycharges * Convert.ToDecimal(daystocharge);
            generatebill = "yes";

        }
        if (billingtypeentry == "Prorate By Fort Night")
        {
            DateTime active = DateTime.Now;
            int d = active.Day;
            if (d > dateformatch)
            {
                monthly_totalcharge = monthlycharges;

            }
            else
            {

                monthly_totalcharge = monthlycharges / 2;
                generatebill = "yes";
            }
        }
        if (generatebill == "yes")
        {
            decimal taxcharge = Convert.ToDecimal("0.13");
            decimal subtotal = monthly_totalcharge;
            decimal total = monthly_totalcharge * taxcharge;
            DataSet ds_creditamount = new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='" + user + "' order by CreditNoteID desc");
            if (ds_creditamount.Tables[0].Rows.Count > 0)
            {
                creditamount = Convert.ToDecimal(ds_creditamount.Tables[0].Rows[0][2].ToString());

            }
            DataSet ds_amountoutsatnding = new clsInsert().fetchrec("select Amt_Outstanding,Biliing_type from TBL_Invoice where Acct_Num='" + user + "' order by ID desc");
            if (ds_amountoutsatnding.Tables[0].Rows.Count > 0)
            {
                if (creditamount > 0)
                {
                    extramount = Convert.ToDecimal(ds_amountoutsatnding.Tables[0].Rows[0][0].ToString());
                    creditamount = creditamount + extramount;
                }

                for (int i = 0; i < ds_amountoutsatnding.Tables[0].Rows.Count; i++)
                {
                    if (i == 0)
                        continue;
                    else
                        amountoutstanding = amountoutstanding + Convert.ToDecimal(ds_amountoutsatnding.Tables[0].Rows[i][0].ToString());
                }

            }

            decimal grandtotal = monthly_totalcharge + total + amountoutstanding;



            DateTime activedate = DateTime.Now;
            int year1 = activedate.Year;
            int month1 = activedate.Month;
            string month = "";
            if (month1 < 10)
            {
                month = "0" + Convert.ToString(month1);
            }
            if (month1 >= 10)
            {
                month = Convert.ToString(month1);

            }
            string yearmonth = Convert.ToString(year1) + Convert.ToString(month);
            DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
            if (dsinvoice.Tables[0].Rows.Count > 0)
            {
                string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                string[] strArr = oFName.Split('_');
                int sLength = strArr.Length;
                int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                if (number < 10)
                {
                    invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                }
                if (number >= 10 && number < 100)
                {
                    invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                }
                if (number >= 100 && number < 1000)
                {
                    invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                }
                if (number >= 1000 && number < 10000)
                {
                    invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                }
            }
            else
            {
                invoicenumber = yearmonth + "_" + "000001";
            }

            hst.Add("action", "insert_billing_corporate");
            hst.Add("invoicenumber", invoicenumber);

            hst.Add("Parkerusername", user);

            DateTime today = DateTime.Now;
            DateTime startOfMonth = new DateTime(today.Year, today.Month, 1);
            hst.Add("startdate", Convert.ToDateTime(startOfMonth));



            // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
            hst.Add("duedate", DateTime.Now);
            hst.Add("subtotal", subtotal);
            hst.Add("tax", Convert.ToDecimal(total));
            hst.Add("details", "Invoice At The Time Of Account Deactivate");

            hst.Add("totalamount", Convert.ToDecimal(grandtotal));
            if (creditamount < grandtotal)
            {
                decimal dueamount = Convert.ToDecimal(grandtotal) - creditamount;
                hst.Add("dueamount ", dueamount);

            }
            if (creditamount >= grandtotal)
            {
                decimal dueamount = Convert.ToDecimal("0.00");
                hst.Add("dueamount ", dueamount);

            }
            hst.Add("billingtype", Convert.ToInt32(ds_amountoutsatnding.Tables[0].Rows[0][1].ToString()));


            int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
            {
                hst.Clear();
                hst.Add("action", "insert");
                hst.Add("acctnumber", user);
                hst.Add("reason", "At The Time Of Terminating The Account");
                if (creditamount > grandtotal)
                {
                    decimal credit = creditamount - grandtotal;
                    hst.Add("creditamt", Convert.ToDecimal(credit));
                }
                if (creditamount <= grandtotal)
                {
                    // decimal credit = creditamount - grandtotal;
                    hst.Add("creditamt", Convert.ToDecimal("0.00"));
                }
                hst.Add("dateoftransaction", DateTime.Now);
                hst.Add("status", status);

                int result_billing_full1 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                {
                    if (monthly_totalcharge > 0)
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", invoicenumber);
                        hst.Add("billtitle", "Monthly Charges Of Current Month At The Time Of Termination Of Account");
                        hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", user);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (total > 0)
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", invoicenumber);
                        hst.Add("billtitle", "Tax Charges Of Current Month At The Time Of Termination Of Account");
                        hst.Add("amountparticular", Convert.ToDecimal(total));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", user);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (amountoutstanding > 0)
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", invoicenumber);
                        hst.Add("billtitle", "Total OutStanding Charges Of Previous Months At The Time Of Termination Of Account");
                        hst.Add("amountparticular", Convert.ToDecimal(amountoutstanding));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", user);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                }
            }
        }
    }
    //protected void Btn_Submit_Click(object sender, EventArgs e)
    //{
    //    DataTable dt = new clsInsert().getparkers(txtbox_ParkerSearch.Text);
    //    int count = dt.Rows.Count;
    //    if (dt.Rows.Count > 0)
    //    {
    //        Result.Visible = true;
    //        Result.Text = count.ToString()+" "+"Records Found" ;

    //        rpt_searchparker.DataSource = dt;
    //        rpt_searchparker.DataBind();

    //    }
    //    else
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
    //    }
    //}

    protected void rpt_searchparker_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDetails")
        {
            Label UserName = rpt_searchparker.Items[i].FindControl("UserName") as Label;
           Label Billingwith = rpt_searchparker.Items[i].FindControl("Billingwith") as Label;
            Session["UserName"] = UserName.Text;
           Session["Billingwith"] = Billingwith.Text;


            Label SiteName = rpt_searchparker.Items[i].FindControl("SiteName") as Label;
            Label AccountType = rpt_searchparker.Items[i].FindControl("AccountType") as Label;
            Label FirstName = rpt_searchparker.Items[i].FindControl("FirstName") as Label;
            Label LastName = rpt_searchparker.Items[i].FindControl("LastName") as Label;
            Label AdressLine1 = rpt_searchparker.Items[i].FindControl("AdressLine1") as Label;
           Label AdressLine2 = rpt_searchparker.Items[i].FindControl("AdressLine2") as Label;
           Label Zip = rpt_searchparker.Items[i].FindControl("Zip") as Label;
            Label Country = rpt_searchparker.Items[i].FindControl("Country") as Label;
            Label State = rpt_searchparker.Items[i].FindControl("State") as Label;
            Label City = rpt_searchparker.Items[i].FindControl("City") as Label;
            Label CellNo = rpt_searchparker.Items[i].FindControl("CellNo") as Label;
            Label Phoneno = rpt_searchparker.Items[i].FindControl("Phoneno") as Label;
            Label Fax = rpt_searchparker.Items[i].FindControl("Fax") as Label;
            Label Email = rpt_searchparker.Items[i].FindControl("Email") as Label;

            Label Pass = rpt_searchparker.Items[i].FindControl("Pass") as Label;

            //Label TariffPlanNametoSite = rpt_searchparker.Items[i].FindControl("TariffPlanNametoSite") as Label;
            Label ActivationDate = rpt_searchparker.Items[i].FindControl("ActivationDate") as Label;
           Label CardDeliveryName = rpt_searchparker.Items[i].FindControl("CardDeliveryName") as Label;
            //Label CustomerName = rpt_searchparker.Items[i].FindControl("CustomerName") as Label;
            //Label BankAccType = rpt_searchparker.Items[i].FindControl("BankAccType") as Label;
            //Label BranchName = rpt_searchparker.Items[i].FindControl("BranchName") as Label;
            //Label BankName = rpt_searchparker.Items[i].FindControl("BankName") as Label;
            //Label Bank_Zipcode = rpt_searchparker.Items[i].FindControl("Bank_Zipcode") as Label;
            //Label Bank_city = rpt_searchparker.Items[i].FindControl("Bank_city") as Label;
            //Label Bank_State = rpt_searchparker.Items[i].FindControl("Bank_State") as Label;
            //Label BankRoutingNo = rpt_searchparker.Items[i].FindControl("BankRoutingNo") as Label;
            //Label BankAccountNo = rpt_searchparker.Items[i].FindControl("BankAccountNo") as Label;
            Label Cardno = rpt_searchparker.Items[i].FindControl("Cardno") as Label;
            Label Parker_id = rpt_searchparker.Items[i].FindControl("Parker_id") as Label;
            Session["Parker_id"] = Parker_id.Text;

            //Session["SiteName"] = SiteName.Text;
            Session["AccountType"] = AccountType.Text;
            Session["FirstName"] = FirstName.Text;

            Session["LastName"] = LastName.Text;
            Session["AdressLine1"] = AdressLine1.Text;
           Session["AdressLine2"] = AdressLine2.Text;
           Session["Zip"] = Zip.Text;
            Session["Country"] = Country.Text;
            Session["State"] = State.Text;
            Session["City"] = City.Text;
            Session["CellNo"] = CellNo.Text;
            Session["Phoneno"] = Phoneno.Text;
            Session["Fax"] = Fax.Text;
            Session["Email"] = Email.Text;

            Session["Pass"] = Pass.Text;

            //Session["TariffPlanNametoSite"] = TariffPlanNametoSite.Text;

            Session["ActivationDate"] = ActivationDate.Text;
            //Session["CardDeliveryName"] = CardDeliveryName.Text;
            ////Session["CustomerName"] = CustomerName.Text;
            ////Session["BankAccType"] = BankAccType.Text;
            ////Session["BranchName"] = BranchName.Text;
            ////Session["BankName"] = BankName.Text;

            ////Session["Bank_Zipcode"] = Bank_Zipcode.Text;
            ////Session["Bank_city"] = Bank_city.Text;
            ////Session["Bank_State"] = Bank_State.Text;
            ////Session["BankRoutingNo"] = BankRoutingNo.Text;
            ////Session["BankAccountNo"] = BankAccountNo.Text;
            Session["Cardno"] = Cardno.Text;
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_ParkerDetails.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                Response.Redirect("Operator_parkerdetail.aspx");
            }






        }

        if (e.CommandName == "cmdInactive")
        {
            string user = e.CommandArgument.ToString();
            DataSet dscheckactive = new clsInsert().fetchrec("select [status] from tbl_UserLogin where Username='" + user + "'");
            if (dscheckactive.Tables[0].Rows[0][0].ToString() == "True")
            {
                countuserbill(user);
                updateuserstatus(user);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Deactivated Successfully')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Corporate Account Already Deactivated')", true);

            }

        }
    }
    string site, campus, lot, name;
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (chk_username.Checked == true )
        {
            name = txtbox_username.Text;
           
        }
         else
         {
                name = "";

          }
            
        if (chk_lot.Checked == true )
        {
           lot =ddl_lotname.SelectedValue; 

            }
            else
            {
              lot ="";
            }
        
        if (chk_campus.Checked == true )
        {
            campus =ddl_campusname.SelectedValue ;
            }
            else
            {
                campus ="";
            }
        
        if (chk_site.Checked == true )
        {
        site = ddl_sitename.SelectedValue ;

            }
            else
            {
                site = "";
            }




        DataTable dt = new clsInsert().getparkersbysite(name, lot,campus,site);
            int count = dt.Rows.Count;
            if (dt.Rows.Count > 0)
            {
                rpt_searchparker.Visible = true;
                
                Result.Visible = true;
                Result.Text = count.ToString() + " " + "Records Found";

                rpt_searchparker.DataSource = dt;
                rpt_searchparker.DataBind();

            }
            else
            {
               
                Result.Visible = false;
                rpt_searchparker.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);

            }



        



        //else if (chk_lot.Checked == true && chk_username.Checked == true && chk_campus.Checked == false && chk_site.Checked == false)
        //{
        //    DataTable dt = new clsInsert().getparkersbynameandlot(txtbox_username.Text, Convert.ToInt32(ddl_lotname.SelectedItem.Value));
        //    int count = dt.Rows.Count;
        //    if (dt.Rows.Count > 0)
        //    {
        //        Result.Visible = true;
        //        Result.Text = count.ToString() + " " + "Records Found";

        //        rpt_searchparker.DataSource = dt;
        //        rpt_searchparker.DataBind();

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
        //    }
        //}
        //else if (chk_campus.Checked == true && chk_username.Checked == true && chk_lot.Checked == false && chk_site.Checked == false)
        //{

        //    DataTable dt = new clsInsert().getparkersbynameandcampus(txtbox_username.Text, Convert.ToInt32(ddl_campusname.SelectedItem.Value));
        //    int count = dt.Rows.Count;
        //    if (dt.Rows.Count > 0)
        //    {
        //        Result.Visible = true;
        //        Result.Text = count.ToString() + " " + "Records Found";

        //        rpt_searchparker.DataSource = dt;
        //        rpt_searchparker.DataBind();

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
        //    }
        //}
        //else if (chk_site.Checked == true && chk_username.Checked == true && chk_campus.Checked == false && chk_lot.Checked == false)
        //{
        //    DataTable dt = new clsInsert().getparkersbynameandsite(txtbox_username.Text, Convert.ToInt32(ddl_sitename.SelectedItem.Value));
        //    int count = dt.Rows.Count;
        //    if (dt.Rows.Count > 0)
        //    {
        //        Result.Visible = true;
        //        Result.Text = count.ToString() + " " + "Records Found";

        //        rpt_searchparker.DataSource = dt;
        //        rpt_searchparker.DataBind();

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
        //    }
        //}
        //else if (chk_campus.Checked == true && chk_site.Checked == true && chk_username.Checked == false && chk_lot.Checked == false)
        //{
        //    DataTable dt = new clsInsert().getparkersbynameandsite(Convert.ToInt32(ddl_campusname.SelectedItem.Value), Convert.ToInt32(ddl_sitename.SelectedItem.Value));
        //    int count = dt.Rows.Count;
        //    if (dt.Rows.Count > 0)
        //    {
        //        Result.Visible = true;
        //        Result.Text = count.ToString() + " " + "Records Found";

        //        rpt_searchparker.DataSource = dt;
        //        rpt_searchparker.DataBind();

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
        //    }
        //}
        
    }
    protected void chk_username_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_username.Checked == true)
        {
            txtbox_username.Enabled = true;
        }
        else
        {
            txtbox_username.Enabled = false;
        }
    }
    protected void chk_lot_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_lot.Checked == true)
        {
            ddl_lotname.Enabled = true;
        }
        else
        {
            ddl_lotname.Enabled = false;
        }
    }
    protected void ddl_lotname_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void chk_campus_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_campus.Checked == true)
        {
            ddl_campusname.Enabled = true;
        }
        else
        {
            ddl_campusname.Enabled = false;
        }
    }
    protected void chk_site_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_site.Checked == true)
        {
            ddl_sitename.Enabled = true;
        }
        else
        {
            ddl_sitename.Enabled = false;
        }
    }
}
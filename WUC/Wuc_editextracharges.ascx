﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_editextracharges.ascx.cs" Inherits="WUC_Wuc_editextracharges" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

  .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    .style2
    {
        width: 119px;
    }
    .style3
    {
        width: 382px;
    }
</style>
<div class="form">
<label>Lot Name</label>
 <asp:TextBox ID="txtbox_lotname" runat="server" ValidationGroup="gp_country" CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
 <div class="clearfix"></div>
 <label> <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </cc1:ToolkitScriptManager>
            Charge Name</label>
             <asp:TextBox ID="txtbox_chargename" runat="server" ValidationGroup="gp_country" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_chargename_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_chargename" WatermarkText="Enter Charge Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_chargename" runat="server" 
                ControlToValidate="txtbox_chargename" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator>
                <div class="clearfix"></div>
<label> Charge Description</label>
 <asp:TextBox ID="txtbox_chargedescription" runat="server"  style="resize: none;"  ValidationGroup="gp_country" 
                CssClass="twitterStyleTextbox" TextMode="MultiLine" ></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_cuntryabbr_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_chargedescription" WatermarkText="Enter Charge Description" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <div class="clearfix">&nbsp;</div>
            <label> Charge Amount</label>
              <asp:TextBox ID="txtbox_chargeamount" runat="server" ValidationGroup="gp_country" 
                CssClass="twitterStyleTextbox" ></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                runat="server" Enabled="True" TargetControlID="txtbox_chargeamount" WatermarkText="Enter Charge Amount(0.00)" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="txtbox_chargeamount" 
                ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="gp_country"></asp:RequiredFieldValidator>
                <div class="clearfix"></div>
                <label>&nbsp;</label>
                <asp:Button ID="btn_submit" runat="server"  onclick="btn_submit_Click" 
                Text="Update" ValidationGroup="gp_country" CssClass="button" Height="35px" Width="79px"/>
</div>
<table class="form-table">


<tr>
        <td class="style2">
            
            </td>
        <td class="style3">
           
            
            
        </td>
    </tr>

    <tr>
        <td class="style2">
           </td>
        <td class="style3">
           
        </td>
    </tr>
   
    <tr>
        <td class="style2">
           </td>
        <td class="style3">
           
            
        </td>
    </tr>
    <tr>
        <td class="style2">
           </td>
        <td class="style3">
         
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style3">
            
        </td>
    </tr>
</table>
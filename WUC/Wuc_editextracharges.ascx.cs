﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_Wuc_editextracharges : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                //Session["chargeFor"] = l1.Text;
                //    Session["descr"] = l3.Text;

                //    Session["charges"] = l5.Text;
                txtbox_chargename.Text = Session["chargeFor"].ToString();
                txtbox_chargedescription.Text = Session["descr"].ToString();
                txtbox_chargeamount.Text = Session["charges"].ToString();
                txtbox_lotname.Text = Session["lotname"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_ExtraChargeMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_ExtraChargeMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
                //Response.Redirect("Country.aspx");
            }
        }
    }

    string status = "true";
    string msg;
    public void updateextracharge()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id",Convert.ToInt32( Session["id"].ToString()));

            hst.Add("chargefor", txtbox_chargename.Text);
            hst.Add("status", status);
            hst.Add("chargedesc", txtbox_chargedescription.Text);
            hst.Add("charges", Convert.ToDecimal(txtbox_chargeamount.Text));


            int result = objData.ExecuteNonQuery("[sp_Extracharges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_ExtraChargeMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_ExtraChargeMaster.aspx?msg=" + msg);

                }

              //  Response.Redirect("Admin_ExtraChargeMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
           // string msg = "1";
            updateextracharge();
           // Response.Redirect("Admin_ExtraChargeMaster.aspx?msg1=" + msg);

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);

        }
    }
}
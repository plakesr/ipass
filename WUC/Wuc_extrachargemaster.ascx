﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_extrachargemaster.ascx.cs" Inherits="WUC_Wuc_extrachargemaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

  .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    .style2
    {
        width: 119px;
    }
    .style3
    {
        width: 382px;
    }
    .button
    {}
</style>
 <cc1:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
            </cc1:ToolkitScriptManager>
<asp:Panel ID="Panel2" runat="server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<table class="form-table" width="100%">
<tr>
<td width="20%"><label> Select Customer</label></td>
<td> <asp:DropDownList ID="ddlSite" runat="server" AutoPostBack="True" CssClass="twitterStyleTextbox" 
                DataSourceID="SqlDataSource2" DataTextField="SiteName" DataValueField="SiteId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
            </asp:SqlDataSource>
            <div class="clearfix"></td>
</tr>
<tr>
<td><label> Select Campus</label></td>
<td><asp:DropDownList ID="ddlcampus" runat="server" AutoPostBack="True"  CssClass="twitterStyleTextbox" 
                DataSourceID="SqlDataSource3" DataTextField="Campus_Name" 
                DataValueField="CampusID" >
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [CampusID], [SiteID], [Campus_Name] FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID) order by Campus_Name">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlSite" Name="SiteID" 
                        PropertyName="SelectedValue" Type="Int64" />
                </SelectParameters>
            </asp:SqlDataSource>
              <div class="clearfix"></div></td>
</tr>
<tr>
<td><label>Select Lot</label></td>
<td> <asp:DropDownList ID="ddlLot" runat="server" DataSourceID="SqlDataSource4"  CssClass="twitterStyleTextbox" 
                DataTextField="LotName" DataValueField="Lot_id" >
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [Lot_id], [LotName] FROM [tbl_LotMaster] WHERE ([Campus_id] = @Campus_id)  order by LotName">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlcampus" Name="Campus_id" 
                        PropertyName="SelectedValue" Type="Int64" />
                </SelectParameters>
            </asp:SqlDataSource>
              <div class="clearfix"></div></td>
</tr>
<tr>
<td><label>Charge Name</label></td>
<td>   <asp:TextBox ID="txtbox_chargename" runat="server" ValidationGroup="gp_country" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_chargename_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_chargename" WatermarkText="Enter Charge Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_chargename" runat="server" 
                ControlToValidate="txtbox_chargename" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator>
                  <div class="clearfix"></div></td>
</tr>
<tr>
<td valign="middle"><label> Charge Description</label></td>
<td> <asp:TextBox ID="txtbox_chargedescription" runat="server"  style="resize: none;"  ValidationGroup="gp_country" 
                CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_cuntryabbr_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_chargedescription" WatermarkText="Enter Charge Description" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
              <div class="clearfix">&nbsp;</div></td>
</tr>
<tr>
<td><label> Charge Amount</label></td>
<td> <asp:TextBox ID="txtbox_chargeamount" runat="server" ValidationGroup="gp_country" 
                CssClass="twitterStyleTextbox" ></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                runat="server" Enabled="True" TargetControlID="txtbox_chargeamount" WatermarkText="Enter Charge Amount(0.00)" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="txtbox_chargeamount" 
                ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="gp_country"></asp:RequiredFieldValidator></td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
<table width="100%">
<tr>
<td width="20%">&nbsp;</td>
<td> <asp:Button ID="btn_submit" runat="server"  onclick="btn_submit_Click" 
                Text="Add" ValidationGroup="gp_country" CssClass="button" Height="35px" Width="79px"/></td>
</tr>
</table>


</asp:Panel>

<div style="width:100%; float:left;">
<div class="clearfix">&nbsp;</div>
<asp:Panel ID="Panel1" runat="server" >
<fieldset>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource1" 
                onitemcommand="Repeater1_ItemCommand">
 <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>

                 <th>
                       Lot&nbsp;&nbsp;Name
                    </th>
                <th>
                       Charge&nbsp;&nbsp;Name
                    </th>
                      <th>
                        Charge&nbsp;&nbsp;Description
                    </th>
                     <th>
                        Charge &nbsp;&nbsp;Amount
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>  <%#Eval("LotName")%></td>
            <td>     <%#Eval("chargeFor")%></td>
                <td>    <%#Eval("descr")%></td>
                  <td>  <%#Eval("charges")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>

                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' 
                                        Visible="False" />
                    </td>
            </tr>
            <asp:Label ID="id" runat="server" Text='<%# Eval("id") %>' 
                                        Visible="False" />
           
  <asp:Label ID="chargeFor" runat="server" Text='<%# Eval("chargeFor") %>' 
                                        Visible="False"></asp:Label>
 
    <asp:Label ID="descr" runat="server" Text='<%# Eval("descr") %>' 
                                        Visible="False"></asp:Label> 
  
           <asp:Label ID="charges" runat="server" Text='<%# Eval("charges") %>' 
                                        Visible="False"></asp:Label>
        <asp:Label ID="lotname" runat="server" Text='<%# Eval("LotName") %>' 
                                        Visible="False"></asp:Label>
       </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
    
                SelectCommand="SELECT tbl_extracharges.id, tbl_extracharges.lotid, tbl_extracharges.chargeFor, tbl_extracharges.descr, tbl_extracharges.charges, tbl_extracharges.status, tbl_LotMaster.LotName FROM tbl_extracharges INNER JOIN tbl_LotMaster ON tbl_LotMaster.Lot_id = tbl_extracharges.lotid"></asp:SqlDataSource>
    </ContentTemplate>
        
    </asp:UpdatePanel>
    </fieldset>
</asp:Panel></div>
<div class="clearfix">&nbsp;</div>
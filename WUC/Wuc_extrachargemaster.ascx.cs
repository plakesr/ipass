﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_Wuc_extrachargemaster : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg1"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }

        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
           // string msg = "1";
            addextracharge();
          //  Response.Redirect("Admin_ExtraChargeMaster.aspx?message=" + msg);

        }
        catch
        {
        }
    }
    string status = "true";
    string msg;
    public void addextracharge()
    {
        try
        {
            hst.Clear();
        
            hst.Add("action", "insert");
            hst.Add("lotid", ddlLot.SelectedValue);

            hst.Add("chargefor", txtbox_chargename.Text);
            hst.Add("status",status);
            hst.Add("chargedesc", txtbox_chargedescription.Text);
            hst.Add("charges", Convert.ToDecimal(txtbox_chargeamount.Text));


            int result = objData.ExecuteNonQuery("[sp_Extracharges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_ExtraChargeMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_ExtraChargeMaster.aspx?message=" + msg);

                }
               // Response.Redirect("Admin_ExtraChargeMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
       public void delextracharge(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_Extracharges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                
                
               // Response.Redirect("Admin_ExtraChargeMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

              //  Response.Redirect("Admin_ExtraChargeMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

          //  Response.Redirect("Admin_ExtraChargeMaster.aspx");

        }


    }
       protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
       {
           int i = e.Item.ItemIndex;
           if (e.CommandName == "cmdDelete")
           {
               int ID = Convert.ToInt32(e.CommandArgument.ToString());
               try
               {
                   if (Session["Type"].ToString() == "admin")
                   {
                       delextracharge(ID);
                       Response.Redirect("Admin_ExtraChargeMaster.aspx");
                   }
                   if (Session["Type"].ToString() == "operator")
                   {
                       if (Session["lbldelete"].ToString() == "False")
                       {
                           ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                       }

                       else
                       {
                           delextracharge(ID);
                           Response.Redirect("Operator_ExtraChargeMaster.aspx");
                       }
                   }

               }
               catch
               {
                   // Response.Redirect("default.aspx");
               }
               
           }
           if (e.CommandName == "cmdEdit")
           {

               Label l1 = rpt1.Items[i].FindControl("chargeFor") as Label;


               Label l3 = rpt1.Items[i].FindControl("descr") as Label;

               Label l5 = rpt1.Items[i].FindControl("charges") as Label;
               Label l6 = rpt1.Items[i].FindControl("id") as Label;
               Label lotname=rpt1.Items[i].FindControl("lotname")as Label;
               try
               {

                   Session["chargeFor"] = l1.Text;
                   Session["descr"] = l3.Text;
                   Session["lotname"] = lotname.Text;
                   Session["charges"] = l5.Text;

                   Session["id"] = l6.Text;
               }
               catch
               {
                   Response.Redirect("Default.aspx");

               }
               if (Session["Type"].ToString() == "admin")
               {
                   Response.Redirect("Admin_EditExtraChargeMaster.aspx");
               }
               if (Session["Type"].ToString() == "operator")
               {
                   if (Session["lbledit"].ToString() == "False")
                   {
                       ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                   }
                   else
                   {
                       Response.Redirect("Operator_EditExtraChargeMaster.aspx");
                   }

               }
               //Response.Redirect("Admin_EditExtraChargeMaster.aspx");
           }
       }
}
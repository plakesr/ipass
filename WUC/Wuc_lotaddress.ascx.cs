﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_Wuc_lotaddress : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Search_Click(object sender, EventArgs e)
    {
        Session["addressname"] = autocomplete.Text;
        try
        {
            DataSet _ds = new DataSet("MyDataSet");
            _ds.ReadXml(@"http://maps.google.com/maps/api/geocode/xml?address=" + autocomplete.Text);

            //Label1.Text = _ds.Tables["location"].Rows[0][0].ToString();
            //Label2.Text = _ds.Tables["location"].Rows[0][1].ToString();
            Session["latitude"] = _ds.Tables["location"].Rows[0][0].ToString();
            Session["longitude"] = _ds.Tables["location"].Rows[0][1].ToString();
            Session["Cityname"] = _ds.Tables["address_component"].Rows[0][1].ToString();
            Response.Redirect("AvailableLotSpace.aspx");
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter correct  address.')", true);
            
        }
       
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Wuc_screen2.ascx.cs" Inherits="WUC_Wuc_screen2" %>
<style type="text/css">
    .style1
    {
        width: 121%;
        height: 316px;
    }
    .style3
    {
        width: 100%;
        height: 147px;
    }
    .style4
    {
        width: 203px;
    }
    .style9
    {
        width: 180px;
    }
    .style10
    {
        width: 100%;
    }
    .style11
    {
        width: 185px;
    }
    .style12
    {
        width: 176px;
    }
    .style13
    {
        width: 219px;
    }
    .style14
    {
        width: 232px;
    }
    .style15
    {
        width: 215px;
    }
    .style30
    {
        width: 54px;
    }
    .style34
    {
        width: 170px;
    }
    .style35
    {
        width: 3px;
    }
    .style37
    {
        width: 189px;
    }
    .style38
    {
        width: 80px;
    }
    .style39
    {
        width: 1px;
    }
</style>

<asp:Panel ID="Panel1" runat="server" Height="380px">
<fieldset>
<legend>Personal Information</legend>
    <table class="style1">
        <tr>
            <td class="style34">
          
                &nbsp;<asp:RequiredFieldValidator ID="selectaccount" runat="server" 
                    ControlToValidate="txtbox_zip" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp; Select Account</td>
            <td class="style37">
                <asp:RadioButton ID="rb_account" runat="server" Text="Individual" GroupName="a" 
                    oncheckedchanged="rb_account_CheckedChanged" ValidationGroup="b" /> &nbsp; &nbsp; &nbsp;
                <asp:RadioButton ID="rb_account1" runat="server" Text="Corporate" GroupName="a"  
                    oncheckedchanged="rb_account1_CheckedChanged" ValidationGroup="b" />
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                &nbsp;<asp:RequiredFieldValidator ID="rfv_firstname" runat="server" 
                    ControlToValidate="txtBox_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp; First Name:</td>
            <td class="style37">
                <asp:TextBox ID="txtBox_firstname" runat="server" 
                     ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style38">
               
                <asp:RequiredFieldValidator ID="rfv_lastname" runat="server" 
                    ControlToValidate="txtbox_lastname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                Last Name:</td>
            <td class="style39">
                <asp:TextBox ID="txtbox_lastname" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                <span ID="Grp_AccountInformation_lblContactName">&nbsp;&nbsp; Card Number(If Issued)</span></td>
            <td class="style37">
                <asp:TextBox ID="txtbox_coupannumber" runat="server"></asp:TextBox>
            </td>
            <td class="style38">
                </td>
            <td class="style39">
                </td>
            <td class="style30">
                </td>
            <td class="style9">
                </td>
        </tr>
        <tr>
            <td class="style34">
                <asp:RequiredFieldValidator ID="rfv_address2" runat="server" 
                    ControlToValidate="txtbox_address1" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp; Address1</td>
            <td class="style37">
                <asp:TextBox ID="txtbox_address1" runat="server"  TextMode="MultiLine" 
                    ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                &nbsp;&nbsp;&nbsp;&nbsp; Address2</td>
            <td class="style37">
                <asp:TextBox ID="txtbox_address2" runat="server"  TextMode="MultiLine"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                
                <asp:RequiredFieldValidator ID="rfv_postal" runat="server" 
                    ControlToValidate="txtbox_postal" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp;Postal Code/Zip Code</td>
            <td class="style37">
                <asp:TextBox ID="txtbox_postal" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;<asp:RequiredFieldValidator ID="rfv_city" runat="server" 
                    ControlToValidate="txtbox_zip" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                City</td>
            <td class="style39">
                <asp:TextBox ID="txtbox_city" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                <asp:RequiredFieldValidator ID="rfv_state0" runat="server" 
                    ControlToValidate="ddl_state" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp; State</td>
            <td class="style37">
                <asp:DropDownList ID="ddlstate" runat="server" ValidationGroup="a">
                </asp:DropDownList>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                &nbsp;&nbsp;&nbsp;&nbsp; Country</td>
            <td class="style37">
                <asp:TextBox ID="txtbox_country" runat="server"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                &nbsp;<asp:RequiredFieldValidator ID="rfv_cellular" runat="server" 
                    ControlToValidate="txtbox_cellular" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp; Cellular No.</td>
            <td class="style37">
                <asp:TextBox ID="txtbox_cellular" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style38">
                Phone No.</td>
            <td class="style39">
                <asp:TextBox ID="txtbox_phone" runat="server"></asp:TextBox>
            </td>
            <td class="style30">
                Fax No.</td>
            <td class="style9">
                <asp:TextBox ID="txtbox_fax" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style34">
                &nbsp;<asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                    ControlToValidate="txtbox_Email" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp; Email Id</td>
            <td class="style37">
                <asp:TextBox ID="txtbox_Email" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td class="style39">
                &nbsp;</td>
            <td class="style30">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
    </table>

    </fieldset>
</asp:Panel>
<br />

<asp:Panel ID="Panel2" runat="server" Height="201px">
<fieldset>
<legend>&nbsp;Login Information</legend>
    <table class="style3">
        <tr>
            <td class="style4">
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtBox_username" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp;&nbsp; User Name:</td>
            <td>
                <asp:TextBox ID="txtBox_username" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style4">
                <asp:RequiredFieldValidator ID="rfv_password" runat="server" 
                    ControlToValidate="txtBox_Passwrd" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp;&nbsp; Password:</td>
            <td>
                <asp:TextBox ID="txtBox_Passwrd" runat="server" ValidationGroup="a"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style4">
                <asp:RequiredFieldValidator ID="rfv_confirmpassword" runat="server" 
                    ControlToValidate="txtBox_cnfrmpasswrd" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                &nbsp;&nbsp; Confirm Password:</td>
            <td>
                <asp:TextBox ID="txtBox_cnfrmpasswrd" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:CompareValidator ID="cmp_confirmpwd" runat="server" 
                    ControlToCompare="txtBox_Passwrd" ControlToValidate="txtBox_cnfrmpasswrd" 
                    ErrorMessage="password does not match" ForeColor="#CC0000" ValidationGroup="a"></asp:CompareValidator>
            </td>
        </tr>
    </table>
    </fieldset>
</asp:Panel>


<asp:Panel ID="Panel3" runat="server" >
<fieldset>
<legend>
    Billing Information</legend>
    <br />
    <table class="style10">
        <tr>
            <td class="style13">
                <asp:RadioButton ID="rbtn_creditcard" runat="server" 
                    oncheckedchanged="rbtn_creditcard_CheckedChanged" 
                    Text="Auto Charge to Credit Card " AutoPostBack=true GroupName="s" />
            </td>
            <td class="style12">
                <asp:RadioButton ID="rbtn_invoicebymail" runat="server" Text="Invoice By Email" 
                    GroupName="s" />
            </td>
            <td class="style11">
                <asp:RadioButton ID="rbtn_invoicebyemail" runat="server" 
                    Text="Invoice By Mail "  GroupName="s" />
            </td>
            <td>
                <asp:RadioButton ID="rbtn_ach" runat="server" Text="Auto Charge to ACH" 
                     oncheckedchanged="rbtn_ach_CheckedChanged" AutoPostBack=true  GroupName="s" />
            </td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Button ID="btn_entercreditcard" runat="server" Text="Enter Credit Card" 
                    Visible="False" onclick="btn_entercreditcard_Click" />
            </td>
            <td class="style12">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
<br />
<br />
<asp:Panel ID="Panel4" runat="server" style="height: 192px; width: 933px" 
    Visible="False">
<fieldset>
<legend>
    Direct&nbsp; Debit Bank Information</legend>
    <br />
    <table class="style10">
        <tr>
            <td class="style9">
                Customer Name</td>
            <td class="style15">
                <asp:TextBox ID="txtbox_customername" runat="server"></asp:TextBox>
            </td>
            <td class="style14">
                Bank Account Type</td>
            <td>
                <asp:DropDownList ID="ddl_accounttype" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style9">
                Bank Name</td>
            <td class="style15">
                <asp:TextBox ID="txtbox_bankname" runat="server"></asp:TextBox>
            </td>
            <td class="style14">
                Branch</td>
            <td>
                <asp:TextBox ID="txtbox_branch" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style9">
                Zip/Postal</td>
            <td class="style15">
                <asp:TextBox ID="txtbox_zip" runat="server"></asp:TextBox>
            </td>
            <td class="style14">
                City</td>
            <td>
                <asp:TextBox ID="txtbox_cityinbankdetails" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style9">
                State/Province</td>
            <td class="style15">
                <asp:DropDownList ID="ddl_state" runat="server">
                </asp:DropDownList>
            </td>
            <td class="style14">
                Bank Routing No</td>
            <td>
                <asp:TextBox ID="txtbox_routingno" runat="server" 
                    ontextchanged="TextBox9_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style9">
                Bank Account No</td>
            <td class="style15">
                <asp:TextBox ID="txtbox_bankaccountno" runat="server"></asp:TextBox>
            </td>
            <td class="style14">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


<asp:Button ID="btn_pre" runat="server" Text="Previous" 
    onclick="btn_pre_Click" />
&nbsp;&nbsp;

<asp:Button ID="btn_next" runat="server" Text="Next" ValidationGroup="a" 
    onclick="btn_next_Click" />


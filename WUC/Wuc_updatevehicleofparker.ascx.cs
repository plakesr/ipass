﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_Wuc_updatevehicleofparker : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            for (int i = 1980; i < 2020; i++)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                ddl_year.Items.Add(li);
            }

         ddl_make.SelectedValue  = Session["make"].ToString();
            txtbox_model.Text = Session["model"].ToString();
          ddl_color.SelectedValue  = Session["color"].ToString();
         

            ddl_year.SelectedValue = Session["year"].ToString();
            txtbox_license.Text = Session["licenseno"].ToString();
        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {


        try
        {
            updatevehicel();
        }
        catch
        {
        }

    }

    public void updatevehicel()
    {

        hst.Clear();
        hst.Add("action", "update");
        hst.Add("id",Convert.ToInt32(Session["vehicleid"].ToString()));
     
        hst.Add("Make", ddl_make.SelectedItem.Text);
        hst.Add("model", txtbox_model.Text);
        hst.Add("year",ddl_year.SelectedItem.Text);
        hst.Add("color", ddl_color.SelectedItem.Text);
        hst.Add("licenseno", txtbox_license.Text);
        int result = objData.ExecuteNonQuery("[sp_VehicleParker]", CommandType.StoredProcedure, hst);
        if (result > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('updated successfully done')", true);
            //Response.Redirect("UserManager.aspx");

        }
        else
        {
            // lbl_error.Visible = true;

        }
    }
}
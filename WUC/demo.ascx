﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="demo.ascx.cs" Inherits="WUC_demo" %>
 <%--<%@ Register src="wuc_lotdistance_rootmap.ascx" tagname="wuc_lotdistance_rootmap" tagprefix="uc2" %>--%>

 <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body, #map-canvas {
        margin: 0px;
        padding: 0px
        }
    </style>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initialize() {
            // Create the autocomplete object, restricting the search
            // to geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
            // When the user selects an address from the dropdown,
            // populate the address fields in the form.
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                fillInAddress();
            });
        }

        // [START region_fillform]
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
        // [END region_fillform]

        // [START region_geolocation]
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
                    autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
                });
            }
        }
        // [END region_geolocation]

    </script>

    <style>
      #locationField, #controls {
        position: relative;
        width: 480px;
      }
      #autocomplete {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 99%;
      }
      .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
      }
      #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
      }
      #address td {
        font-size: 10pt;
      }
      .field {
        width: 99%;
      }
      .slimField {
        width: 80px;
      }
      .wideField {
        width: 200px;
      }
      #locationField {
        height: 20px;
        margin-bottom: 2px;
      }
      .header
      {
          width:100%;
        color:#ec6d58;
        font-size:16px;
        margin-bottom:10px;
      }
     .address-list{ margin:0px 10px 0 0; padding:0px; } 
     .address-list td{ background:#fafafa; padding:5px 10px; font-size:13px; border-bottom:solid 1px #ededed; color:#707070;}
      .address-list td a{ color:#ec6d58; }
      
      .map-address{ width:400px; min-height:100px;}
      .address{ width:340px; float:left}
      .map-photo{ width:60px; height:70px; float:right}
       .address .button{padding:3px 5px !important; margin:10px 3px 0 0; font-size:11px;}
    </style>


 <script type="text/javascript">
    var markers = [
    <asp:Repeater ID="rptMarkers" runat="server">
    <ItemTemplate>
             {
                "title": '<%# Eval("LotName") %>',
                "lat": '<%# Eval("Lot_Latitude") %>',
                "lng": '<%# Eval("Lot_Longitude") %>',
                "description": '<%# Eval("Address") %>',
                  "latandlong":"<div class=map-address>"+"<div class=header>"+'<%# Eval("LotName") %>'+"</div>"+"<div class=address>"+'<%# Eval("Address") %>'+"<br>"+'<asp:LinkButton PostBackUrl='<%# Eval("Lot_id","../Lot_Details.aspx?Lot_id={0}") %>' runat="server" cssclass="button">Lot Details </asp:LinkButton>'+'<asp:LinkButton PostBackUrl='<%# Eval("Lot_id","../Lot_distance_rootmap.aspx?Lot_id={0}") %>' runat="server" cssclass="button">Route Details</asp:LinkButton>'+'<asp:LinkButton PostBackUrl='<%# Eval("Lot_id","../getlotroot.aspx?Lot_id={0}") %>' runat="server" cssclass="button">Route Map</asp:LinkButton>'+"</div>"+"<div class=map-photo>"+'<img src=//maps.googleapis.com/maps/api/streetview?size=60x70&amp;location=<%# Eval("Lot_Latitude") %>,<%# Eval("Lot_Longitude") %>&amp;fov=90&amp;heading=235&amp;pitch=10">'+"</div>"+"</div>"
              
            }
    </ItemTemplate>
    <SeparatorTemplate>
        ,
    </SeparatorTemplate>
    </asp:Repeater>
    ];
 </script>
    <script type="text/javascript">
    
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.latandlong);
                       
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
  <div class="lot-area">  
<div class="lotsearch-left">

<div class="lot-listing">
                <asp:DataList ID="DataList1"  BackColor="White" CssClass="address-list"
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    ForeColor="Black" GridLines="Vertical"  OnItemCommand="Item_Command" 
                    runat="server" >
                   
                    <AlternatingItemStyle BackColor="White" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#363636" Height="40px" ForeColor="white" Font-Bold="true" />
                    <HeaderTemplate>
                       Available Lots in Current City 
                    </HeaderTemplate>
                    <ItemStyle BackColor="#f9f9f9" />
                    <SelectedItemStyle BackColor="#e9e9e9" />
                    <ItemTemplate>
            Lot Name:      <%# Eval("LotName") %><br />
                Address:   <%# Eval("Address") %><br />Total Space <%# Eval("TotalSpaces") %><br />
                Available space:<%#Eval("UnableSpace")%><br />
                        <asp:LinkButton ID="detail" CommandName="cmdRouteDetails" CommandArgument='<%#Eval("Lot_id") %>' runat="server">Route Details</asp:LinkButton>
                      |
                        <asp:LinkButton ID="LinkButton1" CommandName="cmdLotdetails" CommandArgument='<%#Eval("Lot_id") %>' runat="server">Lot Details</asp:LinkButton>
                         <asp:Label ID="lotid" runat="server" Text='<%# Eval("Lot_id") %>' 
                                        Visible="False" />
                        <asp:Label ID="LotName" runat="server" Text='<%# Eval("LotName") %>' 
                                        Visible="False" />
                        <asp:Label ID="Lot_Latitude" runat="server" Text='<%# Eval("Lot_Latitude") %>' 
                                        Visible="False" />
                         <asp:Label ID="Lot_Longitude" runat="server" Text='<%# Eval("Lot_Longitude") %>' 
                                        Visible="False" />
                                        <asp:Label ID="address" runat="server" Text='<%# Eval("Address") %>' 
                                        Visible="False" />

                                         <asp:Label ID="cityname" runat="server" Text='<%# Eval("City_name") %>' 
                                        Visible="False" />

                                         <asp:Label ID="SpaceAvailable" runat="server" Text='<%# Eval("UnableSpace") %>' 
                                        Visible="False" />

                    </ItemTemplate>
                </asp:DataList>
                </div></div>
                <div class="map-area">
            
                 <div id="dvMap" class="map">
                
    </div>
   

</div>
</div>
<div class="footer-container"></div>

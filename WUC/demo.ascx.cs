﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;

public partial class WUC_demo : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
          //Lot Name:      <%# Eval("LotName") %><br />
          //      Address:   <%# Eval("Address") %><br />Total Space <%# Eval("TotalSpaces") %><br />
          //              <asp:LinkButton ID="detail" CommandName="cmdRouteDetails" CommandArgument='<%#Eval("Lot_id") %>' runat="server">Route Details</asp:LinkButton>
          //            |
          //              <asp:LinkButton ID="LinkButton1" CommandName="cmdLotdetails" CommandArgument='<%#Eval("Lot_id") %>' runat="server">Lot Details</asp:LinkButton>
          //               <asp:Label ID="lotid" runat="server" Text='<%# Eval("Lot_id") %>' 
          //                              Visible="False" />
          //              <asp:Label ID="LotName" runat="server" Text='<%# Eval("LotName") %>' 
          //                              Visible="False" />
          //              <asp:Label ID="Lot_Latitude" runat="server" Text='<%# Eval("Lot_Latitude") %>' 
          //                              Visible="False" />
          //               <asp:Label ID="Lot_Longitude" runat="server" Text='<%# Eval("Lot_Longitude") %>' 
          //                              Visible="False" />
          //                              <asp:Label ID="address" runat="server" Text='<%# Eval("address") %>' 
          //                              Visible="False" />

          //                               <asp:Label ID="cityname" runat="server" Text='<%# Eval("City_name") %>' 
          //                              Visible="False" />


        DataTable dtadd = new DataTable();
        dtadd.Columns.Add("LotName", typeof(string));
        dtadd.Columns.Add("Address", typeof(string));
        dtadd.Columns.Add("TotalSpaces", typeof(string));
        dtadd.Columns.Add("Lot_id", typeof(int));
      
        dtadd.Columns.Add("Lot_Latitude", typeof(string));
        dtadd.Columns.Add("Lot_Longitude", typeof(string));
        dtadd.Columns.Add("City_name", typeof(string));
        dtadd.Columns.Add("UnableSpace", typeof(string));
        dtadd.Columns.Add("LotCode", typeof(string));

        if (!IsPostBack == true)
        {
            try
            {
                if (Request.QueryString["msg_time"].ToString() == "true")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Lot Is Not Active Yet,please try another lot')", true);
                }
            }
            catch
            {
            }

            
        }

        try
        {
            string add1 = Session["addressname"].ToString();

            string strText = add1;
            string[] strArr = strText.Split(',');

            for (int i = 0; i < strArr.Length; i++)
            {
                Session["currentlocationname"] = add1;
               // DataTable dt = this.GetData("select tbl_LotMaster.*,tbl_City.City_name from tbl_LotMaster inner join tbl_City on tbl_LotMaster.City=tbl_City.City_id inner join tbl_Province on tbl_City.Province_id=tbl_Province.ProvinceId inner join tbl_country on tbl_Province.Country_id=tbl_country.Country_id where tbl_City.City_name like '%" + strText.ToString().TrimEnd() + "%' or tbl_Province.ProvinceName like '%" + strText.ToString().TrimEnd() + "%' or tbl_country.Country_name like '%" + strText.ToString().TrimEnd() + "%' or tbl_LotMaster.[Address] like '%" + strText.ToString().TrimEnd() + "%'");
               DataTable dt = this.GetData("select tbl_LotMaster.*,tbl_City.City_name from tbl_LotMaster inner join tbl_City on tbl_LotMaster.City=tbl_City.City_id inner join tbl_Province on tbl_City.Province_id=tbl_Province.ProvinceId inner join tbl_country on tbl_Province.Country_id=tbl_country.Country_id where tbl_City.City_name='" + strArr[0].ToString().Trim() + "'or tbl_Province.ProvinceName='" + strArr[0].ToString().Trim() + "'or tbl_country.Country_name='" + strArr[0].ToString().Trim() + "'or tbl_LotMaster.[Address] like '%" + strText.ToString().TrimEnd() + "%'");

               // DataTable dt = this.GetData("select tbl_LotMaster.*,tbl_City.City_name from tbl_LotMaster inner join tbl_City on tbl_LotMaster.City=tbl_City.City_id where tbl_City.City_name='" + strArr[0].ToString().Trim() + "'");
                if (dt.Rows.Count > 0)
                {

                    Session["currentcity"] = dt.Rows[0]["City_name"].ToString();

                    rptMarkers.DataSource = dt;
                    rptMarkers.DataBind();

                    for (int j = 0; j < dt.Rows.Count; j++)
                    {

                        
                        DataRow dr1 = dtadd.NewRow();
                        dr1["LotName"] = dt.Rows[j]["LotName"].ToString();
                        dr1["Address"] = dt.Rows[j]["Address"].ToString();
                        if (dt.Rows[j]["UnableSpace"].ToString() != "0")
                        {
                            int lottotalspace = Convert.ToInt32(dt.Rows[j]["TotalSpaces"].ToString());
                            int lotunablespace = Convert.ToInt32(dt.Rows[j]["UnableSpace"].ToString());
                            int availabeltotalspace = lottotalspace - lotunablespace;
                            dr1["TotalSpaces"] = availabeltotalspace;
                        }
                       
                        dr1["Lot_id"] = Convert.ToInt32( dt.Rows[j]["Lot_id"].ToString());
                        dr1["Lot_Latitude"] = dt.Rows[j]["Lot_Latitude"].ToString();
                        dr1["Lot_Longitude"] = dt.Rows[j]["Lot_Longitude"].ToString();
                        dr1["City_name"] = dt.Rows[j]["City_name"].ToString();
                        dr1["LotCode"] = dt.Rows[j]["Lotcode"].ToString();

                        DataSet dscount = new clsInsert().fetchrec("select count(tbl_ParkerRegis.UserName) as countspace from tbl_ParkerRegis where LotId=" + Convert.ToInt32(dt.Rows[j]["Lot_id"].ToString()));
                        if (dscount.Tables.Count > 0)
                        {
                            int lottotalspace = Convert.ToInt32(dt.Rows[j]["TotalSpaces"].ToString());
                            int lotunablespace = Convert.ToInt32(dt.Rows[j]["UnableSpace"].ToString());
                            int availabeltotalspace = lottotalspace - lotunablespace;

                            int totalspace = availabeltotalspace;
                            int counttotal = Convert.ToInt32(dscount.Tables[0].Rows[0][0].ToString());


                           // int counttotal = Convert.ToInt32(70);
                            int countspace = totalspace - counttotal;
                            if (countspace >= 0)
                            {
                                dr1["UnableSpace"] = Convert.ToString(countspace);
                            }
                            if (countspace < 0)
                            {
                                dr1["UnableSpace"] = "No Space";
                            }
                        }
                        dtadd.Rows.Add(dr1);
                    }
                    RemoveDuplicateRows(dtadd, "LotCode");
                    DataList1.DataSource = dtadd;
                    DataList1.DataBind();
                   
                }
                else
                {


                    DataRow dr = dt.NewRow();
                    dr["LotName"] = Session["Cityname"].ToString();
                    dr["Lot_Latitude"] = Session["latitude"].ToString();
                    dr["Lot_Longitude"] = Session["longitude"].ToString();
                    dr["Address"] = Session["Cityname"].ToString();
                    dr["Lot_id"] = "0";
                    dr["UnableSpace"] = Convert.ToInt32(0);
                    dr["LotCode"] = "0";
                    

                        dt.Rows.Add(dr);



                    rptMarkers.DataSource = dt;
                    rptMarkers.DataBind();
                   DataList1.DataSource = dt;
                  DataList1.DataBind();
                 ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found at this address.')", true);
                    
                }

            }
        }
        catch
        {
        }

    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    private DataTable GetData(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        SqlCommand cmd = new SqlCommand(query);
        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;

                sda.SelectCommand = cmd;
                using (DataTable dt = new DataTable())
                {
                    sda.Fill(dt);
                    return dt;
                }
            }
        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
       
    }
    protected void Item_Command(Object sender, DataListCommandEventArgs e)
    {
        DataList1.SelectedIndex = e.Item.ItemIndex;
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdRouteDetails")
        {
            Label l1 = DataList1.Items[i].FindControl("Lot_Latitude") as Label;
            Label l2 = DataList1.Items[i].FindControl("Lot_Longitude") as Label;
            Label l3 = DataList1.Items[i].FindControl("address") as Label;
            Label cityname = DataList1.Items[i].FindControl("cityname") as Label;
            Session["Lot_Latitude"] = l1.Text;
            Session["Lot_Longitude"] = l2.Text;
            Session["address"] = l3.Text+","+cityname.Text;

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Lot_distance_rootmap.aspx','New Windows','height=380, width=300,location=no');", true);

          
        }

        if (e.CommandName == "cmdLotdetails")
        {
            Label lotid = DataList1.Items[i].FindControl("lotid") as Label;
            Label SpaceAvailable = (Label)DataList1.Items[i].FindControl("SpaceAvailable");
         //   Session["Space Available"] = SpaceAvailable.Text;
            //if (SpaceAvailable.Text == "No Space")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Space Available at this address.')", true);

            //}
            //else
            //{
                Response.Redirect("Lot_Details.aspx?Lot_id=" + Convert.ToInt32(lotid.Text));
            //}

           
        }

    }
   
}
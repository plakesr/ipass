﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="graph.ascx.cs" Inherits="WUC_graph" %>
<asp:Chart ID="Chart1" runat="server">
    <Series>
        <asp:Series Name="Series1"  XValueMember="modifiedby" 
            YValueMembers="totalparker" ChartType="StackedColumn" >
        </asp:Series>
    </Series>
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1">
        <AxisY Title="Number Of Parkers"></AxisY><AxisX Title="Operator's Name"></AxisX>
        </asp:ChartArea>
    </ChartAreas>
</asp:Chart>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="submenu.ascx.cs" Inherits="WUC_submenu" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>

<table class="style1">
<tr>
        <td>
            
            Menu Name</td>
        <td>
            
            <asp:DropDownList ID="ddlmenu" runat="server" DataSourceID="SqlDataSource1" 
                DataTextField="menuname" DataValueField="id">
                   <asp:ListItem Selected = "True" Text = "------Select Menu Name------" Value = "0"></asp:ListItem>
            </asp:DropDownList>
            
            <cc1:DropDownExtender ID="ddlmenu_DropDownExtender" runat="server" 
                DynamicServicePath="" Enabled="True" TargetControlID="ddlmenu">
            </cc1:DropDownExtender>
            
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [id], [menuname] FROM [tbl_menus]">
            </asp:SqlDataSource>
            
        </td>
    </tr>
    <tr>
        <td>
            Sub
            Menu Name</td>
        <td>
            <asp:TextBox ID="txtBox_subMenu" runat="server"></asp:TextBox>
        </td>
    </tr>

    <tr>
        <td>
            NavigationUrl</td>
        <td>
            <asp:TextBox ID="txtbox_navigation" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_submit" runat="server" onclick="btn_submit_Click" 
                Text="Submit" style="height: 26px" />
        </td>
    </tr>
</table>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
public partial class WUC_submenu : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {

            addsubMenu();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void clear()
    {
        txtbox_navigation.Text = "";
        txtBox_subMenu.Text = "";
    }
    public void addsubMenu()
    {
        try
        {
            hst.Clear();
           
            hst.Add("action", "insert");
            hst.Add("parentid", ddlmenu.SelectedValue);
            hst.Add("menuname", txtBox_subMenu.Text);
            hst.Add("navigationurl", txtbox_navigation.Text);

            int result = objData.ExecuteNonQuery("[sp_SubMen]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                Response.Redirect("SubMenuMaster.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
}
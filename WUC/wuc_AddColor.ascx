﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_AddColor.ascx.cs" Inherits="WUC_wuc_AddColor" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

    .style2
    {
        width: 130px;
    }
    .form-table
    {
        width: 64%;
    }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>


<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

  
<asp:Panel ID="Panel1" runat="server">
<div class="form">
<label>Color Name :</label>
 <asp:TextBox ID="txtbox_colorname" runat="server" 
                    ontextchanged="TextBox1_TextChanged" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" Width="119px"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_colorname_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_colorname" WatermarkCssClass="watermark" WatermarkText="Enter Color Name">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_color" runat="server" 
                    ControlToValidate="txtbox_colorname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                    <label>&nbsp;</label>
                    <asp:Button ID="btn_color" runat="server" Text="Add" CssClass="button"
                    onclick="btn_color_Click" ValidationGroup="a"/>
</div>
    
  
    </asp:Panel>


<div style="width:100%; float:left;">
<asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical">
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="SELECT * FROM [tbl_Vehicle_color]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource1" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table  width="100%" class="timezone1-table" cellpadding="5" cellspacing="0">
                <tr>
                
                      <th>
                       Vehicle&nbsp;&nbsp;Name
                    </th>
                    
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
           
            <td>
                    <%#Eval("VehiColor")%>
                    
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("VehiColorid") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("VehiColorid") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>
                    <asp:Label ID="idLabel" runat="server" Text='<%# Eval("VehiColorid") %>' 
                                        Visible="False" />
                    <asp:Label ID="idlabel1" runat="server" Text='<%# Eval("VehiColor") %>' 
                                        Visible="False"></asp:Label>
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
</div>
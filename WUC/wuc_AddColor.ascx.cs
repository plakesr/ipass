﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_AddColor : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }

            if (!IsPostBack == true)
            {
                try
                {
                    
                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel1.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = false;
                    }

                   

                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                           // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                      //  Response.Redirect("default.aspx");
                    }
                }
            }


        }
               
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    string msg;
    protected void btn_color_Click(object sender, EventArgs e)
    {
        try
        { 
           
            addvehicleColor();
         
            clear();
           

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void clear()
    {
        txtbox_colorname.Text = "";
        
    }
    public void addvehicleColor()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("vehi_color", txtbox_colorname.Text);
            

            int result = objData.ExecuteNonQuery("[Sp_vehicleColor]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleColor.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_VehicleColor.aspx?message=" + msg);

                }
                
          
             

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

           

        }
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
                int ID = Convert.ToInt32(e.CommandArgument.ToString());
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        delColor(ID);
                        Response.Redirect("Admin_VehicleColor.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        if (Session["lbldelete"].ToString() == "False")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                        }

                        else
                        {
                            delColor(ID);
                            Response.Redirect("Operator_VehicleColor.aspx");
                        }
                    }

                }
                catch
                {
                   // Response.Redirect("default.aspx");
                }
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

            try
            {
                Session["VehicleID"] = l.Text;
                Session["VehicleName"] = l1.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");
            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditVehicleColor.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditVehicleColor.aspx");
                }
               
            }
            

        }
    }


    public void delColor(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[Sp_vehicleColor]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

            }
        }
        catch
        {

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);
            
        }


    }
}
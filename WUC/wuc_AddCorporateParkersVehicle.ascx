﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_AddCorporateParkersVehicle.ascx.cs" Inherits="WUC_wuc_AddCorporateParkersVehicle" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style2
    {
        width: 148px;
    }
    
    .style3
    {
        width: 226px;
    }
    .twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    
</style>
 <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
 <div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.jpg" /></a></div>
<div class="clearfix"></div>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Add/Edit Vehicle</legend>
    <br />
    <table class="timezone-table">
        <tr>
            <td class="style2">
                Make&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td class="style3">
                <asp:DropDownList ID="ddl_make" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource2" DataTextField="VehiMake" 
                    DataValueField="VehiMakeID" CssClass="twitterStyleTextbox" 
                    onselectedindexchanged="ddl_make_SelectedIndexChanged" >
                </asp:DropDownList>
                <cc1:DropDownExtender ID="ddl_make_DropDownExtender" runat="server" 
                    DynamicServicePath="" Enabled="True" TargetControlID="ddl_make">
                </cc1:DropDownExtender>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_VehicleMakes]"></asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="rfv_make" runat="server" 
                    ControlToValidate="ddl_make" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Model&nbsp;&nbsp;&nbsp; :</td>
            <td class="style3">
                <asp:TextBox ID="txtbox_model" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_model" runat="server" 
                    ControlToValidate="txtbox_model" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td class="style3">
                <asp:DropDownList ID="ddl_year" CssClass="twitterStyleTextbox" runat="server" ValidationGroup="a">
                </asp:DropDownList >
                <cc1:DropDownExtender ID="ddl_year_DropDownExtender" runat="server" 
                    DynamicServicePath="" Enabled="True" TargetControlID="ddl_year">
                </cc1:DropDownExtender>
                <asp:RequiredFieldValidator ID="rfv_year" runat="server" 
                    ControlToValidate="ddl_year" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Color&nbsp;&nbsp;&nbsp; :</td>
            <td class="style3">
                <asp:DropDownList ID="ddl_color" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="VehiColor" 
                    DataValueField="VehiColorid" CssClass="twitterStyleTextbox">
                </asp:DropDownList>
                <cc1:DropDownExtender ID="ddl_color_DropDownExtender" runat="server" 
                    DynamicServicePath="" Enabled="True" TargetControlID="ddl_color">
                </cc1:DropDownExtender>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Vehicle_color]"></asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="rfv_color" runat="server" 
                    ControlToValidate="ddl_color" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                License Plate:</td>
            <td class="style3">
                <asp:TextBox ID="txtbox_license" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_license" runat="server" 
                    ControlToValidate="txtbox_license" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
               <%-- <asp:Label ID="lbl_errrr" runat="server" Text="Label" Visible="false"></asp:Label>--%>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style3">
                <asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="a" 
                    onclick="btn_add_Click" CssClass="button"/>
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical" >
<fieldset>
<asp:Repeater ID="rpt_addvehicle" runat="server">

<HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                 <th>
                        VehicalMake
                    </th>
                      <th>
                       VehicalModel
                    </th>
                    
                    <th>
                        VehicalYear
                    </th>
                    <th>
                      VehicalColor
                    </th>
                    
                    <th>
                     VehicalLicensePlate
                    </th>
                   <%-- <th>
                     User Name
                    </th>--%>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("VehicalMake")%>
            <td>
                    <%#Eval("VehicalModel")%>
                    </td>
                <td>
                  <%#Eval("VehicalYear")%>
                  </td>

                  <td>
                  <%#Eval("VehicalColor")%>
                  </td>

                  <td>
                  <%#Eval("VehicalLicensePlate")%>
                     
                    </td>

               <%-- <td>
                  <%#Eval("Username")%>
                     
                    </td>--%>
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
</fieldset>
</asp:Panel>

</ContentTemplate>
        
    </asp:UpdatePanel>
</div>
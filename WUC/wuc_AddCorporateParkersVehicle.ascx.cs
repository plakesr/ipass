﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_AddCorporateParkersVehicle : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        for (int i = 1980; i < 2020; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            ddl_year.Items.Add(li);
        }
        if (!IsPostBack)
        {

            dt.Columns.Add("VehicalMake", typeof(string));
            dt.Columns.Add("VehicalModel", typeof(string));
            dt.Columns.Add("VehicalYear", typeof(string));
            dt.Columns.Add("VehicalColor", typeof(string));
            dt.Columns.Add("VehicalLicensePlate", typeof(string));
            dt.Columns.Add("Username", typeof(string));

            Session["datatable"] = dt;
        }
    }

    DataTable dt = new DataTable();
    protected void btn_add_Click(object sender, EventArgs e)
    {

        dt = (DataTable)Session["datatable"];
        DataRow dr = dt.NewRow();
        dr[0] = ddl_make.SelectedItem.Text;
        dr[1] = txtbox_model.Text;
        dr[2] = ddl_year.SelectedItem.Text;
        dr[3] = ddl_color.SelectedItem.Text;
        dr[4] = txtbox_license.Text;
        dr[5] = Session["usernameforvehicle"].ToString();

        dt.Rows.Add(dr);

        Session["datatable"] = dt;
        if (dt.Rows.Count > 0)
        {
            rpt_addvehicle.DataSource = dt;
            rpt_addvehicle.DataBind();
        }



        DataTable dbvehicle = (DataTable)Session["datatableforvehicle"];
        DataRow dr1 = dbvehicle.NewRow();

        dr1[0] = ddl_make.SelectedItem.Text;
        dr1[1] = txtbox_model.Text;
        dr1[2] = ddl_year.SelectedItem.Text;
        dr1[3] = ddl_color.SelectedItem.Text;
        dr1[4] = txtbox_license.Text;
        dr1[5] =  Session["usernameforvehicle"].ToString();


        dbvehicle.Rows.Add(dr1);

        Session["datatableforvehicle"] = dbvehicle;




    }
    protected void ddl_make_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
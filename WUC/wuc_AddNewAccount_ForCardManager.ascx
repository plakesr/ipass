﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_AddNewAccount_ForCardManager.ascx.cs" Inherits="WUC_wuc_AddNewAccount_ForCardManager" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 87%;
    }
    .style3
    {
        width: 157px;
    }
    .style4
    {
        width: 157px;
        height: 24px;
    }
    .style5
    {
        height: 24px;
    }
    .style7
    {
        height: 24px;
        width: 205px;
    }
    .style8
    {
        width: 205px;
    }
    .style9
    {
        width: 111px;
    }
    .style10
    {
        height: 24px;
        width: 111px;
    }
    .style12
    {
        width: 125px;
        height: 24px;
    }
    .style13
    {
        width: 125px;
    }
    .style14
    {
        width: 115px;
    }
    .style15
    {
        width: 115px;
        height: 24px;
    }
      .twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    
</style>
<table align="center" style="width: 111%">
      <tr>
        <td>
          <asp:Button Text="General" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Login Information" BorderStyle="None" ID="Tab2" 
                CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Billing Information" BorderStyle="None" ID="Tab3" 
                CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                <tr>
                  <td>
                      <table class="style1">
                          <tr>
                              <td class="style3">
                                  Account Type&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:RadioButton ID="rbt_corporate" runat="server" Text="Corporate" 
                                      ValidationGroup="a" />
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <asp:RadioButton ID="rpt_Individual" runat="server" Text="Individual" 
                                      ValidationGroup="a" />
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Account Flag&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:RadioButton ID="rbt_approved" runat="server" Text="Approved" 
                                      ValidationGroup="b" />
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                  <asp:RadioButton ID="rbt_onhold" runat="server" Text="On_Hold" 
                                      ValidationGroup="b" />
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Account Name&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_AcName" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Contect Name&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_ContectName" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Account#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txt" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  Category</td>
                              <td class="style13">
                                  <asp:DropDownList ID="ddl_category" runat="server">
                                  </asp:DropDownList>
                                  <cc1:DropDownExtender ID="ddl_category_DropDownExtender" runat="server" 
                                      DynamicServicePath="" Enabled="True" TargetControlID="ddl_category">
                                  </cc1:DropDownExtender>
                              </td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Master Account :</td>
                              <td class="style8">
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                              <td class="style9">
                                  Terms</td>
                              <td class="style13">
                                  <asp:DropDownList ID="ddl_terms" runat="server">
                                  </asp:DropDownList>
                                  <cc1:DropDownExtender ID="ddl_terms_DropDownExtender" runat="server" 
                                      DynamicServicePath="" Enabled="True" TargetControlID="ddl_terms">
                                  </cc1:DropDownExtender>
                              </td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style4">
                                  Bill Period&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style7">
                                  <asp:RadioButton ID="rbt_day" runat="server" GroupName="d" Text="Day" />
                                   &nbsp;&nbsp;&nbsp;&nbsp;
                                  <asp:RadioButton ID="rbt_month" runat="server" GroupName="d" 
                                      oncheckedchanged="rbt_month_CheckedChanged" Text="Month" />
                              </td>
                              <td class="style10">
                                  Bill Frequency</td>
                              <td class="style12">
                                  <asp:TextBox ID="txtbox_billfrequency" runat="server" Width="74px" CssClass=twitterStyleTextbox></asp:TextBox>
                                  </td>
                              <td class="style15">
                                  </td>
                              <td class="style5">
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Address 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_address1" runat="server" Height="52px" 
                                      TextMode="MultiLine" Width="236px" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Address 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_address2" runat="server" Height="57px" 
                                      TextMode="MultiLine" Width="229px" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Zip/Postal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_zip" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  City</td>
                              <td class="style13">
                                  <asp:TextBox ID="txtbox_city" runat="server" Width="101px" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style14">
                                  State</td>
                              <td>
                                  <asp:DropDownList ID="ddl_state" runat="server" CssClass=twitterStyleTextbox>
                                  </asp:DropDownList>
                                  <cc1:DropDownExtender ID="ddl_state_DropDownExtender" runat="server" 
                                      DynamicServicePath="" Enabled="True" TargetControlID="ddl_state">
                                  </cc1:DropDownExtender>
                              </td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Country&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_country" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  Office Phone&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_officephone" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  Cell</td>
                              <td class="style13">
                                  <asp:TextBox ID="txtbox_cell" runat="server" Width="104px" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style14">
                                  Fax</td>
                              <td>
                                  <asp:TextBox ID="txtbox_fax" runat="server" Width="103px" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td class="style3">
                                  E-mail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                              <td class="style8">
                                  <asp:TextBox ID="txtbox_email" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                              </td>
                              <td class="style9">
                                  &nbsp;</td>
                              <td class="style13">
                                  &nbsp;</td>
                              <td class="style14">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                      </table>
                  </td>
                </tr>
              </table>
              <br />

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; Site 
                List&nbsp;&nbsp; <asp:Repeater ID="Repeater1" runat="server">
                </asp:Repeater>

                &nbsp;

            </asp:View>
            <asp:View ID="View2" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                <tr>
                  <td>
                    <h3>
                      View 2
                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View3" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                <tr>
                  <td>
                    <h3>
                      View 3
                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>
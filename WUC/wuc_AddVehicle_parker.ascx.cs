﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WUC_wuc_AddVehicle_parker : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        for (int i = 2020; i > 1995; i--)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            ddl_year.Items.Add(li);
        }
        if (!IsPostBack)
        {
            dt.Columns.Add("VehicalMake", typeof(string));
            dt.Columns.Add("VehicalModel", typeof(string));
            dt.Columns.Add("VehicalYear", typeof(string));
            dt.Columns.Add("VehicalColor", typeof(string));
            dt.Columns.Add("VehicalLicensePlate", typeof(string));

            Session["datatable"] = dt;
        }
    }
    DataTable dt = new DataTable();
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
            dt = (DataTable)Session["datatable"];
            DataRow dr = dt.NewRow();
            dr[0] = ddl_make.SelectedItem.Text;
            if (txtbox_model.Text.Trim() != "")
            {
                dr[1] = txtbox_model.Text;
            }
            else
            {
                dr[1] = "Not Given";

            }
            dr[2] = ddl_year.SelectedItem.Text;
            dr[3] = ddl_color.SelectedItem.Text;
            if (txtbox_license.Text.Trim() != "")
            {

                dr[4] = txtbox_license.Text;
            }
            else
            {
                dr[4] = "Not Given";

            }
            dt.Rows.Add(dr);

            Session["datatable"] = dt;
            if (dt.Rows.Count > 0)
            {
                rpt_addvehicle.DataSource = dt;
                rpt_addvehicle.DataBind();
            }

        }
        catch
        {
        }
    }
    //protected void txtbox_license_TextChanged(object sender, EventArgs e)
    //{
    //    //DataTable dt = (DataTable)Session["datatable"];
    //    //try
    //    //{
    //    //    if (dt.Rows[0]["VehicalLicensePlate"].ToString().Trim() == txtbox_license.Text.Trim())
    //    //    {
    //    //        lbl_errrr.Visible = true;
    //    //        lbl_errrr.Text = "already exist";
    //    //        txtbox_license.Text = "";
    //    //    }
    //    //    else
    //    //    {
    //    //        txtbox_license.Text = txtbox_license.Text;
    //    //    }

    //    //}
    //    //catch
    //    //{
    //    //}
    //}
    protected void Button1_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('AddIndividua_vehicleparker.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

    }
}
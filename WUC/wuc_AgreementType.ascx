﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_AgreementType.ascx.cs" Inherits="WUC_wuc_AgreementType" %>
<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
    width:222px;
    resize:none;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style2
    {
        width: 197px;
    }
    </style>


    <table class="form-table">
        <tr>
            <td class="style2" valign="top">
                Agreement Type Name:</td>
            <td>
                <asp:TextBox ID="txtbox_Agreementname" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_agreetypename" runat="server" 
                    ControlToValidate="txtbox_Agreementname" 
                    ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2" valign="top">
                Agreement Type Description:</td>
            <td>
                <asp:TextBox ID="txtbox_AgreementDesc" runat="server" ValidationGroup="a" 
                    Height="66px" TextMode="MultiLine" Width="231px" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_agreementtypedesc" runat="server" 
                    ControlToValidate="txtbox_AgreementDesc" 
                    ErrorMessage="*" 
                    ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_submit" runat="server" Text="Add" 
                    onclick="btn_submit_Click" ValidationGroup="a" CssClass="button" 
                    Height="35px" Width="79px"/>
            </td>
        </tr>
    </table>


<asp:Panel ID="Panel2" runat="server" Height="447px">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="SELECT * FROM [tbl_AgreementTypeMaster]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table class="timezone-table" width="100%">
                <tr>
                      <th>
                        Agreement Type Name
                    </th>
                     <th>
                        Agreement Type Desc
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("AgreementTypeName")%>
                    </td><td>
                    <%#Eval("AgreementTypeDesc")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("AgreementTypeId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("AgreementTypeId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>
                                        <asp:Label ID="idLabel" runat="server" Text='<%# Eval("AgreementTypeId") %>' 
                                        Visible="False" />
                                        <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("AgreementTypeName") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="idLabel2" runat="server" Text='<%# Eval("AgreementTypeDesc") %>' 
                                        Visible="False"></asp:Label>  
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>

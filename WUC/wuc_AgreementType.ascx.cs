﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_AgreementType : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {




            addAgreement();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void addAgreement()
    {
        try
        {
            hst.Clear();
            
            hst.Add("action", "insert");
            hst.Add("agreementtypename", txtbox_Agreementname.Text);
            hst.Add("agreementtypedesc", txtbox_AgreementDesc.Text);
            hst.Add("operatorId", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
           

            int result = objData.ExecuteNonQuery("[sp_AgreementType]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                Response.Redirect("Admin_AgreementType.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }


    public void clear()
    {
        txtbox_Agreementname.Text = "";
        txtbox_AgreementDesc.Text = "";
       
    }

    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            delAgreementtype(ID);
        }
        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

            Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;


            Session["AgreementTypeId"] = l.Text;
            Session["AgreementTypeName"] = l1.Text;
            Session["AgreementTypeDesc"] = l2.Text;

            Response.Redirect("Admin_EditAgreementType.aspx");

        }
    }
    public void delAgreementtype(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("agreementid", id);


            int result = objData.ExecuteNonQuery("[sp_AgreementType]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("Admin_AgreementType.aspx");

            }
            else
            {
                Response.Redirect("Admin_AgreementType.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_AgreementType.aspx");

        }


    }
}
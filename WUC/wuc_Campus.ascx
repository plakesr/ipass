﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Campus.ascx.cs" Inherits="WUC_wuc_country" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc2" %>

<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style2
    {
        width: 127px;
    }
    .style3
    {
        width: 382px;
    }
    .style4
    {
        width: 326px;
    }
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel2" runat="server">

 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
          
             <ContentTemplate>
<table class="form-table">
    <tr>
        <td class="style4">
            Select Customer:</td>
        <td class="style3">
           
            <asp:DropDownList ID="ddl_site" runat="server" ValidationGroup="a" 
                DataSourceID="SqlDataSource2" DataTextField="SiteName" DataValueField="SiteId" AppendDataBoundItems="true">
                      <asp:ListItem Selected = "True" Text = "------Select Customer------" Value = "0"></asp:ListItem>

                <%--<asp:ListItem Value="1">Site1</asp:ListItem>
                        <asp:ListItem Value="2">Site2</asp:ListItem>
                        <asp:ListItem Value="3">Site3</asp:ListItem>
                        <asp:ListItem Value="4">Site4</asp:ListItem>--%>
            </asp:DropDownList>
             
             <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
            </asp:SqlDataSource>
             <asp:RequiredFieldValidator ID="rfv_site" runat="server" 
                ControlToValidate="ddl_site" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="a"></asp:RequiredFieldValidator>
               
        </td>
        <td class="style3" rowspan="3">
           
            
            <%--<div id="map_canvas" style="width:300px; height:300px">--%></td>
    </tr>
    <%--  <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>--%>
    <tr>
        <td class="style4">
            Campus Name :</td>
        <td class="style3">
          
            <asp:TextBox ID="txtbox_campusname" runat="server" ValidationGroup="a" 
                CssClass="twitterStyleTextbox" AutoPostBack="true" 
                ontextchanged="txtbox_campusname_TextChanged"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_campusname" runat="server" 
                ControlToValidate="txtbox_campusname" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator><br />
            <asp:Label ID="lbl_already" runat="server" Visible="False"></asp:Label>
          
        </td>
    </tr>
     <%-- </ContentTemplate>
            <Triggers>
             <asp:AsyncPostBackTrigger ControlID="txtbox_campusname" EventName="TextChanged" />
            </Triggers>
            </asp:UpdatePanel>
    --%>
    <tr>
        <td class="style4">
            Address</td>
        <td class="style3">
            
            <asp:Button ID="Button2" runat="server" onclick="Button1_Click1" 
                        Text="Get Geo-Location" CssClass="button" />
            </td>
        <td class="style3">
             </td>
    </tr>

    <tr>
        <td class="style4">
            Campus Details </td>
        <td class="style3">
        <cc2:Editor ID="Editor1" runat="server" />
         
            
        </td>
        
    </tr>

    <tr>
    <td class="style4">
        <asp:Label ID="Label2" runat="server" ForeColor="#FF3300" Visible="False"></asp:Label>
        </td>
    <td>
        <asp:Button ID="btn_submit" runat="server" CssClass="button" Height="35px" 
            onclick="btn_submit_Click" Text="Submit" ValidationGroup="a" Width="107px" />
        </td>
    </tr>
    <tr>
        <td class="style4">
            &nbsp;</td>
        <caption>
            &nbsp;</td>
          <%--  <tr>
                <td class="style4">
                    <asp:Label ID="Label1" runat="server" Text="Select  Customer  Name"></asp:Label>
                </td>
                <td class="style3">
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        DataSourceID="SqlDataSource3" DataTextField="SiteName" 
                        DataValueField="SiteId" AutoPostBack="True" 
                        AppendDataBoundItems="true">
                        <asp:ListItem Text="--Select Customer--" Value="0" ></asp:ListItem>
                    </asp:DropDownList>
                    <cc1:DropDownExtender ID="DropDownList1_DropDownExtender" runat="server" 
                        DynamicServicePath="" Enabled="True" TargetControlID="DropDownList1">
                    </cc1:DropDownExtender> 
                    <asp:Button ID="Button1" runat="server" Text="Search"  CssClass="button"
                        onclick="Button1_Click" />
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
                    </asp:SqlDataSource>
                </td>
            </tr>--%>
        </caption>
    </tr>
</table>
 </ContentTemplate>
                  </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="Panel3" runat="server">
<fieldset>
<legend>Search On the Basis of Customer Name</legend>
<table class="form-table">
  <tr>
                <td class="style4">
                    <asp:Label ID="Label1" runat="server" Text="Select  Customer  Name"></asp:Label>
                </td>
                <td class="style3">
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        DataSourceID="SqlDataSource3" DataTextField="SiteName" 
                        DataValueField="SiteId" AutoPostBack="True" 
                        AppendDataBoundItems="true">
                        <asp:ListItem Text="--Select Customer--" Value="0" ></asp:ListItem>
                    </asp:DropDownList>
                   
                    <asp:Button ID="Button1" runat="server" Text="Search"  CssClass="button"
                        onclick="Button1_Click" />
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
                    </asp:SqlDataSource>
                </td>
            </tr>
</table>
</fieldset>
</asp:Panel>
<asp:Panel ID="Panel1" runat="server" >
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="select tbl_CampusMaster.*,tbl_SiteMaster.SiteName,tbl_SiteMaster.SiteId from tbl_CampusMaster
inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_CampusMaster.SiteID">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource1" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Campus&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Customer&nbsp;&nbsp; Name
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                        <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                  <%--  <th width="5%">
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("Campus_Name")%>
                    </td><td>
                    <%#Eval("SiteName")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("CampusID") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
              <%--  <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("CampusID") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>--%>
                           <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("CampusID") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("CampusID") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td>
                    <asp:Label ID="campusname" runat="server" Text='<%# Eval("Campus_Name") %>' 
                                        Visible="False" />
                    <asp:Label ID="campusdetail" runat="server" Text='<%# Eval("Campus_Description") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="sitename" runat="server" Text='<%# Eval("SiteId") %>' 
                                        Visible="False"></asp:Label>

                                        <asp:Label ID="id" runat="server" Text='<%# Eval("CampusID") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="address" runat="server" Text='<%# Eval("address") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="latitude" runat="server" Text='<%# Eval("latitude") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="longitude" runat="server" Text='<%# Eval("longitude") %>' 
                                        Visible="False"></asp:Label>
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>





        <asp:Repeater runat="server" id="searchrpt" onitemcommand="searchrpt_ItemCommand" 
                >
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Campus&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Customer&nbsp;&nbsp; Name
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                        <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                 <%--   <th width="5%">
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("Campus_Name")%>
                    </td><td>
                   <%#Eval("SiteName")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("CampusID") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
               <%-- <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("CampusID") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>--%>
                           <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("CampusID") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("CampusID") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td>
                    <asp:Label ID="campusname" runat="server" Text='<%# Eval("Campus_Name") %>' 
                                        Visible="False" />
                    <asp:Label ID="campusdetail" runat="server" Text='<%# Eval("Campus_Description") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="sitename" runat="server" Text='<%# Eval("SiteId") %>' 
                                        Visible="False"></asp:Label>

                                        <asp:Label ID="id" runat="server" Text='<%# Eval("CampusID") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="address" runat="server" Text='<%# Eval("address") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="latitude" runat="server" Text='<%# Eval("latitude") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="longitude" runat="server" Text='<%# Eval("longitude") %>' 
                                        Visible="False"></asp:Label>
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>









            <asp:SqlDataSource ID="SqlDataSource4" runat="server"></asp:SqlDataSource>









        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>







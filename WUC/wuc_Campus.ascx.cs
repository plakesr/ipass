﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Data;
using System.Collections;

public partial class WUC_wuc_country : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }

            if (!IsPostBack == true)
            {
                try
                {
                    
                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }

                   

                }
                catch
                {
                    try
                    {

                        if (Session["Type"].ToString() == "admin")
                        {
                           // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
  

    protected void btn_submit_Click(object sender, EventArgs e)
    {

        try
        {
            if (ddl_site.SelectedItem.Value != "0")
            {
                if (Session["dataaddress"].ToString() != null)
                {
                    addcampus();
                    Label2.Visible = false;
                    Label2.Text = "";
                    Session["dataaddress"] = null;
                }
            }
            else
            {
                Label2.Visible = true;
                Label2.Text = "Please Select Site First";

            }

        }
        catch
        {
            Label2.Visible = true;
            Label2.Text = "Please Give Geo-Location";


        }
        
      
    }



    string s = "true";
    string address1 = "";
    string lat = "";
    string longi = "";
    string msg;
    public void addcampus()
    {
        try
        {
           // hst.Clear();

            hst.Add("action", "insert");
            hst.Add("campusname", txtbox_campusname.Text);
            hst.Add("campusdetail", Editor1.Content.ToString());
            hst.Add("siteid", ddl_site.SelectedValue);
            hst.Add("modifiedby", "admin");
            hst.Add("operator", "admin");
            hst.Add("dateofchanging", DateTime.Now);
            hst.Add("status", s);

            DataTable dsadd = (DataTable)Session["dataaddress"];
            try
            {
            if (dsadd.Rows.Count > 0)
            {

                address1 = dsadd.Rows[0]["address"].ToString();
                lat = dsadd.Rows[0]["latitude"].ToString();
                longi = dsadd.Rows[0]["longitude"].ToString();
                hst.Add("address", address1);
                hst.Add("latitude", lat);
                hst.Add("longitude", longi);
            }
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please addlocation first.')", true);

            }


            

            int result = objData.ExecuteNonQuery("[sp_Campus]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CampusMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CampusMaster.aspx?message=" + msg);

                }

            

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);


        }

    }
    public void delCampus(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("campusid", id);


            int result = objData.ExecuteNonQuery("[sp_Campus]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               
            
                

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delete Data Please Try Again.')", true);

               // Response.Redirect("Admin_CampusMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delete Data Please Try Again.')", true);

           // Response.Redirect("Admin_CampusMaster.aspx");

        }


    }

   
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            if (Session["Type"].ToString() == "admin")
            {
                delCampus(ID);
                Response.Redirect("Admin_CampusMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbldelete"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                }
                else
                {
                    delCampus(ID);
                    Response.Redirect("Operator_CampusMaster.aspx");
                }
            }
            
        }

        if (e.CommandName == "cmdinactive")
        {
            // string user = e.CommandArgument.ToString();
            int campusid = Convert.ToInt32(e.CommandArgument);
            DataSet ds = new clsInsert().fetchrec("select CampusStatus from tbl_CampusMaster  where CampusID=" + campusid);

            if (ds.Tables[0].Rows[0][0].ToString() == "False")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Already Deactivated ')", true);

            }
            else
            {
                inactivestatus(campusid);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Deactived Successfuly')", true);
            }

        }

        if (e.CommandName == "cmdactive")
        {
            int campusid = Convert.ToInt32(e.CommandArgument);
             DataSet ds = new clsInsert().fetchrec("select CampusStatus from tbl_CampusMaster  where CampusID=" + campusid);

             if (ds.Tables[0].Rows[0][0].ToString() == "True")
             {
                 ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Already Activated ')", true);

             }
             else
             {
                 DataSet ds1 = new clsInsert().fetchrec("select tbl_SiteMaster.Site_Status from tbl_CampusMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_CampusMaster.SiteID where tbl_CampusMaster.CampusID=" + campusid);
                 if (ds1.Tables[0].Rows[0][0].ToString() == "False")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Related Site Is Not Active')", true);

                 }
                 else
                 {
                     activeuserstatus(campusid);
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Activated Successfuly')", true);
                 }
             }

        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("campusname") as Label;
            Label l1 = rpt1.Items[i].FindControl("campusdetail") as Label;

            Label l2 = rpt1.Items[i].FindControl("SiteName") as Label;
            Label address = rpt1.Items[i].FindControl("address") as Label;
            Label latitude = rpt1.Items[i].FindControl("latitude") as Label;
            Label longitude = rpt1.Items[i].FindControl("longitude") as Label;


            Session["campusname"] = l.Text;
            Session["campusdetail"] = l1.Text;
            Session["Campus_address"] = address.Text;
            Session["Campus_latitude"] = latitude.Text;

            Session["Campus_longitude"] = longitude.Text;

            Session["SiteName"] = l2.Text;

            int id = Convert.ToInt32(e.CommandArgument.ToString());
            Session["id"] = id;
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditCampusMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditCampusMaster.aspx");
                }
               
            }
           
        }
    }
    public void inactivestatus(int campusid)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "campusstausinactive");
            //hst.Add("ustatus", userstattus1);
            hst.Add("campusid", campusid);



            int result = objData.ExecuteNonQuery("[sp_statusupdate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //  Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
                // Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
            // Response.Redirect("Admin_SiteMaster.aspx");

        }


    }


    public void activeuserstatus(int campusid)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "campusstausactive");
            //hst.Add("ustatus", userstattus1);
            hst.Add("campusid", campusid);



            int result = objData.ExecuteNonQuery("[sp_statusupdate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //  Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
                // Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
            // Response.Redirect("Admin_SiteMaster.aspx");

        }


    }
    protected void txtbox_cuntryabbr_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtMessageBody_TextChanged(object sender, EventArgs e)
    {

    }
    //protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    //{
        
    //}
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataTable dt = new clsInsert().getcampus(Convert.ToInt32(DropDownList1.SelectedValue));
        if (dt.Rows.Count > 0)
        {

            searchrpt.Visible = true;
            rpt1.Visible = false;
            searchrpt.DataSource = dt;
            searchrpt.DataBind();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found.')", true);

        }
    }
    protected void searchrpt_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            delCampus(ID);
        }
        if (e.CommandName == "cmdinactive")
        {
            // string user = e.CommandArgument.ToString();
            int campusid = Convert.ToInt32(e.CommandArgument);

            inactivestatus(campusid);
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Deactived Successfuly')", true);

        }

        if (e.CommandName == "cmdactive")
        {
            int campusid = Convert.ToInt32(e.CommandArgument);
            DataSet ds = new clsInsert().fetchrec("select tbl_SiteMaster.Site_Status from tbl_CampusMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_CampusMaster.SiteID where tbl_CampusMaster.CampusID=" + campusid);
            if (ds.Tables[0].Rows[0][0].ToString() == "0")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Related Site Is Not Active')", true);

            }
            else
            {
                activeuserstatus(campusid);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Activated Successfuly')", true);
            }
        }
        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("campusname") as Label;
            Label l1 = rpt1.Items[i].FindControl("campusdetail") as Label;

            Label l2 = rpt1.Items[i].FindControl("SiteName") as Label;
            Label address = rpt1.Items[i].FindControl("address") as Label;
            Label latitude = rpt1.Items[i].FindControl("latitude") as Label;
            Label longitude = rpt1.Items[i].FindControl("longitude") as Label;


            Session["campusname"] = l.Text;
            Session["campusdetail"] = l1.Text;
            Session["Campus_address"] = address.Text;
            Session["Campus_latitude"] = latitude.Text;

            Session["Campus_longitude"] = longitude.Text;

            Session["SiteName"] = l2.Text;

            int id = Convert.ToInt32(e.CommandArgument.ToString());
            Session["id"] = id;
            Response.Redirect("Admin_EditCampusMaster.aspx");

        }
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('GetAddressLatLong.aspx','New Windows','height=580, width=500,location=center');", true);

    }

    protected void txtbox_campusname_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataSet campus = new clsInsert().fetchrec("select Campus_Name from tbl_CampusMaster where SiteID= " + ddl_site.SelectedValue);
            for (int i = 0; i < campus.Tables[0].Rows.Count; i++)
            {
                if (txtbox_campusname.Text == campus.Tables[0].Rows[i][0].ToString())
                {
                    lbl_already.Visible = true;
                    lbl_already.ForeColor = Color.Red;
                    lbl_already.Text = "Campus Name Already Exist At This Site";
                    txtbox_campusname.Text = "";
                    break;
                }
                else
                {
                    lbl_already.Visible = false;
                }
            }
        }
        catch
        {
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_CashAccountSearchingForParkers.ascx.cs" Inherits="WUC_wuc_CashAccountSearchingForParkers" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 176px;
    }
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<table class="style1">
    <tr>
        <td class="style2">
            Search</td>
        <td>
            <asp:TextBox ID="txtbox_search" runat="server" Height="32px" Width="220px"  CssClass="twitterStyleTextbox"  ></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_search_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_search"  
                WatermarkCssClass="watermark" 
                WatermarkText="Enter Name/User Name/Mobile No./E-mail Id">
            </cc1:TextBoxWatermarkExtender>
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button" 
                onclick="btn_search_Click" />
        </td>
    </tr>
</table>
</fieldset>
</asp:Panel>

<asp:Repeater ID="Repeater1" runat="server" 
    onitemcommand="Repeater1_ItemCommand">
   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                 
                      <th>
                      Lot Name
                    </th>
                    <th>
                       User Name
                    </th>
                    <th>
                    Name
                    </th>
                     <th>
                      Email Id
                    </th>
                   
                    <th>
                     Mobile No.
                    </th>
           
                    <th>
                     Detail
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
        <tr>
          
                <td>
                    <%#Eval("LotName")%>
                 
                </td>
                 <td>
                    <%#Eval("Username")%>
                      <asp:Label ID="username" runat="server" Text='<%# Eval("Username") %>' 
                                        Visible="False" /> 
                </td>
                <td>
                    <%#Eval("f_Name")%>  
                    
                </td>

                <td>
                   <%#Eval("E-mail")%>
                    
                </td>
                  <td>
                    <%#Eval("Mobile_no")%>
                    
                </td>
                
           
                    <td>
                    
                    <asp:Button ID="btn_detail" CommandName="detail" runat="server" Text="Detail"  CssClass="button" />
                </td>
                   
                    
            </tr>
            
        </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>

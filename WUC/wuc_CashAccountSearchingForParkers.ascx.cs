﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_CashAccountSearchingForParkers : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                if (Session["CashGiven"].ToString() == "True")
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Receipt.aspx','New Windows','height=580, width=600,location=no');", true);
                    Session["CashGiven"] = null;
                }
                
            }
            catch
            {
            }
        }
    }
    decimal amountduetotal = Convert.ToDecimal(0.00);
    decimal amountduetotal1 = Convert.ToDecimal(0.00);
    string address, name, lotname, plan, totalamount = "";
    protected void btn_search_Click(object sender, EventArgs e)
    {
        DataTable dt2 = new DataTable();
        dt2.Columns.Add("LotName", typeof(string));
        dt2.Columns.Add("Username", typeof(string));
        dt2.Columns.Add("f_Name", typeof(string));
       

        dt2.Columns.Add("E-mail", typeof(string));
        dt2.Columns.Add("Mobile_no", typeof(string));

        try
        {

            if (Session["Type"].ToString() == "admin")
            {

                DataTable dt11 = new clsInsert().getparker(txtbox_search.Text.Trim());

                if (dt11.Rows.Count > 0)
                {
                    try
                    {
                        for (int i = 0; i < dt11.Rows.Count; i++)
                        {

                            DataRow dr = dt2.NewRow();
                            dr[0] = dt11.Rows[i]["LotName"].ToString();
                            dr[1] = dt11.Rows[i]["UserName"].ToString();

                            dr[2] = dt11.Rows[i]["FirstName"].ToString() + "  "+dt11.Rows[i]["LastName"].ToString(); 
                     

                            dr[3] = dt11.Rows[i]["Email"].ToString();
                            dr[4] = dt11.Rows[i]["CellNo"].ToString();
                            dt2.Rows.Add(dr);
                          
                        }
                        Repeater1.DataSource = dt2;
                        Repeater1.DataBind();
                    }
                    catch
                    {
                    }

                }
                else
                {

                    //Result.Visible = false;
                    //rpt_searchparker.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
                  
                }
            }
        }
        catch
        {
        }

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int m = e.Item.ItemIndex;
        if (e.CommandName == "detail")
        {
            DataSet_Bill detail = new DataSet_Bill();
            Label l = Repeater1.Items[m].FindControl("username") as Label;

            DataSet ds = new clsInsert().fetchrec("select distinct  billing. Acct_Num, p.*, lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby  ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby   as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice   as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid    and billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid    and lotrate.chargeby=chargename.chargebyid  and lr.chargeby =3 and p.UserName='" + l.Text + "'");
            // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
            string table = "";
            string parkingtype = "";
            string UserName = "";

            if (ds.Tables[0].Rows.Count > 0)
            {

                try
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        // DataRow dr = dt.NewRow();


                        DataRow dr1 = detail.Table_bill1.NewRow();







                        // dr[0] = ds.Tables[0].Rows[i][0].ToString();

                        dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                        UserName = ds.Tables[0].Rows[i][0].ToString();
                        dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                        address = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                        dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                        name = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                        dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                        lotname = ds.Tables[0].Rows[i][26].ToString();
                        dr1[7] = ds.Tables[0].Rows[i][23].ToString();

                        if (ds.Tables[0].Rows[i][4].ToString() == "1")
                        {
                            dr1[5] = "Reserved Parking";
                            parkingtype = "Reserved Parking";
                        }
                        if (ds.Tables[0].Rows[i][4].ToString() == "2")
                        {
                            dr1[5] = "Random Parking";
                            parkingtype = "Random Parking";
                        }

                        dr1[6] = ds.Tables[0].Rows[i][29].ToString();
                        plan = ds.Tables[0].Rows[i][29].ToString();

                        DataTable dtamount = new clsInsert().dueamount1(ds.Tables[0].Rows[i][0].ToString());
                        if (dtamount.Rows.Count > 0)
                        {

                            for (int j = 0; j < dtamount.Rows.Count; j++)
                            {


                                amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());

                                DataRow dr2 = detail.BillInformation.NewRow();
                                dr2[0] = dtamount.Rows[j][1].ToString();
                                dr2[1] = dtamount.Rows[j]["Date Issued"].ToString();

                                string startdate = dtamount.Rows[j]["Date Issued"].ToString();
                                dr2[2] = dtamount.Rows[j]["DueDate"].ToString();
                                string duedate = dtamount.Rows[j]["DueDate"].ToString();

                                dr2[3] = dtamount.Rows[j][4].ToString();

                                dr2[4] = dtamount.Rows[j][5].ToString();
                                dr2[5] = dtamount.Rows[j][6].ToString();


                                dr2[6] = dtamount.Rows[j]["Amt_Outstanding"].ToString();
                                string dueamount = dtamount.Rows[j]["Amt_Outstanding"].ToString();

                                table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                detail.BillInformation.Rows.Add(dr2);
                            }
                        }

                        else
                        {
                            DataTable dtamount_amount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                            if (dtamount_amount.Rows.Count > 0)
                            {

                                for (int j = 0; j < dtamount_amount.Rows.Count; j++)
                                {


                                 //   amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Dueamount"].ToString());

                                    DataRow dr2 = detail.BillInformation.NewRow();
                                    dr2[0] = dtamount_amount.Rows[j][1].ToString();
                                    dr2[1] = dtamount_amount.Rows[j]["Date Issued"].ToString();

                                    string startdate = dtamount_amount.Rows[j]["Date Issued"].ToString();
                                    dr2[2] = dtamount_amount.Rows[j]["DueDate"].ToString();
                                    string duedate = dtamount_amount.Rows[j]["DueDate"].ToString();

                                    dr2[3] = dtamount_amount.Rows[j][4].ToString();

                                    dr2[4] = dtamount_amount.Rows[j][5].ToString();
                                    dr2[5] = dtamount_amount.Rows[j][6].ToString();


                                    dr2[6] = dtamount_amount.Rows[j]["Amt_Outstanding"].ToString();
                                    string dueamount = dtamount_amount.Rows[j]["Amt_Outstanding"].ToString();

                                    table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                    detail.BillInformation.Rows.Add(dr2);
                                }
                            }
                        }


                        if (amountduetotal == amountduetotal1)
                        {
                            dr1[1] = amountduetotal;
                            totalamount = Convert.ToString(amountduetotal.ToString());
                            //dt.Rows.Add(dr);
                            detail.Table_bill1.Rows.Add(dr1);
                            amountduetotal = Convert.ToDecimal(0.00);
                        }
                        else
                        {
                            //   dr[1] = amountduetotal;
                            dr1[1] = amountduetotal;
                            totalamount = Convert.ToString(amountduetotal.ToString());
                            //dt.Rows.Add(dr);
                            detail.Table_bill1.Rows.Add(dr1);
                            amountduetotal = Convert.ToDecimal(0.00);
                        }


                    }


                    Session["detailofdue"] = detail;
                    Response.Redirect("CashDetailsOfParker.aspx");
                    //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);
                }
                catch
                {
                }

            }





        }
    }
}
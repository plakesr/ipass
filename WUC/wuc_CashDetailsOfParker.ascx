﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_CashDetailsOfParker.ascx.cs" Inherits="WUC_wuc_CashDetailsOfParker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 1179px;
    }
    .style3
    {
        width: 1178px;
    }
    .style4
    {
        width: 1177px;
    }
    .style6
    {
        width: 138px;
    }
    .style7
    {
        width: 14%;
    }
    .style8
    {
        width: 25%;
    }
    .style9
    {
        width: 18%;
    }
    .style10
    {
        width: 50%;
    }
</style>
<script type="text/javascript">

    function IsOneDecimalPoint(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
        var parts = evt.srcElement.value.split('.');
        if (parts.length > 1 && charCode == 46)
            return false;
        return true;
    }
</script>

<asp:Panel ID="Panel1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
<fieldset>
<table class="style1">
        <tr>
            <td class="style3">
                Name</td>
            <td class="style4">
                <asp:TextBox ID="txtbox_name" runat="server" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:Label ID="lbl_username" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Address</td>
            <td>
                <asp:TextBox ID="txtbox_address" runat="server" Enabled="False" 
                    CssClass="twitterStyleTextbox" TextMode="MultiLine" style="resize:none;" 
                    Width="393px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Lot Name</td>
            <td>
                <asp:TextBox ID="txtbox_lotname" runat="server" Enabled="False" 
                    CssClass="twitterStyleTextbox" TextMode="MultiLine" style="resize:none;" 
                    Width="390px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Parking Type
            </td>
            <td>
                <asp:TextBox ID="txtbox_parkingtype" runat="server" Enabled="False" CssClass="twitterStyleTextbox"   ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Plan</td>
            <td>
                <asp:TextBox ID="txtbox_plan" runat="server" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Plan Rate</td>
            <td>
                <asp:TextBox ID="txtbox_planrate" runat="server" Enabled="False" 
                    ontextchanged="txtbox_planrate_TextChanged"></asp:TextBox>
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
<br />
<asp:Panel ID="Panel2" runat="server">
<fieldset>
<asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand">
    <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                 <th>
                     Invoice Number
                    </th>
                <th>
                     Issued Date
                    </th>
                      <th>
                       Due Date
                    </th>
                     <th>
                       Sub-Total
                    </th>
                  
                  <th>
                       Tax
                    </th>
                     <th>
                       Invoiced Amount
                    </th>
                    <th>
                      Outstanding Amount
                    </th>
                  

                  
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr><td>
             <%#Eval("InvoiceNumber")%>
            </td>
            <td>     <%#DateTime.Parse(Eval("Issued Date").ToString()).ToString("d")%> </td> 
              
                  <td>
                             <%#DateTime.Parse(Eval("Due Date").ToString()).ToString("d")%> 
               
                   
                </td>
                <td>
                     $ <%#Eval("Sub-Total")%> 
                    
                </td><td>
                     $ <%#Eval("Tax")%> 
                    
                </td>
                <td>
                     $ <%#Eval("Invoiced Amount")%> 
                    
                </td>
                <td>
                     $ <%#Eval("Amount Outstanding")%> 
                    
                </td>
               
               
            </tr>
         
  
       </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>

</fieldset>
</asp:Panel>
<table class="style1">
    <tr>
        <td class="style6">
            Total Due&nbsp; Amount</td>
        <td>
            
                <asp:TextBox ID="txtbox_totaldue" runat="server" 
                CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
            
        </td>
    </tr>
</table>
<asp:Panel Visible="false" runat="server" ID="Panel_cash">

<table class="style1">
    <tr>
        <td class="style7">
         Amount Pay  </td>
        <td colspan="3">
           <asp:TextBox ID="txtbox_amontpay" runat="server" CssClass="twitterStyleTextbox" Text="0.00"

                onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>
                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom"
  ValidChars="01234567890." TargetControlID="txtbox_amontpay"></cc1:FilteredTextBoxExtender>
                
            <asp:Label ID="lblerror" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                </td>
    </tr>
    <tr>
        <td class="style7">
            Transaction Type </td>
        <td class="style8">
            
            <asp:DropDownList ID="ddl_transactiontype" runat="server" AutoPostBack="True" 
                onselectedindexchanged="DropDownList1_SelectedIndexChanged" 
                style="height: 22px">
                <asp:ListItem>Cash</asp:ListItem>
                <asp:ListItem>Cheque</asp:ListItem>
                <asp:ListItem>Credit Card</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="style9">
            
            <asp:Label ID="lbltransaction" runat="server" Visible="False"></asp:Label>
        </td>
        <td class="style10">
            
            <asp:TextBox ID="txtbox_tranasactiontype" runat="server" 
                ontextchanged="txtbox_tranasactiontype_TextChanged" Visible="False" CssClass="twitterStyleTextbox"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style7">
            <%--<asp:Button ID="btnprint" runat="server" onclick="btnprint_Click" CssClass="button" 
                Text="Print" />--%>
        </td>
        <td colspan="3">
            <asp:Button ID="btn_ok" runat="server" Text="Submit" CssClass="button" 
                onclick="btn_ok_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="button" 
                onclick="btn_cancel_Click"  />
        </td>
    </tr>
</table>

</asp:Panel>
<asp:Panel Visible="false" runat="server" ID="Panel_nodue">
<table>
<tr>
<td></td>
<td>
  <asp:Button ID="Button1" runat="server" Text="Back" CssClass="button" onclick="Button1_Click" 
                 />
</td>
</tr>

</table>
</asp:Panel>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Text;
public partial class WUC_wuc_CashDetailsOfParker : System.Web.UI.UserControl
{
    string u_name = "";
    clsData cs = new clsData();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["detailofdue"] != null)
            {




                DataSet_Bill ds1 = (DataSet_Bill)Session["detailofdue"];
                txtbox_address.Text = ds1.Tables["Table_bill1"].Rows[0][2].ToString();
                txtbox_lotname.Text = ds1.Tables["Table_bill1"].Rows[0][4].ToString();
                txtbox_name.Text = ds1.Tables["Table_bill1"].Rows[0][3].ToString();
                txtbox_parkingtype.Text = ds1.Tables["Table_bill1"].Rows[0][5].ToString();
                txtbox_plan.Text = ds1.Tables["Table_bill1"].Rows[0][6].ToString();
                txtbox_totaldue.Text = "$" + ds1.Tables["Table_bill1"].Rows[0][1].ToString();
                if (txtbox_totaldue.Text == "$0")
                {
                    Panel_cash.Visible = false;
                    Panel_nodue.Visible = true;
                }
                else
                {
                    Panel_cash.Visible = true;
                    Panel_nodue.Visible = false;
                }

                u_name = ds1.Tables["Table_bill1"].Rows[0][0].ToString();

                lbl_username.Text = ds1.Tables["Table_bill1"].Rows[0][0].ToString();
                txtbox_planrate.Text = ds1.Tables["Table_bill1"].Rows[0][7].ToString();
                Repeater1.DataSource = ds1.Tables["BillInformation"];
                Repeater1.DataBind();
            }
        }
        catch
        {
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void txtbox_planrate_TextChanged(object sender, EventArgs e)
    {

    }
    decimal givenamount = Convert.ToDecimal(0.00);
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        if (txtbox_amontpay.Text != "" && Convert.ToDecimal(txtbox_amontpay.Text)>0)
        {
            DataSet dsadvance=new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='"+u_name+"' order by CreditNoteID desc");
            if (dsadvance.Tables[0].Rows.Count > 0)
            {
                givenamount = Convert.ToDecimal(txtbox_amontpay.Text)+Convert.ToDecimal(dsadvance.Tables[0].Rows[0]["credit_Amt"].ToString());
            }
            try
            {
                DataTable dtbiiling = new clsInsert().billingtransact(u_name);
                for (int i = 0; i < dtbiiling.Rows.Count; i++)
                {
                    if (Convert.ToDecimal(dtbiiling.Rows[i]["Amt_Outstanding"].ToString()) > Convert.ToDecimal(0.00))
                    {
                        //decimal paymenttopay_Compare = Convert.ToDecimal(dtbiiling.Rows[i]["Invoiced_Amount"].ToString()) - Convert.ToDecimal(dtbiiling.Rows[i]["DepositeAmount"].ToString());
                        decimal paymenttopay_Compare = Convert.ToDecimal(dtbiiling.Rows[i]["Amt_Outstanding"].ToString());
                        if (givenamount >= paymenttopay_Compare)
                        {
                         //   decimal paymenttopay = Convert.ToDecimal(dtbiiling.Rows[i]["totalAmount"].ToString()) - Convert.ToDecimal(dtbiiling.Rows[i]["DepositeAmount"].ToString());
                            decimal paymenttopay = Convert.ToDecimal(dtbiiling.Rows[i]["Amt_Outstanding"].ToString());
                           // decimal deposit = paymenttopay + Convert.ToDecimal(dtbiiling.Rows[i]["DepositeAmount"].ToString());
                            decimal dueamount = Convert.ToDecimal(0.00);
                            updatedeposit(dtbiiling.Rows[i]["Inv_num"].ToString(), Convert.ToDecimal(dueamount));
                            givenamount = givenamount - paymenttopay;
                        }
                        else
                        {
                            if (givenamount > 0)
                            {
                                decimal paymenttopay = Convert.ToDecimal(dtbiiling.Rows[i]["Amt_Outstanding"].ToString()) ;
                         //       decimal depositamount = Convert.ToDecimal(dtbiiling.Rows[i]["DepositeAmount"].ToString()) + givenamount;
                                decimal dueamount = Convert.ToDecimal(dtbiiling.Rows[i]["Amt_Outstanding"].ToString()) - Convert.ToDecimal(givenamount);
                                updatedeposit(dtbiiling.Rows[i]["Inv_num"].ToString(), Convert.ToDecimal(dueamount));
                                givenamount = Convert.ToDecimal(0.00);
                            }
                            else
                            {

                            }
                        }
                    }
                }
               // Response.Redirect("CashAccountForParker.aspx");
            }
            catch
            {
            }
            try
            {
                updatedeposit1();
            }
            catch
            {
            }
            try
            {
                if (givenamount > 0)
                {
                    updatecreditnote(givenamount);
                }
                else
                {
                    updatecreditnote1();
                }
            }
            catch
            {
            }
            try
            {



                DataSet_Bill h = new DataSet_Bill();
                h.Receipt_cashcounter.Clear();

                DataRow dr1 = h.Receipt_cashcounter.NewRow();
                dr1[0] = "1";
                dr1[1] = txtbox_name.Text;

                dr1[2] = txtbox_amontpay.Text;

                dr1[3] = "By" + " " + ddl_transactiontype.SelectedItem.Text;
                if (ddl_transactiontype.SelectedItem.Text == "Cheque")
                {
                    dr1[4] = txtbox_tranasactiontype.Text;
                }
                if (ddl_transactiontype.SelectedItem.Text == "Credit Card")
                {
                    dr1[4] = txtbox_tranasactiontype.Text;

                }
                if (ddl_transactiontype.SelectedItem.Text == "Cash")
                {
                    dr1[4] = txtbox_tranasactiontype.Text;

                }


                dr1[5] = txtbox_parkingtype.Text;

                dr1[6] = txtbox_plan.Text;

                dr1[7] = txtbox_planrate.Text;
                string formatted = txtbox_amontpay.Text;
                if (formatted.Contains('.'))
                {
                    string[] arr = txtbox_amontpay.Text.Split('.');
                    string s = "$ " + NumberToText(long.Parse(arr[0].ToString())) + " And " + NumberToText(long.Parse(arr[1].ToString())) + " Cents";
                    dr1[8] = s;
                }
                else
                {
                    string s = "$ " + NumberToText(long.Parse(txtbox_amontpay.Text));

                    dr1[8] = s;
                }

                h.Receipt_cashcounter.Rows.Add(dr1);

                Session["detail_invoice"] = h;
                Session["CashGiven"] = "True";
                Response.Redirect("CashAccountForParker.aspx");

                //ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Receipt.aspx','New Windows','height=580, width=600,location=no');", true);

            }
            catch
            {
            }

        }
        else
        {
            lblerror.Visible = true;
            lblerror.Text = "Please Enter Value ";
        }

    }

    string stat1 = "False";
    string stat = "true";
    public void updatedeposit(string invoicenumber,decimal dueamount)
    {
        hst.Clear();
        hst.Add("action", "update");
        hst.Add("invoicenumber", invoicenumber);

        hst.Add("dueamount", dueamount);

        int result = cs.ExecuteNonQuery("[sp_BillingTransact]", CommandType.StoredProcedure, hst);
        if(result>0)
        {
          //  hst.Clear(); 
          // // @acctnum,@amountpaid,@bocmmnt,@reconciled,@status,@transactiondate,@paymntmenthod)    
          ////  depositamount
          //  hst.Add("action", "depositamount");
          //  hst.Add("acctnum", lbl_username.Text);
          //  hst.Add("bocmmnt", "Cash");
          //  hst.Add("amountpaid", Convert.ToDecimal(txtbox_amontpay.Text));
          //  hst.Add("reconciled", stat1);
          //  hst.Add("status", stat);
          //  hst.Add("transactiondate", DateTime.Now);
          //  hst.Add("paymntmenthod", "Cash");
          //  //hst.Add("dateofpayment", DateTime.Now);
          //  //@paymntmenthod,@dateofpayment
          //  int result_payment = cs.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
          //  if (result_payment > 0)
          //  {
          //      //upadate tbl_invoice amount_outstanding amount



                
          //  }
        }

    }

    public void updatedeposit1()
    {
        hst.Clear();
       
            hst.Clear();
            // @acctnum,@amountpaid,@bocmmnt,@reconciled,@status,@transactiondate,@paymntmenthod)    
            //  depositamount
            hst.Add("action", "depositamount");
            hst.Add("acctnum", lbl_username.Text);
            if (ddl_transactiontype.SelectedItem.Text == "Cheque")
            {
                string Comment = "By Cheque" + "And The Cheque Number IS:" + txtbox_tranasactiontype.Text;
                hst.Add("bocmmnt", Comment);

            }
            if (ddl_transactiontype.SelectedItem.Text == "Credit Card")
            {
                string Comment = "By Credit Card" + "And The Credit Card Code IS:" + txtbox_tranasactiontype.Text;

                hst.Add("bocmmnt", Comment);

            }
            if (ddl_transactiontype.SelectedItem.Text == "Cash")
            {
                hst.Add("bocmmnt", "By Cash");

            }
            hst.Add("amountpaid", Convert.ToDecimal(txtbox_amontpay.Text));
            hst.Add("reconciled", stat1);
            hst.Add("status", stat);
            hst.Add("transactiondate", DateTime.Now);
            hst.Add("paymntmenthod", "Cash");
            //hst.Add("dateofpayment", DateTime.Now);
            //@paymntmenthod,@dateofpayment
            int result_payment = cs.ExecuteNonQuery("[sp_BillingTransact]", CommandType.StoredProcedure, hst);
            if (result_payment > 0)
            {
                //upadate tbl_invoice amount_outstanding amount




            }
        

    }

    public void updatecreditnote(decimal money)
    {
        hst.Clear();
        hst.Add("action", "insert");
        hst.Add("acctnumber", u_name);
        hst.Add("reason", "Advance At the Time Of Registration");
        hst.Add("creditamt", Convert.ToDecimal(money));
        hst.Add("dateoftransaction", DateTime.Now);
        hst.Add("status", stat);

        int result_billing_full = cs.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
        {
        }
    }
    public void updatecreditnote1()
    {
        hst.Clear();
        hst.Add("action", "insert");
        hst.Add("acctnumber", u_name);
        hst.Add("reason", "Advance At the Time Of Registration");
        hst.Add("creditamt", Convert.ToDecimal("0.00"));
        hst.Add("dateoftransaction", DateTime.Now);
        hst.Add("status", stat);

        int result_billing_full = cs.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
        {
        }
    }
    protected void txtbox_amontpay_TextChanged(object sender, EventArgs e)
    {
        //if (Convert.ToDecimal(txtbox_amontpay.Text) > Convert.ToDecimal(txtbox_totaldue.Text.TrimStart('$')))
        //{
        //    txtbox_amontpay.Text = "";
        //    lblerror.Visible = true;
        //    lblerror.Text = "Please Enter Value Less Than Due Amount";
        //}
        //else
        //{
        //    lblerror.Visible = false;
        //    txtbox_amontpay.Text = txtbox_amontpay.Text;
        //}
    }

    public static string NumberToText(long number)
    {
        StringBuilder wordNumber = new StringBuilder();

        string[] powers = new string[] { "Thousand ", "Million ", "Billion " };
        string[] tens = new string[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        string[] ones = new string[] { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", 
                                       "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };

        if (number == 0) { return "Zero"; }
        if (number < 0)
        {
            wordNumber.Append("Negative ");
            number = -number;
        }

        long[] groupedNumber = new long[] { 0, 0, 0, 0 };
        int groupIndex = 0;

        while (number > 0)
        {
            groupedNumber[groupIndex++] = number % 1000;
            number /= 1000;
        }

        for (int i = 3; i >= 0; i--)
        {
            long group = groupedNumber[i];

            if (group >= 100)
            {
                wordNumber.Append(ones[group / 100 - 1] + " Hundred ");
                group %= 100;

                if (group == 0 && i > 0)
                    wordNumber.Append(powers[i - 1]);
            }

            if (group >= 20)
            {
                if ((group % 10) != 0)
                    wordNumber.Append(tens[group / 10 - 2] + " " + ones[group % 10 - 1] + " ");
                else
                    wordNumber.Append(tens[group / 10 - 2] + " ");
            }
            else if (group > 0)
                wordNumber.Append(ones[group - 1] + " ");

            if (group != 0 && i > 0)
                wordNumber.Append(powers[i - 1]);
        }

        return wordNumber.ToString().Trim();
    }

    protected void txtbox_tranasactiontype_TextChanged(object sender, EventArgs e)
    {

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_transactiontype.SelectedItem.Text == "Cheque")
        {
            lbltransaction.Visible = true;
            txtbox_tranasactiontype.Visible = true;
            lbltransaction.Text = "Enter Cheque Number";
        }
        if (ddl_transactiontype.SelectedItem.Text == "Credit Card")
        {
            lbltransaction.Visible = true;
            txtbox_tranasactiontype.Visible = true;
            lbltransaction.Text = "Enter Credit Card  Code";

        }
        if (ddl_transactiontype.SelectedItem.Text == "Cash")
        {
            lbltransaction.Visible = false;
            txtbox_tranasactiontype.Visible = false;
            txtbox_tranasactiontype.Text = "";
            lbltransaction.Text = "";
        }
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("CashAccountForParker.aspx");
        
    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
       try
       {



             DataSet_Bill h = new DataSet_Bill();
             h.Receipt_cashcounter.Clear();
     
             DataRow dr1 = h.Receipt_cashcounter.NewRow();
                     dr1[0] = "1";
                     dr1[1]=txtbox_name.Text;

                     dr1[2] = txtbox_amontpay.Text;

                     dr1[3] = "By" +" "+ ddl_transactiontype.SelectedItem.Text;
                  if (ddl_transactiontype.SelectedItem.Text == "Cheque")
                 {
                     dr1[4] = txtbox_tranasactiontype.Text;
                 }
                 if (ddl_transactiontype.SelectedItem.Text == "Credit Card")
                 {
                     dr1[4] = txtbox_tranasactiontype.Text;
          
                 }
                 if (ddl_transactiontype.SelectedItem.Text == "Cash")
                 {
                    dr1[4] = txtbox_tranasactiontype.Text;
           
                 }
                   
                    
                        dr1[5] = txtbox_parkingtype.Text;
                    
                        dr1[6] = txtbox_plan.Text;
                    
                    dr1[7] = txtbox_planrate.Text;
                    string formatted = txtbox_amontpay.Text;
                    if (formatted.Contains('.'))
                    {
                        string[] arr = txtbox_amontpay.Text.Split('.');
                        string s = "$ " + NumberToText(long.Parse(arr[0].ToString())) + " And " + NumberToText(long.Parse(arr[1].ToString())) + " Cents";
                        dr1[8] = s;
                    }
                    else
                    {
                        string s = "$ " + NumberToText(long.Parse(txtbox_amontpay.Text)) ;

                        dr1[8] = s;
                    }

                    h.Receipt_cashcounter.Rows.Add(dr1);
                        
                Session["detail_invoice"] = h;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Receipt.aspx','New Windows','height=580, width=600,location=no');", true);

            }
            catch
            {
            }
          
        }
    public string DecimalToWords(decimal d)
    {
        //Grab a string form of your decimal value ("12.34")
        string formatted = d.ToString();

        if (formatted.Contains("."))
        {
            //If it contains a decimal point, split it into both sides of the decimal

            string[] sides = formatted.Split('.');
            //Process each side and append them with "and", "dot" or "point" etc.
            return NumberToText(int.Parse(sides[0])) + " and " + NumberToText(int.Parse(sides[1]));
        }
        else
        {
            //Else process as normal
            return NumberToText(Convert.ToInt32(d));
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("CashAccountForParker.aspx");

    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Category.ascx.cs" Inherits="WUC_wuc_Category" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 169px;
    }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>

    <br />
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server">

    <table class="form-table" width="60%">
        <tr>
            <td class="style2" valign="top">
                 Name
            </td>
            <td>
                <asp:TextBox ID="txtbox_Catname" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_Catname_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_Catname" WatermarkCssClass="watermark" WatermarkText="Enter Category Name">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_catname" runat="server" 
                    ControlToValidate="txtbox_Catname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2" valign="top">
            Description</td>
            <td>
                <asp:TextBox ID="txtbox_catdescription" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_catdescription_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_catdescription" WatermarkCssClass="watermark" WatermarkText="Enter Category Description">
                </cc1:TextBoxWatermarkExtender>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="a" CssClass="button"
                    onclick="btn_add_Click"  Height="35px" Width="79px" />
            </td>
        </tr>
    </table>
    </asp:Panel>
<asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="SELECT * FROM [tbl_CategoryMaster]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rptcategory" DataSourceID="SqlDataSource2" onitemcommand="rptcategory_ItemCommand" 
                >
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Category&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Category&nbsp;&nbsp;Description
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("CategoryName")%>
                    </td><td>
                    <%#Eval("CategoryDesc")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("CategoryId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("CategoryId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>
                    <asp:Label ID="id" runat="server" Text='<%# Eval("CategoryId") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="categoryname" runat="server" Text='<%# Eval("CategoryName") %>' 
                                        Visible="False" />
  <asp:Label ID="categorydesc" runat="server" Text='<%# Eval("CategoryDesc") %>' 
                                        Visible="False"></asp:Label>
 
         
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>


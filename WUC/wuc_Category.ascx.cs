﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_Category : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["message"] == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
        }
        if (Request.QueryString["msg"] == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
        }
        if (!IsPostBack == true)
        {
            try
            {

                if (Session["lbladd"].ToString() == "True")
                {
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                }



            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        // Response.Redirect("Deshboard.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_deshboard.aspx");

                    }
                }
                catch
                {
                    //  Response.Redirect("default.aspx");
                }
            }
        }


    }
    string msg;
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
            
            AddCategory();
     
            clear();
           
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void AddCategory()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("Categoryname", txtbox_Catname.Text);
            hst.Add("CategoryDesc", txtbox_catdescription.Text);
          
            hst.Add("operator", "lovey_operator");
            hst.Add("modified", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[Sp_Category]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CategoryMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CategoryMaster.aspx?message=" + msg);

                }
         

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);
               

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);
           

        }
    }
    public void clear()
    {
        txtbox_catdescription.Text = "";
        txtbox_Catname.Text = "";
        
    }
    protected void rptcategory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
           
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delcategory(ID);
                    Response.Redirect("Admin_CategoryMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delcategory(ID);
                        Response.Redirect("Operator_CategoryMaster.aspx");
                    }
                }

            }
            catch
            {
                // Response.Redirect("default.aspx");
            }





            
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rptcategory.Items[i].FindControl("categoryname") as Label;
            Label l1 = rptcategory.Items[i].FindControl("categorydesc") as Label;
                      
            Label l3 = rptcategory.Items[i].FindControl("id") as Label;

            try
            {

                Session["categoryname"] = l.Text;
                Session["categorydesc"] = l1.Text;

                Session["Id"] = l3.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");
            }

            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditCategoryMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);
                    
                }
                else
                {                    
                    Response.Redirect("Operator_EditCategoryMaster.aspx");
                }
            }
        }
    }
    public void delcategory(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[Sp_Category]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

                //Response.Redirect("Admin_CategoryMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

           // Response.Redirect("Admin_CategoryMaster.aspx");

        }


    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_ChargeByMaster.ascx.cs" Inherits="WUC_wuc_ChargeByMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 152px;
    }
    
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

</style>


    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:Panel ID="Panel2" runat="server">
    <div class="form">
    <label>Charge Name</label>
     <asp:TextBox ID="txtbox_chargename" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_chargename" runat="server" 
                    ControlToValidate="txtbox_chargename" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
                    <label>Description</label>
                    <asp:TextBox ID="txtbox_chargedescription" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_chargedescription" runat="server" 
                    ControlToValidate="txtbox_chargedescription" ErrorMessage="*" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
                    <label>&nbsp;</label> <asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="a"  
                    CssClass=button onclick="btn_add_Click1"/>
    </div>

    
        </asp:Panel>
<br />
<div style="width:100%; float:left;">
    <asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Vertical">
    <fieldset>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand1" 
                DataSourceID="SqlDataSource1">
     <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Charge&nbsp;Name
                    </th>
                     <th>
                        Description
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                    <th width="5%">
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>
                    <%#Eval("chargebyname")%>
                    </td><td>
                    <%#Eval("descr")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("chargebyid") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("chargebyid") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>
                    <asp:Label ID="chargename" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False" />
                    <asp:Label ID="description" runat="server" Text='<%# Eval("descr") %>' 
                                        Visible="False"></asp:Label>
                   
                                        <asp:Label ID="id" runat="server" Text='<%# Eval("chargebyid") %>' 
                                        Visible="False"></asp:Label>
                    
            </tr>
        </ItemTemplate>
           <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT * FROM [tbl_Chargeby]"></asp:SqlDataSource>
            </ContentTemplate>
        
    </asp:UpdatePanel>
    </fieldset>
    </asp:Panel>
</div>



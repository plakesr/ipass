﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_ChargeByMaster : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }
    
        }
    }
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    public void delCharge(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("chargebyid", id);


            int result = objData.ExecuteNonQuery("[sp_Chargeby]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // Response.Redirect("Admin_ChargebyMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

              //  Response.Redirect("Admin_ChargebyMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

            //Response.Redirect("Admin_ChargebyMaster.aspx");

        }


    }
    protected void btn_add_Click(object sender, EventArgs e)
    {

    }
    protected void Repeater1_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delCharge(ID);
                    Response.Redirect("Admin_ChargebyMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delCharge(ID);
                        Response.Redirect("Operator_ChargebyMaster.aspx");
                    }
                }

            }
            catch
            {
                // Response.Redirect("default.aspx");
            }
            
        }
        if (e.CommandName == "cmdEdit")
        {
            Label l = Repeater1.Items[i].FindControl("chargename") as Label;
            Label l1 = Repeater1.Items[i].FindControl("description") as Label;

            try
            {
                Session["chargename"] = l.Text;
                Session["description"] = l1.Text;
           
            int id = Convert.ToInt32(e.CommandArgument.ToString());
            Session["id"] = id;
            }
            catch
            {
                Response.Redirect("Default.aspx");

            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditChargebyMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditChargebyMaster.aspx");
                }
            }
           // Response.Redirect("Admin_EditChargebyMaster.aspx");

        }
    }
    string msg;
    protected void btn_add_Click1(object sender, EventArgs e)
    {

        try
        {
           // string msg = "1";
            addchargeby();
           // Response.Redirect("Admin_ChargebyMaster.aspx?message=" + msg);

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);

        }
    }
    public void addchargeby()
    {
        try
        {
            hst.Clear();

           hst.Add("action", "insert");
           hst.Add("chargebyname", txtbox_chargename.Text);
           hst.Add("descr", txtbox_chargedescription.Text);
           int result = objData.ExecuteNonQuery("[sp_Chargeby]", CommandType.StoredProcedure, hst);
           if (result > 0)
           {
               msg = "1";
               if (Session["Type"].ToString() == "admin")
               {
                   Response.Redirect("Admin_ChargebyMaster.aspx?message=" + msg);
               }
               if (Session["Type"].ToString() == "operator")
               {
                   Response.Redirect("Operator_ChargebyMaster.aspx?message=" + msg);

               }
             //  Response.Redirect("Admin_ChargebyMaster.aspx");

           }
           else
           {
               ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);
             

           }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);
            

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_CoupanSearch : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    protected void btn_search_Click(object sender, EventArgs e)
    {
        try
        {
            
            Coupansearch();
         
          
        }
        catch
        {
        }
    }



    protected void Item_Command(Object sender, DataListCommandEventArgs e)
    {
       // DataList1.SelectedIndex = e.Item.ItemIndex;
       // int i = e.Item.ItemIndex;
       // if (e.CommandName == "cmdLotDetails")
       //{
            
       //    Label lotid = DataList1.Items[i].FindControl("lotid") as Label;
       //    Label coupantype = DataList1.Items[i].FindControl("coupantype") as Label;
       //    Label CoupanDiscountLabel = DataList1.Items[i].FindControl("CoupanDiscountLabel") as Label;

       //    Session["lotid"] = lotid.Text;
       //    Session["coupantype"] = coupantype.Text;
       //    Session["CoupanDiscount"] = CoupanDiscountLabel.Text;
       //    Session["Coupancode"] = txtbox_coupannumber.Text;

       //    Response.Redirect("Lot_Details.aspx");
       
       // }

        

    }
    public void Coupansearch()
    {
        DataTable dt = new clsInsert().getcoupan(txtbox_coupannumber.Text);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["status"].ToString() == "True" && dt.Rows[0]["Active_Inactive"].ToString() == "True" )
            {
                //DataList1.Visible = true;

                Session["PlanrateID"] = dt.Rows[0]["LotRateid"].ToString();
                Session["LOTIDForInserting"] = dt.Rows[0]["Lot_id"].ToString();
                Session["parkinttype"] = dt.Rows[0]["Parkingtype"].ToString();
                Session["amount_parking"] = dt.Rows[0]["CoupanDiscount"].ToString();
                Session["Typeinv"] = dt.Rows[0]["CoupanType"].ToString();
                Session["Chargebyinv"] = dt.Rows[0]["chargebyname"].ToString();
                Session["Coupancode"] = dt.Rows[0]["Coupan"].ToString();
                Session["Lotnamefinv"]=dt.Rows[0]["LotName"].ToString();
                DataTable dt11 = new clsInsert().get_Lotdetails_lotsearch(Convert.ToInt32(dt.Rows[0]["Lot_id"].ToString()));
                if (Convert.ToDateTime(dt11.Rows[0]["EffectiveDate"].ToString()) < DateTime.Now)
                {
                    try
                    {
                        if (dt11.Rows.Count > 0)
                        {
                           
                            //DataSet dscount = new clsInsert().fetchrec("select count(tbl_ParkerRegis.UserName) as countspace from tbl_ParkerRegis where LotId=" + Convert.ToInt32(Session["LOTIDForInserting"].ToString()));
                            // if (dscount.Tables.Count > 0)
                            // {
                            //     int lottotalspace = Convert.ToInt32(dt11.Rows[0]["TotalSpaces"].ToString());
                            //     int lotunablespace = Convert.ToInt32(dt11.Rows[0]["UnableSpace"].ToString());
                            //     int availabeltotalspace = lottotalspace - lotunablespace;
                            //     int counttotal = Convert.ToInt32(dscount.Tables[0].Rows[0][0].ToString());
                            //     int countspace = availabeltotalspace - counttotal;
                            //     if (countspace <= 0)
                            //     {
                            //         ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No space.')", true);
                            //         txtbox_coupannumber.Text = "";
                            //     }
                            //     else
                            //     {
                                     Response.Redirect("Registration_New_Parker.aspx");
                                 //}


                           //  }
                        }
                    }
                    catch
                    {
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Lot is not active.')", true);
                    txtbox_coupannumber.Text = "";
                }
               

               
            }
            if (dt.Rows[0]["status"].ToString() == "False" && dt.Rows[0]["Active_Inactive"].ToString() == "False")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('coupon number has used.')", true);
                txtbox_coupannumber.Text = "";
            
            }

            if (dt.Rows[0]["status"].ToString() == "False" && dt.Rows[0]["Active_Inactive"].ToString() == "True")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('coupon number is not active.')", true);
                txtbox_coupannumber.Text = "";

            }
          
        }

        
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('coupon number is wrong.')", true);


        }

    }
    
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_DetailOfRefundParker.ascx.cs" Inherits="WUC_wuc_DetailOfRefundParker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

 <script src="WUC/js/jquery.min.js" type="text/javascript"></script>

   <script>
       // JQUERY ".Class" SELECTOR.
       $(document).ready(function () {
           $(".groupOfTexbox").keypress(function (event) { return isNumber(event) });
       });

       // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
       function isNumber(evt) {
           var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) &&
                (charCode < 48 || charCode > 57))
               return false;

           return true;
       }    
</script>
 <style>
        .groupOfTexbox {color:#000; font:11px/1.5 Arial, tahoma; padding:2px 4px; 
            border:solid 1px #BFBFBF; border-radius:2px; -moz-border-radius:2px; -webkit-border-radius:2px}
     .style1
     {
         width: 186px;
     }
     .style2
     {
         width: 217px;
     }
     .style3
     {
         width: 222px;
     }
     .style4
     {
         width: 223px;
     }
     .style5
     {
         width: 225px;
     }
     .style6
     {
         width: 227px;
     }
     .style7
     {
         width: 228px;
     }
     .style8
     {
         width: 234px;
     }
    </style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

<div >
    <asp:Panel ID="Panel4" runat="server">
    <fieldset>
    <legend>Client/Location Detail</legend>

        <table class="form-table">
            <tr>
                <td class="style40" width="150">
                    Client Name</td>
                <td>
                    <asp:TextBox ID="txtbox_clientname" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style40">
                    Campus Name</td>
                <td>
                    <asp:TextBox ID="txtbox_campusname" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style40">
                    Lot Name</td>
                <td>
                    <asp:TextBox ID="txtbox_lotname" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
          <%--  <tr>
                <td class="style40">
                    Refund Tracking Id</td>
                <td>
                    <asp:TextBox ID="txtbox_trackingid" runat="server" CssClass="twitterStyleTextbox" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
            --%>
            <tr>
                <td class="style40">
                    Device/Machine #</td>
                <td>
                    <asp:TextBox ID="txtbox_machine" runat="server" CssClass="twitterStyleTextbox" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
        </table>

    </fieldset>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server">
 <fieldset>
 <legend>Parker's Detail</legend>
    <table class="form-table">
        <tr>
            <td class="style2" width="150">
                First Name</td>
            <td class="style6">
                <asp:TextBox ID="txtbox_firstname" runat="server" 
                    CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
            </td>
            <td width="50"></td>
            <td class="style7" width="150">
                Last Name</td>
            <td class="style20">
                <asp:TextBox ID="txtbox_lastname" runat="server"  
                    CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
            </td>
            <td rowspan="5" valign="top">
                <%--<asp:Image ID="imgreceipt" runat="server" Height="225px" Width="247px" />--%>
           </td>
        </tr>
        <tr>
            <td class="style2">
                Address</td>
            <td class="style6">
                <asp:TextBox ID="txtbox_address" runat="server"  style=" resize:none;"  
                    TextMode="MultiLine"  CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
            </td>
             <td width="50"></td>
            <td class="style7">
                APT/UNIT#</td>
            <td class="style20">
                <asp:TextBox ID="txtbox_apt" runat="server" CssClass="twitterStyleTextbox" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Cell Number</td>
            <td class="style6">
                <asp:TextBox ID="txtbox_cellnumber" runat="server"  
                    CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
            </td>
             <td width="50"></td>
            <td class="style7">
                E-mail</td>
            <td class="style20">
                <asp:TextBox ID="txtbox_email" runat="server"  CssClass="twitterStyleTextbox" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style14">
                Licence Plate</td>
            <td class="style15">
                <asp:TextBox ID="txtbox_licence" runat="server"  CssClass="twitterStyleTextbox" 
                    Enabled="False"></asp:TextBox>
            </td>
             <td width="50"></td>
            <td class="style16">
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                <%--Signature--%></td>
            <td class="style6">
               <%-- <asp:Image ID="imgsignature" runat="server" Height="100px" Width="234px" />--%>
            </td>
            <td class="style7">
                &nbsp;</td>
            <td class="style20">
                &nbsp;</td>
        </tr>
        </table>
         </fieldset>
     </asp:Panel>
        <br />
     <asp:Panel ID="Panel2" runat="server">
     <fieldset>
     <legend>Payment Detail</legend>
      
        <table  class="form-table">
        <tr>
            <td class="style51">
                TKT – Ticket No.</td>
            <td class="style49">
                 &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtbox_ticketno" runat="server" CssClass="twitterStyleTextbox" 
                    Enabled="False"></asp:TextBox>
            </td>
            <td class="style45">
                &nbsp;</td>
            <td class="style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style51">
                Method of payment</td>
            <td class="style52">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                         &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rbt_cash" runat="server" AutoPostBack="true" 
                            Enabled="False" GroupName="r" Text="CASH" />
                        &nbsp;
                        <asp:RadioButton ID="rbt_debit" runat="server" AutoPostBack="true" 
                            Enabled="False" GroupName="r" Text="DEBIT" />
                        &nbsp;
                        <asp:RadioButton ID="rbt_credit" runat="server" AutoPostBack="true" 
                            Enabled="False" GroupName="r" Text="CREDIT" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="style45">
                 &nbsp; &nbsp; &nbsp;<asp:Label ID="lbl_credit_authorization" runat="server" 
                    Text="Credit Card Authorization Code" Visible="False"></asp:Label>
            </td>
            <td class="style3">
              
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtbox_authorisation" runat="server" 
                    CssClass="twitterStyleTextbox" Enabled="False" Height="16px" Visible="False" 
                    Width="103px"></asp:TextBox>
              
            </td>
        </tr>
        <tr>
            <td class="style51">
                Total Paid By Customer&nbsp;&nbsp;&nbsp;</td>
            <td class="style52">
                $&nbsp;
                <asp:TextBox ID="txtbox_givenamount" runat="server" Enabled="False" 
                    Height="19px" Width="130px"></asp:TextBox>
            </td>
            <td class="style45">
                Total Amount Customer is <br />Requesting to be Refunded </td>
            <td class="style3">
                 $&nbsp;<asp:TextBox ID="txtbox_requestedamount" runat="server" Height="18px" 
                     Width="130px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style51">
                Date of Refund Request</td>
            <td class="style52">
                 &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtbox_transcationdate" runat="server"  
                    CssClass="twitterStyleTextbox" Enabled="False" Height="20px" Width="130px"></asp:TextBox>
               
            </td>
            <td class="style45">
                Date Of Transaction </td>
            <td class="style3">
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtbox_dateofreceipt" runat="server" 
                    Height="16px" Width="130px" Enabled="False" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style51">
                Reason for the refund request</td>
            <td class="style3" colspan="3">
                
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtbox_reason" runat="server" Height="81px"  CssClass="twitterStyleTextbox" 
                    style=" resize:none;" TextMode="MultiLine" Width="560px" Enabled="False"></asp:TextBox>
                
            </td>
        </tr>
          

          
            <tr>
                             <td class="style51">
                  <asp:Label ID="lblstat" runat="server" Text="Status" Visible="true"></asp:Label>   </td>

                <td class="style3" colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rbtn_accepted" runat="server" GroupName="a" 
                        oncheckedchanged="rbtn_accepted_CheckedChanged" Text="Accepted"  AutoPostBack="true"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtn_rejected" runat="server" GroupName="a" 
                        oncheckedchanged="rbtn_rejected_CheckedChanged" Text="Rejected" AutoPostBack="true"/>
                </td>
            </tr>
         


            <tr>
                <td class="style51">
                    <asp:Label ID="lbl_reason" runat="server" Text="Reason" Visible="False"></asp:Label>
                </td>
                <td class="style3" colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtbox_rejectreason" style=" resize:none;" runat="server" Height="64px"  
                        TextMode="MultiLine" Width="726px"></asp:TextBox>
                </td>
                
            </tr>
    </table>
   
    </fieldset>
       </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" Visible="false">
    <fieldset>
    <legend>Office Use Only</legend>
      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
    <table  class="form-table">

         
        <tr>
        
            <td class="style1">
                Total paid by customer</td>
            <td class="style53">
                <asp:TextBox ID="txtbox_customerpaid" runat="server" Height="10px" 
                    Width="104px" Enabled="False"></asp:TextBox>
            </td>
            <td class="style8">
               - Minus approved parking charge $</td>

            <td  style="width:100px;">
               <asp:TextBox ID="txtbox_parkingcharges" runat="server" AutoPostBack="true" 
                    ontextchanged="txtbox_parkingcharges_TextChanged" Height="10px" CssClass="groupOfTexbox"
                    Width="104px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="approvedparking" runat="server" 
                    ControlToValidate="txtbox_parkingcharges" ErrorMessage="*" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>   
                   
            </td>

            <td class="style48">

             Approved refund amount
                </td>

            <td>
                <asp:TextBox ID="txtbox_refundamount" runat="server" Enabled="False" 
                    Height="10px" Width="104px" style="margin-left: 25px"></asp:TextBox>
            </td>
            
        </tr>
        </table>
        
      </ContentTemplate>
                     <Triggers>
       
        <asp:AsyncPostBackTrigger ControlID="txtbox_parkingcharges" EventName="TextChanged" />
       

    </Triggers>
                </asp:UpdatePanel>
        <table class="form-table" align="left">
      
        <tr>
            <td class="style34">
                <asp:Label ID="lbl_pmsign" runat="server" Text="PM Refund Approval Signature" 
                    Visible="False"></asp:Label>
            </td>
            <td class="style53">
                <asp:FileUpload ID="PM_signature" runat="server" Height="22px" Width="191px" 
                    Visible="False" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style34">
                <asp:Label ID="lbl_cmsign" runat="server" 
                    Text="Client Manager Approval Signature" Visible="False"></asp:Label>
            </td>
            <td class="style53">
                <asp:FileUpload ID="PMO_signature" runat="server" Height="22px" Width="172px" 
                    Visible="False" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    </fieldset>
    </asp:Panel>
       <table>
       <tr>
       <td class="style5">
           <asp:Button ID="Button1" runat="server" Text="Ok"  CssClass="button" 
               Visible="false" onclick="Button1_Click"/>
           </td>
       <td class="style4">
       <asp:Button ID="btn_send" runat="server"  CssClass="button" 
               ValidationGroup="a" Height="29px" Width="83px" onclick="btn_send_Click" />
           </td>
       </tr>
       </table>
</div>
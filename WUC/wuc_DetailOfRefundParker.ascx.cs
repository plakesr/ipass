﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
public partial class WUC_wuc_DetailOfRefundParker : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                DataTable dtdeatails=new clsInsert().getdetailsofrefundcustomer(Convert.ToInt32(Session["refund_customerid"].ToString()));
                if (dtdeatails.Rows.Count > 0)
                {
                    txtbox_address.Text = dtdeatails.Rows[0]["Address"].ToString() + "," + dtdeatails.Rows[0]["City"].ToString()+","+dtdeatails.Rows[0]["Province"].ToString()+" "+dtdeatails.Rows[0]["Postal_code"].ToString() ;
                    
                    txtbox_apt.Text = dtdeatails.Rows[0]["Apt"].ToString();
                    if (dtdeatails.Rows[0]["BillingType"].ToString() == "1")
                    {
                        rbt_cash.Checked = true;
                    }
                    if (dtdeatails.Rows[0]["BillingType"].ToString() == "2")
                    {
                        rbt_debit.Checked = true;
                    }
                    if (dtdeatails.Rows[0]["BillingType"].ToString() == "3")
                    {
                        rbt_credit.Checked = true;
                        lbl_credit_authorization.Visible = true;
                        txtbox_authorisation.Visible = true;
                        txtbox_authorisation.Text = dtdeatails.Rows[0]["AuthorisationCode_CreditCard"].ToString();

                    }
                    txtbox_cellnumber.Text = dtdeatails.Rows[0]["phone_no"].ToString();
                    
                    txtbox_email.Text = dtdeatails.Rows[0]["e_mail"].ToString();
                    txtbox_firstname.Text = dtdeatails.Rows[0]["F_name"].ToString();
                    txtbox_lastname.Text = dtdeatails.Rows[0]["L_name"].ToString();
                    txtbox_licence.Text = dtdeatails.Rows[0]["LicensePlate"].ToString();
                  //  txtbox_machine.Text = dtdeatails.Rows[0]["DeviceMachineID"].ToString();
                      txtbox_machine.Text = dtdeatails.Rows[0]["DeviceMachineID"].ToString();
                   // txtbox_trackingid.Text = dtdeatails.Rows[0]["REFUND_TRACKING_ID"].ToString();
                    txtbox_reason.Text = dtdeatails.Rows[0]["ReasonOfRefund"].ToString();
                    txtbox_ticketno.Text = dtdeatails.Rows[0]["TicketNo"].ToString();
                    txtbox_transcationdate.Text = dtdeatails.Rows[0]["DateofRequest"].ToString();
                    txtbox_requestedamount.Text = dtdeatails.Rows[0]["RefundAmountRequesting"].ToString();
                    txtbox_givenamount.Text = dtdeatails.Rows[0]["TotalAmountGiven"].ToString();
                    txtbox_dateofreceipt.Text = dtdeatails.Rows[0]["DateOfReceipt"].ToString();
                    txtbox_clientname.Text= dtdeatails.Rows[0]["SiteName"].ToString();

                    txtbox_campusname.Text = dtdeatails.Rows[0]["Campus_Name"].ToString();
                    txtbox_lotname.Text = dtdeatails.Rows[0]["LotName"].ToString();



                   
                    Session["sign"] = dtdeatails.Rows[0]["customer_signature"].ToString();
                  
                    lbl_reason.Visible = false;
                    txtbox_rejectreason.Visible = false;
                }
            }
            catch
            {
            }
            try
            {
                if (Session["approval"].ToString() == "true")
                {
                    rbtn_accepted.Enabled = false;
                    rbtn_rejected.Enabled = false;

                    rbtn_accepted.Checked = true;
                    Panel3.Visible = true;
                    Panel3.Enabled = false;
                    btn_send.Text = "Back";
                    txtbox_customerpaid.Visible = true;

                    txtbox_parkingcharges.Visible = true;
                    txtbox_refundamount.Visible = true;
                    DataTable dtdeatails1 = new clsInsert().getdetailsofrefundcustomercm(Convert.ToInt32(Session["refund_customerid"].ToString()));
                    if (dtdeatails1.Rows.Count > 0)
                    {
                        txtbox_customerpaid.Text = dtdeatails1.Rows[0]["TotalAmountGiven"].ToString();
                        txtbox_parkingcharges.Text = dtdeatails1.Rows[0]["ApprovedParkingCharges"].ToString();
                        txtbox_refundamount.Text = dtdeatails1.Rows[0]["ApprovedRefundAmount"].ToString();
                    }
                }
                if (Session["approval"].ToString() == "false")
                {
                    rbtn_accepted.Enabled = false;
                    rbtn_rejected.Enabled = false;

                    rbtn_rejected.Checked = true;
                    Panel3.Visible = false;
                    Panel3.Enabled = false;
                    btn_send.Visible = false;
                    Button1.Visible = true;
                    lbl_reason.Visible = true;
                    txtbox_rejectreason.Visible = true;
                    Panel3.Visible = false;
                    txtbox_parkingcharges.Text = "";
                    txtbox_refundamount.Text = "";
                    txtbox_rejectreason.Enabled = false;

                    DataTable dtdeatails1 = new clsInsert().refundcustomerdetailforrejection(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
                    if (dtdeatails1.Rows.Count > 0)
                    {
                        if (Session["operatorid"].ToString() == "PM")
                        {
                            txtbox_rejectreason.Text = dtdeatails1.Rows[0]["PM_DeclineReason"].ToString();

                        }
                        if (Session["operatorid"].ToString() == "Client Manager")
                        {
                            txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                        }
                        if (Session["operatorid"].ToString() == "Data Center")
                        {
                            txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                        }
                        if (Session["operatorid"].ToString() == "Accounting")
                        {
                            txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                        }
                        if (Session["operatorid"].ToString() == "PMO")
                        {
                            txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                        }
                    }
                }




                if (Session["approval"].ToString() == "Pending_PMO")
                {
                    rbtn_accepted.Enabled = false;
                    rbtn_rejected.Enabled = false;
                    lblstat.Visible = false;
                    rbtn_accepted.Visible = false;
                    rbtn_rejected.Visible = false;
                   // rbtn_rejected.Checked = true;
                    Panel3.Visible = false;
                    Panel3.Enabled = false;
                    btn_send.Visible = false;
                    Button1.Visible = true;
                   // lbl_reason.Visible = true;
                  //  txtbox_rejectreason.Visible = true;
                    Panel3.Visible = false;
                  //  txtbox_parkingcharges.Text = "";
                   // txtbox_refundamount.Text = "";
                   // txtbox_rejectreason.Enabled = false;

                    //DataTable dtdeatails1 = new clsInsert().refundcustomerdetailforrejection(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
                    //if (dtdeatails1.Rows.Count > 0)
                    //{
                    //    if (Session["operatorid"].ToString() == "PM")
                    //    {
                    //        txtbox_rejectreason.Text = dtdeatails1.Rows[0]["PM_DeclineReason"].ToString();

                    //    }
                    //    if (Session["operatorid"].ToString() == "Client Manager")
                    //    {
                    //        txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                    //    }
                    //    if (Session["operatorid"].ToString() == "Data Center")
                    //    {
                    //        txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                    //    }
                    //    if (Session["operatorid"].ToString() == "Accounting")
                    //    {
                    //        txtbox_rejectreason.Text = dtdeatails1.Rows[0]["ClientManager_DeclineReason"].ToString();

                    //    }
                    //}
                }










            }
            catch
            {
                btn_send.Text = "Submit";
            }


        }

    }

    protected void rbtn_accepted_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (Session["operatorid"].ToString() == "PM")
            {
                if (rbtn_accepted.Checked == true)
                {
                    //if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) <= limit)
                    //{
                    lbl_pmsign.Visible = true;
                    PM_signature.Visible = true;
                        lbl_reason.Visible = false;
                        txtbox_rejectreason.Visible = false;
                        Panel3.Visible = true;
                        DataTable dtdeatails1 = new clsInsert().getdetailsofrefundcustomer(Convert.ToInt32(Session["refund_customerid"].ToString()));
                        if (dtdeatails1.Rows.Count > 0)
                        {
                            txtbox_customerpaid.Text = dtdeatails1.Rows[0]["TotalAmountGiven"].ToString();
                           
                        }
                    //}
                    //else
                    //{
                    //    lbl_reason.Visible = false;
                    //    txtbox_rejectreason.Visible = false;
                    //}

                }
            }



            if (Session["operatorid"].ToString() == "Client Manager")
            {
                if (rbtn_accepted.Checked == true)
                {
                    if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) > limit)
                    {
                        lbl_cmsign.Visible = true;
                        PMO_signature.Visible = true;
                        lbl_reason.Visible = false;
                        txtbox_rejectreason.Visible = false;
                        Panel3.Visible = true;
                        DataTable dtdeatails1 = new clsInsert().getdetailsofrefundcustomercm(Convert.ToInt32(Session["refund_customerid"].ToString()));
                        if (dtdeatails1.Rows.Count > 0)
                        {
                            txtbox_customerpaid.Text = dtdeatails1.Rows[0]["TotalAmountGiven"].ToString();
                            txtbox_parkingcharges.Text = dtdeatails1.Rows[0]["ApprovedParkingCharges"].ToString();
                            txtbox_refundamount.Text = dtdeatails1.Rows[0]["ApprovedRefundAmount"].ToString();
                        }
                    }
                    else
                    {
                        lbl_reason.Visible = false;
                        txtbox_rejectreason.Visible = false;
                    }

                }
            }




        }
        catch
        {
        }
    }
    protected void rbtn_rejected_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_rejected.Checked == true)
        {
            lbl_reason.Visible = true;
            txtbox_rejectreason.Visible = true;
            Panel3.Visible = false;
            txtbox_parkingcharges.Text = "";
            txtbox_refundamount.Text = "";


        }
    }
    string imagesignpm = "";
    string imagesignpmo = "";
    protected void btn_send_Click(object sender, EventArgs e)
    {
        try
        {
            if (btn_send.Text == "Submit")
            {
                statusupdate();
                Session["SuccessFull"] = "true";
                Response.Redirect("Operator_Search_RefundableByParker.aspx");

            }
            if (btn_send.Text == "Back")
            {
                Session["approval"] = null;
                Response.Redirect("Operator_Search_RefundableByParker.aspx");
            
            }

        }
        catch
        {
        }
    }
    string pmstatus = "";
   decimal limit = Convert.ToDecimal(50.00);
    decimal Default_customerpaid = Convert.ToDecimal(0.00);
    decimal Default_parkingcharges = Convert.ToDecimal(0.00);
    decimal Default_refundamount = Convert.ToDecimal(0.00);
    public void statusupdate()
    {
        hst.Clear();

        if (Session["operatorid"].ToString() == "PM")
        {
            hst.Add("action", "statusupdatebypm");
            hst.Add("refundtrackingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
            if (rbtn_accepted.Checked == true)
            {
                if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) <= limit)
                {

                    pmstatus = "True";
                    hst.Add("pmstatusfortrans", pmstatus);
                    hst.Add("pmdeclinereason", "");
                    hst.Add("pmstatus", 3);

                }
                if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) >limit)
                {

                    pmstatus = "True";
                    hst.Add("pmstatusfortrans", pmstatus);
                    hst.Add("pmdeclinereason", "");
                    hst.Add("pmstatus", 2);
                }

            }
            if (rbtn_rejected.Checked == true)
            {
                pmstatus = "False";

                hst.Add("pmstatusfortrans", pmstatus);
                hst.Add("pmdeclinereason", txtbox_rejectreason.Text);

                hst.Add("cmstatusfortrans", pmstatus);
                hst.Add("cmdeclinereason", txtbox_rejectreason.Text);

                hst.Add("accstatusfortrans", pmstatus);
                hst.Add("acceclinereason", txtbox_rejectreason.Text);

                hst.Add("datatatusfortrans", pmstatus);
                hst.Add("dataeclinereason", txtbox_rejectreason.Text);
                hst.Add("pmstatus", 4);

            }


        }





        if (Session["operatorid"].ToString() == "Client Manager")
        {
            hst.Add("action", "statusupdatebyClientManager");
            hst.Add("refundtrackingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
            if (rbtn_accepted.Checked == true)
            {
                if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) > limit)
                {
                    //update tbl_Request_StatusTransaction set
                    //    PM_Status=@pmstatusfortrans,PM_DeclineReason=@pmdeclinereason,
                    //    ClientManager_Status=@cmstatusfortrans,
                        
                    //    ClientManager_DeclineReason=@cmdeclinereason,Accounting_DeclineReason=@acceclinereason,
                    //    Datacenter_DeclineReason=@dataeclinereason,
                    //    Accounting_status=@accstatusfortrans,
                    //    DataCenter_Status=@datatatusfortrans where RequestTrackingID=@refundtrackingid
                    pmstatus = "True";
                    hst.Add("pmstatusfortrans", pmstatus);
                    hst.Add("pmdeclinereason", "");
                    hst.Add("cmstatusfortrans", pmstatus);
                    hst.Add("cmdeclinereason", "");
                    hst.Add("pmstatus", 3);

                }
               

            }
            if (rbtn_rejected.Checked == true)
            {
                pmstatus = "False";
                
                hst.Add("pmstatusfortrans", pmstatus);
                hst.Add("pmdeclinereason", txtbox_rejectreason.Text);

                hst.Add("cmstatusfortrans", pmstatus);
                hst.Add("cmdeclinereason", txtbox_rejectreason.Text);

                hst.Add("accstatusfortrans", pmstatus);
                hst.Add("acceclinereason", txtbox_rejectreason.Text);

                hst.Add("datatatusfortrans", pmstatus);
                hst.Add("dataeclinereason", txtbox_rejectreason.Text);


               
                hst.Add("pmstatus", 4);

            }
        }






       


        int result = objData.ExecuteNonQuery("[sp_RefundPayment]", CommandType.StoredProcedure, hst);
        {
            if (Session["operatorid"].ToString() == "PM")
            {
                if (rbtn_accepted.Checked == true)
                {
                //    if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) < limit)
                //    {
                        hst.Clear();

                        hst.Add("action", "insert");


                        hst.Add("refundtranckingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
                        hst.Add("refundcustomer", Convert.ToInt32(Session["refund_customerid"].ToString()));
                        hst.Add("customersign", Session["sign"].ToString());
                        if (PM_signature.HasFile)
                        {
                            string oFName = PM_signature.FileName.ToString().Trim();
                            string[] strArr = oFName.Split('.');
                            int sLength = strArr.Length;
                            if (strArr[sLength - 1] == "pdf" || strArr[sLength - 1] == "PDF")
                            {
                                imagesignpm = Session["REFUND_TRACKING_ID"].ToString() + "_" + Session["Login"].ToString() + "." + strArr[1];
                                string Path = Server.MapPath("~/PM_Signature/");
                                PM_signature.SaveAs(Path + imagesignpm);

                            }

                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload PDF only.')", true);
                                return;
                            }
                        }


                        if (PMO_signature.HasFile)
                        {
                            string oFName = PMO_signature.FileName.ToString().Trim();
                            string[] strArr = oFName.Split('.');
                            int sLength = strArr.Length;
                            if (strArr[sLength - 1] == "pdf" || strArr[sLength - 1] == "PDF")
                            {
                                imagesignpmo = Session["REFUND_TRACKING_ID"].ToString() + "_" + "PMO_sign" + "." + strArr[1];
                                string Path = Server.MapPath("~/PMO_Signature/");
                                PMO_signature.SaveAs(Path + imagesignpmo);

                            }

                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload PDF only.')", true);
                                return;
                            }
                        }
                        hst.Add("pmsign", imagesignpm);
                        hst.Add("pmosign","");
                        hst.Add("totalpaid", Convert.ToDecimal(txtbox_customerpaid.Text));

                        hst.Add("parkingcharge", Convert.ToDecimal(txtbox_parkingcharges.Text));

                        hst.Add("approvedrefund", Convert.ToDecimal(txtbox_refundamount.Text));
                        hst.Add("dateofpmsign", DateTime.Now);
                        hst.Add("dateofcustomersign", DateTime.Now);
                        hst.Add("dateofpmosign", DateTime.Now);

                        //if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) < limit)
                        //{

                           // hst.Add("ForwardLevelStatus", "False");
                        //}
                        //else
                        //{
                            hst.Add("ForwardLevelStatus", "True");

                        //}

                        int result1 = objData.ExecuteNonQuery("[sp_pmtransaction]", CommandType.StoredProcedure, hst);
                        if (result1 > 0)
                        {
                        }
                    //}

                    //else
                    //{
                    //    hst.Clear();

                    //    hst.Add("action", "insert");


                    //    hst.Add("refundtranckingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
                    //    hst.Add("refundcustomer", Convert.ToInt32(Session["refund_customerid"].ToString()));
                    //    hst.Add("customersign", Session["sign"].ToString());
                        
                    //    hst.Add("pmsign", imagesignpm);
                    //    hst.Add("pmosign", imagesignpmo);
                    //    hst.Add("totalpaid", Default_customerpaid);
                       

                    //    hst.Add("parkingcharge", Default_parkingcharges);

                    //    hst.Add("approvedrefund", Default_refundamount);
                    //    hst.Add("dateofpmsign", DateTime.Now);
                    //    hst.Add("dateofcustomersign", DateTime.Now);
                    //    hst.Add("dateofpmosign", DateTime.Now);

                    //    if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) < limit)
                    //    {

                    //        hst.Add("ForwardLevelStatus", "False");
                    //    }
                    //    else
                    //    {
                    //        hst.Add("ForwardLevelStatus", "True");

                    //    }

                    //    int result1 = objData.ExecuteNonQuery("[sp_pmtransaction]", CommandType.StoredProcedure, hst);
                    //    if (result1 > 0)
                    //    {
                    //    }
                    //}




                }  
            }




            if (Session["operatorid"].ToString() == "Client Manager")
            {
                if (rbtn_accepted.Checked == true)
                {
                    if (Convert.ToDecimal(Session["REFUND_Amount"].ToString()) > limit)
                    {
                        


                            hst.Clear();

                            hst.Add("action", "update");
                            hst.Add("refundtranckingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
                       
                        if (PM_signature.HasFile)
                        {
                            string oFName = PM_signature.FileName.ToString().Trim();
                            string[] strArr = oFName.Split('.');
                            int sLength = strArr.Length;
                            if (strArr[sLength - 1] == "pdf" || strArr[sLength - 1] == "PDF")
                            {
                                imagesignpm = Session["REFUND_TRACKING_ID"].ToString() + "_" + Session["Login"].ToString() + "." + strArr[1];
                                string Path = Server.MapPath("~/PM_Signature/");
                                PM_signature.SaveAs(Path + imagesignpm);

                            }

                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload PDF only.')", true);
                                return;
                            }
                        }


                        if (PMO_signature.HasFile)
                        {
                            string oFName = PMO_signature.FileName.ToString().Trim();
                            string[] strArr = oFName.Split('.');
                            int sLength = strArr.Length;
                            if (strArr[sLength - 1] == "pdf" || strArr[sLength - 1] == "PDF")
                            {
                                imagesignpmo = Session["REFUND_TRACKING_ID"].ToString() + "_" + "PMO_sign" + "." + strArr[1];
                                string Path = Server.MapPath("~/PMO_Signature/");
                                PMO_signature.SaveAs(Path + imagesignpmo);

                            }

                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload PDF only.')", true);
                                return;
                            }
                        }
                        DataTable getsign = new clsInsert().getpmsign(Convert.ToInt32(Session["refund_customerid"].ToString()));

                        hst.Add("pmsign", getsign.Rows[0][0].ToString());
                        hst.Add("pmosign", imagesignpmo);
                        hst.Add("totalpaid", Convert.ToDecimal(txtbox_customerpaid.Text));

                        hst.Add("parkingcharge", Convert.ToDecimal(txtbox_parkingcharges.Text));

                        hst.Add("approvedrefund", Convert.ToDecimal(txtbox_refundamount.Text));
                        hst.Add("dateofpmsign", DateTime.Now);
                        hst.Add("dateofcustomersign", DateTime.Now);
                        hst.Add("dateofpmosign", DateTime.Now);

                        int result1 = objData.ExecuteNonQuery("[sp_pmtransaction]", CommandType.StoredProcedure, hst);
                        if (result1 > 0)
                        {
                        }
                    }
                }
            }












        }
    }

    protected void txtbox_parkingcharges_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtbox_customerpaid.Text) > Convert.ToDecimal(txtbox_parkingcharges.Text))
        {

            decimal refund_amount = Convert.ToDecimal(txtbox_customerpaid.Text) - Convert.ToDecimal(txtbox_parkingcharges.Text);
            txtbox_refundamount.Text = refund_amount.ToString();
        }
        else
        {
            txtbox_parkingcharges.Text = "";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Parking Charges is Less than Customer Paid.')", true);

        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["approval"] = null;
        Response.Redirect("Operator_Search_RefundableByParker.aspx");
    }
}
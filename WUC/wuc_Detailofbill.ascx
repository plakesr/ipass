﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Detailofbill.ascx.cs" Inherits="WUC_wuc_Detailofbill" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 162px;
    }
    .style3
    {
        width: 162px;
        height: 23px;
    }
    .style4
    {
        height: 23px;
    }
    .style5
    {
        width: 243px;
    }
    .style6
    {
        width: 804px;
        text-align: right;
    }
</style>
<link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>Detail
</legend>

    <table class="style1">
        <tr>
            <td class="style3">
                Corpration Name</td>
            <td class="style4">
                <asp:TextBox ID="txtbox_name" runat="server" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Address</td>
            <td>
                <asp:TextBox ID="txtbox_address" runat="server" Enabled="False" 
                    CssClass="twitterStyleTextbox" TextMode="MultiLine" Height="43px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Lot Name</td>
            <td>
                <asp:TextBox ID="txtbox_lotname" runat="server" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                User Name</td>
            <td>
                <asp:TextBox ID="txtbox_username" runat="server" Enabled="False" CssClass="twitterStyleTextbox"   ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Contact Person Name</td>
            <td>
                <asp:TextBox ID="txtbox_Contactperson" runat="server" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

</fieldset>
</asp:Panel>
<br />
<asp:Panel ID="Panel2" runat="server">
<fieldset>
<asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand">
    <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>

                <th>
                     Invoice Number
                    </th>
                      <th>
                       Date Issued
                    </th>
                    
                    <th>
                       Due Date
                    </th>
                <th>
                     Sub Total
                    </th>
                      <th>
                       Tax
                    </th>
                    
                    <th>
                       Invoiced Amount
                    </th>
                    <th>
                       Outstanding Amount
                    </th>
                  
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
             <td>
                     <%#Eval("InvoiceNumber")%> 
                    
                </td>
                <td>     <%#DateTime.Parse(Eval("Issued Date").ToString()).ToString("d")%> </td> 
              
                  <td>
                             <%#DateTime.Parse(Eval("Due Date").ToString()).ToString("d")%> 
               
                   
                </td>
                 <td>
                     $ <%#Eval("Sub-Total")%> 
                    
                </td>
                 <td>
                     $ <%#Eval("Tax")%> 
                    
                </td>
            
                
                <td>
                     $ <%#Eval("Invoiced Amount")%> 
                    
                </td>
                 <td>
                     $ <%#Eval("Amount Outstanding")%> 
                    
                </td>
               
            </tr>
         
  
       </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>

</fieldset>
</asp:Panel>
<table class="style1">
    <tr>
        <td class="style6">
            Total Due Amount</td>
        <td>
            
                <asp:TextBox ID="txtbox_totaldue" runat="server" 
                CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
            
        </td>
    </tr>
</table>
<table class="style1">
    <tr>
        <td class="style5">
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_ok" runat="server" Height="37px" Text="Back" Width="105px"  
                CssClass="button" onclick="btn_ok_Click"/>
        &nbsp;
            <asp:Button ID="btn_sendmail" runat="server" Text="Send Mail" CssClass="button" 
                onclick="btn_sendmail_Click" />
&nbsp;&nbsp;
            <asp:Button ID="btn_invoice" runat="server" Text="Invoce Generate" 
                CssClass="button" onclick="btn_invoice_Click" />
        &nbsp;
        </td>
    </tr>
</table>

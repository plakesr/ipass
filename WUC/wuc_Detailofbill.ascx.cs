﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WUC_wuc_Detailofbill : System.Web.UI.UserControl
{
    string u_name = "";
    string address, name, lotname, plan, totalamount = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["detailofdue"] != null)
            {




                DataSet_Bill ds1 = (DataSet_Bill)Session["detailofdue"];
                txtbox_address.Text = ds1.Tables["Table_bill_Corporate"].Rows[0][2].ToString();
                txtbox_lotname.Text = ds1.Tables["Table_bill_Corporate"].Rows[0][4].ToString();
                txtbox_name.Text = ds1.Tables["Table_bill_Corporate"].Rows[0][0].ToString();
                txtbox_username.Text = ds1.Tables["Table_bill_Corporate"].Rows[0][3].ToString();
                txtbox_Contactperson.Text = ds1.Tables["Table_bill_Corporate"].Rows[0][5].ToString();
                txtbox_totaldue.Text = "$" + ds1.Tables["Table_bill_Corporate"].Rows[0][1].ToString();
                u_name = ds1.Tables["Table_bill_Corporate"].Rows[0][3].ToString();
                Label1.Text = u_name;
                Repeater1.DataSource = ds1.Tables["BillInformation"];
                Repeater1.DataBind();
            }
        }
        catch
        {
        }
    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {

    }
    protected void btn_sendmail_Click(object sender, EventArgs e)
    {

    }
    protected void btn_invoice_Click(object sender, EventArgs e)
    {

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
}
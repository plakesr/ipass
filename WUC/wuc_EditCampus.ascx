﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_EditCampus.ascx.cs" Inherits="WUC_wuc_EditCampus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style2
    {
        width: 121px;
    }
    .style3
    {
        width: 382px;
    }
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<table class="form-table">
    <tr>
        <td class="style2">
            Select Customer:</td>
        <td class="style3">
           
            
            <asp:DropDownList ID="ddl_site" runat="server" ValidationGroup="a" 
                DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId" CssClass="twitterStyleTextbox">
                <asp:ListItem Value="1">Site1</asp:ListItem>
                        <asp:ListItem Value="2">Site2</asp:ListItem>
                        <asp:ListItem Value="3">Site3</asp:ListItem>
                        <asp:ListItem Value="4">Site4</asp:ListItem>
            </asp:DropDownList>
             <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
            </asp:SqlDataSource>
             <asp:RequiredFieldValidator ID="rfv_site" runat="server" 
                ControlToValidate="ddl_site" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
        <td class="style3" rowspan="3">
           
            
             <div id="map_canvas" style="width:300px; height:300px"></td>
    </tr>
    <tr>
        <td class="style2">
            Campus Name :</td>
        <td class="style3">
            <asp:TextBox ID="txtbox_campusname" runat="server" ValidationGroup="a" 
                CssClass="twitterStyleTextbox"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_campusname" runat="server" 
                ControlToValidate="txtbox_campusname" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Campus Details </td>
        <td class="style3">
            <cc2:Editor ID="Editor1" runat="server" />
         
            
        </td>
        
    </tr>
    <tr>
        <td class="style2">
            Address</td>
        <td class="style3">
            <asp:TextBox ID="address" runat="server" Width="246px" CssClass="twitterStyleTextbox" TextMode="MultiLine" style="resize:none"></asp:TextBox></td>
        <td class="style3">
            <label for="latitude">
        Latitude:</label>
   <asp:TextBox ID="latitude" runat="server" Width="79px" Enabled="false" CssClass="twitterStyleTextbox"></asp:TextBox>
   <%-- <input id="txtLat" type="text" value="28.47399" />--%>
    <label for="longitude">
        Longitude:</label>
   <asp:TextBox ID="longitude" runat="server" Width="96px" Enabled="false" CssClass="twitterStyleTextbox"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <caption>
            &nbsp;</td>
            <td class="style3">
                <asp:Button ID="btn_update" runat="server" CssClass="button" Height="35px" 
                    onclick="btn_submit_Click" Text="Update" ValidationGroup="a" Width="96px" />
            </td>
            <td class="style3">
                &nbsp;</td>
        </caption>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>



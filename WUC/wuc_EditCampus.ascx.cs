﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditCampus : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                txtbox_campusname.Text = Session["campusname"].ToString();
                Editor1.Content = Session["campusdetail"].ToString();
                ddl_site.SelectedValue = Session["SiteName"].ToString();
                address.Text = Session["Campus_address"].ToString();
                latitude.Text = Session["Campus_latitude"].ToString();

                longitude.Text = Session["Campus_longitude"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_CampusMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_CampusMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
           
            updatecampus();
          

        }
        catch
        {
        }

    }

   
    string s = "true";
    string msg;
    public void updatecampus()
    {
        try
        {
            // hst.Clear();

            hst.Add("action", "update");
            hst.Add("campusid", Convert.ToInt32(Session["id"].ToString()));

            hst.Add("campusname", txtbox_campusname.Text);
            hst.Add("campusdetail", Editor1.Content.ToString());
            hst.Add("siteid", ddl_site.SelectedValue);
            hst.Add("modifiedby", "campus");
            hst.Add("operator", "lovey_operator");
            hst.Add("dateofchanging", DateTime.Now);
            hst.Add("status", s);
            hst.Add("address", address.Text);
            hst.Add("latitude", latitude.Text);
            hst.Add("longitude", longitude.Text);
            int result = objData.ExecuteNonQuery("[sp_Campus]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CampusMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CampusMaster.aspx?msg=" + msg);

                }

              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

              

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

            

        }

    }

}
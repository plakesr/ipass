﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_EditCardDeliveryMaster.ascx.cs" Inherits="WUC_wuc_EditCardDeliveryMaster" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 219px;
    }
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

</style>
<div class="form">
<label>Customer Name</label>
  <asp:TextBox ID="txtbox_sitename" runat="server" ReadOnly="True" CssClass="twitterStyleTextbox"></asp:TextBox>
  <div class="clearfix"></div>
  <label>Card Delivery Method Name</label>
  <asp:TextBox ID="txtbox_deliveryname" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
  <div class="clearfix"></div>
  <label>Culture Type</label>
  <div class="radio">
   <asp:RadioButton ID="rbtn_English" runat="server" GroupName="a" 
                     Text="English" />
                 <asp:RadioButton ID="rbtn_French" runat="server" GroupName="a" Text="French" />
                 </div>
                 <div class="clearfix"></div>
                 <label>Delivery Type</label>
                 <asp:DropDownList ID="ddl_deliverytype" runat="server" 
                     CssClass="twitterStyleTextbox">
                     <asp:ListItem Value="1">Pick up at CSC</asp:ListItem>
                     <asp:ListItem Value="2">To be delivered</asp:ListItem>
                 </asp:DropDownList>
                 <div class="clearfix"></div>
<label>Charges</label>
 <asp:TextBox ID="txtbox_charges" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
 <div class="clearfix"></div>
 <label>&nbsp;</label>
  <asp:Button ID="btn_update" runat="server" onclick="btn_add_Click" 
                    Text="Update" CssClass=button />
    

</div>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditCardDeliveryMaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack == true)
        {
            try
            {
                txtbox_deliveryname.Text = Session["CardDeliveryName"].ToString();
               txtbox_sitename.Text= Session["SiteName"].ToString();
               ddl_deliverytype.SelectedValue = Session["CardDeliveryType"].ToString();
                txtbox_charges.Text = Session["Charges"].ToString();
                string s = Session["CultureType"].ToString();
                if (Session["CultureType"].ToString()=="1")
                {
                    rbtn_English.Checked = true;
                }
               
                else
                {
                    rbtn_French.Checked = true;
                }
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_CardDeliveryMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_CardDeliveryMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx" );
                }
            }
        }


      
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        //string msg = "1";
        updatecarddelivery();
       // Response.Redirect("Admin_CardDeliveryMaster.aspx?msg=" + msg);

       // Response.Redirect("Admin_CardDeliveryMaster.aspx");
    }
    string s = "true";

     int culture;
     string msg;
    public void updatecarddelivery()
    {
        try
        {

            hst.Clear();
            int i;
            i = Convert.ToInt32(Session["CardDeliveryId"].ToString());

            hst.Add("action", "update");
            hst.Add("id", i);
           hst.Add("CardDeliveryName",txtbox_deliveryname.Text);
            if(rbtn_English.Checked==true)
            {
                culture = 1;
                 hst.Add("CultureType", culture);
            }
            if (rbtn_French.Checked == true)
            {
                culture = 2;
                hst.Add("CultureType", culture);
            }
            hst.Add("Charges", txtbox_charges.Text);
            hst.Add("ModifiedBy", "card delivery");
            hst.Add("CardDeliveryType", ddl_deliverytype.SelectedItem.Value);
            hst.Add("Address", "h-392");
            hst.Add("oprator", s);
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_CardDeliveryMethod]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CardDeliveryMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CardDeliveryMaster.aspx?msg=" + msg);

                }


            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

            }
           

        }
        catch
        {

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

        }

    }

}
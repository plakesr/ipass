﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditCategory : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
             txtbox_Catname.Text= Session["categoryname"].ToString();
             txtbox_catdescription.Text= Session["categorydesc"].ToString();

                //Session["Id"] = l3.Text;
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_CategoryMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_CategoryMaster.aspx.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
    string msg;
    public void UpdateCategory()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id", Convert.ToInt32(Session["Id"].ToString()));

            hst.Add("Categoryname", txtbox_Catname.Text);
            hst.Add("CategoryDesc", txtbox_catdescription.Text);

            hst.Add("operator", "lovey_operator");
            hst.Add("modified", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[Sp_Category]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CategoryMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CategoryMaster.aspx?msg=" + msg);

                }
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Upadated succesfully.')", true);

               // Response.Redirect("Admin_CategoryMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
           // string msg = "1";
            UpdateCategory();
//Response.Redirect("Admin_CategoryMaster.aspx?msg=" + msg);

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);

        }
    }
}
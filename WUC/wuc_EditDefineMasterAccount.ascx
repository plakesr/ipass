﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_EditDefineMasterAccount.ascx.cs" Inherits="WUC_wuc_EditDefineMasterAccount" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style3
    {
        width: 167px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Define Master Account</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="style3">
                Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_editname" runat="server" 
                     ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_editname" runat="server" 
                    ControlToValidate="txtbox_editname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_editdescription" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="a" 
                    Width="55px" onclick="btn_update_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
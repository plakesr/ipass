﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditDefineMasterAccount : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
              
            //Session["id"] = id.Text;
           
            txtbox_editdescription.Text= Session["Masterdesc"].ToString();
            txtbox_editname.Text=Session["MasterName"].ToString();
        }

    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = "1";
            updateMasterAccount();
            Response.Redirect("MasterAccount.aspx?msg=" + msg);

        }
        catch
        {
        }
    }

    public void updateMasterAccount()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id", Convert.ToInt32(Session["id"].ToString()));

            hst.Add("masteraccountname", txtbox_editname.Text);
            hst.Add("masteraccountdesc", txtbox_editdescription.Text);

            hst.Add("opreator", "lovey_operator");
            hst.Add("modified", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_MasterAccount]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                Response.Redirect("MasterAccount.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_EditLotMaster.ascx.cs" Inherits="WUC_wuc_EditLotMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
 <link href="style/main.css" rel="stylesheet" type="text/css" />
    <link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<style>
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style1
    {
        width: 341px;
    }
    .style2
    {
        width: 360px;
    }
    .style4
    {
        width: 240px;
    }
    .style5
    {
        width: 248px;
    }
    .style6
    {
        width: 295px;
    }
    .form-table
    {
        width: 872px;
    }
    .style12
    {
        width: 159px;
    }
    .style14
    {
        width: 283px;
    }
    .style23
    {
        width: 321px;
    }
    .style28
    {
        width: 485px;
    }
    .style29
    {
        width: 232px;
    }
    .style32
    {
        width: 611px;
    }
    .style35
    {
        width: 418px;
    }
    .style36
    {
        width: 176px;
    }
    .registration-area .Clicked{ text-decoration: none;
   border-radius:5px 5px 0 0;

  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; padding:8px 20px 7px;}
  
  .registration-area .Initial{text-decoration: none;
   border-radius:5px 5px 0 0;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #F9F9F9;
  color:#707070; padding:8px 20px; margin:0px;}
    .style37
    {
        width: 511px;
    }
    .map-area
    {
        width: 428px;
    }
</style>

<div class="registration-area">

<asp:UpdatePanel ID="UpdatePanel3" runat="server">
<ContentTemplate>

    <asp:Label ID="Label14" runat="server" style="text-align:right; float:right" Text="* Represents Mandatory Fields" ForeColor="Red" ></asp:Label>
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="3" >
    <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"  CssClass="Clicked"><HeaderTemplate>General Details</HeaderTemplate><ContentTemplate>
    
        <asp:Panel ID="Panel8" runat="server">
        <fieldset>
    <table style="width: 100%; height: 200px;"><tr><td><span><table class="form-table"><tr><td class="style2">Customer Name</td><td><asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:TextBox ID="txtbox_customername" runat="server" Enabled="False"></asp:TextBox>
        </ContentTemplate>
        </asp:UpdatePanel></td></tr><tr><td class="style2">Campus Name</td><td><asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:TextBox ID="txtbox_campusname" runat="server" Enabled="False"></asp:TextBox>
            </ContentTemplate>
            </asp:UpdatePanel></td></tr><tr><td class="style2">Lot&nbsp;&nbsp; Name<asp:Label ID="Label8" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td>
            <asp:TextBox ID="txtbox_lotname" runat="server" 
                                              ontextchanged="txtbox_name_TextChanged" 
                ValidationGroup="m" CssClass="twitterStyleTextbox" AutoPostBack="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_LotName" runat="server" 
                                              ControlToValidate="txtbox_lotname" 
                ErrorMessage="Enter Lot Name" ForeColor="#CC0000" Display="Dynamic" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
            <asp:Label ID="lbl_exist" runat="server" Visible="False"></asp:Label>
            </td></tr><tr><td class="style2">Lot&nbsp;&nbsp; Code<asp:Label ID="Label9" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td><asp:TextBox ID="txtbox_lotcode" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox><span>
            <asp:RequiredFieldValidator ID="rfv_Lotcode" runat="server" 
                                              ControlToValidate="txtbox_lotcode" 
                ErrorMessage="Enter Lot Code" ForeColor="#CC0000" Display="Dynamic" 
                ValidationGroup="a"></asp:RequiredFieldValidator></span></td></tr><tr><td class="style2">Effective Date<asp:Label ID="Label10" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td><asp:TextBox ID="txtbox_date" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox><cc1:CalendarExtender ID="txtbox_date_CalendarExtender" runat="server" 
                                              Enabled="True" TargetControlID="txtbox_date"></cc1:CalendarExtender><span>
            <asp:RequiredFieldValidator ID="rfv_effectivedate" runat="server" 
                                              ControlToValidate="txtbox_date" 
                ErrorMessage="Enter Effective Date" ForeColor="#CC0000" Display="Dynamic" 
                ValidationGroup="a"></asp:RequiredFieldValidator></span></td></tr><tr><td class="style2">Time Zone</td><td>
            <asp:DropDownList ID="ddl_timezone" runat="server" 
                CssClass="twitterStyleTextbox" 
                onselectedindexchanged="ddl_timezone_SelectedIndexChanged" ></asp:DropDownList></td></tr><tr><td class="style2">Lot Hours<asp:Label ID="Label11" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td>
            <asp:TextBox ID="txtbox_hours" runat="server" ValidationGroup="m" 
                CssClass="twitterStyleTextbox" ontextchanged="txtbox_hours_TextChanged"></asp:TextBox>
       
        <span><asp:RequiredFieldValidator ID="rfv_lothours" runat="server" 
                                              ControlToValidate="txtbox_hours" 
            ErrorMessage="Enter Lot Hours" ForeColor="#CC0000" Display="Dynamic" 
            ValidationGroup="a"></asp:RequiredFieldValidator></span></td></tr><tr><td></td><td> 
            <asp:Button 
                                  ID="btn1_next" runat="server" CssClass="button" Height="35px" 
                                  onclick="btn1_next_Click" Text="Next" Width="79px" /></td></tr>
        <caption>
            </caption>
        </table><br /><br /><br /><br /><br /><br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        </span></td></tr></table>
        </fieldset>
        </asp:Panel>
        </ContentTemplate></cc1:TabPanel>
   
    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2"  CssClass="Initial" ><HeaderTemplate>Contact Details</HeaderTemplate><ContentTemplate><asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
    
        <asp:Panel ID="Panel7" runat="server">
        <fieldset>
    <table ><tr><td class="style2"><br /><br />
       
        <table class="form-table"><tr>
        <td class="style12">City</td><td class="style1"><asp:DropDownList ID="ddl_city" runat="server" DataSourceID="SqlDataSource2" 
                        DataTextField="City_name" DataValueField="City_id" CssClass="twitterStyleTextbox"></asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        
            SelectCommand="SELECT [City_id], [City_name] FROM [tbl_City] ORDER BY [City_name]"></asp:SqlDataSource></td></tr><tr>
            <td class="style12">Postal Code/Zip Code<asp:Label ID="Label4" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_zip" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox><br />
                    <br />
                    <br />
                    <br />
            <asp:RequiredFieldValidator ID="rfv_postalcode" runat="server" 
                ControlToValidate="txtbox_zip" ErrorMessage="Enter Postal Code" 
                ForeColor="#CC0000" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_zip" 
                 
                ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                ForeColor="#CC0000" ValidationGroup="b" ></asp:RegularExpressionValidator>
            </td></tr><tr>
        <td class="style12">Cellular<asp:Label ID="Label5" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_cell" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox><br />
                    <br />
                    <br />
                    <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ErrorMessage="Enter Cell number" ControlToValidate="txtbox_cell" 
                ValidationGroup="a" ForeColor="#CC0000" Display="Dynamic"></asp:RequiredFieldValidator></td></tr><tr>
        <td class="style12">Office No<asp:Label ID="Label6" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_office" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
        
        <asp:RegularExpressionValidator
                                                           
                ID="RegularExpressionValidator1" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_office" ValidationGroup="b" 
                ForeColor="#CC0000"  ></asp:RegularExpressionValidator><br />
                    <br />
                    <br />
                    <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ErrorMessage="Enter Office Number" ControlToValidate="txtbox_office" 
                ValidationGroup="a" ForeColor="#CC0000" Display="Dynamic"></asp:RequiredFieldValidator></td></tr><tr>
        <td class="style12">E-mail<asp:Label ID="Label7" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1">
                <asp:TextBox ID="txtbox_email" runat="server" CssClass="twitterStyleTextbox" 
                    ontextchanged="txtbox_email_TextChanged"></asp:TextBox><br />
                    <br />
                    <br />
                    <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ErrorMessage="Enter Email" ControlToValidate="txtbox_email" ValidationGroup="a" 
                ForeColor="#CC0000" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="rev_email" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="Format is wrong" 
                        ForeColor="#CC0000"   
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                ValidationGroup="b" ></asp:RegularExpressionValidator></td><td>&#160;<br /> &nbsp;&nbsp;<br /><br />&nbsp;&nbsp;<br />&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /> <br />
                    <br />
                    &nbsp;<br /><br />&nbsp;</td></tr><tr><td>
        Address</td>
        <td>
            <asp:TextBox ID="txtbox_Adress1" runat="server" CssClass="twitterStyleTextbox" 
                ontextchanged="txtbox_Adress1_TextChanged" Style="resize:none" 
                TextMode="MultiLine" ValidationGroup="a" Width="337px"></asp:TextBox>
        </td>
                </tr></table>
                
                <div class="map-area">
 <div id="map_canvas" style="width:352px; height:300px"></div>
<label for="latitude">
                    Latitude</label>
  <asp:TextBox ID="latitude" runat="server" Enabled="False"></asp:TextBox>

    
    <label for="longitude">
                    Longitude </label>
 <asp:TextBox ID="longitude" runat="server" Enabled="False"></asp:TextBox>
        </div>
                
                <table>
                <tr>
              <td class="style37"></td>
                <td class="style32">
        <asp:Button 
            ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" 
            CssClass="button" Height="35px" Width="79px" ValidationGroup="b"></asp:Button></td></tr>
                </table>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
       </fieldset>
        </asp:Panel> 
        </ContentTemplate></asp:UpdatePanel></ContentTemplate></cc1:TabPanel>
    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3"  CssClass="Initial"><HeaderTemplate>Web Direction</HeaderTemplate><ContentTemplate><table class="form-table"><tr><td><h3>&nbsp;<asp:Panel ID="Panel4" runat="server" Height="227px" Width="705px"><fieldset><legend>Web Direction English</legend><table class="form-table"><tr><td class="style4">Entrance Description</td><td class="style5"><asp:TextBox ID="txtbox_enteranceinformationEnglish" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" Width="217px" 
                                                TextMode="MultiLine" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Exit Description</td><td class="style5"><asp:TextBox ID="txtbox_exitinformationEnglish" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Lot Restriction</td><td class="style5">
            <asp:TextBox ID="txtbox_restriction" runat="server" 
                                                CssClass="twitterStyleTextbox" 
                ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none" 
                ontextchanged="txtbox_restriction_TextChanged"></asp:TextBox></td></tr><tr><td>Lot Description</td><td class="style5"><asp:TextBox ID="txtbox_Description" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="221px" style="resize:none"></asp:TextBox></td></tr></table></fieldset></asp:Panel><br />
        <br />
      <br />
      <br />
   
        <asp:Panel ID="Panel5" runat="server"><fieldset><legend>Web Direction French</legend><table class="form-table"><tr><td class="style4">Entrance Description</td><td class="style5"><asp:TextBox ID="txtbox_enteranceinformationFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" Width="217px" 
                                                TextMode="MultiLine" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Exit Description</td><td class="style5"><asp:TextBox ID="txtbox_exitinformationFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Lot Restriction</td><td class="style5"><asp:TextBox ID="txtbox_restrictionFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" 
                                                ontextchanged="txtbox_restrictionFrench_TextChanged" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td>Lot Description</td><td class="style5"><asp:TextBox ID="txtbox_DescriptionFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr></table></fieldset> </asp:Panel>
                      
                      
                      
                      
                  </td></tr></table><br /><br />
        <asp:Panel ID="Panel6" runat="server">
        <fieldset>
                  
                  <table style="width: 881px"><tr>
            <td class="style23">Lot Type </td><td class="style6"><asp:DropDownList ID="ddl_lottype" runat="server" CssClass="twitterStyleTextbox" Width="239px"><asp:ListItem Value="1">Key Scan</asp:ListItem><asp:ListItem Value="2">HangTags</asp:ListItem></asp:DropDownList></td></tr><tr>
            <td class="style23">Other Details </td><td class="style6"><asp:TextBox ID="txtbox_otherdetailsweb" runat="server" Height="42px" CssClass="twitterStyleTextbox" 
                                    TextMode="MultiLine" Width="229px" style="resize:none"></asp:TextBox></td></tr></table>
                </fieldset>
        </asp:Panel><br /><br />
        <table>
        <tr>
                <td class="style23">
                <asp:Button ID="next3" runat="server" CssClass="button" Height="35px" 
                    OnClick="next3_Click" Text="Next" Width="79px" />
                </td><td>&nbsp;</td></tr>
        </table>
        <br /><br /><br /><br /><br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
       </ContentTemplate></cc1:TabPanel>
    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="TabPanel4"  CssClass="Initial"><HeaderTemplate>Billing Entry</HeaderTemplate>
    <ContentTemplate>
    
    <asp:Panel ID="Panel1" runat="server" >
    <fieldset>
    <table>
      <tr>
            <td class="style28">Billing Entry</td><td class="style14"><asp:DropDownList ID="ddl_billingEntry" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="1">First Month Full</asp:ListItem><asp:ListItem Value="2">Prorate</asp:ListItem><asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem></asp:DropDownList></td>
      </tr> 
       <tr>
        <td class="style28">Billing Exit</td><td class="style14"><asp:DropDownList ID="ddl_billingexit" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="1">First Month Full</asp:ListItem><asp:ListItem Value="2">Prorate</asp:ListItem><asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem></asp:DropDownList></td>
        </tr>
      <tr>
            <td class="style28">Payment Reminder Days<asp:Label ID="Label1" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label>
            </td><td class="style14"><asp:TextBox ID="txtbox_paymentreminderdays" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtbox_paymentreminderdays_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_paymentreminderdays" 
                FilterType="Numbers" >
            </cc1:FilteredTextBoxExtender><br />
                <br />
                <br />
                <br />
            <asp:RequiredFieldValidator ID="rfv_reminderdays" runat="server" 
                ControlToValidate="txtbox_paymentreminderdays" 
                ErrorMessage="Enter Payment Reminder Days" ForeColor="#CC0000" 
                Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
      </tr>
      <tr>
            <td class="style28">Interest Rate Payment<asp:Label ID="Label2" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style14"><asp:TextBox ID="txtbox_interestrate" runat="server"  CssClass="twitterStyleTextbox"  onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_interestrate_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_interestrate"
  ValidChars="01234567890." >
                </cc1:FilteredTextBoxExtender>
          <br />  
                <br />
                <br />
                <br />
                <asp:RequiredFieldValidator ID="rfv_ratepayment" runat="server" 
                ErrorMessage="Enter Interest Rate Payment" ForeColor="#CC0000" 
                ValidationGroup="a" Display="Dynamic" 
                ControlToValidate="txtbox_interestrate"></asp:RequiredFieldValidator>
            </td>
      </tr>
      <tr>
            <td class="style28">Late Fee($)<asp:Label ID="Label3" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style14"><asp:TextBox ID="txtbox_latefee" runat="server"   CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_latefee_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_latefee"
  ValidChars="01234567890." >
                </cc1:FilteredTextBoxExtender>
           <br />
                <br />
                <br />
                <br />
                <asp:RequiredFieldValidator ID="rfv_latefee" runat="server" 
                ControlToValidate="txtbox_latefee" ErrorMessage="Enter Late Fee" 
                ForeColor="#CC0000" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
      </tr>
      <tr>
      <td>
          Activation Charge</td>
      <td>
          <asp:TextBox ID="txtbox_activationcharge" runat="server"></asp:TextBox>
          </td>
      </tr>
      <tr>
            <td class="style28">Display Activation Charge</td><td class="style14">
            <asp:RadioButton ID="rbtn_yes" runat="server" GroupName="r"  
                oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rbtn_no" runat="server" GroupName="r" Text="No" 
                oncheckedchanged="rbtn_no_CheckedChanged" /></td>
      </tr>
      <tr>
            <td class="style28">Display Deposit</td><td class="style14">
            <asp:RadioButton ID="rbtn_yes_deposite" runat="server" GroupName="r1" 
                oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rbtn_no_deposite" runat="server" GroupName="r1" Text="No" 
                oncheckedchanged="rbtn_no_deposite_CheckedChanged" /></td>
      </tr>
      <tr>
            <td class="style28">Late Payment Action</td><td class="style14"><asp:DropDownList ID="ddl_latepaymentaction" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="0">Disable Card</asp:ListItem><asp:ListItem Value="1">Send Email by Notification</asp:ListItem></asp:DropDownList></td>
      </tr>
      <tr>
        <td class="style28">Credit Card Payment Failed</td><td class="style14"><asp:DropDownList ID="ddl_creditcardpayment" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="0">Disable Card</asp:ListItem><asp:ListItem Value="1">Send Email by Notification</asp:ListItem></asp:DropDownList></td>
      </tr>
      <tr>
        <td class="style28">Transaction Type </td><td><asp:CheckBox ID="chk_monthly" 
              runat="server" Text="Monthly" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox ID="chk_daily" runat="server" Text="Daily" /></td> 
      </tr>

    </table> 
    </fieldset>
    </asp:Panel>
    <br />
        <br />
      
    <asp:Panel ID="Panel2" runat="server" >
  <fieldset>
  <legend>Reserved</legend>
<table>
<tr>
   
   
<td>
 <asp:Panel ID="Panel14" runat="server">
    <fieldset>
    <asp:Panel ID="Panel10" runat="server">
   <fieldset><legend>Space Available</legend>
   <table>
   <tr>
<td>Total Space
</td>
<td>
 <asp:TextBox ID="txtbox_reservedtotalspace" 
                    runat="server" Height="20px"  Width="91px" CssClass="twitterStyleTextbox" 
                    Text="0" ontextchanged="txtbox_reservedtotalspace_TextChanged"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_reservedtotalspace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_reservedtotalspace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
</td>
<td>
Unable Space
</td>
<td>
<asp:TextBox ID="txtbox_reservedunablespace" 
                    runat="server" Height="19px" Width="83px" CssClass="twitterStyleTextbox" 
                    Text="0" ontextchanged="txtbox_reservedunablespace_TextChanged"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_reservedunablespace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_reservedunablespace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
</td>
</tr>
   </table>
   </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel11" runat="server">
    <fieldset><legend>Add More</legend>
    <table>
    <tr>
<td>
    Amount</td>
<td>
    <asp:TextBox ID="amount" runat="server" CssClass="twitterStyleTextbox" Width="60px"  Height="20px"></asp:TextBox>
</td>
<td>Quantity</td>
<td>
    <asp:TextBox ID="Quantity" runat="server" Height="20px" CssClass="twitterStyleTextbox" 
        ontextchanged="Quantity_TextChanged" Width="60px"></asp:TextBox>
        <td>
     <asp:DropDownList ID="ddl_reserved" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="chargebyname" 
                    DataValueField="chargebyid"  CssClass="twitterStyleTextbox">
                </asp:DropDownList>
    </td>
    </td>
    <td>
        <asp:Button ID="Addreserved" runat="server" Text="Add" 
            CssClass="twitterStyleTextbox" onclick="Addreserved_Click" />
    </td>
</tr>
    </table>

    </fieldset>
    </asp:Panel>
    </fieldset>
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
         ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
         SelectCommand="SELECT * FROM [tbl_Chargeby]"></asp:SqlDataSource>
 </asp:Panel>
</td>

<td>
    <asp:Panel ID="Panel15" runat="server">
    <fieldset>
 <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <asp:ListView ID="ListView1" runat="server" OnPagePropertiesChanging="ListView1_PagePropertiesChanging"
                OnItemEditing="ListView1_ItemEditing" OnItemCanceling="ListView1_ItemCanceling"
                OnItemUpdating="ListView1_ItemUpdating"              
                onitemdeleting="ListView1_ItemDeleting" 
                onselectedindexchanged="ListView1_SelectedIndexChanged">
                <EmptyDataTemplate>
                    <p>No Records Found..</p>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table id="Table1" runat="server"  width="400px" style="background-color: darkgrey;">
                        <tr id="Tr1" runat="server" style="background-color: silver;">
                            <td>
                            </td>
                            <td id="Td2" runat="server">
                            <b>Amount</b>
                            </td>
                            <td id="Td1" runat="server">
                                <b>Quantity</b>
                            </td>
                            <td id="Td5" runat="server">
                                <b>Chargeby</b>
                            </td>
                           
                        </tr>
                        <tr id="ItemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                             <asp:Button ID="Button1" Text="Delete" runat="server" CommandName="Delete" />
                        </td>
                     
                        <td>
                        <asp:Label ID="lblDelPkId" runat="server" Text='<%# Eval("LotRateid") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lable2" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lable3" runat="server" Text='<%# Eval("Quantity") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lable4" runat="server" Text='<%# Eval("chargeby") %>'></asp:Label>
                        </td>
                     
                    </tr>
                </ItemTemplate>
                <EditItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />
                        </td>
                       
                        <td>
                        <asp:Label ID="lblPkId" runat="server" Text='<%# Eval("LotRateid") %>' Visible="false"></asp:Label>
                            <asp:TextBox ID="txtpdtId" Width="50px" runat="server" Text='<%# Eval("Amount") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPdtName" Width="50px" runat="server" Text='<%# Eval("Quantity") %>'></asp:TextBox>
                        </td>
                        <td>
                              <asp:DropDownList ID="ddl_reserved" runat="server" ValidationGroup="a"  CssClass="twitterStyleTextbox">

                     <asp:ListItem Value="1">Hourly</asp:ListItem>
                        <asp:ListItem Value="2">Daily</asp:ListItem>
                        <asp:ListItem Value="3">Monthly</asp:ListItem>

                        <asp:ListItem Value="4">Weekly</asp:ListItem>
                </asp:DropDownList>


               <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Chargeby]"></asp:SqlDataSource>--%>
                            
                        </td>
                    
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:DataPager ID="lvDataPager1" runat="server" PagedControlID="ListView1" PageSize="5">
        <Fields>
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
    </fieldset>
    </asp:Panel>
</td>
</tr>
</table>
     

         </asp:Panel> 
         <br />


      <asp:Panel ID="Panel9" runat="server">
 <fieldset><legend>Random</legend>

<table>
<tr>
<td>
    <asp:Panel ID="Panel16" runat="server">
    <fieldset>
    <asp:Panel ID="Panel12" runat="server">
    <fieldset><legend>Space Available</legend>
    
    <table>
    <tr>
<td>Total Space
</td>
<td>
<asp:TextBox ID="txtbox_randomtotalspace" runat="server" ontextchanged="TextBox2_TextChanged" Height="21px" Width="89px" CssClass="twitterStyleTextbox" Text="0"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_randomtotalspace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_randomtotalspace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
</td>
<td>
Unable Space
</td>
<td>
<asp:TextBox ID="txtbox_randomunablespace" 
                    runat="server" Height="20px"  Width="88px" CssClass="twitterStyleTextbox" 
                    Text="0" ontextchanged="txtbox_randomunablespace_TextChanged"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_randomunablespace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_randomunablespace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
</td>
</tr>
    </table>
    
    </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel13" runat="server">
    <fieldset><legend>Add More</legend>
    
    <table>
    <tr>
<td>
    Amount</td>
<td>
    <asp:TextBox ID="amountrandom" runat="server" CssClass="twitterStyleTextbox" Width="60px" Height="20px"></asp:TextBox>
</td>
<td>Quantity</td>
<td>
    <asp:TextBox ID="quantityrandom" runat="server" CssClass="twitterStyleTextbox" Width="60px" Height="20px"></asp:TextBox>
    <td>
    <asp:DropDownList ID="ddlrandom" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="chargebyname" 
                    DataValueField="chargebyid"  CssClass="twitterStyleTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Chargeby]"></asp:SqlDataSource>
    </td>
     
    
    </td>
     <td>
        <asp:Button ID="addrandom" runat="server" Text="Add" 
             CssClass="twitterStyleTextbox" onclick="addrandom_Click" />
    </td>
</tr>
    </table>
    </fieldset>
    </asp:Panel>
    </fieldset>
    </asp:Panel>
</td>
<td>
    <asp:Panel ID="Panel17" runat="server">
    <fieldset>
 <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:ListView ID="ListView2" runat="server" OnPagePropertiesChanging="ListView2_PagePropertiesChanging"
                OnItemEditing="ListView2_ItemEditing" OnItemCanceling="ListView2_ItemCanceling"
                OnItemUpdating="ListView2_ItemUpdating"              
                onitemdeleting="ListView2_ItemDeleting" 
                onselectedindexchanged="ListView2_SelectedIndexChanged">
                <EmptyDataTemplate>
                    <p>No Records Found..</p>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table id="Table1" runat="server"  width="400px" style="background-color: darkgrey;">
                        <tr id="Tr1" runat="server" style="background-color: silver;">
                            <td>
                            </td>
                            <td id="Td2" runat="server">
                            <b>Amount</b>
                            </td>
                            <td id="Td1" runat="server">
                                <b>Quantity</b>
                            </td>
                            <td id="Td5" runat="server">
                                <b>Chargeby</b>
                            </td>
                           
                        </tr>
                        <tr id="ItemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                             <asp:Button ID="Button1" Text="Delete" runat="server" CommandName="Delete" />
                        </td>
                     
                        <td>
                        <asp:Label ID="lblDelPkId" runat="server" Text='<%# Eval("LotRateid") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lable2" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lable3" runat="server" Text='<%# Eval("Quantity") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lable4" runat="server" Text='<%# Eval("chargeby") %>'></asp:Label>
                        </td>
                     
                    </tr>
                </ItemTemplate>
                <EditItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />
                        </td>
                       
                        <td>
                        <asp:Label ID="lblPkId" runat="server" Text='<%# Eval("LotRateid") %>' Visible="false"></asp:Label>
                            <asp:TextBox ID="txtpdtId" Width="50px" runat="server" Text='<%# Eval("Amount") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPdtName" Width="50px" runat="server" Text='<%# Eval("Quantity") %>'></asp:TextBox>
                        </td>
                        <td>
                              <asp:DropDownList ID="ddl_reserved" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox">
                     <asp:ListItem Value="1">Hourly</asp:ListItem>
                        <asp:ListItem Value="2">Daily</asp:ListItem>
                        <asp:ListItem Value="3">Monthly</asp:ListItem>

                        <asp:ListItem Value="4">Weekly</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Chargeby]"></asp:SqlDataSource>
                            
                        </td>
                    
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListView2" PageSize="5">
        <Fields>
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
    </fieldset>
    </asp:Panel>
</td>

</tr>
</table>

    </fieldset>
     </asp:Panel>
     <br />
        <br />
        <br />
        <br />
    <asp:Panel ID="Panel3" runat="server">
   <fieldset><legend>Taxes</legend>
     <table style="width: 875px">
         <tr><td class="style35"><span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Corporate Tax ID #<span class="Apple-converted-space">&nbsp;</span></span></td>
             <td class="style36"><asp:TextBox ID="txtbox_corporate_taxid" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfv_taxid" runat="server" 
                     ControlToValidate="txtbox_corporate_taxid" ErrorMessage="*" ForeColor="#CC0000" 
                     ValidationGroup="t"></asp:RequiredFieldValidator>
             </td>
            
             <td rowspan="5" class="style29" valign="top">
           
 <asp:UpdatePanel ID="UpdatePanel7" runat="server">
        <ContentTemplate>
            <asp:ListView ID="ListView3" runat="server" OnPagePropertiesChanging="ListView3_PagePropertiesChanging"
                OnItemEditing="ListView3_ItemEditing" OnItemCanceling="ListView3_ItemCanceling"
                OnItemUpdating="ListView3_ItemUpdating"              
                onitemdeleting="ListView3_ItemDeleting" 
                onselectedindexchanged="ListView3_SelectedIndexChanged">
                <EmptyDataTemplate>
                    <p>No Records Found..</p>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table id="Table1" runat="server" width="565px" style="background-color: darkgrey;">
                        <tr id="Tr1" runat="server" style="background-color: silver;">
                            <td>
                            </td>
                            <td id="Td2" runat="server">
                            <b>TaxName</b>
                            </td>
                            <td id="Td1" runat="server">
                                <b>TaxRate</b>
                            </td>
                            <td id="Td5" runat="server">
                                <b>Calculation</b>
                            </td>
                               <td id="Td6" runat="server">
                                <b>Corporate tax Id</b>
                            </td>
                           
                        </tr>
                        <tr id="ItemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                             <asp:Button ID="Button1" Text="Delete" runat="server" CommandName="Delete" />
                        </td>
                     
                        <td>
                        <asp:Label ID="lblDelPkId" runat="server" Text='<%# Eval("TaxLot_id") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lable2" runat="server" Text='<%# Eval("TaxName") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lable3" runat="server" Text='<%# Eval("TaxRate") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lable4" runat="server" Text='<%# Eval("Sequenceofcalc") %>'></asp:Label>
                        </td>
                         <td>
                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("corporatetaxid") %>'></asp:Label>
                        </td>
                     
                    </tr>
                </ItemTemplate>
                <EditItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />
                        </td>
                       
                        <td>
                        <asp:Label ID="TaxLot_id" runat="server" Text='<%# Eval("TaxLot_id") %>' Visible="false"></asp:Label>
                            <asp:TextBox ID="TaxName" Width="50px" runat="server" Text='<%# Eval("TaxName") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="taxrate" Width="50px" runat="server" Text='<%# Eval("TaxRate") %>'></asp:TextBox>

                        </td>
                          <td>
                          <asp:DropDownList ID="ddl_seq" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox">
                     <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>

                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                </asp:DropDownList>

                        </td>
                        <td>
                            <asp:TextBox ID="corporatetaxid" Width="50px" runat="server" Text='<%# Eval("corporatetaxid") %>'></asp:TextBox>
                              
              
                            
                        </td>
                    
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:DataPager ID="DataPager2" runat="server" PagedControlID="ListView2" PageSize="5">
        <Fields>
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
  
</td>

      </tr>
      <tr>
              <td class="style35"><span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Tax Name</span></td>
              <td class="style36"><asp:TextBox ID="txtbox_taxname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfv_taxid0" runat="server" 
                      ControlToValidate="txtbox_taxname" ErrorMessage="*" ForeColor="#CC0000" 
                      ValidationGroup="t"></asp:RequiredFieldValidator>
              </td>
         </tr><tr>
         <td class="style35"><span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Tax %</span></td>
         <td class="style36"><asp:TextBox ID="txtbox_tax" runat="server"  CssClass="twitterStyleTextbox"   onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>
             <cc1:FilteredTextBoxExtender ID="txtbox_tax_FilteredTextBoxExtender" 
                 runat="server" Enabled="True" TargetControlID="txtbox_tax"   ValidChars="01234567890.">
             </cc1:FilteredTextBoxExtender>
             <asp:RequiredFieldValidator ID="rfv_taxid1" runat="server" 
                 ControlToValidate="txtbox_tax" ErrorMessage="*" ForeColor="#CC0000" 
                 ValidationGroup="t"></asp:RequiredFieldValidator>
             </td>
         </tr><tr>
         <td class="style35"><span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">&#160;Sequence for Calculation</span></td>
              <td class="style36"><asp:DropDownList ID="ddl_squencecal" runat="server" 
                      CssClass="twitterStyleTextbox" Width="130px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        </asp:DropDownList>
               </td>
        </tr>
        <tr>
                <td class="style35">&#160;</td>
                <td class="style36">
                    <asp:Button ID="btn_tax" runat="server" Text="ADD TAX"  CssClass="button" 
                        onclick="btn_tax_Click" ValidationGroup="t" />
                </td>
        </tr>

     </table>
     </fieldset>
    </asp:Panel>
    <asp:Button ID="btn_FINISH_FINAL" runat="server" Height="43px"  CssClass="button"   
            style="margin-top: 0px" Text="Update" 
            ValidationGroup="a" onclick="btn_FINISH_FINAL_Click1"/>&nbsp; &nbsp;&nbsp; 
        <asp:Button CssClass="button" runat="server" Text="Cancel" Height="43px" 
            onclick="Unnamed1_Click" />  <br />
        <br />
        <br />
        <br />
   <asp:validationsummary id="vsmSummary" runat="server" ForeColor="DarkRed" HeaderText="Please Fill the Mendentry fields"
			 Font-Bold="True" ValidationGroup="a" ShowMessageBox="True"></asp:validationsummary>

        </ContentTemplate>
                                  </cc1:TabPanel>
</cc1:TabContainer>
</ContentTemplate>
</asp:UpdatePanel>
</div>

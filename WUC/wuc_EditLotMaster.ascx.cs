﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditLotMaster : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                BindData();
            }
            catch
            {
                
            }
            try
            {
                BindData_random();
            }
            catch
            {
            }
            try
            {
                BindData_tax();
            }
            catch
            {
            }
            clsInsert onj = new clsInsert();
            DataSet rec = onj.fetchrec("SELECT [TimeZoneTblId], [TimeZoneName], [TimeZoneId] FROM [tbl_TimeZoneMaster] ORDER BY [TimeZoneId]");
            DataTable t = new DataTable("timezone");
            t.Columns.Add("TimeZoneTblId", typeof(int));
            t.Columns.Add("TimeZoneName", typeof(string));

            for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
            {
                DataRow dr = t.NewRow();
                dr[0] = rec.Tables[0].Rows[i][0].ToString();
                dr[1] = rec.Tables[0].Rows[i][2].ToString() + " [" + rec.Tables[0].Rows[i][1].ToString() + "]";
                t.Rows.Add(dr);
            }


            ddl_timezone.DataSource = t;
            ddl_timezone.DataTextField = "TimeZoneName";
            ddl_timezone.DataValueField = "TimeZoneTblId";
            ddl_timezone.DataBind();





            int id=Convert.ToInt32( Session["lotid"].ToString());
            DataSet ds = new clsInsert().fetchrec("select tbl_City.City_name, tbl_CampusMaster.Campus_Name,tbl_SiteMaster.SiteName , tbl_LotMaster.* from tbl_LotMaster inner join tbl_City on tbl_City.City_id=tbl_LotMaster.City inner join tbl_CampusMaster on tbl_CampusMaster.CampusID=tbl_LotMaster.Campus_id inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_LotMaster.Site_Id where Lot_id=" + id);
            txtbox_customername.Text = ds.Tables[0].Rows[0]["SiteName"].ToString();
            txtbox_campusname.Text = ds.Tables[0].Rows[0]["Campus_Name"].ToString();
            txtbox_lotname.Text = ds.Tables[0].Rows[0]["LotName"].ToString();
            txtbox_lotcode.Text = ds.Tables[0].Rows[0]["Lotcode"].ToString();
            txtbox_date.Text = ds.Tables[0].Rows[0]["EffectiveDate"].ToString();
           // ddl_timezone.SelectedItem.Text = ds.Tables[0].Rows[0]["Lot_timezone"].ToString();
            txtbox_hours.Text = ds.Tables[0].Rows[0]["LotHours"].ToString();
            ddl_city.SelectedValue = ds.Tables[0].Rows[0]["City"].ToString();
            txtbox_zip.Text = ds.Tables[0].Rows[0]["zip"].ToString();
            txtbox_cell.Text = ds.Tables[0].Rows[0]["Lot_Cellular"].ToString();
            txtbox_office.Text = ds.Tables[0].Rows[0]["Lot_office"].ToString();
            txtbox_email.Text = ds.Tables[0].Rows[0]["Lot_email"].ToString();
            latitude.Text = ds.Tables[0].Rows[0]["Lot_Latitude"].ToString();
            longitude.Text = ds.Tables[0].Rows[0]["Lot_Longitude"].ToString();
            txtbox_Adress1.Text = ds.Tables[0].Rows[0]["Address"].ToString();
            txtbox_enteranceinformationEnglish.Text = ds.Tables[0].Rows[0]["English_EntryGateInformation"].ToString();
            txtbox_enteranceinformationFrench.Text = ds.Tables[0].Rows[0]["French_EntryGateInformation"].ToString();
            txtbox_exitinformationEnglish.Text = ds.Tables[0].Rows[0]["English_ExitGateInformation"].ToString();
            txtbox_exitinformationFrench.Text = ds.Tables[0].Rows[0]["French_ExitGateInformation"].ToString();
            txtbox_restriction.Text = ds.Tables[0].Rows[0]["Englis_Restrictions"].ToString();
            txtbox_restrictionFrench.Text = ds.Tables[0].Rows[0]["French_Restriction"].ToString();
            txtbox_Description.Text = ds.Tables[0].Rows[0]["English_Description"].ToString();
            txtbox_DescriptionFrench.Text = ds.Tables[0].Rows[0]["French_Description"].ToString();
            ddl_lottype.SelectedValue = ds.Tables[0].Rows[0]["Lot_type"].ToString();
            txtbox_otherdetailsweb.Text = ds.Tables[0].Rows[0]["otherdetailslot"].ToString();


            ddl_billingEntry.SelectedValue = ds.Tables[0].Rows[0]["Billing_entry"].ToString();
            ddl_billingexit.SelectedValue = ds.Tables[0].Rows[0]["Billing_exit"].ToString();
            txtbox_paymentreminderdays.Text = ds.Tables[0].Rows[0]["PaymentRemdays"].ToString();
            txtbox_interestrate.Text = ds.Tables[0].Rows[0]["Interest_Rate"].ToString();
            txtbox_latefee.Text = ds.Tables[0].Rows[0]["latefees"].ToString();
            txtbox_activationcharge.Text = ds.Tables[0].Rows[0]["ActivationCharge"].ToString();
            if (ds.Tables[0].Rows[0]["DisplayActivationcharge"] == "true")
            {
                rbtn_yes.Checked = true;
                rbtn_no.Checked = false;
            }
            else
            {
                rbtn_no.Checked = true;
                rbtn_yes.Checked = false;
            }
            if (ds.Tables[0].Rows[0]["Display_deposite"] == "true")
            {
                rbtn_yes_deposite.Checked = true;
                rbtn_no_deposite.Checked = false;
            }
            else
            {
                rbtn_yes_deposite.Checked = false;
                rbtn_no_deposite.Checked = true;
            }
            ddl_latepaymentaction.SelectedValue = ds.Tables[0].Rows[0]["LatepaymentAction"].ToString();
            ddl_creditcardpayment.SelectedValue = ds.Tables[0].Rows[0]["CreditcardfailureAction"].ToString();
            if (ds.Tables[0].Rows[0]["LotTransaction_Monthly"] == "true")
            {
                chk_monthly.Checked = true;
               
            }
            if (ds.Tables[0].Rows[0]["LotTransaction_Daily"] == "true")
            {
                chk_daily.Checked = true;

            }
            txtbox_reservedtotalspace.Text = ds.Tables[0].Rows[0]["Reserved_Totalspace"].ToString();
            txtbox_reservedunablespace.Text = ds.Tables[0].Rows[0]["Reserved_Unablespace"].ToString();
            txtbox_randomtotalspace.Text = ds.Tables[0].Rows[0]["Random_Totalspace"].ToString();
            txtbox_randomunablespace.Text = ds.Tables[0].Rows[0]["Random_Unablespace"].ToString();

           
            
         
        }
    }
    public void BindData()
    {
        DataTable dt_reserved = new DataTable();
        dt_reserved.Columns.Add("LotRateid", typeof(int));
        dt_reserved.Columns.Add("Amount", typeof(decimal));
        dt_reserved.Columns.Add("Quantity", typeof(decimal));
        dt_reserved.Columns.Add("chargeby", typeof(string));

        DataSet _dsBind = new clsInsert().fetchrec("select * from tbl_LotRateMaster  where Lotid=" + Session["lotid"].ToString() + " and Parkingtype=1 and ISSpecial=0");
        for (int i = 0; i < _dsBind.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dt_reserved.NewRow();
            dr[0] = Convert.ToInt32(_dsBind.Tables[0].Rows[i][0].ToString());
            dr[1] = _dsBind.Tables[0].Rows[i][3].ToString();
            dr[2] = _dsBind.Tables[0].Rows[i][4].ToString();
            if (_dsBind.Tables[0].Rows[i][5].ToString() == "1")
            {
                dr[3] = "Hourly";
            }
            if (_dsBind.Tables[0].Rows[i][5].ToString() == "2")
            {
                dr[3] = "Days";

            }
            if (_dsBind.Tables[0].Rows[i][5].ToString() == "3")
            {
                dr[3] = "Monthly";

            }
            if (_dsBind.Tables[0].Rows[i][5].ToString() == "4")
            {
                dr[3] = "Weekly";

            }

            dt_reserved.Rows.Add(dr);
        }

        ListView1.DataSource = dt_reserved;
        ListView1.DataBind();

    }

    public void BindData_random()
    {
        
        DataTable dt_random = new DataTable();
        dt_random.Columns.Add("LotRateid",typeof(int));
        dt_random.Columns.Add("Amount", typeof(decimal));
        dt_random.Columns.Add("Quantity", typeof(decimal));
        dt_random.Columns.Add("chargeby", typeof(string));



        DataSet _dsBind_random = new clsInsert().fetchrec("select * from tbl_LotRateMaster  where Lotid=" + Session["lotid"].ToString() + " and Parkingtype=2 and ISSpecial=0");

        for (int i = 0; i < _dsBind_random.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dt_random.NewRow();
            dr[0] = Convert.ToInt32(_dsBind_random.Tables[0].Rows[i][0].ToString());
            dr[1] = _dsBind_random.Tables[0].Rows[i][3].ToString();
            dr[2] = _dsBind_random.Tables[0].Rows[i][4].ToString();
            if (_dsBind_random.Tables[0].Rows[i][5].ToString() == "1")
            {
                dr[3] = "Hourly";
            }
            if (_dsBind_random.Tables[0].Rows[i][5].ToString() == "2")
            {
                dr[3] = "Days";

            }
            if (_dsBind_random.Tables[0].Rows[i][5].ToString() == "3")
            {
                dr[3] = "Monthly";

            }
            if (_dsBind_random.Tables[0].Rows[i][5].ToString() == "4")
            {
                dr[3] = "Weekly";

            }
            
            dt_random.Rows.Add(dr);
        }

        ListView2.DataSource = dt_random;
        ListView2.DataBind();

    }

    protected void txtbox_unablespace_TextChanged(object sender, EventArgs e)
    {

    }
  
    protected void chbk_Reserved_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void chbk_Random_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_name_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_hours_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn1_next_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;
    }
    protected void txtbox_email_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn2_next_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 2;

    }
    protected void txtbox_Adress1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void next3_Click(object sender, EventArgs e)
    {

    }
    protected void btn_tax_Click(object sender, EventArgs e)
    {
        try
        {


            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("lot_id", Convert.ToInt32(Session["lotid"].ToString()));
            hst.Add("taxname", txtbox_taxname.Text);
            hst.Add("taxrate", Convert.ToDecimal(txtbox_tax.Text));
            hst.Add("seqofcal", Convert.ToInt32(ddl_squencecal.SelectedItem.Value));
            hst.Add("corporatetaxid", txtbox_corporate_taxid.Text);

            int result = objData.ExecuteNonQuery("[sp_LotTax]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("UserManager.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }

            BindData_tax();
            txtbox_taxname.Text = "";
            txtbox_tax.Text = "";
            txtbox_corporate_taxid.Text = "";

        }
        catch (Exception ex)
        {

            //lblmsg.Text = ex.Message.ToString();
        } 
    }
    protected void btn_FINISH_FINAL_Click(object sender, EventArgs e)
    {

    }
    protected void txtbox_restriction_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_restrictionFrench_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_yes_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_no_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_no_deposite_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_reservedtotalspace_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_reservedunablespace_TextChanged(object sender, EventArgs e)
    {

    }
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_randomunablespace_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void ddl_timezone_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    string stat = "false";
    protected void Addreserved_Click(object sender, EventArgs e)
    {
        try
        {


            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("lotid", Convert.ToInt32( Session["lotid"].ToString()));
            hst.Add("parkingtype", 1);
            hst.Add("amount", Convert.ToDecimal(amount.Text));
            hst.Add("quantity", Convert.ToDecimal(Quantity.Text));
            hst.Add("chargeby", Convert.ToInt32(ddl_reserved.SelectedItem.Value));
            hst.Add("isspecial", stat);

            int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("UserManager.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }

            BindData();
            amount.Text = "";
            Quantity.Text = "";
           
        }
        catch (Exception ex)
        {

            //lblmsg.Text = ex.Message.ToString();
        } 
    }
    protected void ListView1_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {

        lvDataPager1.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
        BindData();
    }

    protected void ListView1_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        ListView1.EditIndex = e.NewEditIndex;
        BindData();
    }

    protected void ListView1_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        Label lblPkId = (Label)ListView1.Items[e.ItemIndex].FindControl("lblPkId");
        TextBox amount = ((TextBox)ListView1.Items[e.ItemIndex].FindControl("txtpdtId"));
        TextBox quantity = ((TextBox)ListView1.Items[e.ItemIndex].FindControl("txtPdtName"));
        DropDownList chargeby = ((DropDownList)ListView1.Items[e.ItemIndex].FindControl("ddl_reserved"));


        hst.Clear();

        hst.Add("action", "updatereserved");
        hst.Add("lotrateid", Convert.ToInt32(lblPkId.Text));
        
        hst.Add("amount", Convert.ToDecimal(amount.Text));
        hst.Add("quantity", Convert.ToDecimal(quantity.Text));
        hst.Add("chargeby", Convert.ToInt32(chargeby.SelectedValue));

        int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
        if (result > 0)
        {
              ListView1.EditIndex = -1;
            BindData();
            //Response.Redirect("UserManager.aspx");

        }
        else
        {
              ListView1.EditIndex = -1;
            BindData();
            // lbl_error.Visible = true;

        }

       
          
            //lblmsg.Text = "Successfully Updated!...";
        

        
        //lblmsg.Text = "Successfully Updated!...";
    }

    protected void ListView1_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        ListView1.EditIndex = -1;
        BindData();
    }


    protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        try
        {
            Label lotrateid = ((Label)ListView1.Items[e.ItemIndex].FindControl("lblDelPkId"));

            hst.Clear();

            hst.Add("action", "delete");
            hst.Add("lotrateid", Convert.ToInt32(lotrateid.Text));
           
            int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // ListView1.EditIndex = -1;
                BindData();
                //Response.Redirect("UserManager.aspx");

            }
            else
            {
               // ListView1.EditIndex = -1;
                BindData();
                // lbl_error.Visible = true;

            }





          
        }

        catch (Exception)
        {

            throw;
        }



    }



    protected void addrandom_Click(object sender, EventArgs e)
    {
        try
        {


            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("lotid", Convert.ToInt32(Session["lotid"].ToString()));
            hst.Add("parkingtype", 2);
            hst.Add("amount", Convert.ToDecimal(amountrandom.Text));
            hst.Add("quantity", Convert.ToDecimal(quantityrandom.Text));
            hst.Add("chargeby", Convert.ToInt32(ddlrandom.SelectedItem.Value));
            hst.Add("isspecial", stat);

            int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("UserManager.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }

            BindData_random();
            amount.Text = "";
            Quantity.Text = "";

        }
        catch (Exception ex)
        {

            //lblmsg.Text = ex.Message.ToString();
        } 
    }
    protected void ListView2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ListView2_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {

        DataPager1.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
        BindData_random();
    }

    protected void ListView2_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        ListView2.EditIndex = e.NewEditIndex;
        BindData_random();
    }

    protected void ListView2_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        Label lblPkId = (Label)ListView2.Items[e.ItemIndex].FindControl("lblPkId");
        TextBox amount = ((TextBox)ListView2.Items[e.ItemIndex].FindControl("txtpdtId"));
        TextBox quantity = ((TextBox)ListView2.Items[e.ItemIndex].FindControl("txtPdtName"));
        DropDownList chargeby = ((DropDownList)ListView2.Items[e.ItemIndex].FindControl("ddl_reserved"));


        hst.Clear();

        hst.Add("action", "updatereserved");
        hst.Add("lotrateid", Convert.ToInt32(lblPkId.Text));

        hst.Add("amount", Convert.ToDecimal(amount.Text));
        hst.Add("quantity", Convert.ToDecimal(quantity.Text));
        hst.Add("chargeby", Convert.ToInt32(chargeby.SelectedValue));

        int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
        if (result > 0)
        {
            ListView2.EditIndex = -1;
            BindData_random();
            //Response.Redirect("UserManager.aspx");

        }
        else
        {
            ListView2.EditIndex = -1;
            BindData_random();
            // lbl_error.Visible = true;

        }



        //lblmsg.Text = "Successfully Updated!...";



        //lblmsg.Text = "Successfully Updated!...";
    }

    protected void ListView2_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        ListView2.EditIndex = -1;
        BindData_random();
    }


    protected void ListView2_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        try
        {
            Label lotrateid = ((Label)ListView2.Items[e.ItemIndex].FindControl("lblDelPkId"));

            hst.Clear();

            hst.Add("action", "delete");
            hst.Add("lotrateid", Convert.ToInt32(lotrateid.Text));

            int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                // ListView1.EditIndex = -1;
                BindData_random();
                //Response.Redirect("UserManager.aspx");

            }
            else
            {
                // ListView1.EditIndex = -1;
                BindData_random();
                // lbl_error.Visible = true;

            }






        }

        catch (Exception)
        {

            throw;
        }



    }

    public void BindData_tax()
    {

        DataTable dt_tax = new DataTable();
       dt_tax.Columns.Add("TaxLot_id", typeof(int));
        dt_tax.Columns.Add("TaxName", typeof(string));
        dt_tax.Columns.Add("TaxRate", typeof(decimal));
        dt_tax.Columns.Add("Sequenceofcalc", typeof(int));
        dt_tax.Columns.Add("corporatetaxid", typeof(string));



        DataSet _dsBind_tax = new clsInsert().fetchrec("select * from tbl_LotTaxMaster  where Lotid=" + Session["lotid"].ToString());

        for (int i = 0; i < _dsBind_tax.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dt_tax.NewRow();
            dr[0] = Convert.ToInt32(_dsBind_tax.Tables[0].Rows[i][0].ToString());
            dr[1] = _dsBind_tax.Tables[0].Rows[i][2].ToString();
            dr[2] = _dsBind_tax.Tables[0].Rows[i][3].ToString();
            dr[3] = _dsBind_tax.Tables[0].Rows[i][4].ToString();
            dr[4] = _dsBind_tax.Tables[0].Rows[i][5].ToString();

            dt_tax.Rows.Add(dr);
        }

        ListView3.DataSource = dt_tax;
        ListView3.DataBind();

    }

    protected void ListView3_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ListView3_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {

        DataPager2.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
        BindData_tax();
    }

    protected void ListView3_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        ListView3.EditIndex = e.NewEditIndex;
        BindData_tax();
    }

  
    protected void ListView3_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        Label TaxLot_id = (Label)ListView3.Items[e.ItemIndex].FindControl("TaxLot_id");
        TextBox TaxRate = ((TextBox)ListView3.Items[e.ItemIndex].FindControl("taxrate"));
        TextBox TaxName = ((TextBox)ListView3.Items[e.ItemIndex].FindControl("TaxName"));
        TextBox corporatetaxid = ((TextBox)ListView3.Items[e.ItemIndex].FindControl("corporatetaxid"));
        DropDownList Sequenceofcalc = ((DropDownList)ListView3.Items[e.ItemIndex].FindControl("ddl_seq"));

        
        hst.Clear();

        hst.Add("action", "update");
        hst.Add("taxlotid", Convert.ToInt32(TaxLot_id.Text));

        hst.Add("taxname", TaxName.Text);

        hst.Add("taxrate", Convert.ToDecimal(TaxRate.Text));
        hst.Add("corporatetaxid",corporatetaxid.Text);
        hst.Add("seqofcal", Convert.ToInt32(Sequenceofcalc.SelectedValue));

        int result = objData.ExecuteNonQuery("[sp_LotTax]", CommandType.StoredProcedure, hst);
        if (result > 0)
        {
            ListView3.EditIndex = -1;
            BindData_tax();
            //Response.Redirect("UserManager.aspx");

        }
        else
        {
            ListView3.EditIndex = -1;
            BindData_tax();
            // lbl_error.Visible = true;

        }



        //lblmsg.Text = "Successfully Updated!...";



        //lblmsg.Text = "Successfully Updated!...";
    }

    protected void ListView3_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        ListView3.EditIndex = -1;
        BindData_tax();
       // BindData_random();
    }

    protected void ListView3_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        try
        {
            Label taxid = ((Label)ListView3.Items[e.ItemIndex].FindControl("lblDelPkId"));

            hst.Clear();

            hst.Add("action", "delete");
            hst.Add("taxlotid", Convert.ToInt32(taxid.Text));

            int result = objData.ExecuteNonQuery("[sp_LotTax]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                BindData_tax();
                

            }
            else
            {

                BindData_tax();
                
            }






        }

        catch (Exception)
        {

            throw;
        }



    }
    protected void btn_FINISH_FINAL_Click1(object sender, EventArgs e)
    {
        try
        {

            updaterec();
            Response.Redirect("Admin_LotManger.aspx");
        }
        catch
        {
        }
    }

    string status_activation = "";
    string status_deposit = "";
    string status_monthly = "";
    string status_daily = "";
    string status_reserved = "";
    string status_random = "";
    decimal defaultvalue =Convert.ToDecimal( 0.00);
    int total_reservedspace, total_randomsapce, total_unablespace_reserved, totalunable_randomsapce = 0;
    public void updaterec()
    {
 
    
        try
        {
          
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("lotid", Convert.ToInt32(Session["lotid"].ToString()));
            
            hst.Add("lotname", txtbox_lotname.Text);
            hst.Add("lotcode", txtbox_lotcode.Text);
            hst.Add("effectvdate ", Convert.ToDateTime(txtbox_date.Text));
           
            hst.Add("otherdetail", txtbox_otherdetailsweb.Text);
            hst.Add("address", txtbox_Adress1.Text);
            hst.Add("city", ddl_city.SelectedValue);
            hst.Add("zip", txtbox_zip.Text);
            hst.Add("lot_latitude", latitude.Text);
            hst.Add("lot_longitude", longitude.Text);
            hst.Add("lot_cell", txtbox_cell.Text);

            hst.Add("lot_office", txtbox_office.Text);
            hst.Add("lot_email", txtbox_email.Text);
            hst.Add("lottimezone", ddl_timezone.SelectedItem.Value);
            hst.Add("lothours", txtbox_hours.Text);
           
            hst.Add("englishentrygateinformation", txtbox_enteranceinformationEnglish.Text);
            hst.Add("englishexitgateinformation",txtbox_exitinformationEnglish.Text);
            hst.Add("englishdescription", txtbox_Description.Text);
            hst.Add("englishrestriction", txtbox_restriction.Text);
            hst.Add("frenchentrygateinformation", txtbox_enteranceinformationFrench.Text);
            hst.Add("frenchexitgateinformation", txtbox_exitinformationFrench.Text);
            hst.Add("frenchrestriction", txtbox_restrictionFrench.Text);
            hst.Add("frenchdescription", txtbox_DescriptionFrench.Text);


           
            hst.Add("lottype", ddl_lottype.SelectedValue);
            hst.Add("billingentry", ddl_billingEntry.SelectedValue);
            hst.Add("billingexit", ddl_billingexit.SelectedValue);
            hst.Add("paymentrem", Convert.ToInt32(txtbox_paymentreminderdays.Text));
            hst.Add("intrestrate", Convert.ToDecimal(txtbox_interestrate.Text));
            hst.Add("latefees", Convert.ToDecimal(txtbox_latefee.Text));
            hst.Add("activationcharge", Convert.ToDecimal(txtbox_activationcharge.Text));
            if (rbtn_yes.Checked == true)
            {
                status_activation = "true";
                hst.Add("displayactivationcharge", status_activation);
            }
            if (rbtn_no.Checked == true)
            {
                status_activation="false";
                hst.Add("displayactivationcharge", status_activation);

            }

            if (rbtn_yes_deposite.Checked == true)
            {
                status_deposit = "true";
                hst.Add("displaydeposite", status_deposit);
            }
            if (rbtn_no.Checked == true)
            {
                status_deposit = "false";
                hst.Add("displaydeposite", status_deposit);

            }
           
            hst.Add("latepaymentaction", ddl_latepaymentaction.SelectedValue);
            hst.Add("creditpaymentaction", ddl_creditcardpayment.SelectedValue);

            if (chk_monthly.Checked == true)
            {
                status_monthly = "true";
                hst.Add("lottransaction_monthly", status_monthly);

            }
            else
            {
                status_monthly = "false";
                hst.Add("lottransaction_monthly", status_monthly);

            }

            if (chk_daily.Checked == true)
            {
                status_daily = "true";
                hst.Add("lottansaction_daily", status_daily);

            }
            else
            {
                status_daily = "false";
                hst.Add("lottansaction_daily", status_daily);

            }

            DataSet dsreservedcheck = new clsInsert().fetchrec("select * from tbl_LotRateMaster where  Parkingtype=1 and ISSpecial=0 and Lotid="+Convert.ToInt32(Session["lotid"].ToString()));
            if (dsreservedcheck.Tables.Count > 0)
            {
                status_reserved = "true";
                hst.Add("reserved_parking", status_reserved);
              
            }
            else
            {
                status_reserved = "true";
                hst.Add("reserved_parking", status_reserved);
               
            }
            status_random = "true";

            hst.Add("random_parking", status_random);

            if (txtbox_randomtotalspace.Text != "")
            {

                hst.Add("random_parking_totalspace", Convert.ToInt32(txtbox_randomtotalspace.Text));
                total_randomsapce = Convert.ToInt32(txtbox_randomtotalspace.Text);
            }
            if (txtbox_randomtotalspace.Text == "")
            {

                hst.Add("random_parking_totalspace", Convert.ToInt32(0));
                total_randomsapce = Convert.ToInt32(0);
            }
            if (txtbox_randomunablespace.Text != "")
            {
                hst.Add("random_parking_unablespace", Convert.ToInt32(txtbox_randomunablespace.Text));
                totalunable_randomsapce = Convert.ToInt32(txtbox_randomunablespace.Text);
            }

            if (txtbox_randomunablespace.Text == "")
            {
                hst.Add("random_parking_unablespace", Convert.ToInt32(0));
                totalunable_randomsapce = Convert.ToInt32(0);

            }

            if (txtbox_reservedtotalspace.Text != "")
            {
                hst.Add("reserved_totalspace", Convert.ToInt32(txtbox_reservedtotalspace.Text));
                total_reservedspace = Convert.ToInt32(txtbox_reservedtotalspace.Text);
            }
            if (txtbox_reservedtotalspace.Text == "")
            {
                hst.Add("reserved_totalspace", Convert.ToInt32(0));
                total_reservedspace = Convert.ToInt32(0);

            }
            if (txtbox_reservedunablespace.Text != "")
            {
                hst.Add("reserved_unablespace", Convert.ToInt32(txtbox_reservedunablespace.Text));
                total_unablespace_reserved = Convert.ToInt32(txtbox_reservedunablespace.Text);
            }
            if (txtbox_reservedunablespace.Text == "")
            {
                hst.Add("reserved_unablespace", Convert.ToInt32(0));
                total_unablespace_reserved = Convert.ToInt32(0);

            }

            int totalspace = Convert.ToInt32(total_reservedspace) + Convert.ToInt32(total_randomsapce);
            int unablespace = Convert.ToInt32(total_unablespace_reserved) + Convert.ToInt32(totalunable_randomsapce);

            hst.Add("totalspaces", totalspace);
            hst.Add("unablespace", unablespace);

            hst.Add("modifiedby", "admin");
            hst.Add("operatorid", "admin");

            hst.Add("dateofchanging", DateTime.Now);
           






            int result = objData.ExecuteNonQuery("[sp_LotMaster]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                try
                {
                    hst.Clear();


                    hst.Add("action", "updatecharges");
                    hst.Add("lotid", Convert.ToInt32(Session["lotid"].ToString()));

                    if (txtbox_activationcharge.Text != "")
                    {
                        hst.Add("charges", Convert.ToDecimal(txtbox_activationcharge.Text));
                    }
                    else
                    {
                        hst.Add("charges", defaultvalue);

                    }

                    int result_activation = objData.ExecuteNonQuery("[sp_Extracharges]", CommandType.StoredProcedure, hst);
                    if (result_activation > 0)
                    {


                    }
                    else
                    {


                        // lbl_error.Visible = true;

                    }
                }
                catch
                {
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                    //lbl_error.Visible = true;

                }
                
            }
            else
            {
                // lbl_error.Visible = true;

            }


        }
        catch
        {

        }

    }
    public void updateextracharge(int id,decimal charges)
    {
        try
        {
            hst.Clear();

            hst.Add("action", "updatecharges");
            hst.Add("lotid", id);

            if (txtbox_activationcharge.Text != "")
            {
                hst.Add("charges", Convert.ToDecimal(txtbox_activationcharge.Text));
            }
            else
            {
                hst.Add("charges", defaultvalue);

            }

            int result = objData.ExecuteNonQuery("[sp_Extracharges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {


            }
            else
            {


                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
    protected void Unnamed1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Admin_LotManger.aspx");
    }
    protected void Quantity_TextChanged(object sender, EventArgs e)
    {

    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_EditLotToPlanAllot.ascx.cs" Inherits="WUC_wuc_EditLotToPlanAllot" %>
<style type="text/css">
    .style1
    {
        width: 202px;
    }
    .style2
    {
        width: 72px;
    }
</style>
<div>
<table style="height: 85px; width: 929px">
<tr>
<td class="style2">
Site Name :
</td>
<td class="style1">
    <asp:DropDownList ID="ddl_sitename" runat="server" 
        DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId" 
        onselectedindexchanged="ddl_sitename_SelectedIndexChanged" AutoPostBack=true>
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
    </asp:SqlDataSource>
</td>
</tr>
<tr>
<td class="style2">
    Lot Name :</td>
<td class="style1"><br />
<br />
    <asp:CheckBoxList ID="chk_Lotname" runat="server" 
        onselectedindexchanged="chk_Lotname_SelectedIndexChanged">
    </asp:CheckBoxList>
</td>
</tr>
<tr>
<td>
    Plans&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
<td>
<br />
<br />
<br />
    <asp:CheckBoxList ID="chk_plans" runat="server">
    </asp:CheckBoxList>
</td>
</tr>
<tr>
<td>
</td>
<td>
    <asp:Button ID="btn_add" runat="server" onclick="btn_add_Click" Text="Update" />
</td>
</tr>
</table>
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource2" 
        onitemcommand="Repeater1_ItemCommand">
    <HeaderTemplate>
            <table width="100%" class="rounded-corner">
                <tr>
                     
                     <th>
                        Lot_id
                    </th>
                     <th>
                      Plan_id
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>
                    <%#Eval("lot_id")%>
                    </td><td>
                    <%#Eval("plan_id")%>
                    

                </td>
            
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td> 
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>
                    <asp:Label ID="idLabel" runat="server" Text='<%# Eval("lot_id") %>' 
                                        Visible="False" />
                     <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("plan_id") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="Label1" runat="server" Text='<%# Eval("status") %>' 
                                        Visible="False"></asp:Label>
                 </tr>
        </ItemTemplate>
         <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_LotToPlanAllot]"></asp:SqlDataSource>
</div>

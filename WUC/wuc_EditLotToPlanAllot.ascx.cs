﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditLotToPlanAllot : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            this.showplan();


        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {

    }
    private void showplan()
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_PlanMaster";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["Plan_Name"].ToString();
                        item.Value = sdr["Plan_id"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_plans.Items.Add(item);
                    }
                }
                conn.Close();
            }
        }
    }
    protected void chk_Lotname_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void ddl_sitename_SelectedIndexChanged(object sender, EventArgs e)
    {
        chk_Lotname.Items.Clear();
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_LotMaster where Site_Id='" + ddl_sitename.SelectedValue + "'";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["LotName"].ToString();
                        item.Value = sdr["Lot_id"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_Lotname.Items.Add(item);

                    }
                }
                conn.Close();
            }
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            delete_plan(ID);
        }

    }
    public void delete_plan(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_lot_to_plan_allot]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("EditLotToPlanAllot.aspx");

            }
            else
            {
                Response.Redirect("EditLotToPlanAllot.aspx");
            }
        }
        catch
        {
            Response.Redirect("EditLotToPlanAllot.aspx");

        }


    }

}
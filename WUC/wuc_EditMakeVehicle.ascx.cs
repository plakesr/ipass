﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditMakeVehicle : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
         
            //Session["id"] = l3.Text;
        if (!IsPostBack == true)
        {
            try
            {
                txtbox_makeEdit.Text = Session["VehicleMake"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_VehicleMakeModelMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_Vehicle Make_Model.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
              //  Response.Redirect("Admin_VehicleMakeModelMaster.aspx");
            }
        }
        
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
           // string msg = "1";
            updateVehicleMake();
           // Response.Redirect("Admin_VehicleMakeModelMaster.aspx?msg=" + msg);

           // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            //clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    string msg;
    public void updateVehicleMake()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id", Convert.ToInt32(Session["id"].ToString()));

            hst.Add("vehiclemake", txtbox_makeEdit.Text);
            hst.Add("operator_id", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_Vehicle_make]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleMakeModelMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_Vehicle Make_Model.aspx?msg=" + msg);

                }

             //   Response.Redirect("Admin_VehicleMakeModelMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_EditModelVehicle.ascx.cs" Inherits="WUC_wuc_EditModelVehicle" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 175px;
    }
    .style3
    {
        width: 175px;
        height: 23px;
    }
    .style4
    {
        height: 23px;
    }
</style>

    <table class="style1">
        <tr>
            <td class="style2">
                Vehicle Make&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_makeEdit" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="VehiMake" 
                    DataValueField="VehiMakeID">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_VehicleMakes]"></asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="rfv_editMake" runat="server" 
                    ControlToValidate="ddl_makeEdit" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Vehicle_Model&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_modelEdit" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_makeedit" runat="server" 
                    ControlToValidate="txtbox_modelEdit" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
            </td>
            <td class="style4">
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_Update" runat="server" Text="Update" ValidationGroup="a" class="btn"
                    onclick="btn_Update_Click" Height="28px" Width="66px" />
            </td>
        </tr>
    </table>
    <br />
  


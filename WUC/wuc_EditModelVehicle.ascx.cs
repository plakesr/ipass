﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditModelVehicle : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                txtbox_modelEdit.Text = Session["Vehicle Model"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_VehicleModel.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_VehicleModel.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
    protected void btn_Update_Click(object sender, EventArgs e)
    {
        try
        {
           // string msg = "1";
            UpdateVehicleModel();
            //Response.Redirect("Admin_VehicleModel.aspx?msg=" + msg);

            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    string msg;
    public void UpdateVehicleModel()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id",Session["id"].ToString());

            hst.Add("vehiclemake", ddl_makeEdit.SelectedValue);

            hst.Add("vehiclemodel", txtbox_modelEdit.Text);
            hst.Add("operator_id", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_Vehicle_Model]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleModel.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_VehicleModel.aspx?msg=" + msg);

                }


              //  Response.Redirect("Admin_VehicleModel.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
}
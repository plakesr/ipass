﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.IO;

public partial class WUC_wuc_EditUploadTermsAndConditionForSite : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            
            try
            {
                txtbox_sitename.Text = Session["sitename"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_uploadSiteAgreementDocument.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_UploadSiteAgreementDocument.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
    string pdfname = "";
    string msg = "";
    public void updateTermsDocumenttosite()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id", Convert.ToInt32(Session["id"].ToString()));

            hst.Add("siteid", Convert.ToInt32(Session["Siteid"].ToString()));
            hst.Add("sitetermpdfname", pdfname);

            hst.Add("operator", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_TermsToSite]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_uploadSiteAgreementDocument.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_UploadSiteAgreementDocument.aspx?msg=" + msg);

                }

             

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);
               

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);


        }

    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {



            if (fup_pdf.HasFile)
            {
                string oFName = fup_pdf.FileName.ToString().Trim();
                string[] strArr = oFName.Split('.');
                int sLength = strArr.Length;
                if (strArr[sLength - 1] == "pdf")
                {
                    pdfname = txtbox_sitename.Text + "." + strArr[1];
                    string Path = Server.MapPath("~/SitePDFForTems/");
                    File.Delete(Path + pdfname);
                    fup_pdf.SaveAs(Path + pdfname);
                    

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload pdf only.')", true);
                    return;
                }
            }

            
            updateTermsDocumenttosite();
            
          
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
}
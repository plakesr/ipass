﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_EditVehicleColor : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                txtbox_colorname.Text = Session["VehicleName"].ToString();

                
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_VehicleColor.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_VehicleColor.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
    string msg;
    public void updatevehicleColor()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "update");
            hst.Add("id",Convert.ToInt32(Session["VehicleID"].ToString()));
            hst.Add("vehi_color", txtbox_colorname.Text);


            int result = objData.ExecuteNonQuery("[Sp_vehicleColor]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleColor.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_VehicleColor.aspx?msg=" + msg);

                }

                //Response.Redirect("Admin_VehicleColor.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    protected void btn_colorname_Click(object sender, EventArgs e)
    {
        try
        {
           

            updatevehicleColor();
      
            

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
}
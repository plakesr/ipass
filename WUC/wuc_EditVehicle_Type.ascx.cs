﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;

public partial class WUC_wuc_EditVehicle : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {

            try
            {
                txtbox_vehicleType.Text = Session["vehicle_type"].ToString();
                
            }
            catch
            {

                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_VehicleTypeMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_VehicleTypeMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
               // Response.Redirect("Admin_VehicleTypeMaster.aspx");
            }
        }
    }
    protected void btn_submit_Click1(object sender, EventArgs e)
    {
        try
        {
            //string msg = "1";
            updatevehicle();
          //  Response.Redirect("Admin_VehicleTypeMaster.aspx?msg=" + msg);

            clear();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    string msg;
    public void updatevehicle()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "UPDATE");
            hst.Add("v_id", Convert.ToInt32(Session["vehicleid"].ToString()));

            hst.Add("vehicle_type", txtbox_vehicleType.Text);
            hst.Add("modifiedby", "lovey_modifier");
            hst.Add("operator_id", "lovey_operator");
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_vehicle]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleTypeMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_VehicleTypeMaster.aspx?msg=" + msg);

                }

              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
    public void clear()
    {
        txtbox_vehicleType.Text = "";

    }

}
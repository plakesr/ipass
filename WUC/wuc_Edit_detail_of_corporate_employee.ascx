﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Edit_detail_of_corporate_employee.ascx.cs" Inherits="wuc_Edit_detail_of_caorporate_employee" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
   <style type="text/css">
       .style1
       {
           width: 89px;
       }
       .style2
       {
           width: 108px;
       }
       .style7
       {
           text-align: right;
       }
   </style>
   <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
    
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
 <div class="body-container">

  

<%--<table class="registration-area" width="100%">
    <tr><td class="style1" valign="top">Employee Code </td>
        <td class="style3">
            <asp:TextBox ID="txtboxcode_employeecode" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Employee Code" Enabled="False"></asp:TextBox>

</td><td class="style4"></td><td class="style5">&nbsp;</td><td class="style6">
        &nbsp;</td><td>&nbsp;</td></tr><tr><td class="style1" valign="top">First Name </td><td class="style3"><asp:TextBox ID="txtbox_emp_firstname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  First Name"></asp:TextBox>

</td><td class="style4" valign="top">Middle Name</td>
        <td class="style5"><asp:TextBox ID="txtbox_emp_middlename" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the Middle Name" 
                    ValidationGroup="a"></asp:TextBox>

</td><td class="style6" valign="top">Last Name</td>
        <td><asp:TextBox ID="txtbox_emp_lastname" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Last Name" 
                ValidationGroup="a"></asp:TextBox>

</td></tr><tr><td class="style1" valign="top">St#</td><td class="style3">
            <asp:TextBox ID="txtBox_emp_address1" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the St#" 
                    ValidationGroup="a"></asp:TextBox>

</td><td class="style4" valign="top">Street Name</td>
        <td class="style5"><asp:TextBox ID="txtBox_emp_address2" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the  Street Name" 
                    ValidationGroup="a"></asp:TextBox>

</td><td class="style6" valign="top">Email</td>
        <td><asp:TextBox ID="txtBox_emp_emailid" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Email Id" 
                ValidationGroup="a"></asp:TextBox>
                <asp:RegularExpressionValidator ID="rgexp_email" runat="server" 
                    ErrorMessage="*Please Enter Email In The Correct Format" ControlToValidate="txtBox_emp_emailid" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

</td></tr><tr><td class="style1">Country</td>
        <td class="style3"><asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <asp:DropDownList ID="ddl_Parkercountry" runat="server" AutoPostBack="True" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource5" 
                                DataTextField="Country_name" 
            DataValueField="Country_id"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                
                                SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country] ORDER BY [Country_name]"></asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>

</td><td class="style4" >Province</td><td class="style5"><asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
        <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="True" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource3" 
                                DataTextField="ProvinceName" DataValueField="ProvinceId" 
                                ValidationGroup="a" ></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                 
                                SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName"><SelectParameters>
                        <asp:ControlParameter ControlID="ddl_Parkercountry" Name="Country_id" 
                                        PropertyName="SelectedValue" Type="Int64" /></SelectParameters></asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>

</td><td class="style6">City</td><td valign="bottom" ><asp:UpdatePanel 
            ID="UpdatePanel6" runat="server"><ContentTemplate>
        <asp:DropDownList ID="ddl_Parkercity" runat="server" CssClass="twitterStyleTextbox" 
                DataSourceID="SqlDataSource6" DataTextField="City_name" 
                DataValueField="City_id" 
                onselectedindexchanged="ddl_Parkercity_SelectedIndexChanged"></asp:DropDownList>&#160;<asp:SqlDataSource 
                ID="SqlDataSource6" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                onselecting="SqlDataSource6_Selecting" 
                SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id) order by City_name">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlstate" Name="Province_id" 
                            PropertyName="SelectedValue" Type="Int64" /></SelectParameters></asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>

</td></tr><tr><td class="style1">Postal Code</td>
        <td class="style3"><asp:TextBox ID="txtbox_emp_postal" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the Postal Code" 
                    ValidationGroup="a"></asp:TextBox>

<asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_emp_postal" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator>

</td><td class="style4" >Phone#</td><td class="style5"><asp:TextBox ID="txtbox_emp_phone" runat="server" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Phone Number" ValidationGroup="a"></asp:TextBox>

<asp:RegularExpressionValidator    ID="RegularExpressionValidator1" runat="server"  ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_emp_phone" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>

<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_emp_phone" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>

</td><td class="style6" >Cell#</td><td >
        <asp:TextBox ID="txtbox_emp_cell" runat="server" CssClass="twitterStyleTextbox" 
            placeholder="Enter the  Cell Number" ValidationGroup="a" 
   ></asp:TextBox>

<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
            ControlToValidate="txtbox_emp_cell" ErrorMessage="Enter Correct Format" 
            ForeColor="#FF3300" 
            ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
            ValidationGroup="b"></asp:RegularExpressionValidator>

<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
            Enabled="True" TargetControlID="txtbox_emp_cell" WatermarkCssClass="watermark" 
            WatermarkText="Enter In Format (123)123-4567"></cc1:TextBoxWatermarkExtender>

</td></tr></table>--%>





<asp:Panel ID="Panel11" runat="server" Width="718px">
    <fieldset><legend>Employee Detail</legend>
        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
        <ContentTemplate>
<table class="form-table" style="width: 699px">
    <tr><td class="style12" style="width: 77px" >Employee ID </td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="#CC0000"></asp:Label></td>
        <td class="style3">
            <asp:TextBox ID="txtboxcode_employeecode" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Employee Code" 
        ontextchanged="txtboxcode_employeecode_TextChanged" AutoPostBack="True" Enabled="False"></asp:TextBox>
            
</td>
        <td class="style3"><asp:RequiredFieldValidator ID="rfv_code" runat="server" 
                ControlToValidate="txtboxcode_employeecode" ErrorMessage="*" 
                ForeColor="#CC0000" ValidationGroup="m"></asp:RequiredFieldValidator></td><td class="style11">&nbsp;</td>
        <td class="style11">
            &nbsp;</td>
        <td class="style5">

        <asp:Label ID="lblerror" runat="server" ForeColor="#FF3300" Visible="False"></asp:Label>

</td><td class="style14">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style12" style="width: 77px">
            Name</td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label17" runat="server" ForeColor="#CC0000" Text="*"></asp:Label>
        </td>
        <td class="style3">
           <asp:TextBox ID="txtbox_emp_firstname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  First Name"></asp:TextBox>
            
        </td>
        <td class="style3">
           <asp:RequiredFieldValidator ID="rfv_code0" runat="server" 
                ControlToValidate="txtbox_emp_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator></td>
        <td class="style11">
           <asp:Label ID="Label19" runat="server" ForeColor="#CC0000" Text="*"></asp:Label></td>
        <td class="style11">
            
            Email</td>
        <td class="style5">
           <asp:TextBox ID="txtBox_emp_emailid" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Email Id" 
                ValidationGroup="a"></asp:TextBox>
            
        </td>
        <td class="style14">
        <asp:RequiredFieldValidator ID="rfv_code2" runat="server" 
                ControlToValidate="txtBox_emp_emailid" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ErrorMessage="Enter Correct Format" ControlToValidate="txtBox_emp_emailid" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
    </tr>
    <tr><td class="style12" style="width: 77px" >Address</td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label18" runat="server" ForeColor="#CC0000" Text="*"></asp:Label>
        </td>
        <td class="style3">
            <asp:TextBox ID="txtBox_emp_address1" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the St#" 
                    ValidationGroup="a"></asp:TextBox>

            

</td><td class="style3">
           <asp:RequiredFieldValidator ID="rfv_code1" runat="server" 
                ControlToValidate="txtBox_emp_address1" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator></td><td class="style11" >
            <asp:Label ID="Label20" runat="server" ForeColor="#CC0000" Text="*"></asp:Label></td>
        <td class="style11">
            
            Cell#</td>
        <td class="style5">
            <asp:TextBox ID="txtbox_emp_cell" runat="server" CssClass="twitterStyleTextbox" 
            placeholder="Enter the  Cell Number" ValidationGroup="a"></asp:TextBox>


            


</td><td class="style14" >
<asp:RequiredFieldValidator ID="rfv_code3" runat="server" 
                ControlToValidate="txtbox_emp_cell" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
            ControlToValidate="txtbox_emp_cell" ErrorMessage="Enter Correct Format" 
            ForeColor="#FF3300" 
            ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
            ValidationGroup="b"></asp:RegularExpressionValidator></td>
    </tr>
    </table>
    </ContentTemplate>
     <Triggers>
      
         <asp:AsyncPostBackTrigger ControlID="txtboxcode_employeecode" EventName="TextChanged" />
         
                </Triggers>
        </asp:UpdatePanel>
    </fieldset>
    </asp:Panel>
                 <table class="form-table" style="width: 1252px" ><tr><td class="style7" >
                         <asp:Button ID="btnclose1" runat="server" CssClass="button" 
                onclick="btnclose1_Click" style="text-align: center" Text="Close" />

</td><td><asp:Button ID="btn_save" runat="server" Text="Update" CssClass="button" 
                  onclick="btn_save_Click" />

</td></tr></table>


  </div>

    </div>

    </div>

        <div class="footer-container"></div>
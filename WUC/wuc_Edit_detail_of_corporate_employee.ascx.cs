﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
public partial class wuc_Edit_detail_of_caorporate_employee : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                DataSet dsdetails = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where Corporate_EmployeeID="+Convert.ToInt32(Session["empid_edit"].ToString()));
                    if(dsdetails.Tables[0].Rows.Count>0)
                    {
                        txtboxcode_employeecode.Text = dsdetails.Tables[0].Rows[0]["EmployeeCode"].ToString();
                        txtbox_emp_cell.Text=dsdetails.Tables[0].Rows[0]["Cell"].ToString();
                        txtbox_emp_firstname.Text = dsdetails.Tables[0].Rows[0]["Name"].ToString();
                        //txtbox_emp_middlename.Text = dsdetails.Tables[0].Rows[0]["M_Name"].ToString();
                        //txtbox_emp_lastname.Text = dsdetails.Tables[0].Rows[0]["L_Name"].ToString();
                        txtBox_emp_address1.Text=dsdetails.Tables[0].Rows[0]["Address"].ToString();
                        //txtBox_emp_address2.Text = dsdetails.Tables[0].Rows[0]["Address2"].ToString();
                        txtBox_emp_emailid.Text=dsdetails.Tables[0].Rows[0]["Emailid"].ToString();
                        //txtbox_emp_postal.Text = dsdetails.Tables[0].Rows[0]["PostalCode"].ToString();
                        //txtbox_emp_phone.Text = dsdetails.Tables[0].Rows[0]["Phone"].ToString();

                        //ddl_Parkercountry.SelectedValue = dsdetails.Tables[0].Rows[0]["CountryID"].ToString();
                        //ddlstate.SelectedValue = dsdetails.Tables[0].Rows[0]["ProvinceiD"].ToString();
                        //ddl_Parkercity.SelectedValue = dsdetails.Tables[0].Rows[0]["CityID"].ToString();
                    }
            }
            catch
            {
            }
        }

    }

    public void updatedetails()
    {

        try
        {
            hst.Clear();
            
//            Emailid=@emailid,Name=@fname,  
//[Address]=@address1,Cell=@cell where Corporate_EmployeeID=@employeeid
            hst.Add("action", "updatedetails");
            hst.Add("employeeid", Convert.ToInt32(Session["empid_edit"].ToString()));
            hst.Add("emailid", txtBox_emp_emailid.Text);
            hst.Add("fname", txtbox_emp_firstname.Text);
            hst.Add("address1", txtBox_emp_address1.Text);
            hst.Add("cell", txtbox_emp_cell.Text);
      
            int result = objData.ExecuteNonQuery("[Sp_employeeDetailsCorporate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               

            }
        }
        catch
        {
        }
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            updatedetails();
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('EditDetailsOfEmploy.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

        }
        catch
        {
        }
    }

    protected void btnclose1_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('EditDetailsOfEmploy.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

    }
    protected void ddl_Parkercity_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void SqlDataSource6_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void txtboxcode_employeecode_TextChanged(object sender, EventArgs e)
    {

    }
}
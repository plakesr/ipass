﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_FillDetail_of_Corporate.ascx.cs" Inherits="wuc_FillDetail_of_Corporate" %>
<%--<asp:Panel ID="pfill_skip" runat="server" Width="1262px" >
        <fieldset>
        <table>
        <tr>
        <td style="width: 189px">
            <asp:RadioButton ID="rbtn_filldetail" runat="server"  GroupName="r" 
                Text="Fill Details" oncheckedchanged="rbtn_filldetail_CheckedChanged" 
                AutoPostBack="True" />
        </td>
        <td style="width: 723px">
            <asp:RadioButton ID="rbtn_skip" runat="server" GroupName="r" 
                Text="Skip this Step" AutoPostBack="True" 
                oncheckedchanged="rbtn_skip_CheckedChanged"  />
        </td>
        </tr>
        </table>
        </fieldset>
        </asp:Panel>--%>
        <asp:Panel ID="panel_reserved" runat="server" Visible="False">
        <fieldset><legend>Reserved Parking</legend>
        <asp:Repeater ID="rpt_reserved" runat="server" 
                onitemcommand="rpt_reserved_ItemCommand"  >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Number Of Parker
                    </th>

                     <th>
                 Plan
                    </th>
                   <%-- <th>Plan
                    </th>--%>
                    <th>
                  Total Amount
                    </th>
                   
                    <th>
                       Fill Detail Of Parker
                    </th>
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("Total_Parker")%></td>
              <asp:Label ID="totalparker" runat="server" Text='<%# Eval("Total_Parker") %>' 
                                        Visible="False"></asp:Label>
  <asp:Label ID="username" runat="server" Text='<%# Eval("Username") %>' 
                                        Visible="False"></asp:Label>
 <asp:Label ID="Planid" runat="server" Text='<%# Eval("Planid") %>' 
                                        Visible="False"></asp:Label>
 
               <td>

            <%#Eval("plandetail")%></td>
                   <td>
            <asp:Label ID="Label2" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>
            <%#Eval("TotalAmount")%></td>
           
           <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="fill" CssClass="button">Fill Detail</asp:LinkButton>
                    </td>
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
        
        </fieldset>
        </asp:Panel>
        <br />
        <asp:Panel ID="panel_random" runat="server" Visible="False">
        <fieldset><legend>Random Parking</legend>
        <asp:Repeater ID="rpt_random" runat="server" 
                onitemcommand="rpt_random_ItemCommand"  >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Number Of Parker
                    </th>

                     <th>
                 Plan
                    </th>
                   <%-- <th>Plan
                    </th>--%>
                    <th>
                  Total Amount
                    </th>
                   
                    <th>
                       Fill Detail Of Parker
                    </th>
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("Total_Parker")%></td>
              <asp:Label ID="totalparker" runat="server" Text='<%# Eval("Total_Parker") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="username" runat="server" Text='<%# Eval("Username") %>' 
                                        Visible="False"></asp:Label>
  <asp:Label ID="Planid" runat="server" Text='<%# Eval("Planid") %>' 
                                        Visible="False"></asp:Label>
 <%--<td>

            <%#Eval("Plan")%></td>--%>

               <td>

            <%#Eval("plandetail")%></td>
                   <td>
            <asp:Label ID="Label2" runat="server" Text='<%# Eval("TotalAmount") %>' 
                                        Visible="False"></asp:Label>
            <%#Eval("TotalAmount")%></td>
           
           <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="fill" CssClass="button">Fill Detail</asp:LinkButton>
                    </td>
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
        
        </fieldset>
        </asp:Panel>
<asp:Button ID="btn_Finish" runat="server" Text="Finish" CssClass="button" 
    onclick="btn_Finish_Click" />

&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn_skip" runat="server" Text="Skip"  CssClass="button" 
    onclick="btn_skip_Click"/>



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class wuc_FillDetail_of_Corporate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                panel_reserved.Visible = true;
                DataTable reserved = (DataTable)Session["Chargetable"];

                rpt_reserved.DataSource = reserved;
                rpt_reserved.DataBind();
                panel_random.Visible = true;
                DataTable random = (DataTable)Session["ChargetableRandom"];
                rpt_random.DataSource = random;
                rpt_random.DataBind();
            }
            catch
            {
            }
        }
    }
    protected void rbtn_filldetail_CheckedChanged(object sender, EventArgs e)
    {
       
    }
    protected void rbtn_skip_CheckedChanged(object sender, EventArgs e)
    {
       

    }
    protected void rpt_reserved_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            int i = e.Item.ItemIndex;
            if (e.CommandName == "fill")
            {

                Label totalparker1 = rpt_reserved.Items[i].FindControl("totalparker") as Label;
                Label PlanIDforParker_Corporate1 = rpt_reserved.Items[i].FindControl("Planid") as Label;

                Label username1 = rpt_reserved.Items[i].FindControl("username") as Label;
               

                DataSet filldetail_no = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32( PlanIDforParker_Corporate1.Text));
                if (filldetail_no.Tables[0].Rows.Count < Convert.ToInt32(totalparker1.Text))
                {
                    int l = Convert.ToInt32(totalparker1.Text) - filldetail_no.Tables[0].Rows.Count;
                    Session["corporate_username"] = username1.Text;
                    Session["totalparker"] = l;
                    Session["PlanIDforParker_Corporate"] = PlanIDforParker_Corporate1.Text;

                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Add_Details_Of_Corporate_Parker.aspx','New Windows','height=1000, width=1400,location=no');", true);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('All Parker added.')", true);


                }








                //if (Session["numberofparkerremaining"]!=null)
                //{
                //    if (Session["numberofparkerremaining"].ToString() != "0")
                //    {
                //        Label totalparker = rpt_reserved.Items[i].FindControl("totalparker") as Label;
                //        Label PlanIDforParker_Corporate = rpt_reserved.Items[i].FindControl("Planid") as Label;

                //        Label username = rpt_reserved.Items[i].FindControl("username") as Label;
                //        Session["corporate_username"] = username.Text;
                //        Session["totalparker"] = totalparker.Text;
                //        Session["PlanIDforParker_Corporate"] = PlanIDforParker_Corporate.Text;

                //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Add_Details_Of_Corporate_Parker.aspx','New Windows','height=600, width=900,location=no');", true);
                //    }
                //    if (Session["numberofparkerremaining"].ToString() == "0")
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('All Parker added.')", true);

                //    }
                //}
                //if (Session["numberofparkerremaining"] == null)
                //{

                //    Label totalparker = rpt_reserved.Items[i].FindControl("totalparker") as Label;
                //    Label PlanIDforParker_Corporate = rpt_reserved.Items[i].FindControl("Planid") as Label;

                //    Label username = rpt_reserved.Items[i].FindControl("username") as Label;
                //    Session["corporate_username"] = username.Text;
                //    Session["totalparker"] = totalparker.Text;
                //    Session["PlanIDforParker_Corporate"] = PlanIDforParker_Corporate.Text;
                //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Add_Details_Of_Corporate_Parker.aspx','New Windows','height=600, width=900,location=no');", true);

                //}

            }
        }
        catch
        {
        }
    }
    protected void rpt_random_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        try
        {
            int i = e.Item.ItemIndex;
            if (e.CommandName == "fill")
            {



                Label totalparker1 = rpt_random.Items[i].FindControl("totalparker") as Label;
                Label PlanIDforParker_Corporate1 = rpt_random.Items[i].FindControl("Planid") as Label;

                Label username1 = rpt_random.Items[i].FindControl("username") as Label;


                DataSet filldetail_no = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(PlanIDforParker_Corporate1.Text));
                if (filldetail_no.Tables[0].Rows.Count < Convert.ToInt32(totalparker1.Text))
                {
                    int l = Convert.ToInt32(totalparker1.Text) - filldetail_no.Tables[0].Rows.Count;
                    Session["corporate_username"] = username1.Text;
                    Session["totalparker"] = l;
                    Session["PlanIDforParker_Corporate"] = PlanIDforParker_Corporate1.Text;

                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Add_Details_Of_Corporate_Parker.aspx','New Windows','height=1000, width=1400,location=no');", true);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('All Parker added.')", true);


                }

                //if (Session["numberofparkerremaining"] != null)
                //{
                //    if (Session["numberofparkerremaining"].ToString() != "0")
                //    {
                //        Label totalparker = rpt_reserved.Items[i].FindControl("totalparker") as Label;

                //        Label username = rpt_reserved.Items[i].FindControl("username") as Label;
                //        Session["corporate_username"] = username.Text;
                //        Session["totalparker"] = totalparker.Text;

                //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Add_Details_Of_Corporate_Parker.aspx','New Windows','height=600, width=900,location=no');", true);
                //    }
                //    if (Session["numberofparkerremaining"].ToString() == "0")
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('All Parker added.')", true);

                //    }
                //}
                //if (Session["numberofparkerremaining"] == null)
                //{
                //    Label totalparker = rpt_reserved.Items[i].FindControl("totalparker") as Label;
                //    Label PlanIDforParker_Corporate = rpt_reserved.Items[i].FindControl("Planid") as Label;

                //    Label username = rpt_reserved.Items[i].FindControl("username") as Label;
                //    Session["corporate_username"] = username.Text;
                //    Session["totalparker"] = totalparker.Text;
                //    Session["PlanIDforParker_Corporate"] = PlanIDforParker_Corporate.Text;
                //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Add_Details_Of_Corporate_Parker.aspx','New Windows','height=600, width=900,location=no');", true);

                //}

            }
        }
        catch
        {
        }
    }
    protected void btn_Finish_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["check"] == "1")
            {
                Session["random"] = null;
                Session["reserved"] = null;
            }
        }
        catch
        {
        }
        try
        {

            Session["corporate_username"] = null;
            Session["totalparker"] = null;
            Session["PlanIDforParker_Corporate"] = null;
            Session["Chargetable"] = null;
            Session["ChargetableRandom"] = null;
            Session["lotid"] = null;
        }
        catch
        {
        }
     
        if (Session["Type"].ToString() == "admin")
        {
            Response.Redirect("Admin_Search_Corporate_Parker.aspx");
        }
        if (Session["Type"].ToString() == "operator")
        {
           
            Response.Redirect("Admin_Search_Corporate_Parker.aspx");

        }

    }
    protected void btn_skip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["check"] == "1")
            {
                Session["random"] = null;
                Session["reserved"] = null;
            }
        }
        catch
        {
        }

        try
        {

            Session["corporate_username"] = null;
            Session["totalparker"] = null;
            Session["PlanIDforParker_Corporate"] = null;
            Session["Chargetable"] = null;
            Session["ChargetableRandom"] = null;
            Session["lotid"] = null;
        }
        catch
        {
        }
            if (Session["Type"].ToString() == "admin")
            {
               
                Response.Redirect("Admin_Search_Corporate_Parker.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                
                Response.Redirect("Operator_Corporate_Registration.aspx");
            }
       
        

    }
}
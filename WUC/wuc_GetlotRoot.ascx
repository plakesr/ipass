﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_GetlotRoot.ascx.cs" Inherits="WUC_wuc_GetlotRoot" %>


   <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script language="javascript" type="text/javascript">
        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();

        function InitializeMap() {
            directionsDisplay = new google.maps.DirectionsRenderer();
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions =
        {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
            var map = new google.maps.Map(document.getElementById("map"), myOptions);

            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('directionpanel'));

            var control = document.getElementById('control');
            control.style.display = 'block';


        }



        function calcRoute() {

            var start = document.getElementById('autocomlplete').value;
            var end = '<%= Session["address"] %>';
            var request = {
                origin: start,
                destination: end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

        }



        function Button1_onclick() {
            calcRoute();
        }

        window.onload = InitializeMap;
    </script>  
    <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
<div class="clearfix"></div>
<table id ="control" class="form-table">
<tr>
<td>
<table>
<tr>
<td>Find The Way to the Lot </td>
<td>
    <input id="autocomlplete" type="text" style="width: 305px"  onFocus="geolocate()" /></td>
</tr>
<tr>
<td>Lot Address</td>
<td>
    <asp:TextBox ID="endvalue" style="width: 301px" runat="server" Enabled="false"></asp:TextBox>
    <%--<input id="endvalue" type="text" style="width: 301px" />--%>
</td>
</tr>
<tr>
<td align ="right" >
    <input id="Button1" type="button" class="button" value="GetDirections" onclick="return Button1_onclick()" /></td>
</tr>
<tr><td colspan="2"><div id ="directionpanel"  style="height: 390px;overflow: auto" ></div></td></tr>
</table>

<td valign="top"><div id ="map" style="height: 390px; width: 489px"></div></td>
</td>
</tr>

</table>
</div>
</div>


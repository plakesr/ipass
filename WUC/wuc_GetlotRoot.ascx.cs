﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class WUC_wuc_GetlotRoot : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int id = int.Parse(Request.QueryString["lot_id"].ToString());
            if (id > 0)
            {

                DataTable dt = this.GetData("select tbl_LotMaster.*,tbl_City.City_name from tbl_LotMaster inner join tbl_City on tbl_LotMaster.City=tbl_City.City_id where tbl_LotMaster.Lot_id='" + id + "'");

                if (dt.Rows.Count > 0)
                {
                    endvalue.Text = dt.Rows[0]["Address"].ToString() + "," + dt.Rows[0]["City_name"].ToString();
                    Session["address"] = dt.Rows[0]["Address"].ToString() + "," + dt.Rows[0]["City_name"].ToString();
                   
                    try
                    {
                        
                    }
                    catch
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No route available.')", true);
                    }
                   
                }
            }
        }
        catch
        {
            try
            {
                if (!IsPostBack == true)
                {
                    endvalue.Text = Session["address"].ToString();
                }
            }
            catch
            {
                Response.Redirect("LotAddresssearch.aspx");
            }
        }
    }

    private DataTable GetData(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        SqlCommand cmd = new SqlCommand(query);
        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;

                sda.SelectCommand = cmd;
                using (DataTable dt = new DataTable())
                {
                    sda.Fill(dt);
                    return dt;
                }
            }
        }
    }
}
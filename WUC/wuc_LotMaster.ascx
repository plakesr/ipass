﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_LotMaster.ascx.cs" Inherits="WUC_wuc_LotMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
 
<style>
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style1
    {
        width: 341px;
    }
    .style2
    {
        width: 360px;
    }
    .style4
    {
        width: 240px;
    }
    .style5
    {
        width: 248px;
    }
    .style6
    {
        width: 295px;
    }
    .form-table
    {
        width: 872px;
    }
    .style12
    {
        width: 159px;
    }
    .style14
    {
        width: 283px;
    }
    .style17
    {
        width: 304px;
    }
    .style19
    {
        width: 384px;
    }
    .style21
    {
        width: 381px;
    }
    .style23
    {
        width: 321px;
    }
    .style28
    {
        width: 485px;
    }
    .style29
    {
        width: 232px;
    }
    .style30
    {
        height: 21px;
    }
    .style32
    {
        width: 456px;
    }
    .style35
    {
        width: 179px;
    }
    .style36
    {
        width: 49px;
    }
    .registration-area .Clicked{ text-decoration: none;
   border-radius:5px 5px 0 0;

  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; padding:8px 20px 7px;}
  
  .registration-area .Initial{text-decoration: none;
   border-radius:5px 5px 0 0;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #F9F9F9;
  color:#707070; padding:8px 20px; margin:0px;}
</style>

<div class="registration-area">

<asp:UpdatePanel ID="UpdatePanel3" runat="server">
<ContentTemplate>

    <asp:Label ID="Label14" runat="server" style="text-align:right; float:right" Text="* Represents Mandatory Fields" ForeColor="Red" ></asp:Label>
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="3" >
    <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"  CssClass="Clicked"><HeaderTemplate>General Details</HeaderTemplate><ContentTemplate>
    
        <asp:Panel ID="Panel8" runat="server">
        <fieldset>
    <table style="width: 100%; height: 200px;"><tr><td><span><table class="form-table"><tr><td class="style2">Customer Name</td><td><asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate><asp:DropDownList ID="ddt_sitename" runat="server" 
                                              CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource1" 
                                              DataTextField="SiteName" DataValueField="SiteId" ValidationGroup="a" 
                                              AutoPostBack="True" ></asp:DropDownList></ContentTemplate></asp:UpdatePanel><asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                              ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                              SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]"></asp:SqlDataSource></td></tr><tr><td class="style2">Campus Name</td><td><asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
            <asp:DropDownList ID="ddl_campusname" runat="server" 
                                              CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource5" 
                                              DataTextField="Campus_Name" 
                DataValueField="CampusID" Height="37px"  ValidationGroup="a" Width="240px" ></asp:DropDownList></ContentTemplate></asp:UpdatePanel><asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                              ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                              
                                              SelectCommand="SELECT * FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID)"><SelectParameters><asp:ControlParameter ControlID="ddt_sitename" Name="SiteID" 
                                                      PropertyName="SelectedValue" Type="Int64" /></SelectParameters></asp:SqlDataSource></td></tr><tr><td class="style2">Lot&nbsp;&nbsp; Name<asp:Label ID="Label8" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td>
            <asp:TextBox ID="txtbox_lotname" runat="server" 
                                              ontextchanged="txtbox_name_TextChanged" 
                ValidationGroup="m" CssClass="twitterStyleTextbox" AutoPostBack="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_LotName" runat="server" 
                                              ControlToValidate="txtbox_lotname" 
                ErrorMessage="Enter Lot Name" ForeColor="#CC0000" Display="Dynamic" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
            <asp:Label ID="lbl_exist" runat="server" Visible="False"></asp:Label>
            </td></tr><tr><td class="style2">Lot&nbsp;&nbsp; Code<asp:Label ID="Label9" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td><asp:TextBox ID="txtbox_lotcode" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox><span>
            <asp:RequiredFieldValidator ID="rfv_Lotcode" runat="server" 
                                              ControlToValidate="txtbox_lotcode" 
                ErrorMessage="Enter Lot Code" ForeColor="#CC0000" Display="Dynamic" 
                ValidationGroup="a"></asp:RequiredFieldValidator></span></td></tr><tr><td class="style2">Effective Date<asp:Label ID="Label10" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td><asp:TextBox ID="txtbox_date" runat="server" ValidationGroup="m" CssClass="twitterStyleTextbox"></asp:TextBox><cc1:CalendarExtender ID="txtbox_date_CalendarExtender" runat="server" 
                                              Enabled="True" TargetControlID="txtbox_date"></cc1:CalendarExtender><span>
            <asp:RequiredFieldValidator ID="rfv_effectivedate" runat="server" 
                                              ControlToValidate="txtbox_date" 
                ErrorMessage="Enter Effective Date" ForeColor="#CC0000" Display="Dynamic" 
                ValidationGroup="a"></asp:RequiredFieldValidator></span></td></tr><tr><td class="style2">Time Zone</td><td><asp:DropDownList ID="ddl_timezone" runat="server" CssClass="twitterStyleTextbox" ></asp:DropDownList></td></tr><tr><td class="style2">Lot Hours<asp:Label ID="Label11" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td>
            <asp:TextBox ID="txtbox_hours" runat="server" ValidationGroup="m" 
                CssClass="twitterStyleTextbox" ontextchanged="txtbox_hours_TextChanged"></asp:TextBox>
       
        <span><asp:RequiredFieldValidator ID="rfv_lothours" runat="server" 
                                              ControlToValidate="txtbox_hours" 
            ErrorMessage="Enter Lot Hours" ForeColor="#CC0000" Display="Dynamic" 
            ValidationGroup="a"></asp:RequiredFieldValidator></span></td></tr>
            
            
            
            
            
                <tr>
                <td></td>
                
                <td> 
            <asp:Button 
                                  ID="btn1_next" runat="server" CssClass="button" Height="35px" 
                                  onclick="btn1_next_Click" Text="Next" Width="79px" /></td></tr>
        <caption>
            </caption>
        </table><br /><br /><br /><br /><br /><br /></span></td></tr></table>
        </fieldset>
        </asp:Panel>
        </ContentTemplate></cc1:TabPanel>
   
    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2"  CssClass="Initial"><HeaderTemplate>Contact Details</HeaderTemplate><ContentTemplate><asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate>
    
        <asp:Panel ID="Panel7" runat="server">
        <fieldset>
    <table ><tr><td class="style2"><br /><br /><table class="form-table"><tr>
        <td class="style12">Select GeoLocation</td><td class="style1"><asp:Button ID="Button1" runat="server" onclick="Button1_Click1" 
                        Text="Get Geo-Location" CssClass="button" /></td><td rowspan="4"><asp:Label ID="lbladdress" runat="server" Text="Address" Visible="false"></asp:Label><asp:TextBox ID="txtadd" runat="server" Visible="false" CssClass="twitterStyleTextbox"></asp:TextBox><br /><br /><asp:Label ID="lbllatitude" runat="server" Text="Latitude" Visible="false"></asp:Label><asp:TextBox ID="txtlati" runat="server" Visible="false" CssClass="twitterStyleTextbox"></asp:TextBox><br /><br /><br /><br /><asp:Label ID="lbllongitude" runat="server" Text="Longitude" Visible="false"></asp:Label><asp:TextBox ID="txtlongi" runat="server" Visible="false" CssClass="twitterStyleTextbox"></asp:TextBox></td></tr><tr>
        <td class="style12">City</td><td class="style1"><asp:DropDownList ID="ddl_city" runat="server" DataSourceID="SqlDataSource2" 
                        DataTextField="City_name" DataValueField="City_id" CssClass="twitterStyleTextbox"></asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        
            SelectCommand="SELECT [City_id], [City_name] FROM [tbl_City] ORDER BY [City_name]"></asp:SqlDataSource></td></tr><tr>
            <td class="style12">Postal Code/Zip Code<asp:Label ID="Label4" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_zip" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="rfv_postalcode" runat="server" 
                ControlToValidate="txtbox_zip" ErrorMessage="Enter Postal Code" 
                ForeColor="#CC0000" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_zip" 
                 
                ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                ForeColor="#CC0000" ValidationGroup="b" ></asp:RegularExpressionValidator>
            </td></tr><tr>
        <td class="style12">Cellular<asp:Label ID="Label5" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_cell" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ErrorMessage="Enter Cell number" ControlToValidate="txtbox_cell" 
                ValidationGroup="a" ForeColor="#CC0000" Display="Dynamic"></asp:RequiredFieldValidator></td></tr><tr>
        <td class="style12">Office No<asp:Label ID="Label6" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_office" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
        
        <asp:RegularExpressionValidator
                                                           
                ID="RegularExpressionValidator1" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_office" ValidationGroup="b" 
                ForeColor="#CC0000"  ></asp:RegularExpressionValidator><br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ErrorMessage="Enter Office Number" ControlToValidate="txtbox_office" 
                ValidationGroup="a" ForeColor="#CC0000" Display="Dynamic"></asp:RequiredFieldValidator></td></tr><tr>
        <td class="style12">E-mail<asp:Label ID="Label7" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style1"><asp:TextBox ID="txtbox_email" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ErrorMessage="Enter Email" ControlToValidate="txtbox_email" ValidationGroup="a" 
                ForeColor="#CC0000" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="rev_email" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="Format is wrong" 
                        ForeColor="#CC0000"   
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                ValidationGroup="b" ></asp:RegularExpressionValidator></td><td>&#160;<br /><br /><br /><%-- <input id="txtLat" type="text" value="28.47399" />--%><%-- <input id="txtLat" type="text" value="28.47399" />--%></td></tr><tr><td></td><td>
        <asp:Button 
            ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" 
            CssClass="button" Height="35px" Width="79px" ValidationGroup="b"></asp:Button></td></tr></table>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
       </fieldset>
        </asp:Panel> 
        </ContentTemplate></asp:UpdatePanel></ContentTemplate></cc1:TabPanel>
    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3"  CssClass="Initial"><HeaderTemplate>Web Direction</HeaderTemplate><ContentTemplate><table class="form-table"><tr><td><h3>&nbsp;<asp:Panel ID="Panel4" runat="server" Height="227px" Width="705px"><fieldset><legend>Web Direction English</legend><table class="form-table"><tr><td class="style4">Entrance Description</td><td class="style5"><asp:TextBox ID="txtbox_enteranceinformationEnglish" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" Width="217px" 
                                                TextMode="MultiLine" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Exit Description</td><td class="style5"><asp:TextBox ID="txtbox_exitinformationEnglish" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Lot Restriction</td><td class="style5"><asp:TextBox ID="txtbox_restriction" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td>Lot Description</td><td class="style5"><asp:TextBox ID="txtbox_Description" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="221px" style="resize:none"></asp:TextBox></td></tr></table></fieldset></asp:Panel><br /><br /><br /><br /><br /><br /><asp:Panel ID="Panel5" runat="server"><fieldset><legend>Web Direction French</legend><table class="form-table"><tr><td class="style4">Entrance Description</td><td class="style5"><asp:TextBox ID="txtbox_enteranceinformationFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" Width="217px" 
                                                TextMode="MultiLine" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Exit Description</td><td class="style5"><asp:TextBox ID="txtbox_exitinformationFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td class="style4">Lot Restriction</td><td class="style5"><asp:TextBox ID="txtbox_restrictionFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" 
                                                ontextchanged="txtbox_restrictionFrench_TextChanged" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr><tr><td>Lot Description</td><td class="style5"><asp:TextBox ID="txtbox_DescriptionFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine" Width="217px" style="resize:none"></asp:TextBox></td></tr></table></fieldset> </asp:Panel>
                      
                      
                      
                      
                  </td></tr></table><br /><br />
        <asp:Panel ID="Panel6" runat="server">
        <fieldset>
                  
                  <table style="width: 881px"><tr>
            <td class="style23">Lot Type </td><td class="style6"><asp:DropDownList ID="ddl_lottype" runat="server" CssClass="twitterStyleTextbox" Width="239px"><asp:ListItem Value="1">Key Scan</asp:ListItem><asp:ListItem Value="2">HangTags</asp:ListItem></asp:DropDownList></td></tr><tr>
            <td class="style23">Other Details </td><td class="style6"><asp:TextBox ID="txtbox_otherdetailsweb" runat="server" Height="42px" CssClass="twitterStyleTextbox" 
                                    TextMode="MultiLine" Width="229px" style="resize:none"></asp:TextBox></td></tr></table>
                </fieldset>
        </asp:Panel><br /><br />
        <table>
        <tr>
                <td class="style23">
                <asp:Button ID="next3" runat="server" CssClass="button" Height="35px" 
                    OnClick="next3_Click" Text="Next" Width="79px" />
                </td><td>&nbsp;</td></tr>
        </table>
        <br /><br /><br /><br /><br />
       </ContentTemplate></cc1:TabPanel>
    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="TabPanel4"  CssClass="Initial"><HeaderTemplate>Billing Entry</HeaderTemplate>
    <ContentTemplate>
    
    <asp:Panel ID="Panel1" runat="server" >
    <fieldset>
    <table>
      <tr>
            <td class="style28">Billing Entry</td><td class="style14"><asp:DropDownList ID="ddl_billingEntry" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="1">First Month Full</asp:ListItem><asp:ListItem Value="2">Prorate</asp:ListItem><asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem></asp:DropDownList></td>
      </tr> 
       <tr>
        <td class="style28">Billing Exit</td><td class="style14"><asp:DropDownList ID="ddl_billingexit" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="1">First Month Full</asp:ListItem><asp:ListItem Value="2">Prorate</asp:ListItem><asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem></asp:DropDownList></td>
        </tr>
      <tr>
            <td class="style28">Payment Reminder Days<asp:Label ID="Label1" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label>
            </td><td class="style14"><asp:TextBox ID="txtbox_paymentreminderdays" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtbox_paymentreminderdays_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_paymentreminderdays" 
                FilterType="Numbers" >
            </cc1:FilteredTextBoxExtender><br />
            <asp:RequiredFieldValidator ID="rfv_reminderdays" runat="server" 
                ControlToValidate="txtbox_paymentreminderdays" 
                ErrorMessage="Enter Payment Reminder Days" ForeColor="#CC0000" 
                Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
      </tr>
      <tr>
            <td class="style28">Interest Rate Payment<asp:Label ID="Label2" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style14"><asp:TextBox ID="txtbox_interestrate" runat="server"  CssClass="twitterStyleTextbox"  onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_interestrate_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_interestrate"
  ValidChars="01234567890." >
                </cc1:FilteredTextBoxExtender>
          <br />  <asp:RequiredFieldValidator ID="rfv_ratepayment" runat="server" 
                ErrorMessage="Enter Interest Rate Payment" ForeColor="#CC0000" 
                ValidationGroup="a" Display="Dynamic" 
                ControlToValidate="txtbox_interestrate"></asp:RequiredFieldValidator>
            </td>
      </tr>
      <tr>
            <td class="style28">Late Fee($)<asp:Label ID="Label3" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td><td class="style14"><asp:TextBox ID="txtbox_latefee" runat="server"   CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_latefee_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_latefee"
  ValidChars="01234567890." >
                </cc1:FilteredTextBoxExtender>
           <br /><asp:RequiredFieldValidator ID="rfv_latefee" runat="server" 
                ControlToValidate="txtbox_latefee" ErrorMessage="Enter Late Fee" 
                ForeColor="#CC0000" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
      </tr>
      <tr>
      <td>
          Activation Charge<asp:Label ID="Label12" runat="server" 
                    ForeColor="#CC0000" Text="(*)"></asp:Label></td>
      <td>
          <asp:TextBox ID="txtbox_activationcharge" runat="server" style="height: 22px" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                    runat="server" Enabled="True" TargetControlID="txtbox_activationcharge"
  ValidChars="01234567890." >
                </cc1:FilteredTextBoxExtender>
           <br /><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                ControlToValidate="txtbox_latefee" ErrorMessage="Enter Activation Charge" 
                ForeColor="#CC0000" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
          </td>
      </tr>
      <tr>
            <td class="style28">Display Activation Charge</td><td class="style14">
            <asp:RadioButton ID="rbtn_yes" runat="server" GroupName="r"  
                oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rbtn_no" runat="server" GroupName="r" Text="No" /></td>
      </tr>
      <tr>
            <td class="style28">Display Deposit</td><td class="style14">
            <asp:RadioButton ID="rbtn_yes_deposite" runat="server" GroupName="r1" 
                oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rbtn_no_deposite" runat="server" GroupName="r1" Text="No" /></td>
      </tr>
      <tr>
            <td class="style28">Late Payment Action</td><td class="style14"><asp:DropDownList ID="ddl_latepaymentaction" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="0">Disable Card</asp:ListItem><asp:ListItem Value="1">Send Email by Notification</asp:ListItem></asp:DropDownList></td>
      </tr>
      <tr>
        <td class="style28">Credit Card Payment Failed</td><td class="style14"><asp:DropDownList ID="ddl_creditcardpayment" runat="server" CssClass="twitterStyleTextbox"><asp:ListItem Value="0">Disable Card</asp:ListItem><asp:ListItem Value="1">Send Email by Notification</asp:ListItem></asp:DropDownList></td>
      </tr>
      <tr>
        <td class="style28">Transaction Type </td><td><asp:CheckBox ID="chk_monthly" 
              runat="server" Text="Monthly" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox ID="chk_daily" runat="server" Text="Daily" /></td> 
      </tr>

    </table> 
    </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel2" runat="server" >
  <fieldset>
    <table>
    <tr>
            <td class="style32">
                <asp:CheckBox ID="chbk_Reserved" runat="server" Text="Reserved" 
                    oncheckedchanged="chbk_Reserved_CheckedChanged"  AutoPostBack="True" /></td>
            <td class="style17">Total Space: <asp:TextBox ID="txtbox_reservedtotalspace" 
                    runat="server" Height="20px"  Width="91px" CssClass="twitterStyleTextbox" 
                     ontextchanged="txtbox_reservedtotalspace_TextChanged"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_reservedtotalspace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_reservedtotalspace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td class="style21">Unable Space: <asp:TextBox ID="txtbox_reservedunablespace" runat="server" Height="19px" Width="83px" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_reservedunablespace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_reservedunablespace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender><br />
            </td>
            <td class="style19"><asp:Button ID="Rate_for_reserevd" runat="server" Text="Rate_reserved" CssClass="button"  onclick="Rate_for_reserevd_Click" /></td>
            <td>
                <asp:Label ID="reserved" runat="server" Text="Label" Visible="False"></asp:Label></td>
    </tr>   
                                                
    <tr>
            <td class="style32">
                <asp:CheckBox ID="chbk_Random" runat="server"  Text="Random" 
                    oncheckedchanged="chbk_Random_CheckedChanged" AutoPostBack="True" /></td>
            <td class="style17">Total Space: <asp:TextBox ID="txtbox_randomtotalspace" runat="server" ontextchanged="TextBox2_TextChanged" Height="21px" Width="89px" CssClass="twitterStyleTextbox" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_randomtotalspace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_randomtotalspace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="txtbox_randomtotalspace" 
                    ErrorMessage="Enter Random Total Space" ForeColor="#CC0000" 
                    Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style21">Unable Space <asp:TextBox ID="txtbox_randomunablespace" runat="server" Height="20px"  Width="88px" CssClass="twitterStyleTextbox" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_randomunablespace_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_randomunablespace" 
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender><br />
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="txtbox_randomunablespace" 
                    ErrorMessage="Enter Random Unable Space" ForeColor="#CC0000" 
                    Display="Dynamic" ValidationGroup="a"></asp:RequiredFieldValidator>
                
                
            </td>
            <td class="style19"><asp:Button ID="_Random" runat="server" Text="Rate_Random"  CssClass="button" onclick="_Random_Click" /></td>
            <td>
                <asp:Label ID="random" runat="server" Text="Label" Visible="False"></asp:Label></td>
   </tr>
    </table>
    </fieldset>
     </asp:Panel>
     <br />
    <asp:Panel ID="Panel3" runat="server">
   <fieldset>
     <table style="width: 875px">
      <tr>
             <td colspan="3" class="style30">TAXES</td>
             </tr><tr><td class="style35"><span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Corporate Tax ID #<span class="Apple-converted-space">&nbsp;</span></span></td>
             <td class="style36"><asp:TextBox ID="txtbox_corporate_taxid" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfv_taxid" runat="server" 
                     ControlToValidate="txtbox_corporate_taxid" ErrorMessage="*" ForeColor="#CC0000" 
                     ValidationGroup="t"></asp:RequiredFieldValidator>
             </td>
             <td rowspan="5" class="style29" valign="top">
                 <asp:Repeater ID="Repeater1" runat="server" 
                     onitemcommand="Repeater1_ItemCommand">

                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                        Tax&nbsp;&nbsp;Name
                    </th>
                      <th>
                        Tax&nbsp;&nbsp;(%)
                    </th>
                     <th>
                        Sequence
                    </th>
                    
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>     <%#Eval("taxname")%></td>
                <td>    <%#Eval("tax")%></td>
                  <td>  <%#Eval("sequence")%>
                </td>
                
               
                <td>
               <asp:LinkButton ID="LinkButton1" runat="server" CommandName="remove" CssClass="button">Remove</asp:LinkButton>
      </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
                 </asp:Repeater>
</td>
      </tr>
      <tr>
              <td class="style35"><span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Tax Name</span></td>
              <td class="style36"><asp:TextBox ID="txtbox_taxname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfv_taxid0" runat="server" 
                      ControlToValidate="txtbox_taxname" ErrorMessage="*" ForeColor="#CC0000" 
                      ValidationGroup="t"></asp:RequiredFieldValidator>
              </td></tr><tr>
         <td class="style35"><span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">Tax %</span></td>
         <td class="style36"><asp:TextBox ID="txtbox_tax" runat="server"  CssClass="twitterStyleTextbox"   onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>
             <cc1:FilteredTextBoxExtender ID="txtbox_tax_FilteredTextBoxExtender" 
                 runat="server" Enabled="True" TargetControlID="txtbox_tax"   ValidChars="01234567890.">
             </cc1:FilteredTextBoxExtender>
             <asp:RequiredFieldValidator ID="rfv_taxid1" runat="server" 
                 ControlToValidate="txtbox_tax" ErrorMessage="*" ForeColor="#CC0000" 
                 ValidationGroup="t"></asp:RequiredFieldValidator>
             </td></tr><tr>
         <td class="style35"><span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">&#160;Sequence for Calculation</span></td>
              <td class="style36">
              <asp:DropDownList ID="ddl_squencecal" runat="server" 
                      CssClass="twitterStyleTextbox" Width="160px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        </asp:DropDownList>
               </td>
        </tr>
        <tr>
                <td class="style35">&#160;</td>
                <td class="style36">
                    <asp:Button ID="btn_tax" runat="server" Text="ADD TAX"  CssClass="button" 
                        onclick="btn_tax_Click" ValidationGroup="t" />
                </td>
        </tr>

     </table>
     </fieldset>
    </asp:Panel>
        <asp:Label ID="lbl_error" runat="server" ForeColor="#CC0000" 
            Text="* Parking Space Does not Matched !" Visible="False"></asp:Label>
    <asp:Button ID="btn_FINISH_FINAL" runat="server" Height="43px"  CssClass="button"  
            onclick="btn_FINISH_FINAL_Click" style="margin-top: 0px" Text="Finish" 
            ValidationGroup="a"/><br />
   <asp:validationsummary id="vsmSummary" runat="server" ForeColor="DarkRed" HeaderText="Please Fill the Mendentry fields"
			 Font-Bold="True" ValidationGroup="a" ShowMessageBox="True"></asp:validationsummary>

        </ContentTemplate>
                                  </cc1:TabPanel>
</cc1:TabContainer>
</ContentTemplate>
</asp:UpdatePanel>
</div>
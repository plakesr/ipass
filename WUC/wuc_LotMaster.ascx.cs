﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Drawing;

public partial class WUC_wuc_LotMaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            clsInsert onj = new clsInsert();
            DataSet rec = onj.fetchrec("SELECT [TimeZoneTblId], [TimeZoneName], [TimeZoneId] FROM [tbl_TimeZoneMaster] ORDER BY [TimeZoneId]");
            DataTable t = new DataTable("timezone");
            t.Columns.Add("TimeZoneTblId", typeof(int));
            t.Columns.Add("TimeZoneName", typeof(string));

            for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
            {
                DataRow dr = t.NewRow();
                dr[0] = rec.Tables[0].Rows[i][0].ToString();
                dr[1] = rec.Tables[0].Rows[i][2].ToString() + " [" + rec.Tables[0].Rows[i][1].ToString() + "]";
                t.Rows.Add(dr);
            }


            ddl_timezone.DataSource = t;
            ddl_timezone.DataTextField = "TimeZoneName";
            ddl_timezone.DataValueField = "TimeZoneTblId";
            ddl_timezone.DataBind();

           
            try
            {

                dt.Columns.Add("taxname", typeof(string));
                dt.Columns.Add("tax", typeof(decimal));
                dt.Columns.Add("sequence", typeof(int));
                dt.Columns.Add("corporatetaxid", typeof(string));



                Session["datatable"] = dt;
            }
            catch
            {

            }


            //if (Request.QueryString["message"] == "1")
            //{
            //    Session["datatable"] = null;
            //    Session["datatable_random"] = null;
            //    Session["datatable_reserved"] = null;
            //    Session["dataaddress"] = null;
            //    TabContainer1.ActiveTabIndex = 0;
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            //}
            try
            {
                if (Session["insert"].ToString() == "insertsuccess")
                {
                    Session["datatable"] = null;
                    Session["datatable_random"] = null;
                    Session["datatable_reserved"] = null;
                    Session["dataaddress"] = null;
                    Session["insert"] = null;
                    TabContainer1.ActiveTabIndex = 0;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                }
            }
            catch
            {
            }
            //for (int i = 1; i <= 5; i++)
            //{
            //    ListItem li = new ListItem();
            //    li.Text = i.ToString();
            //    li.Value = i.ToString();
            //    //ddl.Items.Add(li);
            //    ddl_squencecal.Items.Add(li);
            //}
        }
    }
    protected void ddt_spaceavailable_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btn_add_Click(object sender, EventArgs e)
    {

    }
   
    string approval = "true";
    string lotstatus = "true";
    protected void btn1_next_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;

    }

    public void first()
    {
        Hashtable h1 = new Hashtable();
        h1.Clear();
        h1.Add("siteid", ddt_sitename.SelectedItem.Value);
        h1.Add("campusid", ddl_campusname.SelectedItem.Value);
        h1.Add("Lotname", txtbox_lotname.Text);
        h1.Add("Lotcode", txtbox_lotcode.Text);
        h1.Add("Effctivedate", txtbox_date.Text);

        h1.Add("Approval", approval);
        h1.Add("Timezone", ddl_timezone.SelectedItem.Value);
        h1.Add("Lothours", txtbox_hours.Text);
        //if (txtbox_totalspace.Text == " ")
        //{
        //    h1.Add("Lottotalspace", 0);

        //}
        //else
        //{
        //    h1.Add("Lottotalspace", Convert.ToInt32(txtbox_totalspace.Text));
        //}
        //if (txtbox_unablespace.Text == " ")
        //{
        //    h1.Add("lotunablespace", 0);
        //}
        //else
        //{
        //    h1.Add("lotunablespace", Convert.ToInt32(txtbox_unablespace.Text));

        //}

        Session["hasht1"] = h1;

        
    }
    public void second()
    {
        Hashtable h2 = new Hashtable();
        h2.Clear();
        //h2.Add("address", address.Text);
        h2.Add("city", ddl_city.SelectedItem.Value);
        h2.Add("zip", txtbox_zip.Text);
        h2.Add("cellular", txtbox_cell.Text);
        h2.Add("officeno", txtbox_office.Text);

        h2.Add("email", txtbox_email.Text);
        //Session["lat"] = latitude.Text;
        //Session["long"] = longitude.Text;
        //h2.Add("latitude", Session["lat"]);
        //h2.Add("longitude", Session["long"]);
        Session["hasht2"] = h2;
    }
    protected void btn2_next_Click(object sender, EventArgs e)
    {


       
        TabContainer1.ActiveTabIndex = 2;

    }
    protected void next3_Click(object sender, EventArgs e)
    {
        
        TabContainer1.ActiveTabIndex = 3;
    }




    public void third()
    {
        Hashtable h3 = new Hashtable();
        h3.Clear();

        h3.Add("entryenglish", txtbox_enteranceinformationEnglish.Text);
        h3.Add("exitenglish", txtbox_exitinformationEnglish.Text);
        h3.Add("restrictionenglish", txtbox_restriction.Text);
        h3.Add("descriptionenglish", txtbox_Description.Text);
        h3.Add("entryfrench", txtbox_enteranceinformationFrench.Text);

        h3.Add("exitfrench", txtbox_exitinformationFrench.Text);
        h3.Add("restrictionfrench", txtbox_restrictionFrench.Text);
        h3.Add("descriptionfrench", txtbox_DescriptionFrench.Text);

        h3.Add("Lottype", ddl_lottype.SelectedItem.Value);
        h3.Add("otherdetails", txtbox_otherdetailsweb.Text);
        Session["hasht3"] = h3;
    }


    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void ddlvehiclechargetype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtbox_name_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataSet lot = new clsInsert().fetchrec("select LotName from tbl_LotMaster where Campus_id= " + ddl_campusname.SelectedValue);
            for (int i = 0; i < lot.Tables[0].Rows.Count; i++)
            {
                if (txtbox_lotname.Text == lot.Tables[0].Rows[i][0].ToString())
                {
                    lbl_exist.Visible = true;
                    lbl_exist.ForeColor = Color.Red;
                    lbl_exist.Text = "Lot name already exist";
                    txtbox_lotname.Text = "";
                    break;
                }
                else
                {
                    lbl_exist.Visible = false;
                }
            }
        }
        catch
        {
        }
    }
    protected void rbtn_yes_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_reserved_Amt_TextChanged(object sender, EventArgs e)
    {

    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    DataTable dt = new DataTable();
    protected void btn_add_Click1(object sender, EventArgs e)
    {


    }
  
    protected void btn_addtax_Click(object sender, EventArgs e)
    {



    }
    protected void Button1_Click(object sender, EventArgs e)
    {


    }
    protected void txtbox_restrictionFrench_TextChanged(object sender, EventArgs e)
    {

    }
    string displayactivation = "";
    string displaydeposit = "";
    string parking_reserved = "";
    string parking_random = "";
    int lot_reserved_totalspace = 0;
    int lot_reserved_unablespace = 0;
    int lot_random_totalspace = 0;
    int lot_random_unablespace = 0;

    string LotTransaction_Daily;
    string LotTransaction_Monthly;

    decimal defaultvalue = Convert.ToDecimal(0.00);
    int totalspace_lot_reserved = 0;
    int unablespace_lot_reserved = 0;
    int totalspace_lot_random = 0;
    int unablespace_lot_random = 0;
    int totalspace_lot = 0;
    int unablespace_lot = 0;
    int defaultvalue1 = 1;
    public void forth()
    {
        Hashtable h4 = new Hashtable();
        h4.Add("Billingentry", ddl_billingEntry.SelectedItem.Value);
        h4.Add("Billingexit", ddl_billingexit.SelectedItem.Value);
        if (txtbox_paymentreminderdays.Text != "")
        {
            h4.Add("paymentreminder", txtbox_paymentreminderdays.Text);
        }
        else
        {
            h4.Add("paymentreminder", defaultvalue1);

        }
        if (txtbox_paymentreminderdays.Text != "")
        {
            h4.Add("interestrate",Convert.ToDecimal( txtbox_interestrate.Text));
        }
        else
        {
            h4.Add("interestrate", defaultvalue);

        }

        if (txtbox_latefee.Text != "")
        {
            h4.Add("Latefee",Convert.ToDecimal( txtbox_latefee.Text));
        }
        else
        {
            h4.Add("Latefee", defaultvalue);

        }
        if (txtbox_activationcharge.Text != "")
        {

            h4.Add("activationcharge", Convert.ToDecimal(txtbox_activationcharge.Text));
        }
        else
        {
            h4.Add("activationcharge", Convert.ToDecimal(defaultvalue));

        }
        if (rbtn_yes.Checked == true)
        {
            displayactivation = "true";
            h4.Add("displayactivationcharge", displayactivation);
        }
        else if (rbtn_no.Checked == false)
        {
            displayactivation = "false";
            h4.Add("displayactivationcharge", displayactivation);
        }

        if (rbtn_yes_deposite.Checked == true)
        {
            displaydeposit = "true";
            h4.Add("displaydeposite", displaydeposit);
        }
        else if (rbtn_yes_deposite.Checked == false)
        {
            displaydeposit = "false";
            h4.Add("displaydeposite", displaydeposit);
        }
        int j = Convert.ToInt32(ddl_latepaymentaction.SelectedItem.Value);
        h4.Add("latepaymentaction", j);
        int k = Convert.ToInt32(ddl_creditcardpayment.SelectedItem.Value);
        h4.Add("creditcardpaymentaction", k);

        if (chk_monthly.Checked == true)
        {
            LotTransaction_Daily = "true";
            h4.Add("LotTransaction_Daily", LotTransaction_Daily);

        }
        else
        {
            LotTransaction_Daily = "false";
            h4.Add("LotTransaction_Daily", LotTransaction_Daily);
        }
        if (chk_daily.Checked == true)
        {
            LotTransaction_Monthly = "true";
            h4.Add("LotTransaction_Monthly", LotTransaction_Monthly);
        }
        else
        {
            LotTransaction_Monthly = "false";
            h4.Add("LotTransaction_Monthly", LotTransaction_Monthly);
        }
       

        if (chbk_Reserved.Checked == true)
        {
            parking_reserved = "true";
            h4.Add("tranasactiontype_reserved", parking_reserved);
            if (txtbox_reservedtotalspace.Text != "")
            {
                h4.Add("reserved_totalspace", Convert.ToInt32(txtbox_reservedtotalspace.Text));
            }
            if (txtbox_reservedtotalspace.Text == "")
            {
                h4.Add("reserved_totalspace", lot_reserved_totalspace);

            }
            if (txtbox_reservedunablespace.Text != "")
            {
                h4.Add("reserved_unablespace", Convert.ToInt32(txtbox_reservedunablespace.Text));
            }
            if (txtbox_reservedunablespace.Text == "")
            {
                h4.Add("reserved_unablespace", lot_reserved_unablespace);

            }
            if (txtbox_reservedtotalspace.Text != "")
            {

                totalspace_lot_reserved = Convert.ToInt32(txtbox_reservedtotalspace.Text);
            }
            if (txtbox_reservedtotalspace.Text == "")
            {
                totalspace_lot_reserved = Convert.ToInt32(lot_reserved_totalspace);

            }
            if (txtbox_reservedunablespace.Text != "")
            {

                unablespace_lot_reserved = Convert.ToInt32(txtbox_reservedunablespace.Text);
            }
            if (txtbox_reservedunablespace.Text == "")
            {
                unablespace_lot_reserved = Convert.ToInt32(lot_reserved_unablespace);

            }

            //unablespace_lot_reserved = Convert.ToInt32(lot_reserved_unablespace);
        }
        else
        {
            parking_reserved = "false";
            h4.Add("tranasactiontype_reserved", parking_reserved);
            h4.Add("reserved_totalspace", lot_reserved_totalspace);
            h4.Add("reserved_unablespace", lot_reserved_unablespace);
            totalspace_lot_reserved = lot_reserved_totalspace;
            unablespace_lot_reserved = lot_reserved_unablespace;
        }



        if (chbk_Random.Checked == true)
        {
            parking_random = "true";
            h4.Add("tranasactiontype_random", parking_random);

            if (txtbox_randomtotalspace.Text != "")
            {

                h4.Add("random_totalspace", Convert.ToInt32(txtbox_randomtotalspace.Text));

            }
            if (txtbox_randomtotalspace.Text == "")
            {
                h4.Add("random_totalspace", Convert.ToInt32(lot_random_totalspace));


            }
            if (txtbox_randomunablespace.Text != "")
            {

                h4.Add("random_unablespace", Convert.ToInt32(txtbox_randomunablespace.Text));
               
            }
            if (txtbox_randomunablespace.Text == "")
            {
                h4.Add("random_unablespace", Convert.ToInt32(lot_random_unablespace));


            }


            if (txtbox_randomtotalspace.Text != "")
            {

                totalspace_lot_random = Convert.ToInt32(txtbox_randomtotalspace.Text);
            }
            if (txtbox_randomtotalspace.Text == "")
            {
                totalspace_lot_random = Convert.ToInt32(lot_random_totalspace);

            }
            if (txtbox_randomunablespace.Text != "")
            {

                unablespace_lot_random = Convert.ToInt32(txtbox_randomunablespace.Text);
            }
            if (txtbox_randomunablespace.Text == "")
            {
                unablespace_lot_random = Convert.ToInt32(lot_random_unablespace);

            }





            //totalspace_lot_random = Convert.ToInt32(txtbox_randomtotalspace.Text);
            //unablespace_lot_random = Convert.ToInt32(txtbox_randomunablespace.Text);
        }
        else
        {
            parking_random = "false";
            h4.Add("tranasactiontype_random", parking_random);
            h4.Add("random_totalspace", lot_random_totalspace);
            h4.Add("random_unablespace", lot_random_unablespace);
            totalspace_lot_random = lot_random_totalspace;
            unablespace_lot_random = lot_random_unablespace;
        }
        totalspace_lot = totalspace_lot_random + totalspace_lot_reserved;
        unablespace_lot = unablespace_lot_random + unablespace_lot_reserved;
        h4.Add("Lottotalspace", totalspace_lot);
        h4.Add("lotunablespace", unablespace_lot);
        Session["hasht4"] = h4;
    }

    protected void btn_tax_Click(object sender, EventArgs e)
    {
        try
        {

            dt = (DataTable)Session["datatable"];
            DataRow dr = dt.NewRow();
            dr[0] = txtbox_taxname.Text;
            dr[1] = txtbox_tax.Text;
            dr[2] = ddl_squencecal.SelectedItem.Text;
            dr[3] = txtbox_corporate_taxid.Text;

            dt.Rows.Add(dr);

            Session["datatable"] = dt;
            if (dt.Rows.Count > 0)
            {
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please fill  details.')", true);
        }
    }


    protected void btn_reserved_Click(object sender, EventArgs e)
    {

    }
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {

    }
    int parkingtyp;
    protected void Rate_for_reserevd_Click(object sender, EventArgs e)
    {
        if (chbk_Reserved.Checked == true)
        {
            parkingtyp = 1;
            Session["reservedparkingtype"] = parkingtyp;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Lot_Reserved_RateManager.aspx','New Windows','height=480, width=500,location=no');", true);
        }
        else
        {
            reserved.Visible = true;
            reserved.ForeColor = Color.Red;
            reserved.Text = "please mark the checkbox first.";
            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please mark the checkbox first.')", true);
        }

    }
    protected void _Random_Click(object sender, EventArgs e)
    {
        if (chbk_Random.Checked == true)
        {
            parkingtyp = 2;
            Session["randomparkingtype"] = parkingtyp;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Lot_Random_RateManager.aspx','New Windows','height=480, width=500,location=no');", true);
        }
        else
        {
            random.Visible = true;
            random.ForeColor = Color.Red;
            random.Text = "please mark the checkbox first.";
            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please mark the checkbox first.')", true);
        }
    }
  string  address ="";
  string  lat ="";
      string longi="";
    public void insertrec()
    {


        Hashtable first = (Hashtable)Session["hasht1"];
        int Siteid = Convert.ToInt32(first["siteid"].ToString());
        int campusid = Convert.ToInt32(first["campusid"].ToString());
        string lotname = first["Lotname"].ToString();
        string lotcode = first["Lotcode"].ToString();
        DateTime effectivedate = Convert.ToDateTime(first["Effctivedate"].ToString());
        string approv = first["Approval"].ToString();
        int timezone = Convert.ToInt32(first["Timezone"].ToString());
        string lothours = first["Lothours"].ToString();
       

        Hashtable second = (Hashtable)Session["hasht2"];

       // string address = second["address"].ToString();
        int city = Convert.ToInt32(second["city"].ToString());
        string zip = second["zip"].ToString();
        string cellular = second["cellular"].ToString();
        string office = second["officeno"].ToString();
        string email = second["email"].ToString();
       // string lat = second["latitude"].ToString();
        //string longi = second["longitude"].ToString();


        Hashtable third = (Hashtable)Session["hasht3"];
        string entryenglish = third["entryenglish"].ToString();
        string exitenglish = third["exitenglish"].ToString();
        string desceng = third["descriptionenglish"].ToString();
        string restrictionenglish = third["restrictionenglish"].ToString();

        string entryfrench = third["entryfrench"].ToString();
        string exitfrench = third["exitfrench"].ToString();
        string descfrnch = third["descriptionfrench"].ToString();
        string restrictionfrench = third["restrictionfrench"].ToString();

        int lottype = Convert.ToInt32(third["Lottype"].ToString());
        string otherdetails = third["otherdetails"].ToString();

        Hashtable four = (Hashtable)Session["hasht4"];
        int billingentry = Convert.ToInt32(four["Billingentry"].ToString());
        int billingexit = Convert.ToInt32(four["Billingexit"].ToString());
        int paymentrem = Convert.ToInt32(four["paymentreminder"].ToString());
        decimal interestrat = Convert.ToDecimal(four["interestrate"].ToString());
        decimal Latefee = Convert.ToDecimal(four["Latefee"].ToString());
        decimal activationcharge = Convert.ToDecimal(four["activationcharge"].ToString());
        string displayactivationcharge = four["displayactivationcharge"].ToString();
        string displaydeposite = four["displaydeposite"].ToString();

        int latepaymentaction = Convert.ToInt32(four["latepaymentaction"].ToString());
        int creditcardpaymentaction = Convert.ToInt32(four["creditcardpaymentaction"].ToString());


        string tranasactiontype_reserved = four["tranasactiontype_reserved"].ToString();
      
        int reserved_totalspace = Convert.ToInt32(four["reserved_totalspace"].ToString());
        int reserved_unablespace = Convert.ToInt32(four["reserved_unablespace"].ToString());
       
        string tranasactiontype_random = four["tranasactiontype_random"].ToString();
        int random_totalspace = Convert.ToInt32(four["random_totalspace"].ToString());
        int random_unablespace = Convert.ToInt32(four["random_unablespace"].ToString());


        int totalspace = Convert.ToInt32(four["Lottotalspace"].ToString());

        int unabelspace = Convert.ToInt32(four["lotunablespace"].ToString());


        string LotTransaction_Monthly1 = four["LotTransaction_Monthly"].ToString();
        string LotTransaction_Daily1 = four["LotTransaction_Daily"].ToString();

           DataTable dsadd = (DataTable)Session["dataaddress"];
           if (dsadd.Rows.Count > 0)
           {

               address = dsadd.Rows[0]["address"].ToString();
               lat = dsadd.Rows[0]["latitude"].ToString();
               longi = dsadd.Rows[0]["longitude"].ToString();
               lbladdress.Visible=true;
               txtadd.Visible = true;
               txtadd.Text = address;
               lbllatitude.Visible=true;
               txtlati.Visible = true;
               txtlati.Text=lat;
               lbllongitude.Visible=true;
               txtlongi.Visible = true;
               txtlongi.Text = longi;
           }
           else
           {
               ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please addlocation first.')", true);

           }
           try
           {
              
                   hst.Clear();

                   hst.Add("action", "insert");
                   hst.Add("siteid", Siteid);
                   hst.Add("campusid", campusid);

                   hst.Add("lotname", lotname);
                   hst.Add("lotcode", lotcode);
                   hst.Add("effectvdate ", effectivedate);
                   hst.Add("apprvl", approv);
                   hst.Add("otherdetail", otherdetails);
                   hst.Add("address", address);
                   hst.Add("city", city);
                   hst.Add("zip", zip);
                   hst.Add("lot_latitude", lat);
                   hst.Add("lot_longitude", longi);
                   hst.Add("lot_cell", cellular);

                   hst.Add("lot_office", office);
                   hst.Add("lot_email", email);
                   hst.Add("lottimezone", timezone);
                   hst.Add("lothours", lothours);
                   hst.Add("totalspaces", totalspace);
                   hst.Add("unablespace", unabelspace);
                   hst.Add("englishentrygateinformation", entryenglish);
                   hst.Add("englishexitgateinformation", exitenglish);
                   hst.Add("englishdescription", desceng);
                   hst.Add("englishrestriction", restrictionenglish);
                   hst.Add("frenchentrygateinformation", entryfrench);
                   hst.Add("frenchexitgateinformation", exitfrench);
                   hst.Add("frenchrestriction", restrictionfrench);
                   hst.Add("frenchdescription", descfrnch);
                   hst.Add("lottype", lottype);
                   hst.Add("billingentry", billingentry);
                   hst.Add("billingexit", billingexit);
                   hst.Add("paymentrem", paymentrem);
                   hst.Add("intrestrate", interestrat);
                   hst.Add("latefees", Latefee);
                   hst.Add("activationcharge", activationcharge);
                   hst.Add("displayactivationcharge", displayactivationcharge);
                   hst.Add("displaydeposite", displaydeposite);
                   hst.Add("latepaymentaction", latepaymentaction);
                   hst.Add("creditpaymentaction", creditcardpaymentaction);
                   hst.Add("lottansaction_daily", LotTransaction_Daily1);
                   hst.Add("lottransaction_monthly", LotTransaction_Monthly1);

                   hst.Add("random_parking", tranasactiontype_random);
                   hst.Add("random_parking_totalspace", random_totalspace);
                   hst.Add("random_parking_unablespace", random_unablespace);
                   hst.Add("reserved_parking", tranasactiontype_reserved);

                   hst.Add("reserved_totalspace", reserved_totalspace);
                   hst.Add("reserved_unablespace", reserved_unablespace);


                   hst.Add("modifiedby", "admin");
                   hst.Add("operatorid", "admin");

                   hst.Add("dateofchanging", DateTime.Now);
                   hst.Add("lotstaus", lotstatus);






                   int result = objData.ExecuteNonQuery("[sp_LotMaster]", CommandType.StoredProcedure, hst);
                   if (result > 0)
                   {

                       //Response.Redirect("Admin_UserPermissionMaster.aspx");

                   }
                   else
                   {
                       // lbl_error.Visible = true;

                   }

               
           }
           catch
           {

           }

    }

    string isspecial = "False";
    string status = "true";
    public void addextracharge(int id)
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("lotid", id);

            hst.Add("chargefor", "New Card Registration");
            hst.Add("status", status);
            hst.Add("chargedesc", "Activation Charge");
            if (txtbox_activationcharge.Text != "")
            {
                hst.Add("charges", Convert.ToDecimal(txtbox_activationcharge.Text));
            }
            else
            {
                hst.Add("charges", defaultvalue);

            }

            int result = objData.ExecuteNonQuery("[sp_Extracharges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               
                
            }
            else
            {
               

                // lbl_error.Visible = true;

            }
        }
        catch
        {
//ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }

    protected void btn_FINISH_FINAL_Click(object sender, EventArgs e)
    {
        try
        {
            //int totals, rndr, res;
            //int unabletotalspace, unablerandr, unablereserv;
            //totals = int.Parse(txtbox_totalspace.Text);
            //rndr = int.Parse(txtbox_randomtotalspace.Text);
            //res = int.Parse(txtbox_reservedtotalspace.Text);
            //unabletotalspace = int.Parse(txtbox_unablespace.Text);
            //unablerandr = int.Parse(txtbox_randomunablespace.Text);
            //unablereserv = int.Parse(txtbox_reservedunablespace.Text);

     //       if (totals != (rndr + res) || unabletotalspace != (unablerandr + unablereserv))
     //       {
     //           lbl_error.Visible = true;
     //           ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
     //"alert('Parking Space unmatched');", true);
               

     //       }
            
     //       else
     //       {
                first();
                second();
                third();
                forth();

                //if (chbk_Finish.Checked == true)
                //{
                try
                {
                    insertrec();

                    clsInsert cs = new clsInsert();
                    DataSet ds1 = cs.select_operation("select Lot_id from tbl_LotMaster where LotName='" + txtbox_lotname.Text + "'order by  Lot_id desc");
                    // int lastid= ds1.table[0].rows[0][0].tostring();
                    int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());

                    try
                    {
                        DataTable reservedparkingtype = (DataTable)Session["datatable_reserved"];

                        for (int i = 0; i < reservedparkingtype.Rows.Count; i++)
                        {

                            hst.Clear();

                            hst.Add("action", "insert");
                            hst.Add("lotid", lastid);
                            hst.Add("parkingtype", Convert.ToInt32(reservedparkingtype.Rows[i]["typeofparking"].ToString()));
                            hst.Add("amount", Convert.ToDecimal(reservedparkingtype.Rows[i]["Amount_reserved"].ToString()));
                            hst.Add("quantity", Convert.ToDecimal(reservedparkingtype.Rows[i]["quantity_reserved"].ToString()));
                            hst.Add("chargeby", Convert.ToInt32(reservedparkingtype.Rows[i]["chargeby_reserved"].ToString()));
                            hst.Add("isspecial", isspecial);

                            int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
                            if (result > 0)
                            {

                                //Response.Redirect("UserManager.aspx");

                            }
                            else
                            {
                                // lbl_error.Visible = true;

                            }
                        }

                    }
                    catch
                    {
                    }


                    try
                    {
                        DataTable randomparkingtype = (DataTable)Session["datatable_random"];

                        for (int i = 0; i < randomparkingtype.Rows.Count; i++)
                        {

                            hst.Clear();

                            hst.Add("action", "insert");
                            hst.Add("lotid", lastid);
                            hst.Add("parkingtype", Convert.ToInt32(randomparkingtype.Rows[i]["typeofparking"].ToString()));
                            hst.Add("amount", Convert.ToDecimal(randomparkingtype.Rows[i]["Amount_random"].ToString()));
                            hst.Add("quantity", Convert.ToDecimal(randomparkingtype.Rows[i]["quantity_random"].ToString()));
                            hst.Add("chargeby", Convert.ToInt32(randomparkingtype.Rows[i]["chargeby_random"].ToString()));
                            hst.Add("isspecial", isspecial);

                            int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
                            if (result > 0)
                            {

                                //Response.Redirect("UserManager.aspx");

                            }
                            else
                            {
                                // lbl_error.Visible = true;

                            }
                        }

                    }
                    catch
                    {
                    }


                    try
                    {


                        DataTable datatable = (DataTable)Session["datatable"];

                        for (int i = 0; i < datatable.Rows.Count; i++)
                        {

                            hst.Clear();

                            hst.Add("action", "insert");
                            hst.Add("lot_id", lastid);
                            hst.Add("taxname", datatable.Rows[i]["taxname"].ToString());
                            hst.Add("taxrate", Convert.ToDecimal(datatable.Rows[i]["tax"].ToString()));
                            hst.Add("seqofcal", Convert.ToInt32(datatable.Rows[i]["sequence"].ToString()));
                            hst.Add("corporatetaxid", datatable.Rows[i]["corporatetaxid"].ToString());


                            int result = objData.ExecuteNonQuery("[sp_LotTax]", CommandType.StoredProcedure, hst);
                            if (result > 0)
                            {

                                //Response.Redirect("UserManager.aspx");

                            }
                            else
                            {
                                // lbl_error.Visible = true;

                            }
                        }

                        //Session["datatable"] = null;
                        //Session["datatable_random"] = null;
                        //Session["datatable_reserved"] = null;
                        //Session["dataaddress"] = null;

                    }
                    catch
                    {

                    }

                    try
                    {
                        addextracharge(lastid);
                    }
                    catch
                    {

                    }

                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);

                    string msg = "1";
                   // Response.Redirect("LotMaster.aspx?message=" + msg);
                    Session["insert"] = "insertsuccess";
                    Response.Redirect("LotMaster.aspx");
                }

                catch(Exception e11)
                {
                    
                }

               

            //}
        }
        catch (Exception e111)
        {
        }
    }


    protected void txtbox_unablespace_TextChanged(object sender, EventArgs e)
    {
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('GetAddressLatLong.aspx','New Windows','height=380, width=600,location=no');", true);

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "remove")
        {
            dt = (DataTable)Session["datatable"];

            dt.Rows.RemoveAt(e.Item.ItemIndex);

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Session["datatable"] = dt;

        }
    }
    protected void chbk_Reserved_CheckedChanged(object sender, EventArgs e)
    {
        reserved.Visible = false;
    }
    protected void chbk_Random_CheckedChanged(object sender, EventArgs e)
    {
        random.Visible = false;
    }
    protected void txtbox_hours_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_reservedtotalspace_TextChanged(object sender, EventArgs e)
    {

    }
}
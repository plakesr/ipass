﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_LotSearch.ascx.cs" Inherits="WUC_wuc_LotSearch" %>

   <script type="text/javascript">
    var markers = [
    <asp:Repeater ID="rptMarkers" runat="server">
    <ItemTemplate>
             {
                "title": '<%# Eval("LotName") %>',
                "lat": '<%# Eval("Lot_Latitude") %>',
                "lng": '<%# Eval("Lot_Longitude") %>',
                "description": '<%# Eval("otherdetailslot") %>'
            }
    </ItemTemplate>
    <SeparatorTemplate>
        ,
    </SeparatorTemplate>
    </asp:Repeater>
    ];
    </script>
    <script type="text/javascript">

        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
<div>

    <table >
        <tr>
            <td>
                <asp:TextBox ID="txtbox_locationname" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_search" runat="server" Text="Search" 
                    onclick="btn_search_Click" />
            </td>
            <td rowspan="2">
                 <div id="dvMap" style="width: 500px; height: 500px">
    </div></td>
        </tr>
        <tr>
            <td >
                <asp:DataList ID="DataList1" runat="server" BackColor="White" 
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    ForeColor="Black" GridLines="Vertical">
                    <AlternatingItemStyle BackColor="White" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <HeaderTemplate>
                       NEARESt LOTS 
                    </HeaderTemplate>
                    <ItemStyle BackColor="#F7F7DE" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                   LOT NAME <%# Eval("LotName") %><br />
                    Longitude<%# Eval("Lot_Latitude") %><br />
                   Latitude <%# Eval("Lot_Longitude") %><br />
                   <asp:Button ID="details" runat="server" Text="Details" />
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>

</div>

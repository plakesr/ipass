﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class WUC_wuc_LotSearch : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    private DataTable GetData(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        SqlCommand cmd = new SqlCommand(query);
        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;

                sda.SelectCommand = cmd;
                using (DataTable dt = new DataTable())
                {
                    sda.Fill(dt);
                    return dt;
                }
            }
        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        DataTable dt = this.GetData("select tbl_LotMaster.*,tbl_City.City_name from tbl_LotMaster inner join tbl_City on tbl_LotMaster.City=tbl_City.City_id where tbl_City.City_name='" + txtbox_locationname.Text + "'");
        rptMarkers.DataSource = dt;
        rptMarkers.DataBind();
        DataList1.DataSource = dt;
        DataList1.DataBind();
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_LotToPlanAllot.ascx.cs" Inherits="WUC_wuc_LotToPlanAllot" %>
<style type="text/css">
    .style1
    {
        width: 202px;
    }
    .style2
    {
        width: 72px;
    }
      .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
</style>
<div>
<table style="height: 85px; width: 100%">
<tr>
<td class="style2">
    Site Name </td>
<td class="style1">
    <asp:DropDownList ID="ddl_sitename" runat="server" 
        DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId" 
        onselectedindexchanged="ddl_sitename_SelectedIndexChanged" AutoPostBack=true CssClass=twitterStyleTextbox>
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
    </asp:SqlDataSource>
</td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td class="style2" valign="top">
    Lot Name </td>
<td class="style1">
    <asp:CheckBoxList ID="chk_Lotname" runat="server" 
        onselectedindexchanged="chk_Lotname_SelectedIndexChanged">
    </asp:CheckBoxList>
</td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td>
    Plans</td>
<td>

    <asp:CheckBoxList ID="chk_plans" runat="server">
    </asp:CheckBoxList>
</td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td>
</td>
<td>
    <asp:Button ID="btn_add" runat="server" onclick="btn_add_Click" Text="Add" CssClass=button/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_edit" runat="server" onclick="btn_edit_Click" Text="Edit" CssClass=button />
</td>
</tr>
</table>
</div>

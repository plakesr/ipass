﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class WUC_wuc_LotToPlanAllot : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.showplan();


        }
    }
    public void addplantolot()
    {
        for (int i = 0; i < chk_Lotname.Items.Count; i++)
        {

            if (chk_Lotname.Items[i].Selected == true)
            {

                for (int j = 0; j < chk_plans.Items.Count; j++)
                {
                    if (chk_plans.Items[j].Selected == true)
                    {

                      bool k=  insertPlanto_Lot(int.Parse(chk_Lotname.Items[i].Value.ToString()), int.Parse(chk_plans.Items[j].Value.ToString())); 

                    }
                }

            }
        }


        }

    public bool insertPlanto_Lot(int lotid,int planid)
    {
        bool i;
        try
        {



            hst.Clear();
            hst.Add("action", "insert");
            hst.Add("lot_id", lotid );
            hst.Add("plan_id", planid );


            int result = objData.ExecuteNonQuery("[sp_lot_to_plan_allot]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                return i=true ;
                //  Response.Redirect("LotToPlanAllot.aspx");

            }
            else
            {
                return i = false;
                // lbl_error.Visible = true;

            }
            

        }
        catch
        {
            return i = false;
            //lbl_error.Visible = true;

        }
    }

    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {

            addplantolot();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
          

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
   
    protected void ddl_sitename_SelectedIndexChanged(object sender, EventArgs e)
    {
        chk_Lotname.Items.Clear();
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_LotMaster where Site_Id='" + ddl_sitename.SelectedValue + "'";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["LotName"].ToString();
                        item.Value = sdr["Lot_id"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_Lotname.Items.Add(item);

                    }
                }
                conn.Close();
            }
        }
    }
    private void showplan()
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_PlanMaster";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["Plan_Name"].ToString();
                        item.Value = sdr["Plan_id"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_plans.Items.Add(item);
                    }
                }
                conn.Close();
            }
        }
    }
    protected void btn_edit_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditLotToPlanAllot.aspx");
    }
    protected void chk_Lotname_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_MasterAccount.ascx.cs" Inherits="WUC_wuc_MasterAccount" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style3
    {
        width: 167px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Define Master Account</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="style3">
                Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_name" runat="server" 
                     ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_name" runat="server" 
                    ControlToValidate="txtbox_name" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_description" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_Add" runat="server" Text="Add" ValidationGroup="a" 
                    Width="55px" onclick="btn_Add_Click1" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</fieldset>
</asp:Panel>


<asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="SELECT * FROM [tbl_MasterAccount]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="rounded-corner">
                <tr>
                      <th>
                        Master&nbsp;&nbsp;Name
                    </th>
                    
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("MasterAccountName")%>
                    </td>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("MasterAccountId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("MasterAccountId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>

                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("MasterAccountName") %>' 
                                        Visible="False" />
                                        <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("MasterAccountDesc") %>' 
                                        Visible="False" />
                                          
    <asp:Label ID="id" runat="server" Text='<%# Eval("MasterAccountId") %>' 
                                        Visible="False"></asp:Label>     
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
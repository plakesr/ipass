﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_MasterAccount : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["message"] == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
        }
        if (Request.QueryString["msg"] == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn_Add_Click(object sender, EventArgs e)
    {
       
    }
    public void addMasterAccount()
    {
        try
        {
            hst.Clear();
           
            hst.Add("action", "insert");
            hst.Add("masteraccountname", txtbox_name.Text);
            hst.Add("masteraccountdesc", txtbox_description.Text);

            hst.Add("opreator", "lovey_operator");
            hst.Add("modified", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_MasterAccount]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

               // Response.Redirect("MasterAccount.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
    public void clear()
    {
        txtbox_name.Text = "";
        txtbox_description.Text = "";
        
    }
    protected void btn_Add_Click1(object sender, EventArgs e)
    {
        try
        {
            string msg = "1";
            addMasterAccount();
            Response.Redirect("MasterAccount.aspx?message=" + msg);

            clear();
        }
        catch
        {
        }
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            delMaster(ID);
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            Label id = rpt1.Items[i].FindControl("id") as Label;

            Label l2 = rpt1.Items[i].FindControl("idLabel1") as Label;
           



            Session["MasterName"] = l.Text;
            Session["id"] = id.Text;
            Session["Masterdesc"] = l2.Text;

            Response.Redirect("EditMasterAccount.aspx");

        }
    }
    public void delMaster(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_MasterAccount]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //Response.Redirect("MasterAccount.aspx");


            }
            else
            {
                Response.Redirect("MasterAccount.aspx");

            }
        }
        catch
        {
            Response.Redirect("MasterAccount.aspx");


        }


    }
}
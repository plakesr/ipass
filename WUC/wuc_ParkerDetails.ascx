﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_ParkerDetails.ascx.cs" Inherits="WUC_wuc_ParkerDetails" %>


<style type="text/css">
                    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
                </style>
 <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
  <table  class="form-table" width="100%" >
      <tr>
        <td>
         
          <asp:Button Text="Personal Details" BorderStyle="None" ID="btn_personaldetails" 
                CssClass="Initial" runat="server" onclick="btn_personaldetails_Click"
              />
               <asp:Button Text="Plan Details" BorderStyle="None" 
                ID="btn_plan" CssClass="Initial" runat="server" onclick="btn_plan_Click"
              />
          <asp:Button Text="Vehicle Details" BorderStyle="None" 
                ID="btn_addvehicledetails" CssClass="Initial" runat="server" onclick="btn_addvehicledetails_Click"
              />
             
          
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View2" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #dfefff; border-style: solid">
                <tr>
                  <td>
                            <asp:Panel ID="Panel2" runat="server" Height="650px">
                            <fieldset>
                                <legend>Personal Information</legend>
                                <table class="form-table">
                                    <tr>
                                        <td>
                                            Lot Name
                                        </td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_sitename" runat="server" CssClass="twitterStyleTextbox" 
                                                Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Account</td>
                                        <td style="width:225px;">
                                            <asp:RadioButton ID="rb_account" runat="server" AutoPostBack="true" 
                                                Enabled="False" GroupName="a" oncheckedchanged="rb_account_CheckedChanged" 
                                                Text="Individual" ValidationGroup="b" />
                                            <asp:RadioButton ID="rb_account1" runat="server" AutoPostBack="true" 
                                                Enabled="False" GroupName="a" oncheckedchanged="rb_account1_CheckedChanged" 
                                                Text="Corporate" ValidationGroup="b" />
                                        </td>
                                        <td class="style45">
                                            &nbsp;</td>
                                        <td class="style39">
                                            &nbsp;</td>
                                        <td class="style30">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            First Name:</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtBox_firstname" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="a"></asp:TextBox>
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td class="style45" width="130">
                                            Last Name:</td>
                                        <td class="style39">
                                            <asp:TextBox ID="txtbox_lastname" runat="server" CssClass="twitterStyleTextbox" 
                                                ValidationGroup="a"></asp:TextBox>
                                        </td>
                                        <td class="style30">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            <span ID="Grp_AccountInformation_lblContactName">Card Number(If Issued)</span></td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_coupannumber" runat="server" 
                                                CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td width="80">
                                        </td>
                                        <td class="style45" colspan="4" rowspan="5" valign="top">
                                            <asp:Panel ID="Panel_corporate1" runat="server" Visible="false">
                                                <table align="left" class="style40">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbl_ActivationDate" runat="server" Text="Activation Date"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtbox_ActicationDate_corporate" runat="server" 
                                                                CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbl_cardDeliveryMethod" runat="server" 
                                                                Text="card Delivery Method "></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtbox_deliverymethod_corporate" runat="server" 
                                                                CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td>
                        Plan :</td>
                    <td>
                        <asp:TextBox ID="txtbox_plan_corporate" runat="server" Enabled="False" CssClass=twitterStyleTextbox></asp:TextBox>
                    </td>
                    <td rowspan="3">
                        &nbsp;</td>--%>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel_corporate2" runat="server" Visible="false">
                                                <fieldset>
                                                    <legend>Plan Information</legend>
                                                    <table class="style3">
                                                        <tr>
                                                            <td class="style43" width="100">
                                                                Type</td>
                                                            <td>
                                                                <asp:TextBox ID="Txtbox_typ_corporate" runat="server" 
                                                                    CssClass="twitterStyleTextbox" Enabled="False" ValidationGroup="a"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style43">
                                                                Amount</td>
                                                            <td>
                                                                <asp:TextBox ID="txtbox_amount_plan_corporate" runat="server" 
                                                                    CssClass="twitterStyleTextbox" Enabled="False" TextMode="SingleLine" 
                                                                    ValidationGroup="a"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style43">
                                                                Charge Type</td>
                                                            <td>
                                                                <asp:TextBox ID="txtbox_charge_quantity_corporate" runat="server" 
                                                                    CssClass="twitterStyleTextbox" Enabled="False" TextMode="SingleLine" 
                                                                    ValidationGroup="a"></asp:TextBox>
                                                                <%--<asp:Label ID="lbl_Qunatity" runat="server" Text=""></asp:Label>
                  <asp:Label ID="lbl_charge" runat="server" Text=""></asp:Label>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Address1</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_address1" runat="server" style=" resize:none;" CssClass="twitterStyleTextbox" 
                                                TextMode="MultiLine" ValidationGroup="a"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Address2</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_address2" runat="server" style=" resize:none;" CssClass="twitterStyleTextbox" 
                                                TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Postal Code/Zip Code</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_postal" runat="server" CssClass="twitterStyleTextbox" 
                                                ValidationGroup="a"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Country</td>
                                        <td class="style44">
                                            <asp:DropDownList ID="ddl_country" runat="server" AutoPostBack="True" 
                                                DataSourceID="SqlDataSource1" DataTextField="Country_name" 
                                                DataValueField="Country_id" 
                                                onselectedindexchanged="ddl_country_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country] ORDER BY Country_name">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Province</td>
                                        <td class="style44">
                                            <asp:DropDownList ID="ddl_province" runat="server" AutoPostBack="True" 
                                                DataSourceID="SqlDataSource2" DataTextField="ProvinceName" 
                                                DataValueField="ProvinceId">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                SelectCommand="SELECT [ProvinceId], [ProvinceName] FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="ddl_country" Name="Country_id" 
                                                        PropertyName="SelectedValue" Type="Int64" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td class="style45">
                                            City</td>
                                        <td class="style39">
                                            <asp:DropDownList ID="ddl_city" runat="server" DataSourceID="SqlDataSource3" 
                                                DataTextField="City_name" DataValueField="City_id">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                SelectCommand="SELECT [City_id], [City_name], [Province_id] FROM [tbl_City] order by City_name ">
                                            </asp:SqlDataSource>
                                        </td>
                                        <td class="style30">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Cellular No.</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_cellular" runat="server" CssClass="twitterStyleTextbox" 
                                                ValidationGroup="a"></asp:TextBox>
                                        </td>
                                        <td width="50">
                                        </td>
                                        <td class="style45">
                                            Phone No.</td>
                                        <td class="style39">
                                            <asp:TextBox ID="txtbox_phone" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td class="style30">
                                        </td>
                                        <td class="style9">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style30">
                                            Fax No.</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txt_fax" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td class="style45">
                                            &nbsp;</td>
                                        <td class="style39">
                                            &nbsp;</td>
                                        <td class="style30">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style34">
                                            Email Id</td>
                                        <td class="style44">
                                            <asp:TextBox ID="txtbox_Email" runat="server" CssClass="twitterStyleTextbox" 
                                                ValidationGroup="a"></asp:TextBox>
                                        </td>
                                        <td class="style45">
                                            &nbsp;</td>
                                        <td class="style39">
                                            &nbsp;</td>
                                        <td class="style30">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </fieldset>
                        </asp:Panel>
                        <br /> <br />
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div>
                                    <asp:Panel ID="Panel4" runat="server">
                                        <fieldset>
                                            <legend>Billing Information</legend>
                                            <br />
                                            <ContentTemplate>
                                                <table class="style10">
                                                    <tr>
                                                        <td style="width:100px;">
                                                            Billing
                                                        </td>
                                                        <td class="style41">
                                                            <asp:Label ID="lbl_billingtype" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel5" runat="server" style="height: 192px; width: 933px" 
                                        Visible="False">
                                        <fieldset>
                                            <legend>Direct&nbsp; Debit Bank Information</legend>
                                            <br />
                                            <table class="style10">
                                                <tr>
                                                    <td class="style9">
                                                        Bank Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankname" runat="server" CssClass="twitterStyleTextbox" 
                                                            Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        Branch</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_branch" runat="server" CssClass="twitterStyleTextbox" 
                                                            Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style9">
                                                        Customer Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_customername" runat="server" 
                                                            CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        Bank Account Type</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_accounttype" runat="server" 
                                                            CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style9">
                                                        State/Province</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankstate" runat="server" 
                                                            CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        Bank Routing No</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_routingno" runat="server" 
                                                            CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style9">
                                                        Zip/Postal</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankzip" runat="server" CssClass="twitterStyleTextbox" 
                                                            Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        City</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_city_bank" runat="server" 
                                                            CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style9">
                                                        Bank Account No</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankaccountno" runat="server" 
                                                            CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <br />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                   
                        <h3>
                            <asp:Button ID="btn_next3" runat="server" CssClass="button" 
                                onclick="btn_next2_Click" Text="Next" ValidationGroup="a" />
                            <asp:Button ID="btn_upd0" runat="server" CssClass="button" 
                                onclick="btn_upd_Click" Text="Update" />
                        </h3>


                                              
                  </td>
                </tr>
              </table>
            </asp:View>
                       
              <asp:View ID="View1" runat="server">
              
<asp:UpdatePanel ID="UpdatePanel3" runat="server">
<ContentTemplate>
<fieldset>
<legend></legend>
        
            <table align="left" width="100%">
            <tr>
                    <td width="150">
                        Activation Date :</td>
                    <td>
                        <asp:TextBox ID="txtbox_ActicationDate" runat="server" Enabled="False" CssClass=twitterStyleTextbox></asp:TextBox>
                       
                    </td>
                </tr>
                
                 <tr>
                    <td>card Delivery Method
                    </td>
                    <td>
                        <asp:TextBox ID="txtbox_deliverymethod" runat="server" Enabled="False" CssClass=twitterStyleTextbox></asp:TextBox>
                       </td>
                </tr>
                <tr>
                    <%--<td>
                        Plan :</td>
                    <td>
                        <asp:TextBox ID="txtbox_plan" runat="server" Enabled="False" CssClass=twitterStyleTextbox></asp:TextBox>
                    </td>
                    <td rowspan="3">
                        &nbsp;</td>--%>



   
        
    
                </tr>
                
            </table>
            </fieldset>
            <asp:Panel ID="Panel3" runat="server" Height="250px">
<fieldset>
<legend>Plan Information</legend>
    <table class="style3">
        <tr>
            <td class="style43" width="150">
                
                
                Type</td>
            <td>
                <asp:TextBox ID="Txtbox_typ" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                
            </td>
        </tr>
        <tr>
            <td class="style43">
              
                Amount</td>
            <td>
                <asp:TextBox ID="txtbox_amount_plan" runat="server" ValidationGroup="a" 
                    TextMode="SingleLine" CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                  
                  
            </td>
        </tr>

        <tr>
            <td class="style43">
              
                Charge Type</td>
            <td>
            <asp:TextBox ID="txtbox_charge_quantity" runat="server" ValidationGroup="a" 
                    TextMode="SingleLine" CssClass="twitterStyleTextbox" Enabled="False"></asp:TextBox>
                <%--<asp:Label ID="lbl_Qunatity" runat="server" Text=""></asp:Label>
                  <asp:Label ID="lbl_charge" runat="server" Text=""></asp:Label>--%>
                  
            </td>
        </tr>
       
    </table>
    </fieldset>
</asp:Panel>

      </ContentTemplate>
    </asp:UpdatePanel>
     <asp:Button ID="btn_prelast" runat="server" Text="Previous"  CssClass="button"
                      onclick="btn_prelast_Click"/>&nbsp; &nbsp; 
    <asp:Button ID="btn_next1" runat="server" Text="Next" CssClass="button" 
                      onclick="btn_next3_Click" />
              </asp:View>
              
          <asp:View ID="View3" runat="server">
              <table style=" border-width: 1px; border-color: #666; border-style: solid ;width:100%">
                <tr>
                  <td>
                      <asp:Panel ID="Panel1" runat="server"  Width="100%" CssClass="parker-table">
                      <fieldset>

                       <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        DeleteCommand="DELETE FROM [tbl_parkerVehicleDetails] WHERE [ParkerVehicleId] = @ParkerVehicleId" 
        InsertCommand="INSERT INTO [tbl_parkerVehicleDetails] ([Parker_Id], [Make], [Model], [Year], [Color], [LicenseNo], [UserName]) VALUES (@Parker_Id, @Make, @Model, @Year, @Color, @LicenseNo, @UserName)" 
        SelectCommand="SELECT * FROM [tbl_parkerVehicleDetails] where username=@username " 
        
        UpdateCommand="UPDATE [tbl_parkerVehicleDetails] SET  [Make] = @Make, [Model] = @Model, [Year] = @Year, [Color] = @Color, [LicenseNo] = @LicenseNo WHERE [ParkerVehicleId] = @ParkerVehicleId">
        <DeleteParameters>
            <asp:Parameter Name="ParkerVehicleId" Type="Int64" />
        </DeleteParameters>
       <InsertParameters>
            
            <asp:Parameter Name="Make" Type="String" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="String" />
            <asp:Parameter Name="LicenseNo" Type="String" />
           
            <asp:SessionParameter Name="Parker_Id" SessionField="Parker_id" 
                Type="Int64" />
          <asp:SessionParameter Name="UserName" SessionField="UserName" 
                Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="username" SessionField="Username" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Parker_Id" Type="Int64" />
            <asp:Parameter Name="Make" Type="String" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="String" />
            <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="UserName" Type="String" />
            <asp:Parameter Name="ParkerVehicleId" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
        SelectCommand="SELECT [VehiMake] FROM [tbl_VehicleMakes] ORDER BY [VehiMake]">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT [VehiColor] FROM [tbl_Vehicle_color]"></asp:SqlDataSource>
     <asp:ListView ID="ListView1" runat="server" DataKeyNames="ParkerVehicleId" 
        DataSourceID="SqlDataSource4" InsertItemPosition="LastItem" 
        onselectedindexchanged="ListView1_SelectedIndexChanged">
        <AlternatingItemTemplate>
            <tr style="background-color: #FAFAD2;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button"  
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button" />
                </td>
                
                <td >
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="background-color: #FFCC66;color: #000080;">
                <td> 
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" CssClass="button"  
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="button" 
                        Text="Cancel" />
                </td>
                
                <td>
               <%-- <asp:Label ID="ParkerVehicleIdLabel1" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' Visible="false" />
                        <asp:TextBox ID="Parker_IdTextBox" runat="server" 
                        Text='<%# Bind("Parker_Id") %>' Visible="false" />--%>
                 <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource5" DataTextField="VehiMake" 
                DataValueField="VehiMake" SelectedValue='<%# Bind("Make") %>'> </asp:DropDownList>

                    
                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
              

                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource6" DataTextField="VehiColor" 
                DataValueField="VehiColor" SelectedValue='<%# Bind("Color") %>'> </asp:DropDownList>
                  
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table id="Table1" runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" CssClass="add"  
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="cancel"  
                        Text="Clear" />
                </td>
                 
                <td>
                    
                    <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource5" DataTextField="VehiMake" 
                DataValueField="VehiMake" SelectedValue='<%# Bind("Make", "{0}") %>'> </asp:DropDownList>
                    

                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource6" DataTextField="VehiColor" 
                DataValueField="VehiColor" SelectedValue='<%# Bind("Color", "{0}") %>'> </asp:DropDownList>
                  <%--  <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />--%>
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
              
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="background-color: #FFFBD6;color: #333333;">
                <td>
             
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit"  />
                </td>
               
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td >
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td >
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
               
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table id="Table2" runat="server">
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="1"
                            style=" background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family:Open Sans; width:100%;">
                            <tr id="Tr2" runat="server" style="background-color: #FFFBD6;color: #333333;">
                                <th id="Th1" runat="server">
                                </th>
                               <%-- <th id="Th2" runat="server">
                                    ParkerVehicleId</th>
                                <th id="Th3" runat="server">
                                    Parker_Id</th>--%>
                                <th id="Th4"  runat="server">
                                    Make</th>
                                <th id="Th5" runat="server">
                                    Model</th>
                                <th id="Th6" runat="server">
                                    Year</th>
                                <th id="Th7" runat="server">
                                    Color</th>
                                <th id="Th8" runat="server">
                                    LicenseNo</th>
                                
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td2" runat="server" 
                        style="text-align: center;background-color: #6699ff;font-family: Verdana, Arial, Helvetica, sans-serif;color: #333333;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                    ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #FFCC66;font-weight: bold;color: #000080;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit" />
                </td>
                <td>
                    <%--<asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />--%>
                </td>
                <td>
                   <%-- <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />--%>
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                   <%-- <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />--%>
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
     </fieldset>
                      
                      </asp:Panel>

              <%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Repeater ID="rpt_parkervehicles" runat="server" 
                                    onitemcommand="rpt_parkervehicles_ItemCommand" >
                                    <HeaderTemplate>
                                        <table cellpadding="5" cellspacing="0" class="timezone-table" width="100%">
                                            <tr>
                                                <th>
                                                    Vehicle Make
                                                </th>
                                                <th>
                                                    Vehicle Model
                                                </th>
                                                <th>
                                                    Vehicle Color
                                                </th>
                                                <th>
                                                    Year
                                                </th>
                                                <th>
                                                    Licence No
                                                </th>
                                                <th>
                                                    Edit
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                            </tr>
                                            
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <tr>
                                        <td>
                                       <asp:TextBox ID="make" runat="server" Text=' <%#Eval("Make")%>' Width="90%" Enabled="false"></asp:TextBox>    
                                        </td>
                                        <td>
                                       <asp:TextBox ID="model" runat="server" Text=' <%#Eval("Model")%>'  Width="90%" Enabled="false"> </asp:TextBox>   
                                        </td>
                                        <td>
                                       <asp:TextBox ID="color" runat="server" Text=' <%#Eval("Color")%>'  Width="90%" Enabled="false"></asp:TextBox> 
                                            
                                        </td>
                                        <td>
                                        <asp:TextBox ID="year" runat="server" Text=' <%#Eval("Year")%>'  Width="90%" Enabled="false"> </asp:TextBox>    
                                        </td>
                                        <td>
                                        <asp:TextBox ID="licenseno" runat="server" Text=' <%#Eval("LicenseNo")%>'  Width="90%" Enabled="false"> </asp:TextBox>    
                                           
                                        </td>
                                         <td>
                                        <asp:LinkButton ID="LinkButton1" CommandName="cmdEdit" CommandArgument='<%#Eval("ParkerVehicleId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/edit.png" /> </asp:LinkButton>
                                        </td>
                                        <td>
                                        <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("ParkerVehicleId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" /> </asp:LinkButton>
                                        </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                       <br />
                       <br /><br /> <br />
                       <br /><br /> <br />
                       <br /><br /> <br />
                       <br /><br /> <br />
                       <br /><br />
                        <asp:Button ID="btn_prev3" runat="server" CssClass="button" onclick="btn_prev3_Click" 
                            Text="Previous" />

                     

                     

                        &nbsp; &nbsp;
                        <asp:Button ID="btn_ok" runat="server" CssClass="button" onclick="btn_ok_Click" 
                            Text="Update" />

                     

                   
                  </td>
                </tr>
              </table>
            </asp:View>
 </asp:MultiView>
        </td>
      </tr>
    </table>
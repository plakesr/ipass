﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_ParkerDetails : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            btn_personaldetails.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;

            try
            {
            if (Session["AccountType"].ToString() == "Individual" )
            {
                rb_account.Checked = true;
                rb_account.Enabled = false;
                rb_account1.Enabled = false;
                rb_account.Enabled = false;
                btn_addvehicledetails.Visible = true;
                btn_plan.Visible = true;

                txtBox_firstname.Text = Session["FirstName"].ToString();
                txtbox_lastname.Text = Session["LastName"].ToString();
                txtbox_coupannumber.Text = Session["Cardno"].ToString();
                txtbox_address1.Text = Session["AdressLine1"].ToString();
                txtbox_address2.Text = Session["AdressLine2"].ToString();
                txtbox_postal.Text = Session["Zip"].ToString();

                txtbox_phone.Text = Session["Phoneno"].ToString();
                txtbox_cellular.Text = Session["CellNo"].ToString();
                txt_fax.Text = Session["Fax"].ToString();
                txtbox_Email.Text = Session["Email"].ToString();

                txtbox_ActicationDate.Text = Session["ActivationDate"].ToString();

                clsInsert obbj = new clsInsert();
                DataSet dds = obbj.fetchdatea("select Country_id  from tbl_country where Country_name ='" + Session["Country"].ToString() + "' ;select ProvinceId  from tbl_Province where ProvinceName ='" + Session["State"].ToString() + "';select City_id  from tbl_City where City_name ='" + Session["City"].ToString() + "';");
                ddl_country.SelectedValue = dds.Tables[0].Rows[0][0].ToString();
                ddl_province.SelectedValue = dds.Tables[1].Rows[0][0].ToString();
                ddl_city.SelectedValue = dds.Tables[2].Rows[0][0].ToString();

                if (Session["Billingwith"].ToString() == "4")
                {
                    // rbtn_ach.Checked = true;
                    lbl_billingtype.Text = "(Billing With Account)";
                    Panel5.Visible = true;

                    DataTable dt = new clsInsert().getparkersBankAccountDetails(Session["UserName"].ToString());
                    int count = dt.Rows.Count;
                    if (dt.Rows.Count > 0)
                    {

                        txtbox_customername.Text = dt.Rows[0]["CustomerName"].ToString();
                        txtbox_city_bank.Text = dt.Rows[0]["Bank_city"].ToString();
                        txtbox_bankzip.Text = dt.Rows[0]["Bank_Zipcode"].ToString();
                        txtbox_bankstate.Text = dt.Rows[0]["Bank_State"].ToString();
                        txtbox_bankname.Text = dt.Rows[0]["BankName"].ToString();
                        txtbox_bankaccountno.Text = dt.Rows[0]["BankAccountNo"].ToString();
                        txtbox_accounttype.Text = dt.Rows[0]["BankAccType"].ToString();
                        txtbox_branch.Text = dt.Rows[0]["BranchName"].ToString();
                        txtbox_routingno.Text = dt.Rows[0]["BankRoutingNo"].ToString();
                    }
                }
                if (Session["Billingwith"].ToString() == "1")
                {
                    // rbtn_creditcard.Checked = true;
                    lbl_billingtype.Text = "(Credit Card Payment)";
                }

                if (Session["Billingwith"].ToString() == "2")
                {
                    //  rbtn_invoicebymail.Checked = true;
                    lbl_billingtype.Text = "(Invoice By mail)";
                }
                if (Session["Billingwith"].ToString() == "3")
                {
                    // rbtn_invoicebyemail.Checked = true;
                    lbl_billingtype.Text = "(Invoice By Email)";
                }
                DataTable dtdetails = new clsInsert().getparkersotherDetails(Session["UserName"].ToString());

                if (dtdetails.Rows.Count > 0)
                {

                    txtbox_sitename.Text = dtdetails.Rows[0]["LotName"].ToString();
                    txtbox_deliverymethod.Text = dtdetails.Rows[0]["CardDeliveryName"].ToString();
                }


                DataTable dt_plandetail = new clsInsert().getparkersplan(Session["UserName"].ToString());

                if (dt_plandetail.Rows.Count > 0)
                {

                    if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "1")
                    {
                        Txtbox_typ.Text = "Reserved Parking";
                    }

                    if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "2")
                    {
                        Txtbox_typ.Text = "Random Parking";
                    }

                    if (dt_plandetail.Rows[0]["chargeby"].ToString() == "1")
                    {
                        txtbox_charge_quantity.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Hour";
                    }

                    if (dt_plandetail.Rows[0]["chargeby"].ToString() == "2")
                    {
                        txtbox_charge_quantity.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Days";
                    }

                    if (dt_plandetail.Rows[0]["chargeby"].ToString() == "3")
                    {
                        txtbox_charge_quantity.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Month";
                    }
                    //lbl_Qunatity.Text = dt_plandetail.Rows[0]["Quantity"].ToString();
                    txtbox_amount_plan.Text = dt_plandetail.Rows[0]["Amount"].ToString() + " $";
                }
                //DataTable dtvehicle = new clsInsert().getparkersvehicles(Session["UserName"].ToString());
                //int countdtvehicle = dtvehicle.Rows.Count;
                //if (dtvehicle.Rows.Count > 0)
                //{


                //    //rpt_parkervehicles.DataSource = dtvehicle;
                //    //rpt_parkervehicles.DataBind();

                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Vehicles Found.')", true);
                //}

            }

            if (Session["AccountType"].ToString() == "Corporate")
            {
                rb_account1.Checked = true;
                rb_account1.Enabled = false;
                rb_account.Enabled = false;
                rb_account.Checked = false;
                btn_plan.Visible = false;
                btn_addvehicledetails.Visible = false;
                Panel_corporate1.Visible = true;
                Panel_corporate2.Visible = true;
                    

                txtBox_firstname.Text = Session["FirstName"].ToString();
                txtbox_lastname.Text = Session["LastName"].ToString();
                txtbox_coupannumber.Text = Session["Cardno"].ToString();
                txtbox_address1.Text = Session["AdressLine1"].ToString();
                txtbox_address2.Text = Session["AdressLine2"].ToString();
                txtbox_postal.Text = Session["Zip"].ToString();

                txtbox_phone.Text = Session["Phoneno"].ToString();
                txtbox_cellular.Text = Session["CellNo"].ToString();
                txt_fax.Text = Session["Fax"].ToString();
                txtbox_Email.Text = Session["Email"].ToString();

                txtbox_ActicationDate.Text = Session["ActivationDate"].ToString();

                clsInsert obbj = new clsInsert();
                DataSet dds = obbj.fetchdatea("select Country_id  from tbl_country where Country_name ='" + Session["Country"].ToString() + "' ;select ProvinceId  from tbl_Province where ProvinceName ='" + Session["State"].ToString() + "';select City_id  from tbl_City where City_name ='" + Session["City"].ToString() + "';");
                ddl_country.SelectedValue = dds.Tables[0].Rows[0][0].ToString();
                ddl_province.SelectedValue = dds.Tables[1].Rows[0][0].ToString();
                ddl_city.SelectedValue = dds.Tables[2].Rows[0][0].ToString();


                if (Session["Billingwith"].ToString() == "4")
                {
                    // rbtn_ach.Checked = true;
                    lbl_billingtype.Text = "(Billing With Account)";
                    Panel5.Visible = true;

                    DataTable dt = new clsInsert().getparkersBankAccountDetails(Session["UserName"].ToString());
                    int count = dt.Rows.Count;
                    if (dt.Rows.Count > 0)
                    {

                        txtbox_customername.Text = dt.Rows[0]["CustomerName"].ToString();
                        txtbox_city_bank.Text = dt.Rows[0]["Bank_city"].ToString();
                        txtbox_bankzip.Text = dt.Rows[0]["Bank_Zipcode"].ToString();
                        txtbox_bankstate.Text = dt.Rows[0]["Bank_State"].ToString();
                        txtbox_bankname.Text = dt.Rows[0]["BankName"].ToString();
                        txtbox_bankaccountno.Text = dt.Rows[0]["BankAccountNo"].ToString();
                        txtbox_accounttype.Text = dt.Rows[0]["BankAccType"].ToString();
                        txtbox_branch.Text = dt.Rows[0]["BranchName"].ToString();
                        txtbox_routingno.Text = dt.Rows[0]["BankRoutingNo"].ToString();
                    }
                }
                if (Session["Billingwith"].ToString() == "1")
                {
                    // rbtn_creditcard.Checked = true;
                    lbl_billingtype.Text = "(Credit Card Payment)";
                }

                if (Session["Billingwith"].ToString() == "2")
                {
                    //  rbtn_invoicebymail.Checked = true;
                    lbl_billingtype.Text = "(Invoice By mail)";
                }
                if (Session["Billingwith"].ToString() == "3")
                {
                    // rbtn_invoicebyemail.Checked = true;
                    lbl_billingtype.Text = "(Invoice By Email)";
                }



                DataTable dtdetails = new clsInsert().getparkersotherDetails(Session["UserName"].ToString());

                if (dtdetails.Rows.Count > 0)
                {

                    txtbox_sitename.Text = dtdetails.Rows[0]["LotName"].ToString();
                    txtbox_deliverymethod_corporate.Text = dtdetails.Rows[0]["CardDeliveryName"].ToString();
                }


                DataTable dt_plandetail = new clsInsert().getparkersplan(Session["UserName"].ToString());

                if (dt_plandetail.Rows.Count > 0)
                {

                    if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "1")
                    {
                        Txtbox_typ_corporate.Text = "Reserved Parking";
                    }

                    if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "2")
                    {
                        Txtbox_typ_corporate.Text = "Random Parking";
                    }

                    if (dt_plandetail.Rows[0]["chargeby"].ToString() == "1")
                    {
                        txtbox_charge_quantity_corporate.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Hour";
                    }

                    if (dt_plandetail.Rows[0]["chargeby"].ToString() == "2")
                    {
                        txtbox_charge_quantity_corporate.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Days";
                    }

                    if (dt_plandetail.Rows[0]["chargeby"].ToString() == "3")
                    {
                        txtbox_charge_quantity_corporate.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Month";
                    }
                    //lbl_Qunatity.Text = dt_plandetail.Rows[0]["Quantity"].ToString();
                    txtbox_amount_plan_corporate.Text = dt_plandetail.Rows[0]["Amount"].ToString() + " $";
                }

            }
        }
            catch
            {
                try
                {
                    DataTable dtdetailsforupdate = new clsInsert().getparkersdetails(Session["Login"].ToString());
                    if (dtdetailsforupdate.Rows.Count > 0)
                    {
                        if (Session["Type"].ToString() == "individual")
                        {
                            rb_account.Checked = true;
                            rb_account.Enabled = false;
                            rb_account1.Enabled = false;
                            rb_account.Enabled = false;
                            Session["Parker_id"] = dtdetailsforupdate.Rows[0]["Parker_id"].ToString();
                            txtBox_firstname.Text = dtdetailsforupdate.Rows[0]["FirstName"].ToString();
                            txtbox_lastname.Text = dtdetailsforupdate.Rows[0]["LastName"].ToString();
                            txtbox_coupannumber.Text = dtdetailsforupdate.Rows[0]["CardNo"].ToString();
                            txtbox_address1.Text = dtdetailsforupdate.Rows[0]["AdressLine1"].ToString();
                            txtbox_address2.Text = dtdetailsforupdate.Rows[0]["AdressLine2"].ToString();
                            txtbox_postal.Text = dtdetailsforupdate.Rows[0]["Zip"].ToString();

                            txtbox_phone.Text = dtdetailsforupdate.Rows[0]["Phoneno"].ToString();
                            txtbox_cellular.Text = dtdetailsforupdate.Rows[0]["CellNo"].ToString();
                            txt_fax.Text = dtdetailsforupdate.Rows[0]["Fax"].ToString();
                            txtbox_Email.Text = dtdetailsforupdate.Rows[0]["Email"].ToString();

                            txtbox_ActicationDate.Text = dtdetailsforupdate.Rows[0]["ActivationDate"].ToString();

                            clsInsert obbj = new clsInsert();
                            DataSet dds = obbj.fetchdatea("select Country_id  from tbl_country where Country_name ='" + dtdetailsforupdate.Rows[0]["Country"].ToString() + "' ;select ProvinceId  from tbl_Province where ProvinceName ='" + dtdetailsforupdate.Rows[0]["State"].ToString() + "';select City_id  from tbl_City where City_name ='" + dtdetailsforupdate.Rows[0]["City"].ToString() + "';");
                            ddl_country.SelectedValue = dds.Tables[0].Rows[0][0].ToString();
                            ddl_province.SelectedValue = dds.Tables[1].Rows[0][0].ToString();
                            ddl_city.SelectedValue = dds.Tables[2].Rows[0][0].ToString();




                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "4")
                            {
                                // rbtn_ach.Checked = true;
                                lbl_billingtype.Text = "(Billing With Account)";
                                Panel5.Visible = true;

                                DataTable dt = new clsInsert().getparkersBankAccountDetails(Session["Login"].ToString());
                                int count = dt.Rows.Count;
                                if (dt.Rows.Count > 0)
                                {

                                    txtbox_customername.Text = dt.Rows[0]["CustomerName"].ToString();
                                    txtbox_city_bank.Text = dt.Rows[0]["Bank_city"].ToString();
                                    txtbox_bankzip.Text = dt.Rows[0]["Bank_Zipcode"].ToString();
                                    txtbox_bankstate.Text = dt.Rows[0]["Bank_State"].ToString();
                                    txtbox_bankname.Text = dt.Rows[0]["BankName"].ToString();
                                    txtbox_bankaccountno.Text = dt.Rows[0]["BankAccountNo"].ToString();
                                    txtbox_accounttype.Text = dt.Rows[0]["BankAccType"].ToString();
                                    txtbox_branch.Text = dt.Rows[0]["BranchName"].ToString();
                                    txtbox_routingno.Text = dt.Rows[0]["BankRoutingNo"].ToString();
                                }
                            }
                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "1")
                            {
                                // rbtn_creditcard.Checked = true;
                                lbl_billingtype.Text = "(Credit Card Payment)";
                            }

                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "2")
                            {
                                //  rbtn_invoicebymail.Checked = true;
                                lbl_billingtype.Text = "(Invoice By mail)";
                            }
                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "3")
                            {
                                // rbtn_invoicebyemail.Checked = true;
                                lbl_billingtype.Text = "(Invoice By Email)";
                            }

                            DataTable dtdetails = new clsInsert().getparkersotherDetails(Session["Login"].ToString());

                            if (dtdetails.Rows.Count > 0)
                            {

                                txtbox_sitename.Text = dtdetails.Rows[0]["LotName"].ToString();
                                txtbox_deliverymethod.Text = dtdetails.Rows[0]["CardDeliveryName"].ToString();
                            }

                            DataTable dt_plandetail = new clsInsert().getparkersplan(Session["Login"].ToString());

                            if (dt_plandetail.Rows.Count > 0)
                            {

                                if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "1")
                                {
                                    Txtbox_typ.Text = "Reserved Parking";
                                }

                                if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "2")
                                {
                                    Txtbox_typ.Text = "Random Parking";
                                }

                                if (dt_plandetail.Rows[0]["chargeby"].ToString() == "1")
                                {
                                    txtbox_charge_quantity.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Hour";
                                }

                                if (dt_plandetail.Rows[0]["chargeby"].ToString() == "2")
                                {
                                    txtbox_charge_quantity.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Days";
                                }

                                if (dt_plandetail.Rows[0]["chargeby"].ToString() == "3")
                                {
                                    txtbox_charge_quantity.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Month";
                                }
                                //lbl_Qunatity.Text = dt_plandetail.Rows[0]["Quantity"].ToString();
                                txtbox_amount_plan.Text = dt_plandetail.Rows[0]["Amount"].ToString() + " $";
                            }

                        }

                        if (Session["Type"].ToString() == "Corporate")
                        {


                            rb_account.Checked = false;
                            rb_account.Enabled = false;
                            rb_account1.Checked = true;
                            rb_account1.Enabled = false;
                            btn_plan.Visible = false;
                            btn_addvehicledetails.Visible = false;
                            Panel_corporate1.Visible = true;
                            Panel_corporate2.Visible = true;
                            Session["Parker_id"] = dtdetailsforupdate.Rows[0]["Parker_id"].ToString();
                            txtBox_firstname.Text = dtdetailsforupdate.Rows[0]["FirstName"].ToString();
                            txtbox_lastname.Text = dtdetailsforupdate.Rows[0]["LastName"].ToString();
                            txtbox_coupannumber.Text = dtdetailsforupdate.Rows[0]["CardNo"].ToString();
                            txtbox_address1.Text = dtdetailsforupdate.Rows[0]["AdressLine1"].ToString();
                            txtbox_address2.Text = dtdetailsforupdate.Rows[0]["AdressLine2"].ToString();
                            txtbox_postal.Text = dtdetailsforupdate.Rows[0]["Zip"].ToString();

                            txtbox_phone.Text = dtdetailsforupdate.Rows[0]["Phoneno"].ToString();
                            txtbox_cellular.Text = dtdetailsforupdate.Rows[0]["CellNo"].ToString();
                            txt_fax.Text = dtdetailsforupdate.Rows[0]["Fax"].ToString();
                            txtbox_Email.Text = dtdetailsforupdate.Rows[0]["Email"].ToString();

                            txtbox_ActicationDate_corporate.Text = dtdetailsforupdate.Rows[0]["ActivationDate"].ToString();

                            clsInsert obbj = new clsInsert();
                            DataSet dds = obbj.fetchdatea("select Country_id  from tbl_country where Country_name ='" + dtdetailsforupdate.Rows[0]["Country"].ToString() + "' ;select ProvinceId  from tbl_Province where ProvinceName ='" + dtdetailsforupdate.Rows[0]["State"].ToString() + "';select City_id  from tbl_City where City_name ='" + dtdetailsforupdate.Rows[0]["City"].ToString() + "';");
                            ddl_country.SelectedValue = dds.Tables[0].Rows[0][0].ToString();
                            ddl_province.SelectedValue = dds.Tables[1].Rows[0][0].ToString();
                            ddl_city.SelectedValue = dds.Tables[2].Rows[0][0].ToString();




                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "4")
                            {
                                // rbtn_ach.Checked = true;
                                lbl_billingtype.Text = "(Billing With Account)";
                                Panel5.Visible = true;

                                DataTable dt = new clsInsert().getparkersBankAccountDetails(Session["Login"].ToString());
                                int count = dt.Rows.Count;
                                if (dt.Rows.Count > 0)
                                {

                                    txtbox_customername.Text = dt.Rows[0]["CustomerName"].ToString();
                                    txtbox_city_bank.Text = dt.Rows[0]["Bank_city"].ToString();
                                    txtbox_bankzip.Text = dt.Rows[0]["Bank_Zipcode"].ToString();
                                    txtbox_bankstate.Text = dt.Rows[0]["Bank_State"].ToString();
                                    txtbox_bankname.Text = dt.Rows[0]["BankName"].ToString();
                                    txtbox_bankaccountno.Text = dt.Rows[0]["BankAccountNo"].ToString();
                                    txtbox_accounttype.Text = dt.Rows[0]["BankAccType"].ToString();
                                    txtbox_branch.Text = dt.Rows[0]["BranchName"].ToString();
                                    txtbox_routingno.Text = dt.Rows[0]["BankRoutingNo"].ToString();
                                }
                            }
                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "1")
                            {
                                // rbtn_creditcard.Checked = true;
                                lbl_billingtype.Text = "(Credit Card Payment)";
                            }

                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "2")
                            {
                                //  rbtn_invoicebymail.Checked = true;
                                lbl_billingtype.Text = "(Invoice By mail)";
                            }
                            if (dtdetailsforupdate.Rows[0]["Billingwith"].ToString() == "3")
                            {
                                // rbtn_invoicebyemail.Checked = true;
                                lbl_billingtype.Text = "(Invoice By Email)";
                            }

                            DataTable dtdetails = new clsInsert().getparkersotherDetails(Session["Login"].ToString());

                            if (dtdetails.Rows.Count > 0)
                            {

                                txtbox_sitename.Text = dtdetails.Rows[0]["LotName"].ToString();
                                txtbox_deliverymethod_corporate.Text = dtdetails.Rows[0]["CardDeliveryName"].ToString();
                            }

                            DataTable dt_plandetail = new clsInsert().getparkersplan(Session["Login"].ToString());

                            if (dt_plandetail.Rows.Count > 0)
                            {

                                if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "1")
                                {
                                    Txtbox_typ_corporate.Text = "Reserved Parking";
                                }

                                if (dt_plandetail.Rows[0]["Parkingtype"].ToString() == "2")
                                {
                                    Txtbox_typ_corporate.Text = "Random Parking";
                                }

                                if (dt_plandetail.Rows[0]["chargeby"].ToString() == "1")
                                {
                                    txtbox_charge_quantity_corporate.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Hour";
                                }

                                if (dt_plandetail.Rows[0]["chargeby"].ToString() == "2")
                                {
                                    txtbox_charge_quantity_corporate.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Days";
                                }

                                if (dt_plandetail.Rows[0]["chargeby"].ToString() == "3")
                                {
                                    txtbox_charge_quantity_corporate.Text = "For " + dt_plandetail.Rows[0]["Quantity"].ToString() + " " + "Month";
                                }
                                //lbl_Qunatity.Text = dt_plandetail.Rows[0]["Quantity"].ToString();
                                txtbox_amount_plan_corporate.Text = dt_plandetail.Rows[0]["Amount"].ToString() + " $";
                            }
                        
                        }

                    }
                    else
                    {
                    }
                }
                catch
                {
                }
            }

        }
    }
    protected void btn_next2_Click(object sender, EventArgs e)
    {

        //btn_addvehicledetails.Enabled = true;

        //btn_personaldetails.Enabled = true;

        MainView.ActiveViewIndex = 1;
    }
    
    protected void btn_prev3_Click(object sender, EventArgs e)
    {

        //btn_addvehicledetails.Enabled = true;
     
        //btn_personaldetails.Enabled = true;

        MainView.ActiveViewIndex = 1;
    }
    protected void rbtn_creditcard_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_invoicebymail0_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_invoicebyemail0_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_ach_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rb_account1_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rb_account_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void btn_entercreditcard_Click(object sender, EventArgs e)
    {

    }
    protected void btn_next3_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 2;
    }
    protected void btn_personaldetails_Click(object sender, EventArgs e)
    {
        btn_personaldetails.CssClass = "Clicked";
        btn_addvehicledetails.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void btn_addvehicledetails_Click(object sender, EventArgs e)
    {

        btn_personaldetails.CssClass = "Initial";
        btn_addvehicledetails.CssClass = "Clicked";
       


        btn_addvehicledetails.Enabled = true;

        btn_personaldetails.Enabled = true;

        MainView.ActiveViewIndex = 2;
    }
    protected void btn_plan_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 1;
    }
    protected void btn_prelast_Click(object sender, EventArgs e)
    {
        MainView.ActiveViewIndex = 0;
    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
     
        try
        {
           // DataTable dt = (DataTable)Session["datatable"];
            string msg = "1";
            UpdateProfile();
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_SearchParker.aspx?msg=" + msg);
            }
            if (Session["Type"].ToString() == "operator")
            {
                Response.Redirect("Search_ParkerByOperator.aspx?msg=" + msg);
            }

            if (Session["Type"].ToString() == "individual")
            {
                Response.Redirect("Deshboard.aspx?msg=" + msg);
            }

            if (Session["Type"].ToString() == "Corporate")
            {
                Response.Redirect("Deshboard.aspx?msg=" + msg);
            }
            //clsInsert cs = new clsInsert();
            //DataSet ds1 = cs.select_operation("select Parker_id from tbl_ParkerRegis where UserName='" + Session["UserName"].ToString() + "'order by  Parker_id desc");

            //int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
            //try
            //{



            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {

            //        hst.Clear();
            //        hst.Add("parkeridforvehicle", lastid);

            //        hst.Add("VehicelUserName", Session["UserName"].ToString());
            //        hst.Add("Make", dt.Rows[i]["VehicalMake"].ToString());
            //        hst.Add("model", dt.Rows[i]["VehicalModel"].ToString());
            //        hst.Add("year", dt.Rows[i]["VehicalYear"].ToString());
            //        hst.Add("color", dt.Rows[i]["VehicalColor"].ToString());
            //        hst.Add("licenseno", dt.Rows[i]["VehicalLicensePlate"].ToString());
            //        int result = objData.ExecuteNonQuery("[sp_VehicleParker]", CommandType.StoredProcedure, hst);
            //        if (result > 0)
            //        {
            //          //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('updated successfully done')", true);
            //            //Response.Redirect("UserManager.aspx");

            //        }
            //        else
            //        {
            //            // lbl_error.Visible = true;

            //        }
            //    }
            //    string msg = "1";
            //   // UpdateProfile();
            //    Response.Redirect("Admin_SettingMaster.aspx?msg=" + msg);
            //}
            //catch
            //{


            //}
        }
        catch
        {
            Response.Redirect("Admin_SearchParker.aspx");
        }
    }
    protected void ddl_country_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    public void UpdateProfile()
    {


      

        try
        {
            hst.Clear();

            hst.Add("action", "update");
           
            hst.Add("CardNo ", txtbox_coupannumber.Text);
            hst.Add("firstname", txtBox_firstname.Text);
            hst.Add("lastname", txtbox_lastname.Text);
            hst.Add("AdressLine1", txtbox_address1.Text);
            hst.Add("AddressLine2", txtbox_address2.Text);
            hst.Add("Zip_Parker",txtbox_postal.Text);
            hst.Add("Country_parker", ddl_country.SelectedItem.Text);
            hst.Add("State_Parker", ddl_province.SelectedItem.Text);
            hst.Add("city_parker", ddl_city.SelectedItem.Text);

            hst.Add("cell_parker", txtbox_cellular.Text);
            hst.Add("phone_parker", txtbox_phone.Text);
            hst.Add("fax", txt_fax.Text);
            hst.Add("email", txtbox_Email.Text);
            hst.Add("UserName", Session["UserName"].ToString().Trim());
            

            int result = objData.ExecuteNonQuery("[sp_Parker]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                

                //Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }

        }
        catch
        {

        }

    }
    protected void btn_addVehicle_Click(object sender, EventArgs e)
    {
     //   ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Update_IndividualParker_Vehicle.aspx','New Windows','height=380, width=300,location=no');", true);
    }
    //protected void rpt_parkervehicles_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    int i = e.Item.ItemIndex;
    //    if (e.CommandName == "cmdDelete")
    //    {
    //        int ID = Convert.ToInt32(e.CommandArgument.ToString());

    //        delvehicle(ID);
    //    }
    //    if (e.CommandName == "cmdEdit")
    //    {
    //        int ID = Convert.ToInt32(e.CommandArgument.ToString());
    //       // Label l = rpt1.Items[i].FindControl("Label1") as Label;
    //        TextBox make = rpt_parkervehicles.Items[i].FindControl("make") as TextBox;
    //        TextBox model = rpt_parkervehicles.Items[i].FindControl("model") as TextBox;
    //        TextBox color = rpt_parkervehicles.Items[i].FindControl("color") as TextBox;
    //        TextBox year = rpt_parkervehicles.Items[i].FindControl("year") as TextBox;
    //        TextBox licenseno = rpt_parkervehicles.Items[i].FindControl("licenseno") as TextBox;

    //        Session["vehicleid"] = ID;
    //        Session["make"] = make.Text;
    //        Session["model"] = model.Text;
    //        Session["color"] = color.Text;
    //        Session["year"] = year.Text;
    //        Session["licenseno"] = licenseno.Text;

    //      //  Response.Redirect("UpdateVehicle_IndividualParker_Vehicle.aspx");
    //       ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('UpdateVehicle_IndividualParker_Vehicle.aspx','New Windows','height=380, width=300,location=no');", true);
           
    //    }
    //}

    public void delvehicle(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "vehicledelete");
            hst.Add("parkervehicleid", id);


            int result = objData.ExecuteNonQuery("[sp_Parker]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                // Response.Redirect("Admin_CityMaster.aspx");

            }
            else
            {
                // Response.Redirect("Admin_CityMaster.aspx");
            }
        }
        catch
        {
            //  Response.Redirect("Admin_CityMaster.aspx");

        }


    }
    protected void btn_upd_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = "1";
            UpdateProfile();


            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_SearchParker.aspx?msg=" + msg);
            }
            if (Session["Type"].ToString() == "operator")
            {
                Response.Redirect("Search_ParkerByOperator.aspx?msg=" + msg);
            }

            if (Session["Type"].ToString() == "individual")
            {
                Response.Redirect("Deshboard.aspx?msg=" + msg);
            }

            if (Session["Type"].ToString() == "Corporate")
            {
                Response.Redirect("Deshboard.aspx?msg=" + msg);
            }
           
        }
        catch
        {
        }
    }
    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
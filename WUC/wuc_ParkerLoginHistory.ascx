﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_ParkerLoginHistory.ascx.cs" Inherits="WUC_wuc_ParkerLoginHistory" %>
 <style>
      html, body, #map-canvas {
        margin: 0px;
        padding: 0px
        }
    </style>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script type="text/javascript">
        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initialize() {
            // Create the autocomplete object, restricting the search
            // to geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
            // When the user selects an address from the dropdown,
            // populate the address fields in the form.
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                fillInAddress();
            });
        }

        // [START region_fillform]
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
        // [END region_fillform]

        // [START region_geolocation]
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
                    autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
                });
            }
        }
        // [END region_geolocation]

    </script>

    <style type="text/css">
      #locationField, #controls {
        position: relative;
        width: 480px;
      }
      #autocomplete {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 99%;
      }
      .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
      }
      #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
      }
      #address td {
        font-size: 10pt;
      }
      .field {
        width: 99%;
      }
      .slimField {
        width: 80px;
      }
      .wideField {
        width: 200px;
      }
      #locationField {
        height: 20px;
        margin-bottom: 2px;
      }
      .header
      {
          width:100%;
        color:#ec6d58;
        font-size:16px;
       
      }
     .address-list{ margin:0px; padding:0px; width:270px;} 
     .address-list td{ background:#fafafa; padding:5px 10px; font-size:13px; border-bottom:solid 1px #ededed; color:#707070;}
      .address-list td a{ color:#ec6d58; }
      
      .map-address{ width:400px; min-height:100px;}
      .address{ width:340px; float:left}
      .map-photo{ width:60px; height:70px; float:right}
       .address .button{padding:3px 5px !important; margin:10px 3px 0 0; font-size:11px;}
    </style>


 <script type="text/javascript">
    var markers = [
    <asp:Repeater ID="rptMarkers" runat="server">
    <ItemTemplate>
             {
                "title": '<%# Eval("CountryName") %>',
                "lat": '<%# Eval("Latitude") %>',
                "lng": '<%# Eval("Longitude") %>',
                "description": '<%# Eval("CityName") %>',
                
              
            }
    </ItemTemplate>
    <SeparatorTemplate>
        ,
    </SeparatorTemplate>
    </asp:Repeater>
    ];
 </script>
    <script type="text/javascript">

        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);

                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
<asp:Panel ID="Panel1" runat="server" Height="200px" ScrollBars="Vertical"  BorderWidth="2">
<fieldset>
<asp:Repeater runat="server" id="rpt1"  >
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        IP-Address
                    </th>
                     <th>
                        Country&nbsp;Name
                    </th>
                    <th>
                        City&nbsp;&nbsp;Name 
                    </th>
                    <th>
                       Region Name
                    </th>
                    <th>
                        Time_of_login
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("ipaddress")%>
                    </td><td>
                    <%#Eval("CountryName")%>
                </td>
                <td>
                <%#Eval("CityName")%>
                </td>
                <td>
                  <%#Eval("RegionName")%>
                </td>
                <td>
                    <%#Eval("Time_of_login")%>
                    
                </td>
               

                   
                        
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </fieldset>
</asp:Panel>

<br />
<asp:Panel ID="Panel2" runat="server" Height="394px" ScrollBars="Vertical"  BorderWidth="2" >
        <div class="map-area">
            
                 <div id="dvMap" class="map1">
                
    </div>
   </asp:Panel>
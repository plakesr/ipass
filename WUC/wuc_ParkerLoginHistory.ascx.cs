﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_ParkerLoginHistory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DataTable dt1 = new clsInsert().LoginHistory(Session["Login"].ToString());
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                rpt1.DataSource = dt1;
                rpt1.DataBind();
                
                rptMarkers.DataSource = dt1;
                rptMarkers.DataBind();
            }
        }
        catch
        {
        }
    }
}
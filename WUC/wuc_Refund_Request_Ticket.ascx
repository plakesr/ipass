﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Refund_Request_Ticket.ascx.cs" Inherits="WUC_wuc_Refund_Request_Ticket" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />
<link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 214px;
    }
    .style3
    {
        width: 845px;
    }
    .style4
    {
        width: 901px;
    }
    .style5
    {
        width: 227px;
    }
    .style7
    {
        width: 172px;
    }
    .style8
    {
        width: 1039px;
    }
    .style10
    {
        width: 480px;
    }
    .style13
    {
        width: 260px;
    }
    .style16
    {
        width: 2288px;
    }
    .style17
    {
        width: 280px;
    }
    .style18
    {
        width: 269px;
    }
       .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>
  
<div class="form-table">
    <asp:Panel ID="Panel1" runat="server">
   
 <fieldset>
 <legend>Personal Detail</legend>
     
     
    <table class="style1">
        <tr>
            <td class="style2">
                Select Site</td>
            <td class="style13">
               
                <asp:DropDownList ID="ddl_site" runat="server" AutoPostBack="True" 
                     CssClass="twitterStyleTextbox" 
                    onselectedindexchanged="ddl_site_SelectedIndexChanged" >
                </asp:DropDownList>
               
                
            </td>
            <td class="style7">
                Select Campus</td>
            <td class="style18">
                <asp:DropDownList ID="ddl_campus" runat="server" AutoPostBack="True" 
                    CssClass="twitterStyleTextbox" 
                    onselectedindexchanged="ddl_campus_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                Select Lot</td>
            <td>
                <asp:DropDownList ID="ddl_lot" runat="server" CssClass="twitterStyleTextbox">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style2">
                First Name</td>
            <td class="style13">
               
                <asp:TextBox ID="txtbox_firstname" runat="server" 
                    CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_f_name" runat="server" 
                    ControlToValidate="txtbox_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
               
               
            </td>
            <td class="style7">
                Last Name</td>
            <td class="style18">
                <asp:TextBox ID="txtbox_lastname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv__l_name" runat="server" 
                    ControlToValidate="txtbox_lastname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td>
                Address</td>
            <td>
                <asp:TextBox ID="txtbox_address" runat="server" CssClass="twitterStyleTextbox" 
                    style=" resize:none;" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                City</td>
            <td class="style13">
               
                <asp:TextBox ID="txtbox_city" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
               
               
            </td>
            <td class="style7">
                Province</td>
            <td class="style18">
                <asp:TextBox ID="txtbox_province" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                Postal Code</td>
            <td>
                <asp:TextBox ID="txtbox_postalcode" runat="server" 
                    CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Cell Number</td>
            <td class="style13">
                <asp:TextBox ID="txtbox_cellnumber" runat="server" 
                    CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_cellno" runat="server" 
                    ControlToValidate="txtbox_cellnumber" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style7">
                E-mail</td>
            <td class="style18">
                <asp:TextBox ID="txtbox_email" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                    ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="rev_email" runat="server" 
                    ControlToValidate="txtbox_email" ErrorMessage="Format is wrong" 
                    ForeColor="#CC0000" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                Licence Plate</td>
            <td class="style13">
                <asp:TextBox ID="txtbox_licence" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_licence" runat="server" 
                    ControlToValidate="txtbox_licence" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style7">
                APT/UNIT#</td>
            <td class="style18">
                <asp:TextBox ID="txtbox_apt" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                Customer Signature</td>
            <td>
                <asp:TextBox ID="txtboxfullname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_fullname" runat="server" 
                    ControlToValidate="txtboxfullname" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>
            </td>
        </tr>
        </table>
       
         </fieldset>
          
     </asp:Panel>
        <br />
     <asp:Panel ID="Panel2" runat="server">
     <fieldset>
     
     <legend>Refund Detail</legend>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


       <table>
        
        <tr>
      
            <td class="style16">
                Method of payment</td>
            <td class="style8">
              
                <asp:RadioButton ID="rbt_cash" runat="server" GroupName="r" Text="CASH" 
                    AutoPostBack="true" oncheckedchanged="rbt_cash_CheckedChanged" />
                &nbsp;
                <asp:RadioButton ID="rbt_debit" runat="server" GroupName="r" Text="DEBIT" 
                    AutoPostBack="true" oncheckedchanged="rbt_debit_CheckedChanged" />
                &nbsp;
                <asp:RadioButton ID="rbt_credit" runat="server" GroupName="r" Text="CREDIT" 
                    AutoPostBack="true" oncheckedchanged="rbt_credit_CheckedChanged" />
                  
            </td>
          
            <td class="style10">
                <asp:Label ID="lbl_credit_authorization" runat="server" 
                    Text="Credit Card Authorization Code" Visible="False"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txtbox_authorisation" runat="server" Visible="False"  CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
       
        </tr>
         
        <tr>
            <td class="style16">
                Device/Machine #</td>
            <td class="style3" colspan="2">
                <asp:TextBox ID="txtbox_machine" runat="server"  CssClass="twitterStyleTextbox" 
                    Height="14px" Width="111px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_machine" runat="server" 
                    ControlToValidate="txtbox_machine" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style16">
                TKT – Ticket No.</td>
            <td class="style3" colspan="2">
                <asp:TextBox ID="txtbox_ticketno" runat="server"  
                     Height="14px" Width="111px"></asp:TextBox>
              <%--  <cc1:AutoCompleteExtender ID="txtbox_ticketno_AutoCompleteExtender" MinimumPrefixLength="1"
                    runat="server" TargetControlID="txtbox_ticketno" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries" >
                </cc1:AutoCompleteExtender>--%>

               <%-- <cc1:AutoCompleteExtender ID="txtbox_ticketno_AutoCompleteExtender" runat="server" TargetControlID="txtbox_ticketno" 
MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries" >
</cc1:AutoCompleteExtender> --%>
                <asp:RequiredFieldValidator ID="rfv_ticketno" runat="server" 
                    ControlToValidate="txtbox_ticketno" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
            <tr>
                <td class="style16">
                    Total Amount Given&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $</td>
                <td class="style3">
                   
                    <asp:TextBox ID="txtbox_givenamopunt" runat="server" 
                        CssClass="twitterStyleTextbox" Height="14px" Width="111px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_totalamountgiven" runat="server" 
                        ControlToValidate="txtbox_ticketno" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                   
                </td>
              
                <td class="style3">
                    Total Amount To Be Refund&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $</td>
                <td class="style3">
                    <asp:TextBox ID="txtbox_refundamount" runat="server"  CssClass="twitterStyleTextbox"
                        ontextchanged="txtbox_refundamount_TextChanged" AutoPostBack="true" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_refundamount" runat="server" 
                        ControlToValidate="txtbox_refundamount" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblerr" runat="server" Text="" Visible="false"></asp:Label>
                </td>
                      
            </tr>
        <tr>
            <td class="style16">
                Attach Original Recepit &amp; Ticket</td>
            <td class="style3" colspan="2">
                <asp:FileUpload ID="originalrecepit" runat="server" />
                <asp:RequiredFieldValidator ID="rfv_attachoriginal" runat="server" 
                    ControlToValidate="originalrecepit" ErrorMessage="*Please Upload Original Receipt" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
          
        </tr>
        <tr>
            <td class="style16">
                Date of transaction charge</td>
            <td class="style3" colspan="2">
                <asp:TextBox ID="txtbox_transcationdate" runat="server"   CssClass="disable_future_dates"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtbox_transcationdate">
                </cc1:CalendarExtender>
                <asp:RequiredFieldValidator ID="rfv_date" runat="server" 
                    ControlToValidate="txtbox_transcationdate" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
           <asp:UpdatePanel ID="UpdatePanel2" runat="server">
           <ContentTemplate>
        <tr>

            <td class="style16">
                Reason for the refund request</td>
            <td class="style3" colspan="2">
                <asp:DropDownList ID="ddl_reason" runat="server" AutoPostBack="True"  CssClass="twitterStyleTextbox"
                    onselectedindexchanged="ddl_reason_SelectedIndexChanged">
                    <asp:ListItem> Patient Passed away / Discharged / Staff no longer work</asp:ListItem>
                    <asp:ListItem> Bought Mistakenly wrong pass</asp:ListItem>
                    <asp:ListItem> Buying multiple tickets at pay and display</asp:ListItem>
                    <asp:ListItem>Using Credit Cards on way in but not on way out</asp:ListItem>
                    <asp:ListItem> Paid an old Ticket</asp:ListItem>
                    <asp:ListItem>Paystation Short changes with or with out credit note, hopper/dispenser error</asp:ListItem>
                    <asp:ListItem> P&amp;D Machines -Parker wrongly usese Max Button</asp:ListItem>
                    <asp:ListItem>Visitor wrongly purchased multi-use pass, but wanted to buy short term ticket</asp:ListItem>
                    <asp:ListItem> Appointment Cancelled</asp:ListItem>
                    <asp:ListItem>Waited the pay station to be serviced then went over time limit</asp:ListItem>
                    <asp:ListItem> Holiday rate not in effect / Programming error i.e.Holiday rate not programmed</asp:ListItem>
                    <asp:ListItem>Other</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:TextBox ID="txtbox_reason" runat="server" Height="45px"  CssClass="twitterStyleTextbox" 
                    style=" resize:none;" TextMode="MultiLine" Width="485px" Visible="False"></asp:TextBox>
                     <cc1:TextBoxWatermarkExtender ID="txtbox_vehicleType_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_reason" 
                        WatermarkCssClass="watermark" WatermarkText="Enter Reason">
                    </cc1:TextBoxWatermarkExtender>
                
               <%--  <asp:RequiredFieldValidator ID="rfv_reason" runat="server" 
                    ControlToValidate="txtbox_reason" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>--%>
  
                
            </td>
        </tr>
        </ContentTemplate>
           </asp:UpdatePanel>
            <tr>
                <td class="style16">
                    &nbsp;</td>
                <td class="style3" colspan="2">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    <cc1:AutoCompleteExtender ID="TextBox1_AutoCompleteExtender" runat="server" 
                        DelimiterCharacters="" Enabled="True" ServicePath="" TargetControlID="TextBox1" MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries">
                    </cc1:AutoCompleteExtender>

                     <%-- <cc1:AutoCompleteExtender ID="txtbox_ticketno_AutoCompleteExtender" runat="server" TargetControlID="txtbox_ticketno" 
MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries" >
</cc1:AutoCompleteExtender> --%>
                </td>
            </tr>
    </table>


     
      </ContentTemplate>
                     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rbt_credit" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="txtbox_refundamount" EventName="TextChanged" />
       

    </Triggers>
                </asp:UpdatePanel>
    </fieldset>
       </asp:Panel>
       <table>
       <tr>
       <td class="style5">
           &nbsp;</td>
       <td class="style4">
       <asp:Button ID="btn_send" runat="server" Text="Send Request" CssClass="button" 
               ValidationGroup="a" onclick="btn_send_Click" />
           </td>
       </tr>
       </table>
</div>
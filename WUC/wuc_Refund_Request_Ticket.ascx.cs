﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Security.Cryptography;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;
public partial class WUC_wuc_Refund_Request_Ticket : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    clsInsert cs = new clsInsert();

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Fillddlsites();
        }
    }

    string ImageCustomerSign = "";
    string ImageAttachementReceipt = "";




   
    public static List<string> GetCountries(string prefixText)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
        con.Open();
        SqlCommand cmd = new SqlCommand("select LotName from tbl_LotMaster where LotName like ''+@Name+'%'", con);
        cmd.Parameters.AddWithValue("@Name", prefixText);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }









    public void Fillddlsites()
    {

        List<clsData> lst = new clsInsert().GetSiteslist(0);
        if (lst.Count() > 0)
        {
            ddl_site.DataSource = lst;
            ddl_site.DataTextField = "SiteName";
            ddl_site.DataValueField = "SiteId";
            ddl_site.DataBind();
        }
        ddl_site.Items.Insert(0, new ListItem("Select Sites", "0"));

    }


    public void Fillddlcampus(int siteid)
    {

        List<clsData> lst = new clsInsert().GetCampuslist(siteid);
        if (lst.Count() > 0)
        {
            ddl_campus.DataSource = lst;
            ddl_campus.DataTextField = "campusname";
            ddl_campus.DataValueField = "campusid";
            ddl_campus.DataBind();
        }
        ddl_campus.Items.Insert(0, new ListItem("Select Campus", "0"));

    }

    public void Fillddllots(int campusid)
    {

        List<clsData> lst = new clsInsert().Getlotslist(campusid);
        if (lst.Count() > 0)
        {
            ddl_lot.DataSource = lst;
            ddl_lot.DataTextField = "lotname";
            ddl_lot.DataValueField = "lotid";
            ddl_lot.DataBind();
        }
        ddl_lot.Items.Insert(0, new ListItem("Select Lots", "0"));

    }
    public string Get5Digits()
    {
        var bytes = new byte[4];
        var rng = RandomNumberGenerator.Create();
        rng.GetBytes(bytes);
        uint random = BitConverter.ToUInt32(bytes, 0) % 100000;
        return String.Format("{0:D5}", random);
    }
    string refundtrackingid = "";
    public void addcustomerrefund()
    {
        try
        {
            hst.Clear();
            //@f_name,@l_name,@Apt,@Add,@city,@province,@postalcode,@customersign,@phoneno,@email,@licenseplate,@RefundTrackingid,@billing)
            hst.Add("action", "insert");
            hst.Add("f_name", txtbox_firstname.Text);
            hst.Add("l_name",txtbox_lastname.Text);
            hst.Add("Apt", txtbox_apt.Text);
            hst.Add("Add", txtbox_address.Text);
            hst.Add("city", txtbox_city.Text);
            hst.Add("province", txtbox_province.Text);
            hst.Add("postalcode", txtbox_postalcode.Text);
            //if (customersign.HasFile)
            //{
            //    string oFName = customersign.FileName.ToString().Trim();
            //    string[] strArr = oFName.Split('.');
            //    int sLength = strArr.Length;
            //    if (strArr[sLength - 1] == "pdf"||strArr[sLength - 1] == "PDF")
            //    {
            //        ImageCustomerSign = txtbox_firstname.Text + "_" + txtbox_lastname.Text + "." + strArr[1];
            //        string Path = Server.MapPath("~/CustomerSign/");
            //        customersign.SaveAs(Path + ImageCustomerSign);
            //    }

            //    else
            //    {
            //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload pdf only.')", true);
            //        return;
            //    }
            //}
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload pdf of sign .')", true);

            //}

            hst.Add("customersign", txtboxfullname.Text);
            hst.Add("phoneno", txtbox_cellnumber.Text);
            hst.Add("email", txtbox_email.Text);
            hst.Add("licenseplate", txtbox_licence.Text);
          //  refundtrackingid = Get5Digits().ToString();
            hst.Add("RefundTrackingid",Convert.ToInt64 (refundtrackingid));
            if (rbt_cash.Checked == true)
            {
                hst.Add("billing", 1);
            }

            if (rbt_credit.Checked == true)
            {
                hst.Add("billing", 3);
            }
            if (rbt_debit.Checked == true)
            {
                hst.Add("billing", 2);
            }
            int result = objData.ExecuteNonQuery("[sp_Refund_Customer]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);



            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);



        }

    }
     clsData obj = new clsData();
     decimal defaultvalue =Convert.ToDecimal(0.00);
     string status_paid = "false";
    protected void btn_send_Click(object sender, EventArgs e)
    {
        try
        {
            
            refundtrackingid = Get5Digits().ToString();
           
            bool b = obj.mail(txtbox_email.Text, "Tracking ID: " + refundtrackingid ,"Request are send successfully" + System.DateTime.Now.ToString());
            if (b == true)
            {



                addcustomerrefund();

                hst.Clear();
                hst.Add("action", "insert");

                hst.Add("RefundTrackngid", refundtrackingid);

                DataSet ds1 = cs.select_operation("select Refund_Customer_ID from tbl_Refund_Customer where e_mail='" + txtbox_email.Text + "'order by  Refund_Customer_ID desc");

                int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
                hst.Add("refundcustomrid", Convert.ToInt32(lastid));


                if (originalrecepit.HasFile)
                {
                    string oFName = originalrecepit.FileName.ToString().Trim();
                    string[] strArr = oFName.Split('.');
                    int sLength = strArr.Length;
                    if (strArr[sLength - 1] == "pdf" || strArr[sLength - 1] == "PDF" )
                    {
                        ImageAttachementReceipt = refundtrackingid + "." + strArr[1];
                        string Path = Server.MapPath("~/CustomerReceipt/");
                        originalrecepit.SaveAs(Path + ImageAttachementReceipt);

                    }

                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload PDF only.')", true);
                        return;
                    }
                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload pdf of receipt.')", true);

                }
                hst.Add("attachment", ImageAttachementReceipt);

                int result1 = objData.ExecuteNonQuery("[sp_Refund_Attachment]", CommandType.StoredProcedure, hst);

                if (result1 > 0)
                {
                    hst.Clear();
                   
                   
                    hst.Add("devicemachineid", txtbox_machine.Text);
                    hst.Add("ticketno", txtbox_ticketno.Text);
                    hst.Add("attchment", ImageAttachementReceipt);
                    hst.Add("licensplate", txtbox_licence.Text);
                    hst.Add("dateofreq", DateTime.Now);
                    hst.Add("dateofreceipt", Convert.ToDateTime(txtbox_transcationdate.Text));
                    if (ddl_reason.SelectedItem.Text == "Other")
                    {
                        hst.Add("reasondofrefund", txtbox_reason.Text);
                    }
                    else
                    {
                        hst.Add("reasondofrefund",ddl_reason.SelectedItem.Text );
                    }
                   
                    if (rbt_cash.Checked == true)
                    {
                        hst.Add("billingtype", 1);
                        hst.Add("authorisationcode", txtbox_authorisation.Text);

                    }

                    if (rbt_credit.Checked == true)
                    {
                        hst.Add("billingtype", 3);
                        hst.Add("authorisationcode", txtbox_authorisation.Text);
                    }
                    if (rbt_debit.Checked == true)
                    {
                        hst.Add("billingtype", 2);
                        hst.Add("authorisationcode", txtbox_authorisation.Text);

                    }

                    hst.Add("refundtrckingid", Convert.ToInt32(refundtrackingid));

                    hst.Add("refundcustmerid", lastid);
                    hst.Add("requst", 1);

                    hst.Add("refund_category", "");
                    hst.Add("business", "");

                   
                    hst.Add("siteid", ddl_site.SelectedValue);
                    hst.Add("lotid", ddl_lot.SelectedValue);
                    hst.Add("campus", ddl_campus.SelectedValue);

                    hst.Add("totalamount", Convert.ToDecimal(txtbox_givenamopunt.Text));
                    hst.Add("refundamount", Convert.ToDecimal(txtbox_refundamount.Text));

                    int result2 = objData.ExecuteNonQuery("[sp_RefundTranscation]", CommandType.StoredProcedure, hst);
                    if (result2 > 0)
                    {
                        hst.Clear();


                        hst.Add("requestTrackingid", Convert.ToInt32(refundtrackingid));
                        hst.Add("requestcustomerid", lastid);

                        int result3 = objData.ExecuteNonQuery("[sp_Refundstatustransact]", CommandType.StoredProcedure, hst);
                        if (result3 > 0)
                        {
                            try
                            {
                                hst.Clear();
                                hst.Add("action", "insert");
                                hst.Add("refundtrackingid", Convert.ToInt32(refundtrackingid));
                                hst.Add("refundcustomerid", lastid);
                                hst.Add("amountrefund", defaultvalue);
                                hst.Add("dateofrefund", DateTime.Now);
                                hst.Add("paidby", "");
                                hst.Add("checknumber", "0");
                                hst.Add("clienttype", "");

                                if (rbt_cash.Checked == true)
                                {

                                    hst.Add("creditcardathcode", "");

                                }

                                if (rbt_credit.Checked == true)
                                {

                                    hst.Add("creditcardathcode", txtbox_authorisation.Text);
                                }
                                if (rbt_debit.Checked == true)
                                {

                                    hst.Add("creditcardathcode", "");

                                }
                              
                                hst.Add("status_paid", status_paid);

                                int result_refundpaidstatus = objData.ExecuteNonQuery("[sp_refundpaidstatus]", CommandType.StoredProcedure, hst);
                                if (result_refundpaidstatus > 0)
                                {
                                }
                            }
                            catch
                            {
                            }



                            try
                            {
                                Session["RefundForRequest"] = "refund successfuly";
                                Session["Tracking"] = Convert.ToString(refundtrackingid);

                                Response.Redirect("Default.aspx");

                            }
                            catch
                            {
                                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                            }
                        }
                    }

                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Email Id not exist.')", true);

            }

        }
        
        catch
        {
        }
    }
    protected void rbt_cash_CheckedChanged(object sender, EventArgs e)
    {
        if (rbt_cash.Checked == true)
        {
            lbl_credit_authorization.Visible = false;
            txtbox_authorisation.Visible = false;
        }
    }
    protected void rbt_debit_CheckedChanged(object sender, EventArgs e)
    {
        if (rbt_debit.Checked == true)
        {
            lbl_credit_authorization.Visible = false;
            txtbox_authorisation.Visible = false;
        }
    }
    protected void rbt_credit_CheckedChanged(object sender, EventArgs e)
    {
        if (rbt_credit.Checked == true)
        {
           
            lbl_credit_authorization.Visible = true;
            txtbox_authorisation.Visible = true;
        }
        else
        {
            lbl_credit_authorization.Visible = false;
            txtbox_authorisation.Visible = false;
        }
    }
    protected void txtbox_refundamount_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToDecimal(txtbox_givenamopunt.Text) < Convert.ToDecimal(txtbox_refundamount.Text))
            {
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please insert the value less than given amount.')", true);

                lblerr.Visible = true;
                lblerr.Text = "Please Money Less Than :$" + txtbox_givenamopunt.Text;
                txtbox_refundamount.ForeColor = Color.Red;
            }
            if (Convert.ToDecimal(txtbox_givenamopunt.Text) > Convert.ToDecimal(txtbox_refundamount.Text))
            {
                lblerr.Visible = false;
                txtbox_refundamount.ForeColor = Color.Green;
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please donot insert any alphabetic value')", true);
        }
    }





    protected void ddl_campus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Fillddllots(Convert.ToInt32(ddl_campus.SelectedValue));
        }
        catch
        {
        }
    }
    protected void ddl_site_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Fillddlcampus(Convert.ToInt32(ddl_site.SelectedValue));
        }
        catch
        {
        }
        
    }
    protected void txtbox_reason_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddl_reason_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_reason.SelectedItem.Text == "Other")
        {
            txtbox_reason.Visible = true;
        }
        else
        {
            txtbox_reason.Visible = false;
        }
    }
}
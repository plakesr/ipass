﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Search_Operator.ascx.cs" Inherits="WUC_wuc_Search_Operator" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 178px;
    }
</style>
<div>
    <table class="style1">
        <tr>
            <td class="style2">
                <asp:CheckBox ID="chk_lot" runat="server" Text="Select Lot" AutoPostBack="true" 
                    oncheckedchanged="chk_lot_CheckedChanged"  />
            </td>
            <td>
               <asp:DropDownList 
                    ID="ddl_lotname" runat="server" DataSourceID="SqlDataSource1"  CssClass="twitterStyleTextbox"
                    DataTextField="LotName" DataValueField="Lot_id" Enabled="False">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [Lot_id], [LotName] FROM [tbl_LotMaster] order by LotName ">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:CheckBox ID="chk_operatorname" runat="server" Text="Operator name" 
                    AutoPostBack="true" oncheckedchanged="chk_operatorname_CheckedChanged" />
            </td>
            <td>
                <asp:TextBox ID="txtbox_operatorname" runat="server" Enabled="False" CssClass="twitterStyleTextbox" Width="285"></asp:TextBox>
            </td>
        </tr>
      
        <tr>
            <td class="style2">
                <asp:Label ID="Result" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button"
                    onclick="btn_search_Click" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Repeater ID="Repeater1" runat="server"  onitemcommand="Repeater1_ItemCommand" >
<HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        User Name
                    </th>
                     <th>
                        Name
                    </th>
                     <th>
                      Designation
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                    <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
           <tr>
            <td>
                    <%#Eval("username")%>
                    </td><td>
                    <%#Eval("Name")%>
                    

                </td>
                <td><%#Eval("Designation")%></td>
               <td>
                   <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("username") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td> 
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("username") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>
                    <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("username") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("username") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td>
                    
                    
                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("username") %>' 
                                        Visible="False" />
                     <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("Name") %>' 
                                        Visible="False"></asp:Label>
                     <asp:Label ID="idLabel2" runat="server" Text='<%# Eval("Dob") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel3" runat="server" Text='<%# Eval("Email") %>' 
                                        Visible="False"></asp:Label>  
                      <asp:Label ID="idLabel4" runat="server" Text='<%# Eval("Mob") %>' 
                                        Visible="False" />
                    <asp:Label ID="idLabel5" runat="server" Text='<%# Eval("Gender") %>' 
                                        Visible="False"></asp:Label>
                    <asp:Label ID="idLabel6" runat="server" Text='<%# Eval("Address") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel7" runat="server" Text='<%# Eval("Designation") %>' 
                                        Visible="False"></asp:Label>    
                    <asp:Label ID="idLabel8" runat="server" Text='<%# Eval("Status") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel9" runat="server" Text='<%# Eval("city") %>' 
                                        Visible="False"></asp:Label>    
                    <asp:Label ID="idLabel10" runat="server" Text='<%# Eval("Username") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel11" runat="server" Text='<%# Eval("Pass") %>' 
                                        Visible="False"></asp:Label>    
                    <asp:Label ID="idLabel12" runat="server" Text='<%# Eval("status") %>' 
                                        Visible="False"></asp:Label>  
                    <asp:Label ID="idLabel13" runat="server" Text='<%# Eval("type") %>' 
                                        Visible="False"></asp:Label>                         

            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
</div>
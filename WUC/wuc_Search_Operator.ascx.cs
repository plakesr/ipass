﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_Search_Operator : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (chk_operatorname.Checked==true)
        {
         
            DataTable dt = new clsInsert().getoperator(txtbox_operatorname.Text);
            int count = dt.Rows.Count;
            if (dt.Rows.Count > 0)
            {
                Repeater1.Visible = true;

                Result.Visible = true;
                Result.Text = count.ToString() + " " + "Records Found";

                Repeater1.DataSource = dt;
                Repeater1.DataBind();

            }
            else 
            {
                Result.Visible = false;
                Repeater1.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);


            }
        }
        if (chk_lot.Checked == true)
        {
            
            DataTable dt1 = new clsInsert().getoperatorbylot(Convert.ToInt32(ddl_lotname.SelectedItem.Value),txtbox_operatorname.Text);
            int count = dt1.Rows.Count;
            if (dt1.Rows.Count > 0)
            {
                Repeater1.Visible = true;

                Result.Visible = true;
                Result.Text = count.ToString() + " " + "Records Found";

                Repeater1.DataSource = dt1;
                Repeater1.DataBind();

            }
            else
            {
                Result.Visible = false;
                Repeater1.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);


            }
        }
        if (chk_lot.Checked == true && chk_operatorname.Checked == true)
        {

            DataTable dt2 = new clsInsert().getoperatorbylotandname(txtbox_operatorname.Text, Convert.ToInt32(ddl_lotname.SelectedItem.Value));
            int count = dt2.Rows.Count;
            if (dt2.Rows.Count > 0)
            {
                Repeater1.Visible = true;

                Result.Visible = true;
                Result.Text = count.ToString() + " " + "Records Found";

                Repeater1.DataSource = dt2;
                Repeater1.DataBind();

            }
            else
            {
                Result.Visible = false;
                Repeater1.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);


            }
        }
    }
    protected void chk_lot_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_lot.Checked == true)
        {
            txtbox_operatorname.Text = "";
            ddl_lotname.Enabled = true;
        }
        else
        {
            
            ddl_lotname.Enabled = false;
        }
    }
    protected void chk_operatorname_CheckedChanged(object sender, EventArgs e)
    {

        if (chk_operatorname.Checked == true)
        {
            txtbox_operatorname.Enabled = true;
        }
        else
        {
            txtbox_operatorname.Text = "";
            txtbox_operatorname.Enabled = false;
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        if (e.CommandName == "cmdDelete")
        {
            string user = e.CommandArgument.ToString();

            delUser(user);
        }

        if (e.CommandName == "cmdinactive")
        {
            string user = e.CommandArgument.ToString();

            updateuserstatus(user);

        }

        if (e.CommandName == "cmdactive")
        {
            string user = e.CommandArgument.ToString();

            activeuserstatus(user);

        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = Repeater1.Items[i].FindControl("idLabel") as Label;
            Label l1 = Repeater1.Items[i].FindControl("idLabel1") as Label;

            Label l2 = Repeater1.Items[i].FindControl("idLabel2") as Label;
            Label l3 = Repeater1.Items[i].FindControl("idLabel3") as Label;

            Label l4 = Repeater1.Items[i].FindControl("idLabel4") as Label;
            Label l5 = Repeater1.Items[i].FindControl("idLabel5") as Label;

            Label l6 = Repeater1.Items[i].FindControl("idLabel6") as Label;
            Label l7 = Repeater1.Items[i].FindControl("idLabel7") as Label;


            Label l8 = Repeater1.Items[i].FindControl("idLabel8") as Label;
            Label l9 = Repeater1.Items[i].FindControl("idLabel9") as Label;

            Label l10 = Repeater1.Items[i].FindControl("idLabel10") as Label;
            Label l11 = Repeater1.Items[i].FindControl("idLabel11") as Label;

            //    Label l12 = rpt1.Items[i].FindControl("idLabel12") as Label;
            //    Label l13 = rpt1.Items[i].FindControl("idLabel13") as Label;




            Session["username"] = l.Text;
            Session["Name"] = l1.Text;
            Session["Dob"] = l2.Text;
            Session["Email"] = l3.Text;

            Session["Mob"] = l4.Text;
            Session["Gender"] = l5.Text;
            Session["Address"] = l6.Text;
            Session["Designation"] = l7.Text;

            Session["Status"] = l8.Text;
            Session["city"] = l9.Text;
            Session["loginame"] = l10.Text;
            Session["Pass"] = l11.Text;

            //  Session["userstatus"] = l12.Text;
            //    Session["type"] = l13.Text;
            Response.Redirect("Admin_EditUserPermissionMaster.aspx");

        }

    }

    string userstattus1 = "false";
    public void updateuserstatus(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "updatestatus");
            //hst.Add("ustatus", userstattus1);
            hst.Add("username", username);



            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
              //  Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
           // Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }


    public void activeuserstatus(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "activestatus");
            //hst.Add("ustatus", userstattus1);
            hst.Add("username", username);



            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                //Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
           // Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }


    public void delUser(string username)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("userloginname", username);
            hst.Add("username", username);
            hst.Add("menuusername", username);


            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
              //  Response.Redirect("Admin_UserPermissionMaster.aspx");
            }
        }
        catch
        {
           // Response.Redirect("Admin_UserPermissionMaster.aspx");

        }


    }
}
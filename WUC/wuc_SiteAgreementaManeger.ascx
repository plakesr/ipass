﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_SiteAgreementaManeger.ascx.cs" Inherits="WUC_wuc_SiteAgreementaManeger" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
    width:222px;
    resize:none;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
</style>
    <table class="form-table">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </cc1:ToolkitScriptManager>
                <tr>
            <td class="style2">
                
                Site Name:</td>
            <td>
                <asp:DropDownList ID="ddl_site" runat="server" DataSourceID="SqlDataSource1" 
                    DataTextField="SiteName" DataValueField="SiteId" CssClass=twitterStyleTextbox>
                </asp:DropDownList>
                
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
                </asp:SqlDataSource>
                
                <asp:RequiredFieldValidator ID="rfv_site" runat="server" 
                    ControlToValidate="ddl_site" ErrorMessage="*" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>

    <tr>
            <td class="style2">
                
                Agreement type:</td>
            <td>
                <asp:DropDownList ID="ddl_agreementtype" runat="server" 
                    DataSourceID="SqlDataSource2" DataTextField="AgreementTypeName" CssClass=twitterStyleTextbox
                    DataValueField="AgreementTypeId">
                </asp:DropDownList>
                
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [AgreementTypeId], [AgreementTypeName] FROM [tbl_AgreementTypeMaster]">
                </asp:SqlDataSource>
                
                <asp:RequiredFieldValidator ID="rfv_agreementtype" runat="server" 
                    ControlToValidate="ddl_agreementtype" ErrorMessage="*" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td class="style2">
                Start Date:</td>
            <td>
                <asp:TextBox ID="txtbox_startdate" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                <cc1:CalendarExtender ID="txtbox_startdate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtbox_startdate">
                </cc1:CalendarExtender>
                <asp:RequiredFieldValidator ID="rfv_startdate" runat="server" 
                    ControlToValidate="txtbox_startdate" ErrorMessage="*" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                End Date:</td>
            <td>
                <asp:TextBox ID="txtbox_enddate" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                <cc1:CalendarExtender ID="txtbox_enddate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtbox_enddate">
                </cc1:CalendarExtender>
                <asp:RequiredFieldValidator ID="rfv_enddate" runat="server" 
                    ControlToValidate="txtbox_enddate" ErrorMessage="*" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Comments:</td>
            <td>
                <asp:TextBox ID="txtbox_comments" runat="server" TextMode="MultiLine" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_comments" runat="server" 
                    ControlToValidate="txtbox_comments" 
                    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_submit" runat="server" Text="Add" CssClass=button 
                    onclick="btn_submit_Click"   Height="35px" Width="79px"/>
            </td>
        </tr>
    </table>


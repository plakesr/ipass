﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_SiteManger.ascx.cs" Inherits="WUC_wuc_SiteManger" %>



 <link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>



 
<style type="text/css">
    .form-table
    {
        width: 97%;
    }
    .style8
    {
        width: 100%;
    }
        .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .form-table
    {
        width: 66%;
    }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
    }
    .style9
    {
        width: 252px;
    }
    .style10
    {
        width: 118px;
    }
    </style>

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:Panel ID="Panel4" runat="server">

<asp:Panel ID="Panel1" runat="server">
    <div class="form" style="float:none;">
         <fieldset>
         <legend>Customer Detail</legend>

    <label>Customer Name</label>
    <asp:TextBox ID="txtbox_SiteName" runat="server" ValidationGroup="a" 
                 CssClass="twitterStyleTextbox" AutoPostBack="true" 
                 ontextchanged="txtbox_SiteName_TextChanged"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_SiteName" runat="server" 
                                                       ControlToValidate="txtbox_SiteName" 
                 ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="b"></asp:RequiredFieldValidator>
                                                        <asp:Label ID="lblerror" 
                 runat="server" Visible="False" ForeColor="#FF3300"></asp:Label>
                                                        <div class="clearfix">
                                                        
                                                        
                                                           
                                                        
                                                        
                                                        </div>
<label>Time Zone</label>
 <asp:DropDownList ID="ddl_TimeZone" runat="server" AppendDataBoundItems="true"  CssClass="twitterStyleTextbox">
                                                    
                                                   </asp:DropDownList>
                                                   <%--<cc1:DropDownExtender ID="ddl_TimeZone_DropDownExtender" runat="server" 
                                                       DynamicServicePath="" Enabled="True" TargetControlID="ddl_TimeZone">
                                                   </cc1:DropDownExtender>--%>

                                                    
                                                   <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                                       ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                       SelectCommand="SELECT [TimeZoneTblId], [TimeZoneName] FROM [tbl_TimeZoneMaster] ">
                                                   </asp:SqlDataSource>
                                                    <asp:RequiredFieldValidator ID="rfv_timezone" runat="server" 
                 ControlToValidate="ddl_TimeZone" ErrorMessage="*" ForeColor="Red" 
                 ValidationGroup="b"></asp:RequiredFieldValidator>
                                                    <div class="clearfix"></div>
<label>Get Address</label>
   <asp:Button ID="Button1" runat="server" CssClass="button" 
                                                       onclick="Button1_Click1" Text="Get Geo-Location" />
                                                        <div class="clearfix">&nbsp;</div>
                                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                       <ContentTemplate>
                                                       <label>Country</label>
                                                        <asp:DropDownList ID="ddl_country" runat="server" AutoPostBack="True" 
                                                                           DataSourceID="SqlDataSource2" DataTextField="Country_name" 
                                                                           DataValueField="Country_id" CssClass="twitterStyleTextbox" >
                                                                          </asp:DropDownList>  
                                                                       <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                                                           ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                                           
                                                               SelectCommand="SELECT * FROM [tbl_country] ORDER BY [Country_name]"></asp:SqlDataSource>
                                                                            <div class="clearfix"></div>
                                                                           <label>Province</label>
                                                                           <asp:DropDownList ID="ddl_province" runat="server" AutoPostBack="True" 
                                                                           DataSourceID="SqlDataSource6" DataTextField="ProvinceName" 
                                                                           DataValueField="ProvinceId"  CssClass="twitterStyleTextbox" >
                                                                          
                                                                          </asp:DropDownList>  
                                                                       <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                                                                           ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                                           SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) ORDER BY [ProvinceName]">
                                                                           <SelectParameters>
                                                                               <asp:ControlParameter ControlID="ddl_country" Name="Country_id" 
                                                                                   PropertyName="SelectedValue" Type="Int64" />
                                                                           </SelectParameters>
                                                                       </asp:SqlDataSource>
                                                                        <div class="clearfix"></div>
                                                                       <label>City</label>
                                                                        <asp:DropDownList ID="ddl_city" runat="server"  CssClass="twitterStyleTextbox" 
                                                                           DataSourceID="SqlDataSource3" DataTextField="City_name" 
                                                                           DataValueField="City_id" >
                                                                          
                                                                       </asp:DropDownList>
                                                                       <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                                                           ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                                           SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id) ORDER BY [City_name] ">
                                                                           <SelectParameters>
                                                                               <asp:ControlParameter ControlID="ddl_province" Name="Province_id" 
                                                                                   PropertyName="SelectedValue" Type="Int64" />
                                                                           </SelectParameters>
                                                                       </asp:SqlDataSource>
                                                                       <%--<asp:DropDownList ID="ddl_city" runat="server" DataSourceID="SqlDataSource5"   CssClass="twitterStyleTextbox"
                                                                           DataTextField="City_name" DataValueField="City_id" AppendDataBoundItems="true">
                                                                            
                                                                       </asp:DropDownList>--%>
                                                                      
                                                                       <asp:RequiredFieldValidator ID="rfv_city" runat="server" 
                                                                           ControlToValidate="ddl_city" ErrorMessage="*" ForeColor="#CC0000" 
                                                                           ValidationGroup="b"></asp:RequiredFieldValidator>
                                                           
                                                       </ContentTemplate>
                                                   </asp:UpdatePanel>
                                                    <div class="clearfix"></div>
                                                   <label> Postal Code/Zip Code</label>
                                                    <asp:TextBox ID="txtbox_zip" runat="server" ValidationGroup="a"  CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_zip" runat="server" 
                                                       ControlToValidate="txtbox_zip" 
                 ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="b"></asp:RequiredFieldValidator>
             <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_zip" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator>
                                                        <div class="clearfix"></div>
                                                       <label> Phone Number(Home)</label>
                                                        <asp:TextBox ID="txtbox_Phome" runat="server" ValidationGroup="b"  CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_Phome" runat="server" 
                                                       ControlToValidate="txtbox_Phome" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="b"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                           ID="rgv_phone" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_Phome" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                                                        <div class="clearfix">
<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_Phome" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                                                        
                                                        </div>
                                                       <label>Cellular</label>
                                                        <asp:TextBox ID="txtbox_Cellular" runat="server" ValidationGroup="b"  CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_Cellular" runat="server" 
                                                       ControlToValidate="txtbox_Cellular" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="b"></asp:RequiredFieldValidator>
                                                        <div class="clearfix"><asp:RegularExpressionValidator
                                                           ID="RegularExpressionValidator2" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_Cellular" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator><cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                 runat="server" Enabled="True" TargetControlID="txtbox_Cellular" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                 </div>
                                                       <label> Phone Number(Office)</label>
                                                        <asp:TextBox ID="txtbox_poffice" 
                 runat="server" ValidationGroup="b"  CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <cc1:TextBoxWatermarkExtender ID="txtbox_poffice_TextBoxWatermarkExtender" 
                 runat="server" Enabled="True" TargetControlID="txtbox_poffice" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                                                   <asp:RequiredFieldValidator ID="rfv_poffice" runat="server" 
                                                       ControlToValidate="txtbox_poffice" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="b"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                           ID="RegularExpressionValidator1" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_poffice" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                                                       <div class="clearfix"></div>
                                                       <label>Email</label>
                                                        <asp:TextBox ID="txtbox_email" runat="server" ValidationGroup="b"  CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                                                       ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="b"></asp:RequiredFieldValidator>
     
             <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                 ErrorMessage="Enter Correct Format" ControlToValidate="txtbox_email" 
                 ForeColor="#FF3300" 
                 ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                       
        
         </fieldset>  
 </div>   
          
         </asp:Panel>


            
              <asp:Panel ID="Panel3" runat="server">
              <fieldset><legend>Contact Person Details </legend>
             <div class="form">
             <div class="clearfix">&nbsp;<table class="style8">
                 <tr>
                     <td class="style10">
                         <label>
                         Contact Person Name</label></td>
                     <td>
                         <asp:TextBox ID="txtbox_ownerName" runat="server" 
                             CssClass="twitterStyleTextbox" ontextchanged="txtbox_ownerName_TextChanged" 
                             ValidationGroup="c"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="rfv_ownername" runat="server" 
                             ControlToValidate="txtbox_ownerName" ErrorMessage="*" ForeColor="#CC0000" 
                             ValidationGroup="b"></asp:RequiredFieldValidator>
                     </td>
                 </tr>
                 <tr>
                     <td class="style10">
                         <label>
                         Cellular</label></td>
                     <td>
                         <asp:TextBox ID="txtbox_ownercellular" runat="server" 
                             CssClass="twitterStyleTextbox" ValidationGroup="b"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                             ControlToValidate="txtbox_ownercellular" ErrorMessage="*" ForeColor="#CC0000" 
                             ValidationGroup="b"></asp:RequiredFieldValidator>

                             <asp:RegularExpressionValidator
                                                           ID="RegularExpressionValidator5" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_ownercellular" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator><cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" 
                 runat="server" Enabled="True" TargetControlID="txtbox_ownercellular" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                     </td>
                 </tr>
                 <tr>
                     <td class="style10">
                         <label>
                         Phone Number(Office)</label></td>
                     <td>
                         <asp:TextBox ID="txtbox_owneroffice" runat="server" 
                             CssClass="twitterStyleTextbox" ValidationGroup="b"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                             ControlToValidate="txtbox_owneroffice" ErrorMessage="*" ForeColor="#CC0000" 
                             ValidationGroup="b"></asp:RequiredFieldValidator>
                             <asp:RegularExpressionValidator
                                                           ID="RegularExpressionValidator6" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_owneroffice" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator><cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" 
                 runat="server" Enabled="True" TargetControlID="txtbox_owneroffice" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>


                     </td>
                 </tr>
                 <tr>
                     <td class="style10">
                         <label>
                         Email</label>&nbsp;</td>
                     <td>
                         <asp:TextBox ID="txtbox_owneremail" runat="server" 
                             CssClass="twitterStyleTextbox" ValidationGroup="b"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                             ControlToValidate="txtbox_owneremail" ErrorMessage="*" ForeColor="#CC0000" 
                             ValidationGroup="b"></asp:RequiredFieldValidator>

                             <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                 ErrorMessage="Enter Correct Format" ControlToValidate="txtbox_owneremail" 
                 ForeColor="#FF3300" 
                 ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                     </td>
                 </tr>
                 </table>
                 </div>
           
                        
     <asp:Button ID="btn_save" runat="server" Text="Submit" ValidationGroup="b"  CssClass="button"
    onclick="btn_save_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="button" />
             </div>           
  
        
         
        <%--    <table class="form-table1">
            <tr>
            <td>
            <div id="map_canvas" style="width:300px; height:300px"></div>
            </td>
            </tr>
            </table>
            --%>
        </fieldset>

                    
                        </asp:Panel>
                    
      
 
    </asp:Panel>
     <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
    
    SelectCommand="select tbl_SiteMaster.*,tbl_TimeZoneMaster.*,tbl_City.* from tbl_SiteMaster
inner join tbl_TimeZoneMaster on tbl_SiteMaster.TimeZone=tbl_TimeZoneMaster.TimeZoneTblId
inner join tbl_city on tbl_SiteMaster.City=tbl_City.City_id">
</asp:SqlDataSource>
<asp:Panel ID="Panel2" runat="server">

    <asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource4" 
    onitemcommand="Repeater1_ItemCommand">
<HeaderTemplate>
<div style="max-height:420px; overflow-y:auto;">
              <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Customer &nbsp;&nbsp;Name
                    </th>
                     <th >
                        Contact Person 
                    </th>
                    <th>
                        Edit
                    </th>
                  <%--  <th>
                      Delete
                    </th>--%>
                       <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td  >
                    <%#Eval("SiteName")%>
                    </td><td>
                    <%#Eval("OwnerName")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("SiteId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
        <%--        <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("SiteId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>--%>
                     <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("SiteId") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("SiteId") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td>
                    
                     <asp:Label ID="siteid" runat="server" Text='<%# Eval("SiteId") %>' 
                                        Visible="False" />
  <asp:Label ID="sitename" runat="server" Text='<%# Eval("SiteName") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="address1" runat="server" Text='<%# Eval("Address1") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="address2" runat="server" Text='<%# Eval("Address2") %>' 
                                        Visible="False"></asp:Label>  
                                        
     <asp:Label ID="sitetype" runat="server" Text='<%# Eval("SiteTypeId") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="timezone" runat="server" Text='<%# Eval("TimeZoneTblId") %>' 
                                        Visible="False"></asp:Label>  
    <%--<asp:Label ID="sitecity" runat="server" Text='<%# Eval("City_id) %>' 
                                        Visible="False"></asp:Label>--%>

  <asp:Label ID="zip" runat="server" Text='<%# Eval("Zip") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="sitephone" runat="server" Text='<%# Eval("SitePhone") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="SiteCellular" runat="server" Text='<%# Eval("SiteCellular") %>' 
                                        Visible="False"></asp:Label>   


 <asp:Label ID="SiteOffice" runat="server" Text='<%# Eval("SiteOffice") %>' 
                                        Visible="False" />
  <asp:Label ID="SiteEmail" runat="server" Text='<%# Eval("SiteEmail") %>' 
                                        Visible="False"></asp:Label>

    <asp:Label ID="latitude" runat="server" Text='<%# Eval("SiteLatitude") %>' 
                                        Visible="False" />
  <asp:Label ID="longitude" runat="server" Text='<%# Eval("SiteLongitude") %>' 
                                        Visible="False"></asp:Label>

<asp:Label ID="OwnerName" runat="server" Text='<%# Eval("OwnerName") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="OwnerAddress" runat="server" Text='<%# Eval("OwnerAddress") %>' 
                                        Visible="False"></asp:Label>   


                                         
  
<asp:Label ID="OwnerCity" runat="server" Text='<%# Eval("OwnerCity") %>' 
                                        Visible="False"></asp:Label>  
 
<asp:Label ID="OwnerCell" runat="server" Text='<%# Eval("OwnerCell") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="OwnerOffice" runat="server" Text='<%# Eval("OwnerOffice") %>' 
                                        Visible="False"></asp:Label>   


    <asp:Label ID="OwnerEmail" runat="server" Text='<%# Eval("OwnerEmail") %>' 
                                        Visible="False"></asp:Label>


            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
            </div>
        </FooterTemplate>
</asp:Repeater>

</asp:Panel>

<div class="clearfix">&nbsp;</div>


      
  








        

    

   


   

    
    
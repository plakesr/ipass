﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_SiteManger : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            clsInsert onj = new clsInsert();
            DataSet rec = onj.fetchrec("SELECT [TimeZoneTblId], [TimeZoneName], [TimeZoneId] FROM [tbl_TimeZoneMaster] ORDER BY [TimeZoneName]");
            DataTable t = new DataTable("timezone");
            t.Columns.Add("TimeZoneTblId", typeof(int));
            t.Columns.Add("TimeZoneName", typeof(string));

            for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
            {
                DataRow dr = t.NewRow();
                dr[0] = rec.Tables[0].Rows[i][0].ToString();
                dr[1] = rec.Tables[0].Rows[i][2].ToString() + " [" + rec.Tables[0].Rows[i][1].ToString() + "]";
                t.Rows.Add(dr);
            }


            ddl_TimeZone.DataSource = t;
            ddl_TimeZone.DataTextField = "TimeZoneName";
            ddl_TimeZone.DataValueField = "TimeZoneTblId";
            ddl_TimeZone.DataBind();
            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }

            
                try
                {
                    //    Session["lbladd"] = lbladd.Text;
                    //Session["lbledit"] = lbledit.Text;
                    //Session["lbldelete"] = lbldelete.Text;
                    //Session["lblread"] = lblread.Text;

                    //Session["lblfull"] = lblfull.Text;
                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel4.Visible = true;
                    }
                    else
                    {
                        Panel4.Visible = false;
                    }

                    //if (Session["lbldelete"].ToString() == "True")
                    //{
                    //    // Panel1.Visible = false;
                    //}

                    //if (Session["lblfull"].ToString() == "True")
                    //{
                    //    // Panel1.Visible = false;


                    //}


                    //if (Session["lblread"].ToString() == "True")
                    //{
                    //    btn_color.Visible = false;
                    //}

                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            









        }
    }

    string status = "true";
    string msg = "";
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
           // 
            if (Session["dataaddress"].ToString() != null)
            {

                addSite();
                Session["dataaddress"] = null;
                clearresult();
            }
            //else
            //{
                
            //}

        }
        catch
        {
            
              ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Add Geo-Location.')", true);

               
           
        }
    }

    int sitetype = 0;
    string address = "";
    string lat = "";
    string longi = "";
    public void addSite()
    {
        try
        {
            hst.Clear();

        //@sitename,@sitetypeid,@timezone,@address1,@address2,@city,@zip,@sitephone,@siteoffice,@Sitecell,@SiteEmail,@SiteLatitude,@SiteLongitude
            //,@Ownername,@owneraddress,@ownercity,@ownercell,@owneroffice,@owneremail,@modifiedby,@operatorid,@timeofsub,@status
            hst.Add("action", "insert");
            hst.Add("sitetypeid",sitetype );
            hst.Add("timezone", ddl_TimeZone.SelectedValue);
            hst.Add("sitename", txtbox_SiteName.Text);

            DataTable dsadd = (DataTable)Session["dataaddress"];
            if (dsadd.Rows.Count > 0)
            {

                address = dsadd.Rows[0]["address"].ToString();
                lat = dsadd.Rows[0]["latitude"].ToString();
                longi = dsadd.Rows[0]["longitude"].ToString();
               
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please addlocation first.')", true);

            }


            hst.Add("address1", address);
            hst.Add("address2", address);
            hst.Add("city", ddl_city.SelectedValue);
           
            hst.Add("zip", txtbox_zip.Text);
            hst.Add("sitephone", txtbox_Phome.Text);
            hst.Add("Sitecell", txtbox_Cellular.Text);
            hst.Add("siteoffice", txtbox_poffice.Text);
            hst.Add("SiteEmail", txtbox_email.Text);
            hst.Add("SiteLatitude", lat);
            hst.Add("SiteLongitude", longi);
                   
       
            hst.Add("ownername", txtbox_ownerName.Text);
            hst.Add("owneraddress", address);
            
            hst.Add("ownercity", ddl_city.SelectedItem.Text);
                     
            hst.Add("ownercell", txtbox_ownercellular.Text);
            hst.Add("owneroffice", txtbox_owneroffice.Text);
            hst.Add("owneremail", txtbox_owneremail.Text);

            hst.Add("modifiedby", Session["Type"].ToString());
            hst.Add("operatorid", Session["Type"].ToString());
             hst.Add("timeofsub", DateTime.Now);
            hst.Add("status", status);
            int result = objData.ExecuteNonQuery("[sp_SiteMaster]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_SiteMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_SiteMaster.aspx?message=" + msg);

                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    public void clearresult()
    {
        txtbox_Cellular.Text = "";
        txtbox_SiteName.Text = "";
        txtbox_email.Text = "";
        txtbox_ownerName.Text = "";
        txtbox_Phome.Text = "";
        txtbox_zip.Text = "";
        txtbox_ownercellular.Text = "";
        txtbox_owneroffice.Text = "";
       


    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        //if (e.CommandName == "cmdDelete")
        //{
        //    int ID = Convert.ToInt32(e.CommandArgument.ToString());


        //    //try
        //    //{
        //    //    if (Session["Type"].ToString() == "admin")
        //    //    {
        //    //        delete_data(ID);
        //    //        Response.Redirect("Admin_SiteMaster.aspx");
        //    //    }
        //    //    if (Session["Type"].ToString() == "operator")
        //    //    {
        //    //        if (Session["lbldelete"].ToString() == "False")
        //    //        {
        //    //            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

        //    //        }

        //    //        else
        //    //        {
        //    //            delete_data(ID);
        //    //            Response.Redirect("Operator_SiteMaster.aspx");
        //    //        }
        //    //    }

        //    //}
        //    //catch
        //    //{
        //    //    // Response.Redirect("default.aspx");
        //    //}

            
        //}

        if (e.CommandName == "cmdEdit")
        {
          
            Label siteid = rpt1.Items[i].FindControl("siteid") as Label;
            Label sitename = rpt1.Items[i].FindControl("sitename") as Label;

            Label address1 = rpt1.Items[i].FindControl("address1") as Label;
            Label address2 = rpt1.Items[i].FindControl("address2") as Label;
            Label zip = rpt1.Items[i].FindControl("zip") as Label;

            Label sitetype = rpt1.Items[i].FindControl("sitetype") as Label;
            Label timezone = rpt1.Items[i].FindControl("timezone") as Label;

           // Label sitecity = rpt1.Items[i].FindControl("sitecity") as Label;
            Label sitephone = rpt1.Items[i].FindControl("sitephone") as Label;

            Label SiteCellular = rpt1.Items[i].FindControl("SiteCellular") as Label;
            Label SiteOffice = rpt1.Items[i].FindControl("SiteOffice") as Label;

            Label SiteEmail = rpt1.Items[i].FindControl("SiteEmail") as Label;
           
            Label latitude = rpt1.Items[i].FindControl("latitude") as Label;
            Label longitude = rpt1.Items[i].FindControl("longitude") as Label;
            Label OwnerName = rpt1.Items[i].FindControl("OwnerName") as Label;
            Label OwnerAddress = rpt1.Items[i].FindControl("OwnerAddress") as Label;
            Label OwnerCity = rpt1.Items[i].FindControl("OwnerCity") as Label;
            Label OwnerCell = rpt1.Items[i].FindControl("OwnerCell") as Label;
            Label OwnerOffice = rpt1.Items[i].FindControl("OwnerOffice") as Label;
            Label OwnerEmail = rpt1.Items[i].FindControl("OwnerEmail") as Label;
            Session["SiteId"] = siteid.Text;
            Session["SiteName"] = sitename.Text;
            Session["Address1"] = address1.Text;
            Session["Address2"] = address2.Text;
            Session["SiteType"] = sitetype.Text;
            Session["Zip"] = zip.Text;
            Session["Timezone"] = timezone.Text;

            Session["sitephone"] = sitephone.Text;
            Session["SiteCell"] = SiteCellular.Text;
            Session["siteoffice"] = SiteOffice.Text;
           //Session["sitecity"] = sitecity.Text;
            Session["siteemail"] = SiteEmail.Text;
            Session["latitude"] = latitude.Text;
            Session["longitude"] = longitude.Text;

            Session["OwnerName"] = OwnerName.Text;
            Session["OwnerAddress"] = OwnerAddress.Text;
           
            Session["OwnerCity"] = OwnerCity.Text;
            
            Session["OwnerCell"] = OwnerCell.Text;
            Session["OwnerOffice"] = OwnerOffice.Text;
            Session["OwnerEmail"] = OwnerEmail.Text;


            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditSiteMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditSiteMaster.aspx");
                }
               
            }

            

        }
      
        if (e.CommandName == "cmdinactive")
        {
           // string user = e.CommandArgument.ToString();
            int siteid = Convert.ToInt32( e.CommandArgument);
            DataSet ds = new clsInsert().fetchrec(" select Site_Status from tbl_SiteMaster where SiteId=" + siteid);
            if (ds.Tables[0].Rows[0][0].ToString() == "False")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Already Deactivated ')", true);

            }
            else
            {
                inactivestatus(siteid);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Deactivated Successfuly')", true);
            }
        }

        if (e.CommandName == "cmdactive")
        {
            int siteid = Convert.ToInt32(e.CommandArgument);
            DataSet ds = new clsInsert().fetchrec(" select Site_Status from tbl_SiteMaster where SiteId=" + siteid);
            if (ds.Tables[0].Rows[0][0].ToString() == "True")
            {

                
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Already Activated ')", true);

            }
            else
            {
                activeuserstatus(siteid);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Activated Successfuly')", true);
            }

        }
    }

    public void delete_data(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("siteid", id);


            int result = objData.ExecuteNonQuery("[sp_SiteMaster]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
                //Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_SiteMaster.aspx");

        }


    }


    public void inactivestatus(int siteid )
    {
        try
        {
            hst.Clear();
            hst.Add("action", "sitestausinactive");
            //hst.Add("ustatus", userstattus1);
            hst.Add("siteid", siteid);



            int result = objData.ExecuteNonQuery("[sp_statusupdate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              //  Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
               // Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
           // Response.Redirect("Admin_SiteMaster.aspx");

        }


    }


    public void activeuserstatus(int siteid )
    {
        try
        {
            hst.Clear();
            hst.Add("action", "sitestausactive");
            //hst.Add("ustatus", userstattus1);
            hst.Add("siteid", siteid);



            int result = objData.ExecuteNonQuery("[sp_statusupdate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              //  Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
               // Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
           // Response.Redirect("Admin_SiteMaster.aspx");

        }


    }

    
    
    protected void txtbox_ownerName_TextChanged(object sender, EventArgs e)
    {

    }
    protected void latitude_TextChanged(object sender, EventArgs e)
    {
      //  Session["lat"] = latitude.Text;
    }
    protected void longitude_TextChanged(object sender, EventArgs e)
    {
       // Session["long"] = longitude.Text;
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('GetAddressLatLong.aspx','New Windows','height=580, width=500,location=center');", true);

    }
    protected void txtbox_SiteName_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataSet dssitename = new clsInsert().fetchrec("select SiteName from tbl_sitemaster where SiteName='" + txtbox_SiteName.Text+"'");
            if (dssitename.Tables[0].Rows.Count > 0)
            {
                lblerror.Visible = true;
                lblerror.Text = "Already Exist";
                txtbox_SiteName.Text = "";

            }
            else
            {
                lblerror.Visible = false;
                lblerror.Text = "";
                txtbox_SiteName.Text = txtbox_SiteName.Text;
            }
        }
        catch
        {
        }
    }
    
}

  

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_SiteTypeManager.ascx.cs" Inherits="WUC_wuc_SiteType_Manager" %>


    <table class="style1 form-table">
        <tr>
            <td class="style2">
              Type Name:</td>
            <td>
                <asp:TextBox ID="txtbox_typename" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_typename" runat="server" 
                    ControlToValidate="txtbox_typename" 
                    ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Status:</td>
            <td>
                <asp:CheckBox ID="chkbox_status" runat="server" />
               <%-- <asp:RequiredFieldValidator ID="rfv_TimezoneName" runat="server" 
                    ControlToValidate="txtbox_TimezoneName" 
                    ErrorMessage="*Please Enter the Timezone Name" ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_submit" runat="server" Text="Add" 
                    onclick="btn_submit_Click" ValidationGroup="a" class="button" />
            </td>
        </tr>
    </table>



<asp:Panel ID="Panel2" runat="server"  Height="300px" ScrollBars="Vertical">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
               <ContentTemplate>    
<asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource1" 
        onitemcommand="Repeater1_ItemCommand">

 <HeaderTemplate>
            <table width="100%" class="timezone-table">
                <tr>
                    
                      <th>
                        Customer&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Operator_Id
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("SiteTypeName")%>
                    </td><td>
                    <%#Eval("OperatorId")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("SiteTypeId") %>' runat="server"><img src="images/edit.png"/>
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("SiteTypeId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png"/>
                    </asp:LinkButton>
                    </td>
            </tr>
            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("SiteTypeId") %>' 
                                        Visible="False" />
  <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("SiteTypeName") %>' 
                                        Visible="False"></asp:Label>

    
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        
        SelectCommand="SELECT [SiteTypeName], [OperatorId], [SiteTypeId] FROM [tbl_SiteTypeMaster]">
    </asp:SqlDataSource>
</div>
</ContentTemplate> 
</asp:UpdatePanel>
</asp:Panel>
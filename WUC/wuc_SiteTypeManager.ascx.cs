﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_SiteType_Manager : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
        }
             
    }
    int status = 0;
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = "1";

            //if (chkbox_status.Checked == true)
            //{
            //    status = 1;
            //}

            //else
            //{
            //    status= 0;
            //}
            addSiteType();
            Response.Redirect("Admin_SiteType.aspx?message=" + msg);

           // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void addSiteType()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("sitetypename", txtbox_typename.Text);
            hst.Add("operatorid", "lovey_operator");
            hst.Add("status", status);

            hst.Add("modifiedby", "modifier_id");
            hst.Add("DateOfChanging", DateTime.Now);


            int result = objData.ExecuteNonQuery("[sp_SiteType]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("Admin_SiteType.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }


    public void clear()
    {
        txtbox_typename.Text = "";
        chkbox_status.Checked = false;

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            delete_data(ID);
        }
        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;



            Session["SiteTypeId"] = l.Text;
            Session["SiteTypeName"] = l1.Text;

            Response.Redirect("Admin_EditSiteTypeMaster.aspx");

        }
    }


    public void delete_data(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);



            int result = objData.ExecuteNonQuery("[sp_SiteType]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("Admin_SiteType.aspx");

            }
            else
            {
                Response.Redirect("Admin_SiteType.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_SiteType.aspx");

        }


    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_StatusOfParker.ascx.cs" Inherits="WUC_wuc_StatusOfParker" %>
<link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<br />
<asp:Panel ID="Panel3" runat="server">
<fieldset><legend>Track Refund Status</legend>
    <table class="form-table">
        <tr>
            <td class="style3" width="200">
               Enter Last Name</td>
            <td class="style18">
                <asp:TextBox ID="txtbox_searchname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_name" runat="server" 
                    ControlToValidate="txtbox_searchname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Tracking Id</td>
            <td class="style2">
                <asp:TextBox ID="txtbox_searchtrackingid" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_trackingid" runat="server" 
                    ControlToValidate="txtbox_searchtrackingid" ErrorMessage="*" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style18">
                <asp:Button ID="btn_search" runat="server" Text="Search"  CssClass="button" 
                    ValidationGroup="a" onclick="btn_search_Click"/>
            </td>
        </tr>
    </table>
    </fieldset>
</asp:Panel>
  <br />
    <asp:Panel ID="Panel2" runat="server" Visible="false">
    
     <fieldset>
    <legend>Refund Status</legend>
    
    
         <table class="form-table">
             <tr>
                 <td width="200">
                     Status</td>
                 <td>
                     <asp:Image ID="imgstat" runat="server" />
                 </td>
             </tr>
             <tr>
                 <td>
                     <asp:Label ID="lbltext" runat="server" Text="" Visible="false"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="txtbox" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td>
                     <asp:Label ID="lbldate" runat="server" Text="" Visible="false"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="txtdate" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td>
                     &nbsp;</td>
                 <td>
                     <asp:Button ID="Button1" runat="server" Text="Ok" CssClass="button" 
                         onclick="Button1_Click" />
                 </td>
             </tr>
         </table>
    
    
    </fieldset>
    
    
    
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel1" runat="server" Visible="false">
    <fieldset>
    <legend>Parker&#39;s Details</legend>
        <table class="form-table">
            <tr>
                <td class="style5" width="200">
                    Campus&nbsp; Name</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_campusname" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td width="50"></td>
                <td width="150">Lot Name</td>
                <td> <asp:TextBox ID="txtbox_lotname" runat="server" Enabled="False"></asp:TextBox></td>
            </tr>
          
             <tr>
                <td class="style14">
                    Name</td>
                <td class="style15">
                    <asp:TextBox ID="txtbox_name" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td width="50"></td>
                <td class="style5">
                    Address</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_address" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
          
            <tr>
                <td class="style5">
                    E-mail Id</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_email" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td width="50"></td>
                <td class="style5">
                    Tracking Id</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_trackingid" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Licence Plate No.</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_licence" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td width="50"></td>
                 <td class="style5">
                    APT/UNIT</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_aptunit" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
          
            <tr>
                <td >
                    Method Of Payment</td>
                <td>
                    <asp:RadioButton ID="rbtn_cash" runat="server" Enabled="False" Text="CASH" 
                        GroupName="a" />
                    <asp:RadioButton ID="rbtn_debit" runat="server" Enabled="False" Text="DEBIT" 
                        GroupName="a" />
                    <asp:RadioButton ID="rbtn_credit" runat="server" Enabled="False" 
                        Text="CREDIT" GroupName="a" />
                </td>
                <td  width="50">
                    
                </td>
                <td >
                   <asp:Label ID="lblcode" runat="server" Text="" Visible="false"></asp:Label> 
                </td>
                <td><asp:TextBox ID="txtcode" runat="server" Visible="false" Enabled="false"></asp:TextBox></td>
            </tr>
       <%--     <tr>
                <td class="style5">
                    Device/Machine#</td>
                <td class="style11" colspan="3">
                    <asp:TextBox ID="txtbox_machine" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>--%>
         <%--   <tr>
                <td class="style8">
                    TKT/Ticket No.</td>
                <td class="style9" colspan="3">
                    <asp:TextBox ID="txtbox_ticketno" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td class="style5">
                    Date of Submission</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_dateofrequest" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td width="50"></td>
                 <td class="style5">
                    Date of Transaction</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_dateoftransaction" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
           
            <tr>
                <td class="style5">
                    Reason for the Refund Request</td>
                <td class="style11">
                    <asp:TextBox ID="txtbox_reason" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
            </tr>
        </table>

    </fieldset>
    </asp:Panel>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class WUC_wuc_StatusOfParker : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        try
        {
            bool value;
            int result;
            value = int.TryParse(txtbox_searchtrackingid.Text, out result);
            if (value == true)
            {
                DataTable dt = new clsInsert().getrefundparkerstatus(txtbox_searchname.Text, Convert.ToInt32(txtbox_searchtrackingid.Text));
                if (dt.Rows.Count > 0)
                {
                    Panel1.Visible = true;
                    Panel2.Visible=true;
                    txtbox_address.Text = dt.Rows[0]["Address"].ToString() + "," + dt.Rows[0]["Postal_code"].ToString() + "," + dt.Rows[0]["City"].ToString();
                    txtbox_aptunit.Text = dt.Rows[0]["Apt"].ToString();
                    txtbox_dateofrequest.Text = dt.Rows[0]["DateofRequest"].ToString();
                    txtbox_dateoftransaction.Text = dt.Rows[0]["DateofReceipt"].ToString();
                    txtbox_email.Text = dt.Rows[0]["e_mail"].ToString();
                    txtbox_licence.Text = dt.Rows[0]["LicensePlate"].ToString();
                   // txtbox_machine.Text = dt.Rows[0]["DeviceMachineID"].ToString();
                    txtbox_name.Text = dt.Rows[0]["F_name"].ToString() + "  " + dt.Rows[0]["L_name"].ToString();
                    txtbox_reason.Text = dt.Rows[0]["ReasonOfRefund"].ToString();
                   // txtbox_ticketno.Text = dt.Rows[0]["TicketNo"].ToString();
                    txtbox_trackingid.Text = dt.Rows[0]["REFUND_TRACKING_ID"].ToString();
                    txtbox_lotname.Text = dt.Rows[0]["LotName"].ToString();
                    txtbox_campusname.Text = dt.Rows[0]["Campus_Name"].ToString();
                    if (dt.Rows[0]["BillingType"].ToString() == "3")
                    {
                        rbtn_credit.Checked = true;
                        lblcode.Visible = true;
                        lblcode.Text = "Authorisation Code";
                        txtcode.Visible = true;
                        txtcode.Text = dt.Rows[0]["AuthorisationCode_CreditCard"].ToString();
                    }
                    if (dt.Rows[0]["BillingType"].ToString() == "2")
                    {
                        rbtn_debit.Checked = true;
                        lblcode.Visible = false;
                        txtcode.Visible = false;
                    }
                    if (dt.Rows[0]["BillingType"].ToString() == "1")
                    {
                        rbtn_cash.Checked = true;
                        lblcode.Visible = false;
                        txtcode.Visible = false;
                    }

                    if (dt.Rows[0]["requeststatus"].ToString() == "1")
                    {
                        imgstat.ImageUrl = "~/images/pendindrequest.png";
                        imgstat.Height=53;
                        imgstat.Width = 340;
                       lbltext.Visible = false;
                       txtbox.Visible = false;
                       txtdate.Visible = false;
                       lbldate.Visible = false;

                       // lbltext.Text = dt.Rows[0]["AuthorisationCode_CreditCard"].ToString();
                    }

                    if (dt.Rows[0]["requeststatus"].ToString() == "2")
                    {
                        imgstat.ImageUrl = "~/images/processingrequest.png";
                        imgstat.Height = 53;
                        imgstat.Width = 340;
                        lbltext.Visible = false;
                        txtbox.Visible = false;
                        txtdate.Visible = false;
                        lbldate.Visible = false;
                    }
                    if (dt.Rows[0]["requeststatus"].ToString() == "3")
                    {
                        imgstat.ImageUrl = "~/images/approvedrequest.png";
                        imgstat.Height = 53;
                        imgstat.Width = 340;

                        
                        
                        try
                        {
                            DataTable dt11 = new clsInsert().getdateandamount(Convert.ToInt32(txtbox_searchtrackingid.Text));
                            if (dt11.Rows.Count > 0)
                            {
                                if (dt11.Rows[0]["AmountRefund"].ToString() != "0.00")
                                {
                                    lbltext.Visible = true;

                                    lbltext.Text = "Approved Refund Amount";
                                     lbldate.Visible = true;
                                     txtbox.Visible = true;
                                     txtdate.Visible = true;
                                     lbldate.Text = "Date ";
                                    txtbox.Text = "$" + "" + dt11.Rows[0]["AmountRefund"].ToString();
                                    txtdate.Text = dt11.Rows[0]["DateofRefund"].ToString();
                                }
                                else
                                {
                                    lbltext.Visible = true;
                                    lbltext.Text = "Your Request of Refund Has Approved but your refunded amount will be confirmed soon";
                                    
                                }
                            }
                            
                        }
                        catch
                        {
                        }
                      
                    }
                    if (dt.Rows[0]["requeststatus"].ToString() == "4")
                    {
                        imgstat.ImageUrl = "~/images/rejectedrequest.png";
                        imgstat.Height = 53;
                        imgstat.Width = 340;
                        lbltext.Visible = true;
                        txtbox.Visible = true;
                        lbltext.Text = "Reason of Rejection";

                        if (dt.Rows[0]["PM_DeclineReason"].ToString() != "")
                        {

                            txtbox.Text = dt.Rows[0]["PM_DeclineReason"].ToString();
                        }
                        if (dt.Rows[0]["ClientManager_DeclineReason"].ToString() != "")
                        {

                            txtbox.Text = dt.Rows[0]["ClientManager_DeclineReason"].ToString();
                        }
                        if (dt.Rows[0]["Accounting_DeclineReason"].ToString() != "")
                        {

                            txtbox.Text = dt.Rows[0]["Accounting_DeclineReason"].ToString();
                        } 
                        if (dt.Rows[0]["Datacenter_DeclineReason"].ToString() != "")
                        {

                            txtbox.Text = dt.Rows[0]["Datacenter_DeclineReason"].ToString();
                        }

                        
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found.')", true);
                    Panel1.Visible = false;
                    Panel2.Visible=false;
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Correct Tracking ID.')", true);

            }
            

           
        }
        catch
        {
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
        Session.Abandon();
    }
}
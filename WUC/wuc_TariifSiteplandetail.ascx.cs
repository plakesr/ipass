﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_TariifSiteplandetail : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            DataTable dt = new clsInsert().getsiteTariffPlandetailsforparker(Convert.ToInt32(Session["Site_id"].ToString()));

            if (dt.Rows.Count > 0)
            {


                rptsite.DataSource = dt;
                rptsite.DataBind();

            }
        }
    }
}
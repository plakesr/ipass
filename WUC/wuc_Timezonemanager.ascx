﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_Timezonemanager.ascx.cs" Inherits="WUC_wuc_Timezonemanager" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style2
    {
        height: 23px;
    }
     .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

     .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

<div style="overflow-x:auto">
    <asp:Panel ID="Panel1" runat="server">
    <div class="form">
    <label>TimeZone ID:</label>
     <asp:TextBox ID="txtbox_TimezoneId" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_TimezoneId_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_TimezoneId" WatermarkText="Enter TimeZone ID" WatermarkCssClass="watermark">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_TimezoneId" runat="server" 
                    ControlToValidate="txtbox_TimezoneId" 
                    ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
<label>TimeZone Name:</label>
<asp:TextBox ID="txtbox_TimezoneName" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
               <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender0" 
                runat="server" Enabled="True" TargetControlID="txtbox_TimezoneName" WatermarkText="Enter TimeZone Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_TimezoneName" runat="server" 
                    ControlToValidate="txtbox_TimezoneName" 
                    ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
<label>&nbsp;</label> <asp:Button ID="btn_submit" runat="server" Text="Add"  CssClass="button"
                    onclick="btn_submit_Click" ValidationGroup="a" />
 <br /><br />
    </div>
   

    
        </asp:Panel>
    

<asp:Panel ID="Panel2" runat="server" Width="100%">
    

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                
                SelectCommand="SELECT * FROM [tbl_TimeZoneMaster]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table class="timezone1-table">
                <tr>
                      <th width="10%" style="width:30px;">
                       TimeZone&nbsp;&nbsp;Name
                    </th>
                     <th>
                        TimeZone_ID
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td style=" width:500px;">
                    <%#Eval("TimeZoneName")%>
                    </td><td  style=" width:100px;">
                    <%#Eval("TimeZoneId")%>
                </td>
                
                <td  style=" width:50px;">
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("TimeZoneTblId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td  style=" width:50px;">
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("TimeZoneTblId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                                        <asp:Label ID="idLabel" runat="server" Text='<%# Eval("TimeZoneTblId") %>' 
                                        Visible="False" />
                                        <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("TimeZoneName") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="idLabel2" runat="server" Text='<%# Eval("TimeZoneId") %>' 
                                        Visible="False"></asp:Label>  
                    </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
<br /><br />
</div>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_Timezonemanager : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }


            if (!IsPostBack == true)
            {
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel1.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
    string msg ;
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
           
            addTime();
           // Response.Redirect("Admin_TimeZoneMaster.aspx?message=" + msg);

           // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void addTime()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("TimeZoneId", txtbox_TimezoneId.Text);
            hst.Add("TimeZoneName", txtbox_TimezoneName.Text);
            hst.Add("ModifiedBy", "aaaa");
            hst.Add("OperatorId", "aaaa");
            hst.Add("DateOfChanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_TimeZone]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_TimeZoneMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_timeZoneMaster.aspx?message=" + msg);

                }
                
              //  msg = "1";
              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    public void clear()
    {
        txtbox_TimezoneId.Text = "";
        txtbox_TimezoneName.Text = "";
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delTime(ID);
                    Response.Redirect("Admin_TimeZoneMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delTime(ID);
                        Response.Redirect("Operator_timeZoneMaster.aspx");
                    }
                }

            }
            catch
            {

                // Response.Redirect("default.aspx");
            }
          

            //Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            //delTime(int.Parse(l.Text));
        }


        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

            Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
            try
            {

                Session["TimeZoneTblId"] = l.Text;
                Session["TimeZoneName"] = l1.Text;
                Session["TimeZoneId"] = l2.Text;

            }
            catch
            {
                Response.Redirect("Default.aspx");

            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditTimeZoneMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditTimeZoneMaster.aspx");
                }
            }
            

        }
    }

    public void delTime(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "del");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_TimeZone]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //Response.Redirect("Admin_TimeZoneMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

              //  Response.Redirect("Admin_TimeZoneMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

           // Response.Redirect("Admin_TimeZoneMaster.aspx");

        }


    }
}
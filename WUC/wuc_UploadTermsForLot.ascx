﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_UploadTermsForLot.ascx.cs" Inherits="wuc_UploadTermsForLot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style3
    {
        width: 122px;
    }
      .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
      .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    .style4
    {
        width: 324px;
    }
</style>

    <br />
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server">

    <table  class="form-table">
      

        <tr>
            <td class="style3">
                Customer Name&nbsp;&nbsp;&nbsp; :</td>
            <td class="style4">
                <asp:DropDownList ID="ddl_SiteName" runat="server" ValidationGroup="b" 
                    DataSourceID="SqlDataSource1" DataTextField="SiteName" 
                    DataValueField="SiteId" CssClass="twitterStyleTextbox" AutoPostBack="True" >
                </asp:DropDownList>
               
                 
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster] order by SiteName">
                </asp:SqlDataSource>
            
                <asp:RequiredFieldValidator ID="rfv_sitename" runat="server" 
                    ControlToValidate="ddl_SiteName" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="b"></asp:RequiredFieldValidator>
            </td>
        </tr>


        <tr>
            <td class="style3">
                Campus Name&nbsp;&nbsp;&nbsp; :</td>
            <td class="style4">
                <asp:DropDownList ID="ddlcampus" runat="server" ValidationGroup="b" 
                    DataSourceID="SqlDataSource2" DataTextField="Campus_Name" 
                    DataValueField="CampusID" CssClass="twitterStyleTextbox" 
                    AutoPostBack="True" >
                     
                </asp:DropDownList>
                 
                 <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [CampusID], [Campus_Name] FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID) order by Campus_Name">
                     <SelectParameters>
                         <asp:ControlParameter ControlID="ddl_SiteName" Name="SiteID" 
                             PropertyName="SelectedValue" Type="Int64" />
                     </SelectParameters>
                </asp:SqlDataSource>
                 
              
               
            </td>
        </tr>



        <tr>
            <td class="style3">
                Lot Name&nbsp;&nbsp;&nbsp; :</td>
            <td class="style4">
                <asp:DropDownList ID="ddlLot" runat="server" ValidationGroup="b" 
                    DataSourceID="SqlDataSource3" DataTextField="LotName" 
                    DataValueField="Lot_id" CssClass="twitterStyleTextbox" >
                     
                </asp:DropDownList>
                 
                 <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT [Lot_id], [LotName] FROM [tbl_LotMaster] WHERE ([Campus_id] = @Campus_id)  order by LotName">
                     <SelectParameters>
                         <asp:ControlParameter ControlID="ddlcampus" Name="Campus_id" 
                             PropertyName="SelectedValue" Type="Int64" />
                     </SelectParameters>
                </asp:SqlDataSource>
                 
               
               
            </td>
        </tr>




        <tr>
            <td class="style3">
                File Path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td class="style4">
                <asp:FileUpload ID="fup_pdf" runat="server" />
                <asp:RequiredFieldValidator ID="rfv_filepath" runat="server" 
                    ControlToValidate="fup_pdf" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="b"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                <asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="b" CssClass=button 
                    onclick="btn_add_Click" Height="37px" Width="93px" />
            </td>
        </tr>
        <tr>
            <td class="style3" align=center>
         
 Note&nbsp;&nbsp;&nbsp; :</td>
            <td class="style4">
                Please Upload PDF File Only</td>
        </tr>
    </table>
    </asp:Panel>
<asp:Panel ID="Panel2" runat="server"  Height="300px" ScrollBars="Vertical">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="sqldatasourcedata" runat="server" 
    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
    SelectCommand="select tbl_LotMaster.LotName,tbl_uploadlotTerms.* from tbl_uploadlotTerms
inner join tbl_LotMaster on tbl_uploadlotTerms.LotId=tbl_LotMaster.Lot_id"></asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="sqldatasourcedata" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Document&nbsp;&nbsp;Name
                    </th>
                    <th>
                        Lot&nbsp;&nbsp;Name
                    </th>
                    <%--<th>
                        Edit
                    </th>--%>
                    <%--<th>
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("LotTermPdfName")%>
                    </td>
<td>
                    <%#Eval("LotName")%>
                    </td>

                </td>
                
                <%--<td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotTermID") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>--%>
              <%--  <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("LotTermID") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>--%>

                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("LotTermID") %>' 
                                        Visible="False" />
  <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("LotTermPdfName") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="idLabel2" runat="server" Text='<%# Eval("LotName") %>' 
                                        Visible="False"></asp:Label> 
<asp:Label ID="idlabel3" runat="server" Text='<%# Eval("LotId") %>' 
                                        Visible="False"></asp:Label>  
                                         
     
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>

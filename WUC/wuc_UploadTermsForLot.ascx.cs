﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Security.Cryptography;

public partial class wuc_UploadTermsForLot : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }


            try
            {

                if (Session["lbladd"].ToString() == "True")
                {
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                }



            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        // Response.Redirect("Deshboard.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_deshboard.aspx");

                    }
                }
                catch
                {
                    //  Response.Redirect("default.aspx");
                }
            }
        }
    }
    string pdfname = "";


    public string Get4Digits()
    {
        var bytes = new byte[4];
        var rng = RandomNumberGenerator.Create();
        rng.GetBytes(bytes);
        uint random = BitConverter.ToUInt32(bytes, 0) % 10000;
        return String.Format("{0:D4}", random);
    }

    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {



            if (fup_pdf.HasFile)
            {
                string oFName = fup_pdf.FileName.ToString().Trim();
                string[] strArr = oFName.Split('.');
                int sLength = strArr.Length;
                if (strArr[sLength - 1] == "pdf")
                {
                    pdfname = ddlLot.SelectedItem.Text+Get4Digits() +"." + strArr[1];
                    string Path = Server.MapPath("~/TermsForLots/");
                    fup_pdf.SaveAs(Path + pdfname);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload pdf only.')", true);
                    return;
                }
            }

            
            addTermsDocumenttosite();
            

           


        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }

    string msg = "";
    public void addTermsDocumenttosite()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");

            hst.Add("Lotid", Convert.ToInt32(ddlLot.SelectedValue));
            hst.Add("Lottermpdfname", pdfname);

            hst.Add("operator", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_TermsToLot]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_UploadLotTerms.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_UploadlotAgreementDocument.aspx?message=" + msg);

                }
              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);


            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);


        }

    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    //delColor(ID);
                    Response.Redirect("Admin_UploadLotTerms.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                       // delColor(ID);
                        Response.Redirect("Operator_UploadlotAgreementDocument.aspx");
                    }
                }

            }
            catch
            {
                // Response.Redirect("default.aspx");
            }
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

            try
            {
                //Session["VehicleID"] = l.Text;
                //Session["VehicleName"] = l1.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");
            }
            if (Session["Type"].ToString() == "admin")
            {
               // Response.Redirect("Admin_EditUploadTermsAndContionFoLot.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                  //  Response.Redirect("Operator_EditUploadTermsAndContionForLot.aspx");
                }

            }


        }
    }
}
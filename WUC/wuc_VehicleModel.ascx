﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_VehicleModel.ascx.cs" Inherits="WUC_wuc_VehicleModel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 175px;
    }
    .style3
    {
        width: 175px;
        height: 23px;
    }
    .style4
    {
        height: 23px;
    }
     .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server">

    <table class="form-table" width="60%">
        <tr>
            <td class="style2">
                Vehicle Make&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_make" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="VehiMake" CssClass="twitterStyleTextbox "
                    DataValueField="VehiMakeID" Width="167px" AppendDataBoundItems="true">
                    <asp:ListItem Selected = "True" Text = "---select---" Value = "0"></asp:ListItem>
                </asp:DropDownList>
                <cc1:DropDownExtender ID="ddl_country_DropDownExtender" runat="server"  DynamicServicePath="" Enabled="True" TargetControlID="ddl_make">
        </cc1:DropDownExtender>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_VehicleMakes]"></asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="rfv_make" runat="server" 
                    ControlToValidate="ddl_make" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Vehicle_Model&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_model" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_model_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_model" WatermarkCssClass="watermark" WatermarkText="Enter Vehicle Model">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_make0" runat="server" 
                    ControlToValidate="txtbox_model" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
    
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="a" CssClass=button
                    onclick="btn_add_Click" Height="34px" Width="55px" />
            </td>
        </tr>
    </table>
    </asp:Panel>
    <br />
  



<asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="select tbl_VehicleModel.*,tbl_VehicleMakes.* from tbl_VehicleModel
inner join tbl_VehicleMakes on tbl_VehicleModel.VehiMakeID=tbl_VehicleMakes.VehiMakeID">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Vehicle&nbsp;&nbsp;Make
                    </th>
                     <th>
                        Vehicle&nbsp;&nbsp;Model
                    </th>
                    
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("VehiMake")%>
                    </td>
                </td>
                <td>
                    <%#Eval("VehiModel")%>
                    </td>
                </td>
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("VehiModelId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("VehiModelId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>

                   <%--<asp:Label ID="idLabel" runat="server" Text='<%# Eval("StateCode") %>' 
                                        Visible="False" />--%>
  <%--<asp:Label ID="idLabel1" runat="server" Text='<%# Eval("StateName") %>' 
                                        Visible="False"></asp:Label>--%>
<asp:Label ID="modelname" runat="server" Text='<%# Eval("VehiModel") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="id" runat="server" Text='<%# Eval("VehiModelId") %>' 
                                        Visible="False"></asp:Label>  
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_VehicleModel : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel1.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
            //string msg = "1";

            addVehicleModel();
           // Response.Redirect("Admin_VehicleModel.aspx?message=" + msg);
           // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    string msg;
    public void addVehicleModel()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("vehiclemake", ddl_make.SelectedValue);

            hst.Add("vehiclemodel", txtbox_model.Text);
            hst.Add("operator_id", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_Vehicle_Model]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleModel.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_VehicleModel.aspx?message=" + msg);

                }
                
                //Response.Redirect("Admin_VehicleModel.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    public void clear()
    {
        txtbox_model.Text = "";

    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delVehicleModel(ID);
                    Response.Redirect("Admin_VehicleModel.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delVehicleModel(ID);
                        Response.Redirect("Operator_VehicleModel.aspx");
                    }
                }

            }
            catch
            {
                // Response.Redirect("default.aspx");
            }
           
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("modelname") as Label;
            //Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

            //Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
            Label l3 = rpt1.Items[i].FindControl("id") as Label;

            try
            {

                Session["Vehicle Model"] = l.Text;
                Session["id"] = l3.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");

            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditVehicleModel.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditVehicleModel.aspx");
                }

            }
          
           

        }
    }

    public void delVehicleModel(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_Vehicle_Model]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              //  Response.Redirect("Admin_VehicleModel.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

               // Response.Redirect("Admin_VehicleModel.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

          //  Response.Redirect("Admin_VehicleModel.aspx");

        }


    }
}
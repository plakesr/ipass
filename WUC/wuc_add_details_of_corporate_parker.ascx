﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_add_details_of_corporate_parker.ascx.cs" Inherits="WUC_wuc_add_details_of_corporate_parker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style3
    {
        width: 167px;
    }
    .style5
    {
        width: 163px;
    }
    .style7
    {
        width: 591px;
        text-align: right;
    }
    .twitterStyleTextbox
    {
    }
    .registration-area
    {
        width: 1231px;
    }
    .style8
    {
        width: 78px;
    }
    .style10
    {
        width: 115px;
    }
    .style11
    {
        width: 96px;
    }
    .style12
    {
        width: 104px;
    }
    .style13
    {
        width: 235px;
    }
    .style14
    {
        width: 117px;
    }
    </style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
 <div class="body-container">
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" 
        AutoPostBack="True"  >
    <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"  CssClass="Clicked"><HeaderTemplate>
Add Employee Detail
</HeaderTemplate>
    

<ContentTemplate>
   
<table class="registration-area">
    <tr><td class="style12" >Employee Code </td>
        <td class="style3"><asp:TextBox ID="txtboxcode_employeecode" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Employee Code" 
        ontextchanged="txtboxcode_employeecode_TextChanged" AutoPostBack="True"></asp:TextBox>

</td><td class="style11"></td><td class="style5"><asp:Label ID="lblerror" runat="server" ForeColor="#FF3300" Visible="False"></asp:Label>

</td><td class="style14">
    <asp:Label ID="parker" runat="server" Text="Number Of Parker"></asp:Label>


    </td><td class="style13"><asp:Label ID="lbl_number" runat="server"></asp:Label>

</td>
        <td class="style8">
            &nbsp;</td>
    </tr><tr><td class="style12">First Name </td><td class="style3"><asp:TextBox ID="txtbox_emp_firstname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  First Name"></asp:TextBox>

</td><td class="style11">Middle Name</td>
        <td class="style5"><asp:TextBox ID="txtbox_emp_middlename" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the Middle Name" 
                    ValidationGroup="a"></asp:TextBox>

</td><td class="style14" >Last Name</td>
        <td class="style13"><asp:TextBox ID="txtbox_emp_lastname" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Last Name" 
                ValidationGroup="a"></asp:TextBox>

</td>
        <td class="style8">
            &nbsp;</td>
    </tr><tr><td class="style12" >St#</td><td class="style3">
            <asp:TextBox ID="txtBox_emp_address1" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the St#" 
                    ValidationGroup="a"></asp:TextBox>

</td><td class="style11" >Street Name</td>
        <td class="style5"><asp:TextBox ID="txtBox_emp_address2" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the  Street Name" 
                    ValidationGroup="a"></asp:TextBox>

</td><td class="style14" >Email</td>
        <td class="style13"><asp:TextBox ID="txtBox_emp_emailid" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Email Id" 
                ValidationGroup="a"></asp:TextBox>
                

</td>
        <td class="style8">
            </td>
    </tr>
    <tr>
        <td class="style12">
            &nbsp;</td>
        <td class="style3">
            &nbsp;</td>
        <td class="style11">
            &nbsp;</td>
        <td class="style5">
            &nbsp;</td>
        <td class="style14">
            &nbsp;</td>
        <td class="style13">
           <asp:RegularExpressionValidator ID="rgexp_email" runat="server" 
                    ErrorMessage="Enter Correct Format" ControlToValidate="txtBox_emp_emailid" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
        <td class="style8">
            &nbsp;</td>
    </tr>
    <tr><td class="style12">Country</td>
        <td class="style3"><asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <asp:DropDownList ID="ddl_Parkercountry" runat="server" AutoPostBack="True" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource7" 
                                DataTextField="Country_name" 
            DataValueField="Country_id"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                
                                SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country] ORDER BY [Country_name]"></asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>

</td><td class="style11">Province</td><td class="style5"><asp:UpdatePanel ID="UpdatePanel5" runat="server"><ContentTemplate>
        <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="True" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource3" 
                                DataTextField="ProvinceName" DataValueField="ProvinceId" 
                                ValidationGroup="a" ></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                 
                                SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName"><SelectParameters>
                        <asp:ControlParameter ControlID="ddl_Parkercountry" Name="Country_id" 
                                        PropertyName="SelectedValue" Type="Int64" /></SelectParameters></asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>

</td><td class="style14" >City</td><td class="style13" ><asp:UpdatePanel 
            ID="UpdatePanel6" runat="server"><ContentTemplate>
        <asp:DropDownList ID="ddl_Parkercity" runat="server" CssClass="twitterStyleTextbox" 
                DataSourceID="SqlDataSource8" DataTextField="City_name" 
                DataValueField="City_id"></asp:DropDownList>&#160;<asp:SqlDataSource 
                ID="SqlDataSource8" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                onselecting="SqlDataSource6_Selecting" 
                SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id) order by City_name">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlstate" Name="Province_id" 
                            PropertyName="SelectedValue" Type="Int64" /></SelectParameters></asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>

</td>
        <td valign="bottom" class="style8">
            &nbsp;</td>
    </tr><tr><td class="style12">Postal Code</td>
        <td class="style3"><asp:TextBox ID="txtbox_emp_postal" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the Postal Code" 
                    ValidationGroup="a"></asp:TextBox>
                  


</td><td class="style11"  >Phone#</td><td class="style5"><asp:TextBox ID="txtbox_emp_phone" runat="server" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Phone Number" ValidationGroup="a"></asp:TextBox>



<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_emp_phone" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>

</td><td class="style14" >Cell#</td><td class="style13" >
        <asp:TextBox ID="txtbox_emp_cell" runat="server" CssClass="twitterStyleTextbox" 
            placeholder="Enter the  Cell Number" ValidationGroup="a"></asp:TextBox>



<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
            Enabled="True" TargetControlID="txtbox_emp_cell" WatermarkCssClass="watermark" 
            WatermarkText="Enter In Format (123)123-4567"></cc1:TextBoxWatermarkExtender>

</td>
        <td class="style8">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style12">
            &nbsp;</td>
        <td class="style3">
          <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_emp_postal" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator></td>
        <td class="style11">
            &nbsp;</td>
        <td class="style5">
           <asp:RegularExpressionValidator    ID="RegularExpressionValidator1" runat="server"  ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_emp_phone" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator></td>
        <td class="style14">
            &nbsp;</td>
        <td class="style13">
           <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
            ControlToValidate="txtbox_emp_cell" ErrorMessage="Enter Correct Format" 
            ForeColor="#FF3300" 
            ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
            ValidationGroup="b"></asp:RegularExpressionValidator></td>
        <td class="style8">
            &nbsp;</td>
    </tr>
    </table>
                 <table class="form-table" style="width: 1252px" ><tr><td class="style7" >
                         <asp:Button ID="btnclose1" runat="server" CssClass="button" 
                onclick="btnclose1_Click" style="text-align: center" Text="Close" />

</td><td><asp:Button ID="btn_save" runat="server" Text="Save" CssClass="button" 
                  onclick="btn_save_Click" />

</td></tr></table><asp:Panel ID="Panel1" runat="server">
            <fieldset>
            
            <asp:Repeater ID="rpt_emp" runat="server" Visible="False" onitemcommand="rpt_emp_ItemCommand" 
                >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                     Employee Code
                    </th>

                     <th>
                Employee Name
                    </th>
                   <%-- <th>Plan
                    </th>--%>
                    <th>
               Employee Email-Id
                    </th>
                   
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("empcode")%></td>
             
 
               <td>

            <%#Eval("emp_name")%></td>
                   <td>
          
            <%#Eval("emp_emailid")%></td>
           
        
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
            </fieldset> </asp:Panel>


                 
                  
                  
</ContentTemplate>

</cc1:TabPanel>
   

  


    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2"  CssClass="Initial" Enabled="false"><HeaderTemplate>
Add Vehicle
</HeaderTemplate>

    

<ContentTemplate>













                    <asp:Panel ID="Panel2" runat="server"  Width="100%" CssClass="parker-table">
                      <fieldset>
                    
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        DeleteCommand="DELETE FROM [tbl_parkerVehicleDetails] WHERE [ParkerVehicleId] = @ParkerVehicleId" 
        InsertCommand="INSERT INTO [tbl_parkerVehicleDetails] ([Parker_Id], [Make], [Model], [Year], [Color], [LicenseNo], [UserName]) VALUES (@Parker_Id, @Make, @Model, @Year, @Color, @LicenseNo, @UserName)" 
        SelectCommand="SELECT * FROM [tbl_parkerVehicleDetails] where username=@username " 
        
        UpdateCommand="UPDATE [tbl_parkerVehicleDetails] SET  [Make] = @Make, [Model] = @Model, [Year] = @Year, [Color] = @Color, [LicenseNo] = @LicenseNo WHERE [ParkerVehicleId] = @ParkerVehicleId">
        <DeleteParameters>
            <asp:Parameter Name="ParkerVehicleId" Type="Int64" />
        </DeleteParameters>
       <InsertParameters>
            
            <asp:Parameter Name="Make" Type="String" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="String" />
            <asp:Parameter Name="LicenseNo" Type="String" />
           
            <asp:SessionParameter Name="Parker_Id" SessionField="Parker_id" 
                Type="Int64" />
          <asp:SessionParameter Name="UserName" SessionField="UserName" 
                Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="username" SessionField="Username" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Parker_Id" Type="Int64" />
            <asp:Parameter Name="Make" Type="String" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="String" />
            <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="UserName" Type="String" />
            <asp:Parameter Name="ParkerVehicleId" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
        SelectCommand="SELECT [VehiMake] FROM [tbl_VehicleMakes] ORDER BY [VehiMake]">
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT [VehiColor] FROM [tbl_Vehicle_color]">
        </asp:SqlDataSource>
                    
                      <asp:ListView ID="ListView1" runat="server" DataKeyNames="ParkerVehicleId" 
        DataSourceID="SqlDataSource4" InsertItemPosition="LastItem" 
        onselectedindexchanged="ListView1_SelectedIndexChanged">
        <AlternatingItemTemplate>
            <tr style="background-color: #FAFAD2;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button"  
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button" />
                </td>
                
                <td >
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="background-color: #FFCC66;color: #000080;">
                <td> 
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" CssClass="button"  
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="button" 
                        Text="Cancel" />
                </td>
                
                <td>
               <%-- <asp:Label ID="ParkerVehicleIdLabel1" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' Visible="false" />
                        <asp:TextBox ID="Parker_IdTextBox" runat="server" 
                        Text='<%# Bind("Parker_Id") %>' Visible="false" />--%>
                 <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource5" DataTextField="VehiMake" 
                DataValueField="VehiMake" SelectedValue='<%# Bind("Make") %>'> </asp:DropDownList>

                    
                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
              

                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource6" DataTextField="VehiColor" 
                DataValueField="VehiColor" SelectedValue='<%# Bind("Color") %>'> </asp:DropDownList>
                  
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table id="Table1" runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" CssClass="add"  
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="cancel"  
                        Text="Clear" />
                </td>
                 
                <td>
                    
                    <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource5" DataTextField="VehiMake" 
                DataValueField="VehiMake" SelectedValue='<%# Bind("Make", "{0}") %>'> </asp:DropDownList>
                    

                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource6" DataTextField="VehiColor" 
                DataValueField="VehiColor" SelectedValue='<%# Bind("Color", "{0}") %>'> </asp:DropDownList>
                  <%--  <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />--%>
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
              
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="background-color: #FFFBD6;color: #333333;">
                <td>
             
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit"  />
                </td>
               
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td >
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td >
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
               
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table id="Table2" runat="server">
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="1"
                            style=" background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family:Open Sans; width:100%;">
                            <tr id="Tr2" runat="server" style="background-color: #FFFBD6;color: #333333;">
                                <th id="Th1" runat="server">
                                </th>
                               <%-- <th id="Th2" runat="server">
                                    ParkerVehicleId</th>
                                <th id="Th3" runat="server">
                                    Parker_Id</th>--%>
                                <th id="Th4"  runat="server">
                                    Make</th>
                                <th id="Th5" runat="server">
                                    Model</th>
                                <th id="Th6" runat="server">
                                    Year</th>
                                <th id="Th7" runat="server">
                                    Color</th>
                                <th id="Th8" runat="server">
                                    LicenseNo</th>
                                
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td2" runat="server" 
                        style="text-align: center;background-color: #6699ff;font-family: Verdana, Arial, Helvetica, sans-serif;color: #333333;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                    ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #FFCC66;font-weight: bold;color: #000080;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit" />
                </td>
                <td>
                    <%--<asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />--%>
                </td>
                <td>
                   <%-- <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />--%>
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                   <%-- <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />--%>
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
     </fieldset>
                      
                      </asp:Panel>
    <asp:Button ID="Button1" runat="server" Text="Save" CssClass="button" onclick="Button1_Click" />

    </ContentTemplate>
</cc1:TabPanel>
</cc1:TabContainer>
  </div>

    </div>

    </div>

        <div class="footer-container"></div>
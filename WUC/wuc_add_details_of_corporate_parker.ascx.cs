﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
public partial class WUC_wuc_add_details_of_corporate_parker : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    int corporate_id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //try
            //{
            //    DataSet mainhead = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='" + Session["corporate_username"].ToString() + "'");


            //    DataSet emp = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails  where CorporationID=" + Convert.ToInt32(mainhead.Tables[0].Rows[0][0]) + " and PlanRateID=" + Convert.ToInt32(Session["PlanIDforParker_Corporate"].ToString()));


            //    corporate_id = Convert.ToInt32(mainhead.Tables[0].Rows[0][0]);

            //    // DataSet mainhead = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='manu'");
            //    // DataSet emp = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails  where CorporationID=" + Convert.ToInt32(mainhead.Tables[0].Rows[0][0]) + " and PlanRateID=133" );


            //    empshow.Columns.Add("empcode", typeof(string));
            //    empshow.Columns.Add("emp_name", typeof(string));
            //    empshow.Columns.Add("emp_emailid", typeof(string));
            //    for (int i = 0; i < emp.Tables[0].Rows.Count; i++)
            //    {
            //        DataRow dr = empshow.NewRow();
            //        dr[0] = emp.Tables[0].Rows[i][15].ToString();
            //        dr[1] = emp.Tables[0].Rows[i][4].ToString() + " " + emp.Tables[0].Rows[i][5].ToString() + " " + emp.Tables[0].Rows[i][6].ToString();
            //        dr[2] = emp.Tables[0].Rows[i][3].ToString();

            //        empshow.Rows.Add(dr);

            //    }
            //    Session["empshow"] = empshow;
            //    if (empshow.Rows.Count > 0)
            //    {
            //        rpt_emp.Visible = true;
            //        rpt_emp.DataSource = empshow;
            //        rpt_emp.DataBind();
            //    }
            //    for (int i = 2020; i > 1990; i--)
            //    {
            //        ListItem li = new ListItem();
            //        li.Text = i.ToString();
            //        li.Value = i.ToString();
            //      //  ddl_year.Items.Add(li);

            //    }

            //    lbl_number.Text = Session["totalparker"].ToString();

            //    dt.Columns.Add("VehicalMake", typeof(string));
            //    dt.Columns.Add("VehicalModel", typeof(string));
            //    dt.Columns.Add("VehicalYear", typeof(string));
            //    dt.Columns.Add("VehicalColor", typeof(string));
            //    dt.Columns.Add("VehicalLicensePlate", typeof(string));
            //    dt.Columns.Add("emp_code", typeof(string));

            //    Session["datatable"] = dt;
            //}
            //catch
            //{
            //}

        }
    }
    DataTable empshow = new DataTable();
    DataTable dt=new DataTable();
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
            insertvehicledetails();
        }
        catch
        {
        }


    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (lbl_number.Text != "0")
            {
                if (lblerror.Visible == false)
                {
                  //  insertrec_parkerdetail();
                  //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);

                  // empshow = (DataTable)Session["empshow"] ;
                  // DataRow dr = empshow.NewRow();
                  // dr[0] = txtboxcode_employeecode.Text;
                  // dr[1] =txtbox_emp_firstname.Text+ " " + txtbox_emp_middlename.Text+ " " +txtbox_emp_lastname.Text;
                  // dr[2] = txtBox_emp_emailid.Text;

                  // empshow.Rows.Add(dr);
                  // rpt_emp.Visible = true;
                  // rpt_emp.DataSource = empshow;
                  // rpt_emp.DataBind();
                  //Session["code"] = txtboxcode_employeecode.Text;
                    TabContainer1.Tabs[1].Enabled = true;
                   TabContainer1.ActiveTabIndex = 1;
                   TabContainer1.Tabs[0].Enabled = false;
                  
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Fill Correct Employee Code.')", true);

                }
                txtBox_emp_address1.Text = "";
                txtBox_emp_address2.Text = "";
                txtbox_emp_cell.Text = "";
                txtBox_emp_emailid.Text = "";
                txtbox_emp_firstname.Text = "";
                txtbox_emp_lastname.Text = "";
                txtbox_emp_middlename.Text = "";
                txtbox_emp_phone.Text = "";
                txtbox_emp_postal.Text = "";
                txtboxcode_employeecode.Text = "";
                
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Limit Over.')", true);

            }
        }
        catch
        {

        }
    }

    int Planid;
    string username = "";
    string totalparker = "";
    int numberremain ;
    DataSet dscorporationid = new DataSet();
    public void insertrec_parkerdetail()
    {

        try
        {
            Planid = Convert.ToInt32(Session["PlanIDforParker_Corporate"].ToString());
            username = Session["corporate_username"].ToString();
            totalparker = Session["totalparker"].ToString();
        }
        catch
        {
        }


        try
        {
            hst.Clear();
            dscorporationid = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='" + username + "'");

            hst.Add("action", "insert");
            hst.Add("CorporationID", Convert.ToInt32(dscorporationid.Tables[0].Rows[0][0].ToString()));
            hst.Add("planrateID", Convert.ToInt32(Planid));
            hst.Add("emailid", txtBox_emp_emailid.Text);

            hst.Add("fname", txtbox_emp_firstname.Text);
            hst.Add("mname", txtbox_emp_middlename.Text);
            hst.Add("lname ", txtbox_emp_lastname.Text);

            hst.Add("address1", txtBox_emp_address1.Text);
            hst.Add("address2", txtBox_emp_address2.Text);
            hst.Add("countryID", ddl_Parkercountry.SelectedValue);
            hst.Add("provinceID", ddlstate.SelectedValue);
            hst.Add("cityID", ddl_Parkercity.SelectedValue);
            hst.Add("postalcode", txtbox_emp_postal.Text);

            hst.Add("phone", txtbox_emp_phone.Text);
            hst.Add("cell", txtbox_emp_cell.Text);
            hst.Add("EmployeeCode", txtboxcode_employeecode.Text);






            int result = objData.ExecuteNonQuery("[Sp_employeeDetailsCorporate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                numberremain = Convert.ToInt32(lbl_number.Text) - result;
                lbl_number.Text = numberremain.ToString();
                Session["numberofparkerremaining"] = lbl_number.Text;

                int i = 1;

                if (i >= 1)
                {

                    TabContainer1.Tabs[1].Enabled = true;
                    DataSet code = new clsInsert().fetchrec("select EmployeeCode,Corporate_EmployeeID from tbl_CorporateEmployeeDetails where CorporationID=" + Convert.ToInt32(dscorporationid.Tables[0].Rows[0][0].ToString()) + "and PlanRateID=" + Planid);
                    if (code.Tables[0].Rows.Count > 0)
                    {
                       // ddl_emp_code.DataSource = code.Tables[0];
                       // ddl_emp_code.DataTextField = "EmployeeCode";
                       // ddl_emp_code.DataValueField = "Corporate_EmployeeID";
                      //  ddl_emp_code.DataBind();
                    }
                  //  ddl_emp_code.Items.Insert(0, new ListItem("Select Employee Code", "0"));

                }

            }
        }
        catch
        {

        }





    }
    public void insertvehicledetails()
    {



        //try
        //{
        //    hst.Clear();


        //    hst.Add("action", "insertvehicledetails");
        //    hst.Add("employeeid", Convert.ToInt32(ddl_emp_code.SelectedValue));
        //    hst.Add("licenseno", txtbox_license.Text);
        //    hst.Add("makeid", ddl_make.SelectedValue);
        //    hst.Add("color", ddl_color.SelectedValue);
        
        //    hst.Add("modelid", txtbox_model.Text);
        //    hst.Add("year", ddl_year.Text);







        //    int result = objData.ExecuteNonQuery("[Sp_employeeDetailsCorporate]", CommandType.StoredProcedure, hst);
        //    if (result > 0)
        //    {

        //        dt = (DataTable)Session["datatable"];
        //        DataRow dr = dt.NewRow();
        //        dr[0] = ddl_make.SelectedItem.Text;
        //        dr[1] = txtbox_model.Text;
        //        dr[2] = ddl_year.SelectedItem.Text;
        //        dr[3] = ddl_color.SelectedItem.Text;
        //        dr[4] = txtbox_license.Text;
        //        dr[5] = ddl_emp_code.SelectedItem.Text;

        //        dt.Rows.Add(dr);


        //        Session["datatable"] = dt;
        //        if (dt.Rows.Count > 0)
        //        {
        //            Panel2.Visible = true;
        //            rpt_addvehicle.DataSource = dt;
        //            rpt_addvehicle.DataBind();
        //        }
        //    }
        //}
        //catch
        //{

        //        Session["datatable"] = dt;
        //        if (dt.Rows.Count > 0)
        //        {
        //            Panel2.Visible = true;
        //            //rpt_addvehicle.DataSource = dt;
        //            //rpt_addvehicle.DataBind();
        //        }
        //    }
        //}
        //catch
        //{


        //}





    }
    protected void SqlDataSource3_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }

    protected void SqlDataSource6_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void btnclose_Click(object sender, EventArgs e)
    {
        Session["datatable"] = null;
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('Admin_Add_Details_Of_Corporate_Parker.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

    }
    protected void btnclose1_Click(object sender, EventArgs e)
    {
        Session["datatable"] = null;
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('Admin_Add_Details_Of_Corporate_Parker.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

    }
    protected void txtboxcode_employeecode_TextChanged(object sender, EventArgs e)
    {

        DataSet mainhead = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='" + Session["corporate_username"].ToString() + "'");
        DataSet codereturn = new clsInsert().fetchrec("select EmployeeCode from tbl_CorporateEmployeeDetails where  CorporationID=" + Convert.ToInt32(mainhead.Tables[0].Rows[0][0].ToString()) + " and EmployeeCode='" + txtboxcode_employeecode.Text + "'");
        if (codereturn.Tables[0].Rows.Count > 0)
        {
            lblerror.Visible = true;
            lblerror.Text = "Already Exist... Try Another";
            txtboxcode_employeecode.Text = "";
        }
        else
        {
            lblerror.Visible = false;
            lblerror.Text = "";
        }
    }
    protected void rpt_emp_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            TabContainer1.Tabs[1].Enabled = false;
            TabContainer1.Tabs[0].Enabled = true;
            TabContainer1.ActiveTabIndex = 0;
        }
        catch
        {
        }
    }
    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
  

  

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_addmorevehicleparker_individual : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            for (int i = 1980; i < 2020; i++)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                ddl_year.Items.Add(li);
            }
            try
            {
                DataTable dtvehicle = new clsInsert().getparkersvehicles(Session["UserName"].ToString());
                int countdtvehicle = dtvehicle.Rows.Count;
                if (dtvehicle.Rows.Count > 0)
                {


                    rpt_parkervehicles.DataSource = dtvehicle;
                    rpt_parkervehicles.DataBind();

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
                }

                dt.Columns.Add("VehicalMake", typeof(string));
                dt.Columns.Add("VehicalModel", typeof(string));
                dt.Columns.Add("VehicalYear", typeof(string));
                dt.Columns.Add("VehicalColor", typeof(string));
                dt.Columns.Add("VehicalLicensePlate", typeof(string));

                Session["datatable"] = dt;
            }
            catch
            {

            }
        }
    }
    DataTable dt = new DataTable();
    protected void btn_add_Click(object sender, EventArgs e)
    {
        dt = (DataTable)Session["datatable"];
        DataRow dr = dt.NewRow();
        dr[0] = ddl_make.SelectedItem.Text;
        dr[1] = txtbox_model.Text;
        dr[2] = ddl_year.SelectedItem.Text;
        dr[3] = ddl_color.SelectedItem.Text;
        dr[4] = txtbox_license.Text;

        dt.Rows.Add(dr);

        Session["datatable"] = dt;
        if (dt.Rows.Count > 0)
        {
            Panel3.Visible = true;
            rpt_addvehicle.DataSource = dt;
            rpt_addvehicle.DataBind();
        }
    }
}
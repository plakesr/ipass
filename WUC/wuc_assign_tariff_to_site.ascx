﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_assign_tariff_to_site.ascx.cs" Inherits="WUC_wuc_assign_tariff_to_site" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 130px;
    }
    .style3
    {
        width: 160px;
    }
</style>

<div>
    <table class="style1">
        <tr>
            <td class="style2">
                Site Name</td>
            <td>
                <asp:DropDownList ID="ddl_sitename" runat="server" Height="31px" Width="298px">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    
    <panel>
    <fieldset><legend>Add Tariff</legend>
        <table class="style1">
            <tr>
                <td class="style3">
                    Tariff Name</td>
                <td>
                    <asp:DropDownList ID="ddl_tariffname" runat="server" Height="16px" 
                        Width="262px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Available Spaces</td>
                <td>
                    <asp:TextBox ID="txtbox_available_spaces" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_add" runat="server" Text="Add" Width="70px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            
        </table>
        <br />
            <br />
    </fieldset></panel>
    <br />
    <asp:GridView ID="GridView1" runat="server" BackColor="White" 
        BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" 
        GridLines="Horizontal">
        <Columns>
            <asp:BoundField HeaderText="Tariff Name" ReadOnly="True" />
            <asp:BoundField HeaderText="Space Available" ReadOnly="True" />
            <asp:BoundField HeaderText="Display" ReadOnly="True" />
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#333333" />
        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="White" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#487575" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#275353" />
    </asp:GridView>
    <br />
</div>


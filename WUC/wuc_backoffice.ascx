﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_backoffice.ascx.cs" Inherits="WUC_wuc_backoffice" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style3
    {
        width: 245px;
    }
    .style4
    {
        width: 293px;
    }
    .style5
    {
        width: 279px;
    }
    .twitterStyleTextbox
    {}
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

<asp:Panel ID="Panel1" runat="server" Visible="false">
<fieldset>
<legend>Fill Only Back Office</legend>

    <table class="form-table">
        <tr>
            <td class="style5">
                Date processed/date on refund cheque</td>
            <td>
               &nbsp;&nbsp; <asp:TextBox ID="txtbox_daterefund" runat="server" 
                    CssClass="twitterStyleTextbox"  ></asp:TextBox>
                     <cc1:CalendarExtender ID="txtbox_daterefund1" 
                            runat="server" Enabled="True" TargetControlID="txtbox_daterefund" ></cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Refund Processed By</td>
            <td>
               &nbsp;&nbsp; <asp:TextBox ID="txtbox_name" runat="server" 
                    CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Refund amount</td>
            <td>
                &nbsp;$<asp:TextBox ID="txtbox_amount" runat="server" 
                    CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Precise refund cheque #</td>
            <td>
                <asp:TextBox ID="txtbox_check" runat="server" 
                    CssClass="twitterStyleTextbox" ontextchanged="txtbox_check_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Client Type</td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="---Select---"></asp:ListItem>
                <asp:ListItem Text="Ontario Customer"></asp:ListItem>
                <asp:ListItem Text="British Coloumbia Customer"></asp:ListItem>

                </asp:DropDownList>
            </td>
        </tr>
    </table>

</fieldset>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server" Visible="false" >
<fieldset>
<legend>Refund Through Credit Card</legend>

    <table class="form-table">
        <tr>
            <td class="style3">
                Date received</td>
            <td>
                <asp:TextBox ID="txtbox_creditdate" runat="server" 
                    CssClass="twitterStyleTextbox"></asp:TextBox>
                     <cc1:CalendarExtender ID="txtbox_creditdate1" 
              runat="server" Enabled="True" TargetControlID="txtbox_creditdate" ></cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td class="style3">
                Received By </td>
            <td>
                <asp:TextBox ID="txtbox_creditname" runat="server" 
                    CssClass="twitterStyleTextbox" Enabled="false" Height="22px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style3">
                Credit card authorization code</td>
            <td>
                <asp:TextBox ID="txtbox_code" runat="server" CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>

</fieldset>
</asp:Panel>

<table class="style1">
    <tr>
        <td class="style4">
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_refund" runat="server" Text="Refund" CssClass="button" 
                onclick="btn_refund_Click" />
        </td>
    </tr>
</table>



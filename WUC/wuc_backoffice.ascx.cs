﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_backoffice : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();

    string status_paid = "True";
    string pmstatus = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            // = l.Text;
            // = l1.Text;
            // = amount.Text;
            try
            {

                if (Session["operatorid"].ToString() == "Accounting")
                {
                    Panel1.Visible = true;
                    txtbox_amount.Text = Session["REFUND_Amount"].ToString();
                    txtbox_name.Text = Session["Login"].ToString();
                }
                if (Session["operatorid"].ToString() == "Data Center")
                {

                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    txtbox_creditname.Text = Session["Login"].ToString();
                    txtbox_code.Text = Session["authorisationcode"].ToString();

                }
            }
            catch
            {
                Response.Redirect("default.aspx");
            }
        }

    }

    protected void txtbox_check_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn_refund_Click(object sender, EventArgs e)
    {
        try
        {
            

                if (Session["operatorid"].ToString() == "Accounting")
                {
                    add_accounting();
                    statusupdate();
                    Response.Redirect("Operator_Search_RefundableByParker.aspx");
                }
                if (Session["operatorid"].ToString() == "Data Center")
                {
                    add_datacenter();
                    statusupdate();
                    Response.Redirect("Operator_Search_RefundableByParker.aspx");
                }
           
          

        }
        catch
        {

        }
    }
    

    public void add_accounting()
    {
        try
        {
            hst.Clear();
            hst.Add("action", "update");
           
           
                
            hst.Add("refundtrackingid", Session["REFUND_TRACKING_ID"].ToString());

            hst.Add("amountrefund", Convert.ToDecimal(txtbox_amount.Text));
            hst.Add("dateofrefund", Convert.ToDateTime(txtbox_daterefund.Text));
            hst.Add("paidby", Session["Login"].ToString());
            hst.Add("checknumber", txtbox_check.Text);
            hst.Add("clienttype", DropDownList1.SelectedItem.Text);
            hst.Add("status_paid", status_paid);

            int result = objData.ExecuteNonQuery("[sp_refundpaidstatus]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
            }
        }
        catch
        {
        }
    }


    public void add_datacenter()
    {
        try
        {
            hst.Clear();
            hst.Add("action", "update");
            hst.Add("refundtrackingid", Session["REFUND_TRACKING_ID"].ToString());

            hst.Add("amountrefund", Convert.ToDecimal(Session["REFUND_Amount"].ToString()));
            hst.Add("dateofrefund", Convert.ToDateTime(txtbox_creditdate.Text));
            hst.Add("paidby", Session["Login"].ToString());
            hst.Add("checknumber", "");
            hst.Add("clienttype", "");
            hst.Add("status_paid", status_paid);
            int result = objData.ExecuteNonQuery("[sp_refundpaidstatus]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
            }
        }
        catch
        {
        }
    }
    
    public void statusupdate()
    {
        hst.Clear();

        if (Session["operatorid"].ToString() == "Accounting")
        {
            hst.Add("action", "statusupdatebyAccounting");
            hst.Add("refundtrackingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
            
                pmstatus = "True";
                hst.Add("pmstatusfortrans", pmstatus);
                hst.Add("pmdeclinereason", "");
                hst.Add("pmstatus", 3);

           
        }

        if (Session["operatorid"].ToString() == "Data Center")
        {
            hst.Add("action", "statusupdatebyDatacenter");
            hst.Add("refundtrackingid", Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));
            
                pmstatus = "True";
                hst.Add("pmstatusfortrans", pmstatus);
                hst.Add("pmdeclinereason", "");
                hst.Add("pmstatus", 3);

            
            
        }

        int result = objData.ExecuteNonQuery("[sp_RefundPayment]", CommandType.StoredProcedure, hst);
        {
        }
             
    
    }



   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
public partial class WUC_wuc_bill : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    decimal monthlyreservedcharges = Convert.ToDecimal("0.00");
    decimal monthlyrandomcharges = Convert.ToDecimal("0.00");
    decimal monthlycharges = Convert.ToDecimal("0.00");
   
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    string status = "true";
    string invoicenumber = "";
    string monthforinvoice = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {

            try
            {
                individualbilling();
            }
            catch
            {
            }
            //Billing Portion of Corporate Account Full Charge

            try
            {
                corporatefullbilling();
            }
            catch
            {
            }
            // Billing Portion Of Pay Per Use Corporate Parker

            try
            {
                corporatepayperuse();
            }
            catch
            {
            }



        }
        
    }
    decimal tax_sequence1 = Convert.ToDecimal("0.00");
    decimal tax_sequence2 = Convert.ToDecimal("0.00");
    decimal tax_sequence3 = Convert.ToDecimal("0.00");
    decimal tax_sequence4 = Convert.ToDecimal("0.00");
    decimal tax_sequence5 = Convert.ToDecimal("0.00");
    decimal tax_sequence6 = Convert.ToDecimal("0.00");
    decimal tax_sequence7 = Convert.ToDecimal("0.00");
    

    public void individualbilling()
    {
        try
        {
            DataTable dt = new clsInsert().countuserbill();
            RemoveDuplicateRows(dt, "Acct_Num");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DateTime lastdate = Convert.ToDateTime(dt.Rows[i][3].ToString());
                    DateTime date1 = Convert.ToDateTime(dt.Rows[i][3].ToString()).AddMonths(1);
                    DateTime date2 = DateTime.Now;


                    if (date1 <= date2)
                    {

                        while (date1 <= System.DateTime.Now)
                        {

                            try
                            {
                                DateTime today1 = date1;
                                //int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                                DateTime startOfMonth = new DateTime(today1.Year, today1.Month, Convert.ToInt32(1));


                                DateTime today = startOfMonth;
                                int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                                DateTime endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);




                                DateTime activedate = startOfMonth;
                                int year1 = activedate.Year;
                                int month1 = activedate.Month;
                                if (month1 < 10)
                                {
                                    monthforinvoice = "0" + Convert.ToString(month1);
                                }
                                if (month1 >= 10)
                                {
                                    monthforinvoice = Convert.ToString(month1);
                                }
                                string yearmonth = Convert.ToString(year1) + Convert.ToString(monthforinvoice);
                                DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
                                if (dsinvoice.Tables[0].Rows.Count > 0)
                                {
                                    string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                                    string[] strArr = oFName.Split('_');
                                    int sLength = strArr.Length;
                                    int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                                    if (number < 10)
                                    {
                                        invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                                    }
                                    if (number >= 10 && number < 100)
                                    {
                                        invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                                    }
                                    if (number >= 100 && number < 1000)
                                    {
                                        invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                                    }
                                    if (number >= 1000 && number < 10000)
                                    {
                                        invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                                    }
                                }
                                else
                                {
                                    invoicenumber = yearmonth + "_" + "000001";
                                }

                                //@invoicenumber,@startdate,@duedate,@subtotal,@tax,@totalamount,@dueamount,@billingtype,@Parkerusername
                                hst.Clear();

                                hst.Add("action", "insert_billing_corporate");
                                hst.Add("invoicenumber", invoicenumber);

                                hst.Add("Parkerusername", dt.Rows[i][10].ToString());

                                hst.Add("startdate", startOfMonth);
                                try
                                {
                                    DataSet dssequence1=new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=1 and LotId="+Convert.ToInt32(dt.Rows[i][14].ToString()));
                                    if (dssequence1.Tables[0].Rows.Count>0)
                                    {
                                        for (int se1 = 0; se1 < dssequence1.Tables[0].Rows.Count; se1++)
                                        {
                                            tax_sequence1 = tax_sequence1 +Convert.ToDecimal( dssequence1.Tables[0].Rows[se1][3].ToString());
                                        }
                                    }
                                    else
                                    {
                                        tax_sequence1 = Convert.ToDecimal("0.00");
                                    }

                                }
                                catch
                                {
                                }
                                tax_sequence1 = Convert.ToDecimal(tax_sequence1/100);

                                decimal tax = Convert.ToDecimal(tax_sequence1) * Convert.ToDecimal(dt.Rows[i][12].ToString());
                                decimal totalamount = tax + Convert.ToDecimal(dt.Rows[i][12].ToString());
                                tax_sequence1 = Convert.ToDecimal("0.00");

                                try
                                {
                                    DataSet dssequence2 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=2 and LotId=" + Convert.ToInt32(dt.Rows[i][14].ToString()));
                                    if (dssequence2.Tables[0].Rows.Count > 0)
                                    {
                                        for (int se1 = 0; se1 < dssequence2.Tables[0].Rows.Count; se1++)
                                        {
                                            tax_sequence2 = tax_sequence2 + Convert.ToDecimal(dssequence2.Tables[0].Rows[se1][3].ToString());
                                        }
                                    }
                                    else
                                    {
                                        tax_sequence2 = Convert.ToDecimal("0.00");

                                    }

                                }
                                catch
                                {
                                }
                                tax_sequence2 = Convert.ToDecimal(tax_sequence2 / 100);

                                decimal tax1 = Convert.ToDecimal(tax_sequence2) *totalamount;
                                decimal totalamount1 = tax1 + totalamount;
                                tax_sequence2 = Convert.ToDecimal("0.00");

                                try
                                {
                                    DataSet dssequence3 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=3 and LotId=" + Convert.ToInt32(dt.Rows[i][14].ToString()));
                                    if (dssequence3.Tables[0].Rows.Count > 0)
                                    {
                                        for (int se1 = 0; se1 < dssequence3.Tables[0].Rows.Count; se1++)
                                        {
                                            tax_sequence3 = tax_sequence3 + Convert.ToDecimal(dssequence3.Tables[0].Rows[se1][3].ToString());
                                        }
                                    }
                                    else
                                    {
                                        tax_sequence3 = Convert.ToDecimal("0.00");

                                    }
                                }
                                catch
                                {
                                }
                                tax_sequence3 = Convert.ToDecimal(tax_sequence3 / 100);
                                decimal tax2 = Convert.ToDecimal(tax_sequence3) * totalamount1;
                                decimal totalamount2 = tax2 + totalamount1;
                                tax_sequence3 = Convert.ToDecimal("0.00");

                                try
                                {
                                    DataSet dssequence4 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=4 and LotId=" + Convert.ToInt32(dt.Rows[i][14].ToString()));
                                    if (dssequence4.Tables[0].Rows.Count > 0)
                                    {
                                        for (int se1 = 0; se1 < dssequence4.Tables[0].Rows.Count; se1++)
                                        {
                                            tax_sequence4 = tax_sequence4 + Convert.ToDecimal(dssequence4.Tables[0].Rows[se1][3].ToString());
                                        }
                                    }
                                    else
                                    {
                                        tax_sequence4 = Convert.ToDecimal("0.00");

                                    }
                                }
                                catch
                                {
                                }
                                tax_sequence4 = Convert.ToDecimal(tax_sequence4 / 100);
                                decimal tax3 = Convert.ToDecimal(tax_sequence4) * totalamount2;
                                decimal totalamount3 = tax3 + totalamount2;
                                tax_sequence4 = Convert.ToDecimal("0.00");

                                decimal finaltax = totalamount3 - Convert.ToDecimal(dt.Rows[i][12].ToString());
                                // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
                                hst.Add("duedate", endOfMonth);
                                hst.Add("subtotal", Convert.ToDecimal(dt.Rows[i][12].ToString()));
                                hst.Add("tax", Convert.ToDecimal(finaltax));
                                hst.Add("details", "Invoice ");

                                hst.Add("totalamount", Convert.ToDecimal(totalamount3));
                                DataSet dt_creditcheck = new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='" + dt.Rows[i][10].ToString() + "' order by CreditNoteID desc");
                                if (dt_creditcheck.Tables[0].Rows.Count > 0)
                                {
                                    if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) > totalamount)
                                    {
                                        decimal dueamount = Convert.ToDecimal("0.00");
                                        hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                                    }
                                    if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) <= totalamount)
                                    {
                                        decimal dueamount = totalamount - Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString());
                                        hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                                    }
                                }

                                hst.Add("billingtype", Convert.ToInt32("3"));


                                int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);

                                if (result_billing > 0)
                                {
                                    string lastid_bill = invoicenumber;
                                    hst.Clear();
                                    // @billid,@billtitle,@amountparticular,@date,@accountnumber
                                    hst.Add("action", "fulldetailsaboutbill");
                                    hst.Add("billid", lastid_bill);
                                    hst.Add("billtitle", "Monthly Charges");
                                    hst.Add("amountparticular", Convert.ToDecimal(dt.Rows[i][12].ToString()));
                                    hst.Add("date", DateTime.Now);
                                    hst.Add("accountnumber", dt.Rows[i][10].ToString());

                                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                                    if (result_billing_full > 0)
                                    {
                                        hst.Clear();
                                        hst.Add("action", "insert");
                                        hst.Add("acctnumber", dt.Rows[i][10].ToString());
                                        hst.Add("reason", "Advance");
                                        if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) > totalamount)
                                        {
                                            decimal dueamount = Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) - totalamount;

                                            // hst.Add("dueamount ", Convert.ToDecimal(dueamount));
                                            hst.Add("creditamt", Convert.ToDecimal(dueamount));

                                        }
                                        if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) <= totalamount)
                                        {
                                            //decimal dueamount = totalamount - Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString());
                                            hst.Add("creditamt ", Convert.ToDecimal("0.00"));

                                        }
                                        hst.Add("dateoftransaction", DateTime.Now);
                                        hst.Add("status", status);

                                        int result_billing_full11 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                                        {
                                        }
                                    }



                                }

                                lastdate = lastdate.AddMonths(1);
                                date1 = date1.AddMonths(1);

                            }
                            catch
                            {

                            }
                        }



                    }

                }





            }
        }
        catch
        {
        }

    }
    public void corporatefullbilling()
    {
        try
            {

                DataTable dt_corporate = new clsInsert().countuserbill_corporate();
                RemoveDuplicateRows(dt_corporate, "Acct_Num");
                if (dt_corporate.Rows.Count > 0)
                {

                    for (int i = 0; i < dt_corporate.Rows.Count; i++)
                    {
                        if (dt_corporate.Rows[i][10].ToString() == "1")
                        {
                            DateTime lastdate = Convert.ToDateTime(dt_corporate.Rows[i][2].ToString());
                            DateTime date1 = Convert.ToDateTime(dt_corporate.Rows[i][2].ToString()).AddMonths(1);
                            DateTime date2 = DateTime.Now;


                            if (date1 <= date2)
                            {

                                while (date1 <= System.DateTime.Now)
                                {

                                    try
                                    {
                                        DateTime today1 = date1;
                                        //int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                                        DateTime startOfMonth = new DateTime(today1.Year, today1.Month, Convert.ToInt32(1));


                                        DateTime today = startOfMonth;
                                        int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                                        DateTime endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);




                                        DateTime activedate = startOfMonth;
                                        int year1 = activedate.Year;
                                        int month1 = activedate.Month;
                                        if (month1 < 10)
                                        {
                                            monthforinvoice = "0" + Convert.ToString(month1);
                                        }
                                        if (month1 >= 10)
                                        {
                                            monthforinvoice = Convert.ToString(month1);
                                        }
                                        string yearmonth = Convert.ToString(year1) + Convert.ToString(monthforinvoice);
                                        DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
                                        if (dsinvoice.Tables[0].Rows.Count > 0)
                                        {
                                            string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                                            string[] strArr = oFName.Split('_');
                                            int sLength = strArr.Length;
                                            int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                                            if (number < 10)
                                            {
                                                invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                                            }
                                            if (number >= 10 && number < 100)
                                            {
                                                invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                                            }
                                            if (number >= 100 && number < 1000)
                                            {
                                                invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                                            }
                                            if (number >= 1000 && number < 10000)
                                            {
                                                invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                                            }
                                        }
                                        else
                                        {
                                            invoicenumber = yearmonth + "_" + "000001";
                                        }

                                        //@invoicenumber,@startdate,@duedate,@subtotal,@tax,@totalamount,@dueamount,@billingtype,@Parkerusername
                                        hst.Clear();

                                        hst.Add("action", "insert_billing_corporate");
                                        hst.Add("invoicenumber", invoicenumber);

                                        hst.Add("Parkerusername", dt_corporate.Rows[i][9].ToString());

                                        hst.Add("startdate", startOfMonth);

                                        DataSet dt_monthlycharge_reserved = new clsInsert().fetchrec("select tbl_CorporateLotRatePlan.*,tbl_CorporateReservedParkingDetails.* from tbl_CorporateLotRatePlan inner join tbl_CorporateReservedParkingDetails on tbl_CorporateReservedParkingDetails.Planid=tbl_CorporateLotRatePlan.PlanID_Corporate where Corporateusername='" + dt_corporate.Rows[i][9].ToString() + "' and ParkingType=1");
                                        if (dt_monthlycharge_reserved.Tables[0].Rows.Count > 0)
                                        {
                                            for (int k = 0; k < dt_monthlycharge_reserved.Tables[0].Rows.Count; k++)
                                            {
                                                decimal totalmoney = Convert.ToDecimal(Convert.ToDecimal(dt_monthlycharge_reserved.Tables[0].Rows[k][2].ToString()) * Convert.ToInt32(dt_monthlycharge_reserved.Tables[0].Rows[k][9].ToString()));
                                                monthlyreservedcharges = monthlyreservedcharges + Convert.ToDecimal(totalmoney);
                                            }
                                        }
                                        else
                                        {
                                            monthlyreservedcharges = Convert.ToDecimal("0.00");
                                        }

                                        DataSet dt_monthlycharge_random = new clsInsert().fetchrec("select tbl_CorporateLotRatePlan.*,tbl_CorporateRandomParkingDetails.* from tbl_CorporateLotRatePlan inner join tbl_CorporateRandomParkingDetails on tbl_CorporateRandomParkingDetails.Planid=tbl_CorporateLotRatePlan.PlanID_Corporate where Corporateusername='"+dt_corporate.Rows[i][9].ToString()+"' and ParkingType=2");
                                        if (dt_monthlycharge_random.Tables[0].Rows.Count > 0)
                                        {
                                            for (int k = 0; k < dt_monthlycharge_random.Tables[0].Rows.Count; k++)
                                            {
                                                decimal totalmoney = Convert.ToDecimal(Convert.ToDecimal(dt_monthlycharge_random.Tables[0].Rows[k][2].ToString()) * Convert.ToInt32(dt_monthlycharge_random.Tables[0].Rows[k][9].ToString()));
                                                monthlyrandomcharges = monthlyrandomcharges + Convert.ToDecimal(totalmoney);
                                            }
                                        }
                                        else
                                        {
                                            monthlyrandomcharges = Convert.ToDecimal("0.00");
                                        }


                                        decimal fullmoneytopay = monthlyrandomcharges + monthlyreservedcharges;

                                        try
                                        {
                                            DataSet dssequence1 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=1 and LotId=" + Convert.ToInt32(dt_corporate.Rows[i][12].ToString()));
                                            if (dssequence1.Tables[0].Rows.Count > 0)
                                            {
                                                for (int se1 = 0; se1 < dssequence1.Tables[0].Rows.Count; se1++)
                                                {
                                                    tax_sequence1 = tax_sequence1 + Convert.ToDecimal(dssequence1.Tables[0].Rows[se1][3].ToString());
                                                }
                                            }
                                            else
                                            {
                                                tax_sequence1 = Convert.ToDecimal("0.00");
                                            }

                                        }
                                        catch
                                        {
                                        }
                                        tax_sequence1 = Convert.ToDecimal(tax_sequence1 / 100);

                                        decimal tax = Convert.ToDecimal(tax_sequence1) * Convert.ToDecimal(fullmoneytopay);
                                        decimal totalamount = tax + Convert.ToDecimal(fullmoneytopay);
                                        tax_sequence1 = Convert.ToDecimal("0.00");

                                        try
                                        {
                                            DataSet dssequence2 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=2 and LotId=" + Convert.ToInt32(dt_corporate.Rows[i][12].ToString()));
                                            if (dssequence2.Tables[0].Rows.Count > 0)
                                            {
                                                for (int se1 = 0; se1 < dssequence2.Tables[0].Rows.Count; se1++)
                                                {
                                                    tax_sequence2 = tax_sequence2 + Convert.ToDecimal(dssequence2.Tables[0].Rows[se1][3].ToString());
                                                }
                                            }
                                            else
                                            {
                                                tax_sequence2 = Convert.ToDecimal("0.00");

                                            }

                                        }
                                        catch
                                        {
                                        }
                                        tax_sequence2 = Convert.ToDecimal(tax_sequence2 / 100);

                                        decimal tax1 = Convert.ToDecimal(tax_sequence2) * totalamount;
                                        decimal totalamount1 = tax1 + totalamount;
                                        tax_sequence2 = Convert.ToDecimal("0.00");

                                        try
                                        {
                                            DataSet dssequence3 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=3 and LotId=" + Convert.ToInt32(dt_corporate.Rows[i][12].ToString()));
                                            if (dssequence3.Tables[0].Rows.Count > 0)
                                            {
                                                for (int se1 = 0; se1 < dssequence3.Tables[0].Rows.Count; se1++)
                                                {
                                                    tax_sequence3 = tax_sequence3 + Convert.ToDecimal(dssequence3.Tables[0].Rows[se1][3].ToString());
                                                }
                                            }
                                            else
                                            {
                                                tax_sequence3 = Convert.ToDecimal("0.00");

                                            }
                                        }
                                        catch
                                        {
                                        }
                                        tax_sequence3 = Convert.ToDecimal(tax_sequence3 / 100);
                                        decimal tax2 = Convert.ToDecimal(tax_sequence3) * totalamount1;
                                        decimal totalamount2 = tax2 + totalamount1;
                                        tax_sequence3 = Convert.ToDecimal("0.00");

                                        try
                                        {
                                            DataSet dssequence4 = new clsInsert().fetchrec("select * from  tbl_LotTaxMaster where Sequenceofcalc=4 and LotId=" + Convert.ToInt32(dt_corporate.Rows[i][12].ToString()));
                                            if (dssequence4.Tables[0].Rows.Count > 0)
                                            {
                                                for (int se1 = 0; se1 < dssequence4.Tables[0].Rows.Count; se1++)
                                                {
                                                    tax_sequence4 = tax_sequence4 + Convert.ToDecimal(dssequence4.Tables[0].Rows[se1][3].ToString());
                                                }
                                            }
                                            else
                                            {
                                                tax_sequence4 = Convert.ToDecimal("0.00");

                                            }
                                        }
                                        catch
                                        {
                                        }
                                        tax_sequence4 = Convert.ToDecimal(tax_sequence4 / 100);
                                        decimal tax3 = Convert.ToDecimal(tax_sequence4) * totalamount2;
                                        decimal totalamount3 = tax3 + totalamount2;
                                        tax_sequence4 = Convert.ToDecimal("0.00");

                                        decimal finaltax = totalamount3 - Convert.ToDecimal(fullmoneytopay);

                                    //    decimal tax = Convert.ToDecimal(".13") * Convert.ToDecimal(fullmoneytopay);
                                        // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
                                        hst.Add("duedate", endOfMonth);
                                        hst.Add("subtotal", Convert.ToDecimal(fullmoneytopay));
                                        hst.Add("tax", Convert.ToDecimal(finaltax));
                                        hst.Add("details", "Invoice ");
                                     //   decimal totalamount = tax + Convert.ToDecimal(fullmoneytopay);
                                        hst.Add("totalamount", Convert.ToDecimal(totalamount3));

                                        DataSet dt_creditcheck_corporate = new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='" + dt_corporate.Rows[i][9].ToString() + "' order by CreditNoteID desc");
                                        if (dt_creditcheck_corporate.Tables[0].Rows.Count > 0)
                                        {
                                            if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) > totalamount3)
                                            {
                                                decimal dueamount = Convert.ToDecimal("0.00");
                                                hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                                            }
                                            if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) <= totalamount3)
                                            {
                                                decimal dueamount = totalamount3 - Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString());
                                                hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                                            }
                                        }


                                      //  decimal dueamount = Convert.ToDecimal(totalamount) - Convert.ToDecimal("0");
                                       // hst.Add("dueamount ", Convert.ToDecimal(dueamount));
                                        hst.Add("billingtype", Convert.ToInt32(Convert.ToDecimal(dt_corporate.Rows[i][10].ToString())));


                                        int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);

                                        if (result_billing > 0)
                                        {
                                            string lastid_bill = invoicenumber;
                                            hst.Clear();
                                            // @billid,@billtitle,@amountparticular,@date,@accountnumber
                                            hst.Add("action", "fulldetailsaboutbill");
                                            hst.Add("billid", lastid_bill);
                                            hst.Add("billtitle", "Monthly Charges");
                                            hst.Add("amountparticular", Convert.ToDecimal(fullmoneytopay));
                                            hst.Add("date", DateTime.Now);
                                            hst.Add("accountnumber", dt_corporate.Rows[i][9].ToString());

                                            int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                                            if (result_billing_full > 0)
                                            {
                                                hst.Clear();
                                                hst.Add("action", "insert");
                                                hst.Add("acctnumber", dt_corporate.Rows[i][9].ToString());
                                                hst.Add("reason", "Advance");
                                                if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) > totalamount3)
                                                {
                                                    decimal dueamount = Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) - totalamount3;

                                                    // hst.Add("dueamount ", Convert.ToDecimal(dueamount));
                                                    hst.Add("creditamt", Convert.ToDecimal(dueamount));

                                                }
                                                if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) <= totalamount3)
                                                {
                                                    //decimal dueamount = totalamount - Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString());
                                                    hst.Add("creditamt ", Convert.ToDecimal("0.00"));

                                                }
                                                hst.Add("dateoftransaction", DateTime.Now);
                                                hst.Add("status", status);

                                                int result_billing_full11 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                                                {
                                                    monthlyrandomcharges = Convert.ToDecimal("0.00");
                                                    monthlyreservedcharges = Convert.ToDecimal("0.00");
                                                }
                                            }



                                        }

                                        lastdate = lastdate.AddMonths(1);
                                        date1 = date1.AddMonths(1);

                                    }
                                    catch
                                    {

                                    }
                                }



                            }

                        }
                        else
                        {
                        }




                    }
                }
            }
            catch
            {
            }
    }

    public void corporatepayperuse()
    {
          try
            {

                DataTable dt_corporate_payperuse = new clsInsert().countuserbill_corporate_payperuse();
                RemoveDuplicateRows(dt_corporate_payperuse, "Acct_Num");
                if (dt_corporate_payperuse.Rows.Count > 0)
                {

                    for (int i = 0; i < dt_corporate_payperuse.Rows.Count; i++)
                    {
                        if (dt_corporate_payperuse.Rows[i][10].ToString() == "2")
                        {
                            DateTime lastdate = Convert.ToDateTime(dt_corporate_payperuse.Rows[i][2].ToString());
                            DateTime date1 = Convert.ToDateTime(dt_corporate_payperuse.Rows[i][2].ToString()).AddMonths(1);
                            DateTime date2 = DateTime.Now;


                            if (date1 <= date2)
                            {

                                while (date1 <= System.DateTime.Now)
                                {

                                    try
                                    {
                                        DateTime today1 = date1;
                                        //int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                                        DateTime startOfMonth = new DateTime(today1.Year, today1.Month, Convert.ToInt32(1));


                                        DateTime today = startOfMonth;
                                        int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                                        DateTime endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);




                                        DateTime activedate = startOfMonth;
                                        int year1 = activedate.Year;
                                        int month1 = activedate.Month;
                                        if (month1 < 10)
                                        {
                                            monthforinvoice = "0" + Convert.ToString(month1);
                                        }
                                        if (month1 >= 10)
                                        {
                                            monthforinvoice = Convert.ToString(month1);
                                        }
                                        string yearmonth = Convert.ToString(year1) + Convert.ToString(monthforinvoice);
                                        DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
                                        if (dsinvoice.Tables[0].Rows.Count > 0)
                                        {
                                            string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                                            string[] strArr = oFName.Split('_');
                                            int sLength = strArr.Length;
                                            int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                                            if (number < 10)
                                            {
                                                invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                                            }
                                            if (number >= 10 && number < 100)
                                            {
                                                invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                                            }
                                            if (number >= 100 && number < 1000)
                                            {
                                                invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                                            }
                                            if (number >= 1000 && number < 10000)
                                            {
                                                invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                                            }
                                        }
                                        else
                                        {
                                            invoicenumber = yearmonth + "_" + "000001";
                                        }

                                        //@invoicenumber,@startdate,@duedate,@subtotal,@tax,@totalamount,@dueamount,@billingtype,@Parkerusername
                                        hst.Clear();

                                        hst.Add("action", "insert_billing_corporate");
                                        hst.Add("invoicenumber", invoicenumber);

                                        hst.Add("Parkerusername", dt_corporate_payperuse.Rows[i][9].ToString());

                                        hst.Add("startdate", startOfMonth);

                                        DataSet dt_monthlycharge_reserved = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where CorporationID='"+dt_corporate_payperuse.Rows[i][9].ToString()+"'");
                                        if (dt_monthlycharge_reserved.Tables[0].Rows.Count > 0)
                                        {
                                            for (int k = 0; k < dt_monthlycharge_reserved.Tables[0].Rows.Count; k++)
                                            {
                                                DataSet dt_money=new clsInsert().fetchrec("select * from tbl_CorporateLotRatePlan where tbl_CorporateLotRatePlan.PlanID_Corporate ="+Convert.ToInt32(dt_monthlycharge_reserved.Tables[0].Rows[k][2].ToString()));
                                                decimal totalmoney = Convert.ToDecimal(Convert.ToDecimal(dt_money.Tables[0].Rows[0][2].ToString()));
                                                
                                                monthlycharges = monthlycharges + Convert.ToDecimal(totalmoney);
                                            }
                                        }
                                        else
                                        {
                                            monthlycharges = Convert.ToDecimal("0.00");
                                        }

                                       


                                        decimal fullmoneytopay = monthlycharges;
                                        decimal tax = Convert.ToDecimal(".13") * Convert.ToDecimal(fullmoneytopay);
                                        // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
                                        hst.Add("duedate", endOfMonth);
                                        hst.Add("subtotal", Convert.ToDecimal(fullmoneytopay));
                                        hst.Add("tax", Convert.ToDecimal(tax));
                                        hst.Add("details", "Invoice ");
                                        decimal totalamount = tax + Convert.ToDecimal(fullmoneytopay);
                                        hst.Add("totalamount", Convert.ToDecimal(totalamount));

                                        DataSet dt_creditcheck_corporate = new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='" + dt_corporate_payperuse.Rows[i][9].ToString() + "' order by CreditNoteID desc");
                                        if (dt_creditcheck_corporate.Tables[0].Rows.Count > 0)
                                        {
                                            if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) > totalamount)
                                            {
                                                decimal dueamount = Convert.ToDecimal("0.00");
                                                hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                                            }
                                            if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) <= totalamount)
                                            {
                                                decimal dueamount = totalamount - Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString());
                                                hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                                            }
                                        }


                                        //  decimal dueamount = Convert.ToDecimal(totalamount) - Convert.ToDecimal("0");
                                        // hst.Add("dueamount ", Convert.ToDecimal(dueamount));
                                        hst.Add("billingtype", Convert.ToInt32(Convert.ToDecimal(dt_corporate_payperuse.Rows[i][10].ToString())));


                                        int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);

                                        if (result_billing > 0)
                                        {
                                            string lastid_bill = invoicenumber;
                                            hst.Clear();
                                            // @billid,@billtitle,@amountparticular,@date,@accountnumber
                                            hst.Add("action", "fulldetailsaboutbill");
                                            hst.Add("billid", lastid_bill);
                                            hst.Add("billtitle", "Monthly Charges");
                                            hst.Add("amountparticular", Convert.ToDecimal(fullmoneytopay));
                                            hst.Add("date", DateTime.Now);
                                            hst.Add("accountnumber", dt_corporate_payperuse.Rows[i][9].ToString());

                                            int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                                            if (result_billing_full > 0)
                                            {
                                                hst.Clear();
                                                hst.Add("action", "insert");
                                                hst.Add("acctnumber", dt_corporate_payperuse.Rows[i][9].ToString());
                                                hst.Add("reason", "Advance");
                                                if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) > totalamount)
                                                {
                                                    decimal dueamount = Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) - totalamount;

                                                    // hst.Add("dueamount ", Convert.ToDecimal(dueamount));
                                                    hst.Add("creditamt", Convert.ToDecimal(dueamount));

                                                }
                                                if (Convert.ToDecimal(dt_creditcheck_corporate.Tables[0].Rows[0][2].ToString()) <= totalamount)
                                                {
                                                    //decimal dueamount = totalamount - Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString());
                                                    hst.Add("creditamt ", Convert.ToDecimal("0.00"));

                                                }
                                                hst.Add("dateoftransaction", DateTime.Now);
                                                hst.Add("status", status);

                                                int result_billing_full11 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                                                {
                                                    monthlycharges = Convert.ToDecimal("0.00");
                                                }
                                            }



                                        }

                                        lastdate = lastdate.AddMonths(1);
                                        date1 = date1.AddMonths(1);

                                    }
                                    catch
                                    {

                                    }
                                }



                            }

                        }
                        else
                        {
                        }




                    }
                }

            }
            catch
            {
            }

    }
       
}
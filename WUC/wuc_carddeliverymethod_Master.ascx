﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_carddeliverymethod_Master.ascx.cs" Inherits="WUC_wuc_carddeliverymethod_Master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

    </style>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>

<asp:Panel ID="Panel2" runat="server">

<div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="form">
<label> Customer Name</label>
<asp:DropDownList ID="ddl_sitename" runat="server" 
                     AutoPostBack="True"  
        CssClass="twitterStyleTextbox" 
        onselectedindexchanged="ddl_sitename_SelectedIndexChanged" >
                  

                </asp:DropDownList>
               
                <div class="clearfix"></div>
                <label> Campus Name</label>
                <asp:DropDownList ID="ddlcampus" runat="server" AutoPostBack="True" 
                  CssClass="twitterStyleTextbox" 
        onselectedindexchanged="ddlcampus_SelectedIndexChanged">
               

            </asp:DropDownList>
           
              <div class="clearfix"></div>
            <label>Lot Name</label>
            <asp:DropDownList ID="ddlLot" runat="server" 
                DataTextField="LotName"   
                CssClass=twitterStyleTextbox >
                    

            </asp:DropDownList>
          
              <div class="clearfix"></div>
            <label>Card Delivery Method Name</label>
            <asp:TextBox ID="txtbox_deliveryname" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
              <div class="clearfix"></div>
            <label>Culture Type</label>
            <div class="radio">  <asp:RadioButton ID="rbtn_English" runat="server" GroupName="a" 
                     Text="English" /> <asp:RadioButton ID="rbtn_French" runat="server" GroupName="a" Text="French" /></div>
            
                   <div class="clearfix"></div>
                 <label> Delivery Type</label>
                  <asp:DropDownList ID="ddl_deliverytype" runat="server" CssClass=twitterStyleTextbox >
                     <asp:ListItem Value="1">Pick up at CSC</asp:ListItem>
                     <asp:ListItem Value="2">To be delivered</asp:ListItem>
                 </asp:DropDownList>
                <%-- <cc1:DropDownExtender ID="ddl_deliverytype_DropDownExtender" runat="server" 
                     DynamicServicePath="" Enabled="True" TargetControlID="ddl_deliverytype">
                 </cc1:DropDownExtender>--%>
                   <div class="clearfix"></div>
                 <label>Charges</label>
                  <asp:TextBox ID="txtbox_charges" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                  <div class="clearfix"></div>
                  <label>&nbsp;</label>
                  <asp:Button ID="btn_add" runat="server" onclick="btn_add_Click" Text="Submit" 
                CssClass=button  />
</div>

    
    </ContentTemplate>
    </asp:UpdatePanel>
     
</div>


</asp:Panel>
<div style="width:100%; float:left; margin:10px 0 0 0;">
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource2" 
    onitemcommand="Repeater1_ItemCommand">
    <HeaderTemplate>
    <div style="max-height:420px; overflow-y:auto;">
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                       Lot Name
                    </th>
                      <th>
                        Card Delivery Name
                    </th>
                     <th>
                        Charges
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>     <%#Eval("LotName")%></td>
                <td>    <%#Eval("CardDeliveryName")%></td>
                  <td>  <%#Eval("Charges")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("CardDeliveryId") %>' runat="server"><img src="images/edit.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("CardDeliveryId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>

                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("CardDeliveryId") %>' 
                                        Visible="False" />
                    </td>
            </tr>
            <asp:Label ID="CardDeliveryId" runat="server" Text='<%# Eval("CardDeliveryId") %>' 
                                        Visible="False" />
           
  <asp:Label ID="SiteName" runat="server" Text='<%# Eval("LotName") %>' 
                                        Visible="False"></asp:Label>
 
    <asp:Label ID="CardDeliveryName" runat="server" Text='<%# Eval("CardDeliveryName") %>' 
                                        Visible="False"></asp:Label> 
  
           <asp:Label ID="Charges" runat="server" Text='<%# Eval("Charges") %>'    Visible="False"></asp:Label>
            <asp:Label ID="CultureType" runat="server" Text='<%# Eval("CultureType") %>' 
                                        Visible="False"></asp:Label>                          
       <asp:Label ID="CardDeliveryType" runat="server" Text='<%# Eval("CardDeliveryType") %>' 
                                        Visible="False"></asp:Label> 
       </ItemTemplate>
        <FooterTemplate>
            </table>
            </div>
        </FooterTemplate>
</asp:Repeater>
<asp:SqlDataSource ID="SqlDataSource2" runat="server" 
    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
    
    SelectCommand="
select tbl_LotMaster.LotName,tbl_CardDeliveryMaster.*,tbl_CardDeliveryID.* from tbl_CardDeliveryMaster
inner join tbl_CardDeliveryID on tbl_CardDeliveryID.card_delivery_id=tbl_CardDeliveryMaster.CardDeliveryId
inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_CardDeliveryID.Lotid"></asp:SqlDataSource>
</fieldset>
</asp:Panel>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_carddeliverymethod_Master : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {
                    Fillddlsites();
                }
                catch
                {
                }
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }

        }
    }

    public void Fillddlsites()
    {

        List<clsData> lst = new clsInsert().GetSiteslist(0);
        if (lst.Count() > 0)
        {
            ddl_sitename.DataSource = lst;
            ddl_sitename.DataTextField = "SiteName";
            ddl_sitename.DataValueField = "SiteId";
            ddl_sitename.DataBind();
        }
        ddl_sitename.Items.Insert(0, new ListItem("Select Sites", "0"));

    }
    public void Fillddlcampus(int siteid)
    {

        List<clsData> lst = new clsInsert().GetCampuslist(siteid);
        if (lst.Count() > 0)
        {
            ddlcampus.DataSource = lst;
            ddlcampus.DataTextField = "campusname";
            ddlcampus.DataValueField = "campusid";
            ddlcampus.DataBind();
        }
        ddlcampus.Items.Insert(0, new ListItem("Select Campus", "0"));

    }

    public void Fillddllots(int campusid)
    {

        List<clsData> lst = new clsInsert().Getlotslist(campusid);
        if (lst.Count() > 0)
        {
            ddlLot.DataSource = lst;
            ddlLot.DataTextField = "lotname";
            ddlLot.DataValueField = "lotid";
            ddlLot.DataBind();
        }
        ddlLot.Items.Insert(0, new ListItem("Select Lots", "0"));

    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
            //string msg = "1";
            addcarddelivery();
            //Response.Redirect("Admin_CardDeliveryMaster.aspx?message=" + msg);

            

            
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);

        }
    }
    string s = "true";
    int culture;
    string msg;
    public void addcarddelivery()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("CardDeliveryName",txtbox_deliveryname.Text);
            if(rbtn_English.Checked==true)
            {
                culture = 1;
                 hst.Add("CultureType", culture);
            }
            if (rbtn_French.Checked == true)
            {
                culture = 2;
                hst.Add("CultureType", culture);
            }
            hst.Add("Charges", txtbox_charges.Text);
            hst.Add("ModifiedBy", "card delivery");
            hst.Add("CardDeliveryType", ddl_deliverytype.SelectedItem.Value);
            hst.Add("Address", "h-392");
            hst.Add("oprator", s);
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_CardDeliveryMethod]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                clsInsert cs = new clsInsert();
                DataSet ds1 = cs.select_operation("select CardDeliveryId from tbl_CardDeliveryMaster where CardDeliveryName='" + txtbox_deliveryname.Text + "'order by  CardDeliveryId desc");

                int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
                hst.Clear();

                hst.Add("action", "addid");
                hst.Add("siteid", ddlLot.SelectedValue);
                hst.Add("id", lastid);
                int result1 = objData.ExecuteNonQuery("[sp_CardDeliveryMethod]", CommandType.StoredProcedure, hst);
                if (result1 > 0)
                {
                    msg = "1";
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_CardDeliveryMaster.aspx?message=" + msg);
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_CardDeliveryMaster.aspx?message=" + msg);

                    }

                  //  Response.Redirect("Admin_CardDeliveryMaster.aspx");

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                    // lbl_error.Visible = true;

                }
              //  Response.Redirect("Admin_CardDeliveryMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delcarddeliver(ID);
                    Response.Redirect("Admin_CardDeliveryMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delcarddeliver(ID);
                        Response.Redirect("Operator_CardDeliveryMaster.aspx");
                    }
                }

            }
            catch
            {
                // Response.Redirect("default.aspx");
            }
          
        }
        if (e.CommandName == "cmdEdit")
        {



            Label l1 = Repeater1.Items[i].FindControl("CardDeliveryId") as Label;


            Label l3 = Repeater1.Items[i].FindControl("SiteName") as Label;

            Label l5 = Repeater1.Items[i].FindControl("CardDeliveryName") as Label;
            Label l6 = Repeater1.Items[i].FindControl("Charges") as Label;
            Label l7 = Repeater1.Items[i].FindControl("CultureType") as Label;
            Label l8 = Repeater1.Items[i].FindControl("CardDeliveryType") as Label;
            try
            {
                Session["CardDeliveryId"] = l1.Text;
                Session["SiteName"] = l3.Text;

                Session["CardDeliveryName"] = l5.Text;

                Session["Charges"] = l6.Text;
                Session["CultureType"] = l7.Text;
                Session["CardDeliveryType"] = l8.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");

            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditCardDeliveryMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditCardDeliveryMaster.aspx");
                }
            }
          

        }
    }
    public void delcarddeliver(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_CardDeliveryMethod]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                
            
              //  Response.Redirect("Admin_CardDeliveryMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delete Data Please Try Again.')", true);

               // Response.Redirect("Admin_CardDeliveryMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delete Data Please Try Again.')", true);

           // Response.Redirect("Admin_CardDeliveryMaster.aspx");

        }


    }
    protected void ddlcampus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Fillddllots(Convert.ToInt32(ddlcampus.SelectedValue));
        }
        catch
        {
        }
    }
    protected void ddl_sitename_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Fillddlcampus(Convert.ToInt32(ddl_sitename.SelectedValue));
        }
        catch
        {
        }
    }
}
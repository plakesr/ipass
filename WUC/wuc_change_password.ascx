﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_change_password.ascx.cs" Inherits="WUC_wuc_change_password" %>
<style type="text/css">

    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 136px;
    }
       .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 169px;
    }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div>
    <asp:Panel ID="Panel1" runat="server">
    <fieldset>
    <table class="style1">
        <tr>
            <td class="style2">
                Current Password :</td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <asp:TextBox ID="txtbox_current_password" runat="server" ValidationGroup="u" 
                    TextMode="Password" CssClass="twitterStyleTextbox"  
                    ontextchanged="txtbox_current_password_TextChanged" AutoPostBack="True"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_current_password" runat="server" 
                    ControlToValidate="txtbox_current_password" ErrorMessage="*" 
                    ForeColor="#CC0000" ValidationGroup="u"></asp:RequiredFieldValidator>
                    </ContentTemplate>
                    <Triggers>  
  
        <asp:AsyncPostBackTrigger ControlID="txtbox_current_password" EventName="TextChanged" />
       

    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;New
                Password&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_new_password" runat="server" ValidationGroup="u"  CssClass=twitterStyleTextbox
                    Height="22px" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_new_password" runat="server" ErrorMessage="*" 
                    ForeColor="#CC0000" ControlToValidate="txtbox_new_password" 
                    ValidationGroup="u"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Confirm Password</td>
            <td>
                <asp:TextBox ID="txtbox_confirm_password" runat="server" TextMode="Password" ValidationGroup="u" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_confirm_password" runat="server" 
                    ControlToValidate="txtbox_confirm_password" ErrorMessage="*Please Enter the new password again" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="u"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cmpv_confirm_password" runat="server" 
                    ControlToCompare="txtbox_new_password" 
                    ControlToValidate="txtbox_confirm_password" 
                    ErrorMessage="Password is not match with new Password" Font-Bold="True" 
                    ForeColor="#CC0000" Visible="False"  ValidationGroup="u"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
               

                <asp:Label ID="lbl_error" runat="server" Font-Bold="True" ForeColor="#CC0000" 
                    Text="*current password is wrong" Visible="False"></asp:Label>
               

            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="u" 
                    onclick="btn_update_Click"  CssClass=button /> &nbsp; &nbsp; &nbsp;
                <asp:Button ID="btn_cancel" runat="server" Text="Cancel" ValidationGroup="f" CssClass=button />
            </td>
        </tr>
    </table>
    </fieldset>
    </asp:Panel>
</div>


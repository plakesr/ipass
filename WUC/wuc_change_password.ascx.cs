﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_change_password : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    if (Session["Login"].ToString() == null && Session["Login"].ToString() == "")
        //    {
        //        Response.Redirect("Login.aspx");
        //    }
        //}
        //catch
        //{
        //    Response.Redirect("Login.aspx");
        //}
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
            if (Session["Type"].ToString() == "individual")
            {
                try
                {
                    updatepassword();
                }
                catch
                {
                }
            }
            else if (Session["Type"].ToString() == "operator")
            {
                try
                {
                    updatepassword();
                }
                catch
                {
                }
            }
            else if (Session["Type"].ToString() == "admin")
            {
                try
                {
                    updatepassword();
                }
                catch
                {
                }
            }
            else if (Session["Type"].ToString() == "Corporate")
            {
                try
                {
                    updatepassword();
                }
                catch
                {
                }
            }
        
    }

    public void updatepassword()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "changepass");
            hst.Add("type", Session["Type"].ToString());
           
            hst.Add("username", Session["Login"].ToString());
            hst.Add("newusername", Session["Login"].ToString());

            hst.Add("pass", txtbox_current_password.Text);
            hst.Add("newpass", txtbox_confirm_password.Text);
           
          


            int result = objData.ExecuteNonQuery("[spGetLoginCheck]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('updated succesfully.')", true);
              


            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
    protected void txtbox_current_password_TextChanged(object sender, EventArgs e)
    {
        string ss = Session["password"].ToString();
        if (txtbox_current_password.Text != ss)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('current password is wrong.')", true);
        }
        else
        {
            txtbox_current_password.Text = ss;
        }


    }
}
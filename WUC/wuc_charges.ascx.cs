﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_charges : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = "1";
            addcharge();
            Response.Redirect("VehicleCharge.aspx?message=" + msg);

            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted Successfully.')", true);
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void addcharge()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("time_in_hours", txtbox_time.Text);
            hst.Add("operator_id", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);
           


            int result = objData.ExecuteNonQuery("[sp_vehicle_Charges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                clear();
              //  Response.Redirect("VehicleCharge.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }


    public void clear()
    {
        txtbox_time.Text = "";
    
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());

            delCity(ID);
        }
        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("Label1") as Label;
            Label l1 = rpt1.Items[i].FindControl("code") as Label;




            Session["id"] = l.Text;
            
            Session["time_in_hours"] = l1.Text;

            Response.Redirect("EditVehicleCharge.aspx");

        }
    }
    public void delCity(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("c_id", id);


            int result = objData.ExecuteNonQuery("[sp_vehicle_Charges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("VehicleCharge.aspx");

            }
            else
            {
                Response.Redirect("VehicleCharge.aspx");
            }
        }
        catch
        {
            Response.Redirect("VehicleCharge.aspx");

        }


    }
}
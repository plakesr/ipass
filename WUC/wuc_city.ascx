﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_city.ascx.cs" Inherits="WUC_wuc_city" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
       
.twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
      .style2
    {
        width: 181px;
        height: 49px;
    }
    
    .style4
    {
        height: 49px;
    }
    
    .form-table
    {
        width: 72%;
    }
    
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel2" runat="server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
<table class="form-table">
<tr>

        <td class="style2">
            Select Country</td>
        <td class="style4">
            <asp:DropDownList ID="ddl_country" runat="server" 
                CssClass="twitterStyleTextbox"  
                DataSourceID="SqlDataSource4" DataTextField="Country_name" 
                DataValueField="Country_id" AutoPostBack="True" >

                
            </asp:DropDownList>
            <%--<cc1:DropDownExtender ID="DropDownExtender1" runat="server" 
                DynamicServicePath="" Enabled="True" TargetControlID="ddl_country">
            </cc1:DropDownExtender>--%>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="SELECT * FROM [tbl_country] ORDER BY Country_name">
            </asp:SqlDataSource>
            &nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="ddl_country" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="gp_city"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="style2">
            Select Province</td>
        <td class="style4">
            <asp:DropDownList ID="ddl_state" runat="server"  CssClass="twitterStyleTextbox" 
               DataSourceID="SqlDataSource1" 
                DataTextField="ProvinceName" DataValueField="ProvinceId" >
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                SelectCommand="SELECT [ProvinceId], [Country_id], [ProvinceName] FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddl_country" Name="Country_id" 
                        PropertyName="SelectedValue" Type="Int64" />
                </SelectParameters>
            </asp:SqlDataSource>
            &nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="rfv_selectcountry" runat="server" 
                ControlToValidate="ddl_state" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="gp_city"></asp:RequiredFieldValidator>
        </td>
       
    </tr>
    <tr>
        <td class="style2">
            CityName</td>
        <td>
            <asp:TextBox ID="txtbox_cityname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_cityname_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_cityname" WatermarkCssClass ="watermark" WatermarkText="Enter City Name">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_cityname" runat="server" 
                ControlToValidate="txtbox_cityname" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="gp_city"></asp:RequiredFieldValidator>
        </td>
    </tr>
   
    <tr>
        <td class="style2">
            City Abbreviation</td>
        <td>
            <asp:TextBox ID="txtbox_cityabbr" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_cityabbr_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_cityabbr" WatermarkCssClass ="watermark" WatermarkText="Enter City Abbreviation">
            </cc1:TextBoxWatermarkExtender>
            
        </td>
    </tr>
   
    <tr>
        <td class="style2">
           
           
            </td>
        <td>
           
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click"  Text="Add" CssClass="button"
                ValidationGroup="gp_city" Height="35px" Width="79px" />
        </td>
    </tr>
</table>

 </ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="Panel1" runat="server">
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="select tbl_City.*,tbl_Province.* from tbl_City
inner join tbl_Province on tbl_City.Province_id=tbl_Province.ProvinceId
">
            </asp:SqlDataSource>
            <br />
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                        Province&nbsp;&nbsp;Name
                    </th>
                      <th>
                        City&nbsp;&nbsp;Name
                    </th>
                     <th>
                        City&nbsp;&nbsp; Abbreviation
                    </th>
                    <th>
                        Edit
                    </th>
                    <%--<th>
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>     <%#Eval("ProvinceName")%></td>
                <td>    <%#Eval("City_name")%></td>
                  <td>  <%#Eval("City_abbr")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("City_id") %>' runat="server"><img src="images/edit.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                    
                </td>
                <td>
               <%-- <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("City_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>--%>

                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("City_id") %>' 
                                        Visible="False" />
                    </td>
            </tr>
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("City_id") %>' 
                                        Visible="False" />
           
  <asp:Label ID="cityname" runat="server" Text='<%# Eval("City_name") %>' 
                                        Visible="False"></asp:Label>
 
    <asp:Label ID="abbr" runat="server" Text='<%# Eval("City_abbr") %>' 
                                        Visible="False"></asp:Label> 
  
           <asp:Label ID="state" runat="server" Text='<%# Eval("Province_id") %>' 
                                        Visible="False"></asp:Label>
       <asp:Label ID="country_id" runat="server" Text='<%# Eval("Country_id") %>' 
                                        Visible="False"></asp:Label>
       </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>


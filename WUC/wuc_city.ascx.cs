﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;

public partial class WUC_wuc_city : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {
                    
                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }
                    

                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                          //  Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
    string msg;
    protected void Button1_Click(object sender, EventArgs e)
    {
        
        try
        {
            addcity();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }

    public void addcity()
     {
        try
        {
            hst.Clear();
            
            hst.Add("action", "insert");
          

            hst.Add("province_id", Convert.ToInt32(ddl_state.SelectedItem.Value));
            hst.Add("city_name", txtbox_cityname.Text);
          
        
            hst.Add("city_abbrvtion", txtbox_cityabbr.Text);
        
           
          

            int result = objData.ExecuteNonQuery("[sp_city]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                clear();
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CityMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CityMaster.aspx?message=" + msg);

                }
              
                
              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }


    public void clear()
    {
        txtbox_cityname.Text = "";
      
        txtbox_cityabbr.Text = "";
      
    }

    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {

            int i = e.Item.ItemIndex;
            if (e.CommandName == "cmdDelete")
            {
                int ID = Convert.ToInt32(e.CommandArgument.ToString());

                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        delCity(ID);
                        Response.Redirect("Admin_CityMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        if (Session["lbldelete"].ToString() == "False")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                        }

                        else
                        {
                            delCity(ID);
                            Response.Redirect("Operator_CityMaster.aspx");
                        }
                    }

                }
                catch
                {
                    // Response.Redirect("default.aspx");
                }


            }
            if (e.CommandName == "cmdEdit")
            {

                Label l1 = rpt1.Items[i].FindControl("cityname") as Label;


                Label l3 = rpt1.Items[i].FindControl("abbr") as Label;

                Label l5 = rpt1.Items[i].FindControl("Label1") as Label;
                Label l6 = rpt1.Items[i].FindControl("state") as Label;
                Label l7 = rpt1.Items[i].FindControl("country_id") as Label;


                Session["City_name"] = l1.Text;
                Session["Country_id"] = l7.Text;

                Session["City_abbr"] = l3.Text;

                Session["City_id"] = l5.Text;
                Session["state_id"] = l6.Text;
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_EditCityMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbledit"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                    }
                    else
                    {
                        Response.Redirect("Operator_EditCityMaster.aspx");
                    }

                }

            }
        }
        catch
        {
        }
    }



    public void delCity(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("ID", id);


            int result = objData.ExecuteNonQuery("[sp_city]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CityMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }
                    else
                    {
                        Response.Redirect("Operator_CityMaster.aspx");
                    }
                }
              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

              
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

          

        }


    }
   
}
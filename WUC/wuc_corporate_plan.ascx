﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_corporate_plan.ascx.cs" Inherits="WUC_wuc_corporate_plan" %>
<div>
<table>
<tr>
<td>

<asp:Panel ID="Panel1" runat="server" >
        <fieldset >
        <legend> Random</legend>
        
           <div class="parking-price">
            <table >
                <tr>
                    <td >
                        Daily</td>
                    <td >
                        <asp:Repeater ID="rpt_RandomDaily" runat="server" 
                            onitemcommand="rpt_RandomDaily_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                        
                    </th>
                      <th>
                      
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td >$  <%#Eval("Amount")%> </td>
                <td >   <asp:Label ID="Label1" runat="server"   Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>  <%#Eval("chargebyname")%> </td> 

                  
               
                <td >
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server"><img src="images/Buy.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                    
                    
                </td>
               <asp:Label ID="random_daily_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="random_daily_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="chargeby_random_daily" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                        </asp:Repeater>

                    </td>
                </tr>
                <tr>
                    <td >
                        Monthly</td>
                    <td >
                        <asp:Repeater ID="rpt_RandomMonthly" runat="server" 
                            onitemcommand="rpt_RandomMonthly_ItemCommand">
                        <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                        
                    </th>
                      <th>
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td>  $   <%#Eval("Amount")%> </td>
                <td>   <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>'  runat="server"><img src="images/Buy.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="random_month_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="random_month_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>

                                          <asp:Label ID="chargeby_random_monthly" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                        </asp:Repeater>
                     </td>
                </tr>
               
            </table></div>
            </fieldset>
         </asp:Panel>
</td>
<td>
<asp:Panel ID="Panel2" runat="server">
         <fieldset >
         <legend>Reserved</legend>
         <div class="parking-price">
             <table>
                 <tr>
                     <td>
                         Daily</td>
                     <td >
                         <asp:Repeater ID="rpt_ReservedDaily" runat="server" 
                             onitemcommand="rpt_ReservedDaily_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                        
                    </th>
                      <th>
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td>    $ <%#Eval("Amount")%> </td>
                <td>   
                    <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server"><img src="images/Buy.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="reserved_daily_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="reserved_daily_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>
                                          <asp:Label ID="chargeby_reserved_daily" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
                 <tr>
                     <td >
                         Monthly</td>
                     <td >
                         <asp:Repeater ID="rpt_ReservedMonthly" runat="server" 
                             onitemcommand="rpt_ReservedMonthly_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                       
                    </th>
                      <th>
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td>  $   <%#Eval("Amount")%> </td>
                <td>   <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server"><img src="images/Buy.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="reserved_month_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                         <asp:Label ID="reserved_month_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>
                                          <asp:Label ID="chargeby_reserved_monthly" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
             </table>
             </div>
         </fieldset>
         </asp:Panel>
</td>
</tr>
</table>





         
</div>
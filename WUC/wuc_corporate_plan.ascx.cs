﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_corporate_plan : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            int lotid = int.Parse(Session["lotid"].ToString());
            if (lotid > 0)
            {

                DataTable dt = new clsInsert().get_Lotdetails_lotsearch(Convert.ToInt32(lotid));
                try
                {
                    if (dt.Rows.Count > 0)
                    {

                        if (dt.Rows[0]["Reserved_parking"].ToString() == "True")
                        {
                            int chargebyid = 3;
                            int parkingtypeid = 1;
                            DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearchmonth(lotid, chargebyid, parkingtypeid);
                            rpt_ReservedMonthly.DataSource = dt1;
                            rpt_ReservedMonthly.DataBind();
                        }

                        if (dt.Rows[0]["Random_parking"].ToString() == "True")
                        {
                            int chargebyid = 3;
                            int parkingtypeid = 2;
                            DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearchmonth(lotid, chargebyid, parkingtypeid);
                            //DataTable dt = new clsInsert().get_LotRatedetails_lotsearch(8,3,1)
                            rpt_RandomMonthly.DataSource = dt1;
                            rpt_RandomMonthly.DataBind();
                        }


                        if (dt.Rows[0]["Reserved_parking"].ToString() == "True")
                        {
                            //int chargebyid = 2;
                            int parkingtypeid = 1;
                            DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearch(lotid, 3, parkingtypeid);
                            rpt_ReservedDaily.DataSource = dt1;
                            rpt_ReservedDaily.DataBind();
                        }

                        if (dt.Rows[0]["Random_parking"].ToString() == "True")
                        {
                            int chargebyid = 3;
                            int parkingtypeid = 2;
                            DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearch(lotid, chargebyid, parkingtypeid);
                            //DataTable dt = new clsInsert().get_LotRatedetails_lotsearch(8,3,1)
                            rpt_RandomDaily.DataSource = dt1;
                            rpt_RandomDaily.DataBind();
                        }

                      
                    }
                }
                catch
                {
                    
                }
            }
            else
            {
               
            }
        }
        catch
        {
        }

    }
    protected void rpt_RandomDaily_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {

            int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
            Session["PlanrateID"] = PlanrateID;


            DataTable dbplan = (DataTable)Session["plantable"];
            DataRow dr2 = dbplan.NewRow();

            dr2[0] = Convert.ToInt32(PlanrateID);
            dr2[1] = Session["usernameforplan"].ToString();




            dbplan.Rows.Add(dr2);

            Session["plantable"] = dbplan;






            ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('AddCorporate_vehicleparker.aspx','New Windows','height=380, width=300,location=no');", true);

            //try
            //{
            //    if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
            //    {
            //        Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());


            //    }
            //}
            //catch
            //{
            //    Session["LOTIDForInserting"] = Session["lotid"].ToString();

            //}
            //Label parkinttype = rpt_RandomDaily.Items[i].FindControl("random_daily_parkingtype") as Label;
            //Label amount_random_daily_amount = rpt_RandomDaily.Items[i].FindControl("random_daily_amount") as Label;
            //Label chargeby = rpt_RandomDaily.Items[i].FindControl("chargeby_random_daily") as Label;
            //Session["Chargebyinv"] = chargeby.Text;
            //Session["amount_parking"] = amount_random_daily_amount.Text;
            //Session["Typeinv"] = "Random Parking";


            //Session["parkinttype"] = int.Parse(parkinttype.Text);


            //Response.Redirect("Registration_New_Parker.aspx");

        }
    }
    protected void rpt_RandomMonthly_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        if (e.CommandName == "cmdEdit")
        {

            int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
            Session["PlanrateID"] = PlanrateID;


            DataTable dbplan = (DataTable)Session["plantable"];
            DataRow dr2 = dbplan.NewRow();

            dr2[0] = Convert.ToInt32(PlanrateID);
            dr2[1] = Session["usernameforplan"].ToString();




            dbplan.Rows.Add(dr2);

            Session["plantable"] = dbplan;




            ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('AddCorporate_vehicleparker.aspx','New Windows','height=380, width=300,location=no');", true);

            //try
            //{
            //    if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
            //    {
            //        Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());


            //    }
            //}
            //catch
            //{
            //    Session["LOTIDForInserting"] = Session["lotid"].ToString();

            //}
            //Label parkinttype = rpt_RandomMonthly.Items[i].FindControl("random_month_parkingtype") as Label;

            //Label amount_random_month_amount = rpt_RandomMonthly.Items[i].FindControl("random_month_amount") as Label;
            //Session["amount_parking"] = amount_random_month_amount.Text;
            //Session["parkinttype"] = int.Parse(parkinttype.Text);
            //Label chargeby = rpt_RandomMonthly.Items[i].FindControl("chargeby_random_monthly") as Label;
            //Session["Chargebyinv"] = chargeby.Text;
            //Session["Typeinv"] = "Random Parking";

            //Response.Redirect("Registration_New_Parker.aspx");



        }
    }
    protected void rpt_ReservedDaily_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        if (e.CommandName == "cmdEdit")
        {

            int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
            Session["PlanrateID"] = PlanrateID;

            DataTable dbplan = (DataTable)Session["plantable"];
            DataRow dr2 = dbplan.NewRow();

            dr2[0] = Convert.ToInt32(PlanrateID);
            dr2[1] = Session["usernameforplan"].ToString();




            dbplan.Rows.Add(dr2);

            Session["plantable"] = dbplan;









            ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('AddCorporate_vehicleparker.aspx','New Windows','height=380, width=300,location=no');", true);

            //try
            //{
            //    if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
            //    {
            //        Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());


            //    }
            //}
            //catch
            //{
            //    Session["LOTIDForInserting"] = Session["lotid"].ToString();

            //}
            //Label parkinttype = rpt_ReservedDaily.Items[i].FindControl("reserved_daily_parkingtype") as Label;

            //Label amount_reserved_daily_amount = rpt_ReservedDaily.Items[i].FindControl("reserved_daily_amount") as Label;
            //Session["amount_parking"] = amount_reserved_daily_amount.Text;
            //Session["parkinttype"] = int.Parse(parkinttype.Text);
            //Session["Typeinv"] = "Reserved Parking";

            //Label chargeby = rpt_ReservedDaily.Items[i].FindControl("chargeby_reserved_daily") as Label;
            //Session["Chargebyinv"] = chargeby.Text;
            //Response.Redirect("Registration_New_Parker.aspx");

        }
    }
    protected void rpt_ReservedMonthly_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {
            int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
            Session["PlanrateID"] = PlanrateID;

            DataTable dbplan = (DataTable)Session["plantable"];
            DataRow dr2 = dbplan.NewRow();

            dr2[0] = Convert.ToInt32(PlanrateID);
            dr2[1] = Session["usernameforplan"].ToString();




            dbplan.Rows.Add(dr2);

            Session["plantable"] = dbplan;

            //try
            //{
            //    if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
            //    {
            //        Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());

            //    }
            //}
            //catch
            //{
            //    Session["LOTIDForInserting"] = Session["lotid"].ToString();

            //}
            //Label parkinttype = rpt_ReservedMonthly.Items[i].FindControl("reserved_month_parkingtype") as Label;

            //Label amount_reserved_month_amount = rpt_ReservedMonthly.Items[i].FindControl("reserved_month_amount") as Label;
            //Session["amount_parking"] = amount_reserved_month_amount.Text;
            //Session["parkinttype"] = int.Parse(parkinttype.Text);
            //Session["Typeinv"] = "Reserved Parking";

            //Label chargeby = rpt_ReservedMonthly.Items[i].FindControl("chargeby_reserved_monthly") as Label;
            //Session["Chargebyinv"] = chargeby.Text;
            //Response.Redirect("Registration_New_Parker.aspx");

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('AddCorporate_vehicleparker.aspx','New Windows','height=380, width=300,location=no');", true);

        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_country.ascx.cs" Inherits="WUC_wuc_country" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    
  .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    .style2
    {
        width: 98px;
    }
    .style3
    {
        width: 382px;
    }
</style>
<asp:Panel ID="Panel2" runat="server">

<table class="form-table">
    <tr>
        <td class="style2">
            <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </cc1:ToolkitScriptManager>
            CountryName</td>
        <td class="style3">
            <asp:TextBox ID="txtbox_cuntryname" runat="server" ValidationGroup="gp_country" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_cuntryname_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_cuntryname" WatermarkText="Enter Country Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_cntryname" runat="server" 
                ControlToValidate="txtbox_cuntryname" ErrorMessage="*" 
                ForeColor="#FF0066" ValidationGroup="gp_country"></asp:RequiredFieldValidator>
        </td>
    </tr>
   
    <tr>
        <td class="style2">
            CountryAbbreviation</td>
        <td class="style3">
            <asp:TextBox ID="txtbox_cuntryabbr" runat="server" ValidationGroup="gp_country" 
                CssClass="twitterStyleTextbox" ontextchanged="txtbox_cuntryabbr_TextChanged"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_cuntryabbr_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_cuntryabbr" WatermarkText="Enter Country Abbreviation" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_cntryAbb" runat="server" 
                ControlToValidate="txtbox_cuntryabbr" 
                ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="gp_country"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="style2">
            CountryFlag</td>
        <td class="style3">
            <asp:FileUpload ID="fup_cuntryflag" runat="server" 
                  Width="219px"  CssClass="twitterStyleTextbox" Height="30px" BorderStyle="none"  />
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style3">
            <asp:Button ID="btn_submit" runat="server"  onclick="btn_submit_Click" 
                Text="Add" ValidationGroup="gp_country" CssClass="button" Height="35px" Width="79px"/>
        </td>
    </tr>
</table>
</asp:Panel>
<asp:Panel ID="Panel1" runat="server"  Height="300px" ScrollBars="Vertical">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="SELECT * FROM [tbl_country]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource1" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Country&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Country&nbsp;&nbsp;Abbreviation
                    </th>
                    <th>
                    Country &nbsp;&nbsp; Flag
                    </th>
                    <th width="5%">
                        Edit
                    </th>
                    <%--<th width="5%">
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("Country_name")%>
                    </td><td>
                    <%#Eval("Country_Abbreviation")%>
                </td>
<td>
    <asp:Image ID="Image1" runat="server" ImageUrl='<%#"~/FlagImage/Resized/"+Eval("Country_flag") %>' />
                </td>
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Country_id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <%--<asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("Country_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>--%>
                    </td>
                    <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Country_name") %>' 
                                        Visible="False" />
                                   
                    <asp:Label ID="idlabel2" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>
                                       <asp:Label ID="idlabel3" runat="server" Text='<%# Eval("Country_flag") %>' 
                                        Visible="False"></asp:Label>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>







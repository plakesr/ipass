﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Data;
using System.Collections;

public partial class WUC_wuc_country : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {
                   
                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }
                  
                    
                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                           // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
    string ImageName = "";
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        try
        {

            
           
            if (fup_cuntryflag.HasFile)
            {
                string oFName = fup_cuntryflag.FileName.ToString().Trim();
                string[] strArr = oFName.Split('.');
                int sLength = strArr.Length;
                if (strArr[sLength - 1] == "jpg" || strArr[sLength - 1] == "png" || strArr[sLength - 1] == "gif")
                {
                    ImageName = txtbox_cuntryname.Text + "." + strArr[1];
                    string Path = Server.MapPath("~/FlagImage/");
                   // fup_cuntryflag.SaveAs(Path + ImageName);

                    
           
               string SaveLocation = Path + "Original\\" + ImageName;
              //  string SaveLocation2 = pth + "Big\\" + filename;
               string SaveLocation3 = Path + "Resized\\" + ImageName;
                
                try
                {
                    fup_cuntryflag.PostedFile.SaveAs(SaveLocation);

                    ///image resize start
                    ///
                    System.Drawing.Image img = System.Drawing.Image.FromFile(SaveLocation);

                    double iw = Convert.ToDouble(img.Width.ToString());
                    double ih = Convert.ToDouble(img.Height.ToString());
                    //Response.Write(iw.ToString() + "<br>");
                    //Response.Write(ih.ToString() + "<br>");
                    double percent = 1;
                    if (iw > 160)
                    {
                        percent = (160 / iw);
                    }

                    double niw = 16;
                    double nih = 16;

                    //   Response.Write(niw.ToString() + "<br>");
                    //   Response.Write(nih.ToString());


                    Bitmap b = new Bitmap((int)niw, (int)nih);
                    Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.DrawImage(img, 0, 0, (float)niw, (float)nih);
                    g.Dispose();

                    img = (System.Drawing.Image)b;

                    // Encoder parameter for image quality

                    // Jpeg image codec
                    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
                    //find the encoder with the image/jpeg mime-type
                    ImageCodecInfo ici = null;
                    foreach (ImageCodecInfo codec in codecs)
                    {

                        if (codec.MimeType == "image/jpeg")
                            ici = codec;

                    }

                    EncoderParameters ep = new EncoderParameters();

                    //Create an encoder parameter for quality with an appropriate level setting

                    ep.Param[0] = new EncoderParameter(Encoder.Quality, (long)100);

                    //Save the image with a filename that indicates the compression quality used

                    img.Save(SaveLocation3, ici, ep);
                    img.Dispose();
                    ///image resize end


                  
                    ///

                 }
                catch (Exception ex)
                {
                    //Response.Write("Error: " + ex.Message);
                    //Note: Exception.Message returns a detailed message that describes the current exception. 
                    //For security reasons, we do not recommend that you return Exception.Message to end users in 
                    //production environments. It would be better to put a generic error message. 
                }
                
                           }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload image only.')", true);
                    return;
                }
            }

         
            addcountry();
          
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }

    public void clear()
    {
        txtbox_cuntryabbr.Text = "";
        txtbox_cuntryname.Text = "";
    }

    string msg;
    public void addcountry()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("country_name", txtbox_cuntryname.Text);
            //hst.Add("country_code","aaa");
            hst.Add("country_flag", ImageName);
            hst.Add("country_abbrvtion",txtbox_cuntryabbr.Text);
           

            int result = objData.ExecuteNonQuery("[sp_country]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CountryMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CountryMaster.aspx?message=" + msg);

                }

            

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

               

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

           

        }

    }
    public void delCountry(int id)
    {


        clsInsert obj=new clsInsert ();

        DataSet ds1 = obj.fetchrec("select c.City_id  from tbl_City c, tbl_Province p,tbl_country co where co.Country_id =p.Country_id and p.ProvinceId =c.Province_id and co.Country_id ="+id );

        for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
        {
            delCity(int.Parse(ds1.Tables[0].Rows[i][0].ToString()));
        }
        
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("ID", id);
            hst.Add("state_countryid", id);

            int result = objData.ExecuteNonQuery("[sp_country]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               
               

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

               
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

          

        }


    }

    public void delstate(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);
            int id1 = id;
            hst.Add("city_provinceid", id1);

            int result = objData.ExecuteNonQuery("[sp_State]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
             

            }
            else
            {
              
            }
        }
        catch
        {
          

        }


    }
    public void delCity(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("ID", id);


            int result = objData.ExecuteNonQuery("[sp_city]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               

            }
            else
            {
               
            }
        }
        catch
        {
          

        }


    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID =Convert.ToInt32( e.CommandArgument.ToString());
            if (Session["Type"].ToString() == "admin")
            {
                delCountry(ID);
                Response.Redirect("Admin_CountryMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbldelete"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                }
                else
                {
                    delCountry(ID);
                    Response.Redirect("Operator_CountryMaster.aspx");
                }
            }
          
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;
            

            Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
            Label l3 = rpt1.Items[i].FindControl("idlabel3") as Label;
            try
            {

                Session["countryname"] = l.Text;

                Session["countryabbr"] = l2.Text;

                Session["Country_flag"] = l3.Text;
            }
            catch
            {
                Response.Redirect("default.aspx");
            }
            int id = Convert.ToInt32(e.CommandArgument.ToString());
            Session["id"] = id;
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditCountryMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditCountryMaster.aspx");
                }

            }
           

        }
    }
    protected void txtbox_cuntryabbr_TextChanged(object sender, EventArgs e)
    {

    }
}
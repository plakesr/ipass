﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_detailsofduepayment : System.Web.UI.UserControl
{
    string u_name="";
    string address, name, lotname, plan, totalamount = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["detailofdue"] != null)
            {




                DataSet_Bill ds1 = (DataSet_Bill)Session["detailofdue"];
                txtbox_address.Text = ds1.Tables["Table_bill"].Rows[0][2].ToString();
                txtbox_lotname.Text = ds1.Tables["Table_bill"].Rows[0][4].ToString();
                txtbox_name.Text = ds1.Tables["Table_bill"].Rows[0][3].ToString();
                txtbox_parkingtype.Text = ds1.Tables["Table_bill"].Rows[0][5].ToString();
                txtbox_plan.Text = ds1.Tables["Table_bill"].Rows[0][6].ToString();
                txtbox_totaldue.Text ="$"+ ds1.Tables["Table_bill"].Rows[0][1].ToString();
                u_name = ds1.Tables["Table_bill"].Rows[0][0].ToString();
                Label1.Text = u_name;
                Repeater1.DataSource = ds1.Tables["BillInformation"];
                Repeater1.DataBind();
            }
        }
        catch
        {
        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Session["detailofdue"] = null;
        Response.Redirect("BillingofParker.aspx");
    }
    decimal amountduetotal = Convert.ToDecimal(0.00);
    decimal amountduetotal1 = Convert.ToDecimal(0.00);
    clsData obj = new clsData();

    protected void btn_invoice_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("Username", typeof(string));
        dt.Columns.Add("DueAmount", typeof(decimal));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("Address", typeof(string));
        dt.Columns.Add("LotName", typeof(string));
        dt.Columns.Add("ParkingType", typeof(string));
        dt.Columns.Add("Plan", typeof(string));
        dt.Columns.Add("", typeof(string));




        DataSet_Bill h = new DataSet_Bill();
        h.Table_bill.Clear();
        h.Amount_table.Clear();

        DataSet ds = new clsInsert().fetchrec(" select distinct  billing. Acct_Num, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid, lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,    tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice as billing ,tbl_LotMaster as lot     where lr.LotRateid =p.PlanRateid and billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id      and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 and p.UserName='" + u_name + "'");
        // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");

        if (ds.Tables[0].Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    DataRow dr = dt.NewRow();
                    DataRow dr1 = h.Table_bill.NewRow();

                    dr[0] = ds.Tables[0].Rows[i][0].ToString();

                    dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                    dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                    dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                    dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                    if (ds.Tables[0].Rows[i][4].ToString() == "1")
                    {
                        dr1[5] = "Reserved Parking";
                    }
                    if (ds.Tables[0].Rows[i][4].ToString() == "2")
                    {
                        dr1[5] = "Random Parking";
                    }

                    dr1[6] = ds.Tables[0].Rows[i][29].ToString();


                    DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                    if (dtamount.Rows.Count > 0)
                    {

                        for (int j = 0; j < dtamount.Rows.Count; j++)
                        {


                            amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());

                            DataRow dr2 = h.Amount_table.NewRow();
                            DateTime start_date = Convert.ToDateTime(dtamount.Rows[j]["Date Issued"]);
                            DateTime end_date = Convert.ToDateTime(dtamount.Rows[j]["DueDate"]);

                            string start = start_date.ToShortDateString();
                            dr2[0] = start_date.ToShortDateString();

                            dr2[1] = end_date.ToShortDateString();
                            dr2[2] = "$" + dtamount.Rows[j]["Amt_Outstanding"].ToString();

                            h.Amount_table.Rows.Add(dr2);
                        }
                    }
                    if (amountduetotal == amountduetotal1)
                    {
                    }
                    else
                    {
                        dr[1] = amountduetotal;
                        dr1[1] ="$"+ amountduetotal;
                        dt.Rows.Add(dr);
                        h.Table_bill.Rows.Add(dr1);
                        amountduetotal = Convert.ToDecimal(0.00);
                    }


                }
                Session["detail_invoice"] = h;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);

            }
            catch
            {
            }
            if (dt.Rows.Count > 0)
            {


            }
        }
    }
    protected void btn_sendmail_Click(object sender, EventArgs e)
    {

        try
        {
            DataSet_Bill ds1 = (DataSet_Bill)Session["detailofdue"];
            DataSet_Bill h = new DataSet_Bill();
            h.Table_bill.Clear();
            h.Amount_table.Clear();

            DataSet ds = new clsInsert().fetchrec("select distinct  billing. Acct_Num, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 and p.UserName='" + Label1.Text + "'");
            // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
            string table = "";
            string parkingtype = "";
            string UserName = "";

            if (ds.Tables[0].Rows.Count > 0)
            {

                try
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        // DataRow dr = dt.NewRow();


                        DataRow dr1 = h.Table_bill.NewRow();

                        // dr[0] = ds.Tables[0].Rows[i][0].ToString();

                        dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                        UserName = ds.Tables[0].Rows[i][0].ToString();
                        dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                        address = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                        dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                        name = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                        dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                        lotname = ds.Tables[0].Rows[i][26].ToString();

                        if (ds.Tables[0].Rows[i][4].ToString() == "1")
                        {
                            dr1[5] = "Reserved Parking";
                            parkingtype = "Reserved Parking";
                        }
                        if (ds.Tables[0].Rows[i][4].ToString() == "2")
                        {
                            dr1[5] = "Random Parking";
                            parkingtype = "Random Parking";
                        }

                        dr1[6] = ds.Tables[0].Rows[i][29].ToString();
                        plan = ds.Tables[0].Rows[i][29].ToString();

                        DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                        if (dtamount.Rows.Count > 0)
                        {

                            for (int j = 0; j < dtamount.Rows.Count; j++)
                            {


                                amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_OutStanding"].ToString());

                                DataRow dr2 = h.Amount_table.NewRow();
                                dr2[0] = dtamount.Rows[j]["Date Issued"].ToString();
                                string startdate = dtamount.Rows[j]["Date Issued"].ToString();
                                dr2[1] = dtamount.Rows[j]["DueDate"].ToString();
                                string duedate = dtamount.Rows[j]["DueDate"].ToString();
                                dr2[2] = dtamount.Rows[j]["Amt_OutStanding"].ToString();
                                string dueamount = dtamount.Rows[j]["Amt_OutStanding"].ToString();

                                table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                h.Amount_table.Rows.Add(dr2);
                            }
                        }
                        if (amountduetotal == amountduetotal1)
                        {
                        }
                        else
                        {
                            //   dr[1] = amountduetotal;
                            dr1[1] = amountduetotal;
                            totalamount = Convert.ToString(amountduetotal.ToString());
                            //dt.Rows.Add(dr);
                            h.Table_bill.Rows.Add(dr1);
                            amountduetotal = Convert.ToDecimal(0.00);
                        }


                    }


                    //  Session["datab"] = h;
                    //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);
                }
                catch
                {
                }

            }


            bool b = obj.mail(ds.Tables[0].Rows[0]["Email"].ToString(), @"<div>   <div class='clearfix'></div> <table style='  border:solid 1px #c1d5e5; padding:3px; border-collapse:collapse; width:1060px; height:300px; margin-top:15px; font-family:Arial; font-size:13px; margin:auto; 'width='960' border='0' cellpadding='10' height='200' >         <tr> <td colspan='4' style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'><img border='0' alt='Salesforce' src='images/ipasslogo.png' /></td></tr>            <tr>                <td                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'                     class='style6'>                                        </td>                <td colspan='2'                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'>                                        <span class='style2'>&nbsp;BILLING INFORMATION&nbsp;</span></td>                <td                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'>                                        &nbsp;</td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    <asp:Label ID='lbl_name' runat='server' Text='Label'></asp:Label>                </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                       &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                       User Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + UserName + " </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        &nbsp;&nbsp;&nbsp;                                        Address</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + address + "                </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                      &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                      Lot Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + lotname + "                </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                       &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                       Plan</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + plan + "                 </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                        Parking Type</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + parkingtype + "                 </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                     &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                    Total Due Amount </td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + totalamount + @"                 </td>            </tr>        </table>        </div><br />    <div>       " + table + "        </div>", "Checked");

        }
        catch
        {
        }
            
      
     
    }
  
}
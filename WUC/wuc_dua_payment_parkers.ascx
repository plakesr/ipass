﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_dua_payment_parkers.ascx.cs" Inherits="WUC_wuc_dua_payment_parkers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 647px;
    }
    .style2
    {
        width: 166px;
    }
    .style3
    {
        width: 100px;
    }
          .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
    .button
    {}
</style>
<script type = "text/javascript">
    function checkAll(objRef) {
        var GridView = objRef.parentNode.parentNode.parentNode;
        var inputList = GridView.getElementsByTagName("input");
        for (var i = 0; i < inputList.length; i++) {
            //Get the Cell To find out ColumnIndex
            var row = inputList[i].parentNode.parentNode;
            if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                if (objRef.checked) {
                    //If the header checkbox is checked
                    //check all checkboxes
                    //and highlight all rows
                    row.style.backgroundColor = "#B3C6F6";
                    inputList[i].checked = true;
                }
                else {
                    //If the header checkbox is checked
                    //uncheck all checkboxes
                    //and change rowcolor back to original
                    if (row.rowIndex % 2 == 0) {
                        //Alternating Row Color
                       row.style.backgroundColor = "white";
                    }
                    else {
                        row.style.backgroundColor = "white";
                    }
                    inputList[i].checked = false;
                }
            }
        }
    }
</script>
<script type = "text/javascript">
    function Check_Click(objRef) {
        //Get the Row based on checkbox
        var row = objRef.parentNode.parentNode;
        if (objRef.checked) {
            //If checked change color to Aqua
            row.style.backgroundColor = "#B3C6F6";
        }
        else {
            //If not checked change back to original color
            if (row.rowIndex % 2 == 0) {
                //Alternating Row Color
                row.style.backgroundColor = "white";
            }
            else {
                row.style.backgroundColor = "white";
            }
        }

        //Get the reference of GridView
        var GridView = row.parentNode;

        //Get all input elements in Gridview
        var inputList = GridView.getElementsByTagName("input");

        for (var i = 0; i < inputList.length; i++) {
            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];

            //Based on all or none checkboxes
            //are checked check/uncheck Header Checkbox
            var checked = true;
            if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                if (!inputList[i].checked) {
                    checked = false;
                    break;
                }
            }
        }
        headerCheckBox.checked = checked;

    }
</script>
<link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<div>
    <table class="style1">
        <tr>
            <td class="style2">
                Search Individual Parker</td>
            <td class="style3">
                <asp:TextBox ID="txtbox_search" runat="server" CssClass="twitterStyleTextbox"  Height="32px" Width="220px" 
                    style="margin-left: 0px"></asp:TextBox>
                 <cc1:TextBoxWatermarkExtender ID="txtbox_vehicleType_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_search" 
                        WatermarkCssClass="watermark" WatermarkText="Enter Name/Mobile No./E-mail Id">
                    </cc1:TextBoxWatermarkExtender>
            </td>
            <td class="style3">
            <asp:Button ID="btn_search" runat="server" onclick="btn_search_Click" 
                Text="Search"  CssClass="button"/>
            </td>
        </tr>
        <tr>
        <td class="style2"></td>
        <td class="style3">
            &nbsp;</td>
        <td class="style3">
            &nbsp;</td>
        </tr>
    </table>
</div>
<asp:Repeater ID="Repeater1" runat="server"
    onitemcommand="Repeater1_ItemCommand">

            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                       <asp:CheckBox ID="CheckBox1" runat="server" Text="Select All"  onclick = "checkAll(this);" />
                    </th>
                    <th>Lot Name</th>
                <th>User Name
                </th>
                 <th>Due Amount
                </th>
                </th>
                     <th>E-mail Id
                </th>
                 <th>Mobile_No
                </th> 
                 <th>Detail
                
                </tr>

        </HeaderTemplate>
<ItemTemplate>

   <tr>
            <td> <asp:CheckBox ID="CheckBox2" runat="server" onclick = "Check_Click(this)" />
            </td>
            <td><%#Eval("LotName")%></td>
            <td>
             <%#Eval("Username")%>
             <asp:Label ID="username" runat="server" Text='<%# Eval("Username") %>' 
                                        Visible="False" />
            </td>
              <td>
               $<%#Eval("DueAmount")%>
               <asp:Label ID="dueamount" runat="server" Text='<%# Eval("DueAmount") %>' 
                                        Visible="False" />
              </td>
               <td>
               <%#Eval("E-mail")%>
               <asp:Label ID="Label1" runat="server" Text='<%# Eval("E-mail") %>' 
                                        Visible="False" />
              </td>
              <td>
               <%#Eval("Mobile_no")%>
               <asp:Label ID="Label2" runat="server" Text='<%# Eval("Mobile_no") %>' 
                                        Visible="False" />
              </td>
              <td>
                  <asp:Button ID="Details" CommandName="detail" runat="server" Text="Detail" CssClass="button"  />
              </td>
       
    </tr>

    
   

</ItemTemplate>
 <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
<asp:Repeater ID="Repeater2" runat="server" 
    onitemcommand="Repeater2_ItemCommand" Visible="False">
  <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                       <asp:CheckBox ID="CheckBox7" runat="server" Text="Select All"  onclick = "checkAll(this);" />
                    </th>
                    <th>
                    Lot Name</th>
                <th>User Name
                </th>
                 <th>Due Amount
                </th>
                  <th>E-mail Id
                </th>
                 <th>Mobile_No
                </th>
                 <th>Detail
                </th>
               
                     
                </tr>

        </HeaderTemplate>
<ItemTemplate>

   <tr>
            <td> <asp:CheckBox ID="CheckBox8" runat="server" onclick = "Check_Click(this)" />
            </td>
            <td>
            <%#Eval("LotName") %>
            </td>
            <td>
             <%#Eval("Username")%>
             <asp:Label ID="username1" runat="server" Text='<%# Eval("Username") %>' 
                                        Visible="False" />
            </td>
              <td>
               $<%#Eval("DueAmount")%>
               <asp:Label ID="dueamount1" runat="server" Text='<%# Eval("DueAmount") %>' 
                                        Visible="False" />
              </td>
              <td>
               <%#Eval("E-mail")%>
               <asp:Label ID="Label1" runat="server" Text='<%# Eval("E-mail") %>' 
                                        Visible="False" />
              </td>
              <td>
               <%#Eval("Mobile_no")%>
               <asp:Label ID="Label2" runat="server" Text='<%# Eval("Mobile_no") %>' 
                                        Visible="False" />
              </td>
              <td>
                  <asp:Button ID="details1" CommandName="detail1" runat="server" Text="Detail"  CssClass="button" />
              </td>
       
    </tr>

   

    
   
   

</ItemTemplate>
 <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>


<%--<asp:Button ID="R2_btn_Send_Mail" runat="server" Text="Send Mail(R2_send)" 
    onclick="btn_Send_Mail_Click" CssClass="button" Visible="False" />--%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

<table class="style1">

<tr>
<td></td>
<td align=right><asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" 
    Font-Underline="false" ForeColor="Blue" Visible="False" CssClass="button">View All Records</asp:LinkButton>
</td>
<td><asp:Button ID="search_sendmail" runat="server" Text="Sendmail(Search)" 
    Visible="False" CssClass="button" onclick="search_sendmail_Click" />
</td>
</tr>
</table>
<%--<asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Print Invoice" CssClass="button" />--%>

<%--<asp:Button ID="print" runat="server" onclick="Button2_Click" Text="Print" />--%>


<%--<asp:Button ID="btn_r1sendmail" runat="server" onclick="Button3_Click" 
    Text="Send mail(Btn_r1)" Visible="False" CssClass="button" />
--%>
    &nbsp;






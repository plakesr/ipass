﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;


public partial class WUC_wuc_dua_payment_parkers : System.Web.UI.UserControl
{

    decimal amountduetotal = Convert.ToDecimal(0.00);
    decimal amountduetotal1 = Convert.ToDecimal(0.00);
    DataTable dueparker = new DataTable();
  
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            //btn_r1sendmail.Visible = true;
            //R2_btn_Send_Mail.Visible = true;
            search_sendmail.Visible = true;
             DataTable dtAll = new DataTable();
            dtAll.Columns.Add("Username", typeof(string));
            dtAll.Columns.Add("DueAmount", typeof(decimal));
            dtAll.Columns.Add("E-mail", typeof(string));
            dtAll.Columns.Add("Mobile_no", typeof(string));
            dtAll.Columns.Add("LotName", typeof(string));

            if (!IsPostBack == true)
            {
                if (Session["Type"].ToString() == "admin")
                {
                    LinkButton1.Visible = true;
                    DataSet ds = new clsInsert().fetchrec("select distinct top 5  billing. Acct_Num, p.UserName, lr.Amount ,lr.chargeby,p.Email,p.CellNo,lr.Lotid,lot.LotName   from tbl_ParkerRegis p, tbl_LotRateMaster lr,  TBL_Invoice as billing,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id   and lr.chargeby =3");
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        try
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                DataRow dr = dtAll.NewRow();
                                dr[0] = ds.Tables[0].Rows[i][0].ToString();
                                dr[2] = ds.Tables[0].Rows[i][4].ToString();
                                dr[3] = ds.Tables[0].Rows[i][5].ToString();
                                dr[4] = ds.Tables[0].Rows[i][7].ToString();

                                DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                                if (dtamount.Rows.Count > 0)
                                {
                                    for (int j = 0; j < dtamount.Rows.Count; j++)
                                    {
                                        amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());
                                    }
                                }
                                if (amountduetotal == amountduetotal1)
                                {
                                }
                                else
                                {
                                    dr[1] = amountduetotal;
                                    dtAll.Rows.Add(dr);
                                    amountduetotal = Convert.ToDecimal(0.00);
                                }


                            }

                        }
                        catch
                        {
                        }
                        if (dtAll.Rows.Count > 0)
                        {
                            Repeater1.Visible = true;
                            Repeater1.DataSource = dtAll;
                            Repeater1.DataBind();
                            //  Repeater2.Visible = try;
                        }
                    }
                }


                if (Session["Type"].ToString() == "operator")
                {
                    LinkButton1.Visible = false;
                    DataTable dtlotidforparker = new clsInsert().getlotsforPm(Session["Login"].ToString());
                    for (int i = 0; i < dtlotidforparker.Rows.Count; i++)
                    {
                         dueparker = new clsInsert().getdueparkerbylot(Convert.ToInt32(dtlotidforparker.Rows[i]["lotid"]));
                        if (dueparker.Rows.Count > 0)
                        {
                            try
                            {
                                for (int k = 0; k < dueparker.Rows.Count; k++)
                                {

                                    DataRow dr1 = dtAll.NewRow();
                                    dr1[0] = dueparker.Rows[k][0].ToString();
                                    dr1[2] = dueparker.Rows[k][5].ToString();
                                    dr1[3] = dueparker.Rows[k][6].ToString();
                                    DataTable dtamount = new clsInsert().dueamount(dueparker.Rows[k][0].ToString());
                                    if (dtamount.Rows.Count > 0)
                                    {
                                        for (int j = 0; j < dtamount.Rows.Count; j++)
                                        {
                                            amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Dueamount"].ToString());
                                        }
                                    }
                                    if (amountduetotal == amountduetotal1)
                                    {
                                    }
                                    else
                                    {
                                        dr1[1] = amountduetotal;
                                        dtAll.Rows.Add(dr1);
                                        amountduetotal = Convert.ToDecimal(0.00);
                                    }


                                }

                            }
                            catch
                            {
                            }
                            if (dtAll.Rows.Count > 0)
                            {
                                Repeater1.Visible = true;
                                Repeater1.DataSource = dtAll;
                                Repeater1.DataBind();
                                //  Repeater2.Visible = try;
                            }
                        }
                    }
                }
            }
        }

        catch
        {
        }
    }
    protected void chk_allchecked(object sender, EventArgs e)
    {



        //foreach (RepeaterItem ri in Repeater1.Items)
        //  {
        //     CheckBox ch = (CheckBox)ri.FindControl("CheckBox1");

        //     if (ch.Checked == true)
        //     {
        //         CheckBox ch3 = (CheckBox)ri.FindControl("CheckBox2"); ch3.Checked = true;
        //     }
        //    else
        //    {


        //      CheckBox ch3 = (CheckBox)ri.FindControl("CheckBox2"); ch3.Checked = false;

        //   }

        //    }

    }





    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            CheckBox ch = (CheckBox)e.Item.FindControl("CheckBox1");
            if (ch.Checked == true)
            {
                foreach (RepeaterItem ri in Repeater1.Items)
                {
                    CheckBox ch3 = (CheckBox)ri.FindControl("CheckBox2"); ch3.Checked = true;
                }
            }
            else
            {
                foreach (RepeaterItem ri in Repeater1.Items)
                {

                    CheckBox ch3 = (CheckBox)ri.FindControl("CheckBox2"); ch3.Checked = false;
                }
            }
            //  bool isChecked = cb.Checked;
        }
         int m = e.Item.ItemIndex;
         if (e.CommandName == "detail")
         {
             DataSet_Bill detail = new DataSet_Bill();
             Label l = Repeater1.Items[m].FindControl("username") as Label;

             DataSet ds = new clsInsert().fetchrec(" select distinct  billing. Acct_Num, p.* , lr.Amount,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby    as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice    as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and    billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id and    p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid  and lr.chargeby =3 and p.UserName='" + l.Text + "'");
             // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
             string table = "";
             string parkingtype = "";
             string UserName = "";

             if (ds.Tables[0].Rows.Count > 0)
             {

                 try
                 {
                     for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                     {

                         // DataRow dr = dt.NewRow();


                         DataRow dr1 = detail.Table_bill.NewRow();







                         // dr[0] = ds.Tables[0].Rows[i][0].ToString();

                         dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                         UserName = ds.Tables[0].Rows[i][0].ToString();
                         dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                         address = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                         dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                         name = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                         dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                         lotname = ds.Tables[0].Rows[i][26].ToString();

                         if (ds.Tables[0].Rows[i][4].ToString() == "1")
                         {
                             dr1[5] = "Reserved Parking";
                             parkingtype = "Reserved Parking";
                         }
                         if (ds.Tables[0].Rows[i][4].ToString() == "2")
                         {
                             dr1[5] = "Random Parking";
                             parkingtype = "Random Parking";
                         }

                         dr1[6] = ds.Tables[0].Rows[i][29].ToString();
                         plan = ds.Tables[0].Rows[i][29].ToString();

                         DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                         if (dtamount.Rows.Count > 0)
                         {

                             for (int j = 0; j < dtamount.Rows.Count; j++)
                             {


                                 amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());
//InvoiceNumber,Issued Date,Due Date,Sub-Total,Tax,Invoiced Amount,Amount Outstanding
                                 DataRow dr2 = detail.BillInformation.NewRow();
                                 dr2[0] = dtamount.Rows[j][1].ToString();
                                 dr2[1] = dtamount.Rows[j]["Date Issued"].ToString();

                                 string startdate = dtamount.Rows[j]["Date Issued"].ToString();
                                 dr2[2] = dtamount.Rows[j]["DueDate"].ToString();
                                 string duedate = dtamount.Rows[j]["DueDate"].ToString();

                                 dr2[3] = dtamount.Rows[j][4].ToString();
                                
                                 dr2[4] = dtamount.Rows[j][5].ToString();
                                 dr2[5] = dtamount.Rows[j][6].ToString();
                                 

                                 dr2[6] = dtamount.Rows[j]["Amt_Outstanding"].ToString();
                                 string dueamount = dtamount.Rows[j]["Amt_Outstanding"].ToString();

                                 table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                 detail.BillInformation.Rows.Add(dr2);
                             }
                         }
                         if (amountduetotal == amountduetotal1)
                         {
                         }
                         else
                         {
                             //   dr[1] = amountduetotal;
                             dr1[1] = amountduetotal;
                             totalamount = Convert.ToString(amountduetotal.ToString());
                             //dt.Rows.Add(dr);
                             detail.Table_bill.Rows.Add(dr1);
                             amountduetotal = Convert.ToDecimal(0.00);
                         }


                     }


                     Session["detailofdue"] = detail;
                     Response.Redirect("Admin_DetailsOfDueAmount.aspx");
                     //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);
                 }
                 catch
                 {
                 }

             }
               
            

            
          
         }

    }
    clsData obj = new clsData();
  
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int m = e.Item.ItemIndex;
        if (e.CommandName == "detail1")
        {
            DataSet_Bill detail = new DataSet_Bill();
            Label l = Repeater2.Items[m].FindControl("username1") as Label;

            DataSet ds = new clsInsert().fetchrec(" select distinct  billing. Acct_Num, p.* , lr.Amount,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby    as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice    as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and    billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id and    p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid  and lr.chargeby =3 and p.UserName='" + l.Text + "'");

            // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
            string table = "";
            string parkingtype = "";
            string UserName = "";

            if (ds.Tables[0].Rows.Count > 0)
            {

                try
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        // DataRow dr = dt.NewRow();


                        DataRow dr1 = detail.Table_bill.NewRow();







                        // dr[0] = ds.Tables[0].Rows[i][0].ToString();

                        dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                        UserName = ds.Tables[0].Rows[i][0].ToString();
                        dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                        address = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                        dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                        name = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                        dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                        lotname = ds.Tables[0].Rows[i][26].ToString();

                        if (ds.Tables[0].Rows[i][4].ToString() == "1")
                        {
                            dr1[5] = "Reserved Parking";
                            parkingtype = "Reserved Parking";
                        }
                        if (ds.Tables[0].Rows[i][4].ToString() == "2")
                        {
                            dr1[5] = "Random Parking";
                            parkingtype = "Random Parking";
                        }

                        dr1[6] = ds.Tables[0].Rows[i][29].ToString();
                        plan = ds.Tables[0].Rows[i][29].ToString();

                        DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                        if (dtamount.Rows.Count > 0)
                        {

                            for (int j = 0; j < dtamount.Rows.Count; j++)
                            {


                                amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());

                                DataRow dr2 = detail.BillInformation.NewRow();
                                dr2[0] = dtamount.Rows[j][1].ToString();
                                dr2[1] = dtamount.Rows[j]["Date Issued"].ToString();

                                string startdate = dtamount.Rows[j]["Date Issued"].ToString();
                                dr2[2] = dtamount.Rows[j]["DueDate"].ToString();
                                string duedate = dtamount.Rows[j]["DueDate"].ToString();

                                dr2[3] = dtamount.Rows[j][4].ToString();

                                dr2[4] = dtamount.Rows[j][5].ToString();
                                dr2[5] = dtamount.Rows[j][6].ToString();


                                dr2[6] = dtamount.Rows[j]["Amt_Outstanding"].ToString();
                                string dueamount = dtamount.Rows[j]["Amt_Outstanding"].ToString();

                                table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                detail.BillInformation.Rows.Add(dr2);
                            }
                        }
                        if (amountduetotal == amountduetotal1)
                        {
                        }
                        else
                        {
                            //   dr[1] = amountduetotal;
                            dr1[1] = amountduetotal;
                            totalamount = Convert.ToString(amountduetotal.ToString());
                            //dt.Rows.Add(dr);
                            detail.Table_bill.Rows.Add(dr1);
                            amountduetotal = Convert.ToDecimal(0.00);
                        }


                    }


                    Session["detailofdue"] = detail;
                    Response.Redirect("Admin_DetailsOfDueAmount.aspx");
                    //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);
                }
                catch
                {
                }

            }





        }

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        //btn_r1sendmail.Visible = true;
        //R2_btn_Send_Mail.Visible = true ;
        search_sendmail.Visible = true;
        txtbox_search.Text = "";
        DataTable dt = new DataTable();
        dt.Columns.Add("Username", typeof(string));
        dt.Columns.Add("DueAmount", typeof(decimal));
        dt.Columns.Add("E-mail", typeof(string));
        dt.Columns.Add("Mobile_no", typeof(string));
        dt.Columns.Add("LotName", typeof(string));


        DataSet ds = new clsInsert().fetchrec("select distinct   billing. Acct_Num, p.UserName, lr.Amount ,lr.chargeby,p.Email,p.CellNo,lr.Lotid,lot.LotName  from tbl_ParkerRegis p, tbl_LotRateMaster lr,  TBL_Invoice as billing,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Acct_Num=p.UserName  and p.LotId=lot.Lot_id  and lr.chargeby =3");
       
        //DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.UserName , lr.Amount ,lr.chargeby  from tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName   and lr.chargeby =3 ");
        if (ds.Tables[0].Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    DataRow dr = dt.NewRow();
                    dr[0] = ds.Tables[0].Rows[i][0].ToString();
                    dr[2] = ds.Tables[0].Rows[i][4].ToString();

                    dr[3] = ds.Tables[0].Rows[i][5].ToString();
                    dr[4] = ds.Tables[0].Rows[i][7].ToString();

                    DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                    if (dtamount.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtamount.Rows.Count; j++)
                        {
                            amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());
                        }
                    }
                    if (amountduetotal == amountduetotal1)
                    {
                    }
                    else
                    {
                        dr[1] = amountduetotal;
                        dt.Rows.Add(dr);
                        amountduetotal = Convert.ToDecimal(0.00);
                    }


                }

            }
            catch
            {
            }
            if (dt.Rows.Count > 0)
            {
                Repeater2.Visible = true;
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
                Repeater1.Visible = false;

            }
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Username", typeof(string));
        dt.Columns.Add("DueAmount", typeof(decimal));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("Address", typeof(string));
        dt.Columns.Add("LotName", typeof(string));
        dt.Columns.Add("ParkingType", typeof(string));
        dt.Columns.Add("Plan", typeof(string));
        dt.Columns.Add("", typeof(string));




        DataSet_Bill h = new DataSet_Bill();
        h.Table_bill.Clear();


        DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
        if (ds.Tables[0].Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    DataRow dr = dt.NewRow();


                    DataRow dr1 = h.Table_bill.NewRow();







                    dr[0] = ds.Tables[0].Rows[i][0].ToString();

                    dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                    dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                    dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                    dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                    if (ds.Tables[0].Rows[i][4].ToString() == "1")
                    {
                        dr1[5] = "Reserved Parking";
                    }
                    if (ds.Tables[0].Rows[i][4].ToString() == "2")
                    {
                        dr1[5] = "Random Parking";
                    }

                    dr1[6] = ds.Tables[0].Rows[i][29].ToString();


                    DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                    if (dtamount.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtamount.Rows.Count; j++)
                        {
                            amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Dueamount"].ToString());
                        }
                    }
                    if (amountduetotal == amountduetotal1)
                    {
                    }
                    else
                    {
                        dr[1] = amountduetotal;
                        dr1[1] = amountduetotal;
                        dt.Rows.Add(dr);
                        h.Table_bill.Rows.Add(dr1);
                        amountduetotal = Convert.ToDecimal(0.00);
                    }


                }
                Session["datab"] = h;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);

            }
            catch
            {
            }
            if (dt.Rows.Count > 0)
            {


            }
        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        //btn_r1sendmail.Visible = false;
        //R2_btn_Send_Mail.Visible = false;
        search_sendmail.Visible = true;
        DataTable dt2 = new DataTable();
        dt2.Columns.Add("Username", typeof(string));
        dt2.Columns.Add("DueAmount", typeof(decimal));
        dt2.Columns.Add("E-mail", typeof(string));
        dt2.Columns.Add("Mobile_no", typeof(string));
        dt2.Columns.Add("LotName", typeof(string));


        try
        {

            if (Session["Type"].ToString() == "admin")
            {

                DataTable dt11 = new clsInsert().getduepaymentparker(txtbox_search.Text);

                if (dt11.Rows.Count > 0)
                {

                    try
                    {
                        for (int i = 0; i < dt11.Rows.Count; i++)
                        {

                            DataRow dr = dt2.NewRow();
                            dr[0] = dt11.Rows[i][0].ToString();
                            dr[2] = dt11.Rows[i][4].ToString();

                            dr[3] = dt11.Rows[i][5].ToString();
                            dr[4] = dt11.Rows[i][6].ToString();

                            DataTable dtamount = new clsInsert().dueamount(dt11.Rows[i][0].ToString());
                            if (dtamount.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtamount.Rows.Count; j++)
                                {
                                    amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());
                                }
                            }
                            if (amountduetotal == amountduetotal1)
                            {
                            }
                            else
                            {
                                dr[1] = amountduetotal;
                                dt2.Rows.Add(dr);
                                amountduetotal = Convert.ToDecimal(0.00);
                            }

                        }

                    }
                    catch
                    {
                    }







                    Repeater2.Visible = true;
                    Repeater2.DataSource = dt2;
                    Repeater2.DataBind();
                    Repeater1.Visible = false;
                    //rpt_searchparker.Visible = true;

                    //Result.Visible = true;
                    //Result.Text = count.ToString() + " " + "Records Found";

                    //rpt_searchparker.DataSource = dt;
                    //rpt_searchparker.DataBind();

                }
                else
                {

                    //Result.Visible = false;
                    //rpt_searchparker.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
                    Repeater2.Visible = false;

                }
            }
            if (Session["Type"].ToString() == "operator")
            {


                  DataTable dtlotidforparker = new clsInsert().getlotsforPm(Session["Login"].ToString());
                    for (int i = 0; i < dtlotidforparker.Rows.Count; i++)
                    {
                        dueparker = new clsInsert().getduepaymentparkerbyname(Convert.ToInt32(dtlotidforparker.Rows[i]["lotid"]));
                        if (dueparker.Rows.Count > 0)
                        {

                            for (int n = 0; n < dueparker.Rows.Count; n++)
                            {
                                DataRow dr6 = dt2.NewRow();
                                DataTable search = new clsInsert().getduepaymentparker(txtbox_search.Text, Convert.ToInt32(dueparker.Rows[n]["lotid"]));
                                for(int a=0;a<search.Rows.Count;a++)
                                {
                                    dr6[0]=search.Rows[a][1].ToString();
                                    dr6[1] = search.Rows[a][2].ToString();
                                    dr6[2] = search.Rows[a][4].ToString();

                                    dr6[3] = search.Rows[a][5].ToString();

                                    dt2.Rows.Add(dr6);

                                }

                            }

                        }
                       
                    }
                    if (dt2.Rows.Count > 0)
                    {
                        RemoveDuplicateRows(dt2, "Username");
                        Repeater2.Visible = true;
                        Repeater2.DataSource = dt2;
                        Repeater2.DataBind();
                        Repeater1.Visible = false;

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Records Found.')", true);
                        Repeater2.Visible = false;
                        Repeater1.Visible = false;
                    }


                   


                }

            }





        

        catch
        {
        }
    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("Username", typeof(string));
        dt.Columns.Add("DueAmount", typeof(decimal));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("Address", typeof(string));
        dt.Columns.Add("LotName", typeof(string));
        dt.Columns.Add("ParkingType", typeof(string));
        dt.Columns.Add("Plan", typeof(string));
        dt.Columns.Add("", typeof(string));




        DataSet_Bill h = new DataSet_Bill();
        h.Table_bill.Clear();
        h.Amount_table.Clear();

        DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 and p.UserName='gopal'");
        // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");

        if (ds.Tables[0].Rows.Count > 0)
        {

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    DataRow dr = dt.NewRow();


                    DataRow dr1 = h.Table_bill.NewRow();







                    dr[0] = ds.Tables[0].Rows[i][0].ToString();

                    dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                    dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                    dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                    dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                    if (ds.Tables[0].Rows[i][4].ToString() == "1")
                    {
                        dr1[5] = "Reserved Parking";
                    }
                    if (ds.Tables[0].Rows[i][4].ToString() == "2")
                    {
                        dr1[5] = "Random Parking";
                    }

                    dr1[6] = ds.Tables[0].Rows[i][29].ToString();


                    DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                    if (dtamount.Rows.Count > 0)
                    {

                        for (int j = 0; j < dtamount.Rows.Count; j++)
                        {


                            amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Dueamount"].ToString());

                            DataRow dr2 = h.Amount_table.NewRow();
                            dr2[0] = dtamount.Rows[j]["StartDate"].ToString();

                            dr2[1] = dtamount.Rows[j]["DueDate"].ToString();
                            dr2[2] = dtamount.Rows[j]["Dueamount"].ToString();

                            h.Amount_table.Rows.Add(dr2);
                        }
                    }
                    if (amountduetotal == amountduetotal1)
                    {
                    }
                    else
                    {
                        dr[1] = amountduetotal;
                        dr1[1] = amountduetotal;
                        dt.Rows.Add(dr);
                        h.Table_bill.Rows.Add(dr1);
                        amountduetotal = Convert.ToDecimal(0.00);
                    }


                }
                Session["datab"] = h;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);

            }
            catch
            {
            }
            if (dt.Rows.Count > 0)
            {


            }
        }

    }
    string address, name, lotname, plan, totalamount = "";
    string chktrue = "";
   
    int chk = 0;

    protected void search_sendmail_Click(object sender, EventArgs e)
    {
        if (Repeater2.Visible == true)
        {
            foreach (RepeaterItem ri in Repeater2.Items)
            {
                CheckBox ch3 = (CheckBox)ri.FindControl("CheckBox8");
                if (ch3.Checked == true)
                {
                    chk = chk + 1;
                    Label l1 = (Label)ri.FindControl("username1");
                    Label l2 = (Label)ri.FindControl("dueamount1");
                    chktrue = "true";
                    DataSet_Bill h = new DataSet_Bill();
                    h.Table_bill.Clear();
                    h.Amount_table.Clear();

                    DataSet ds = new clsInsert().fetchrec("select distinct  billing. Acct_Num, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 and p.UserName='" + l1.Text + "'");
                    // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
                    string table = "";
                    string parkingtype = "";
                    string UserName = "";

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        try
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                // DataRow dr = dt.NewRow();


                                DataRow dr1 = h.Table_bill.NewRow();







                                // dr[0] = ds.Tables[0].Rows[i][0].ToString();

                                dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                                UserName = ds.Tables[0].Rows[i][0].ToString();
                                dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                                address = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                                dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                                name = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                                dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                                lotname = ds.Tables[0].Rows[i][26].ToString();

                                if (ds.Tables[0].Rows[i][4].ToString() == "1")
                                {
                                    dr1[5] = "Reserved Parking";
                                    parkingtype = "Reserved Parking";
                                }
                                if (ds.Tables[0].Rows[i][4].ToString() == "2")
                                {
                                    dr1[5] = "Random Parking";
                                    parkingtype = "Random Parking";
                                }

                                dr1[6] = ds.Tables[0].Rows[i][29].ToString();
                                plan = ds.Tables[0].Rows[i][29].ToString();

                                DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                                if (dtamount.Rows.Count > 0)
                                {

                                    for (int j = 0; j < dtamount.Rows.Count; j++)
                                    {


                                        amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_OutStanding"].ToString());

                                        DataRow dr2 = h.Amount_table.NewRow();
                                        dr2[0] = dtamount.Rows[j]["Date Issued"].ToString();
                                        string startdate = dtamount.Rows[j]["Date Issued"].ToString();
                                        dr2[1] = dtamount.Rows[j]["DueDate"].ToString();
                                        string duedate = dtamount.Rows[j]["DueDate"].ToString();
                                        dr2[2] = dtamount.Rows[j]["Amt_OutStanding"].ToString();
                                        string dueamount = dtamount.Rows[j]["Amt_OutStanding"].ToString();

                                        table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                        h.Amount_table.Rows.Add(dr2);
                                    }
                                }
                                if (amountduetotal == amountduetotal1)
                                {
                                }
                                else
                                {
                                    //   dr[1] = amountduetotal;
                                    dr1[1] = amountduetotal;
                                    totalamount = Convert.ToString(amountduetotal.ToString());
                                    //dt.Rows.Add(dr);
                                    h.Table_bill.Rows.Add(dr1);
                                    amountduetotal = Convert.ToDecimal(0.00);
                                }


                            }


                            //  Session["datab"] = h;
                            //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);
                        }
                        catch
                        {
                        }

                    }






                    //if (dt.Rows.Count > 0)
                    //{


                    //}

                    bool b = obj.mail(ds.Tables[0].Rows[0]["Email"].ToString(), @"<div>   <div class='clearfix'></div> <table style='  border:solid 1px #c1d5e5; padding:3px; border-collapse:collapse; width:1060px; height:300px; margin-top:15px; font-family:Arial; font-size:13px; margin:auto; 'width='960' border='0' cellpadding='10' height='200' >         <tr> <td colspan='4' style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'><img border='0' alt='Salesforce' src='images/ipasslogo.png' /></td></tr>            <tr>                <td                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'                     class='style6'>                                        </td>                <td colspan='2'                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'>                                        <span class='style2'>&nbsp;BILLING INFORMATION&nbsp;</span></td>                <td                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'>                                        &nbsp;</td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    <asp:Label ID='lbl_name' runat='server' Text='Label'></asp:Label>                </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                       &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                       User Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + UserName + " </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        &nbsp;&nbsp;&nbsp;                                        Address</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + address + "                </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                      &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                      Lot Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + lotname + "                </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                       &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                       Plan</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + plan + "                 </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                        Parking Type</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + parkingtype + "                 </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                     &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                    Total Due Amount </td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + totalamount + @"                 </td>            </tr>        </table>        </div><br />    <div>       " + table + "        </div>", "Checked");

                }

        
            }

        }

        if (Repeater1.Visible == true)
        {
            foreach (RepeaterItem ri in Repeater1.Items)
            {
                CheckBox ch3 = (CheckBox)ri.FindControl("CheckBox2");
                if (ch3.Checked == true)
                {
                    chk = chk + 1;
                    Label l1 = (Label)ri.FindControl("username");
                    Label l2 = (Label)ri.FindControl("dueamount");
                    chktrue = "true";
                    DataSet_Bill h = new DataSet_Bill();
                    h.Table_bill.Clear();
                    h.Amount_table.Clear();

                    DataSet ds = new clsInsert().fetchrec("select distinct  billing. Acct_Num, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,TBL_Invoice as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Acct_Num=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 and p.UserName='" + l1.Text + "'");
                    // DataSet ds = new clsInsert().fetchrec("select distinct  billing. Parker_UserName, p.* , lr.Amount ,lr.chargeby,lot.Lot_id,lot.LotName,lotrate.LotRateid,lotrate.chargeby ,chargename.chargebyname from tbl_LotRateMaster as lotrate, tbl_Chargeby as chargename,tbl_ParkerRegis p, tbl_LotRateMaster lr,tbl_ParkerBilling as billing ,tbl_LotMaster as lot where lr.LotRateid =p.PlanRateid and billing.Parker_UserName=p.UserName and p.LotId=lot.Lot_id and p.PlanRateid=lotrate.LotRateid and lotrate.chargeby=chargename.chargebyid and lr.chargeby =3 ");
                    string table = "";
                    string parkingtype = "";
                    string UserName = "";

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        try
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                // DataRow dr = dt.NewRow();


                                DataRow dr1 = h.Table_bill.NewRow();







                                // dr[0] = ds.Tables[0].Rows[i][0].ToString();

                                dr1[0] = ds.Tables[0].Rows[i][0].ToString();
                                UserName = ds.Tables[0].Rows[i][0].ToString();
                                dr1[2] = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                                address = ds.Tables[0].Rows[i][9].ToString() + "," + ds.Tables[0].Rows[i][10].ToString() + "," + ds.Tables[0].Rows[i][14].ToString() + "," + ds.Tables[0].Rows[i][13].ToString() + "," + ds.Tables[0].Rows[i][12].ToString();
                                dr1[3] = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                                name = ds.Tables[0].Rows[i][7].ToString() + "  " + ds.Tables[0].Rows[i][8].ToString();
                                dr1[4] = ds.Tables[0].Rows[i][26].ToString();
                                lotname = ds.Tables[0].Rows[i][26].ToString();

                                if (ds.Tables[0].Rows[i][4].ToString() == "1")
                                {
                                    dr1[5] = "Reserved Parking";
                                    parkingtype = "Reserved Parking";
                                }
                                if (ds.Tables[0].Rows[i][4].ToString() == "2")
                                {
                                    dr1[5] = "Random Parking";
                                    parkingtype = "Random Parking";
                                }

                                dr1[6] = ds.Tables[0].Rows[i][29].ToString();
                                plan = ds.Tables[0].Rows[i][29].ToString();

                                DataTable dtamount = new clsInsert().dueamount(ds.Tables[0].Rows[i][0].ToString());
                                if (dtamount.Rows.Count > 0)
                                {

                                    for (int j = 0; j < dtamount.Rows.Count; j++)
                                    {


                                        amountduetotal = amountduetotal + Convert.ToDecimal(dtamount.Rows[j]["Amt_Outstanding"].ToString());

                                        DataRow dr2 = h.Amount_table.NewRow();
                                        dr2[0] = dtamount.Rows[j]["Date Issued"].ToString();
                                        string startdate = dtamount.Rows[j]["Date Issued"].ToString();
                                        dr2[1] = dtamount.Rows[j]["DueDate"].ToString();
                                        string duedate = dtamount.Rows[j]["DueDate"].ToString();
                                        dr2[2] = dtamount.Rows[j]["Amt_Outstanding"].ToString();
                                        string dueamount = dtamount.Rows[j]["Amt_Outstanding"].ToString();

                                        table = table + "<table><tr> <td>" + startdate + "</td>  <td>  " + duedate + "</td>   <td> " + dueamount + "</td>        </tr>    </table>";


                                        h.Amount_table.Rows.Add(dr2);
                                    }
                                }
                                if (amountduetotal == amountduetotal1)
                                {
                                }
                                else
                                {
                                    //   dr[1] = amountduetotal;
                                    dr1[1] = amountduetotal;
                                    totalamount = Convert.ToString(amountduetotal.ToString());
                                    //dt.Rows.Add(dr);
                                    h.Table_bill.Rows.Add(dr1);
                                    amountduetotal = Convert.ToDecimal(0.00);
                                }


                            }


                            //  Session["datab"] = h;
                            //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('reportofbilling.aspx','New Windows','height=380, width=300,location=no');", true);
                        }
                        catch
                        {
                        }

                    }






                    //if (dt.Rows.Count > 0)
                    //{


                    //}

                    bool b = obj.mail(ds.Tables[0].Rows[0]["Email"].ToString(), @"<div>   <div class='clearfix'></div> <table style='  border:solid 1px #c1d5e5; padding:3px; border-collapse:collapse; width:1060px; height:300px; margin-top:15px; font-family:Arial; font-size:13px; margin:auto; 'width='960' border='0' cellpadding='10' height='200' >         <tr> <td colspan='4' style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'><img border='0' alt='Salesforce' src='images/ipasslogo.png' /></td></tr>            <tr>                <td                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'                     class='style6'>                                        </td>                <td colspan='2'                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'>                                        <span class='style2'>&nbsp;BILLING INFORMATION&nbsp;</span></td>                <td                     style='font-weight: 700; color: #3333FF; background-color: #FFFFFF;'>                                        &nbsp;</td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    <asp:Label ID='lbl_name' runat='server' Text='Label'></asp:Label>                </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                       &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                       User Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + UserName + " </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        &nbsp;&nbsp;&nbsp;                                        Address</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + address + "                </td>            </tr>            <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                      &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                      Lot Name</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + lotname + "                </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                       &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                       Plan</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                   " + plan + "                 </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                        &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                        Parking Type</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + parkingtype + "                 </td>            </tr>             <tr>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;'>                                     &nbsp;</td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' class='style8'>                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                    Total Due Amount </td>                <td style=' background:#eaf3fa; border-right:solid 1px #c1d5e5;' colspan='2'>                    " + totalamount + @"                 </td>            </tr>        </table>        </div><br />    <div>       " + table + "        </div>", "Checked");

                }

               
            }
        }
        if (chk > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Invoice Emailed Successfully')", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Mark The CheckBox First.')", true);
        }
    }
    
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_editTerms.ascx.cs" Inherits="WUC_wuc_editTerms" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 147px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Terms</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="style2">
                Term Name&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_termname" runat="server" ValidationGroup="b"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_termname" runat="server" 
                    ControlToValidate="txtbox_termname" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="b"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Description&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_description" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Days</td>
            <td>
                <asp:TextBox ID="txtbox_days" runat="server" ValidationGroup="b" Width="84px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_days" runat="server" 
                    ControlToValidate="txtbox_days" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="b"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;&nbsp; &nbsp;</td>
            <td>
                <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="b" />
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
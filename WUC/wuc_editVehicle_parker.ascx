﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_editVehicle_parker.ascx.cs" Inherits="WUC_wuc_editVehicle_parker" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 148px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Add/Edit Vehicle</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="style2">
                Make&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_make" runat="server" ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_make" runat="server" 
                    ControlToValidate="ddl_make" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Model&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_model" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_model" runat="server" 
                    ControlToValidate="txtbox_model" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_year" runat="server" ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_year" runat="server" 
                    ControlToValidate="ddl_year" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Color&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_color" runat="server" ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_color" runat="server" 
                    ControlToValidate="ddl_color" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                License Plate:</td>
            <td>
                <asp:TextBox ID="txtbox_license" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_license" runat="server" 
                    ControlToValidate="txtbox_license" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="a" 
                    onclick="btn_update_Click" />
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
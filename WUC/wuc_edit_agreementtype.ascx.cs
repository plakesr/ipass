﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_agreementtype : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack == true)
        {
            try
            {

                txtbox_AgreementDesc.Text = Session["AgreementTypeDesc"].ToString();
                txtbox_Agreementname.Text = Session["AgreementTypeName"].ToString();
            }
            catch
            {
                Response.Redirect("Admin_AgreementType.aspx");
            }

        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            updateAgreement();

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            Response.Redirect("Admin_AgreementType.aspx");
            // clear();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Try again.')", true);
        }
    }

    public void updateAgreement()
    {
        try
        {
            hst.Clear();

            int ID = Convert.ToInt32(Session["AgreementTypeId"].ToString());
            hst.Add("action", "update");
            hst.Add("agreementid", ID);


           
            hst.Add("agreementtypename", txtbox_Agreementname.Text);
            hst.Add("agreementtypedesc", txtbox_AgreementDesc.Text);
            hst.Add("modifiedby", "lovey_modifier");
            hst.Add("operatorId", "lovey_operator");
            hst.Add("dateofchanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_AgreementType]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                Response.Redirect("Admin_AgreementType.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }
}
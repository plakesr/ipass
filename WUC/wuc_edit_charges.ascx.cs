﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_chargehour : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["id"] = l.Text;
        if (!IsPostBack == true)
        {
            try
            {

                txtbox_time.Text = Session["time_in_hours"].ToString();
            }
            catch
            {
                Response.Redirect("VehicleCharge.aspx");
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = "1";
            updatecharges();
            Response.Redirect("VehicleCharge.aspx?msg=" + msg);

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    public void updatecharges()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "UPDATE");
            int id = Convert.ToInt32(Session["id"].ToString());
            hst.Add("c_id",id);

            hst.Add("time_in_hours", txtbox_time.Text);
            hst.Add("modifiedby", "lovey_modifier");
            hst.Add("operator_id", "lovey_operator");
            hst.Add("dateofchanging", DateTime.Now);


            int result = objData.ExecuteNonQuery("[sp_vehicle_Charges]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                
             //   Response.Redirect("VehicleCharge.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }
}
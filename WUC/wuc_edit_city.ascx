﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_city.ascx.cs" Inherits="WUC_wuc_edit_city" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style2
    {
        width: 214px;
    }
           
.twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<table class="form-table">

    <tr>
        <td class="style2">
            Select Province</td>
        <td>
            <asp:DropDownList ID="ddl_state" runat="server" DataSourceID="SqlDataSource1" 
                DataTextField="ProvinceName" DataValueField="ProvinceId" 
                CssClass="twitterStyleTextbox" AppendDataBoundItems="true">
                      <asp:ListItem Selected = "True" Text = "------Select Province------" Value = "0"></asp:ListItem>
               
            </asp:DropDownList>
           
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                SelectCommand="SELECT * FROM [tbl_Province] order by ProvinceName">
            </asp:SqlDataSource>
            <asp:RequiredFieldValidator ID="rfv_selectcountry" runat="server" 
                ControlToValidate="ddl_state" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="gp_city"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="style2">
            City Name</td>
        <td>
            <asp:TextBox ID="txtbox_cityname" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_cityname" runat="server" 
                ControlToValidate="txtbox_cityname" ErrorMessage="*" 
                ForeColor="Red" ValidationGroup="gp_city"></asp:RequiredFieldValidator>
        </td>
    </tr>
 
    <tr>
        <td class="style2">
            City Abbreviation</td>
        <td>
            <asp:TextBox ID="txtbox_cityabbr" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_CityAbbr" runat="server" 
                ControlToValidate="txtbox_cityabbr" 
                ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="gp_city"></asp:RequiredFieldValidator>
        </td>
    </tr>
  
        <td>
            <asp:Button ID="btn_update" runat="server" onclick="Button1_Click" Text="Update" CssClass=button
                ValidationGroup="gp_city" Height="35px" Width="95px" />
        </td>
    </tr>
</table>
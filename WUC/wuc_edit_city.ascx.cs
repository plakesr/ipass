﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_city : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {

                
                txtbox_cityname.Text = Session["City_name"].ToString();
                txtbox_cityabbr.Text = Session["City_abbr"].ToString();
                
               ddl_state.SelectedValue= Session["state_id"].ToString();
              
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_CityMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_CityMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
               // Response.Redirect("Admin_CityMaster.aspx");
            }
        }  
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {

            updatecity();
        }
        catch
        {
        }
       
    }
    string msg;
    public void updatecity()
    {
        try
        {

            hst.Clear();
            int i;
            i = Convert.ToInt32(Session["City_id"].ToString());

            hst.Add("action", "update");
            hst.Add("ID", i);
            hst.Add("province_id", ddl_state.SelectedItem.Value);


          
            hst.Add("city_name", txtbox_cityname.Text);
           
            hst.Add("city_abbrvtion", txtbox_cityabbr.Text);
         
          
            int result = objData.ExecuteNonQuery("[sp_city]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {


                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CityMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CityMaster.aspx?msg=" + msg);

                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

            }

        }
        catch
        {

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

        }

    }
   
    }

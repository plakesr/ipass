﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_corporate_parker.ascx.cs" Inherits="WUC_wuc_edit_corporate_parker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<link href="stylesheet/multiple-form.css" rel="stylesheet" type="text/css" />
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"  >
    
    
    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel1"  CssClass="Initial"><HeaderTemplate>
        Contact Details
</HeaderTemplate>
    
<ContentTemplate>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <asp:Panel ID="Panel2" runat="server">
                <fieldset>
                    <legend>Corporation Information</legend>
                    <br />
                    <table class="form-table01" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                            <td valign="top" width="150">
                                Corporation&nbsp; Name
                            </td>
                            <td class="style41">
                                <asp:TextBox ID="txtBox_Companyname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the Company Name" Enabled="False"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Label ID="Label3" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150">
                                Campus Name</td>
                            <td class="style41">
                                <asp:TextBox ID="txtbox_campusname" runat="server"  
                    CssClass="twitterStyleTextbox" Enabled="False" ></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Lot Name</td>
                            <td>
                                <asp:TextBox ID="txtbox_lotname" runat="server"  CssClass="twitterStyleTextbox" 
                    Enabled="False" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150">
                                Activation Date
                            </td>
                            <td class="style41">
                                <asp:TextBox ID="txtbox_ActicationDate" runat="server" 
                      CssClass="disable_past_dates" Enabled="False"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtbox_ActicationDate_CalendarExtender" 
                            runat="server" Enabled="True" TargetControlID="txtbox_ActicationDate" >
                                </cc1:CalendarExtender>
                            </td>
                            <td width="50">
                            </td>
                            <td valign="top" width="150">
                                Contact&#39;s Name</td>
                            <td class="style39">
                                <asp:TextBox ID="txtbox_Firstname" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the First Name" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_lname" runat="server" 
                    ControlToValidate="txtbox_Firstname" ErrorMessage="Enter Name" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic" CssClass="validation"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style34">
                                Designation</td>
                            <td class="style41">
                                <asp:TextBox ID="txtbox_designation" runat="server"></asp:TextBox>
                            </td>
                            <td width="50">
                            </td>
                            <td class="style39">
                                Corporation Address
                            </td>
                            <td class="style30">
                                <asp:TextBox ID="txtbox_address1" runat="server" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the Street Address" style="resize: none;" 
                    TextMode="MultiLine" ValidationGroup="a"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_address2" runat="server" 
                    ControlToValidate="txtbox_address1" Display="Dynamic" 
                    ErrorMessage="Enter the address" ForeColor="#CC0000" ValidationGroup="a" 
                                    CssClass="validation"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style34">
                                Country</td>
                            <td class="style41">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddl_Parkercountry" runat="server" AutoPostBack="True" 
                            CssClass="twitterStyleTextbox" onselectedindexchanged="ddl_Parkercountry_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="ddl_Parkercountry" Display="Dynamic" 
                            ErrorMessage="Enter Country Name" ForeColor="#CC0000" ValidationGroup="a" 
                                            CssClass="validation"></asp:RequiredFieldValidator>
                                        <%--<asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country]"></asp:SqlDataSource>--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td width="50">
                            </td>
                            <td class="style39">
                                Province</td>
                            <td class="style30">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="True" 
                            CssClass="twitterStyleTextbox"  ValidationGroup="a" 
                                            onselectedindexchanged="ddlstate_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id)">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="ddl_Parkercountry" Name="Country_id" 
                                    PropertyName="SelectedValue" Type="Int64" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>--%>
                                        <asp:RequiredFieldValidator ID="rfv_state0" runat="server" 
                            ControlToValidate="ddlstate" ErrorMessage="Enter Province Name" ForeColor="#CC0000" 
                            ValidationGroup="a" Display="Dynamic" CssClass="validation"></asp:RequiredFieldValidator>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="style9">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style34">
                                City</td>
                            <td class="style41">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddl_Parkercity" runat="server" 
                            CssClass="twitterStyleTextbox"   onselectedindexchanged="ddl_Parkercity_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="ddl_Parkercity" ErrorMessage="Enter City Name" ForeColor="#CC0000" 
                            ValidationGroup="a" CssClass="validation"></asp:RequiredFieldValidator>
                                        <%--<asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                            SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id)">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="ddlstate" Name="Province_id" 
                                    PropertyName="SelectedValue" Type="Int64" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="style38">
                                &nbsp;</td>
                            <td class="style39">
                                Email Id</td>
                            <td class="style30">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtbox_Email" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" ></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lbl_emailcheck" runat="server" ForeColor="#CC0000" 
                    Visible="False"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                    ControlToValidate="txtbox_Email" ErrorMessage="Enter The Email ID" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic" CssClass="validation"></asp:RequiredFieldValidator>
                            
                                <asp:RegularExpressionValidator ID="rgexp_email" runat="server" 
                    ErrorMessage="*Please Enter Email In The Correct Format" ControlToValidate="txtbox_Email" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="validation"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style34">
                                Postal Code /Zip Code</td>
                            <td class="style41">
                                <asp:TextBox ID="txtbox_postal" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" placeholder="Enter The Zip"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_postal" runat="server" 
                    ControlToValidate="txtbox_postal" 
                    ErrorMessage="Enter the Postal Code/Zip Code " ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic" CssClass="validation"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_postal" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300" CssClass="validation"></asp:RegularExpressionValidator>
                            </td>
                            <td width="50">
                            </td>
                            <td class="style39">
                                Phone No.</td>
                            <td class="style30">
                                <asp:TextBox ID="txtbox_phone" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                               
                                <asp:RequiredFieldValidator ID="rfv_phoneno" runat="server" 
                    ControlToValidate="txtbox_phone" ErrorMessage="Enter Phone Number" 
                    ForeColor="#CC0000" Display="Dynamic" ValidationGroup="a" CssClass="validation"></asp:RequiredFieldValidator>
                              
                                <asp:RegularExpressionValidator    ID="RegularExpressionValidator1" 
                                    runat="server"  ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_phone" ValidationGroup="b" ForeColor="#FF3300" 
                                    CssClass="validation" ></asp:RegularExpressionValidator>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_phone" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="style34">
                                Cellular No.</td>
                            <td class="style41">
                                <asp:TextBox ID="txtbox_cellular" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox" ></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfv_cellular" runat="server" 
                    ControlToValidate="txtbox_cellular" ErrorMessage="Enter Cellular Number" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic" CssClass="validation"></asp:RequiredFieldValidator>
                              
                                <asp:RegularExpressionValidator         ID="RegularExpressionValidator2" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_cellular" ValidationGroup="b" ForeColor="#FF3300" 
                                    CssClass="validation" ></asp:RegularExpressionValidator>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                 runat="server" Enabled="True" TargetControlID="txtbox_cellular" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                            <td width="50">
                            </td>
                            <td class="style38">
                                Fax No.</td>
                            <td class="style39">
                                <asp:TextBox ID="txt_fax" runat="server" CssClass="twitterStyleTextbox" 
                    Placeholder="Enter The Fax Number"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_fax0" runat="server" 
                    ControlToValidate="txt_fax" Display="Dynamic" ErrorMessage="Enter Fax Number" 
                    ForeColor="#CC0000" ValidationGroup="a" CssClass="validation"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Check Counter" Enabled="False"></asp:Label>
                            </td>
                            <td class="style41">
                                <asp:TextBox ID="txtbox_checkcounter" runat="server"></asp:TextBox>
                            </td>
                            <td width="50">
                                &nbsp;</td>
                            <td class="style38" valign="top">
                                &nbsp;</td>
                            <td class="style39">
                                &nbsp;</td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
         
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="Panel3" runat="server">
                        <fieldset>
                            <legend>&nbsp;Login Information</legend>
                            <table class="form-table01">
                                <tr>
                                    <td>
                                        User Name</td>
                                    <td>
                                        <asp:TextBox ID="txtBox_username" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" 
                      
                        Enabled="False" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%">
                                        Password</td>
                                    <td>
                                        <asp:TextBox ID="txtBox_Passwrd" runat="server" ValidationGroup="a" 
                     CssClass="twitterStyleTextbox" 
                    Placeholder="Enter The Password" 
                    ontextchanged="txtBox_Passwrd_TextChanged" AutoPostBack="True"></asp:TextBox>
                                        <cc1:PasswordStrength ID="txtBox_Passwrd_PasswordStrength0" runat="server" 
                    Enabled="True" MinimumUpperCaseCharacters="1" PreferredPasswordLength="10" 
                    StrengthIndicatorType="BarIndicator" TargetControlID="txtBox_Passwrd"  
                    TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent">
                                        </cc1:PasswordStrength>
                                        <asp:RequiredFieldValidator ID="rfv_password" runat="server" 
                    ControlToValidate="txtBox_Passwrd" ErrorMessage="Enter password" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic" CssClass="validation"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div>
                        <asp:Panel ID="Panel4" runat="server">
                            <fieldset>
                                <legend>Billing Information</legend>
                                <br />
                                <asp:Label ID="billinfo" runat="server" Enabled="false"></asp:Label>
                            </fieldset>
                        </asp:Panel>
                       </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Button ID="btn_next" runat="server" Text="Update"  CssClass="button-right pull-right" onclick="btn_next_Click" 
                         />
        </td>
    </tr>
</table>
</ContentTemplate>
</cc1:TabPanel>



    


       <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3"  CssClass="Initial" ><HeaderTemplate>
           Parking Type
</HeaderTemplate>
<ContentTemplate>
        
                  <br />
        







         <table class="registration-area" width="100%">
   <tr>
   <td>
       <asp:Panel ID="Panel6" runat="server" Width="90%"  ScrollBars="Vertical" Height="200"
           BackColor="#F3F3F3">
        <fieldset><legend>Reserved</legend>
        
                    <asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand1" 
                                    >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Number Of Parker
                    </th>
                       <th>
                           Plan Name
                    </th>
                     <th>
                         Monthly Rate
                    </th>
                
               
                    <th>
                        Total Registration
                    </th>
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("Total_Parker")%></td>
             
 
               <td>

            <%#Eval("planname")%></td>
                <td>

            <%#Eval("monthlyrate")%></td>
                <td>

            <%#Eval("total_regis_parker")%></td>
            
           
        
                    </tr>
        </ItemTemplate>
        <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
        
        </fieldset>
        </asp:Panel>
   </td>
 
   <td>
   
   <asp:Panel ID="Panel7" runat="server" Width="90%" ScrollBars="Vertical" Height="200" 
           BackColor="#F3F3F3">
        <fieldset><legend>Random</legend>
                 <asp:Repeater ID="Repeater2" runat="server" onitemcommand="Repeater1_ItemCommand1" 
                                    >
                   <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Number Of Parker
                    </th>
                       <th>
                           Plan Name
                    </th>
                     <th>
                         Monthly Rate
                    </th>
                
                
                    <th>
                        Total Registration
                    </th>
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>

            <%#Eval("Total_Parker")%></td>
             
 
               <td>

            <%#Eval("planname")%></td>
                <td>

            <%#Eval("monthlyrate")%></td>
                <td>

            <%#Eval("total_regis_parker")%></td>
            
           
        
                    </tr>
        </ItemTemplate>
        <FooterTemplate></table></FooterTemplate>
                </asp:Repeater>
        
        </fieldset>
        </asp:Panel>
   </td>
   </tr>
   </table>

    <asp:Panel ID="Panel10" runat="server" Width="100%">
    <fieldset><legend>Select Plan</legend>
        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
        <ContentTemplate>
    <table class="form-table01" width="100%">
    <tr>
    <td width="224">
        Plan Type
    </td>
    <td style="width: 75%" >
        <asp:DropDownList ID="ddl_plantype" runat="server" 
            CssClass="twitterStyleTextbox" 
            onselectedindexchanged="ddl_plantype_SelectedIndexChanged" 
            AutoPostBack="True">
            <asp:ListItem Value="0">--Select--</asp:ListItem>
            <asp:ListItem Value="1">Reserved</asp:ListItem>
            <asp:ListItem Value="2">Random</asp:ListItem>
        </asp:DropDownList>
    </td>
     
    <td style="width: 111px">
        <asp:Label ID="lbl_plan" runat="server" Visible="False"></asp:Label>
        </td>
    <td>
        <asp:DropDownList ID="ddl_plan" runat="server" CssClass="twitterStyleTextbox" 
            onselectedindexchanged="ddl_plan_SelectedIndexChanged1" 
            AutoPostBack="true" Visible="False" >
        </asp:DropDownList>
        <asp:Label ID="lbl_reserved_totalparker" runat="server" Text="0" 
            Visible="False"></asp:Label>
        <asp:Label ID="lbl_random_totalparker" runat="server" Text="0" Visible="False"></asp:Label>
    </td>
    </tr></table>
    </ContentTemplate>
     <Triggers>
      
       <asp:AsyncPostBackTrigger ControlID="ddl_plantype" EventName="SelectedIndexChanged" />
       <asp:AsyncPostBackTrigger ControlID="ddl_plan" EventName="SelectedIndexChanged" />
         
                </Triggers>
        </asp:UpdatePanel>
    </fieldset>
    </asp:Panel>
    <asp:Panel ID="Panel11" runat="server" Width="100%">
    <fieldset><legend>Employee Detail</legend>
        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
        <ContentTemplate>
<table class="form-table01" width="100%">
    <tr><td class="style12" width="150">Employee ID </td>
        <td class="style12">
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="#CC0000"></asp:Label></td>
        <td class="style3">
            <asp:TextBox ID="txtboxcode_employeecode" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  Employee Code" 
        ontextchanged="txtboxcode_employeecode_TextChanged" AutoPostBack="True"></asp:TextBox>
            
</td>
        <td class="style3"><asp:RequiredFieldValidator ID="rfv_code" runat="server" 
                ControlToValidate="txtboxcode_employeecode" ErrorMessage="*" 
                ForeColor="#CC0000" ValidationGroup="m"></asp:RequiredFieldValidator></td><td class="style11">&nbsp;</td>
        <td class="style11">
            </td>
        <td class="style5">

        <asp:Label ID="lblerror" runat="server" ForeColor="#FF3300" Visible="False" 
                CssClass="validation"></asp:Label>
<asp:Label ID="lbl_invoicenumber" runat="server" ForeColor="#FF3300" Visible="False"></asp:Label>
</td><td class="style14">
                    
</td>
    </tr>
    <tr>
        <td class="style12" style="width: 77px">
            Name</td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label17" runat="server" ForeColor="#CC0000" Text="*"></asp:Label>
        </td>
        <td class="style3">
           <asp:TextBox ID="txtbox_emp_firstname" runat="server" 
                     ValidationGroup="a" CssClass="twitterStyleTextbox" 
                    placeholder="Enter the  First Name"></asp:TextBox>
            
        </td>
        <td class="style3">
           <asp:RequiredFieldValidator ID="rfv_code0" runat="server" 
                ControlToValidate="txtbox_emp_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator></td>
        <td class="style11">
           <asp:Label ID="Label19" runat="server" ForeColor="#CC0000" Text="*"></asp:Label></td>
        <td>
            
            Email</td>
        <td class="style5">
           <asp:TextBox ID="txtBox_emp_emailid" runat="server" 
                CssClass="twitterStyleTextbox" placeholder="Enter the Email Id" 
                ValidationGroup="a"></asp:TextBox>
             <asp:RequiredFieldValidator ID="rfv_code2" runat="server" 
                ControlToValidate="txtBox_emp_emailid" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ErrorMessage="Enter Correct Format" 
                ControlToValidate="txtBox_emp_emailid" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                CssClass="validation"></asp:RegularExpressionValidator>
        </td>
        <td class="style14">
       </td>
    </tr>
    <tr><td class="style12" style="width: 77px" >Address</td>
        <td class="style12" style="width: 14px">
            <asp:Label ID="Label18" runat="server" ForeColor="#CC0000" Text="*"></asp:Label>
        </td>
        <td class="style3">
            <asp:TextBox ID="txtBox_emp_address1" runat="server" 
                    CssClass="twitterStyleTextbox" placeholder="Enter the St#" 
                    ValidationGroup="a"></asp:TextBox>

            

</td><td class="style3">
           <asp:RequiredFieldValidator ID="rfv_code1" runat="server" 
                ControlToValidate="txtBox_emp_address1" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator></td><td class="style11" >
            <asp:Label ID="Label20" runat="server" ForeColor="#CC0000" Text="*"></asp:Label></td>
        <td class="style11" width="100">
            
            Cell#</td>
        <td class="style5">
            <asp:TextBox ID="txtbox_emp_cell" runat="server" CssClass="twitterStyleTextbox" 
            placeholder="Enter the  Cell Number" ValidationGroup="a"></asp:TextBox>


<asp:RequiredFieldValidator ID="rfv_code3" runat="server" 
                ControlToValidate="txtbox_emp_cell" ErrorMessage="*" ForeColor="#CC0000" 
                ValidationGroup="m"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
            ControlToValidate="txtbox_emp_cell" ErrorMessage="Enter Correct Format" 
            ForeColor="#FF3300" 
            ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
            ValidationGroup="b" CssClass="validation"></asp:RegularExpressionValidator>
            


</td>
    </tr>
    </table>
    </ContentTemplate>
     <Triggers>
      
         <asp:AsyncPostBackTrigger ControlID="txtboxcode_employeecode" EventName="TextChanged" />
         
                </Triggers>
        </asp:UpdatePanel>
    </fieldset>
    </asp:Panel>
  
                <div class="clearfix">  </div>
                
                   <asp:Button ID="btnclose1" runat="server" CssClass="button-right pull-right" 
                onclick="btnclose1_Click" style="text-align: center; margin-left:5px;" Text="Finish" />
                
                <asp:Button ID="btn_save" runat="server" Text="Save" CssClass="button-right pull-right" 
                  onclick="btn_save_Click"  />
                <div class="clearfix">&nbsp; </div>
<table class="registration-area" width="100%">
<tr>
<td valign="top">
<asp:Panel ID="Panel1" runat="server"  Width="90%"  ScrollBars="Vertical" BackColor="#F3F3F3" Height="350">
    <fieldset><legend>Reserved Parker</legend>
        <asp:Repeater ID="rpt_reserved" runat="server" 
            onitemcommand="rpt_reserved_ItemCommand">
     
         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Employee Code
                    </th>

                     <th>
                         Employee Name
                    </th>
                
                    <th>
                        Edit Detail
                    </th>
                   
                    <th>
                        Edit Vehicle
                    </th>
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>
              <%#Eval("empcode")%>
                 <asp:Label ID="empid" runat="server" Text='<%# Eval("empid") %>' 
                                        Visible="False" /></td>
           <td>
           <%#Eval("emp_name")%></td>
              
                   <td>
           
        <asp:LinkButton ID="lnkEdit" CommandName="cmdEditdetail" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton></td>
                      <td>
           
        <asp:LinkButton ID="LinkButton1" CommandName="cmdEditvehicle" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton></td>
          
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>
    </fieldset>
    </asp:Panel>


</td>
<td valign="top">
<asp:Panel ID="Panel5" runat="server"  Width="90%" ScrollBars="Vertical" BackColor="#F3F3F3" Height="350">
    <fieldset><legend>Random Parker</legend>
    
    <asp:Repeater ID="rpt_random" runat="server" onitemcommand="rpt_random_ItemCommand">
     
         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Employee Code
                    </th>

                     <th>
                         Employee Name
                    </th>
                
                    <th>
                        Edit Detail
                    </th>
                   
                    <th>
                        Edit Vehicle
                    </th>
                   
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
             <td>
              <%#Eval("empcode")%>
                 <asp:Label ID="empid" runat="server" Text='<%# Eval("empid") %>' 
                                        Visible="False" /></td>
          <td>
           <%#Eval("emp_name")%></td>
                   <td>
            <asp:LinkButton ID="lnkEdit" CommandName="cmdEditdetail" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton>
          </td>
              <td>
           
        <asp:LinkButton ID="LinkButton1" CommandName="cmdEditvehicle" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton></td>
          
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>
    
    </fieldset>
    </asp:Panel>


</td>
</tr>
</table>
       
</ContentTemplate>
</cc1:TabPanel>

       <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="TabPanel5" Visible="false">
    <HeaderTemplate>
        Add Vehicle
</HeaderTemplate>

    


<ContentTemplate>













                    <asp:Panel ID="Panel9" runat="server"  Width="100%" CssClass="parker-table">
                      <fieldset>
                    
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        DeleteCommand="DELETE FROM [tbl_CorporateEmployeeVehicleDetails] WHERE [EmployeeVehicleID] = @EmployeeVehicleID" 
        InsertCommand="INSERT INTO [tbl_CorporateEmployeeVehicleDetails] ([EmployeeID], [MakeID], [ModelID], [Year], [LicenseNo], [ColorID]) VALUES (@Parker_id, @Make, @Model, @Year, @LicenseNo, @Color)" 
        SelectCommand="SELECT tbl_CorporateEmployeeVehicleDetails.*,tbl_Vehicle_color.*,tbl_VehicleMakes.* FROM [tbl_CorporateEmployeeVehicleDetails] inner join tbl_Vehicle_color on tbl_Vehicle_color.VehiColorid=tbl_CorporateEmployeeVehicleDetails.ColorID inner join tbl_VehicleMakes on tbl_VehicleMakes.VehiMakeID=tbl_CorporateEmployeeVehicleDetails.MakeID WHERE [EmployeeID]=@Parker_id " 
         UpdateCommand="UPDATE [tbl_CorporateEmployeeVehicleDetails] SET  [MakeID] = @Make, [ModelID] = @Model, [Year] = @Year,  [LicenseNo] = @LicenseNo ,[ColorID] = @Color WHERE [EmployeeVehicleID] = @EmployeeVehicleID" 
                              onselecting="SqlDataSource2_Selecting">
        <DeleteParameters>
            <asp:Parameter Name="EmployeeVehicleID" Type="Int64" />
        </DeleteParameters>
       <InsertParameters>
            
            <asp:Parameter Name="Make" Type="Int64" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
              <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="Color" Type="Int64" />
          
           
            <asp:SessionParameter Name="Parker_id" SessionField="Parker_id" 
                Type="Int64" />
        
        </InsertParameters>
        <SelectParameters>
             <asp:SessionParameter Name="Parker_id" SessionField="Parker_id" 
                Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Parker_Id" Type="Int64" />
            <asp:Parameter Name="Make" Type="Int64" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="Int64" />
            <asp:Parameter Name="LicenseNo" Type="String" />
          
            <asp:Parameter Name="EmployeeVehicleID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource9" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
        SelectCommand="SELECT * FROM [tbl_VehicleMakes] ORDER BY [VehiMake]" 
                              onselecting="SqlDataSource9_Selecting">
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource10" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_Vehicle_color]" 
                              onselecting="SqlDataSource10_Selecting">
        </asp:SqlDataSource>
                    
                      <asp:ListView ID="ListView1" runat="server" DataKeyNames="EmployeeVehicleID" 
        DataSourceID="SqlDataSource2" InsertItemPosition="LastItem" 
        onselectedindexchanged="ListView1_SelectedIndexChanged">
        <AlternatingItemTemplate>
            <tr style="background-color: #FAFAD2;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button"  
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button" />
                </td>
                
                <td >
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="background-color: #FFCC66;color: #000080;">
                <td> 
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" CssClass="button"  
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="button" 
                        Text="Cancel" />
                </td>
                
                <td>
                 <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource9" DataTextField="VehiMake" 
                DataValueField="VehiMakeID" SelectedValue='<%# Bind("VehiMakeID") %>'> </asp:DropDownList>

                    
                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("ModelID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
              

                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource10" DataTextField="VehiColor" 
                DataValueField="VehiColorid" SelectedValue='<%# Bind("ColorID") %>'> </asp:DropDownList>
                  
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table id="Table1" runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" CssClass="add"  
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="cancel"  
                        Text="Clear" />
                </td>
                 
                <td>
                    
                    <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource9" DataTextField="VehiMake" 
                DataValueField="VehiMakeID" SelectedValue='<%# Bind("Make", "{0}") %>'> </asp:DropDownList>
                    

                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource10" DataTextField="VehiColor" 
                DataValueField="VehiColorid" SelectedValue='<%# Bind("Color", "{0}") %>'> </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
              
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="background-color: #FFFBD6;color: #333333;">
                <td>
             
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button"  />
                </td>
               
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td >
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td >
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
               
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table id="Table2" runat="server">
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="1"
                            style=" background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family:Open Sans; width:100%;">
                            <tr id="Tr2" runat="server" style="background-color: #FFFBD6;color: #333333;">
                                <th id="Th1" runat="server">
                                </th>
                                <th id="Th4"  runat="server">
                                    Make</th>
                                <th id="Th5" runat="server">
                                    Model</th>
                                <th id="Th6" runat="server">
                                    Year</th>
                                <th id="Th7" runat="server">
                                    Color</th>
                                <th id="Th8" runat="server">
                                    LicenseNo</th>
                                
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td2" runat="server" 
                        style="text-align: center;background-color: #6699ff;font-family: Verdana, Arial, Helvetica, sans-serif;color: #333333;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                    ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #FFCC66;font-weight: bold;color: #000080;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit" />
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
     </fieldset>
                      
                      </asp:Panel>
    <asp:Button ID="Button1" runat="server" Text="Add More Parker" CssClass="button" 
                        onclick="Button1_Click" />
                    &nbsp;<asp:Button ID="btn_close" runat="server" Text="Close" CssClass="button" 
                        onclick="btn_close_Click" Visible="False"  />
   
    
</ContentTemplate>
    
</cc1:TabPanel>
       
       
       
</cc1:TabContainer>
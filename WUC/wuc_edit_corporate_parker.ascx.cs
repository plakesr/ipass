﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Drawing;

public partial class WUC_wuc_edit_corporate_parker : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    DataTable dtnew = new DataTable();
    DataTable dtnewreserved = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack == true)
        {
            
            try
            {
                dtnew.Columns.Add("username", typeof(string));
                dtnew.Columns.Add("planid", typeof(int));
                dtnew.Columns.Add("numberofparker", typeof(int));
                Session["random"] = dtnew;

                dtnewreserved.Columns.Add("username", typeof(string));
                dtnewreserved.Columns.Add("planid", typeof(int));
                dtnewreserved.Columns.Add("numberofparker", typeof(int));
                Session["reserved"] = dtnewreserved;


                dt_employeecharge.Columns.Add("ActivationCharge", typeof(decimal));
                dt_employeecharge.Columns.Add("MonthlyCharge", typeof(decimal));
                dt_employeecharge.Columns.Add("DeliveryCharge", typeof(decimal));
                Session["dt_employeecharge"] = dt_employeecharge;
            }
            catch
            {
            }
            try
            {
                DataSet details = new clsInsert().fetchrec("select  tbl_UserLogin.* ,tbl_ParkerRegis.* from tbl_ParkerRegis inner join tbl_UserLogin on tbl_ParkerRegis.UserName=tbl_UserLogin.Username  where tbl_ParkerRegis.UserName='" + Session["parkerUsername"].ToString() + "'");
                DataSet dtcampusname = new clsInsert().fetchrec("select tbl_CampusMaster.Campus_Name,tbl_LotMaster.LotName from tbl_LotMaster inner join tbl_CampusMaster on tbl_LotMaster.Campus_id=tbl_CampusMaster.CampusID where Lot_id=" + Convert.ToInt32(Session["lotid"].ToString()));
                if (details.Tables[0].Rows.Count > 0)
                {
                    txtBox_Companyname.Text = details.Tables[0].Rows[0]["FirstName"].ToString();
                    txtbox_campusname.Text = dtcampusname.Tables[0].Rows[0]["LotName"].ToString();
                    txtbox_lotname.Text = dtcampusname.Tables[0].Rows[0]["Campus_Name"].ToString();
                    DateTime dateactivation=Convert.ToDateTime(details.Tables[0].Rows[0]["ActivationDate"].ToString());
                    txtbox_ActicationDate.Text = dateactivation.ToShortDateString();
                    txtbox_address1.Text = details.Tables[0].Rows[0]["AdressLine1"].ToString();
                    txtbox_cellular.Text = details.Tables[0].Rows[0]["CellNo"].ToString();
                    txt_fax.Text = details.Tables[0].Rows[0]["Fax"].ToString();
                    txtbox_Firstname.Text = details.Tables[0].Rows[0]["LastName"].ToString();
                    txtbox_designation.Text =details.Tables[0].Rows[0]["operator_id"].ToString();
                    txtBox_username.Text = details.Tables[0].Rows[0]["Username"].ToString();
                    txtBox_Passwrd.Text = details.Tables[0].Rows[0]["Pass"].ToString();
                    txtbox_postal.Text = details.Tables[0].Rows[0]["Zip"].ToString();
                    txtbox_Email.Text = details.Tables[0].Rows[0]["Email"].ToString();
                    txtbox_phone.Text = details.Tables[0].Rows[0]["Phoneno"].ToString();
                    bindcountry();
                    
                    //bindcity();
                    DataSet dds = new clsInsert().fetchdatea("select Country_id  from tbl_country where Country_name ='" + details.Tables[0].Rows[0]["Country"].ToString() + "' ;select ProvinceId  from tbl_Province where ProvinceName ='" + details.Tables[0].Rows[0]["State"].ToString() + "';select City_id  from tbl_City where City_name ='" + details.Tables[0].Rows[0]["City"].ToString() + "';");
                    int parkercountry = ddl_Parkercountry.Items.Count;
                    //int parkerstate = ddlstate.Items.Count;

                    //int parkercity = ddl_Parkercity.Items.Count;
                    ddl_Parkercountry.SelectedValue = dds.Tables[0].Rows[0][0].ToString();
                    Session["Countryid"] = Convert.ToInt32(dds.Tables[0].Rows[0][0].ToString());
                    bindstate();
                    ddlstate.SelectedValue = dds.Tables[1].Rows[0][0].ToString();
                    Session["Stateid"] = Convert.ToInt32(dds.Tables[1].Rows[0][0].ToString());
                    bindcity();
                    ddl_Parkercity.SelectedValue = dds.Tables[2].Rows[0][0].ToString();

                    

                  //  Label3.Text = details.Tables[0].Rows[0]["Charges"].ToString();
//txtbox_deliverytype.Text = details.Tables[0].Rows[0]["CardDeliveryName"].ToString();
                    try
                    {
                        DataSet dt_billingtype = new clsInsert().fetchrec("select Biliing_type from TBL_Invoice where Acct_Num='" + Session["parkerUsername"].ToString() + "'");
                        if (dt_billingtype.Tables[0].Rows[0]["Biliing_type"].ToString() == "1")
                        {
                            txtbox_checkcounter.Enabled = false;

                            txtbox_checkcounter.Text = "Full";
                        }
                        else
                        {
                            txtbox_checkcounter.Enabled = false;
                            txtbox_checkcounter.Text = "Pay Per Use";
                        }

                    }
                    catch
                    {
                        txtbox_checkcounter.Enabled = false;
                        txtbox_checkcounter.Text = "Pay Per Use";
                    }
                  
                    if (details.Tables[0].Rows[0]["Billingwith"].ToString() == "1")
                    {
                        billinfo.Text = "Auto Charge To Credit Card";
                    }
                    if (details.Tables[0].Rows[0]["Billingwith"].ToString() == "2")
                    {
                        billinfo.Text = "Invoice By Mail";
                    } 
                    if (details.Tables[0].Rows[0]["Billingwith"].ToString() == "3")
                    {
                        billinfo.Text = "Invoice By Email";
                    } 
                    if (details.Tables[0].Rows[0]["Billingwith"].ToString() == "4")
                    {
                        billinfo.Text = "Auto Charge To Account";
                    }
                }
            }
            catch
            {

            }
            try
            {
                reservedalotedspace();
            }
            catch
            {
            }
            try
            {
                randomalotedspace();
            }
            catch
            {
            }

            try
            {
                employeedetails();
            }
            catch
            {

            }
            //try
            //{
            //    clsInsert onj = new clsInsert();
            //    DataSet rec = onj.fetchrec("select  chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"].ToString()) + " and Parkingtype=1 and ISSpecial=0 and chargeby=3");
            //    DataTable t = new DataTable("chargeadd");
            //    t.Columns.Add("LotRateid", typeof(int));
            //    t.Columns.Add("Charge", typeof(string));
            //    if (rec.Tables[0].Rows.Count > 0)
            //    {
            //        Panel6.Visible = true;
            //        for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
            //        {
            //            DataRow dr = t.NewRow();

            //            if (rec.Tables[0].Rows[i][0].ToString() == "1")
            //            {
            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();

            //                dr[1] = "Hourly";
            //                t.Rows.Add(dr);
            //            }
            //            if (rec.Tables[0].Rows[i][0].ToString() == "2")
            //            {
            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();

            //                dr[1] = "Daily";
            //                t.Rows.Add(dr);
            //            }
            //            if (rec.Tables[0].Rows[i][0].ToString() == "3")
            //            {
            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();

            //                dr[1] = "Monthly";
            //                t.Rows.Add(dr);
            //            }
            //            if (rec.Tables[0].Rows[i][0].ToString() == "4")
            //            {
            //                dr[0] = rec.Tables[0].Rows[i][0].ToString();

            //                dr[1] = "Weekly";
            //                t.Rows.Add(dr);
            //            }



            //        }

            //        RemoveDuplicateRows(t, "Charge");
            //        //ddl_reservedtype.DataSource = t;
            //        //ddl_reservedtype.DataTextField = "Charge";
            //        //ddl_reservedtype.DataValueField = "LotRateid";
            //        //ddl_reservedtype.DataBind();




            //    }
            //    else
            //    {
            //        Panel6.Visible = false;
            //    }
            //    DataSet rec1 = onj.fetchrec("select  chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"].ToString()) + " and Parkingtype=2 and ISSpecial=0 and chargeby=3");
            //    DataTable t1 = new DataTable("chargeadd");
            //    t1.Columns.Add("LotRateid", typeof(int));
            //    t1.Columns.Add("Charge", typeof(string));
            //    if (rec1.Tables[0].Rows.Count > 0)
            //    {
            //        Panel7.Visible = true;
            //        for (int i = 0; i < rec1.Tables[0].Rows.Count; i++)
            //        {
            //            DataRow dr1 = t1.NewRow();

            //            if (rec1.Tables[0].Rows[i][0].ToString() == "1")
            //            {
            //                dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

            //                dr1[1] = "Hourly";
            //                t1.Rows.Add(dr1);
            //            }
            //            if (rec1.Tables[0].Rows[i][0].ToString() == "2")
            //            {
            //                dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

            //                dr1[1] = "Daily";
            //                t1.Rows.Add(dr1);
            //            }
            //            if (rec1.Tables[0].Rows[i][0].ToString() == "3")
            //            {
            //                dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

            //                dr1[1] = "Monthly";
            //                t1.Rows.Add(dr1);
            //            }
            //            if (rec1.Tables[0].Rows[i][0].ToString() == "4")
            //            {
            //                dr1[0] = rec1.Tables[0].Rows[i][0].ToString();

            //                dr1[1] = "Weekly";
            //                t1.Rows.Add(dr1);
            //            }



            //        }

            //        //RemoveDuplicateRows(t1, "Charge");
            //        //ddl_Randomparking.DataSource = t1;
            //        //ddl_Randomparking.DataTextField = "Charge";
            //        //ddl_Randomparking.DataValueField = "LotRateid";
            //        //ddl_Randomparking.DataBind();
            //    }
            //    else
            //    {
            //        Panel7.Visible = false;
            //    }
            //}
            //catch
            //{
            //}
            try
            {



                dt_charge.Columns.Add("Total_Parker", typeof(int));

                dt_charge.Columns.Add("PlanType", typeof(string));
                dt_charge.Columns.Add("TotalAmount", typeof(decimal));
                dt_charge.Columns.Add("Planid", typeof(int));
                dt_charge.Columns.Add("plandetail", typeof(string));
                dt_charge.Columns.Add("Username", typeof(string));
                Session["Chargetable"] = dt_charge;



                dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                dt_charge_Random.Columns.Add("PlanType", typeof(string));
                dt_charge_Random.Columns.Add("TotalAmount", typeof(decimal));

                dt_charge_Random.Columns.Add("Planid", typeof(int));
                dt_charge_Random.Columns.Add("plandetail", typeof(string));
                dt_charge_Random.Columns.Add("Username", typeof(string));

                Session["ChargetableRandom"] = dt_charge_Random;


            }
            catch
            {

            }

        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCountries(string prefixText)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
        con.Open();
        //if(HttpContext.Current.Session[" Login"]!=null)



        SqlCommand cmd = new SqlCommand("select lotid,tbl_LotMaster.LotName from tbl_UserAuthenticatedlot inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_UserAuthenticatedlot.lotid where username='" + HttpContext.Current.Session["Login"].ToString() + "' and LotName like''+@Name+'%'", con);
        cmd.Parameters.AddWithValue("@Name", prefixText);


        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }
    public void Fillddlsites(int id)
    {

        //List<clsData> lst = new clsInsert().Getlot_chargeby1(id);
        //if (lst.Count() > 0)
        //{
        //    ddl_reservedtype.DataSource = lst;
        //    ddl_reservedtype.DataTextField = "Charge";
        //    ddl_reservedtype.DataValueField = "LotRateid";
        //    ddl_reservedtype.DataBind();
        //}
        //ddl_reservedtype.Items.Insert(0, new ListItem("Select Sites", "0"));

    }
    protected void Repeater1_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            //if (e.CommandName == "remove")
            //{
            //    Label l = (Label)e.Item.FindControl("Label2");

            //    lbl_alltotal.Text = Convert.ToString(Convert.ToDecimal(lbl_alltotal.Text) - Convert.ToDecimal(l.Text));
            //    dt_charge = (DataTable)Session["Chargetable"];

            //    dt_charge.Rows.RemoveAt(e.Item.ItemIndex);

            //    Repeater1.DataSource = dt_charge;
            //    Repeater1.DataBind();

            //    Session["Chargetable"] = dt_charge;

            //}
        }
        catch
        {
        }
    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    DataTable dt_charge = new DataTable("ChargeByReserved");
    DataTable dt_charge_Random = new DataTable("ChargeByRandom");
    DataTable dt_employeecharge = new DataTable();
    //protected void btn_add_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        dt_charge = (DataTable)Session["Chargetable"];

    //        DataRow dr = dt_charge.NewRow();
    //        dr[0] = Convert.ToInt32(txtbox_numberofparker.Text);
    //        dr[1] = ddl_reservedtype.SelectedItem.Text;
    //        dr[2] = Convert.ToDecimal(lbl_total.Text);
    //        dr[3] = Convert.ToInt32(ddl_chargeby.SelectedItem.Value);
    //        dr[4] = ddl_reservedtype.SelectedItem.Text + ddl_chargeby.SelectedItem.Text + "of $" + lbl_charge.Text;
    //        dr[5] = txtBox_username.Text;
    //        dt_charge.Rows.Add(dr);
    //        Session["Chargetable"] = dt_charge;
    //        RemoveDuplicateRows(dt_charge, "Planid");
    //        //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

    //        if (dt_charge.Rows.Count > 0)
    //        {
    //            Repeater1.Visible = true;
    //            Repeater1.DataSource = dt_charge;
    //            Repeater1.DataBind();
    //            ddl_reservedtype.Visible = false;
    //            ddl_chargeby.Visible = false;
    //            lbl_total.Visible = false;
    //            //lbl_totaltext.Visible = false;
    //            lbl_text.Visible = false;
    //            lbl_charge.Visible = false;
    //            btn_add.Visible = false;
    //            txtbox_numberofparker.Text = "";
    //            // lbl_alltotal.Visible = true;


    //        }
    //        decimal grandtotal = Convert.ToDecimal(lbl_alltotal.Text) + Convert.ToDecimal(lbl_total.Text);
    //        lbl_alltotal.Text = Convert.ToString(grandtotal);
    //    }
    //    catch
    //    {
    //        //lbl_error.Visible = true;

    //    }
    //}
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            //if (e.CommandName == "remove")
            //{
            //    Label l = (Label)e.Item.FindControl("Label2");

            //    lbl_alltotalRandome.Text = Convert.ToString(Convert.ToDecimal(lbl_alltotalRandome.Text) - Convert.ToDecimal(l.Text));
            //    dt_charge_Random = (DataTable)Session["ChargetableRandom"];

            //    dt_charge_Random.Rows.RemoveAt(e.Item.ItemIndex);

            //    Repeater2.DataSource = dt_charge_Random;
            //    Repeater2.DataBind();

            //    Session["ChargetableRandom"] = dt_charge_Random;

            //}
        }
        catch
        {
        }
    }
    //protected void TextBox1_TextChanged(object sender, EventArgs e)
    //{
    //    Panel8.Visible = true;
    //    TextBox1.Enabled = false;

    //}
    //protected void txtbox_totalrandomspace_TextChanged(object sender, EventArgs e)
    //{

    //    Panel9.Visible = true;
    //    txtbox_totalrandomspace.Enabled = false;
    //}

    //protected void ddl_chargeby_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    try
    //    {
    //        if (ddl_chargeby.SelectedItem.Text != "Select")
    //        {
    //            int a = 0;
    //            DataTable dt = (DataTable)Session["Chargetable"];
    //            for (int j = 0; j < dt.Rows.Count; j++)
    //            {
    //                if (Convert.ToString(ddl_chargeby.SelectedItem.Value) == dt.Rows[j][3].ToString())
    //                {
    //                    a++;
    //                    break;

    //                }

    //            }
    //            if (a > 0)
    //            {
    //                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //                ddl_chargeby.Visible = false;
    //                lbl_charge.Visible = false;
    //                lbl_text.Visible = false;
    //                lbl_total.Visible = false;
    //               // lbl_totaltext.Visible = false;
    //                btn_add.Visible = false;

    //            }
    //            else
    //            {

    //                DataSet chargetype = onj.fetchrec("select  * from tbl_LotRateMaster where LotRateid=" + ddl_chargeby.SelectedValue);
    //                decimal total_charge = Convert.ToDecimal(chargetype.Tables[0].Rows[0][3]) * Convert.ToDecimal(txtbox_numberofparker.Text);


    //                lbl_charge.Visible = true;
    //                lbl_text.Visible = true;
    //                lbl_total.Visible = true;
    //                //lbl_totaltext.Visible = true;

    //                lbl_total.Text = Convert.ToString(total_charge);


    //                btn_add.Visible = true;
    //                lbl_charge.Text = chargetype.Tables[0].Rows[0][3].ToString();
    //            }
    //        }
    //        else
    //        {
    //            lbl_charge.Visible = true;
    //            lbl_charge.Text = "Select Atleast One Plan";
    //        }
    //    }
    //    catch
    //    {
    //    }



    //    //try
    //    //{
    //    //    if (ddl_chargeby.SelectedItem.Text != "Select")
    //    //    {
    //    //        int a = 0;
    //    //        DataTable dt = (DataTable)Session["Chargetable"];
    //    //        for (int j = 0; j < dt.Rows.Count; j++)
    //    //        {
    //    //            if (Convert.ToString(ddl_chargeby.SelectedItem.Value) == dt.Rows[j][3].ToString())
    //    //            {
    //    //                a++;
    //    //                break;

    //    //            }

    //    //        }
    //    //        if (a > 0)
    //    //        {
    //    //            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //    //            ddl_chargeby.Visible = false;
    //    //            lbl_charge.Visible = false;
    //    //            lbl_text.Visible = false;
    //    //            lbl_total.Visible = false;
    //    //            btn_add.Visible = false;

    //    //        }
    //    //        else
    //    //        {

    //    //            DataSet chargetype = onj.fetchrec("select  * from tbl_LotRateMaster where LotRateid=" + ddl_chargeby.SelectedValue);
    //    //            decimal total_charge = Convert.ToDecimal(chargetype.Tables[0].Rows[0][3]) * Convert.ToDecimal(txtbox_numberofparker.Text);


    //    //            lbl_charge.Visible = true;
    //    //            lbl_text.Visible = true;
    //    //            lbl_totaltext.Visible = true;
    //    //            lbl_total.Visible = true;

    //    //            lbl_total.Text = Convert.ToString(total_charge);


    //    //            btn_add.Visible = true;
    //    //            lbl_charge.Text = chargetype.Tables[0].Rows[0][3].ToString();
    //    //        }
    //    //    }
    //    //    else
    //    //    {
    //    //        lbl_charge.Visible = true;
    //    //        lbl_charge.Text = "Select Atleast One Plan";
    //    //    }
    //    //}
    //    //catch
    //    //{
    //    //}

    //}

    clsInsert onj = new clsInsert();
    //protected void ddl_reservedtype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        // ddl_chargeby.Items.Clear();
    //        if (ddl_reservedtype.SelectedItem.Text == "Select")
    //        {
    //            ddl_chargeby.Visible = false;
    //            lbl_charge.Visible = true;
    //            lbl_charge.Text = "Select atleast One Type";

    //        }
    //        else
    //        {
    //            //int a = 0;
    //            DataTable dt = (DataTable)Session["Chargetable"];
    //            //for (int j = 0; j < dt.Rows.Count; j++)
    //            //{
    //            //    if (ddl_reservedtype.SelectedItem.Text == dt.Rows[j][1].ToString())
    //            //    {
    //            //        a++;
    //            //        break;

    //            //    }

    //            //}
    //            //if (a > 0)
    //            //{
    //            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //            //    ddl_chargeby.Visible = false;
    //            //    lbl_charge.Visible = false;
    //            //    lbl_text.Visible = false;
    //            //    lbl_total.Visible = false;
    //            //    btn_add.Visible = false;

    //            //}
    //            //else
    //            //{

    //            ddl_chargeby.Items.Clear();
    //            ddl_chargeby.Visible = true;

    //            DataSet rec = onj.fetchrec("select LotRateid,Quantity,chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"]) + " and Parkingtype=1 and ISSpecial=0 and chargeby= " + ddl_reservedtype.SelectedValue);
    //            DataTable t = new DataTable("chargeadd");
    //            t.Columns.Add("LotRateid", typeof(int));
    //            t.Columns.Add("Charge", typeof(string));

    //            for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
    //            {
    //                DataRow dr = t.NewRow();
    //                string charge = rec.Tables[0].Rows[i][1].ToString().Trim();
    //                string[] strArr = charge.Split('.');

    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "1")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Hours]";
    //                    t.Rows.Add(dr);
    //                }
    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "2")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Days]";
    //                    t.Rows.Add(dr);
    //                }
    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "3")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Months]";
    //                    t.Rows.Add(dr);
    //                }
    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "4")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Weeks]";
    //                    t.Rows.Add(dr);
    //                }

    //            }


    //            ddl_chargeby.DataSource = t;
    //            ddl_chargeby.DataTextField = "Charge";
    //            ddl_chargeby.DataValueField = "LotRateid";
    //            ddl_chargeby.DataBind();
    //        }



    //        //}
    //        ddl_chargeby.Items.Insert(0, new ListItem("Select Charge", "0"));
    //    }
    //    catch
    //    {
    //    }
    //}
    //protected void txtbox_numberofparker_TextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int total_parker = 0;
    //        for (int i = 0; i < Repeater1.Items.Count; i++)
    //        {

    //            Label Countparker = Repeater1.Items[i].FindControl("totalparker") as Label;
    //            int parker = Convert.ToInt32(Countparker.Text);
    //            total_parker = parker + total_parker;


    //        }
    //        if (Convert.ToInt32(txtbox_numberofparker.Text) > ((Convert.ToInt32(TextBox1.Text)) - total_parker))
    //        {
    //            txtbox_numberofparker.Text = "";
    //            lbl_check_noofparker.Visible = true;
    //            lbl_check_noofparker.ForeColor = Color.Red;
    //            lbl_check_noofparker.Text = "Exceed Than " + TextBox1.Text;
    //        }
    //        else
    //        {
    //            lbl_check_noofparker.Visible = false;
    //            ddl_reservedtype.Visible = true;
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}

    //protected void TextBox3_TextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int total_parker = 0;
    //        for (int i = 0; i < Repeater2.Items.Count; i++)
    //        {

    //            Label Countparker = Repeater2.Items[i].FindControl("totalparker") as Label;
    //            int parker = Convert.ToInt32(Countparker.Text);
    //            total_parker = parker + total_parker;


    //        }
    //        if (Convert.ToInt32(txtbox_random_numberofparker.Text) > ((Convert.ToInt32(txtbox_totalrandomspace.Text)) - total_parker))
    //        {
    //            txtbox_random_numberofparker.Text = "";
    //            lbl_check_noofparkerRandom.Visible = true;
    //            lbl_check_noofparkerRandom.ForeColor = Color.Red;
    //            lbl_check_noofparkerRandom.Text = "Exceed Than " + txtbox_totalrandomspace.Text;
    //        }
    //        else
    //        {
    //            lbl_check_noofparkerRandom.Visible = false;
    //            ddl_Randomparking.Visible = true;
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}



   
    //protected void ddl_chargebyrandom_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (ddl_chargebyrandom.SelectedItem.Text != "Select")
    //        {
    //            int a = 0;
    //            DataTable dt = (DataTable)Session["ChargetableRandom"];
    //            for (int j = 0; j < dt.Rows.Count; j++)
    //            {
    //                if (Convert.ToString(ddl_chargebyrandom.SelectedItem.Value) == dt.Rows[j][3].ToString())
    //                {
    //                    a++;
    //                    break;

    //                }

    //            }
    //            if (a > 0)
    //            {
    //                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //                ddl_chargebyrandom.Visible = false;
    //                lbl_randome_charge.Visible = false;
    //                lbl_randome_text.Visible = false;
    //                lbl_randome_total.Visible = false;
    //                //lbl_random_totaltext.Visible = false;
    //                Add_random.Visible = false;

    //            }
    //            else
    //            {

    //                DataSet chargetype = onj.fetchrec("select  * from tbl_LotRateMaster where LotRateid=" + ddl_chargebyrandom.SelectedValue);
    //                decimal total_charge = Convert.ToDecimal(chargetype.Tables[0].Rows[0][3]) * Convert.ToDecimal(txtbox_random_numberofparker.Text);


    //                lbl_randome_charge.Visible = true;
    //                lbl_randome_text.Visible = true;
    //                lbl_randome_total.Visible = true;
    //            //    lbl_random_totaltext.Visible = true;

    //                lbl_randome_total.Text = Convert.ToString(total_charge);


    //                Add_random.Visible = true;
    //                lbl_randome_charge.Text = chargetype.Tables[0].Rows[0][3].ToString();
    //            }
    //        }
    //        else
    //        {
    //            lbl_randome_charge.Visible = true;
    //            lbl_randome_charge.Text = "Select Atleast One Plan";
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}
    //protected void txtbox_random_numberofparker_TextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int total_parker = 0;
    //        for (int i = 0; i < Repeater2.Items.Count; i++)
    //        {

    //            Label Countparker = Repeater2.Items[i].FindControl("totalparker") as Label;
    //            int parker = Convert.ToInt32(Countparker.Text);
    //            total_parker = parker + total_parker;


    //        }
    //        if (Convert.ToInt32(txtbox_random_numberofparker.Text) > ((Convert.ToInt32(txtbox_totalrandomspace.Text)) - total_parker))
    //        {
    //            txtbox_random_numberofparker.Text = "";
    //            lbl_check_noofparkerRandom.Visible = true;
    //            lbl_check_noofparkerRandom.ForeColor = Color.Red;
    //            lbl_check_noofparkerRandom.Text = "Exceed Than " + txtbox_totalrandomspace.Text;
    //        }
    //        else
    //        {
    //            lbl_check_noofparkerRandom.Visible = false;
    //            ddl_Randomparking.Visible = true;
    //        }
    //    }
    //    catch
    //    {
    //    }
    //}
    protected void txtBox_Passwrd_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Session["password"] = txtBox_Passwrd.Text;
        }
        catch
        {
        }
    }
   

    //protected void Add_random_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        dt_charge_Random = (DataTable)Session["ChargetableRandom"];

    //        DataRow dr = dt_charge_Random.NewRow();
    //        dr[0] = Convert.ToInt32(txtbox_random_numberofparker.Text);
    //        dr[1] = ddl_Randomparking.SelectedItem.Text;
    //        dr[2] = Convert.ToDecimal(lbl_randome_total.Text);
    //        dr[3] = Convert.ToInt32(ddl_chargebyrandom.SelectedItem.Value);
    //        dr[4] = ddl_Randomparking.SelectedItem.Text + ddl_chargebyrandom.SelectedItem.Text + "of $" + lbl_randome_charge.Text;
    //        dr[5] = txtBox_username.Text;
    //        dt_charge_Random.Rows.Add(dr);
    //        Session["ChargetableRandom"] = dt_charge_Random;
    //        RemoveDuplicateRows(dt_charge_Random, "Planid");
    //        //     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Already Plan Type Exist')", true);

    //        if (dt_charge_Random.Rows.Count > 0)
    //        {
    //            Repeater2.Visible = true;
    //            Repeater2.DataSource = dt_charge_Random;
    //            Repeater2.DataBind();
    //            ddl_Randomparking.Visible = false;
    //            ddl_chargebyrandom.Visible = false;
    //            lbl_randome_total.Visible = false;
    //            lbl_randome_text.Visible = false;
    //            lbl_randome_charge.Visible = false;
    //            // lbl_random_totaltext.Visible = false;
    //            Add_random.Visible = false;
    //            txtbox_random_numberofparker.Text = "";
    //            // lbl_alltotal.Visible = true;


    //        }
    //        else
    //        {
    //            Repeater2.Visible = false;
    //        }
    //        decimal grandtotal = Convert.ToDecimal(lbl_alltotalRandome.Text) + Convert.ToDecimal(lbl_randome_total.Text);
    //        lbl_alltotalRandome.Text = Convert.ToString(grandtotal);
    //    }
    //    catch
    //    {
    //        //lbl_error.Visible = true;

    //    }
    //}
    //protected void ddl_Randomparking_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        // ddl_chargeby.Items.Clear();
    //        if (ddl_Randomparking.SelectedItem.Text == "Select")
    //        {
    //            ddl_chargebyrandom.Visible = false;
    //            lbl_randome_charge.Visible = true;
    //            lbl_randome_charge.Text = "Select atleast One Type";

    //        }
    //        else
    //        {
    //            // int a = 0;
    //            DataTable dt = (DataTable)Session["Chargetable"];
    //            //for (int j = 0; j < dt.Rows.Count; j++)
    //            //{
    //            //    if (ddl_reservedtype.SelectedItem.Text == dt.Rows[j][1].ToString())
    //            //    {
    //            //        a++;
    //            //        break;

    //            //    }

    //            //}
    //            //if (a > 0)
    //            //{
    //            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Plan Type Alredy Exist')", true);
    //            //    ddl_chargeby.Visible = false;
    //            //    lbl_charge.Visible = false;
    //            //    lbl_text.Visible = false;
    //            //    lbl_total.Visible = false;
    //            //    btn_add.Visible = false;

    //            //}
    //            //else
    //            //{

    //            ddl_chargebyrandom.Items.Clear();
    //            ddl_chargebyrandom.Visible = true;

    //            DataSet rec = onj.fetchrec("select LotRateid,Quantity,chargeby from tbl_LotRateMaster where Lotid=" + Convert.ToInt32(Session["lotid"]) + " and Parkingtype=2 and ISSpecial=0 and chargeby= " + ddl_Randomparking.SelectedValue);
    //            DataTable t = new DataTable("chargeadd");
    //            t.Columns.Add("LotRateid", typeof(int));
    //            t.Columns.Add("Charge", typeof(string));

    //            for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
    //            {
    //                DataRow dr = t.NewRow();
    //                string charge = rec.Tables[0].Rows[i][1].ToString().Trim();
    //                string[] strArr = charge.Split('.');

    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "1")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Hours]";
    //                    t.Rows.Add(dr);
    //                }
    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "2")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Days]";
    //                    t.Rows.Add(dr);
    //                }
    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "3")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Months]";
    //                    t.Rows.Add(dr);
    //                }
    //                if (rec.Tables[0].Rows[i]["chargeby"].ToString() == "4")
    //                {
    //                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
    //                    dr[1] = "For " + strArr[0] + " [Weeks]";
    //                    t.Rows.Add(dr);
    //                }

    //            }


    //            ddl_chargebyrandom.DataSource = t;
    //            ddl_chargebyrandom.DataTextField = "Charge";
    //            ddl_chargebyrandom.DataValueField = "LotRateid";
    //            ddl_chargebyrandom.DataBind();
    //        }



    //        //}
    //        ddl_chargebyrandom.Items.Insert(0, new ListItem("Select Charge", "0"));
    //    }
    //    catch
    //    {
    //    }
    //}

    public void updatedetails()
    {


        string postal_parker = txtbox_postal.Text;
        string country_parker = ddl_Parkercountry.SelectedItem.Text;
        string state_parker = ddlstate.SelectedItem.Text;
        string city_parker = ddl_Parkercity.SelectedItem.Text;
        string cellular_parker = txtbox_cellular.Text;
        string phone_parker = txtbox_phone.Text;
        string fax_parker = txt_fax.Text;
        string email_parker = txtbox_Email.Text;
        string loginusername_parker = txtBox_username.Text;
        string pass_parker = txtBox_Passwrd.Text;
        
        string oprator = txtbox_designation.Text;
        string modifiedby = "Admin";


        try
        {
            hst.Clear();
            hst.Add("action", "update");
            
            hst.Add("UserName", txtBox_username.Text);
            
            hst.Add("lastname", txtbox_Firstname.Text);
            hst.Add("AdressLine1", txtbox_address1.Text);
            hst.Add("AddressLine2", txtbox_address1.Text);
            hst.Add("Zip_Parker", postal_parker);
            hst.Add("Country_parker", country_parker);
            hst.Add("State_Parker", state_parker);
            hst.Add("city_parker", city_parker);

            hst.Add("cell_parker", cellular_parker);
            hst.Add("phone_parker", phone_parker);
            hst.Add("fax", fax_parker);
            hst.Add("email", email_parker);
          

            hst.Add("login_username", loginusername_parker);
            hst.Add("pass", pass_parker);
            
        
            hst.Add("operatorid", oprator);
            hst.Add("modifiedby", modifiedby);
              int result = objData.ExecuteNonQuery("[sp_Parker_Corporate]", CommandType.StoredProcedure, hst);
              if (result > 0)
              {
              }

            
        }
        catch
        {
        }
    }
    int total_reserved_parker = 0;
    int regis_reserved = 0;
    public void reservedalotedspace()
    {
        try
        {
            DataSet plan = new clsInsert().fetchrec("select tbl_CorporateReservedParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateReservedParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateReservedParkingDetails.Planid where ParkingType=1  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");



            DataTable reserved_plan = new DataTable();

            reserved_plan.Columns.Add("Total_Parker", typeof(int));
            reserved_plan.Columns.Add("monthlyrate", typeof(decimal));
            reserved_plan.Columns.Add("planname", typeof(string));
            reserved_plan.Columns.Add("total_regis_parker", typeof(int));

            for (int i = 0; i < plan.Tables[0].Rows.Count; i++)
            {
                DataRow dr = reserved_plan.NewRow();
                dr[0] = Convert.ToInt32(plan.Tables[0].Rows[i]["NumberOfparker"].ToString());
                dr[1] = Convert.ToDecimal(plan.Tables[0].Rows[i]["MonthlyRate"].ToString());
                dr[2] = plan.Tables[0].Rows[i]["PlanName"].ToString();


                DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan.Tables[0].Rows[i]["Planid"].ToString()));
                dr[3] = Convert.ToDecimal(registration.Tables[0].Rows[0][0].ToString());



                reserved_plan.Rows.Add(dr);
            }

            Repeater1.DataSource = reserved_plan;
            Repeater1.DataBind();
        }
        catch
        {
        }
    }
    public void randomalotedspace()
    {
        try
        {
            DataSet plan1 = new clsInsert().fetchrec("  select tbl_CorporateRandomParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateRandomParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateRandomParkingDetails.Planid where ParkingType=2  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");



            DataTable random_plan = new DataTable();

            random_plan.Columns.Add("Total_Parker", typeof(int));
            random_plan.Columns.Add("monthlyrate", typeof(decimal));
            random_plan.Columns.Add("planname", typeof(string));
            random_plan.Columns.Add("total_regis_parker", typeof(int));

            for (int i = 0; i < plan1.Tables[0].Rows.Count; i++)
            {
                DataRow dr = random_plan.NewRow();
                dr[0] = Convert.ToInt32(plan1.Tables[0].Rows[i]["NumberOfparker"].ToString());
                dr[1] = Convert.ToDecimal(plan1.Tables[0].Rows[i]["MonthlyRate"].ToString());
                dr[2] = plan1.Tables[0].Rows[i]["PlanName"].ToString();


                DataSet registration1 = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan1.Tables[0].Rows[i]["Planid"].ToString()));
                dr[3] = Convert.ToDecimal(registration1.Tables[0].Rows[0][0].ToString());



                random_plan.Rows.Add(dr);
            }

            Repeater2.DataSource = random_plan;
            Repeater2.DataBind();
        }
        catch
        {
        }
    }
    public void employeedetails()
    {
        try
        {
            DataSet mainhead = new clsInsert().fetchrec("select UserName from tbl_ParkerRegis where UserName='" + txtBox_username.Text + "'");
            DataSet emp_res = new clsInsert().fetchrec("select tbl_CorporateEmployeeDetails.*,tbl_CorporateLotRatePlan.ParkingType from tbl_CorporateEmployeeDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateEmployeeDetails.PlanRateID where CorporationID='" + mainhead.Tables[0].Rows[0][0].ToString() + "'and tbl_CorporateLotRatePlan.ParkingType=1");

            DataTable empshow_reserved = new DataTable();

            empshow_reserved.Columns.Add("empcode", typeof(string));
            empshow_reserved.Columns.Add("emp_name", typeof(string));
            empshow_reserved.Columns.Add("emp_emailid", typeof(string));
            empshow_reserved.Columns.Add("empid", typeof(string));

            for (int i = 0; i < emp_res.Tables[0].Rows.Count; i++)
            {
                DataRow dr = empshow_reserved.NewRow();
                dr[0] = emp_res.Tables[0].Rows[i][7].ToString();
                dr[1] = emp_res.Tables[0].Rows[i][4].ToString();
                dr[2] = emp_res.Tables[0].Rows[i][3].ToString();
                dr[3] = emp_res.Tables[0].Rows[i][0].ToString();

                empshow_reserved.Rows.Add(dr);
            }
            if (empshow_reserved.Rows.Count > 0)
            {
                Panel1.Visible = true;
                rpt_reserved.DataSource = empshow_reserved;
                rpt_reserved.DataBind();
            }

            DataSet emp_ran = new clsInsert().fetchrec("select tbl_CorporateEmployeeDetails.*,tbl_CorporateLotRatePlan.ParkingType from tbl_CorporateEmployeeDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateEmployeeDetails.PlanRateID where CorporationID='" + mainhead.Tables[0].Rows[0][0].ToString() + "'and tbl_CorporateLotRatePlan.ParkingType=2");

            DataTable empshow_random = new DataTable();

            empshow_random.Columns.Add("empcode", typeof(string));
            empshow_random.Columns.Add("emp_name", typeof(string));
            empshow_random.Columns.Add("emp_emailid", typeof(string));
            empshow_random.Columns.Add("empid", typeof(string));

            for (int i = 0; i < emp_ran.Tables[0].Rows.Count; i++)
            {
                DataRow dr = empshow_random.NewRow();
                dr[0] = emp_ran.Tables[0].Rows[i][7].ToString();
                dr[1] = emp_ran.Tables[0].Rows[i][4].ToString();
                dr[2] = emp_ran.Tables[0].Rows[i][3].ToString();
                dr[3] = emp_ran.Tables[0].Rows[i][0].ToString();

                empshow_random.Rows.Add(dr);
            }
            if (empshow_random.Rows.Count > 0)
            {
                Panel2.Visible = true;
                rpt_random.DataSource = empshow_random;
                rpt_random.DataBind();
            }

        }
        catch
        {
        }
    }
    protected void btn_next_Click(object sender, EventArgs e)
    {
        try
        {
         
            updatedetails();
          //  TabContainer1.Tabs[1].Enabled = true;
            TabContainer1.Tabs[1].Enabled = true;
            TabContainer1.ActiveTabIndex = 1;
          //  TabContainer1.Tabs[0].Enabled = false;

            //reserced Plan Show


            //DataSet plan = new clsInsert().fetchrec("select tbl_CorporateReservedParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateReservedParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateReservedParkingDetails.Planid where ParkingType=1  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");

       

            //DataTable reserved_plan = new DataTable();

            //reserved_plan.Columns.Add("Total_Parker", typeof(int));
            //reserved_plan.Columns.Add("monthlyrate", typeof(decimal));
            //reserved_plan.Columns.Add("planname", typeof(string));
            //reserved_plan.Columns.Add("total_regis_parker", typeof(int));
           
            //for (int i = 0; i < plan.Tables[0].Rows.Count; i++)
            //{
            //    DataRow dr = reserved_plan.NewRow();
            //    dr[0] = Convert.ToInt32(plan.Tables[0].Rows[i]["NumberOfparker"].ToString());
            //    dr[1] = Convert.ToDecimal(plan.Tables[0].Rows[i]["MonthlyRate"].ToString());
            //    dr[2] = plan.Tables[0].Rows[i]["PlanName"].ToString();

               
            //    DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan.Tables[0].Rows[i]["Planid"].ToString()));
            //    dr[3] = Convert.ToDecimal(registration.Tables[0].Rows[0][0].ToString());
                
              
              
            //    reserved_plan.Rows.Add(dr);
            //}

            //Repeater1.DataSource = reserved_plan;
            //Repeater1.DataBind();

            //Random Plan Show



            //DataSet plan1 = new clsInsert().fetchrec("  select tbl_CorporateRandomParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateRandomParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateRandomParkingDetails.Planid where ParkingType=2  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");



            //DataTable random_plan = new DataTable();

            //random_plan.Columns.Add("Total_Parker", typeof(int));
            //random_plan.Columns.Add("monthlyrate", typeof(decimal));
            //random_plan.Columns.Add("planname", typeof(string));
            //random_plan.Columns.Add("total_regis_parker", typeof(int));

            //for (int i = 0; i < plan1.Tables[0].Rows.Count; i++)
            //{
            //    DataRow dr = random_plan.NewRow();
            //    dr[0] = Convert.ToInt32(plan1.Tables[0].Rows[i]["NumberOfparker"].ToString());
            //    dr[1] = Convert.ToDecimal(plan1.Tables[0].Rows[i]["MonthlyRate"].ToString());
            //    dr[2] = plan1.Tables[0].Rows[i]["PlanName"].ToString();


            //    DataSet registration1 = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan1.Tables[0].Rows[i]["Planid"].ToString()));
            //    dr[3] = Convert.ToDecimal(registration1.Tables[0].Rows[0][0].ToString());



            //    random_plan.Rows.Add(dr);
            //}

            //Repeater2.DataSource = random_plan;
            //Repeater2.DataBind();







            //Employee Detail Show In Repeater



            //DataSet mainhead = new clsInsert().fetchrec("select UserName from tbl_ParkerRegis where UserName='" + txtBox_username.Text + "'");
            //DataSet emp_res = new clsInsert().fetchrec("select tbl_CorporateEmployeeDetails.*,tbl_CorporateLotRatePlan.ParkingType from tbl_CorporateEmployeeDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateEmployeeDetails.PlanRateID where CorporationID='"+mainhead.Tables[0].Rows[0][0].ToString()+"'and tbl_CorporateLotRatePlan.ParkingType=1");

            //DataTable empshow_reserved = new DataTable();

            //empshow_reserved.Columns.Add("empcode", typeof(string));
            //empshow_reserved.Columns.Add("emp_name", typeof(string));
            //empshow_reserved.Columns.Add("emp_emailid", typeof(string));
            //empshow_reserved.Columns.Add("empid", typeof(string));

            //for (int i = 0; i < emp_res.Tables[0].Rows.Count; i++)
            //{
            //    DataRow dr = empshow_reserved.NewRow();
            //    dr[0] = emp_res.Tables[0].Rows[i][7].ToString();
            //    dr[1] = emp_res.Tables[0].Rows[i][4].ToString();
            //    dr[2] = emp_res.Tables[0].Rows[i][3].ToString();
            //    dr[3] = emp_res.Tables[0].Rows[i][0].ToString();

            //    empshow_reserved.Rows.Add(dr);
            //}
            //if (empshow_reserved.Rows.Count > 0)
            //{
            //    Panel1.Visible = true;
            //    rpt_reserved.DataSource = empshow_reserved;
            //    rpt_reserved.DataBind();
            //}

            //DataSet emp_ran = new clsInsert().fetchrec("select tbl_CorporateEmployeeDetails.*,tbl_CorporateLotRatePlan.ParkingType from tbl_CorporateEmployeeDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateEmployeeDetails.PlanRateID where CorporationID='" + mainhead.Tables[0].Rows[0][0].ToString() + "'and tbl_CorporateLotRatePlan.ParkingType=2");

            //DataTable empshow_random = new DataTable();

            //empshow_random.Columns.Add("empcode", typeof(string));
            //empshow_random.Columns.Add("emp_name", typeof(string));
            //empshow_random.Columns.Add("emp_emailid", typeof(string));
            //empshow_random.Columns.Add("empid", typeof(string));

            //for (int i = 0; i < emp_ran.Tables[0].Rows.Count; i++)
            //{
            //    DataRow dr = empshow_random.NewRow();
            //    dr[0] = emp_ran.Tables[0].Rows[i][7].ToString();
            //    dr[1] = emp_ran.Tables[0].Rows[i][4].ToString();
            //    dr[2] = emp_ran.Tables[0].Rows[i][3].ToString();
            //    dr[3] = emp_ran.Tables[0].Rows[i][0].ToString();

            //    empshow_random.Rows.Add(dr);
            //}
            //if (empshow_random.Rows.Count > 0)
            //{
            //    Panel2.Visible = true;
            //    rpt_random.DataSource = empshow_random;
            //    rpt_random.DataBind();
            //}




        }
        catch
        {

        }
    }
    protected void btn_finish_corporate_Click(object sender, EventArgs e)
    {
       
        try
        {
            updatedetails();
        }
        catch
        {

        }

        try
        {
        //    if (TextBox1.Text.Trim() != "" || txtbox_random_numberofparker.Text.Trim() != "")
        //    {
                if (Session["Chargetable"] != null || Session["ChargetableRandom"] != null)
                {
                    try
                    {
                        insertrec_corporate_billingdetails();
                    }
                    catch
                    {
                    }
                    try
                    {
                        insertrec_corporate_spacedetails();
                    }
                    catch
                    {
                    }
                    try
                    {
                        changerandom();
                    }
                    catch
                    {
                    }


                    try
                    {
                        changereserved();
                    }
                    catch
                    {
                    }

                    try
                    {
                        insertdetail();
                    }
                    catch
                    {
                    }

                    Session["check"] = "1";
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_FillDetail_Of_Coporate_Employee.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_FillDetail_Of_Coporate_Employee.aspx");

                    }
                //}
                //else
                //{
                //    if (Session["Type"].ToString() == "admin")
                //    {
                //        Response.Redirect("Admin_Search_Corporate_Parker.aspx");
                //    }
                //    if (Session["Type"].ToString() == "operator")
                //    {
                //        Response.Redirect("Operator_Search_Corporate_Parker.aspx");

                //    }
                   
                //}
            }
            else
            {
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_Search_Corporate_Parker.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_Search_Corporate_Parker.aspx");

                }
            }
        }
        catch
        {
        }


    }
    int parkingtype = 0;
    int invoicetype_corporate;
    string customername = "";
    string bankaccounttype = "";
    string bankname = "";
    string branch = "";
    string bank_zip = "";
    string bank_city = "";
    string state_bank = "";
    string routingno = "";
    string bankaccountno = "";
    decimal total_randommoney = Convert.ToDecimal(0.00);
    decimal total_resrvedmoney = Convert.ToDecimal(0.00);
    decimal activationcharge = Convert.ToDecimal(0.00);
    decimal carddeliverycharges = Convert.ToDecimal(0.00);
    decimal taxcharge = Convert.ToDecimal(0.00);
    decimal depositamount = Convert.ToDecimal(0.00);

    int totalspace;
    int reservedspace;
    int randomsapce;
    decimal defaultvalue = Convert.ToDecimal(0.00);
    DataTable dt_reserveddetails;
    DataTable dt_randomdetails;
    int planid_random;
    int planid_reserved;
    int totalparker_random = 0;
    int totalparker_reserved = 0;



    public void insertdetail()
    {


        try
        {

            if (Session["reserved"] != null)
            {
                dtnewreserved = (DataTable)Session["reserved"];
            }
        }
        catch
        {
        }

        try
        {

            if (Session["random"] != null)
            {
                dtnew = (DataTable)Session["random"];
            }
        }
        catch
        {

        }
        try
        {
            if (dtnewreserved.Rows.Count > 0)
            {
                for (int i = 0; i < dtnewreserved.Rows.Count; i++)
                {
                    hst.Clear();
                    hst.Add("action", "reservedspaceinsert");

                    hst.Add("numberofparker_reserved", dtnewreserved.Rows[i]["numberofparker"].ToString());
                    hst.Add("planid_reserved", dtnewreserved.Rows[i]["planid"].ToString());
                    hst.Add("parkerusername_reserved", txtBox_username.Text);

                    int result_reservedspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
                    if (result_reservedspace > 0)
                    {

                        //Response.Redirect("UserManager.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }
            }
        }
        catch
        {
        }

        try
        {
            if (dtnew.Rows.Count > 0)
            {
                for (int i = 0; i < dtnew.Rows.Count; i++)
                {
                    hst.Clear();
                    hst.Add("action", "randomspaceinsert");

                    hst.Add("numberofparker_random", dtnew.Rows[i]["numberofparker"].ToString());
                    hst.Add("planid_random", dtnew.Rows[i]["planid"].ToString());
                    hst.Add("parkerusername_random", txtBox_username.Text);

                    int result_randomspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
                    if (result_randomspace > 0)
                    {

                        //Response.Redirect("UserManager.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }
            }
        }
        catch
        {
        }
    }
    public void insertrec_corporate_spacedetails()
    {
        //try
        //{

        //    if (Session["Chargetable"] != null)
        //    {
        //        dt_reserveddetails = (DataTable)Session["Chargetable"];
        //    }
        //}
        //catch
        //{
        //}

        //try
        //{

        //    if (Session["ChargetableRandom"] != null)
        //    {
        //        dt_randomdetails = (DataTable)Session["ChargetableRandom"];
        //    }
        //}
        //catch
        //{

        //}
        //if (TextBox1.Text != "")
        //{
        //    reservedspace = Convert.ToInt32(TextBox1.Text);
        //}
        //if (TextBox1.Text.Trim() == "")
        //{
        //    reservedspace = Convert.ToInt32("0");

        //}

        //if (txtbox_totalrandomspace.Text != "")
        //{
        //    randomsapce = Convert.ToInt32(txtbox_totalrandomspace.Text);
        //}
        //if (txtbox_totalrandomspace.Text.Trim() == "")
        //{
        //    randomsapce = Convert.ToInt32("0");

        //}
        //DataSet ds_take=new clsInsert().fetchrec("select * from tbl_CoporateTotalSpaceOccupied where Parkerusername='"+txtBox_username.Text+"'");
        //int reservedspaceinsert = Convert.ToInt32(ds_take.Tables[0].Rows[0]["ReservedSpace"].ToString())+reservedspace;
        //int randomspaceinsert = Convert.ToInt32(ds_take.Tables[0].Rows[0]["RandomSpace"].ToString())+randomsapce;
        //int totalspaceinsert = Convert.ToInt32(ds_take.Tables[0].Rows[0][2].ToString());

        //totalspace =totalspaceinsert+ reservedspace + randomsapce;
        //int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());
        //try
        //{
        //    hst.Clear();

        //    hst.Add("action", "updatetotalspaceinsert");
        //    hst.Add("parkerusername", txtBox_username.Text);
        //    hst.Add("randomspace", Convert.ToDecimal(randomspaceinsert));
        //    hst.Add("totalspace", totalspace);
        //    hst.Add("reservedspace ", reservedspaceinsert);
         


        //    int result_totalspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
        //    if (result_totalspace > 0)
        //    {
        //        try
        //        {
        //            if (dt_reserveddetails.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dt_reserveddetails.Rows.Count; i++)
        //                {
        //                    hst.Clear();
        //                    hst.Add("action", "reservedspaceinsert");

        //                    hst.Add("numberofparker_reserved", dt_reserveddetails.Rows[i]["Total_Parker"].ToString());
        //                    hst.Add("planid_reserved", dt_reserveddetails.Rows[i]["Planid"].ToString());
        //                    hst.Add("parkerusername_reserved", txtBox_username.Text);

        //                    int result_reservedspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
        //                    if (result_reservedspace > 0)
        //                    {

        //                        //Response.Redirect("UserManager.aspx");

        //                    }
        //                    else
        //                    {
        //                        // lbl_error.Visible = true;

        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {
        //        }

        //        try
        //        {
        //            if (dt_randomdetails.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dt_randomdetails.Rows.Count; i++)
        //                {
        //                    hst.Clear();
        //                    hst.Add("action", "randomspaceinsert");

        //                    hst.Add("numberofparker_random", dt_randomdetails.Rows[i]["Total_Parker"].ToString());
        //                    hst.Add("planid_random", dt_randomdetails.Rows[i]["Planid"].ToString());
        //                    hst.Add("parkerusername_random", txtBox_username.Text);

        //                    int result_randomspace = objData.ExecuteNonQuery("[sp_CorporateSpaceManage]", CommandType.StoredProcedure, hst);
        //                    if (result_randomspace > 0)
        //                    {

        //                        //Response.Redirect("UserManager.aspx");

        //                    }
        //                    else
        //                    {
        //                        // lbl_error.Visible = true;

        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {
        //        }
        //    }

        //}
        //catch
        //{

        //}

    }
    public void insertrec_corporate_billingdetails()
    {



//        int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());

//        //string deposite = "false";


//        try
//        {
//            hst.Clear();


//            DataSet dt_activationcharge = new clsInsert().fetchrec("select activationcharge from tbl_lotmaster where lot_id=" + LOTIDForInserting);
//            if (dt_activationcharge.Tables[0].Rows.Count > 0)
//            {
//                activationcharge = Convert.ToDecimal(dt_activationcharge.Tables[0].Rows[0]["activationcharge"].ToString());
//            }
//            else
//            {
//                activationcharge = Convert.ToDecimal("0.00");

//            }
//           // DataSet dt_carddeliverycharge = new clsInsert().fetchrec(" select charges  from tbl_CardDeliveryMaster where carddeliveryid=" + ddl_deliverymethod_corporate.SelectedValue);
//            if (Label3.Text!="0.00")
//            {
//                carddeliverycharges = Convert.ToDecimal(Label3.Text);

//            }
//            else
//            {
//                carddeliverycharges = Convert.ToDecimal("0.00");

//            }
//            DataSet dt_taxcharge = new clsInsert().fetchrec("  select TAxrate from tbl_lottaxmaster where sequenceofcalc=1 and lotid=" + LOTIDForInserting);
//            if (dt_taxcharge.Tables[0].Rows.Count > 0)
//            {
//                taxcharge = Convert.ToDecimal(dt_taxcharge.Tables[0].Rows[0]["TAxrate"].ToString());

//            }
//            else
//            {
//                taxcharge = Convert.ToDecimal("0.00");

//            }

//            if (lbl_alltotal.Text != "0")
//            {
//                total_resrvedmoney = Convert.ToDecimal(lbl_alltotal.Text);
//            }
//            if (lbl_alltotal.Text == "0")
//            {
//                total_resrvedmoney = Convert.ToDecimal("0.00");
//            }
//            if (lbl_alltotalRandome.Text != "0")
//            {
//                total_randommoney = Convert.ToDecimal(lbl_alltotalRandome.Text);
//            }
//            if (lbl_alltotalRandome.Text == "0")
//            {
//                total_randommoney = Convert.ToDecimal("0.00");
//            }


////            update tbl_CorporateParkerBilling set TotalAmount=@totalamount,DepositAmount=@depositamount,DueAmount=@dueamount,Transactionid=@transactionid
////where Parker_username=@Parkerusername

//            DataSet dstotalbill = new clsInsert().fetchrec("select * from tbl_CorporateParkerBilling where Parker_username='"+txtBox_username.Text+"'");

//            decimal grandtotal =Convert.ToDecimal(dstotalbill.Tables[0].Rows[0]["TotalAmount"].ToString())+ total_randommoney + total_resrvedmoney + activationcharge + carddeliverycharges + taxcharge;

//            hst.Add("action", "update_billing_corporate");
//            hst.Add("Parkerusername", txtBox_username.Text);


//            hst.Add("totalamount", Convert.ToDecimal(grandtotal));
//            hst.Add("depositamount", Convert.ToDecimal(dstotalbill.Tables[0].Rows[0]["DepositAmount"].ToString()));
//            decimal dueamount = Convert.ToDecimal(grandtotal) - Convert.ToDecimal(dstotalbill.Tables[0].Rows[0]["DepositAmount"].ToString());
//            hst.Add("dueamount ", dueamount);

//            hst.Add("transactionid", parkingtype);
          


//            int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
//            if (result_billing > 0)
//            {
//                //sp_billingfullinfo
//                hst.Clear();
//                DataSet ds2 = new clsInsert().select_operation("select ID from tbl_CorporateParkerBilling where Parker_userName='" + txtBox_username.Text + "'order by  ID desc");
//                //@billid,@billtitle,@amountparticular
//                //fulldetailsaboutbill
//                int lastid_bill = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
//                if (Label3.Text != "0.00")
//                {

//                    hst.Add("action", "fulldetailsaboutbill");
//                    hst.Add("billid", lastid_bill);
//                    hst.Add("billtitle", "Card Delivery Charges");
//                    hst.Add("amountparticular", Convert.ToDecimal(Label3.Text.ToString()));
//                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
//                    if (result_billing_full > 0)
//                    {
//                    }
//                }
//                if (dt_activationcharge.Tables[0].Rows[0]["activationcharge"].ToString() != "0.00")
//                {
//                    hst.Clear();
//                    hst.Add("action", "fulldetailsaboutbill");
//                    hst.Add("billid", lastid_bill);
//                    hst.Add("billtitle", "Activation Charges");
//                    hst.Add("amountparticular", Convert.ToDecimal(dt_activationcharge.Tables[0].Rows[0]["activationcharge"].ToString()));
//                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
//                    if (result_billing_full > 0)
//                    {
//                    }
//                }
//                if (lbl_alltotal.Text != "0")
//                {
//                    hst.Clear();
//                    hst.Add("action", "fulldetailsaboutbill");
//                    hst.Add("billid", lastid_bill);
//                    hst.Add("billtitle", "Reserved Parking Charges");
//                    hst.Add("amountparticular", Convert.ToDecimal(lbl_alltotal.Text));
//                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
//                    if (result_billing_full > 0)
//                    {
//                    }
//                }

//                if (lbl_alltotalRandome.Text != "0")
//                {
//                    hst.Clear();
//                    hst.Add("action", "fulldetailsaboutbill");
//                    hst.Add("billid", lastid_bill);
//                    hst.Add("billtitle", "Random Parking Charges");
//                    hst.Add("amountparticular", Convert.ToDecimal(lbl_alltotalRandome.Text));
//                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
//                    if (result_billing_full > 0)
//                    {
//                    }
//                }
//                if (Convert.ToString(taxcharge) != "0.00")
//                {
//                    hst.Clear();
//                    hst.Add("action", "fulldetailsaboutbill");
//                    hst.Add("billid", lastid_bill);
//                    hst.Add("billtitle", "Tax Charges");
//                    hst.Add("amountparticular", Convert.ToDecimal(taxcharge));
//                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
//                    if (result_billing_full > 0)
//                    {
//                    }
//                }
//            }

//        }
//        catch
//        {

//        }

    }
    public void changerandom()
    {
        DataSet ds = new clsInsert().fetchrec("select distinct planid from tbl_CorporateRandomParkingDetails where Parkerusername='"+txtBox_username.Text+"'");
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            DataSet dsplan = new clsInsert().fetchrec("select * from tbl_CorporateRandomParkingDetails where Parkerusername='" + txtBox_username.Text + "' and  Planid=" + Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
            for (int j = 0; j < dsplan.Tables[0].Rows.Count; j++)
            {
                planid_random = Convert.ToInt32(dsplan.Tables[0].Rows[0]["Planid"].ToString());
                totalparker_random = totalparker_random + Convert.ToInt32(dsplan.Tables[0].Rows[j]["NumberOfParker"].ToString());

            }
            dtnew = (DataTable)Session["random"];
            delplanrandom(Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
            DataRow dr = dtnew.NewRow();
            dr[0] = txtBox_username.Text;
            dr[1] = planid_random;
            dr[2] = totalparker_random;
            dtnew.Rows.Add(dr);
            totalparker_random = 0;
            Session["random"] = dtnew;

        }

    }
    public void changereserved()
    {
        DataSet ds = new clsInsert().fetchrec("select distinct planid from tbl_CorporateReservedParkingDetails where Parkerusername='" + txtBox_username.Text + "'");
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            DataSet dsplan = new clsInsert().fetchrec("select * from tbl_CorporateReservedParkingDetails where Parkerusername='" + txtBox_username.Text + "' and  Planid=" + Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
            for (int j = 0; j < dsplan.Tables[0].Rows.Count; j++)
            {
                planid_reserved = Convert.ToInt32(dsplan.Tables[0].Rows[0]["Planid"].ToString());
                totalparker_reserved = totalparker_reserved + Convert.ToInt32(dsplan.Tables[0].Rows[j]["NumberOfParker"].ToString());

            }
            dtnewreserved = (DataTable)Session["reserved"];
            delplanreserved(Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString()));
            DataRow dr = dtnewreserved.NewRow();
            dr[0] = txtBox_username.Text;
            dr[1] = planid_reserved;
            dr[2] = totalparker_reserved;
            dtnewreserved.Rows.Add(dr);
            totalparker_reserved = 0;
            Session["reserved"] = dtnewreserved;

        }

    }
    public void delplanreserved(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "deletereserved");

            hst.Add("username", txtBox_username.Text);
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_try]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {



            }
            else
            {



            }
        }
        catch
        {




        }


    }
    public void delplanrandom(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "deleterandom");

            hst.Add("username", txtBox_username.Text);
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_try]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {



            }
            else
            {



            }
        }
        catch
        {




        }


    }
    protected void btn_edit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditOf Employee_Details.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {

                Response.Redirect("Operator_EditOf Employee_Details.aspx");

            }
        }
        catch
        {
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

    }
    DataTable res = new DataTable();
    int number_totalreseved = 0;
    int number_totalrandom = 0;
    //int regis_reserved = 0;
    int regis_random = 0;
    protected void ddl_plantype_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddl_plantype.SelectedItem.Value == "0")
            {

                lbl_plan.Visible = false;
                ddl_plan.Visible = false;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('PLease Select Plan')", true);


            }

            if (ddl_plantype.SelectedItem.Value == "1")
            {


                DataSet plan = new clsInsert().fetchrec("select tbl_CorporateReservedParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateReservedParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateReservedParkingDetails.Planid where ParkingType=1  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");

                for (int l = 0; l < plan.Tables[0].Rows.Count; l++)
                {
                    total_reserved_parker = total_reserved_parker + Convert.ToInt32(plan.Tables[0].Rows[l][1].ToString());
                    for (int r = l; r <= l; r++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan.Tables[0].Rows[l]["Planid"].ToString()));
                        regis_reserved = regis_reserved + Convert.ToInt32(registration.Tables[0].Rows[0][0].ToString());
                    }
                }
                int exist = total_reserved_parker - regis_reserved;
                if (exist != 0)
                {

                    res.Columns.Add("planid", typeof(int));

                    res.Columns.Add("total_p", typeof(int));
                    res.Columns.Add("regi", typeof(int));


                    DataSet total_plan = new clsInsert().fetchrec("select tbl_CorporateReservedParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateReservedParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateReservedParkingDetails.Planid where ParkingType=1  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");


                    //  res = (DataTable)Session["res"];
                    for (int i = 0; i < total_plan.Tables[0].Rows.Count; i++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString()));
                        DataRow dr = res.NewRow();
                        dr[0] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString());
                        dr[1] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Numberofparker"].ToString());
                        dr[2] = Convert.ToDecimal(registration.Tables[0].Rows[0][0].ToString());

                        res.Rows.Add(dr);
                    }
                    //Session["res"] = res;

                    for (int j = 0; j < res.Rows.Count; j++)
                    {
                        if (Convert.ToInt32(res.Rows[j][1].ToString()) != Convert.ToInt32(res.Rows[j][2].ToString()))
                        {
                            lbl_plan.Text = res.Rows[j][0].ToString();
                            break;
                        }
                    }


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Registration Is Completed In Reserved Parking')", true);
                    ddl_plantype.SelectedValue = "0";

                }





            }

            if (ddl_plantype.SelectedItem.Value == "2")
            {

               

                DataSet plan = new clsInsert().fetchrec("select tbl_CorporateRandomParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateRandomParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateRandomParkingDetails.Planid where ParkingType=2  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");

                for (int l = 0; l < plan.Tables[0].Rows.Count; l++)
                {
                    number_totalrandom = number_totalrandom + Convert.ToInt32(plan.Tables[0].Rows[l][1].ToString());
                    for (int r = l; r <= l; r++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(plan.Tables[0].Rows[l]["Planid"].ToString()));
                        regis_random = regis_random + Convert.ToInt32(registration.Tables[0].Rows[0][0].ToString());
                    }
                }
                int exist = number_totalrandom - regis_random;
                if (exist != 0)
                {
                    res.Columns.Add("planid", typeof(int));

                    res.Columns.Add("total_p", typeof(int));
                    res.Columns.Add("regi", typeof(int));


                    DataSet total_plan = new clsInsert().fetchrec("select tbl_CorporateRandomParkingDetails.*,tbl_CorporateLotRatePlan.* from tbl_CorporateRandomParkingDetails  inner join tbl_CorporateLotRatePlan on tbl_CorporateLotRatePlan.PlanID_Corporate=tbl_CorporateRandomParkingDetails.Planid where ParkingType=2  and tbl_CorporateLotRatePlan.Corporateusername='" + txtBox_username.Text + "' order by tbl_CorporateLotRatePlan.MonthlyRate ");


                    //  res = (DataTable)Session["res"];
                    for (int i = 0; i < total_plan.Tables[0].Rows.Count; i++)
                    {
                        DataSet registration = new clsInsert().fetchrec("select COUNT(*) from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString()));
                        DataRow dr = res.NewRow();
                        dr[0] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Planid"].ToString());
                        dr[1] = Convert.ToInt32(total_plan.Tables[0].Rows[i]["Numberofparker"].ToString());
                        dr[2] = Convert.ToDecimal(registration.Tables[0].Rows[0][0].ToString());

                        res.Rows.Add(dr);
                    }
                    //Session["res"] = res;

                    for (int j = 0; j < res.Rows.Count; j++)
                    {
                        if (Convert.ToInt32(res.Rows[j][1].ToString()) != Convert.ToInt32(res.Rows[j][2].ToString()))
                        {
                            lbl_plan.Text = res.Rows[j][0].ToString();
                            break;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Registration Is Completed In Random Parking')", true);
                    ddl_plantype.SelectedValue = "0";

                }
            }
          
        }
        catch
        {
        }
    }
    protected void ddl_plan_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }
    protected void txtboxcode_employeecode_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataSet mainhead = new clsInsert().fetchrec("select UserName from tbl_ParkerRegis where UserName='" + txtBox_username.Text + "'");
            DataSet codereturn = new clsInsert().fetchrec("select EmployeeCode from tbl_CorporateEmployeeDetails where  CorporationID='" + mainhead.Tables[0].Rows[0][0].ToString() + "'and EmployeeCode='" + txtboxcode_employeecode.Text + "'");
            if (codereturn.Tables[0].Rows.Count > 0)
            {
                lblerror.Visible = true;
                lblerror.Text = "Already Exist... Try Another";
                txtboxcode_employeecode.Text = "";
            }
            else
            {
                lblerror.Visible = false;
                lblerror.Text = "";
            }
        }
        catch
        {
        }
    }
    decimal activationcharge_reserved = Convert.ToDecimal("0.00");
    decimal carddeliverycharges_reserved = Convert.ToDecimal("0.00");
    decimal monthlycharges_reserved = Convert.ToDecimal("0.00");
    string billingtypeentry = "";
    int dateformatch = 15;
    string invoicenumber = "";
    decimal monthly_totalcharge = Convert.ToDecimal("0.00");
    string status = "true";
    DateTime startdate;
    DateTime endOfMonth;
   // decimal activatincharge
    DataTable dt_values;
    public void insertrec_corporate_billingdetails_dependonuseofparking()
    {
        
        try
        {
            dt_values = (DataTable)Session["dt_employeecharge"];
            if (dt_values.Rows.Count > 0)
            {
                for (int i = 0; i < dt_values.Rows.Count; i++)
                {
                   // DataSet employeecount = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeDetails where PlanRateID=" + Convert.ToInt32(dsplanid.Tables[0].Rows[i][0].ToString()));

                    activationcharge_reserved = activationcharge_reserved + (Convert.ToDecimal(dt_values.Rows[i][0].ToString()));
                    carddeliverycharges_reserved = carddeliverycharges_reserved + (Convert.ToDecimal(dt_values.Rows[i][2].ToString()));

                    monthlycharges_reserved = monthlycharges_reserved + (Convert.ToDecimal(dt_values.Rows[i][1].ToString()));
                        //  depositcharges_reserved = depositcharges_reserved + Convert.ToDecimal(dsplanid.Tables[0].Rows[i]["deposit"].ToString());
                    
                }
            }
        }
        catch
        {
        }
        if (monthlycharges_reserved>0)
        {
            int LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());

            try
            {


                DataSet ds_billingentrytype = new clsInsert().fetchrec("select Billing_entry from tbl_LotMaster where Lot_id=" + LOTIDForInserting);
                if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "1")
                {
                    billingtypeentry = "Full Charge";
                }

                if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "2")
                {
                    billingtypeentry = "Prorate";

                }

                if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "3")
                {
                    billingtypeentry = "Prorate By Fort Night";

                }




                hst.Clear();
                // dt_charge_Random.Columns.Add("Total_Parker", typeof(int));
                //dt_charge_Random.Columns.Add("monthlyrate", typeof(decimal));
                //dt_charge_Random.Columns.Add("activationfess", typeof(decimal));
                //dt_charge_Random.Columns.Add("deposit", typeof(decimal));
                //dt_charge_Random.Columns.Add("deliveryfees", typeof(decimal));
                //dt_charge_Random.Columns.Add("planname", typeof(string));
                //dt_charge_Random.Columns.Add("Username", typeof(string));
                //dt_charge_Random.Columns.Add("PlanID", typeof(int));--%>



                //activationcharge = activationcharge_reserved;
                //carddeliverycharges = carddeliverycharges_reserved;

                //monthlycharges = monthlycharges_reserved;
                if (billingtypeentry == "Full Charge")
                {
                    monthly_totalcharge = monthlycharges_reserved;
                }
                if (billingtypeentry == "Prorate")
                {
                    DateTime active = DateTime.Now;
                    int y = active.Year;
                    int m = active.Month;
                    int d = active.Day;
                    int days = DateTime.DaysInMonth(y, m);
                    int daysinmonthremaining = days - d;
                    double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days);
                    monthly_totalcharge = monthlycharges_reserved * Convert.ToDecimal(daystocharge);


                }
                if (billingtypeentry == "Prorate By Fort Night")
                {
                    DateTime active = DateTime.Now;
                    int d = active.Day;
                    if (d > dateformatch)
                    {
                        monthly_totalcharge = monthlycharges_reserved;

                    }
                    else
                    {
                        monthly_totalcharge = monthlycharges_reserved / 2;
                    }
                }



                taxcharge = Convert.ToDecimal("0.13");
                decimal subtotal = (activationcharge_reserved + carddeliverycharges_reserved + monthly_totalcharge);
                decimal total = (activationcharge_reserved + carddeliverycharges_reserved + monthly_totalcharge) * taxcharge;



                decimal grandtotal = subtotal + total;

                DateTime activedate = DateTime.Now;
                int year1 = activedate.Year;
                int month1 = activedate.Month;
                string month = "";
                if (month1 < 10)
                {
                    month = "0" + Convert.ToString(month1);
                }
                if (month1 >= 10)
                {
                    month = Convert.ToString(month1);

                }
                string yearmonth = Convert.ToString(year1) + Convert.ToString(month);
                DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
                if (dsinvoice.Tables[0].Rows.Count > 0)
                {
                    string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                    string[] strArr = oFName.Split('_');
                    int sLength = strArr.Length;
                    int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                    if (number < 10)
                    {
                        invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                    }
                    if (number >= 10 && number < 100)
                    {
                        invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                    }
                    if (number >= 100 && number < 1000)
                    {
                        invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                    }
                    if (number >= 1000 && number < 10000)
                    {
                        invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                    }
                }
                else
                {
                    invoicenumber = yearmonth + "_" + "000001";
                }

                DateTime activationdateprevious = Convert.ToDateTime(txtbox_ActicationDate.Text);
                DateTime registerdate = DateTime.Now;
                int number_month = registerdate.Month;
                int m1 = activationdateprevious.Month;
                int diff = number_month - m1;

                if (diff == 0)
                {

                    startdate = Convert.ToDateTime(txtbox_ActicationDate.Text);
                    DateTime today = Convert.ToDateTime(txtbox_ActicationDate.Text);
                    int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                    endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);

                }
                if (diff > 0)
                {
                    startdate = DateTime.Now;
                    DateTime today = DateTime.Now;
                    int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                    endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);

                }
                //@invoicenumber,@startdate,@duedate,@subtotal,@tax,@totalamount,@dueamount,@billingtype,@Parkerusername
                hst.Add("action", "insert_billing_corporate");
                hst.Add("invoicenumber", invoicenumber);

                hst.Add("Parkerusername", txtBox_username.Text);

                hst.Add("startdate", startdate);



                // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
                hst.Add("duedate", endOfMonth);
                hst.Add("subtotal", subtotal);
                hst.Add("tax", Convert.ToDecimal(total));
                hst.Add("details", "Invoice At The Time Of Registration Of New Employees");

                hst.Add("totalamount", Convert.ToDecimal(grandtotal));

                DataSet dt_creditcheck = new clsInsert().fetchrec("select * from tbl_CreditNote where Acct_Num='" + txtBox_username.Text + "' order by CreditNoteID desc");
                if (dt_creditcheck.Tables[0].Rows.Count > 0)
                {
                    //decimal dueamount = Convert.ToDecimal(grandtotal);
                    //hst.Add("dueamount ", dueamount);

                    if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) > grandtotal)
                    {
                        decimal dueamount = Convert.ToDecimal("0.00");
                        hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                    }
                    if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) <= grandtotal)
                    {
                        decimal dueamount = grandtotal - Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString());
                        hst.Add("dueamount ", Convert.ToDecimal(dueamount));

                    }
                }
               
                hst.Add("billingtype", Convert.ToInt32("2"));


                int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                if (result_billing > 0)
                {
                    // Sending Values in tbl_Charge Table

                    hst.Clear();
                    //DataSet ds2 = new clsInsert().select_operation("select Inv_num from TBL_Invoice where Acct_Num='" + txtBox_username.Text + "'order by  ID desc");

                    string lastid_bill = invoicenumber;
                    if (Convert.ToString(carddeliverycharges) != "0.00")
                    {
                        // @billid,@billtitle,@amountparticular,@date,@accountnumber
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Card Delivery Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(carddeliverycharges_reserved));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                    if (Convert.ToString(carddeliverycharges) == "0.00")
                    {
                        // @billid,@billtitle,@amountparticular,@date,@accountnumber
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Card Delivery Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(carddeliverycharges_reserved));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);

                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(activationcharge) != "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Activation Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(activationcharge_reserved));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                    if (Convert.ToString(activationcharge) == "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Activation Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(activationcharge_reserved));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(monthlycharges_reserved) != "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Monthly Parking Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(monthlycharges_reserved) == "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Monthly Parking Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(monthly_totalcharge));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                    if (Convert.ToString(total) != "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Tax Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(total));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Convert.ToString(total) == "0.00")
                    {
                        hst.Clear();
                        hst.Add("action", "fulldetailsaboutbill");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Tax Charges");
                        hst.Add("amountparticular", Convert.ToDecimal(total));
                        hst.Add("date", DateTime.Now);
                        hst.Add("accountnumber", txtBox_username.Text);
                        int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }

                   
                    //For Bill
                    lbl_invoicenumber.Text = invoicenumber;
                    // Sending Values in Payment Table

                    hst.Clear();
                    hst.Add("action", "insert");
                    hst.Add("acctnumber", txtBox_username.Text);
                    hst.Add("reason", "Advance");
                    if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) > grandtotal)
                    {
                        decimal dueamount = Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) - grandtotal;

                        // hst.Add("dueamount ", Convert.ToDecimal(dueamount));
                        hst.Add("creditamt", Convert.ToDecimal(dueamount));

                    }
                    if (Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString()) <= grandtotal)
                    {
                        //decimal dueamount = totalamount - Convert.ToDecimal(dt_creditcheck.Tables[0].Rows[0][2].ToString());
                        hst.Add("creditamt ", Convert.ToDecimal("0.00"));

                    }
                    hst.Add("dateoftransaction", DateTime.Now);
                    hst.Add("status", status);

                    int result_billing_full11 = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                    {
                    }






                }

            }
            catch
            {

            }
        }

        else
        {
            lbl_invoicenumber.Text = "";
        }

    }
    protected void btnclose1_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtbox_checkcounter.Text == "Pay Per Use")
            {
                try
                {
                    insertrec_corporate_billingdetails_dependonuseofparking();
                }
                catch
                {
                }
            }
            Session["empshow"] = null;
            Session["Parker_id"] = null;

            Session["Chargetable"] = null;

            Session["ChargetableRandom"] = null;
            Session["password"] = null;
            Session["lotid"] = null;
            Session["Billusername"] = txtBox_username.Text;
            if (lbl_invoicenumber.Text != "")
            {
                Session["InvoiceNumber"] = lbl_invoicenumber.Text;
            }
            else
            {
                Session["InvoiceNumber"] = "Invoice Not Generated";

            }
            if (Session["Type"].ToString() == "admin")
            {
                Session["registersucceess"] = "Update";
                Response.Redirect("Admin_Search_Corporate_Parker.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                Session["registersucceess"] = "Update";
                Response.Redirect("Operator_Search_Corporate_Parker.aspx");

            }
            if (Session["Type"].ToString() == "Corporate")
            {
                Session["registersucceess"] = "Update";
                Response.Redirect("Deshboard.aspx");

            }
           // Response.Redirect("Admin_Search_Corporate_Parker.aspx");



        }
        catch
        {
        }
    }
    int Planid = 0;
    
    DataTable empshow = new DataTable();
    public void insertrec_parkerdetail()
    {

        try
        {
            // Planid = Convert.ToInt32(ddl_plan.SelectedValue);

            Planid = Convert.ToInt32(lbl_plan.Text);
        }
        catch
        {
        }


        try
        {
            hst.Clear();
            // DataSet   dscorporationid = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='" + txtBox_username.Text + "'");
            //@CorporationID,@planrateID,@emailid,@fname,@address1,@cell,@EmployeeCode,@dateofregis)
            hst.Add("action", "insert");
            hst.Add("CorporationID", txtBox_username.Text);
            // hst.Add("CorporationID", Convert.ToInt32(5));

            hst.Add("planrateID", Convert.ToInt32(Planid));
            hst.Add("emailid", txtBox_emp_emailid.Text);

            hst.Add("fname", txtbox_emp_firstname.Text);


            hst.Add("address1", txtBox_emp_address1.Text);



            hst.Add("cell", txtbox_emp_cell.Text);
            hst.Add("EmployeeCode", txtboxcode_employeecode.Text);
            hst.Add("dateofregis", DateTime.Now);






            int result = objData.ExecuteNonQuery("[Sp_employeeDetailsCorporate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {


                int i = 1;

                if (i >= 1)
                {

                    //TabContainer1.Tabs[1].Enabled = true;
                    // DataSet code = new clsInsert().fetchrec("select EmployeeCode,Corporate_EmployeeID from tbl_CorporateEmployeeDetails where CorporationID=" + Convert.ToInt32(5) + "and PlanRateID=" + Planid + "order by Corporate_EmployeeID DESC");

                    DataSet code = new clsInsert().fetchrec("select EmployeeCode,Corporate_EmployeeID from tbl_CorporateEmployeeDetails where CorporationID='" + txtBox_username.Text + "' and PlanRateID=" + Planid + "order by Corporate_EmployeeID DESC");
                    if (code.Tables[0].Rows.Count > 0)
                    {
                        //ddl_emp_code.DataSource = code.Tables[0];
                        //ddl_emp_code.DataTextField = "EmployeeCode";
                        //ddl_emp_code.DataValueField = "Corporate_EmployeeID";
                        //ddl_emp_code.DataBind();
                        Session["Parker_id"] = code.Tables[0].Rows[0]["Corporate_EmployeeID"].ToString();
                    }


                }

                if (txtbox_checkcounter.Text == "Pay Per Use")
                {
                    DataSet plandetails = new clsInsert().fetchrec("select * from tbl_CorporateLotRatePlan where PlanID_Corporate=" + Planid);
                        if(plandetails.Tables[0].Rows.Count>0)
                        {
                            dt_employeecharge=(DataTable)Session["dt_employeecharge"];
                            DataRow drneew=dt_employeecharge.NewRow();
                            drneew[0]=Convert.ToDecimal(plandetails.Tables[0].Rows[0][3].ToString());
                            drneew[1]=Convert.ToDecimal(plandetails.Tables[0].Rows[0][2].ToString());
                            drneew[2]=Convert.ToDecimal(plandetails.Tables[0].Rows[0][4].ToString());
                            dt_employeecharge.Rows.Add(drneew);
                            Session["dt_employeecharge"]=dt_employeecharge;
                        }
                    
                    
                }

            }
        }
        catch
        {

        }





    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            //if (lbl_number.Text != "0")
            //{
            if (ddl_plan.SelectedValue != "0")
            {

                if (lblerror.Visible == false)
                {
                    insertrec_parkerdetail();
                    try
                    {
                        reservedalotedspace();
                    }
                    catch
                    {
                    }
                    try
                    {
                        randomalotedspace();
                    }
                    catch
                    {
                    }
                    try
                    {
                        employeedetails();
                    }
                    catch
                    {
                    }
                    ddl_plan.SelectedValue = "0";
                    ddl_plantype.SelectedValue = "0";
                    // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);

                    TabContainer1.Tabs[2].Visible = true;
                    TabContainer1.Tabs[2].Enabled = true;
                    TabContainer1.ActiveTabIndex = 2;
                    TabContainer1.Tabs[1].Enabled = false;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Fill Correct Employee Code.')", true);

                }
                txtBox_emp_address1.Text = "";
                // txtBox_emp_address2.Text = "";
                txtbox_emp_cell.Text = "";
                txtBox_emp_emailid.Text = "";
                txtbox_emp_firstname.Text = "";
                // txtbox_emp_lastname.Text = "";
                //  txtbox_emp_middlename.Text = "";
                // txtbox_emp_phone.Text = "";
                // txtbox_emp_postal.Text = "";
                txtboxcode_employeecode.Text = "";

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Select The Plan.')", true);

            }

        }
        catch
        {

        }




       
    }
    //protected void rpt_emp_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    try
    //    {
    //        int i = e.Item.ItemIndex;
    //        if (e.CommandName == "cmdEditdetail")
    //        {
    //            Label empid = rpt_emp.Items[i].FindControl("empid") as Label;
    //            Session["empid_edit"] = empid.Text;
    //            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('EditDetailsOfEmploy.aspx','New Windows','height=1000, width=1400,location=no');", true);




    //        }
    //        if (e.CommandName == "cmdEditvehicle")
    //        {
    //            Label empid = rpt_emp.Items[i].FindControl("empid") as Label;
    //            Session["empid_edit"] = empid.Text;
    //            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Edit_Vehicle_Of_Employee.aspx','New Windows','height=1000, width=1400,location=no');", true);

    //        }




    //        //int i = e.Item.ItemIndex;
    //        //if (e.CommandName == "cmdEditemp")
    //        //{

    //        //    Label l1 = rpt_emp.Items[i].FindControl("empcode") as Label;
    //        //    Session["empcode"] = l1.Text;
             
    //        //    if (Session["Type"].ToString() == "admin")
    //        //    {
    //        //        Response.Redirect("Admin_EditCityMaster.aspx");
    //        //    }
    //        //    if (Session["Type"].ToString() == "operator")
    //        //    {
                
    //        //        Response.Redirect("Operator_EditCityMaster.aspx");
                  
    //        //    }

    //        //}
    //    }
    //    catch
    //    {
    //    }
    //}
    protected void SqlDataSource2_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSource9_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSource10_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            TabContainer1.Tabs[2].Enabled = false;
            TabContainer1.Tabs[1].Enabled = true;
            TabContainer1.ActiveTabIndex = 1;
        }
        catch
        {
        }
    }
    protected void btn_close_Click(object sender, EventArgs e)
    {
        try
        {
            TabContainer1.Tabs[4].Enabled = false;
            TabContainer1.Tabs[3].Enabled = true;
            TabContainer1.ActiveTabIndex = 3;
        }
        catch
        {
        }
    }
    protected void rpt_random_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEditdetail")
        {
            Label empid = rpt_random.Items[i].FindControl("empid") as Label;
            Session["empid_edit"] = empid.Text;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('EditDetailsOfEmploy.aspx','New Windows','height=1000, width=1400,location=no');", true);



        }
        if (e.CommandName == "cmdEditvehicle")
        {
            Label empid = rpt_random.Items[i].FindControl("empid") as Label;
            Session["empid_edit"] = empid.Text;
            Session["Parker_id"] = empid.Text;

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Edit_Vehicle_Of_Employee.aspx','New Windows','height=1000, width=1400,location=no');", true);

        }
    }
    protected void rpt_reserved_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEditdetail")
        {
            Label empid = rpt_reserved.Items[i].FindControl("empid") as Label;
            Session["empid_edit"] = empid.Text;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('EditDetailsOfEmploy.aspx','New Windows','height=1000, width=1400,location=no');", true);




        }
        if (e.CommandName == "cmdEditvehicle")
        {
            Label empid = rpt_reserved.Items[i].FindControl("empid") as Label;
            Session["empid_edit"] = empid.Text;
            Session["Parker_id"] = empid.Text;

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Edit_Vehicle_Of_Employee.aspx','New Windows','height=1000, width=1400,location=no');", true);

        }
    }

    public void bindcountry()
    {
        DataTable dt_country = new clsInsert().GetAllCountry();
        ddl_Parkercountry.DataSource = dt_country;
        //ddl_Parkercountry.DataBind();
        ddl_Parkercountry.DataTextField = "Country_name";
        ddl_Parkercountry.DataValueField = "Country_id";
        ddl_Parkercountry.DataBind();
 
    }
    public void bindstate()
    {
        try
        {
            if (Session["Countryid"] != null)
            {
                DataTable dt_state = new clsInsert().GetAllstate(Convert.ToInt32(Session["Countryid"].ToString()));
                ddlstate.DataSource = dt_state;
                //ddlstate.DataBind();
                ddlstate.DataTextField = "ProvinceName";
                ddlstate.DataValueField = "ProvinceId";
                ddlstate.DataBind();
                //Session["Countryid"] = null;
            }
            if (Session["Countryid"] == null)
            {
                DataTable dt_state = new clsInsert().GetAllstate(Convert.ToInt32(ddl_Parkercountry.SelectedValue));
                ddlstate.DataSource = dt_state;
                //ddlstate.DataBind();
                ddlstate.DataTextField = "ProvinceName";
                ddlstate.DataValueField = "ProvinceId";
                ddlstate.DataBind();
                //Session["Countryid"] = null;
            }
        }
        catch
        {
            DataTable dt_state = new clsInsert().GetAllstate(Convert.ToInt32(ddl_Parkercountry.SelectedValue));
            ddlstate.DataSource = dt_state;
            //ddlstate.DataBind();
            ddlstate.DataTextField = "ProvinceName";
            ddlstate.DataValueField = "ProvinceId";
            ddlstate.DataBind();
        }
    }

    public void bindcity()
    {
        try
        {
            if (Session["Stateid"] != null)
            {
                DataTable dt_city = new clsInsert().GetAllCity(Convert.ToInt32(Session["Stateid"].ToString()));
                ddl_Parkercity.DataSource = dt_city;
                // ddl_Parkercity.DataBind();
                ddl_Parkercity.DataTextField = "City_name";
                ddl_Parkercity.DataValueField = "City_id";
                ddl_Parkercity.DataBind();

            }
            if (Session["Stateid"] == null)
            {
                DataTable dt_city = new clsInsert().GetAllCity(Convert.ToInt32(ddlstate.SelectedValue));
                ddl_Parkercity.DataSource = dt_city;
                // ddl_Parkercity.DataBind();
                ddl_Parkercity.DataTextField = "City_name";
                ddl_Parkercity.DataValueField = "City_id";
                ddl_Parkercity.DataBind();

            }
        }
        catch
        {
            DataTable dt_city = new clsInsert().GetAllCity(Convert.ToInt32(ddlstate.SelectedValue));
            ddl_Parkercity.DataSource = dt_city;
            // ddl_Parkercity.DataBind();
            ddl_Parkercity.DataTextField = "City_name";
            ddl_Parkercity.DataValueField = "City_id";
            ddl_Parkercity.DataBind();
        }
    }
    protected void ddl_Parkercountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["Countryid"] = null;
            
        }
        catch
        {
        }
        try
        {

            bindstate();

        }
        catch
        {
        }
    }
    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["Stateid"] = null;

        }
        catch
        { 
        
        }
        try
        {

            bindcity();

        }
        catch
        {
        }
    }
    protected void ddl_Parkercity_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class WUC_wuc_edit_country : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack == true)
        {
            try
            {
                txtbox_cuntryabbr.Text = Session["countryabbr"].ToString();
               
                txtbox_cuntryname.Text = Session["countryname"].ToString();

                imgflag.Height = 20;
                imgflag.Width = 30;
                imgflag.ImageUrl = "../FlagImage/Resized/" + Session["Country_flag"].ToString().Trim();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_CountryMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_CountryMaster.aspx.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
                
            }
        }
    }
    string ImageName = "";

    string msg;
    public void updatecountry()
    {
        try
        {
            hst.Clear();
           
            int ID = Convert.ToInt32(Session["id"].ToString());
            hst.Add("action", "update");
            hst.Add("ID", ID);
            hst.Add("country_name", txtbox_cuntryname.Text);
            if (ImageName == "")
            {
                hst.Add("country_flag", Session["Country_flag"].ToString().Trim());

            }
            else
            {
                hst.Add("country_flag", ImageName);
            }
            hst.Add("country_abbrvtion", txtbox_cuntryabbr.Text);
           

            int result = objData.ExecuteNonQuery("[sp_country]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_CountryMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_CountryMaster.aspx?msg=" + msg);

                }
             

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

                

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

           

        }

    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {



            if (fup_cuntryflag.HasFile)
            {
                string oFName = fup_cuntryflag.FileName.ToString().Trim();
                string[] strArr = oFName.Split('.');
                int sLength = strArr.Length;
                if (strArr[sLength - 1] == "jpg" || strArr[sLength - 1] == "png" || strArr[sLength - 1] == "gif")
                {
                    ImageName = txtbox_cuntryname.Text + "." + strArr[1];
                    string Path = Server.MapPath("~/FlagImage/");
                    // fup_cuntryflag.SaveAs(Path + ImageName);



                    string SaveLocation = Path + "Original\\" + ImageName;
                    //  string SaveLocation2 = pth + "Big\\" + filename;
                    string SaveLocation3 = Path + "Resized\\" + ImageName;

                    try
                    {
                        fup_cuntryflag.PostedFile.SaveAs(SaveLocation);

                        ///image resize start
                        ///
                        System.Drawing.Image img = System.Drawing.Image.FromFile(SaveLocation);

                        double iw = Convert.ToDouble(img.Width.ToString());
                        double ih = Convert.ToDouble(img.Height.ToString());
                        //Response.Write(iw.ToString() + "<br>");
                        //Response.Write(ih.ToString() + "<br>");
                        double percent = 1;
                        if (iw > 160)
                        {
                            percent = (160 / iw);
                        }

                        double niw = 16;
                        double nih = 16;

                        //   Response.Write(niw.ToString() + "<br>");
                        //   Response.Write(nih.ToString());


                        Bitmap b = new Bitmap((int)niw, (int)nih);
                        Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        g.DrawImage(img, 0, 0, (float)niw, (float)nih);
                        g.Dispose();

                        img = (System.Drawing.Image)b;

                        // Encoder parameter for image quality

                        // Jpeg image codec
                        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
                        //find the encoder with the image/jpeg mime-type
                        ImageCodecInfo ici = null;
                        foreach (ImageCodecInfo codec in codecs)
                        {

                            if (codec.MimeType == "image/jpeg")
                                ici = codec;

                        }

                        EncoderParameters ep = new EncoderParameters();

                        //Create an encoder parameter for quality with an appropriate level setting

                        ep.Param[0] = new EncoderParameter(Encoder.Quality, (long)100);

                        //Save the image with a filename that indicates the compression quality used

                        img.Save(SaveLocation3, ici, ep);
                        img.Dispose();
                        ///image resize end



                        ///

                    }
                    catch (Exception ex)
                    {
                        //Response.Write("Error: " + ex.Message);
                        //Note: Exception.Message returns a detailed message that describes the current exception. 
                        //For security reasons, we do not recommend that you return Exception.Message to end users in 
                        //production environments. It would be better to put a generic error message. 
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please upload image only.')", true);
                    return;
                }
            }


           
            updatecountry();
          

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_employee_of_corporate_parker.ascx.cs" Inherits="WUC_wuc_edit_employee_of_corporate_parker" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 174px;
    }
</style>
<div>

  
    <asp:Panel ID="Panel1" runat="server" Visible="false">
    <fieldset><legend>Reserved Parker</legend>
        <asp:Repeater ID="rpt_reserved" runat="server" 
            onitemcommand="rpt_reserved_ItemCommand">
     
         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Employee Code
                    </th>

                     <th>
                      Employee Name
                    </th>
                
                    <th>
                Edit Detail
                    </th>
                   
                    <th>
                Edit Vehicle
                    </th>
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>
                 <asp:Label ID="empid" runat="server" Text='<%# Eval("empid") %>' 
                                        Visible="False" />
            <%#Eval("empcode")%></td>
          
               <td>

            <%#Eval("emp_name")%></td>
                   <td>
           
        <asp:LinkButton ID="lnkEdit" CommandName="cmdEditdetail" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton></td>
                      <td>
           
        <asp:LinkButton ID="LinkButton1" CommandName="cmdEditvehicle" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton></td>
          
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>
    </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel2" runat="server" Visible="false">
    <fieldset><legend>Random Parker</legend>
    
    <asp:Repeater ID="rpt_random" runat="server" onitemcommand="rpt_random_ItemCommand">
     
         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                <th>
                      Employee Code
                    </th>

                     <th>
                      Employee Name
                    </th>
                
                    <th>
                Edit Detail
                    </th>
                   
                    <th>
                Edit Vehicle
                    </th>
                   
                   
                   
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
         <tr>
            <td>
             <asp:Label ID="empid" runat="server" Text='<%# Eval("empid") %>' 
                                        Visible="False" />
            <%#Eval("empcode")%></td>
          
               <td>

            <%#Eval("emp_name")%></td>
                   <td>
            <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton>
          </td>
              <td>
           
        <asp:LinkButton ID="LinkButton1" CommandName="cmdEditvehicle" CommandArgument='<%#Eval("empid") %>' runat="server"><img src="images/edit.png" />

                    </asp:LinkButton></td>
          
                    </tr>
        </ItemTemplate>
     <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>
    
    </fieldset>
    </asp:Panel>

</div>
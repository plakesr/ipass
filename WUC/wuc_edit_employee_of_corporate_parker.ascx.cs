﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_edit_employee_of_corporate_parker : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DataSet mainhead = new clsInsert().fetchrec("select Parker_id from tbl_ParkerRegis where UserName='" + Session["parkerUsername"].ToString().ToString() + "'");
            DataSet emp_res = new clsInsert().fetchrec("select tbl_CorporateEmployeeDetails.*,tbl_LotRateMaster.Parkingtype from tbl_CorporateEmployeeDetails  inner join tbl_LotRateMaster on tbl_LotRateMaster.LotRateid=tbl_CorporateEmployeeDetails.PlanRateID where CorporationID=" + Convert.ToInt32(mainhead.Tables[0].Rows[0][0])+"and tbl_LotRateMaster.Parkingtype=1");
           
            DataTable empshow_reserved = new DataTable();

            empshow_reserved.Columns.Add("empcode", typeof(string));
            empshow_reserved.Columns.Add("emp_name", typeof(string));
            empshow_reserved.Columns.Add("emp_emailid", typeof(string));
            empshow_reserved.Columns.Add("empid", typeof(string));

            for (int i = 0; i < emp_res.Tables[0].Rows.Count; i++)
            {
                DataRow dr = empshow_reserved.NewRow();
                dr[0] = emp_res.Tables[0].Rows[i][15].ToString();
                dr[1] = emp_res.Tables[0].Rows[i][4].ToString() + " " + emp_res.Tables[0].Rows[i][5].ToString() + " " + emp_res.Tables[0].Rows[i][6].ToString();
                dr[2] = emp_res.Tables[0].Rows[i][3].ToString();
                dr[3] = emp_res.Tables[0].Rows[i][0].ToString();

                empshow_reserved.Rows.Add(dr);
            }
            if (empshow_reserved.Rows.Count > 0)
            {
                Panel1.Visible = true;
                rpt_reserved.DataSource = empshow_reserved;
                rpt_reserved.DataBind();
            }

            DataSet emp_ran = new clsInsert().fetchrec("select tbl_CorporateEmployeeDetails.*,tbl_LotRateMaster.Parkingtype from tbl_CorporateEmployeeDetails  inner join tbl_LotRateMaster on tbl_LotRateMaster.LotRateid=tbl_CorporateEmployeeDetails.PlanRateID where CorporationID=" + Convert.ToInt32(mainhead.Tables[0].Rows[0][0]) + "and tbl_LotRateMaster.Parkingtype=2");

            DataTable empshow_random = new DataTable();

            empshow_random.Columns.Add("empcode", typeof(string));
            empshow_random.Columns.Add("emp_name", typeof(string));
            empshow_random.Columns.Add("emp_emailid", typeof(string));
            empshow_random.Columns.Add("empid", typeof(string));

            for (int i = 0; i < emp_ran.Tables[0].Rows.Count; i++)
            {
                DataRow dr = empshow_random.NewRow();
                dr[0] = emp_ran.Tables[0].Rows[i][15].ToString();
                dr[1] = emp_ran.Tables[0].Rows[i][4].ToString() + " " + emp_ran.Tables[0].Rows[i][5].ToString() + " " + emp_ran.Tables[0].Rows[i][6].ToString();
                dr[2] = emp_ran.Tables[0].Rows[i][3].ToString();
                dr[3] = emp_ran.Tables[0].Rows[i][0].ToString();

                empshow_random.Rows.Add(dr);
            }
            if (empshow_random.Rows.Count > 0)
            {
                Panel2.Visible = true;
                rpt_random.DataSource = empshow_random;
                rpt_random.DataBind();
            }


        }
        catch
        {

        }
    }

    protected void rpt_reserved_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
         int i = e.Item.ItemIndex;
         if (e.CommandName == "cmdEditdetail")
         {
             Label empid = rpt_reserved.Items[i].FindControl("empid") as Label;
             Session["empid_edit"] = empid.Text;
             ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('EditDetailsOfEmploy.aspx','New Windows','height=1000, width=1400,location=no');", true);

         

            
         }
         if (e.CommandName == "cmdEditvehicle")
         {
             Label empid = rpt_reserved.Items[i].FindControl("empid") as Label;
             Session["empid_edit"] = empid.Text;
             ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Edit_Vehicle_Of_Employee.aspx','New Windows','height=1000, width=1400,location=no');", true);

         }
    }
    protected void rpt_random_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEditdetail")
        {
            Label empid = rpt_random.Items[i].FindControl("empid") as Label;
            Session["empid_edit"] = empid.Text;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('EditDetailsOfEmploy.aspx','New Windows','height=1000, width=1400,location=no');", true);

         

        }
        if (e.CommandName == "cmdEditvehicle")
        {
            Label empid = rpt_random.Items[i].FindControl("empid") as Label;
            Session["empid_edit"] = empid.Text;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Edit_Vehicle_Of_Employee.aspx','New Windows','height=1000, width=1400,location=no');", true);

        }
    }
}
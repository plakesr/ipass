﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_lotmanager.ascx.cs" Inherits="WUC_wuc_edit_lotmanager" %>

<style type="text/css">
    .style2
    {
        width: 98px;
    }
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style3
    {
        height: 34px;
    }
    .style4
    {
        width: 148px;
    }
    .style5
    {
        height: 34px;
        width: 209px;
    }
    .style6
    {
        width: 209px;
    }
</style>
<table width="80%" align="center">
      <tr>
        <td>
          <asp:Button Text="Site Details" BorderStyle="None" ID="btn_Sitedetails" 
                CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Lot Details" BorderStyle="None" ID="btn_Lotdetails" 
                CssClass="Initial" runat="server"
              OnClick="Tab2_Click" Enabled="False" />
          <asp:Button Text="Enterance Details" BorderStyle="None" ID="btn_enterance" 
                CssClass="Initial" runat="server"
              OnClick="Tab3_Click" Enabled="False" />
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
              <table>
                <tr>
                  <td>
                    <h3>
                      <span>
                      
    <asp:Panel ID="Panel1" runat="server">
    <fieldset>
    <legend>
        Site Details</legend>
        <table class="form-table">
            <tr>
                <td class="style2">
                    Customer Name</td>
                <td>
                    <asp:DropDownList ID="ddt_sitename" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox
                        DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
                    </asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfv_sitename" runat="server" 
                        ControlToValidate="ddt_sitename" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Site Type</td>
                <td>
                    <asp:DropDownList ID="ddt_sitetype" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox
                        DataSourceID="SqlDataSource2" DataTextField="SiteTypeName" 
                        DataValueField="SiteTypeId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [SiteTypeId], [SiteTypeName] FROM [tbl_SiteTypeMaster]">
                    </asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfv_sitetype" runat="server" 
                        ControlToValidate="ddt_sitetype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td class="style2">
                    Vehicle Type</td>
                <td>
                    <asp:DropDownList ID="ddlvehicletype" runat="server" ValidationGroup="a"  CssClass=twitterStyleTextbox
                        DataSourceID="SqlDataSource4" DataTextField="vehicle_type"  Height="34px" Width="140px"
                        DataValueField="v_id" >
                    </asp:DropDownList>
                    
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT * FROM [tbl_Vehicle_Type]"></asp:SqlDataSource>
                    
                    <asp:RequiredFieldValidator ID="rfv_ddlvehicletype" runat="server" 
                        ControlToValidate="ddlvehicletype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style2">
                    Vehicle Charge Type</td>
                <td>
                    <asp:DropDownList ID="ddlvehiclechargetype" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox
                        DataSourceID="SqlDataSource5" DataTextField="time_in_hours" 
                        DataValueField="c_id"   Width="142px">
                    </asp:DropDownList>
                    
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT * FROM [tbl_Charge_of_vehicle]"></asp:SqlDataSource>
                    
                    <asp:RequiredFieldValidator ID="rfv_vehiclechargetype" runat="server" 
                        ControlToValidate="ddlvehiclechargetype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td class="style2">
                    Space Available</td>
                <td>
                                        <asp:TextBox ID="txtbox_spaceavailable" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_spaceavailable" runat="server" 
                        ControlToValidate="txtbox_spaceavailable" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table><br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btn1_next" runat="server" Text="Next" onclick="btn1_next_Click" 
            ValidationGroup="a" CssClass=button Height="35px" Width="79px"></asp:Button>
    </fieldset>
</asp:Panel>

<%--<asp:Panel ID="Panel2" runat="server" Height="270px">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
               <ContentTemplate> --%>   
<%--<asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource3" 
        onitemcommand="Repeater1_ItemCommand">

 <HeaderTemplate>
            <table width="100%" class="rounded-corner">
                <tr>
                    
                      <th>
                        Lot_id
                    </th>
                     <th>
                        Site_Type_Id
                    </th>


                    <th>
                        Site_Id
                    </th><th>
                        Entry_Gate_Information
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("Lot_id")%>
                    </td><td>
                    <%#Eval("SiteTypeId")%>
                </td>

                 <td>
                    <%#Eval("Site_Id")%>
                    </td><td>
                    <%#Eval("EntryGateInformation")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Lot_id") %>' runat="server"><img src="images/edit.png"/>
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png"/>
                    </asp:LinkButton>
                    </td>
            </tr>
          
            <asp:Label ID="isspace" runat="server" Text='<%# Eval("IsSpaceAvailable") %>' 
                                        Visible="False" />
  <asp:Label ID="lotnumber" runat="server" Text='<%# Eval("LotNumber") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="lothours" runat="server" Text='<%# Eval("LotHours") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="totalspace" runat="server" Text='<%# Eval("TotalSpaces") %>' 
                                        Visible="False"></asp:Label> 
  <asp:Label ID="entry" runat="server" Text='<%# Eval("EntryGateInformation") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="exit" runat="server" Text='<%# Eval("ExitGateInformation") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="rest" runat="server" Text='<%# Eval("Restrictions") %>' 
                                        Visible="False"></asp:Label>  
   <asp:Label ID="des" runat="server" Text='<%# Eval("Description") %>' 
                                        Visible="False"></asp:Label>
   <asp:Label ID="id" runat="server" Text='<%# Eval("Lot_id") %>' 
                                        Visible="False"></asp:Label>
   <asp:Label ID="name" runat="server" Text='<%# Eval("LotName") %>' Visible="False"></asp:Label>

        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>--%>
   <%-- <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        
        
                       SelectCommand="SELECT 
                       * FROM [tbl_LotMaster]">
    </asp:SqlDataSource>--%>

         </span>
                       
                       
                       
                        <h3>
                        </h3>
                       
                       
                       
                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View2" runat="server">
              <table class="form-table">
                <tr>
                  <td>
                    <h3>
                        &nbsp;<asp:Panel ID="Panel3" runat="server">

    <asp:Panel ID="Panel2" runat="server">
        </asp:Panel>
    <fieldset>
    <legend>
        
        Lot Details 
    </legend>
        <table class="form-table">
        <tr>
                <td class="style3">
                    Lot Name</td>
                <td>
                    <asp:TextBox ID="txtbox_LotName" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_LotName" runat="server" 
                        ControlToValidate="txtbox_LotName" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style3">
                    Lot Number</td>
                <td>
                    <asp:TextBox ID="txtbox_lotnumber" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_lotnumber" runat="server" 
                        ControlToValidate="txtbox_lotnumber" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    LotHours
                </td>
                <td>
                    <asp:TextBox ID="txtbox_lothours" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_lothours" runat="server" 
                        ControlToValidate="txtbox_lothours" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Total Spaces</td>
                <td>
                    <asp:TextBox ID="txtbox_totalspaces" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_totalspaces" runat="server" 
                        ControlToValidate="txtbox_totalspaces" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                
                
                <td class="style3">
                    InActive</td>
                <td>
                    <asp:CheckBox ID="chkbox_inactive" runat="server" />
                </td>
                
                
            </tr>
        </table>
        <br />
        <br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" 
            ValidationGroup="b" CssClass=button Height="35px" Width="79px"></asp:Button>
    </fieldset>
   </asp:Panel>

                    

                        <h3>
                        </h3>

                    

                        <h3>
                        </h3>

                    

                        <h3>
                        </h3>

                    

                        <h3>
                        </h3>

                    

                        <h3>
                        </h3>

                    

                        <h3>
                        </h3>

                    

                        <h3>
                        </h3>

                    

                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View3" runat="server">
              <table class="form-table">
                <tr>
                  <td>
                    <h3>
                       &nbsp;<asp:Panel ID="Panel4" runat="server" Height="332px" Width="454px">
                            <fieldset>
                                <legend>Enterance Details</legend>
                                <table class="form-table">
                                    <tr>
                                        <td class="style4">
                                            Enterance Information</td>
                                        <td class="style6">
                                            <asp:TextBox ID="txtbox_enteranceinformation" runat="server" CssClass=twitterStyleTextbox 
                                                ValidationGroup="a" Width="154px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_enterance_information" runat="server" 
                                                ControlToValidate="txtbox_exitinformation" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="a"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            Exit Information</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_exitinformation" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_exit_information" runat="server" 
                                                ControlToValidate="txtbox_exitinformation" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="a"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            Restriction</td>
                                        <td class="style6">
                                            <asp:TextBox ID="txtbox_restriction" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_restriction" runat="server" 
                                                ControlToValidate="txtbox_restriction" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="a"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description</td>
                                        <td class="style6">
                                            <asp:TextBox ID="txtbox_Description" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                                                ControlToValidate="txtbox_Description" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="a"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset></asp:Panel>
                        <br />
                        <br />
    <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="a" CssClass=button
        onclick="btn_update_Click" Height="35px" Width="79px" />

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass=button Height="35px" Width="79px" />
                        <h3>
                        </h3>

                        <h3>
                        </h3>

                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>






















<%--<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 147px;
    }
    .style3
    {
        width: 150px;
    }
    .style4
    {
        width: 145px;
    }
</style>
<div>
    <br />
    <panel>
    <fieldset>
    <legend>
        Site Details</legend>
        <table class="style1">
            <tr>
                <td class="style2">
                    Site Name</td>
                <td>
                    <asp:DropDownList ID="ddt_sitename" runat="server" ValidationGroup="a" 
                        DataSourceID="SqlDataSource1" DataTextField="SiteName" DataValueField="SiteId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
                    </asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfv_sitename" runat="server" 
                        ControlToValidate="ddt_sitename" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Site Type</td>
                <td>
                    <asp:DropDownList ID="ddt_sitetype" runat="server" ValidationGroup="a" 
                        DataSourceID="SqlDataSource2" DataTextField="SiteTypeName" 
                        DataValueField="SiteTypeId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [SiteTypeId], [SiteTypeName] FROM [tbl_SiteTypeMaster]">
                    </asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfv_sitetype" runat="server" 
                        ControlToValidate="ddt_sitetype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>


              <tr>
                <td class="style2">
                    Vehicle Type</td>
                <td>
                    <asp:DropDownList ID="ddlvehicletype" runat="server" ValidationGroup="a" 
                        DataSourceID="SqlDataSource4" DataTextField="vehicle_type" 
                        DataValueField="v_id" >
                    </asp:DropDownList>
                    
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT * FROM [tbl_Vehicle_Type]"></asp:SqlDataSource>
                    
                    <asp:RequiredFieldValidator ID="rfv_ddlvehicletype" runat="server" 
                        ControlToValidate="ddlvehicletype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style2">
                    Vehicle Charge Type</td>
                <td>
                    <asp:DropDownList ID="ddlvehiclechargetype" runat="server" ValidationGroup="a" 
                        DataSourceID="SqlDataSource5" DataTextField="time_in_hours" 
                        DataValueField="c_id" >
                    </asp:DropDownList>
                    
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT * FROM [tbl_Charge_of_vehicle]"></asp:SqlDataSource>
                    
                    <asp:RequiredFieldValidator ID="rfv_vehiclechargetype" runat="server" 
                        ControlToValidate="ddlvehiclechargetype" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>



            <tr>
                <td class="style2">
                    Space Available</td>
                <td>
                   
                    <asp:TextBox ID="txtbox_spaceavailable" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_spaceavailable" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table><br /><br />
    </fieldset>
    </panel>
    <br />
    <br />
    <panel>
    <fieldset>
    <legend>
        Lot Details 
    </legend>
        <table class="style1">
        <tr>
                <td class="style3">
                    Lot Name</td>
                <td>
                    <asp:TextBox ID="txtbox_LotName" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_LotName" runat="server" 
                        ControlToValidate="txtbox_LotName" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style3">
                    Lot Number</td>
                <td>
                    <asp:TextBox ID="txtbox_lotnumber" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_lotnumber" runat="server" 
                        ControlToValidate="txtbox_lotnumber" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    LotHours
                </td>
                <td>
                    <asp:TextBox ID="txtbox_lothours" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_lothours" runat="server" 
                        ControlToValidate="txtbox_lothours" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Total Spaces</td>
                <td>
                    <asp:TextBox ID="txtbox_totalspaces" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_totalspaces" runat="server" 
                        ControlToValidate="txtbox_totalspaces" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            
            <tr>
                <td class="style3">
                    InActive</td>
                <td>
                    <asp:CheckBox ID="chkbox_inactive" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <br />
    </fieldset>
    </panel>
    <br />
    <br />
    <panel>
    <fieldset>
    <legend>
        Enterance Details</legend>
        <table class="style1">
            <tr>
                <td class="style4">
                    Enterance Information</td>
                <td>
                    <asp:TextBox ID="txtbox_enteranceinformation" runat="server" 
                        ValidationGroup="a" Width="128px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_enterance_information" runat="server" 
                        ControlToValidate="txtbox_exitinformation" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    Exit Information</td>
                <td>
                    <asp:TextBox ID="txtbox_exitinformation" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_exit_information" runat="server" 
                        ControlToValidate="txtbox_exitinformation" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    Restriction</td>
                <td>
                    <asp:TextBox ID="txtbox_restriction" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_restriction" runat="server" 
                        ControlToValidate="txtbox_restriction" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    Description</td>
                <td>
                    <asp:TextBox ID="txtbox_Description" runat="server" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                        ControlToValidate="txtbox_Description" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </fieldset>
    </panel><br />
    <br />
    <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="a" 
        onclick="btn_update_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" ValidationGroup="b" />
    <br />
    
    
&nbsp;&nbsp;&nbsp;&nbsp;
    
    
</div>
--%>
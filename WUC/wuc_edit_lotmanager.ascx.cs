﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_lotmanager : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {

                txtbox_Description.Text = Session["Description"].ToString();
                txtbox_enteranceinformation.Text = Session["EntryGateInformation"].ToString();
                txtbox_exitinformation.Text = Session["ExitGateInformation"].ToString();
                txtbox_lothours.Text = Session["LotHours"].ToString();
                txtbox_lotnumber.Text = Session["LotNumber"].ToString();
                txtbox_restriction.Text = Session["Restrictions"].ToString();
                txtbox_spaceavailable.Text = Session["IsSpaceAvailable"].ToString();
                txtbox_totalspaces.Text = Session["TotalSpaces"].ToString();
                txtbox_LotName.Text = Session["Lot_name"].ToString();
               
                    btn_Sitedetails.CssClass = "Clicked";
                    MainView.ActiveViewIndex = 0;



                
            }
            catch
            {
                Response.Redirect("Admin_LotMaster.aspx");
            }
            
        }  
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        string msg = "1";
        updatelotmanager();
        Response.Redirect("Admin_LotMaster.aspx?msg=" + msg);

        //Response.Redirect("Admin_LotMaster.aspx");
    }
    public void updatelotmanager()
    {
        try
        {

            hst.Clear();
            int i;
            i = Convert.ToInt32(Session["Lot_id"].ToString());

            hst.Add("Action", "UPDATE");
            hst.Add("lotid", i);
            hst.Add("spaceavailable", txtbox_spaceavailable.Text);
            hst.Add("lotnumber", txtbox_lotnumber.Text);
            hst.Add("lothours", txtbox_lothours.Text);
            hst.Add("totalspaces", txtbox_totalspaces.Text);
            hst.Add("entrygateinformation", txtbox_enteranceinformation.Text);
            hst.Add("exitgateinformation", txtbox_exitinformation.Text);
            hst.Add("restriction", txtbox_restriction.Text);
            hst.Add("description", txtbox_Description.Text);
            hst.Add("agreement_id",6);
            hst.Add("cancelpolicy", "ddd");
            hst.Add("sitetypeid", Convert.ToInt32(ddt_sitetype.SelectedValue));
            hst.Add("siteid", Convert.ToInt32(ddt_sitename.SelectedValue));
            hst.Add("vehicletype", ddlvehicletype.SelectedItem.Value);
            hst.Add("vehiclechargetype", ddlvehiclechargetype.SelectedItem.Value);
            hst.Add("lotname", txtbox_LotName.Text);

            hst.Add("modifiedby", "lovey_modifiername");
            hst.Add("operatorid", "lovey_operatorname");
            hst.Add("dateofchanging", DateTime.Now);


            int result = objData.ExecuteNonQuery("[sp_LotMaster]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {



            }

        }
        catch
        {


        }

    }
    protected void Tab1_Click(object sender, EventArgs e)
    {

        btn_Sitedetails.CssClass = "Clicked";
        btn_Lotdetails.CssClass = "Initial";
        btn_enterance.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void Tab2_Click(object sender, EventArgs e)
    {
        btn_enterance.CssClass = "Initial";
        btn_Lotdetails.CssClass = "Clicked";
        btn_Sitedetails.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
    protected void Tab3_Click(object sender, EventArgs e)
    {
        btn_Lotdetails.CssClass = "Initial";
        btn_Sitedetails.CssClass = "Initial";
        btn_enterance.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;
    }
    protected void btn1_next_Click(object sender, EventArgs e)
    {
        btn_Lotdetails.Enabled = true;
        btn_Sitedetails.Enabled = false;

        MainView.ActiveViewIndex = 1;
    }
    protected void btn2_next_Click(object sender, EventArgs e)
    {
        btn_enterance.Enabled = true;
        btn_Sitedetails.Enabled = false;
        btn_Lotdetails.Enabled = false;

        MainView.ActiveViewIndex = 2;
    }
}
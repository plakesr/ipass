﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_plan_master.ascx.cs" Inherits="WUC_wuc_edit_plan_master" %>
<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style2
    {
        width: 98px;
    }
    .style3
    {
        width: 382px;
    }
</style>
<asp:Panel ID="Panel1" runat="server" >
<fieldset>
<legend> Plan Details  </legend>
<table class="style1">
    <tr>
        <td>
            Plan Name</td>
        <td>
            <asp:TextBox ID="txtbox_planname" runat="server" ValidationGroup="a"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_planname" runat="server" 
                ControlToValidate="txtbox_planname" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Plan Description</td>
        <td>
            <asp:TextBox ID="txtbox_plandesc" runat="server" TextMode="MultiLine" 
                ValidationGroup="a"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                ControlToValidate="txtbox_plandesc" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>

    <tr>
        <td>
            State</td>
        <td>
           <asp:CheckBox ID="chbk_state" runat="server" 
                oncheckedchanged="chbk_state_CheckedChanged" AutoPostBack="true" />
        </td>
    </tr>
</table>
</fieldset>
</asp:Panel>


<asp:Panel ID="Panel2" runat="server" Visible="false">
<fieldset>
<legend> Vehicle Details  </legend>
    <asp:Repeater ID="Repeater1" runat="server"  onitemcommand="Repeater1_ItemCommand">

    <HeaderTemplate>
            <table width="100%" class="rounded-corner">
                <tr>
                      <th>
                        Vehicle Type
                    </th>
                     <th>
                       Rate
                    </th>
                    <th>
                        Plan Type
                    </th>
                   
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text='<%#Eval("v_id")%>' Visible="False"></asp:Label>     <%#Eval("vehicle_type")%>
                    </td><td>
                        <asp:TextBox ID="txtbox_rateforvehicle" runat="server" Text='<%#Eval("Rate")%>'></asp:TextBox>
                </td>
                
                <td>
                    
                    
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                        <asp:ListItem Value="2">Daywise</asp:ListItem>
                        <asp:ListItem Value="3">Quarterly</asp:ListItem>
                        <asp:ListItem Value="4">HalfYearly</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                   <%-- <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Country_name") %>' 
                                        Visible="False" />
                                    <asp:Label ID="idlabel1" runat="server" Text='<%# Eval("Country_code") %>' 
                                        Visible="False"></asp:Label>
                    <asp:Label ID="idlabel2" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="idlabel3" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>--%>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="Repeater2" runat="server" DataSourceID="SqlDataSource1" Visible="false">
    <HeaderTemplate>
            <table width="100%" class="rounded-corner">
                <tr>
                      <th>
                        Vehicle Type
                    </th>
                     <th>
                       Rate
                    </th>
                    <th>
                        Plan Type
                    </th>
                   
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text='<%#Eval("v_id")%>' Visible="False"></asp:Label>     <%#Eval("vehicle_type")%>
                    </td><td>
                        <asp:TextBox ID="txtbox_rateforvehicle" runat="server" ></asp:TextBox>
                </td>
                
                <td>
                    
                    
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                        <asp:ListItem Value="2">Daywise</asp:ListItem>
                        <asp:ListItem Value="3">Quarterly</asp:ListItem>
                        <asp:ListItem Value="4">HalfYearly</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                   <%-- <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Country_name") %>' 
                                        Visible="False" />
                                    <asp:Label ID="idlabel1" runat="server" Text='<%# Eval("Country_code") %>' 
                                        Visible="False"></asp:Label>
                    <asp:Label ID="idlabel2" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="idlabel3" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>--%>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>


    <asp:Label ID="idLabel" runat="server" Text="" Visible="False" />


    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_Vehicle_Type]"></asp:SqlDataSource>





    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_Vehicle_Type]"></asp:SqlDataSource>--%>
</fieldset>
</asp:Panel>

<asp:Panel ID="Panel3" runat="server" Visible="true">
<fieldset>
<legend> Money Details  </legend>
<table class="style1">
    <tr>
        <td>
           Rate for Normal</td>
        <td>
            <asp:TextBox ID="txtbox_ratefornormal" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Plan type</td>
        <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                        <asp:ListItem Value="2">Daywise</asp:ListItem>
                        <asp:ListItem Value="3">Quarterly</asp:ListItem>
                        <asp:ListItem Value="4">HalfYearly</asp:ListItem>
                    </asp:DropDownList>
                    </td>
    </tr>

    
</table>
</fieldset>
</asp:Panel>
<panel>
<fieldset>
<asp:Button ID="btn_update" runat="server" onclick="btn_submit_Click" 
    Text="Update" ValidationGroup="a" CssClass=button/>
</fieldset></panel>
<%--<%#Eval("vehicle_type")%>--%>
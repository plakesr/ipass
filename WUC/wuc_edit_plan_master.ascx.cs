﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

public partial class WUC_wuc_edit_plan_master : System.Web.UI.UserControl
{
    bool k;
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {

         try
            {
                txtbox_plandesc.Text = Session["Plandescription"].ToString();
                txtbox_planname.Text = Session["PlanName"].ToString();
                txtbox_ratefornormal.Text = Session["Ratefornormal"].ToString();
                idLabel.Text = Session["id"].ToString();

                DataTable dt = new clsInsert().getvalue(txtbox_planname.Text);

                if (dt.Rows.Count > 0)
                {

                    chbk_state.Checked = true;
                    Panel2.Visible = true;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    Repeater2.Visible = false;
                    Panel3.Visible = false;

                    k = true;

                    Session["k"] = "True";
                }
                else
                {
                    //chbk_state.Checked = true;
                    if(chbk_state.Checked==true)
                    {
                        Repeater2.Visible = true;
                        Repeater1.Visible = false;
                        Panel3.Visible = false;
                    }

                    Panel2.Visible = false;
                    
                    Repeater2.Visible = false;
                    Panel3.Visible = true ;
                    k = false;
                    Session["k"] = "False";
                }






            }
            catch
            {
                k = false  ;
                Session["k"] = "False";
            }
        }
    }
    protected void chbk_state_CheckedChanged(object sender, EventArgs e)
    {
        if (chbk_state.Checked == true)
        {
          //  Panel2.Visible = true;
           // Repeater2.Visible = true;
            //Panel3.Visible = false;
           // txtbox_ratefornormal.Text = "";

            if (Session["k"].ToString () == "True"  )
            {
                Panel2.Visible = true;
                Repeater2.Visible = false;
                Repeater1.Visible = true;
                Panel3.Visible = false;
            }
            else
            {
                Panel2.Visible = true;
                Repeater2.Visible = true ;
                Repeater1.Visible = false ;
                Panel3.Visible = false;
            }

        }
        else
        {
            Panel2.Visible = false;
            Panel3.Visible = true;
            txtbox_ratefornormal.Text = Session["Ratefornormal"].ToString();
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    int vehicletype;
    string rate;
    int plantype;
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["Ratefornormal"].ToString() != "")
            {
                if (chbk_state.Checked == true)
                {
                    // state = "yes";
                    Panel2.Visible = true;
                    addplan();


                    clsInsert cs = new clsInsert();
                    DataSet ds1 = cs.select_operation("select Plan_id from tbl_PlanMaster where Plan_Name='" + txtbox_planname.Text + "'order by  Plan_id desc");
                    // int lastid= ds1.table[0].rows[0][0].tostring();
                    int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
                    //use this id in Looping
                    for (int i = 0; i < Repeater2.Items.Count; i++)
                    {
                        TextBox t = Repeater2.Items[i].FindControl("txtbox_rateforvehicle") as TextBox;
                        DropDownList ddl = Repeater2.Items[i].FindControl("DropDownList1") as DropDownList;
                        Label l = Repeater2.Items[i].FindControl("Label1") as Label;

                        vehicletype = Convert.ToInt32(l.Text);
                        rate = t.Text;
                        plantype = Convert.ToInt32(ddl.SelectedItem.Value);
                        hst.Clear();
                        hst.Add("action", "update");
                        hst.Add("type", "normal");
                        hst.Add("click", "clicked");

                        hst.Add("planforvehicle", txtbox_planname.Text);
                        //use Plan ID
                        hst.Add("vehicletype", vehicletype);
                        hst.Add("rateofvehicle", rate);
                        hst.Add("plantypeforvehicle", plantype);


                        hst.Add("v_modifiedby", "lovey");
                        hst.Add("v_operator", "lovey_operator");
                        hst.Add("v_dateofchanging", DateTime.Now);
                        hst.Add("planid", lastid);


                        int result1 = objData.ExecuteNonQuery("[sp_vehicel]", CommandType.StoredProcedure, hst);
                        if (result1 > 0)
                        {

                            //Response.Redirect("Country.aspx");

                        }
                        else
                        {
                            // lbl_error.Visible = true;

                        }



                    }

                    
                }

                else
                {
                    addplan();
                }
              //  Response.Redirect("PlanMaster.aspx");
            }
            else
            {
                if (Session["Ratefornormal"].ToString() == "")
                {
                    if (chbk_state.Checked == true)
                    {
                        // state = "yes";
                        Panel2.Visible = true;
                        addplan();


                        clsInsert cs = new clsInsert();
                        DataSet ds1 = cs.select_operation("select Plan_id from tbl_PlanMaster where Plan_Name='" + txtbox_planname.Text + "'order by  Plan_id desc");
                        // int lastid= ds1.table[0].rows[0][0].tostring();
                        int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
                        //use this id in Looping
                        for (int i = 0; i < Repeater1.Items.Count; i++)
                        {

                            hst.Clear();
                            TextBox t = Repeater1.Items[i].FindControl("txtbox_rateforvehicle") as TextBox;
                            DropDownList ddl = Repeater1.Items[i].FindControl("DropDownList1") as DropDownList;
                            Label l = Repeater1.Items[i].FindControl("Label1") as Label;

                            vehicletype = Convert.ToInt32(l.Text);
                            rate = t.Text;
                            plantype = Convert.ToInt32(ddl.SelectedItem.Value);

                            hst.Add("action", "update");
                            hst.Add("type", "abnormal");
                            hst.Add("click", "clicked");
                            hst.Add("planid", lastid);
                            hst.Add("planforvehicle", txtbox_planname.Text);
                            //use Plan ID
                            hst.Add("vehicletype", vehicletype);
                            hst.Add("rateofvehicle", rate);
                            hst.Add("plantypeforvehicle", plantype);


                            hst.Add("v_modifiedby", "lovey");
                            hst.Add("v_operator", "lovey_operator");
                            hst.Add("v_dateofchanging", DateTime.Now);



                            int result1 = objData.ExecuteNonQuery("[sp_vehicel]", CommandType.StoredProcedure, hst);
                            if (result1 > 0)
                            {

                                //Response.Redirect("Country.aspx");

                            }
                            else
                            {
                                // lbl_error.Visible = true;

                            }
                        }

                    }

                    else
                    {
                       // Panel2.Visible = true;
                        addplan();

                        hst.Clear();

                        hst.Add("action", "update");
                        hst.Add("type", "abnormal");
                        hst.Add("click", "notclicked");
                        hst.Add("planid", Convert.ToInt32(idLabel.Text));

                        int result1 = objData.ExecuteNonQuery("[sp_vehicel]", CommandType.StoredProcedure, hst);
                        if (result1 > 0)
                        {

                            //Response.Redirect("Country.aspx");

                        }
                        else
                        {
                            // lbl_error.Visible = true;

                        }

                    }

                }

              //  Response.Redirect("Admin_PlanMaster.aspx");
            }




            //clsInsert cs = new clsInsert();
            //DataSet ds1 = cs.select_operation("select Plan_id from tbl_PlanMaster where Plan_Name='" + txtbox_planname.Text + "'order by  Plan_id desc");
            //// int lastid= ds1.table[0].rows[0][0].tostring();
            //int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
            ////use this id in Looping
            //for (int i = 0; i < Repeater1.Items.Count; i++)
            //{

            //    hst.Clear();
            //    TextBox t = Repeater1.Items[i].FindControl("txtbox_rateforvehicle") as TextBox;
            //    DropDownList ddl = Repeater1.Items[i].FindControl("DropDownList1") as DropDownList;
            //    Label l = Repeater1.Items[i].FindControl("Label1") as Label;

            //    vehicletype = Convert.ToInt32(l.Text);
            //    rate = t.Text;
            //    plantype = Convert.ToInt32(ddl.SelectedItem.Value);

            //    hst.Add("action", "update");
            //    hst.Add("type", "abnormal");
            //    hst.Add("click", "notclicked");
            //    hst.Add("planid", lastid);
            //    //hst.Add("planforvehicle", txtbox_planname.Text);
            //    ////use Plan ID
            //    //hst.Add("vehicletype", vehicletype);
            //    //hst.Add("rateofvehicle", rate);
            //    //hst.Add("plantypeforvehicle", plantype);


            //    //hst.Add("v_modifiedby", "lovey");
            //    //hst.Add("v_operator", "lovey_operator");
            //    //hst.Add("v_dateofchanging", DateTime.Now);



            //int result1 = objData.ExecuteNonQuery("[sp_vehicel]", CommandType.StoredProcedure, hst);
            //if (result1 > 0)
            //{

            //    //Response.Redirect("Country.aspx");

            //}
            //else
            //{
            //    // lbl_error.Visible = true;

            //}



        }

        catch
        {

        }
        string msg = "1";
        Response.Redirect("Admin_PlanMaster.aspx?msg=" + msg);

    }

    public void addplan()
    {
        try
        {
            hst.Clear();
            if (Session["Ratefornormal"].ToString() == "")
            {
                hst.Add("action", "update");
                hst.Add("type", "notready");
                hst.Add("planid", Convert.ToInt32(idLabel.Text));

                hst.Add("planname", txtbox_planname.Text);
                hst.Add("plandesc", txtbox_plandesc.Text);


                // state = "";
                hst.Add("RateforNormal", txtbox_ratefornormal.Text);
                hst.Add("plantype", DropDownList1.SelectedItem.Value);


                hst.Add("modifiedby", "lovey");
                hst.Add("operator", "lovey_operator");
                hst.Add("dateofchanging", DateTime.Now);



                int result = objData.ExecuteNonQuery("[sp_Plan]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {

                    //Response.Redirect("Country.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }
            else
            {
                hst.Add("action", "update");
                hst.Add("type", "ready");
                hst.Add("planid", Convert.ToInt32(idLabel.Text));


                hst.Add("planname", txtbox_planname.Text);
                hst.Add("plandesc", txtbox_plandesc.Text);


                // state = "";
                if (chbk_state.Checked == true)
                {
                    hst.Add("RateforNormal", "");
                    hst.Add("plantype", 0);
                }
                else
                {
                    hst.Add("RateforNormal", txtbox_ratefornormal.Text);
                    hst.Add("plantype", DropDownList1.SelectedItem.Value);

                }

                hst.Add("modifiedby", "lovey");
                hst.Add("operator", "lovey_operator");
                hst.Add("dateofchanging", DateTime.Now);



                int result = objData.ExecuteNonQuery("[sp_Plan]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {

                    //Response.Redirect("Country.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }
          
        }

        catch
        {
            //lbl_error.Visible = true;

        }

    }

    public DataTable getvalue(string id)
    {
        QueryStringAccess obj = new QueryStringAccess();
        SqlConnection con = new SqlConnection(obj.connString());
        SqlDataAdapter adp = new SqlDataAdapter("sp_forrepeater", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        adp.SelectCommand.Parameters.AddWithValue("@name", id);

        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds.Tables[0];
    }

}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_rate.ascx.cs" Inherits="WUC_wuc_edit_rate" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 15px;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Panel ID="Panel1" runat="server" Height="337px">

        <fieldset style="height: 338px"><legend>Flat Rate Details</legend>
        
        
        
        
        
        
        <table class="style1">
                <tr>
                    <td>
                        Name</td>
                    <td>
                        <asp:TextBox ID="txtbox_FlatName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Rate</td>
                    <td>
                        <asp:TextBox ID="txtbox_FlatRate" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Period</td>
                    <td>
                        <asp:TextBox ID="txtbox_FlatPeriod" runat="server"></asp:TextBox>
                        <asp:DropDownList ID="ddl_period" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btn_update" runat="server" Text="Update" />
                    </td>
                </tr>
            </table>
        
        
        
        
        
       
            
            </fieldset>


        </asp:Panel>

        <br /><br />
        <asp:Panel ID="Panel2" runat="server" Height="328px">
        <fieldset style="height: 338px">
        
        <legend>Variable Details</legend>           
<table class="style1">
            <tr>
                <td>
                    Name</td>
                <td class="style2">
                    <asp:TextBox ID="txtbox_Variableratename" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    On Day Change Return to Step</td>
                <td class="style2">
                    <asp:DropDownList ID="ddlchange" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Max Type</td>
                <td class="style2">
                    <asp:DropDownList ID="ddl_maxtype" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Max Rate</td>
                <td class="style2">
                    <asp:TextBox ID="txtbox_maxrate" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Expiration Time Type
                </td>
                <td class="style2">
                    <asp:DropDownList ID="ddlexpiry" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Max End on</td>
            </tr>
            <tr>
                <td>
                    Days:</td>
                <td class="style2">
                    <asp:TextBox ID="txtbox_days" runat="server"></asp:TextBox></td>
            </tr>

            <tr>
                <td>
                    Time:</td>
                <td class="style2">
                    <asp:TextBox ID="txtbox_time" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    </td>
                <td class="style2">
                     <asp:Button ID="bt_variable_update" runat="server" Text="Update" />
            </tr>
        </table>
            </fieldset>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
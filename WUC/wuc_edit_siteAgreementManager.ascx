﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_siteAgreementManager.ascx.cs" Inherits="WUC_wuc_edit_siteAgreementManager" %>
<asp:Panel ID="Panel1" runat="server" Height="193px">
<fieldset style="height: 143px">

<legend>
Site Agreement Manager

</legend>

    <table class="style1">

    <tr>
            <td class="style2">
                Agreement type:</td>
            <td>
                <asp:DropDownList ID="ddl_agreementtype" runat="server">
                </asp:DropDownList>
                
                <asp:RequiredFieldValidator ID="rfv_agreementtype" runat="server" 
                    ControlToValidate="ddl_agreementtype" ErrorMessage="*Plese select agreement type" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td class="style2">
                Start Date:</td>
            <td>
                <asp:TextBox ID="txtbox_startdate" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_startdate" runat="server" 
                    ControlToValidate="txtbox_startdate" ErrorMessage="*Plese Enter The Starting Date" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                End Date:</td>
            <td>
                <asp:TextBox ID="txtbox_enddate" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_enddate" runat="server" 
                    ControlToValidate="txtbox_enddate" ErrorMessage="*Plese Enter The End Date" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Comments:</td>
            <td>
                <asp:TextBox ID="txtbox_comments" runat="server" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_comments" runat="server" 
                    ControlToValidate="txtbox_comments" 
                    ErrorMessage="*Plese Enter The Comments" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_update" runat="server" Text="Update" 
                    onclick="btn_update_Click" />
            </td>
        </tr>
    </table>

</fieldset>

</asp:Panel>
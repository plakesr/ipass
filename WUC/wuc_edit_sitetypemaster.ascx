﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_sitetypemaster.ascx.cs" Inherits="WUC_wuc_edit_sitetypemaster" %>


    <table class="style1">
        <tr>
            <td class="style2">
              Type Name:</td>
            <td>
                <asp:TextBox ID="txtbox_typename" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_typename" runat="server" 
                    ControlToValidate="txtbox_typename" 
                    ErrorMessage="*" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Status:</td>
            <td>
                <asp:CheckBox ID="chkbox_status" runat="server" />
               <%-- <asp:RequiredFieldValidator ID="rfv_TimezoneName" runat="server" 
                    ControlToValidate="txtbox_TimezoneName" 
                    ErrorMessage="*Please Enter the Timezone Name" ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_update" runat="server" Text="Update" 
                    onclick="btn_update_Click" class="btn" ValidationGroup="a" />
            </td>
        </tr>
    </table>


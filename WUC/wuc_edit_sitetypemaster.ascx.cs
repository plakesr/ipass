﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_sitetypemaster : System.Web.UI.UserControl
{
     clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            //Session["StateAbbreviation"]=l2.Text;
            try
            {
                txtbox_typename.Text = Session["SiteTypeName"].ToString();
            }
            catch
            {
                Response.Redirect("Admin_SiteType.aspx");
            }

        }  
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        string msg = "1";
        updatestate();
        Response.Redirect("Admin_SiteType.aspx?msg=" + msg);


       // Response.Redirect("Admin_SiteType.aspx");
    }
    int status = 0;
    public void updatestate()
    {
        try
        {

            hst.Clear();
            int i;
            i = Convert.ToInt32(Session["SiteTypeId"].ToString());

            hst.Add("Action", "UPDATE");
            hst.Add("id", i);

            hst.Add("sitetypename", txtbox_typename.Text);
            hst.Add("operatorid", "lovey_operator");
            hst.Add("status", status);

            hst.Add("modifiedby", "modifier_id");
            hst.Add("DateOfChanging", DateTime.Now);

            int result = objData.ExecuteNonQuery("[sp_SiteType]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {



            }

        }
        catch
        {


        }

    }

}
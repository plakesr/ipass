﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_statemaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {


                txtbox_StateName.Text = Session["StateName"].ToString();
               
                txtbox_StateAbbrv.Text = Session["StateAbbreviation"].ToString();
                ddl_countryid.SelectedValue = Session["country_id"].ToString();


                DataTable dt = new clsInsert().getcuntryflag(Convert.ToInt32(Session["country_id"].ToString()));

                if (dt.Rows.Count > 0)
                {

                    imgflag.Height = 20;
                    imgflag.Width = 30;
                    imgflag.ImageUrl = "../FlagImage/Resized/" + dt.Rows[0]["Country_flag"].ToString().Trim();

                }
            }
            catch
            {

                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_StateMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_ProvinceMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
                
            }
        }  
        
    }

    string msg = "";
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            updatestate();
        }
        catch
        {
        }
       
    }

    public void updatestate()
    {
        try
        {

            hst.Clear();
            int i;
            i = Convert.ToInt32(Session["StateId"].ToString());

            hst.Add("Action", "UPDATE");
            hst.Add("id", i);
            hst.Add("statename", txtbox_StateName.Text);
            hst.Add("stateabbr", txtbox_StateAbbrv.Text);
            hst.Add("country_id", ddl_countryid.SelectedItem.Value);
           
            int result = objData.ExecuteNonQuery("[sp_State]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_StateMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_ProvinceMaster.aspx?msg=" + msg);

                }

            }

        }
        catch
        {


        }

    }
    protected void ddl_countryid_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new clsInsert().getcuntryflag(Convert.ToInt32(ddl_countryid.SelectedItem.Value));

        if (dt.Rows.Count > 0)
        {

            imgflag.Height = 20;
            imgflag.Width = 30;
            imgflag.ImageUrl = "../FlagImage/Resized/" + dt.Rows[0]["Country_flag"].ToString().Trim();

        }
    }
}
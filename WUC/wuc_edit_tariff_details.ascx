﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_tariff_details.ascx.cs" Inherits="WUC_wuc_edit_tariff_details" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 158px;
    }
    .style4
    {
        width: 158px;
        height: 26px;
    }
    .style6
    {
        height: 26px;
    }
    .style7
    {
        width: 283px;
    }
    .style8
    {
        width: 283px;
        height: 26px;
    }
    .style9
    {
        width: 100px;
    }
    .style10
    {
        height: 26px;
        width: 100px;
    }
    .style11
    {
        width: 303px;
    }
    .style12
    {
        width: 303px;
        height: 26px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>Tariff details</legend>
<div>
    <table class="style1">
        <tr>
            <td class="style2">
                Tariff Name</td>
            <td class="style11">
                <asp:TextBox ID="txtbox_tariffname" runat="server" Height="23px" Width="264px" 
                    ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_tariffname" runat="server" 
                    ErrorMessage="*" ControlToValidate="txtbox_tariffname" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style7">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                Start Date</td>
            <td class="style11">
                <asp:DropDownList ID="ddl_startdate" runat="server" ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_startdate" runat="server" 
                    ErrorMessage="*" ControlToValidate="ddl_startdate" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style7">
                End Date</td>
            <td class="style9">
                <asp:DropDownList ID="ddl_enddate" runat="server" ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_enddate" runat="server" 
                    ErrorMessage="*" ControlToValidate="ddl_enddate" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Tariff Type</td>
            <td class="style11">
                <asp:RadioButton ID="rbtn_flattariff" GroupName="b" runat="server"  />
                <asp:RadioButton ID="rbtn_varibletariff"  GroupName="b" runat="server" 
                    oncheckedchanged="rbtn_varibletariff_CheckedChanged" />
            </td>
            <td class="style7">
                Fixed Time</td>
            <td class="style9">
                <asp:TextBox ID="txtbox_fixedtime" runat="server" Width="91px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Day Defination</td>
            <td class="style11">
                <asp:DropDownList ID="ddl_daydefination" runat="server" ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_daydefination" runat="server" 
                    ErrorMessage="*" ControlToValidate="ddl_daydefination" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style7">
                <asp:CheckBox ID="CheckBox1" runat="server" 
                    Text="Check Transaction in billing" />
            </td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                Rate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td class="style11">
                <asp:DropDownList ID="ddl_rate" runat="server" Height="16px" Width="272px" 
                    ValidationGroup="a">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfv_rate" runat="server" 
                    ErrorMessage="*" ControlToValidate="ddl_rate" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style7">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                Overall Max($)</td>
            <td class="style12">
                <asp:TextBox ID="txtbox_overallmax" runat="server"></asp:TextBox>
            </td>
            <td class="style8">
            </td>
            <td class="style10">
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style11">
                <asp:Button ID="btn_update" runat="server" Text="Update" Width="70px" 
                    ValidationGroup="a" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_cancel" runat="server" Text="Cencel" />
            </td>
            <td class="style7">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
        </tr>
    </table>
    </fieldset>
    </asp:Panel>
</div>

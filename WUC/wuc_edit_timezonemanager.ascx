﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_timezonemanager.ascx.cs" Inherits="WUC_wuc_edit_timezonemanager" %>
<style>
       .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

</style>
<div class="form">
<label>TimeZone ID:</label>
<asp:TextBox ID="txtbox_TimezoneId" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_TimezoneId" runat="server" 
                    ControlToValidate="txtbox_TimezoneId" 
                    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
<label> TimeZone Name:</label>
  <asp:TextBox ID="txtbox_TimezoneName" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_TimezoneName" runat="server" 
                    ControlToValidate="txtbox_TimezoneName" 
                    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
<label>&nbsp;</label> <asp:Button ID="btn_update" runat="server" Text="Update" CssClass=button
                    onclick="btn_submit_Click" />
</div>
    


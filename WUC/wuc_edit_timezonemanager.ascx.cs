﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_timezonemanager : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                txtbox_TimezoneId.Text = Session["TimeZoneId"].ToString();
                txtbox_TimezoneName.Text = Session["TimeZoneName"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_TimeZoneMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_timeZoneMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
              //  Response.Redirect("TimeZone.aspx");
            }

        }
    }
    string msg;
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            //string msg = "1";
            updateTime();
          //  Response.Redirect("Admin_TimeZoneMaster.aspx?msg=" + msg);


            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
           // Response.Redirect("Admin_TimeZoneMaster.aspx");
            // clear();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Try again.')", true);
        }
    }

    public void updateTime()
    {
        try
        {
            hst.Clear();

            int ID = Convert.ToInt32(Session["TimeZoneTblId"].ToString());
            hst.Add("action", "update");
            hst.Add("id", ID);
            hst.Add("timezonename", txtbox_TimezoneName.Text);
            hst.Add("TimeZoneId", txtbox_TimezoneId.Text);
            hst.Add("modifiedby","lovey_modifier");
            hst.Add("operatorid", "lovey_operator");
            hst.Add("DateOfChanging", DateTime.Now);
             
            int result = objData.ExecuteNonQuery("[sp_TimeZone]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_TimeZoneMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_timeZoneMaster.aspx?msg=" + msg);

                }

             //   Response.Redirect("Admin_TimeZoneMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
}
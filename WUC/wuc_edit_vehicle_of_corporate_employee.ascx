﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_edit_vehicle_of_corporate_employee.ascx.cs" Inherits="WUC_wuc_edit_vehicle_of_corporate_employee" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">
    .style1
    {
        width: 288px;
    }
    .style2
    {
        width: 190px;
        text-align: center;
    }
    .timezone-table
    {
        width: 1019px;
    }
    .button
    {
        text-align: right;
    }
</style>

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
 <div class="body-container">
<%--<table  class="reg-form" width="100%"><tr><td class="style2">Make&nbsp;&nbsp;&nbsp;&nbsp; </td>
    <td class="style1"><asp:DropDownList ID="ddl_make" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource2" DataTextField="VehiMake" 
                    DataValueField="VehiMakeID" CssClass="twitterStyleTextbox" Width="181px"><asp:ListItem Selected = "True" Text = "------Select Vehicle Make------" Value = "0"></asp:ListItem></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_VehicleMakes]"></asp:SqlDataSource><asp:RequiredFieldValidator ID="rfv_make" runat="server" 
                    ControlToValidate="ddl_make" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td></tr><tr><td class="style2">
        Model&nbsp;&nbsp;&nbsp; </td>
        <td class="style1">
                    
                    <asp:TextBox ID="txtbox_model" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox><asp:RequiredFieldValidator ID="rfv_model" runat="server" 
                    ControlToValidate="txtbox_model" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td></tr><tr><td class="style2">
        Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td class="style1"><asp:DropDownList ID="ddl_year" CssClass="twitterStyleTextbox" runat="server" ValidationGroup="a" Width="181px"><asp:ListItem Selected = "True" Text = "------Select Year------" Value = "0"></asp:ListItem></asp:DropDownList ><asp:RequiredFieldValidator ID="rfv_year" runat="server" 
                    ControlToValidate="ddl_year" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td></tr><tr><td class="style2">Color&nbsp;&nbsp;&nbsp; </td>
        <td class="style1"><asp:DropDownList ID="ddl_color" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="VehiColor" 
                    DataValueField="VehiColorid" CssClass="twitterStyleTextbox" Width="181px"><asp:ListItem Selected = "True" Text = "------Select Color------" Value = "0"></asp:ListItem></asp:DropDownList><asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Vehicle_color]"></asp:SqlDataSource><asp:RequiredFieldValidator ID="rfv_color" runat="server" 
                    ControlToValidate="ddl_color" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td></tr><tr><td class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        License Plate</td>
        <td class="style1"><asp:TextBox ID="txtbox_license" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" ></asp:TextBox><asp:RequiredFieldValidator ID="rfv_license" runat="server" 
                    ControlToValidate="txtbox_license" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td></tr><tr><td class="style2" align="right">
        &nbsp;</td><td class="style1"><asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="a"  CssClass="button"
                    onclick="btn_add_Click" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnclose" runat="server" CssClass="button" 
            onclick="btnclose_Click" Text="Close" /></td></tr></table>
                    <asp:Panel ID="Panel17" runat="server">
    <fieldset>
 <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:ListView ID="ListView2" runat="server" OnPagePropertiesChanging="ListView2_PagePropertiesChanging"
                OnItemEditing="ListView2_ItemEditing" OnItemCanceling="ListView2_ItemCanceling"
                OnItemUpdating="ListView2_ItemUpdating"              
                onitemdeleting="ListView2_ItemDeleting" 
                onselectedindexchanged="ListView2_SelectedIndexChanged" OnItemDataBound="OnItemDataBound">
                <EmptyDataTemplate>
                    <p>No Records Found..</p>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table id="Table1" runat="server"  width="900px" style="background-color: darkgrey;">
                        <tr id="Tr1" runat="server" style="background-color: silver;">
                            <td>
                            </td>
                            <td id="Td2" runat="server">
                            <b>Make</b>
                            </td>
                            <td id="Td1" runat="server">
                                <b>Model</b>
                            </td>
                            <td id="Td5" runat="server">
                                <b>Color</b>
                            </td>
                              <td id="Td3" runat="server">
                                <b>Year</b>
                            </td>
                            <td id="Td6" runat="server">
                                <b>Licence</b>
                            </td>
                        </tr>
                        <tr id="ItemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td style="width:120px;">
                            <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                             <asp:Button ID="Button1" Text="Delete" runat="server" CommandName="Delete" />
                        </td>
                      <td>
                        <asp:Label ID="empid" runat="server" Text='<%# Eval("empid") %>' Visible="false"></asp:Label>

                            <asp:Label ID="make" runat="server" Text='<%# Eval("make") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="model" runat="server" Text='<%# Eval("model") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="color" runat="server" Text='<%# Eval("color") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="year" runat="server" Text='<%# Eval("year") %>'></asp:Label>
                        </td>

                          <td>
                            <asp:Label ID="licence" runat="server" Text='<%# Eval("licence") %>'></asp:Label>
                        </td>
                     
                    </tr>
                </ItemTemplate>
                <EditItemTemplate>
                    <tr id="Tr1" runat="server" style="background-color: antiquewhite;">
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />
                        </td>
                       
                        <td>
                        <asp:Label ID="empid" runat="server" Text='<%# Eval("empid") %>' Visible="false"></asp:Label>
                           <asp:DropDownList ID="make" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource2" DataTextField="VehiMake" 
                    DataValueField="VehiMakeID" CssClass="twitterStyleTextbox"><asp:ListItem Selected = "True" Text = "------Select Vehicle Make------" Value = "0"></asp:ListItem></asp:DropDownList>
                      <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_VehicleMakes]"></asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:TextBox ID="model" Width="50px" runat="server" Text='<%# Eval("model") %>'></asp:TextBox>
                        </td>
                          <td>
              <asp:DropDownList ID="color" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="VehiColor" 
                    DataValueField="VehiColorid" CssClass="twitterStyleTextbox"><asp:ListItem Selected = "True" Text = "------Select Color------" Value = "0"></asp:ListItem></asp:DropDownList><asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Vehicle_color]"></asp:SqlDataSource>
                        </td>
                        <td>
                  <asp:DropDownList ID="year" CssClass="twitterStyleTextbox" runat="server" ValidationGroup="a"><asp:ListItem Selected = "True" Text = "------Select Year------" Value = "0"></asp:ListItem></asp:DropDownList >
                          <asp:Label ID="Label1" runat="server" Text='<%# Eval("year") %>' Visible="false"></asp:Label>     
                        </td>
                        <td>
                            <asp:TextBox ID="licence" Width="50px" runat="server" Text='<%# Eval("licence") %>'></asp:TextBox>
                        </td>
                    
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListView2" PageSize="5">
        <Fields>
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
    </fieldset>
    </asp:Panel>--%>






    
                    <asp:Panel ID="Panel9" runat="server"  Width="100%" CssClass="parker-table">
                      <fieldset>
                    
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        DeleteCommand="DELETE FROM [tbl_CorporateEmployeeVehicleDetails] WHERE [EmployeeVehicleID] = @EmployeeVehicleID" 
        InsertCommand="INSERT INTO [tbl_CorporateEmployeeVehicleDetails] ([EmployeeID], [MakeID], [ModelID], [Year], [LicenseNo], [ColorID]) VALUES (@Parker_id, @Make, @Model, @Year, @LicenseNo, @Color)" 
        SelectCommand="SELECT tbl_CorporateEmployeeVehicleDetails.*,tbl_Vehicle_color.*,tbl_VehicleMakes.* FROM [tbl_CorporateEmployeeVehicleDetails] inner join tbl_Vehicle_color on tbl_Vehicle_color.VehiColorid=tbl_CorporateEmployeeVehicleDetails.ColorID inner join tbl_VehicleMakes on tbl_VehicleMakes.VehiMakeID=tbl_CorporateEmployeeVehicleDetails.MakeID WHERE [EmployeeID]=@Parker_id " 
         UpdateCommand="UPDATE [tbl_CorporateEmployeeVehicleDetails] SET  [MakeID] = @Make, [ModelID] = @Model, [Year] = @Year,  [LicenseNo] = @LicenseNo ,[ColorID] = @Color WHERE [EmployeeVehicleID] = @EmployeeVehicleID" 
                              onselecting="SqlDataSource2_Selecting">
        <DeleteParameters>
            <asp:Parameter Name="EmployeeVehicleID" Type="Int64" />
        </DeleteParameters>
       <InsertParameters>
            
            <asp:Parameter Name="Make" Type="Int64" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
              <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="Color" Type="Int64" />
          
           
            <asp:SessionParameter Name="Parker_id" SessionField="Parker_id" 
                Type="Int64" />
        
        </InsertParameters>
        <SelectParameters>
             <asp:SessionParameter Name="Parker_id" SessionField="Parker_id" 
                Type="Int64" />
        </SelectParameters>
        <UpdateParameters>
            
            <asp:Parameter Name="Make" Type="Int64" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="Int64" />
            <asp:Parameter Name="LicenseNo" Type="String" />
          
            <asp:Parameter Name="EmployeeVehicleID" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource9" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
        SelectCommand="SELECT * FROM [tbl_VehicleMakes] ORDER BY [VehiMake]" 
                              onselecting="SqlDataSource9_Selecting">
    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource10" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_Vehicle_color]" 
                              onselecting="SqlDataSource10_Selecting">
        </asp:SqlDataSource>
                    
                      <asp:ListView ID="ListView1" runat="server" DataKeyNames="EmployeeVehicleID" 
        DataSourceID="SqlDataSource2" InsertItemPosition="LastItem" 
        onselectedindexchanged="ListView1_SelectedIndexChanged">
        <AlternatingItemTemplate>
            <tr style="background-color: #FAFAD2;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button"  
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button" />
                </td>
                
                <td >
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="background-color: #FFCC66;color: #000080;">
                <td> 
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" CssClass="button"  
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="button" 
                        Text="Cancel" />
                </td>
                
                <td>
               <%-- <asp:Label ID="ParkerVehicleIdLabel1" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' Visible="false" />
                        <asp:TextBox ID="Parker_IdTextBox" runat="server" 
                        Text='<%# Bind("Parker_Id") %>' Visible="false" />--%>
                 <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource9" DataTextField="VehiMake" 
                DataValueField="VehiMakeID" SelectedValue='<%# Bind("VehiMakeID") %>'> </asp:DropDownList>

                    
                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("ModelID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
              

                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource10" DataTextField="VehiColor" 
                DataValueField="VehiColorid" SelectedValue='<%# Bind("ColorID") %>'> </asp:DropDownList>
                  
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table id="Table1" runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" CssClass="add"  
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" CssClass="cancel"  
                        Text="Clear" />
                </td>
                 
                <td>
                    
                    <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource9" DataTextField="VehiMake" 
                DataValueField="VehiMakeID" SelectedValue='<%# Bind("Make", "{0}") %>'> </asp:DropDownList>
                    

                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                 <asp:DropDownList ID="ColorTextBox" runat="server" 
                DataSourceID="SqlDataSource10" DataTextField="VehiColor" 
                DataValueField="VehiColorid" SelectedValue='<%# Bind("Color", "{0}") %>'> </asp:DropDownList>
                  <%--  <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />--%>
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
              
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="background-color: #FFFBD6;color: #333333;">
                <td>
             
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="button" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="button"  />
                </td>
               
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td >
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td >
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
               
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table id="Table2" runat="server">
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="1"
                            style=" background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family:Open Sans; width:100%;">
                            <tr id="Tr2" runat="server" style="background-color: #FFFBD6;color: #333333;">
                                <th id="Th1" runat="server">
                                </th>
                               <%-- <th id="Th2" runat="server">
                                    ParkerVehicleId</th>
                                <th id="Th3" runat="server">
                                    Parker_Id</th>--%>
                                <th id="Th4"  runat="server">
                                    Make</th>
                                <th id="Th5" runat="server">
                                    Model</th>
                                <th id="Th6" runat="server">
                                    Year</th>
                                <th id="Th7" runat="server">
                                    Color</th>
                                <th id="Th8" runat="server">
                                    LicenseNo</th>
                                
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td2" runat="server" 
                        style="text-align: center;background-color: #6699ff;font-family: Verdana, Arial, Helvetica, sans-serif;color: #333333;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                    ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #FFCC66;font-weight: bold;color: #000080;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" CssClass="delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="edit" />
                </td>
                <td>
                    <%--<asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />--%>
                </td>
                <td>
                   <%-- <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />--%>
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("VehiMake") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("ModelID") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("VehiColor") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                   <%-- <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />--%>
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
     </fieldset>
                      
                      </asp:Panel>
    <asp:Button ID="Button1" runat="server" Text="Close" CssClass="button" 
                        onclick="Button1_Click" />
      </div>

    </div>

    </div>

        <div class="footer-container"></div>













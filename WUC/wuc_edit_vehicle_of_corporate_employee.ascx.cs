﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_edit_vehicle_of_corporate_employee : System.Web.UI.UserControl
{
    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            
        }
     }
    DataTable dt = new DataTable();
    public void insertvehicledetailsinlist()
    {
        try
        {
            DataTable dt_vehicle = new DataTable();

            dt_vehicle.Columns.Add("make", typeof(string));
            dt_vehicle.Columns.Add("model", typeof(string));
            dt_vehicle.Columns.Add("color", typeof(string));
            dt_vehicle.Columns.Add("year", typeof(string));
            dt_vehicle.Columns.Add("licence", typeof(string));
            dt_vehicle.Columns.Add("empid", typeof(int));

            // DataSet vehicle_bind = new clsInsert().fetchrec("select * from tbl_CorporateEmployeeVehicleDetails where EmployeeID="+Convert.ToInt32( Session["empid"]));
            DataSet vehicle_bind = new clsInsert().fetchrec("select  tbl_CorporateEmployeeVehicleDetails.* ,tbl_Vehicle_color.VehiColor,tbl_VehicleMakes.VehiMake from tbl_CorporateEmployeeVehicleDetails inner join tbl_Vehicle_color on tbl_Vehicle_color.VehiColorid=tbl_CorporateEmployeeVehicleDetails.ColorID inner join tbl_VehicleMakes on tbl_VehicleMakes.VehiMakeID=tbl_CorporateEmployeeVehicleDetails.MakeID where EmployeeID=" + Convert.ToInt32(Session["empid_edit"].ToString()));

            for (int i = 0; i < vehicle_bind.Tables[0].Rows.Count; i++)
            {
                DataRow dr = dt_vehicle.NewRow();
                dr[0] = vehicle_bind.Tables[0].Rows[i][8].ToString();
                dr[1] = vehicle_bind.Tables[0].Rows[i][3].ToString();
                dr[2] = vehicle_bind.Tables[0].Rows[i][7].ToString();
                dr[3] = vehicle_bind.Tables[0].Rows[i][4].ToString();
                dr[4] = vehicle_bind.Tables[0].Rows[i][5].ToString();
                dr[5] = vehicle_bind.Tables[0].Rows[i][0].ToString();

                dt_vehicle.Rows.Add(dr);
            }

            //ListView2.DataSource = dt_vehicle;
           // ListView2.DataBind();
        }
        catch
        {
        }

    }
    
    protected void ListView2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ListView2_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {

       // DataPager1.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
        //insertvehicledetailsinlist();
    }

    protected void ListView2_ItemEditing(object sender, ListViewEditEventArgs e)
    {
     //   ListView2.EditIndex = e.NewEditIndex;
       
       // insertvehicledetailsinlist();
    }
   
  


    
    public void insertvehicledetails()
    {



        //try
        //{
        //    hst.Clear();


        //    hst.Add("action", "insertvehicledetails");
        //    hst.Add("employeeid", Convert.ToInt32(Session["empid_edit"].ToString()));
        //    hst.Add("licenseno", txtbox_license.Text);
        //    hst.Add("makeid", ddl_make.SelectedValue);
        //    hst.Add("color", ddl_color.SelectedValue);

        //    hst.Add("modelid", txtbox_model.Text);
        //    hst.Add("year", ddl_year.Text);







        //    int result = objData.ExecuteNonQuery("[Sp_employeeDetailsCorporate]", CommandType.StoredProcedure, hst);
        //    if (result > 0)
        //    {
        //        insertvehicledetailsinlist();
        //       // dt = (DataTable)Session["datatable"];
        //       // DataRow dr = dt.NewRow();
        //       // dr[0] = ddl_make.SelectedItem.Text;
        //       // dr[1] = txtbox_model.Text;
        //       // dr[2] = ddl_year.SelectedItem.Text;
        //       // dr[3] = ddl_color.SelectedItem.Text;
        //       // dr[4] = txtbox_license.Text;
        //       //// dr[5] = ddl_emp_code.SelectedItem.Text;

        //       // dt.Rows.Add(dr);

        //       // Session["datatable"] = dt;
        //       // if (dt.Rows.Count > 0)
        //       // {
        //       //     insertvehicledetailsinlist();
        //       //    // Panel2.Visible = true;
        //       //   //  rpt_addvehicle.DataSource = dt;
        //       //    // rpt_addvehicle.DataBind();
        //       // }
        //    }
        //}
        //catch
        //{

        //}





    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
            insertvehicledetails();

        }
        catch
        {
        }
    }
    protected void btnclose_Click(object sender, EventArgs e)
    {
        Session["datatable"] = null;

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('Admin_Add_Details_Of_Corporate_Parker.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

    }
    protected void SqlDataSource2_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSource9_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSource10_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void ListView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "CloseWindow", "window.close('Admin_Add_Details_Of_Corporate_Parker.aspx','New Windows','height=380, width=600,location=no','titlebar= no; toolbar= no; statusbar=no');", true);

    }
}
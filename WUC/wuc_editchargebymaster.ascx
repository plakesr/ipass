﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_editchargebymaster.ascx.cs" Inherits="WUC_wuc_editchargebymaster" %>
<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

    .style2
    {
        width: 98px;
    }
    .style3
    {
        width: 382px;
    }
</style>
<div class="form">
<label>Charge Name</label>
 <asp:TextBox ID="txtbox_chargename" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_chargename" runat="server" 
                    ControlToValidate="txtbox_chargename" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
                    <label>Description</label>
                     <asp:TextBox ID="txtbox_chargedescription" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_chargedescription" runat="server" 
                    ControlToValidate="txtbox_chargedescription" ErrorMessage="*" 
                    ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                    <div class="clearfix"></div>
                    <label>&nbsp;</label> <asp:Button ID="btn_update" runat="server" Text="Update" ValidationGroup="a"  
                    CssClass=button onclick="btn_add_Click1"/>

    </div>
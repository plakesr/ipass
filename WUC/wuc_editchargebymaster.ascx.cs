﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_editchargebymaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                txtbox_chargename.Text = Session["chargename"].ToString();
                txtbox_chargedescription.Text = Session["description"].ToString();
            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_ChargebyMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_ChargebyMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }

        }
    }
    string msg;
    protected void btn_add_Click1(object sender, EventArgs e)
    {
        try
        {
          //  string msg = "1";
            updatechargeby();
            //Response.Redirect("Admin_ChargebyMaster.aspx?msg=" + msg);

        }
        catch
        {
        }
    }
    string s = "true";
    public void updatechargeby()
    {
        try
        {
            // hst.Clear();

            hst.Add("action", "update");
            hst.Add("chargebyid", Convert.ToInt32(Session["id"].ToString()));

            hst.Add("chargebyname",txtbox_chargename.Text);
            hst.Add("descr", txtbox_chargedescription.Text);


            int result = objData.ExecuteNonQuery("[sp_Chargeby]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_ChargebyMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_ChargebyMaster.aspx?msg=" + msg);

                }
               // Response.Redirect("Admin_ChargebyMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Do Not Update Please Try Again.')", true);

            //lbl_error.Visible = true;

        }

    }
}
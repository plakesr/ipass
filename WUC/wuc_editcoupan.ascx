﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_editcoupan.ascx.cs" Inherits="WUC_wuc_editcoupan" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .style2
    {
        height: 23px;
    }
     .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}



</style>

<script type="text/javascript">

    function IsOneDecimalPoint(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode; // restrict user to type only one . point in number
        var parts = evt.srcElement.value.split('.');
        if (parts.length > 1 && charCode == 46)
            return false;
        return true;
    }
</script>
<asp:Panel ID="Panel1" runat="server">

  <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
<table class="form-table">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<tr>
<td>
    Customer Name</td>
<td>
    <asp:TextBox ID="txtbox_customername" runat="server" Enabled="False"></asp:TextBox>
</td>
</tr>
<tr>
<td>
    Campus Name</td>
<td>
    <%--<asp:DropDownList ID="ddl_campusname" runat="server" AutoPostBack="True" 
        DataSourceID="SqlDataSource2" DataTextField="Campus_Name" CssClass=twitterStyleTextbox 
        DataValueField="CampusID" AppendDataBoundItems="True" 
        ondatabound="ddl_campusname_SelectedIndexChanged" >
       
    </asp:DropDownList>--%>
   
    
    <asp:TextBox ID="txtbox_campusname" runat="server" Enabled="False"></asp:TextBox>
   
    
</td>
</tr>
<tr>
<td>
    Lot Name</td>
<td>
   <%-- <asp:DropDownList ID="ddl_lotname" runat="server" 
        DataSourceID="SqlDataSource3"  CssClass=twitterStyleTextbox
        DataTextField="LotName" DataValueField="Lot_id" 
        AppendDataBoundItems="true" 
        onselectedindexchanged="ddl_lotname_SelectedIndexChanged" >
      
    </asp:DropDownList>--%>
   
   
    <asp:TextBox ID="lotname" runat="server" Enabled="False"></asp:TextBox>
   
   
</td>
</tr>

<tr>
        <td>
            Coupon Type</td>
        <td>
            <asp:DropDownList ID="ddl_CoupanType" runat="server" 
                CssClass="twitterStyleTextbox"  Enabled="false">
                <asp:ListItem Value="0">Select Parking Type</asp:ListItem>
                        <asp:ListItem Value="1">Reserved Parking</asp:ListItem>
                        <asp:ListItem Value="2">Random Parking</asp:ListItem>
                        
                    </asp:DropDownList>
                   
        </td>
    </tr>
    <tr>
    
        <td class="style2">
            Coupon Series Name</td>
        <td class="style2">
            <asp:TextBox ID="txtbox_Coupanseries" runat="server" Enabled="false"
                CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender0" 
                runat="server" Enabled="True" TargetControlID="txtbox_Coupanseries" WatermarkText="Enter Coupan Series Name" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
        </td>
    </tr>
    
    <tr>
        <td>
            Card Quantity</td>
        <td>
            <asp:TextBox ID="txtbox_Quantity" runat="server" CssClass="twitterStyleTextbox" AutoPostBack="true" 
                ontextchanged="txtbox_Quantity_TextChanged"></asp:TextBox>
            
            <cc1:FilteredTextBoxExtender ID="txtbox_Quantity_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_Quantity" FilterMode="ValidChars" FilterType="Numbers">
            </cc1:FilteredTextBoxExtender>
            
            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                runat="server" Enabled="True" TargetControlID="txtbox_Quantity" WatermarkText="Enter Quantity" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>
            <asp:Label ID="Label1" runat="server" Visible="False" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Amount</td>
        <td>
            <asp:TextBox ID="txtbox_CoupanDiscount" runat="server" 
                CssClass="twitterStyleTextbox" onkeypress="return IsOneDecimalPoint(event);" ></asp:TextBox>

               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom"
  ValidChars="01234567890." TargetControlID="txtbox_CoupanDiscount"></cc1:FilteredTextBoxExtender>
            <cc1:TextBoxWatermarkExtender ID="txtbox_CoupanDiscount_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_CoupanDiscount" 
                WatermarkCssClass="watermark" WatermarkText="Enter Coupan Discount">
            </cc1:TextBoxWatermarkExtender>
        </td>
    </tr>
    <tr>
        <td>
            Type</td>
        <td>
            <asp:DropDownList ID="ddlchargeby" runat="server" AutoPostBack="True"  onselectedindexchanged="DropDownList1_SelectedIndexChanged" Enabled="false">


                 <asp:ListItem Value="0">Select Charge Type</asp:ListItem>
                        <asp:ListItem Value="1">Hourly</asp:ListItem>
                        <asp:ListItem Value="2">Daily</asp:ListItem>
                        <asp:ListItem Value="3">Monthly</asp:ListItem>

                        <asp:ListItem Value="4">Weekly</asp:ListItem>

            </asp:DropDownList>
           
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lbl_type" runat="server" ></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtbox_type" runat="server" ></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtbox_type_FilteredTextBoxExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_type" FilterMode="ValidChars" FilterType="Numbers">
            </cc1:FilteredTextBoxExtender>
        </td>
    </tr>
    <tr>
        <td>
            Start Date</td>
        <td>
            <asp:TextBox ID="txtbox_StartDate" runat="server" CssClass="disable_past_dates" 
                Enabled="False" ></asp:TextBox>
            <cc1:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtbox_StartDate">
            </cc1:CalendarExtender>
         <%--   <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" 
                runat="server" Enabled="True" TargetControlID="txtbox_StartDate" WatermarkText="Select Start Date" WatermarkCssClass="watermark">
            </cc1:TextBoxWatermarkExtender>--%>
        </td>
    </tr>
    
    
</table>
                                               </ContentTemplate>
                     <Triggers>
      
        <asp:AsyncPostBackTrigger ControlID="txtbox_Quantity" EventName="TextChanged" />
       

    </Triggers>
                </asp:UpdatePanel>

 <asp:Button ID="btn_update" runat="server" Text="Update" CssClass=button
                onclick="btn_submit_Click" />

</asp:Panel>
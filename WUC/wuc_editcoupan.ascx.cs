﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_editcoupan : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    string stat = "true";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                DataTable dtcoupandeta = new clsInsert().fullcoupandetails(Session["coupan_series"].ToString());
                if (dtcoupandeta.Rows.Count > 0)
                {
                    txtbox_Coupanseries.Text = dtcoupandeta.Rows[0]["CoupanSeries"].ToString();
                    Session["couponname"] = txtbox_Coupanseries.Text;
                 
                    txtbox_Quantity.Text = dtcoupandeta.Rows.Count.ToString();
                    Session["totalcoupon"] = txtbox_Quantity.Text;
                    txtbox_CoupanDiscount.Text = dtcoupandeta.Rows[0]["CoupanDiscount"].ToString();
                    txtbox_StartDate.Text = dtcoupandeta.Rows[0]["starting_date"].ToString();
                    Session["Startdate"] = dtcoupandeta.Rows[0]["starting_date"].ToString();
                    if (dtcoupandeta.Rows[0]["ParkingType"].ToString() == "1")
                    {
                        ddl_CoupanType.SelectedValue = "1";
                    }
                    if (dtcoupandeta.Rows[0]["ParkingType"].ToString() == "2")
                    {
                        ddl_CoupanType.SelectedValue = "2";
                    }


                    if (dtcoupandeta.Rows[0]["chargeby"].ToString() == "1")
                    {
                        ddlchargeby.SelectedValue = "1";
                        lbl_type.Text = "Number of Hours";
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        str = dtcoupandeta.Rows[0]["Quantity"].ToString();
                        char[] splitchar = { '.' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length; count++)
                        {
                            txtbox_type.Text = Convert.ToString(strArr[0]);

                        }
                    }

                    if (dtcoupandeta.Rows[0]["chargeby"].ToString() == "2")
                    {
                        ddlchargeby.SelectedValue = "2";
                        lbl_type.Text = "Number of Days";
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        str = dtcoupandeta.Rows[0]["Quantity"].ToString();
                        char[] splitchar = { '.' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length; count++)
                        {
                            txtbox_type.Text = Convert.ToString(strArr[0]);

                        }
                    }
                    if (dtcoupandeta.Rows[0]["chargeby"].ToString() == "3")
                    {
                        ddlchargeby.SelectedValue = "3";
                        lbl_type.Text = "Number of Months";

                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        str = dtcoupandeta.Rows[0]["Quantity"].ToString();
                        char[] splitchar = { '.' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length; count++)
                        {
                            txtbox_type.Text = Convert.ToString(strArr[0]);
                           
                        }
                      
                    }

                    if (dtcoupandeta.Rows[0]["chargeby"].ToString() == "4")
                    {
                        ddlchargeby.SelectedValue = "4";
                        
                        lbl_type.Text = "Number of Weeks";
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        str = dtcoupandeta.Rows[0]["Quantity"].ToString();
                        char[] splitchar = { '.' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length; count++)
                        {
                            txtbox_type.Text = Convert.ToString(strArr[0]);

                        }
                    }
                    try
                    {
                        DataTable dt_sitedetails = new clsInsert().sitename(Convert.ToInt32(dtcoupandeta.Rows[0]["Lot_Id"].ToString()));
                        if (dt_sitedetails.Rows.Count > 0)
                        {
                            txtbox_campusname.Text = dt_sitedetails.Rows[0]["Campus_Name"].ToString();
                            txtbox_customername.Text = dt_sitedetails.Rows[0]["SiteName"].ToString();

                            lotname.Text = dt_sitedetails.Rows[0]["LotName"].ToString();

                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

    }
    int number = 0;
    public void updateCoupan()
    {
        try
        
        {
            int val = Convert.ToInt32(txtbox_Quantity.Text.ToString());
            int runfor = val - Convert.ToInt32(Session["number"].ToString());
            for (int i = 1; i <= runfor; i++)
            {
                hst.Clear();
                number = Convert.ToInt32(Session["number"].ToString()) + i;
                string coupan = txtbox_Coupanseries.Text + number.ToString();
                hst.Add("action", "update");

                DataTable dtlotid = new clsInsert().lotid(txtbox_Coupanseries.Text);


                hst.Add("lot_id", Convert.ToInt32(dtlotid.Rows[0][8].ToString()));
                hst.Add("coupan", coupan);
                //  @coupan,@lot_id,@series,@number,@type,@discount,@status,@startdate,@enddate
                hst.Add("series", txtbox_Coupanseries.Text);
                hst.Add("number", number);


                hst.Add("type", ddl_CoupanType.SelectedItem.Text);

                hst.Add("discount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));
                hst.Add("startdate", Convert.ToDateTime(txtbox_StartDate.Text));
                hst.Add("id", Convert.ToInt32(dtlotid.Rows[0][10].ToString()));
                if (ddlchargeby.SelectedItem.Text == "Monthly")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddMonths(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Hourly")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddHours(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Daily")
                {
                    if (txtbox_type.Text != "")
                    {
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                    }
                }
                if (ddlchargeby.SelectedItem.Text == "Weekly")
                {
                    if (txtbox_type.Text != "")
                    {
                        int multi = Convert.ToInt32(txtbox_type.Text);
                        int daysofweek = 7;
                        int weekdays = daysofweek * multi;
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(weekdays));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                       Session["lastdate"] = enddate;

                    }
                }
                hst.Add("status", stat);
                hst.Add("activestatus", stat);

                int result = objData.ExecuteNonQuery("[sp_Coupan]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                    //clear();
                    //Response.Redirect("Admin_CityMaster.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }

            try
            {
                hst.Clear();
                hst.Add("action", "updateold");
                hst.Add("discount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));

                

                if (Session["Typeofcoupan"].ToString() == "Hourly")
                {
                   DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddHours(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                }

                if (Session["Typeofcoupan"].ToString() == "Daily")
                {
                   
                        DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));
                }

                if (Session["Typeofcoupan"].ToString() == "Monthly")
                {
                    DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddMonths(Convert.ToInt32(txtbox_type.Text));
                        hst.Add("enddate", Convert.ToDateTime(enddate));

                  //  hst.Add("enddate", Convert.ToDateTime(Session["Startdate"].ToString()).AddMonths(txtbox_Quantity);
                }

                 if (Session["Typeofcoupan"].ToString() == "Weekly")
                {
                    int multi = Convert.ToInt32(txtbox_type.Text);
                    int daysofweek = 7;
                    int weekdays = daysofweek * multi;
                    DateTime enddate = Convert.ToDateTime(txtbox_StartDate.Text).AddDays(Convert.ToInt32(weekdays));
                    hst.Add("enddate", Convert.ToDateTime(enddate));
                }
                hst.Add("coupan", Session["coupan_series"].ToString());

                int result11 = objData.ExecuteNonQuery("[sp_Coupan]", CommandType.StoredProcedure, hst);
                if (result11 > 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
                    //clear();
                    //Response.Redirect("Admin_CityMaster.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }


            }
            catch
            {
            }
















            try
            {
                //dt1 = (DataTable)Session["dbdet"];
                hst.Clear();

                hst.Add("action", "update");
                DataTable dtlotrateid1 = new clsInsert().lotid(txtbox_Coupanseries.Text);
                hst.Add("lotrateid", Convert.ToInt32(dtlotrateid1.Rows[0]["PlanRateID"].ToString()));


               
              
                hst.Add("amount", Convert.ToDecimal(txtbox_CoupanDiscount.Text));
                if (txtbox_type.Text != "")
                {
                    hst.Add("quantity", Convert.ToDecimal(txtbox_type.Text));
                }
                if (txtbox_type.Text == "")
                {
                    decimal defaultvalue = Convert.ToDecimal(0.00);
                    hst.Add("quantity", defaultvalue);

                }

             



                int result_lotratemaster = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
                if (result_lotratemaster > 0)
                {

                }
            }
            catch
            {
            }










        }

           // if(txtbox_CoupanDiscount.Text==
        catch
        {
            //lbl_error.Visible = true;

        }

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            updateCoupan();
            Response.Redirect("Admin_CoupanMaster.aspx?msg=true");
        }
        catch
        {
        }
    }
    protected void txtbox_Quantity_TextChanged(object sender, EventArgs e)
    {

        int total_coupon = Convert.ToInt32( Session["totalcoupon"]);
        int newquantity = Convert.ToInt32(txtbox_Quantity.Text);
        if (newquantity <=total_coupon)
        {
            Label1.Visible = true;
            Label1.Text = "Enter Coupon Series Greater Than" + Session["totalcoupon"].ToString();
            txtbox_Quantity.Text = Session["totalcoupon"].ToString();
        }
        else
        {
            Label1.Visible = false;
        }
       
    }
}
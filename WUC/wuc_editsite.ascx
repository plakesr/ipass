﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_editsite.ascx.cs" Inherits="WUC_wuc_editsite" %>

 
<style type="text/css">
    .style3
    {
        width: 186px;
    }
    .style4
    {
        width: 499px;
    }
      .twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

    
    
    </style>

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:Panel ID="Panel1" runat="server">
        <div class="form" style="width:100%">
        <label>Customer Name</label>
        <asp:TextBox ID="txtbox_SiteName" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_SiteName" runat="server" 
                                                       ControlToValidate="txtbox_SiteName" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:RequiredFieldValidator>
                                                       <div class="clearfix"></div>
<label>Time Zone</label>
 <asp:DropDownList ID="ddl_TimeZone" runat="server" CssClass="twitterStyleTextbox">
                                                   </asp:DropDownList>
                                                   <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                                       ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                       SelectCommand="SELECT [TimeZoneTblId], [TimeZoneName] FROM [tbl_TimeZoneMaster]">
                                                   </asp:SqlDataSource>
                                                   <div class="clearfix"></div>
<label>Address</label>
 <asp:TextBox ID="txtbox_Adress1" runat="server" TextMode="MultiLine" 
                                                       ValidationGroup="a" 
                CssClass="twitterStyleTextbox" Style="resize:none" 
                ontextchanged="txtbox_Adress1_TextChanged"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_Adress1" runat="server" 
                                                       ControlToValidate="txtbox_Adress1" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a"></asp:RequiredFieldValidator>
                                                       <div class="clearfix">&nbsp;</div>
<label>Country</label>
 <asp:DropDownList ID="ddl_country" runat="server" AutoPostBack="True" 
                                                       DataSourceID="SqlDataSource2" DataTextField="Country_name" 
                                                       DataValueField="Country_id" CssClass="twitterStyleTextbox">
                                                   </asp:DropDownList>
                                                   <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                                       ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                       SelectCommand="SELECT * FROM [tbl_country] ORDER BY Country_name"></asp:SqlDataSource>
                                                       <div class="clearfix"></div>
<label>Province</label>
 <asp:DropDownList ID="ddl_province" runat="server" AutoPostBack="True" 
                                                       DataSourceID="SqlDataSource6" DataTextField="ProvinceName" 
                                                       DataValueField="ProvinceId" CssClass="twitterStyleTextbox">
                                                   </asp:DropDownList>
                                                   <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                                                       ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                       SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName">
                                                       <SelectParameters>
                                                           <asp:ControlParameter ControlID="ddl_country" Name="Country_id" 
                                                               PropertyName="SelectedValue" Type="Int64" />
                                                       </SelectParameters>
                                                   </asp:SqlDataSource>
                                                   <div class="clearfix"></div>
<label>City</label>
<asp:DropDownList ID="ddl_city" runat="server" DataSourceID="SqlDataSource5" 
                                                       DataTextField="City_name" DataValueField="City_id" CssClass="twitterStyleTextbox">
                                                   </asp:DropDownList>
                                                   <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                                       ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                       
                                                       SelectCommand="SELECT [City_id], [City_name] FROM [tbl_City] WHERE ([Province_id] = @Province_id) order by City_name">
                                                       <SelectParameters>
                                                           <asp:ControlParameter ControlID="ddl_province" Name="Province_id" 
                                                               PropertyName="SelectedValue" Type="Int64" />
                                                       </SelectParameters>
                                                   </asp:SqlDataSource>
                                                   <div class="clearfix">
                                                       <asp:RequiredFieldValidator ID="rfv_city" runat="server" 
                                                           ControlToValidate="ddl_city" ErrorMessage="*" ForeColor="#CC0000" 
                                                           ValidationGroup="b"></asp:RequiredFieldValidator>
            </div>
<label>Postal Code/Zip Code</label>
 <asp:TextBox ID="txtbox_zip" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_zip" runat="server" 
                                                       ControlToValidate="txtbox_zip" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a"></asp:RequiredFieldValidator>
                                                       <div class="clearfix"></div>
<label>Phone Number(Home)</label>
 <asp:TextBox ID="txtbox_Phome" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_Phome" runat="server" 
                                                       ControlToValidate="txtbox_Phome" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a"></asp:RequiredFieldValidator>
                                                       <div class="clearfix"></div>
<label>Cellular</label>
 <asp:TextBox ID="txtbox_Cellular" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_Cellular" runat="server" 
                                                       ControlToValidate="txtbox_Cellular" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a"></asp:RequiredFieldValidator>
                                                       <div class="clearfix"></div>
<label> Phone Number(Office)</label>
 <asp:TextBox ID="txtbox_poffice" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_poffice" runat="server" 
                                                       ControlToValidate="txtbox_poffice" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a"></asp:RequiredFieldValidator>
                                                       <div class="clearfix"></div>
<label>Email</label>
 <asp:TextBox ID="txtbox_email" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                                                       ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                                                       ValidationGroup="a"></asp:RequiredFieldValidator>
                                                       <div class="clearfix"></div>
<div class="map-area">
 <div id="map_canvas" style="width:352px; height:300px"></div>
<label for="latitude">
                    Latitude</label>
  <asp:TextBox ID="latitude" runat="server" Enabled="False"></asp:TextBox>

    
    <label for="longitude">
                    Longitude </label>
 <asp:TextBox ID="longitude" runat="server" Enabled="False"></asp:TextBox>
        </div>     

                </div>
         </asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

              <asp:Panel ID="Panel3" runat="server">
              <fieldset><legend>Contact Person Details</legend>
              <div class="form" style=" border:solid 1px #ccc; width:98%; padding:10px; margin:10px 0;">
                 <div class="clearfix">&nbsp;</div>
              <label> Owner Name</label>
                <asp:TextBox ID="txtbox_ownerName" runat="server" ValidationGroup="a" 
                        ontextchanged="txtbox_ownerName_TextChanged" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ownername" runat="server" 
                        ControlToValidate="txtbox_ownerName" 
                        ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                        <div class="clearfix"></div>
<label>Cellular</label>
  <asp:TextBox ID="txtbox_ownercellular" runat="server" ValidationGroup="a"  CssClass="twitterStyleTextbox"
                       ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_ownercellular" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                         <div class="clearfix"></div>
<label> Phone Number(Office)</label>
   <asp:TextBox ID="txtbox_owneroffice" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtbox_owneroffice" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                         <div class="clearfix"></div>
<label>  Email</label>
  <asp:TextBox ID="txtbox_owneremail" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtbox_owneremail" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                
              </div>
           
                    </fieldset>
                        </asp:Panel>
                    
        <div class="clearfix"></div>
 
     <asp:Button ID="btn_update" runat="server" Text="Update" 
    ValidationGroup="a" class="button"
    onclick="btn_save_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" 
    ValidationGroup="b" class="button" onclick="btn_cancel_Click" />
<div class="clearfix">&nbsp;</div>
     


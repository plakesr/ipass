﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_editsite : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            clsInsert onj = new clsInsert();
            DataSet rec = onj.fetchrec("SELECT [TimeZoneTblId], [TimeZoneName], [TimeZoneId] FROM [tbl_TimeZoneMaster] ORDER BY [TimeZoneName]");
            DataTable t = new DataTable("timezone");
            t.Columns.Add("TimeZoneTblId", typeof(int));
            t.Columns.Add("TimeZoneName", typeof(string));

            for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
            {
                DataRow dr = t.NewRow();
                dr[0] = rec.Tables[0].Rows[i][0].ToString();
                dr[1] = rec.Tables[0].Rows[i][2].ToString() + " [" + rec.Tables[0].Rows[i][1].ToString() + "]";
                t.Rows.Add(dr);
            }


            ddl_TimeZone.DataSource = t;
            ddl_TimeZone.DataTextField = "TimeZoneName";
            ddl_TimeZone.DataValueField = "TimeZoneTblId";
            ddl_TimeZone.DataBind();
            try
            {

            //   = siteid.Text
                txtbox_SiteName.Text = Session["SiteName"].ToString();
                txtbox_Adress1.Text = Session["Address1"].ToString();
                //txtbox_Adress2.Text = Session["Address2"].ToString();
             //   ddl_SiteType.SelectedValue = Session["SiteType"].ToString();
                txtbox_zip.Text= Session["Zip"].ToString();

                txtbox_Phome.Text = Session["sitephone"].ToString();
                txtbox_Cellular.Text = Session["SiteCell"].ToString();
                txtbox_poffice.Text = Session["siteoffice"].ToString();
             //ddl_city.SelectedValue = Session["sitecity"].ToString();
                txtbox_email.Text = Session["siteemail"].ToString();
               latitude.Text = Session["latitude"].ToString();
                longitude.Text = Session["longitude"].ToString();
                ddl_TimeZone.SelectedValue = Session["Timezone"].ToString();
                txtbox_ownerName.Text = Session["OwnerName"].ToString();
                //txtbox_owneradress1.Text = Session["OwnerAddress"].ToString();

                //txtbox_ownercity.Text = Session["OwnerCity"].ToString();

                txtbox_ownercellular.Text = Session["OwnerCell"].ToString();
                txtbox_owneroffice.Text = Session["OwnerOffice"].ToString();
                txtbox_owneremail.Text = Session["OwnerEmail"].ToString();

            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        Response.Redirect("Admin_SiteMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_SiteMaster.aspx");

                    }
                }
                catch
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
    }
    protected void txtbox_ownerName_TextChanged(object sender, EventArgs e)
    {

    }
    string msg = "";
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            
            updatesite();
         //   Response.Redirect("Admin_SiteMaster.aspx?msg=" + msg);

        }
        catch
        {
        }
    }
    string status = "true";
    int sitetype = 0;
    public void updatesite()
    {
        try
        {
            hst.Clear();

             hst.Add("action", "update");
            hst.Add("siteid", Convert.ToInt32( Session["SiteId"].ToString()));

            hst.Add("sitetypeid", sitetype);
            hst.Add("timezone", ddl_TimeZone.SelectedValue);
            hst.Add("sitename", txtbox_SiteName.Text);
            hst.Add("address1", txtbox_Adress1.Text);
            hst.Add("address2", txtbox_Adress1.Text);
            hst.Add("city", ddl_city.SelectedValue);

            hst.Add("zip", txtbox_zip.Text);
            hst.Add("sitephone", txtbox_Phome.Text);
            hst.Add("Sitecell", txtbox_Cellular.Text);
            hst.Add("siteoffice", txtbox_poffice.Text);
            hst.Add("SiteEmail", txtbox_email.Text);
            hst.Add("SiteLatitude", latitude.Text);
            hst.Add("SiteLongitude", longitude.Text);


            hst.Add("ownername", txtbox_ownerName.Text);
            hst.Add("owneraddress", txtbox_Adress1.Text);

            hst.Add("ownercity", ddl_city.SelectedItem.Text);

            hst.Add("ownercell", txtbox_ownercellular.Text);
            hst.Add("owneroffice", txtbox_owneroffice.Text);
            hst.Add("owneremail", txtbox_owneremail.Text);

            hst.Add("modifiedby", Session["Type"].ToString());
            hst.Add("operatorid", Session["Type"].ToString());
            hst.Add("timeofsub", DateTime.Now);
            hst.Add("status", status);
            int result = objData.ExecuteNonQuery("[sp_SiteMaster]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {


                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                   Response.Redirect("Admin_SiteMaster.aspx?msg=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_SiteMaster.aspx?msg=" + msg);

                }
              

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Try Again.')", true);
                

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Please Try Again.')", true);


        }
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Admin_SiteMaster.aspx");
    }
    protected void txtbox_Adress1_TextChanged(object sender, EventArgs e)
    {

    }
}
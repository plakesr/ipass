﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_forgot_password.ascx.cs" Inherits="WUC_wuc_forgot_password" %>

<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 131px;
    }
</style>

<div>
    <table class="style1">
        <tr>
            <td class="style2">
                User Name&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_username" runat="server" ValidationGroup="c"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtbox_username" ErrorMessage="*" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="c"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_error" runat="server" Font-Bold="True" ForeColor="#CC0000" 
                    Text="*UserName is wrong" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_submit" runat="server" Text="Submit" ValidationGroup="c" 
                    onclick="btn_submit_Click" />
            </td>
            <td>
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_cancel" runat="server" Text="Cancel" ValidationGroup="e" />
            </td>
        </tr>
    </table>
</div>


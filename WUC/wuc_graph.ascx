﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_graph.ascx.cs" Inherits="WUC_wuc_graph" %>

    <title>jQuery Google Pie Chart Example in asp.net</title>
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://www.google.com/jsapi" type="text/javascript"></script>
    <script type="text/javascript">
        // Global variable to hold data
        google.load('visualization', '1', { packages: ['corechart'] });
    </script>
    <script type="text/javascript">
        $(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: 'Admin_Parker_Registration_Log.aspx/GetChartData',
                data: '{}',
                success:
                    function (response) {
                        drawchart(response.d);
                    },

                error: function () {
                    alert("Error loading data! Please try again.");
                }
            });
        })
        function drawchart(dataValues) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Column Name');
            data.addColumn('number', 'Registration Done');
            for (var i = 0; i < dataValues.length; i++) {
                data.addRow([dataValues[i].Countryname, dataValues[i].Total]);
            }
            new google.visualization.BarChart(document.getElementById('chartdiv')).
                draw(data, { title: "" });
        }        
    </script>

<div id="chartdiv" style="width: 600px; height: 350px;">
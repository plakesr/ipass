﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

public partial class WUC_wuc_graph : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static List<countrydetails> GetChartData()
    {
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=db_IPASS;Integrated Security=True"))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select modifiedby, COUNT(*) as totalparker from tbl_UserLogin where operator_id='operator'  group by modifiedby", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();
        }
        List<countrydetails> dataList = new List<countrydetails>();


        foreach (DataRow dtrow in dt.Rows)
        {
            countrydetails details = new countrydetails();
            details.Countryname = dtrow[0].ToString();
            details.Total = Convert.ToInt32(dtrow[1]);
            dataList.Add(details);
        }
        return dataList;
    }
}
public class countrydetails
{
    public string Countryname { get; set; }
    public int Total { get; set; }
}
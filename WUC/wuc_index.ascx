﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_index.ascx.cs" Inherits="WUC_wuc_index" %>

<style type="text/css">
    body{ margin:0px;padding:0px;font-family: Verdana;
    font-size: 12px;color: #586064; }
    img{ border:none;}
#navbar 
{
	background:url(images/header-bg.jpg) left top repeat-x;
	height: 104px;
	width: 100%;
	margin-bottom:20px;
}
.headline {
	background: linear-gradient(to bottom, #dfe5ec 0%, #f0f3f6 65%, #ffffff 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
}
a {
    color: #0B7AAE;
}
.login-area{ width:858px; 
  border-radius: 3px 3px 3px 3px;
  box-shadow: 0 2px 7px rgba(0, 0, 0, 0.4);
margin:0px auto;
background:url(images/login-bg.jpg) left top repeat-x;
}
.left_side{width:320px; float:left; padding:0 0px 0 20px;}
.right_side{width:518px; float:left; text-align:right}
.logo-area{ margin:20px 0; text-align:center; width:100%;}
.left_side .text-box{  background-color: #F3F5F6;
  border-left: 1px solid #CCCCCC;
  border-right: 1px solid #CCCCCC;
  border-top: 1px solid #CCCCCC;
  cursor: pointer;
  padding:12px 0 12px 8px; width:92%; margin-bottom:10px; }
.clearfix{ clear:both}
.button{color: #FFFFFF;
    font-family:arial;
    font-size: 13pt;
    font-weight: 300;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3);  background: none repeat scroll 0 0 #3198D8;
    border: 1px solid #4176BA;
    background: -moz-linear-gradient(center bottom , #2789C7 0%, #3F9FD9 100%) repeat scroll 0 0 transparent;
    padding:4px 10px;
    border-radius:5px;
    cursor:pointer;
    float:right;
    margin:10px 14px 0 0;
    text-decoration:none;
    
    }
 .button:hover{ opacity:0.8}   
    

</style>
<div id="navbar"> </div>
<!--end of navbar-->
<div class="login-area">
<div class="left_side">
  <div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/logo-login.png" /></a></div>
   <form novalidate="novalidate" autocomplete="off" target="_top" action="#" onsubmit="handleLogin();" method="post" name="login">
   <asp:TextBox ID="txtbox_username"  runat="server" class="text-box"  placeholder="User Name" ValidationGroup="l" 
                            ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtbox_username" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="l"></asp:RequiredFieldValidator>
                  
  <asp:TextBox ID="txtbox_password"  class="text-box" placeholder="Password" runat="server" ValidationGroup="l" 
                    TextMode="Password"  ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_password" runat="server" ErrorMessage="*" 
                    ForeColor="#CC0000" ControlToValidate="txtbox_password" 
                    ValidationGroup="l"></asp:RequiredFieldValidator>
   <asp:Button ID="btn_LOGIN" runat="server" Text="Login" CssClass="button" 
                          onclick="btn_LOGIN_Click"  />&nbsp;
           <div class="clearfix">
               <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label></div>      
   <input type="checkbox" name="rememberUn" id="Checkbox1">
  
   <label for="rememberUn">Remember User Name</label> <a href="#">Forgot your password</a></span> 
    <div class="clearfix">&nbsp;</div>
   <span class="wrapper_signup"><a href="Refund_Request_For_Parker.aspx" id="A2" class="button">Refund Request.</a>
   &nbsp;&nbsp; <a href="StatusOfParker.aspx" id="A1" class="button">Check Status</a>
   <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     
      </form>
      </div>
  <div class="right_side">
  <img src="images/park.jpg" style=" text-align:right;" />
     <a style="color: #FFFFFF; font-family: Verdana; font-size: 12px;" href="./Operator/Registration_SiteSearch.aspx?ReturnFlag=Home&amp;CCClear=Yes">
  
                <asp:LinkButton ID="link_registration" runat="server" 
        onclick="link_registration_Click"> <img src="images/register-banner.jpg" /></asp:LinkButton>
                </a>
  </div>
  <div class="clearfix"></div>
</div>

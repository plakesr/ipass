﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_index : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack==true)
        {
            try
            {
                if (Session["RefundForRequest"].ToString() == "refund successfuly")
                {
                    string tracking = Session["Tracking"].ToString();
                   
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Your Traking ID:"+tracking+"\\nRequest send successfuly !')", true);
                    Session["RefundForRequest"] = null;
                }
            }
            catch
            {

            }



            try
            {
                if (Session["registration"].ToString() == "True")
                {
                    Label1.Visible = true;
                    Label1.Text = "You Are Successfully Registered Please Check Your Mail For Login Details";
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You Are Successfully Registered Please Check Your Mail For Login Details')", true);
                   ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('InvoiceGenerate.aspx','New Windows','height=800, width=600,location=yes');", true);

                    Session["registration"] = null;
                    try
                    {

                       
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {

            }

        }

    }
    protected void link_registration_Click(object sender, EventArgs e)
    {
        Response.Redirect("LotAddresssearch.aspx");
    }
    protected void link_login_Click(object sender, EventArgs e)
    {
        Response.Redirect("login.aspx");
    }

    protected void btn_LOGIN_Click(object sender, EventArgs e)
    {
        clsInsert cs = new clsInsert();

        if (txtbox_password.Text.Trim() == "" || txtbox_username.Text.Trim()== "")
        {
            if (txtbox_password.Text.Trim() == "" && txtbox_username.Text.Trim() == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter  Username and Password !')", true);

            }
            if (txtbox_password.Text.Trim() == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter  Password !')", true);
            }
            else if (txtbox_username.Text.Trim() == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter  Username !')", true);

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter  Username and Password !')", true);

            }
        }
        else
        {

            string username = txtbox_username.Text.Trim();
            string pass = txtbox_password.Text.Trim();
            Session["password"] = txtbox_password.Text;
            DataTable dt = cs.LoginCheck(username, pass);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["status"].ToString() == "True")
                {
                    if (dt.Rows[0]["type"].ToString() == "admin")
                    {
                        Session["typeofuser"] = dt.Rows[0]["type"].ToString();
                        Session["Login"] = txtbox_username.Text;
                        Session["operatorid"] = dt.Rows[0]["operator_id"].ToString();
                        Session["Type"] = dt.Rows[0]["type"].ToString();

                        Response.Redirect("Dashboard.aspx");
                    }
                    else if (dt.Rows[0]["type"].ToString() == "operator")
                    {

                        Session["Login"] = txtbox_username.Text;
                        Session["Type"] = dt.Rows[0]["type"].ToString();
                        Session["typeofuser"] = dt.Rows[0]["type"].ToString();
                        Session["operatorid"] = dt.Rows[0]["operator_id"].ToString();
                        Response.Redirect("Operator_deshboard.aspx");
                    }

                    else if (dt.Rows[0]["type"].ToString() == "individual")
                    {
                        Session["Login"] = txtbox_username.Text;
                        Session["typeofuser"] = dt.Rows[0]["type"].ToString();
                        Session["Username"] = txtbox_username.Text;
                        Session["operatoeid"] = dt.Rows[0]["operator_id"].ToString();
                        Session["Type"] = dt.Rows[0]["type"].ToString();

                        Response.Redirect("Deshboard.aspx");
                    }
                    else if (dt.Rows[0]["type"].ToString() == "Corporate")
                    {
                        Session["typeofuser"] = dt.Rows[0]["type"].ToString();
                        Session["Type"] = dt.Rows[0]["type"].ToString();
                        Session["Username"] = txtbox_username.Text;
                        Session["Login"] = txtbox_username.Text;
                        Session["operatorid"] = dt.Rows[0]["operator_id"].ToString();
                        Session["parkerUsername"] = txtbox_username.Text;
                        //  Session["Corporate"] = txtbox_username.Text;
                        try
                        {
                            DataSet dslot = new clsInsert().fetchrec("select LotId from tbl_ParkerRegis  where UserName='" + txtbox_username.Text + "'");
                            if (dslot.Tables[0].Rows.Count > 0)
                            {
                                Session["lotid"] = dslot.Tables[0].Rows[0][0].ToString();
                            }

                        }
                        catch
                        {

                        }

                        try
                        {
                            DataTable corp = cs.CorporateRegistrationcheck(txtbox_username.ToString().Trim());
                            if (corp.Rows.Count > 0)
                            {

                                DataTable corpdetails = cs.CorporateParkerParentDetail(username);


                                if (corpdetails.Rows.Count > 0)
                                {
                                    Session["lotid"] = corpdetails.Rows[0]["LotId"].ToString();
                                    Session["ParkingType"] = corpdetails.Rows[0]["ParkingType"].ToString();
                                    Session["CardNo"] = corpdetails.Rows[0]["CardNo"].ToString();
                                    Session["AdressLine1"] = corpdetails.Rows[0]["AdressLine1"].ToString();
                                    Session["AdressLine2"] = corpdetails.Rows[0]["AdressLine2"].ToString();
                                    Session["Zip"] = corpdetails.Rows[0]["Zip"].ToString();
                                    Session["Country"] = corpdetails.Rows[0]["Country"].ToString();
                                    Session["State"] = corpdetails.Rows[0]["State"].ToString();
                                    Session["City"] = corpdetails.Rows[0]["City"].ToString();
                                    Session["CellNo"] = corpdetails.Rows[0]["CellNo"].ToString();
                                    Session["Phoneno"] = corpdetails.Rows[0]["Phoneno"].ToString();
                                    Session["Fax"] = corpdetails.Rows[0]["Fax"].ToString();
                                    Session["Email"] = corpdetails.Rows[0]["Email"].ToString();
                                    Session["Billingwith"] = corpdetails.Rows[0]["Billingwith"].ToString();

                                    DataTable dtforvehicle = new DataTable();
                                    dtforvehicle.Columns.Add("VehicalMake", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalModel", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalYear", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalColor", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalLicensePlate", typeof(string));
                                    dtforvehicle.Columns.Add("Username", typeof(string));

                                    Session["datatableforvehicle"] = dtforvehicle;



                                    DataTable dtplan = new DataTable();
                                    dtplan.Columns.Add("Planrate", typeof(int));

                                    dtplan.Columns.Add("Username", typeof(string));

                                    Session["plantable"] = dtplan;







                                }

                                //Session["Type"] = dt.Rows[0]["type"].ToString();
                            }
                            else
                            {


                                Session["alert"] = "Please Add Parkers in the account";
                                DataTable corpdetails = cs.CorporateParkerParentDetail(username);


                                if (corpdetails.Rows.Count > 0)
                                {
                                    Session["parkerparentid_corporate"] = corpdetails.Rows[0]["Parker_id"].ToString();
                                    Session["lotid"] = corpdetails.Rows[0]["LotId"].ToString();
                                    Session["ParkingType"] = corpdetails.Rows[0]["ParkingType"].ToString();
                                    Session["CardNo"] = corpdetails.Rows[0]["CardNo"].ToString();
                                    Session["AdressLine1"] = corpdetails.Rows[0]["AdressLine1"].ToString();
                                    Session["AdressLine2"] = corpdetails.Rows[0]["AdressLine2"].ToString();
                                    Session["Zip"] = corpdetails.Rows[0]["Zip"].ToString();
                                    Session["Country"] = corpdetails.Rows[0]["Country"].ToString();
                                    Session["State"] = corpdetails.Rows[0]["State"].ToString();
                                    Session["City"] = corpdetails.Rows[0]["City"].ToString();
                                    Session["CellNo"] = corpdetails.Rows[0]["CellNo"].ToString();
                                    Session["Phoneno"] = corpdetails.Rows[0]["Phoneno"].ToString();
                                    Session["Fax"] = corpdetails.Rows[0]["Fax"].ToString();
                                    Session["Email"] = corpdetails.Rows[0]["Email"].ToString();
                                    Session["Billingwith"] = corpdetails.Rows[0]["Billingwith"].ToString();


                                    DataTable dtforvehicle = new DataTable();
                                    dtforvehicle.Columns.Add("VehicalMake", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalModel", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalYear", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalColor", typeof(string));
                                    dtforvehicle.Columns.Add("VehicalLicensePlate", typeof(string));
                                    dtforvehicle.Columns.Add("Username", typeof(string));

                                    Session["datatableforvehicle"] = dtforvehicle;





                                    DataTable dtplan = new DataTable();
                                    dtplan.Columns.Add("Planrate", typeof(int));

                                    dtplan.Columns.Add("Username", typeof(string));

                                    Session["plantable"] = dtplan;

                                }



                                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Details of parkers!')", true);

                            }
                            Response.Redirect("Deshboard.aspx");

                        }
                        catch
                        {

                        }

                    }


                    else
                    {


                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Valid Username or Password !')", true);

                    }

                }

                else
                {
                    if (dt.Rows[0]["status"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised for login !')", true);

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Valid Username or Password !')", true);
                    }
                }
        }
            else
            {
                
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Valid Username or Password !')", true);
                 
            }
        }
    }
    
}
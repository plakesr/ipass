﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class wuc_invoice_corporate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            //Session["Billusername"] = txtBox_username.Text;
            //Session["InvoiceNumber"] = lbl_invoicenumber.Text;
            try
            {
            DataSet dscorporatedetails = new clsInsert().fetchrec("select tbl_ParkerRegis.*,tbl_LotMaster.LotName from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId where tbl_ParkerRegis.UserName='" + Session["Billusername"].ToString()+ "'");
                //DataSet dscorporatedetails = new clsInsert().fetchrec("select tbl_ParkerRegis.*,tbl_LotMaster.LotName from tbl_ParkerRegis inner join tbl_LotMaster on tbl_LotMaster.Lot_id=tbl_ParkerRegis.LotId where tbl_ParkerRegis.UserName='vaibhav'");
            if (dscorporatedetails.Tables[0].Rows.Count > 0)
            {
                lbl_name.Text = dscorporatedetails.Tables[0].Rows[0]["FirstName"].ToString();
                txtbox_address.Text = dscorporatedetails.Tables[0].Rows[0]["AdressLine1"].ToString();

               lbl_invoicenumber.Text = Session["InvoiceNumber"].ToString();

               // lbl_invoicenumber.Text = "201410_000003";
                lbl_invoicedate.Text = Convert.ToString(DateTime.Now);
                lbl_lotname.Text = dscorporatedetails.Tables[0].Rows[0]["LotName"].ToString();
            }

            DataSet dscorporateinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num='" + Session["InvoiceNumber"].ToString() + "'");
           // DataSet dscorporateinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num='201410_000003'");
            if (dscorporateinvoice.Tables[0].Rows.Count > 0)
            {
                lbl_total.Text = dscorporateinvoice.Tables[0].Rows[0]["Sub_total"].ToString();
                lbl_invoicedAmount.Text = dscorporateinvoice.Tables[0].Rows[0]["Amt_Outstanding"].ToString();
                lbl_outstandingamount.Text ="$" +dscorporateinvoice.Tables[0].Rows[0]["Amt_Outstanding"].ToString();
                lbl_grandtotal.Text = dscorporateinvoice.Tables[0].Rows[0]["Invoiced_Amount"].ToString();
                lbl_plushst.Text = dscorporateinvoice.Tables[0].Rows[0]["Tax"].ToString();

            }
            DataSet dscorporatebilldetails = new clsInsert().fetchrec("select * from TBL_CHARGE where Inv_num='" + Session["InvoiceNumber"].ToString() + "'");
            //DataSet dscorporatebilldetails = new clsInsert().fetchrec("select * from TBL_CHARGE where Inv_num='201410_000003'");
            if (dscorporatebilldetails.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dscorporatebilldetails.Tables[0].Rows.Count; i++)
                {
                    if (dscorporatebilldetails.Tables[0].Rows[i]["Desc"].ToString().Trim() == "Card Delivery Charges")
                    {
                        lbl_deliverycharge.Text = dscorporatebilldetails.Tables[0].Rows[i]["Amt"].ToString();

                    }
                    if (dscorporatebilldetails.Tables[0].Rows[i]["Desc"].ToString().Trim() == "Activation Charges")
                    {
                        lbl_activationcharge.Text = dscorporatebilldetails.Tables[0].Rows[i]["Amt"].ToString();

                    }
                    if (dscorporatebilldetails.Tables[0].Rows[i]["Desc"].ToString().Trim() == "Monthly Parking Charges")
                    {
                        lbl_monthlycharge.Text = dscorporatebilldetails.Tables[0].Rows[i]["Amt"].ToString();

                    }
                    if (dscorporatebilldetails.Tables[0].Rows[i]["Desc"].ToString().Trim() == "Security Charges")
                    {
                        lbl_deposit.Text = dscorporatebilldetails.Tables[0].Rows[i]["Amt"].ToString();

                    }

                }
            }
            if (lbl_deposit.Text == "")
            {
                lbl_deposit.Text = "0.00";

            }
            else
            {
            }
            }
            catch
            {
            }
        }
    }
}
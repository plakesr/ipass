﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_invoice_generate.ascx.cs" Inherits="WUC_wuc_invoice_generate" %>
<style type="text/css">
    .style2
    {
        width: 200px;
    }
    .style3
    {
        width: 161px;
    }
    .style4
    {
        width: 161px;
        height: 23px;
    }
    .style5
    {
        height: 23px;
    }
    </style>

<%--<script type="text/javascript">
    function btnClick(sender, args) {
        var printButton = document.getElementById("Button1");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
        window.print();
    }
</script>--%>
<link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
<%--<link href="../stylesheet/main.css" rel="stylesheet" type="text/css" />--%>
<div class="clearfix"></div>
    <asp:Panel ID="Panel1" runat="server">
    <table border="0" cellpadding="10"  class="invoice-table" width="960">
    <tr> <td colspan="4"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></td></tr>
        <tr><th colspan="4" class="invoice-heading">Invoice</th></tr>
       <tr>
      <td colspan="2" align="left"  valign="top" width="60%">
      <table class="invoice-section" cellpadding="10" width="60%">
       <tr class="white">
            <td class="style3">
                <label>
                Client Name</label></td>
            <td>
                <asp:Label ID="lbl_name" runat="server"></asp:Label>
            </td>
           
        </tr>
        <tr class="white">
            <td class="style3" valign="top">
                 <label>Address</label></td>
            <td valign="top" height="55">
               <%-- <asp:TextBox ID="txtbox_address" runat="server" TextMode="MultiLine" Enabled="false"></asp:TextBox>--%>
                <asp:Label ID="txtbox_address" runat="server" ForeColor="Black"></asp:Label>

            </td>
           
        </tr>
        
      </table>
      </td>
    <td colspan="2" align="right" width=" 40%" valign="top">
    
    <table border="0"  cellpadding="10" width="40%" class="invoice-detail">
          <tr class="white">
             <td class="style2">
                 <label>Invoice Number</label></td>
            <td>
                <asp:Label ID="lbl_invoicenumber" runat="server" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr class="white">
            <td class="style2" valign="top">
                <label>Invoice Date</label></td>
            <td>
                <asp:Label ID="lbl_invoicedate" runat="server" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr class="white">
           <td class="style2">
                <label>Status</label></td>
            <td>
                <asp:Label ID="lbl_status" runat="server" Text="Unpaid" ForeColor="#CC0000"></asp:Label>
            </td>
        </tr>
        
      
    </table></td>
    </tr>
    <tr>
    <td width="100%" colspan="4">
    <table border="0" cellpadding="10" class="invoice-section">

       
        <tr class="light-blue">
            <td class="style3">
                <label>Lot Name</label></td>
            <td>
                <asp:Label ID="lbl_lotname" runat="server"></asp:Label>
            </td>
          
        </tr>
        <tr class="light-blue">
            <td class="style3">
                <label>Parking Type</label></td>
            <td>
                <asp:Label ID="lbl_lottype" runat="server"></asp:Label>
            </td>
           
        </tr>
        <tr class="white">
            <td class="style3">
                 <label>Plan</label></td>
            <td>
            <asp:Label ID="lbl_charge" runat="server"></asp:Label> Plan Of
                <asp:Label ID="Label1" runat="server" ForeColor="Black" Text="$"></asp:Label>
                &nbsp;<asp:Label ID="lbl_plan" runat="server" ForeColor="#33CC33"></asp:Label>
            </td>
         
        </tr>
        <tr class="white">
            <td class="style4">
                 <label>Activation Charge</label></td>
            <td class="style5" align="right" style="color:#33CC33;" >
                <asp:Label ID="lbl_activationcharge" runat="server" ForeColor="#33CC33"></asp:Label>
            </td>
           
        </tr>
        <tr class="white">
            <td class="style3" style="color:#33CC33;">
                  <label>Delivery Charge</label></td>
            <td align="right" style="color:#33CC33;">
                $<asp:Label ID="lbl_deliverycharge" runat="server" ForeColor="#33CC33"></asp:Label>
            </td>
          
        </tr>
        <tr class="white">
            <td class="style3">
                 <label> Total</label></td>
            <td align="right">
                <asp:Label ID="lbl_mark" runat="server" ForeColor="#33CC33" Text="$"></asp:Label>
                &nbsp;<asp:Label ID="lbl_total" runat="server" ForeColor="#33CC33"></asp:Label>
            </td>
         
        </tr>
        <tr class="white">
            <td class="style3">
                TAX</td>
            <td align="right" style="color:#33CC33;">
                <asp:Label ID="lbl_plushst" runat="server" Text="0"></asp:Label>%
            </td>
            
        </tr>
          <tr>
            <td class="style2">
            </td>
            <td align="right">
            <table>
            <tr><td><label>
                Grand Total:</label>&nbsp;&nbsp;</td>
            <td><div class="price">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="14px" 
                    ForeColor="#0066FF" Text="$"></asp:Label>
                <asp:Label ID="lbl_grandtotal" runat="server" Font-Bold="True" 
                    Font-Size="13px" ForeColor="#0066FF" Text=""></asp:Label></div></td>
            </tr>
            </table>
            
                
            </td>
        </tr>
    </table>
    
    </td>
</tr>

    </table>
    </asp:Panel>

<%--<asp:Button ID="Button1" runat="server"  Text="Print" 
    OnClientClick="javascript:window.print();"  />--%><%--<body> <a onclick="ClickHereToPrint();">Print</a> <iframe id='ifrmPrint' src='#' style="width:0px; height:0px;"></iframe> --%>

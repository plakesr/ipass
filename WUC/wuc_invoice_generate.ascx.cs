﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
public partial class WUC_wuc_invoice_generate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                if (Session["PlanrateID"].ToString() != "" || Session["PlanrateID"].ToString() != null)
                {
                    lbl_plan.Text = Session["amount_parking"].ToString();
                    lbl_charge.Text = Session["Chargebyinv"].ToString();
                    lbl_invoicenumber.Text = Session["invoicenumber"].ToString();
                    lbl_invoicedate.Text = DateTime.Now.ToString();
                    lbl_lotname.Text = Session["Lotnamefinv"].ToString();
                    lbl_activationcharge.Text = Session["ActivationChargeInv"].ToString();
                    lbl_deliverycharge.Text = Session["DeleveryInv_billtable"].ToString();
                    lbl_total.Text = Session["totalchargeinv"].ToString();

                    //  decimal number = 100;
                    //decimal tax = Convert.ToDecimal("0.13") * Convert.ToDecimal(lbl_total.Text);
                    lbl_plushst.Text = Session["taxrate"].ToString();
                    //decimal extra = ((tax) * (Convert.ToDecimal(lbl_total.Text)));
                    decimal grand = Convert.ToDecimal(Session["grandtotal"].ToString());
                    lbl_name.Text = Session["fullname"].ToString(); ;
                    txtbox_address.Text = Session["Full Address"].ToString();
                    lbl_grandtotal.Text = Convert.ToString(grand);
                    lbl_lottype.Text = Convert.ToString(Session["Typeinv"].ToString());

                }
                else
                {
                }
            }
            catch
            {
            }
        }
    }

    public string Get6Digits()
    {
        var bytes = new byte[6];
        var rng = RandomNumberGenerator.Create();
        rng.GetBytes(bytes);
        uint random = BitConverter.ToUInt32(bytes, 0) % 1000000;
        return String.Format("{0:D6}", random);
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        //Session["ctrl"] = Panel1;
        ////   ScriptManager.RegisterStartupScript(Page, typeof(Page), "onclick", "<script language=javascript>window.open('Print.aspx','PrintMe','height=300px,width=300px,scrollbars=1');</script>");
        ////  ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Print.aspx','PrintMe,'New Windows','height=380, width=300,location=no');", true);
        //Response.Redirect("Print.aspx");
    }
    //protected void Button1_Click1(object sender, EventArgs e)
    //{
    //   // Button1.Visible = false;
    //}
}
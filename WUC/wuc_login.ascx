﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_login.ascx.cs" Inherits="wuc_login" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 136px;
    }
</style>

<div>
    <table class="style1">
        <tr>
            <td class="style2">
                User Name :</td>
            <td>
                <asp:TextBox ID="txtbox_username" runat="server" ValidationGroup="l" 
                    ontextchanged="txtbox_username_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtbox_username" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="l"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Password&nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_password" runat="server" ValidationGroup="l" 
                    TextMode="Password" ontextchanged="txtbox_password_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_password" runat="server" ErrorMessage="*" 
                    ForeColor="#CC0000" ControlToValidate="txtbox_password" 
                    ValidationGroup="l"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:CheckBox ID="chk_remember" runat="server" Text="Remember me next time" 
                    ValidationGroup="a" />
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
               

                <asp:Label ID="lbl_error" runat="server" Font-Bold="True" ForeColor="#CC0000" 
                    Text="*username/password is wrong" Visible="False"></asp:Label>
               

            </td>
        </tr>
        <tr>
            <td class="style2">  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp;  
                <asp:Button ID="btn_login" runat="server" Text="Login" ValidationGroup="l" 
                    onclick="btn_login_Click" class="btn" /> </td>
            <td>
                <asp:Button ID="btn_cancel" runat="server" Text="Cancel" ValidationGroup="h"  class="btn"/>
                &nbsp;&nbsp; &nbsp; &nbsp;
                <asp:LinkButton ID="link_btn_forgotpassword" runat="server" ValidationGroup="b" 
                    onclick="link_btn_forgotpassword_Click">Forgot password</asp:LinkButton>
            </td>
        </tr>
    </table>
</div>


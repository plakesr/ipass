﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_lotShow.ascx.cs" Inherits="WUC_wuc_lotShow" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>

<table class="style1">
    <tr>
        <td>
            Enter LotName</td>
        <td>
            <asp:TextBox ID="txtbox_lotname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="Button1" runat="server" Text="Search" CssClass="button" 
                onclick="Button1_Click" />
        </td>
    </tr>
</table>



    <asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource1" 
    onitemcommand="Repeater1_ItemCommand" Visible="False">
<HeaderTemplate>
              <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Lot &nbsp;&nbsp;Name
                    </th>
                     <th >
                        Lot Code 
                    </th>
                    <th>
                        Edit
                    </th>
                            <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                  <%--  <th>
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td  >
                    <%#Eval("LotName")%>
                    </td><td>
                    <%#Eval("Lotcode")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Lot_id") %>' runat="server"><img src="images/edit.png" />
                  
                    </asp:LinkButton>
                    <asp:Label ID="lotid" runat="server" Text='<%# Eval("Lot_id") %>' 
                                        Visible="False"></asp:Label>
                    
                </td>
                              <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td>
             <%--   <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    
                    </asp:LinkButton>
                    </td>--%>

                 


            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_LotMaster] ORDER BY [Lot_id] DESC, [LotName]">
    </asp:SqlDataSource>

<asp:Repeater ID="Repeater1" runat="server"  onitemcommand="Repeater1_ItemCommand" Visible="False">
<HeaderTemplate>
              <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th >
                        Lot &nbsp;&nbsp;Name
                    </th>
                     <th >
                        Lot Code 
                    </th>
                    <th>
                        Edit
                    </th>
                  <%--  <th>
                      Delete
                    </th>--%>
                            <th>
                      InActive
                    </th>
                    <th>
                    Active
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td  >
                    <%#Eval("LotName")%>
                    </td><td>
                    <%#Eval("Lotcode")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Lot_id") %>' runat="server"><img src="images/edit.png" />
                  
                    </asp:LinkButton>
                       <asp:Label ID="lotid" runat="server" Text='<%# Eval("Lot_id") %>' 
                                        Visible="False"></asp:Label>
                    
                </td>
              <%--  <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    
                    </asp:LinkButton>
                    </td>--%>

                              <td>
                <asp:LinkButton ID="lnkactive" CommandName="cmdinactive" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();">
                 <img src="images/inactive.png" width=50 height=20 />  
                
                    </asp:LinkButton>
                    </td>
                    
                    <td>
                    
               <asp:LinkButton ID="lnkinactive" CommandName="cmdactive" CommandArgument='<%#Eval("Lot_id") %>' runat="server" OnClientClick="return confirmation();">
               <img src="images/active.png" width=50 height=20 />
               </asp:LinkButton>
                    </td> 


            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
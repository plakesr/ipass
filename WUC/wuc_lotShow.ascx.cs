﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_lotShow : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            try
            {
                rpt1.Visible = true;
                Repeater1.Visible = false;
            }
            catch
            {
            }
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
          int i = e.Item.ItemIndex;
          if (e.CommandName == "cmdDelete")
          {
              int ID = Convert.ToInt32(e.CommandArgument.ToString());

              try
              {
                  DataTable dt11 = new clsData().deletelot(ID);
                  Response.Redirect("Admin_LotManger.aspx");
                  
              }
              catch
              {
              }
          }
          if (e.CommandName == "cmdEdit")
          {
              int ID = Convert.ToInt32(e.CommandArgument.ToString());
              DataSet dsactivecheck = new clsInsert().fetchrec("select Approoval,Lot_status from tbl_LotMaster where Lot_id=" + ID);
              if(dsactivecheck.Tables[0].Rows[0][1].ToString()=="False")
              {
                  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Lot Is Not Activated ')", true);

              }
              else
              {
              try
              {


                  Session["lotid"] = ID;
                  Response.Redirect("Admin_EditLotMaster.aspx");

              }
              catch
              {
              }
              }
          }
          if (e.CommandName == "cmdinactive")
          {
              // string user = e.CommandArgument.ToString();
              int lotid = Convert.ToInt32(e.CommandArgument);
              DataSet ds = new clsInsert().fetchrec("select tbl_SiteMaster.Site_Status,tbl_CampusMaster.CampusStatus,tbl_LotMaster.Lot_status from tbl_LotMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_LotMaster.Site_Id inner join tbl_CampusMaster on tbl_CampusMaster.CampusID=tbl_LotMaster.Campus_id where Lot_id=" + lotid);

              if (ds.Tables[0].Rows[0][2].ToString() == "False")
              {
                  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Already Deactivated ')", true);

              }
              else
              {
                 
                      inactivestatus(lotid);
                      ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Deactived Successfuly')", true);
                  
              }

          }

          if (e.CommandName == "cmdactive")
          {
              int lotid = Convert.ToInt32(e.CommandArgument);
              DataSet ds = new clsInsert().fetchrec("select tbl_SiteMaster.Site_Status,tbl_CampusMaster.CampusStatus,tbl_LotMaster.Lot_status from tbl_LotMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_LotMaster.Site_Id inner join tbl_CampusMaster on tbl_CampusMaster.CampusID=tbl_LotMaster.Campus_id where Lot_id=" + lotid);

              if (ds.Tables[0].Rows[0][2].ToString() == "True")
              {
                  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert(' Already Activated ')", true);

              }
              else
              {
                  //DataSet ds1 = new clsInsert().fetchrec("select tbl_SiteMaster.Site_Status from tbl_CampusMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_CampusMaster.SiteID where tbl_CampusMaster.CampusID=" + lotid);
                  if (ds.Tables[0].Rows[0][0].ToString() == "False"  )
                  {

                      ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Related Site Is Not Active')", true);

                  }

                  else if (ds.Tables[0].Rows[0][1].ToString() == "False" && ds.Tables[0].Rows[0][0].ToString() == "True")
                  {
                      ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Related Campus Is Not Active')", true);
                  }
                  else
                  {
                      activeuserstatus(lotid);
                      ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Activated Successfuly')", true);
                  }
              }

          }
    }

    public void inactivestatus(int lotid)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "lotstatusinactive");
            //hst.Add("ustatus", userstattus1);
            hst.Add("lotid", lotid);



            int result = objData.ExecuteNonQuery("[sp_statusupdate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //  Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
                // Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
            // Response.Redirect("Admin_SiteMaster.aspx");

        }


    }


    public void activeuserstatus(int lotid)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "lotstatusactive");
            //hst.Add("ustatus", userstattus1);
            hst.Add("lotid", lotid);



            int result = objData.ExecuteNonQuery("[sp_statusupdate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                //  Response.Redirect("Admin_SiteMaster.aspx");

            }
            else
            {
                // Response.Redirect("Admin_SiteMaster.aspx");
            }
        }
        catch
        {
            // Response.Redirect("Admin_SiteMaster.aspx");

        }


    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        Repeater1.Visible = true;
        rpt1.Visible = false;

        try
        {

            if (Session["Type"].ToString() == "admin")
            {

                DataTable dt11 = new clsData().getlotsforediting(txtbox_lotname.Text);

                if (dt11.Rows.Count > 0)
                {
                    Repeater1.DataSource = dt11;
                    Repeater1.DataBind();
                   
                }
                else
                {
                }
            }
        }
        catch
        {
        }

    }
}
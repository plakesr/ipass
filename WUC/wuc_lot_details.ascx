﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_lot_details.ascx.cs" Inherits="WUC_wuc_lot_details" %>


<link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type ="text/javascript">
    var map;
    function initialize() {
        var lat = '<%= Session["Lot_Latitude"]%>';
        var long = '<%= Session["Lot_Longitude"] %>';

        var latlng = new google.maps.LatLng(lat, long);
        var myOptions = {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        var marker = new google.maps.Marker
        (
            {
                position: new google.maps.LatLng(lat, long),
                map: map,
                title: 'Click me'
            }
        );
        var infowindow = new google.maps.InfoWindow({
            content: 'Location info:<br/>Country Name:<br/>LatLng:'
        });
        google.maps.event.addListener(marker, 'click', function () {
            // Calling the open method of the infoWindow 
            infowindow.open(map, marker);
        });
    }
    window.onload = initialize;
</script>


<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>

<asp:Panel runat="server" ID="Panel8" >
<table width="100%" cellpadding="10" class="form-table" cellspacing="0">
<tr><td valign="top" style="width:500px;">
<table cellpadding="0" cellspacing="0" >
<tr>
<td><h2>LOT DETAILS</h2></td>
<td></td>
</tr>
<tr class="lot-details">
<th>Lot Name</th>
<td><asp:Label ID="lbl_name" runat="server" Text=""></asp:Label></td>
</tr>
<tr class="lot-details"><th> Available Space</th>
<td><asp:Label ID="Lbl_TotalSpace" runat="server" Text=""></asp:Label><asp:Label ID="lbl_space" runat="server" Text="" Visible="false"></asp:Label></td>
</tr>
<tr class="lot-details"><th>Restriction</th>
<td><asp:Label ID="lbl_restriction" runat="server" Text=""></asp:Label></td>
</tr>

<tr class="lot-details"><th>Enterance</th>
<td><asp:Label ID="lbl_entrance" runat="server" Text=""></asp:Label></td>
</tr>
<tr class="lot-details"><th>Exit</th>
<td><asp:Label ID="lbl_exit" runat="server" Text=""></asp:Label></td>
</tr>
<tr><td valign="top"> 
<div>&nbsp;<br /></div>
<asp:Panel ID="Panel1" runat="server" >
        <fieldset style=" height:480px;" >
        <legend> Random</legend>
        
           <div class="parking-price" >
            <table >
                <tr>
                    <td > 
                        </td>
                    <td >
                        <asp:Repeater ID="rpt_RandomDaily" runat="server" 
                            onitemcommand="rpt_RandomDaily_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                       <asp:Label ID="Label4" runat="server" style=" font-weight:bold; font-size:15px;" Text="Daily"></asp:Label> 
                    </th>
                      <th>
                      
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td style="width:100px;" >$  <%#Eval("Amount")%> </td>
                <td  style="width:100px;"  >   <asp:Label ID="Label1" runat="server"   Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>  <%#Eval("chargebyname")%> </td> 

                  
               
                <td valign="middle" >
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img width="86" src="images/Buy01.png" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                    
                    
                </td>
               <asp:Label ID="random_daily_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="random_daily_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>

                                         <asp:Label ID="chargeby_random_daily" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                        </asp:Repeater>

                    </td>
                </tr>
                <tr>
                    <td >
                     
                        </td>
                    <td >
                        <asp:Repeater ID="rpt_RandomMonthly" runat="server" 
                            onitemcommand="rpt_RandomMonthly_ItemCommand">
                        <HeaderTemplate>
                 
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                        <asp:Label ID="lbl_random_month" runat="server" style=" font-weight:bold; font-size:15px;" Text="Monthly"></asp:Label>
                    </th>
                      <th>
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td style="width:100px;">  $   <%#Eval("Amount")%> </td>
                <td  style="width:100px;" >   <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>'  runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img width="86" src="images/Buy01.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="random_month_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="random_month_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>

                                          <asp:Label ID="chargeby_random_monthly" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                        </asp:Repeater>
                     </td>
                </tr>
               
            </table></div>
            </fieldset>
         </asp:Panel>
            </td>
<td>  <div>&nbsp;<br /></div>
<asp:Panel ID="Panel2" runat="server">
         <fieldset  style=" height:480px;"  >
         <legend>Reserved</legend>
         <div class="parking-price" >
             <table>
                 <tr>
                     <td>
                         </td>
                     <td >
                         <asp:Repeater ID="rpt_ReservedDaily" runat="server" 
                             onitemcommand="rpt_ReservedDaily_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                        <asp:Label ID="Label3" runat="server" style=" font-weight:bold; font-size:15px;" Text="Daily"></asp:Label> 
                    </th>
                      <th>
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td style="width:100px;">    $ <%#Eval("Amount")%> </td>
                <td style="width:100px;">   
                    <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img width="86" src="images/Buy01.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="reserved_daily_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="reserved_daily_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>
                                          <asp:Label ID="chargeby_reserved_daily" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
                 <tr>
                     <td >
                        </td>
                     <td >
                         <asp:Repeater ID="rpt_ReservedMonthly" runat="server" 
                             onitemcommand="rpt_ReservedMonthly_ItemCommand">
                         <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     <th>
                       <asp:Label ID="Label2" runat="server" style=" font-weight:bold; font-size:15px;" Text="Monthly"></asp:Label>  
                    </th>
                      <th>
                       
                    </th>
                     <th>
                       
                    </th>
                    
                     
                </tr>
        </HeaderTemplate>
         <ItemTemplate>
         <tr>
            <td style="width:100px;">  $   <%#Eval("Amount")%> </td>
                <td style="width:100px;">   <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:f0}",Eval("Quantity")) %>'></asp:Label>   <%#Eval("chargebyname")%></td>
                  
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("LotRateid") %>' runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img width="86" src="images/Buy01.png" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </asp:LinkButton>
                     
                </td>
                <asp:Label ID="reserved_month_parkingtype" runat="server" Text='<%# Eval("Parkingtype") %>' 
                                        Visible="False"></asp:Label>
                                         <asp:Label ID="reserved_month_amount" runat="server" Text='<%# Eval("Amount") %>' 
                                        Visible="False"></asp:Label>
                                          <asp:Label ID="chargeby_reserved_monthly" runat="server" Text='<%# Eval("chargebyname") %>' 
                                        Visible="False"></asp:Label>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
                         </asp:Repeater>
                     </td>
                 </tr>
             </table>
             </div>
         </fieldset>
         </asp:Panel></td>
</tr>
</table>
</td>
<td style="width:400px;"> <%--<img src="//maps.googleapis.com/maps/api/streetview?size=400x400&amp;location=  44.3976674,-79.7198538&amp;fov=90&amp;heading=235&amp;pitch=10" width="400" height="400px">--%>
            <asp:Image ID="Image1" runat="server" Width="355px"  />
            <div id ="map" 
                    style="height:300px; width:355px;"></div>
            </td>
</tr>
  
</table>
<table width="100%" class="coupan-result">
<tr>
<td>
    &nbsp;</td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>
</asp:Panel>
</div>
</div>
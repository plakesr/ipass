﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_lot_details : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            int lotid = int.Parse(Request.QueryString["Lot_id"].ToString());
            if (lotid > 0)
            {

                DataTable dt = new clsInsert().get_Lotdetails_lotsearch(Convert.ToInt32(lotid));
                if (Convert.ToDateTime(dt.Rows[0]["EffectiveDate"].ToString()) < DateTime.Now && dt.Rows[0]["Approoval"].ToString()!="False")
                {
                    try
                    {
                        if (dt.Rows.Count > 0)
                        {

                            lbl_name.Text = dt.Rows[0]["LotName"].ToString();
                            Session["Lotnamefinv"] = lbl_name.Text;


                            DataSet dscount = new clsInsert().fetchrec("select count(tbl_ParkerRegis.UserName) as countspace from tbl_ParkerRegis where  ParkingType=1 and LotId=" + Convert.ToInt32(dt.Rows[0]["Lot_id"].ToString()));
                            if (dscount.Tables.Count > 0)
                            {
                                int lottotalspace = Convert.ToInt32(dt.Rows[0]["Reserved_TotalSpace"].ToString());
                                int lotunablespace = Convert.ToInt32(dt.Rows[0]["Reserved_Unablespace"].ToString());
                                int availabeltotalspace = lottotalspace - lotunablespace;

                                int totalspace = availabeltotalspace;
                               int counttotal = Convert.ToInt32(dscount.Tables[0].Rows[0][0].ToString());


                                 //int counttotal = Convert.ToInt32(70);
                                int countspace = totalspace - counttotal;






                                int lottotalspace_total = Convert.ToInt32(dt.Rows[0]["TotalSpaces"].ToString());
                                int lotunablespace_total = Convert.ToInt32(dt.Rows[0]["UnableSpace"].ToString());
                                int availabeltotalspace_total = lottotalspace_total - lotunablespace_total;

                                int totalspace_total = availabeltotalspace_total;
                                int counttotal_total = Convert.ToInt32(dscount.Tables[0].Rows[0][0].ToString());


                                //int counttotal = Convert.ToInt32(70);
                                int countspace_total = totalspace_total - counttotal_total;


                                if (countspace_total >= 0)
                                {
                                    Lbl_TotalSpace.Text = Convert.ToString(countspace_total);
                                }
                                if (countspace_total < 0)
                                {
                                    Lbl_TotalSpace.Text = "No Space";

                                }

                                if (countspace >= 0)
                                {
                                    lbl_space.Text = Convert.ToString(countspace);
                                }
                                if (countspace < 0)
                                {
                                    lbl_space.Text = "No Space";
                                }
                            }

                            lbl_entrance.Text = dt.Rows[0]["English_EntryGateInformation"].ToString();
                            lbl_exit.Text = dt.Rows[0]["English_ExitGateInformation"].ToString();
                            lbl_restriction.Text = dt.Rows[0]["Englis_Restrictions"].ToString();
                            
                            string longitude = dt.Rows[0]["Lot_Longitude"].ToString().Trim();
                            Session["Lot_Longitude"] = longitude;
                            string latitude = dt.Rows[0]["Lot_Latitude"].ToString().Trim();
                            Session["Lot_Latitude"] = latitude;

                            Image1.ImageUrl = @"http://maps.googleapis.com/maps/api/streetview?size=290x290&location=" + decimal.Parse(latitude) + "," + decimal.Parse(longitude) + "&fov=90&heading=235&pitch=10%22";
                            string reser = dt.Rows[0]["Reserved_parking"].ToString();
                            if (dt.Rows[0]["Reserved_parking"].ToString() == "True")
                            {
                                int chargebyid = 3;
                                int parkingtypeid = 1;
                                DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearchmonth(lotid, chargebyid, parkingtypeid);
                                rpt_ReservedMonthly.DataSource = dt1;
                                rpt_ReservedMonthly.DataBind();
                            }

                            if (dt.Rows[0]["Random_parking"].ToString() == "True")
                            {
                                int chargebyid = 3;
                                int parkingtypeid = 2;
                                DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearchmonth(lotid, chargebyid, parkingtypeid);
                                //DataTable dt = new clsInsert().get_LotRatedetails_lotsearch(8,3,1)
                                rpt_RandomMonthly.DataSource = dt1;
                                rpt_RandomMonthly.DataBind();
                            }


                            if (dt.Rows[0]["Reserved_parking"].ToString() == "True")
                            {
                                //int chargebyid = 2;
                                int parkingtypeid = 1;
                                DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearch(lotid, 3, parkingtypeid);
                                rpt_ReservedDaily.DataSource = dt1;
                                rpt_ReservedDaily.DataBind();
                            }

                            if (dt.Rows[0]["Random_parking"].ToString() == "True")
                            {
                                int chargebyid = 3;
                                int parkingtypeid = 2;
                                DataTable dt1 = new clsInsert().get_LotRatedetails_lotsearch(lotid, chargebyid, parkingtypeid);
                                //DataTable dt = new clsInsert().get_LotRatedetails_lotsearch(8,3,1)
                                rpt_RandomDaily.DataSource = dt1;
                                rpt_RandomDaily.DataBind();
                            }


                        }
                    }
                    catch
                    {
                        Response.Redirect("LotAddresssearch.aspx");
                    }
                }

                else
                {
                    
                    Response.Redirect("AvailableLotSpace.aspx?msg_time=true");
                }
            }
            else
            {
                Response.Redirect("LotAddresssearch.aspx");
            }
        }
        catch
        {


        } 
    
        }


    protected void rpt_RandomDaily_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {

            //if (lbl_space.Text != "No Space")
            //{
                int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
                Session["PlanrateID"] = PlanrateID;
                try
                {
                    if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
                    {
                        Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());


                    }
                }
                catch
                {
                    Session["LOTIDForInserting"] = Session["lotid"].ToString();

                }
                Label parkinttype = rpt_RandomDaily.Items[i].FindControl("random_daily_parkingtype") as Label;
                Label amount_random_daily_amount = rpt_RandomDaily.Items[i].FindControl("random_daily_amount") as Label;
                Label chargeby = rpt_RandomDaily.Items[i].FindControl("chargeby_random_daily") as Label;
                Session["Chargebyinv"] = chargeby.Text;
                Session["amount_parking"] = amount_random_daily_amount.Text;
                Session["Typeinv"] = "Random Parking";


                Session["parkinttype"] = int.Parse(parkinttype.Text);


                Response.Redirect("Registration_New_Parker.aspx");
            //}
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No space Available at this lot please try another lot')", true);

            //}
        }
    }
    protected void rpt_RandomMonthly_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        if (e.CommandName == "cmdEdit")
        {
            //if (lbl_space.Text != "No Space")
            //{
            int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
            Session["PlanrateID"] = PlanrateID;
            try{
            if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
            {
                Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());


            }
            }
            catch
            {
                Session["LOTIDForInserting"] = Session["lotid"].ToString();

            }
            Label parkinttype = rpt_RandomMonthly.Items[i].FindControl("random_month_parkingtype") as Label;
         
         Label amount_random_month_amount = rpt_RandomMonthly.Items[i].FindControl("random_month_amount") as Label;
         Session["amount_parking"] = amount_random_month_amount.Text;
         Session["parkinttype"] = int.Parse(parkinttype.Text);
         Label chargeby = rpt_RandomMonthly.Items[i].FindControl("chargeby_random_monthly") as Label;
         Session["Chargebyinv"] = chargeby.Text;
         Session["Typeinv"] = "Random Parking";

            Response.Redirect("Registration_New_Parker.aspx");



        //}
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No space Available at this lot please try another lot')", true);

        //    }

        }
            
    }
    protected void rpt_ReservedDaily_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;

        if (e.CommandName == "cmdEdit")
        {
            if (lbl_space.Text != "No Space")
            {
                int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
                Session["PlanrateID"] = PlanrateID;
                try
                {
                    if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
                    {
                        Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());


                    }
                }
                catch
                {
                    Session["LOTIDForInserting"] = Session["lotid"].ToString();

                }
                Label parkinttype = rpt_ReservedDaily.Items[i].FindControl("reserved_daily_parkingtype") as Label;

                Label amount_reserved_daily_amount = rpt_ReservedDaily.Items[i].FindControl("reserved_daily_amount") as Label;
                Session["amount_parking"] = amount_reserved_daily_amount.Text;
                Session["parkinttype"] = int.Parse(parkinttype.Text);
                Session["Typeinv"] = "Reserved Parking";

                Label chargeby = rpt_ReservedDaily.Items[i].FindControl("chargeby_reserved_daily") as Label;
                Session["Chargebyinv"] = chargeby.Text;
                Response.Redirect("Registration_New_Parker.aspx");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No space Available at this lot please try another lot')", true);

            }
        }
    }
    protected void rpt_ReservedMonthly_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       


        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdEdit")
        {
            if (lbl_space.Text != "No Space")
            {
            int PlanrateID = Convert.ToInt32(e.CommandArgument.ToString());
            Session["PlanrateID"] = PlanrateID;
            try
            {
            if (Request.QueryString["Lot_id"].ToString() != null || Request.QueryString["Lot_id"].ToString() != "")
            {
                Session["LOTIDForInserting"] = int.Parse(Request.QueryString["Lot_id"].ToString());

            }
            }
            catch
            {
                Session["LOTIDForInserting"] = Session["lotid"].ToString();

            }
            Label parkinttype = rpt_ReservedMonthly.Items[i].FindControl("reserved_month_parkingtype") as Label;
           
           Label amount_reserved_month_amount = rpt_ReservedMonthly.Items[i].FindControl("reserved_month_amount") as Label;
           Session["amount_parking"] = amount_reserved_month_amount.Text;
           Session["parkinttype"] = int.Parse(parkinttype.Text);
           Session["Typeinv"] = "Reserved Parking";

           Label chargeby = rpt_ReservedMonthly.Items[i].FindControl("chargeby_reserved_monthly") as Label;
           Session["Chargebyinv"] = chargeby.Text;
            Response.Redirect("Registration_New_Parker.aspx");

            
        }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No space Available at this lot please try another lot')", true);

            }
    }
      
    }
   
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_lotdistance_rootmap.ascx.cs" Inherits="WUC_wuc_lotdistance_rootmap" %>
<style type="text/css">
    .style1
    {
        width: 92%;
        height: 280px;
    }
    .style2
    {
        width: 228px;
    }
    .style3
    {
        width: 331px;
    }
</style>
<link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type ="text/javascript">
    var map;
    function initialize() {
        var lat = '<%= Session["Lot_Latitude"]%>';
        var long = '<%= Session["Lot_Longitude"] %>';

        var latlng = new google.maps.LatLng(lat, long);
        var myOptions = {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        var marker = new google.maps.Marker
        (
            {
                position: new google.maps.LatLng(lat, long),
                map: map,
                title: 'Click me'
            }
        );
        var infowindow = new google.maps.InfoWindow({
            content: 'Location info:<br/>Country Name:<br/>LatLng:'
        });
        google.maps.event.addListener(marker, 'click', function () {
            // Calling the open method of the infoWindow 
            infowindow.open(map, marker);
        });
    }
    window.onload = initialize;
</script>
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
<div class="clearfix"></div>
<div style="width:100%; clear:both; padding:20px 0 0 0;">
    <table class="timezone-table">
        <tr>
            <td valign="top">
                <table width="100%" cellpadding="5" style=" border-collapse:collapse;">
                    <tr>
                        <td >
                            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            <asp:Label ID="lbl_countryname" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_distance" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                            <asp:Label ID="lbl_namecountry" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_time" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:LinkButton ID="likbtn_rootmap" runat="server" CssClass="button" 
                                onclick="likbtn_rootmap_Click">Root Map</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                  </td>
            <td>
                <div id="map" style="height:400px; width: 610px;"></div>
                </td>
        </tr>
    </table>
</div>
</div>
</div>
<div class="footer-container"></div>
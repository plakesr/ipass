﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class WUC_wuc_lotdistance_rootmap : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
        int id = int.Parse(Request.QueryString["lot_id"].ToString());
        if (id > 0)
        {

            DataTable dt = this.GetData("select tbl_LotMaster.*,tbl_City.City_name from tbl_LotMaster inner join tbl_City on tbl_LotMaster.City=tbl_City.City_id inner join tbl_Province on tbl_City.Province_id=tbl_Province.ProvinceId inner join tbl_country on tbl_Province.Country_id=tbl_country.Country_id where tbl_LotMaster.Lot_id='" + id+ "'");

            if (dt.Rows.Count > 0)
            {

                lbl_countryname.Visible = true;
                Label1.Text = "Distance From Current City To Lot Address";
                lbl_countryname.Text = dt.Rows[0]["Address"].ToString();

                lbl_namecountry.Visible = true;
                Label2.Text = "Distance From Current City To Lot Address";
                lbl_namecountry.Text = dt.Rows[0]["Address"].ToString();

               

               
                string s1 = dt.Rows[0]["Lot_Latitude"].ToString();
                string s2 = dt.Rows[0]["Lot_Longitude"].ToString();
                string final1 = s1 + "," + s2;
                try
                {
                    double d = GetDrivingDistanceInMiles(final1, dt.Rows[0]["City_name"].ToString());
                    string d1 = GetDrivinghoursInHours(final1, dt.Rows[0]["City_name"].ToString());

                    lbl_time.Visible = true;
                    lbl_time.Text = d1;
                    lbl_distance.Visible = true;
                    lbl_distance.Text = Convert.ToString(d) + "Km";
                }
                catch
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No route available.')", true);
                }
                ////string d1 = GetDrivinghoursInHours("dholpur", "jaipur");
                //

                //string d4 = GetDrivinghoursInHours("England", " Sweden");

                //TextBox1.Text = Convert.ToString(d);
                //TextBox2.Text = d1;
                //TextBox3.Text = Convert.ToString(d2);
                //TextBox4.Text = d4;

                //
            }
        }
        }
        catch
        {
            try
            {

                if (!IsPostBack == true)
                {
                    lbl_countryname.Visible = true;
                    Label1.Text = "Distance From Lot City To Lot Address";
                    lbl_countryname.Text = Session["address"].ToString();

                    lbl_namecountry.Visible = true;
                    Label2.Text = "Distance From Lot City To Lot Address";
                    lbl_namecountry.Text = Session["address"].ToString();
                    //Label1.Text = Session["Lot_Latitude"].ToString();
                    //Label2.Text = Session["Lot_Longitude"].ToString();

                    //lbl_Namecountry.Text = Session["currentlocationname"].ToString();
                    string s1 = Session["Lot_Latitude"].ToString();
                    string s2 = Session["Lot_Longitude"].ToString();
                    string final1 = s1 + "," + s2;
                    try
                    {
                        double d = GetDrivingDistanceInMiles(final1, Session["currentcity"].ToString());
                        string d1 = GetDrivinghoursInHours(final1, Session["currentcity"].ToString());

                        lbl_time.Visible = true;
                        lbl_time.Text = d1;
                        lbl_distance.Visible = true;
                        lbl_distance.Text = Convert.ToString(d) + "Km";
                    }
                    catch
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No route available.')", true);
                    }
                    ////string d1 = GetDrivinghoursInHours("dholpur", "jaipur");
                    //

                    //string d4 = GetDrivinghoursInHours("England", " Sweden");

                    //TextBox1.Text = Convert.ToString(d);
                    //TextBox2.Text = d1;
                    //TextBox3.Text = Convert.ToString(d2);
                    //TextBox4.Text = d4;

                    //
                }
            }
            catch
            {
                Response.Redirect("LotAddresssearch.aspx");
            }
        }
        
    }


    private DataTable GetData(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        SqlCommand cmd = new SqlCommand(query);
        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;

                sda.SelectCommand = cmd;
                using (DataTable dt = new DataTable())
                {
                    sda.Fill(dt);
                    return dt;
                }
            }
        }
    }

    public string GetDrivinghoursInHours(string origin, string destination)
    {
        string url = @"http://maps.googleapis.com/maps/api/distancematrix/xml?origins=" +
          origin + "&destinations=" + destination +
          "&mode=driving&sensor=false&language=en-EN";

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        WebResponse response = request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        StreamReader sreader = new StreamReader(dataStream);
        string responsereader = sreader.ReadToEnd();
        response.Close();

        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(responsereader);


        if (xmldoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
        {
            //   XmlNodeList distance = xmldoc.GetElementsByTagName("distance");
            XmlNodeList duration = xmldoc.GetElementsByTagName("duration");

            string s = duration[0].ChildNodes[1].InnerText;
            return Convert.ToString(s);

            //string m = duration[0].ChildNodes[1].InnerText.Replace(" mins", "");
        }
        else
        {
            return "";
        }
    }
    public static double GetDrivingDistanceInMiles(string origin, string destination)
    {

        string url = @"http://maps.googleapis.com/maps/api/distancematrix/xml?origins=" +
          origin + "&destinations=" + destination +
          "&mode=driving&sensor=false&language=en-EN";

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        WebResponse response = request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        StreamReader sreader = new StreamReader(dataStream);
        string responsereader = sreader.ReadToEnd();
        response.Close();

        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(responsereader);


        if (xmldoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
        {
            XmlNodeList distance = xmldoc.GetElementsByTagName("distance");
            XmlNodeList duration = xmldoc.GetElementsByTagName("duration");

            //string s = duration[0].ChildNodes[1].InnerText;
            return Convert.ToDouble(distance[0].ChildNodes[1].InnerText.Replace(" km", ""));

            //string m = duration[0].ChildNodes[1].InnerText.Replace(" mins", "");
        }
        else
        {
            return 0;
        }

    }
    protected void likbtn_rootmap_Click(object sender, EventArgs e)
    {
        Response.Redirect("getlotroot.aspx");
    }
}
  
    

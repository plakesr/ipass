﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_lotmanager.ascx.cs" Inherits="wuc_lotmanager" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style2
    {
        width: 175px;
    }
    .style3
    {
        width: 177px;
    }
    .style4
    {
        width: 178px;
    }
    .style5
    {
        width: 190px;
    }
    .form-table
    {
        height: 176px;
        width: 804px;
    }
    .style10
    {
        width: 100%;
    }
    .style11
    {
        height: 131px;
    }
    .style13
    {
        height: 241px;
    }
    .style14
    {
        width: 739px;
    }
    .style15
    {
        width: 120px;
    }
    .style17
    {
        width: 209px;
    }
    .style19
    {
        width: 171px;
    }
    .style20
    {
        width: 359px;
    }
    .style21
    {
        width: 213px;
    }
    .style22
    {
        width: 840px;
    }
    .style23
    {
        width: 248px;
    }
    </style>

<table width="80%" align="center">
      <tr>
        <td>
           
          <asp:Button Text="General Details" BorderStyle="None" ID="btn_generaldetails" 
                CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Contact Details" BorderStyle="None" ID="btn_contactDetails" 
                CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Web Direction" BorderStyle="None" ID="btn_webDirection" 
                CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
              <asp:Button Text="Billing Details" BorderStyle="None" ID="btn_billing" 
                CssClass="Initial" runat="server"
              OnClick="Tab4_Click" />
             
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
              <table style="width: 100%; height: 1016px;">
                <tr>
                  <td>
                      <span>
                      <asp:Panel ID="Panel1" runat="server">
                          <fieldset>
                              <legend>General Details<span>
                                  </span></legend>
                              <table class="form-table">
                                  <tr>
                                      <td class="style2">
                                          Customer Name</td>
                                      <td>
                                          <asp:DropDownList ID="ddt_sitename" runat="server" 
                                              CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource1" 
                                              DataTextField="SiteName" DataValueField="SiteId" ValidationGroup="a" 
                                              AutoPostBack="True" AppendDataBoundItems="true">
                                               <asp:ListItem Selected = "True" Text = "----Select Customer----" Value = "0"></asp:ListItem>
                                          </asp:DropDownList>
                                          <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                              ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                              SelectCommand="SELECT [SiteId], [SiteName] FROM [tbl_SiteMaster]">
                                          </asp:SqlDataSource>
                                          <asp:RequiredFieldValidator ID="rfv_sitename" runat="server" 
                                              ControlToValidate="ddt_sitename" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="a"></asp:RequiredFieldValidator>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Campus Name</td>
                                      <td>
                                          <asp:DropDownList ID="ddl_campusname" runat="server" 
                                              CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource5" 
                                              DataTextField="Campus_Name" DataValueField="CampusID" Height="30px" 
                                              ValidationGroup="a" Width="141px" AppendDataBoundItems="true">
                                               <asp:ListItem Selected = "True" Text = "----Select Campus----" Value = "0"></asp:ListItem>

                                          </asp:DropDownList>
                                          <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                              ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                              
                                              SelectCommand="SELECT * FROM [tbl_CampusMaster] WHERE ([SiteID] = @SiteID)">
                                              <SelectParameters>
                                                  <asp:ControlParameter ControlID="ddt_sitename" Name="SiteID" 
                                                      PropertyName="SelectedValue" Type="Int64" />
                                              </SelectParameters>
                                          </asp:SqlDataSource>
                                          <asp:RequiredFieldValidator ID="rfv_sitetype" runat="server" 
                                              ControlToValidate="ddl_campusname" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="a"></asp:RequiredFieldValidator>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Lot&nbsp;&nbsp; Name</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_lotname" runat="server" 
                                              ontextchanged="txtbox_name_TextChanged" ValidationGroup="m"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="rfv_LotName" runat="server" 
                                              ControlToValidate="txtbox_lotname" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Lot&nbsp;&nbsp; Code</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_lotcode" runat="server" ValidationGroup="m"></asp:TextBox>
                                          <span>
                                          <asp:RequiredFieldValidator ID="rfv_Lotcode" runat="server" 
                                              ControlToValidate="txtbox_lotcode" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                          </span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Effective Date</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_date" runat="server" ValidationGroup="m"></asp:TextBox>
                                          <cc1:CalendarExtender ID="txtbox_date_CalendarExtender" runat="server" 
                                              Enabled="True" TargetControlID="txtbox_date">
                                          </cc1:CalendarExtender>
                                          <span>
                                          <asp:RequiredFieldValidator ID="rfv_effectivedate" runat="server" 
                                              ControlToValidate="txtbox_date" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                          </span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Approval</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_approval" runat="server" ValidationGroup="m"></asp:TextBox>
                                          <span>
                                          <asp:RequiredFieldValidator ID="rfv_approval" runat="server" 
                                              ControlToValidate="txtbox_approval" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                          </span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Time Zone</td>
                                      <td>
                                          <asp:DropDownList ID="ddl_timezone" runat="server"  AppendDataBoundItems="true">
                                               <asp:ListItem Selected = "True" Text = "----Select Time----" Value = "0"></asp:ListItem>

                                          </asp:DropDownList>
                                        
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Lot Hours</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_hours" runat="server" ValidationGroup="m"></asp:TextBox>
                                          <span>
                                          <asp:RequiredFieldValidator ID="rfv_lothours" runat="server" 
                                              ControlToValidate="txtbox_hours" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                          </span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Lot Total Space</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_totalspace" runat="server" ValidationGroup="m"></asp:TextBox>
                                          <span>
                                          <asp:RequiredFieldValidator ID="rfv_totalspace" runat="server" 
                                              ControlToValidate="txtbox_totalspace" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                          <%--<asp:RegularExpressionValidator ID="rev_totalspace" runat="server" 
                                              ControlToValidate="txtbox_totalspace" ErrorMessage="Enter Only Number " 
                                              ForeColor="#CC0000" ValidationExpression="^\d$" ValidationGroup="m"></asp:RegularExpressionValidator>--%>
                                          </span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="style2">
                                          Unable Space</td>
                                      <td>
                                          <asp:TextBox ID="txtbox_unablespace" runat="server" 
                                              ontextchanged="txtbox_unablespace_TextChanged" ValidationGroup="m"></asp:TextBox>
                                          <span>
                                          <asp:RequiredFieldValidator ID="rfv_unablespace" runat="server" 
                                              ControlToValidate="txtbox_unablespace" ErrorMessage="*" ForeColor="#CC0000" 
                                              ValidationGroup="m"></asp:RequiredFieldValidator>
                                          </span>
                                          <%--<asp:RegularExpressionValidator ID="rev_unablespace" runat="server" 
                                              ControlToValidate="txtbox_unablespace" ErrorMessage="Enter Only Number " 
                                              ValidationExpression="^\d$" ForeColor="#CC0000" ValidationGroup="m"></asp:RegularExpressionValidator>--%>
                                      </td>
                                  </tr>
                              </table>
                              <br />
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button 
                                  ID="btn1_next" runat="server" CssClass="button" Height="35px" 
                                  onclick="btn1_next_Click" Text="Next" Width="79px" ValidationGroup="m" />
                          </fieldset>
                      </asp:Panel>
                      <br />
                     
                      </span>
                  
                      
        
                 
                           
                   
                  </td>
                </tr>
              </table>
            </asp:View>
            <br />
            <br />
            <br />

            <asp:View ID="View2" runat="server">
          
              <table >
                <tr>
                  <td class="style22">
                    
                        &nbsp;
                       <%-- <asp:Panel ID="Panel3" runat="server" Width="713px">--%>

    
    <fieldset>
    <legend>
        Contact&nbsp; Details 
    </legend>
    <br />

        <table class="form-table">
        <tr>
                <td class="style3">
                    Address</td>
                <td class="style23">
                   <%-- <asp:TextBox ID="txtbox_address" runat="server" Height="45px" 
                        TextMode="MultiLine" Width="231px"></asp:TextBox>--%>


                        <asp:TextBox ID="address" runat="server"></asp:TextBox>
                </td>
                <td rowspan="4">
                    <div id="map_canvas" style="width:300px; height:300px"></div></td>
            </tr>

            <tr>
                <td class="style3">
                    City</td>
                <td class="style23">
                    <asp:DropDownList ID="ddl_city" runat="server" DataSourceID="SqlDataSource2" 
                        DataTextField="City_name" DataValueField="City_id">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT [City_id], [City_name] FROM [tbl_City]">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Postal Code/Zip Code</td>
                <td class="style23">
                    <asp:TextBox ID="txtbox_zip" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                
                
                <td class="style3">
                    Cellular</td>
                <td class="style23">
                    <asp:TextBox ID="txtbox_cell" runat="server"></asp:TextBox>
                </td>
                
                
            </tr>
             <tr>
                
                
                <td class="style3">
                    Office No</td>
                <td class="style23">
                    <asp:TextBox ID="txtbox_office" runat="server"></asp:TextBox>
                 </td>
                
                
            </tr>
             <tr>
                
                
                <td class="style3">
                    E-mail</td>
                <td class="style23">
                    <asp:TextBox ID="txtbox_email" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rev_email" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="Formate is wrong" 
                        ForeColor="#CC0000" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                 </td>
                
                
                 <td>
                          
    <label for="latitude">
        Latitude</label>&nbsp;&nbsp;&nbsp;
   <asp:TextBox ID="latitude" runat="server" Enabled="false"></asp:TextBox><br />
   <%-- <input id="txtLat" type="text" value="28.47399" />--%>
    <label for="longitude">
        Longitude</label>
   <asp:TextBox ID="longitude" runat="server" Enabled="false"></asp:TextBox></td>
                
                
            </tr>
        </table>


   
<%--<input id="txtLng" type="text" value="77.026489" /><br />--%>
    <br />
    <br />
    <br/>
        <br />
        <br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button 
            ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" 
            CssClass="button" Height="35px" Width="79px"></asp:Button>
    </fieldset>
 <%--  </asp:Panel>--%>


                        


                    
                  </td>
                </tr>
              </table>
            
              <br />
            </asp:View>
            <br />

            <asp:View ID="View3" runat="server">
              <table class="form-table">
                <tr>
                  <td>
                    <h3>
                        &nbsp;<asp:Panel ID="Panel4" runat="server" Height="227px" Width="705px">
                            <fieldset>
                                <legend>Web Direction English</legend>
                                <table class="form-table">
                              
                                    <tr>
                                        <td class="style4">
                                            Enterance Description</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_enteranceinformationEnglish" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" Width="155px" 
                                                TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_enterance_information" runat="server" 
                                                ControlToValidate="txtbox_enteranceinformationEnglish" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            Exit Description</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_exitinformationEnglish" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_exit_information" runat="server" 
                                                ControlToValidate="txtbox_exitinformationEnglish" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            Lot Restriction</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_restriction" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_restriction" runat="server" 
                                                ControlToValidate="txtbox_restriction" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Lot Description</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_Description" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                                                ControlToValidate="txtbox_Description" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset></asp:Panel>  
                 
                        <asp:Panel ID="Panel5" runat="server">
                        <fieldset>
                                <legend>Web Direction French</legend>
                                <table class="form-table">
                              
                                    <tr>
                                        <td class="style4">
                                            Enterance Description</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_enteranceinformationFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" Width="155px" 
                                                TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_enterance_informationfrench" runat="server" 
                                                ControlToValidate="txtbox_enteranceinformationFrench" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            Exit Description</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_exitinformationFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_exit_informationfrench" runat="server" 
                                                ControlToValidate="txtbox_exitinformationFrench" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            Lot Restriction</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_restrictionFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" 
                                                ontextchanged="txtbox_restrictionFrench_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_restrictionfrench" runat="server" 
                                                ControlToValidate="txtbox_restrictionFrench" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Lot Description</td>
                                        <td class="style5">
                                            <asp:TextBox ID="txtbox_DescriptionFrench" runat="server" 
                                                CssClass="twitterStyleTextbox" ValidationGroup="n" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_descriptionfrench" runat="server" 
                                                ControlToValidate="txtbox_DescriptionFrench" ErrorMessage="*" ForeColor="#CC0000" 
                                                ValidationGroup="n"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </asp:Panel>
                      
                      
                      
                      
                  </td>
                </tr>
              </table>
              <table class="form-table">
                <tr>
                  <td>
           
                             
                      
                        
                      <%--      <table class="form-table">
                                <tr>
                                    <td>
                                        Lot Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownList1" runat="server">
                                            <asp:ListItem Value="1">Key Scan</asp:ListItem>
                                            <asp:ListItem Value="2">HangTags</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Other Details
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server" Height="42px" TextMode="MultiLine" 
                                            Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>--%>
                                &nbsp;Lot Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:DropDownList ID="ddl_lottype" runat="server">
                                    <asp:ListItem Value="1">Key Scan</asp:ListItem>
                                    <asp:ListItem Value="2">HangTags</asp:ListItem>
                                </asp:DropDownList>
                                <br />

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br />
                                Other Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtbox_otherdetailsweb" runat="server" Height="42px" 
                                    TextMode="MultiLine" Width="200px"></asp:TextBox>
                            <br />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="next3" runat="server" CssClass="button" Height="35px" 
                                Text="Next" Width="79px" onclick="next3_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                   
                      
                      
                      
                     
                      
                      
                    
                  
                  </td>
                </tr>
              </table>
            </asp:View>
        
              <asp:View ID="View4" runat="server">
              
                  <table class="style10">
                      <tr>
                <td class="style9">
                    Billing Entry</td>
                <td class="style14">
                    <asp:DropDownList ID="ddl_billingEntry" runat="server">
                        <asp:ListItem Value="1">First Month Full</asp:ListItem>
                        <asp:ListItem Value="2">Prorate</asp:ListItem>
                        <asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td class="style9">
                    Billing Exit</td>
                <td class="style14">
                    <asp:DropDownList ID="ddl_billingexit" runat="server">
                        <asp:ListItem Value="1">First Month Full</asp:ListItem>
                        <asp:ListItem Value="2">Prorate</asp:ListItem>
                        <asp:ListItem Value="3">Prorate By Fort Night</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td class="style9">
                    Payment Reminder Days</td>
                <td class="style14">
                    <asp:TextBox ID="txtbox_paymentreminderdays" runat="server"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="rfv_paymentReminderCharge" runat="server" 
                        ControlToValidate="txtbox_paymentreminderdays" ErrorMessage="*" 
                        ForeColor="#CC0000"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td class="style9">
                    Interest Rate Payment</td>
                <td class="style14">
                    <asp:TextBox ID="txtbox_interestrate" runat="server"></asp:TextBox>
                   <%-- <asp:RequiredFieldValidator ID="rfv_interestratepayment" runat="server" 
                        ControlToValidate="txtbox_interestrate" ErrorMessage="*" ForeColor="#CC0000"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                
                
                <td class="style9">
                    Late Fee($)</td>
                <td class="style14">
                    <asp:TextBox ID="txtbox_latefee" runat="server"></asp:TextBox>
                </td>
                
                
            </tr>
             <tr>
                
                
                <td class="style9">
                    Activation Charge</td>
                <td class="style14">
                    <asp:TextBox ID="txtbox_Activation" runat="server"></asp:TextBox>
                 </td>
                
                
            </tr>
             <tr>
                
                
                <td class="style9">
                    Display Activation Charge</td>
                <td class="style14">
                    <asp:RadioButton ID="rbtn_yes" runat="server" GroupName="r" 
                        oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtn_no" runat="server" GroupName="r" Text="No" />
                 </td>
                
                
            </tr>
            <tr>
            <td class="style9">
                Display Deposit</td>
            <td class="style14">
                <asp:RadioButton ID="rbtn_yes_deposite" runat="server" GroupName="r1" 
                    oncheckedchanged="rbtn_yes_CheckedChanged" Text="Yes" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="rbtn_no_deposite" runat="server" GroupName="r1" Text="No" />
                </td>
            </tr>
             <tr>
            <td class="style9">
                Late Payment Action</td>
            <td class="style14">
                <asp:DropDownList ID="ddl_latepaymentaction" runat="server">
                    <asp:ListItem Value="0">Disable Card</asp:ListItem>
                    <asp:ListItem Value="1">Send Email by Notification</asp:ListItem>
                </asp:DropDownList>
                 </td>
            </tr>
             <tr>
            <td class="style9">
                Credit Card Payment Failed</td>
            <td class="style14">
                <asp:DropDownList ID="ddl_creditcardpayment" runat="server">
                    <asp:ListItem Value="0">Disable Card</asp:ListItem>
                    <asp:ListItem Value="1">Send Email by Notification</asp:ListItem>
                </asp:DropDownList>
                 </td>
            </tr>

            <tr>
           <td>
           Transaction Type
           </td>
           <td>
            <asp:CheckBox ID="chk_monthly" runat="server" Text="Monthly" />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:CheckBox ID="chk_daily" runat="server" Text="Daily" />
           </td>
           </tr>





                      
                     
                     
                   
                      <tr>
                    
                      
                   
                          <td class="style11" colspan="2">
                          
                              <table class="style10">
                                  <tr>
                                      <td class="style20">
                                         <asp:CheckBox ID="chbk_Reserved" runat="server" Text="Reserved" 
                                             /></td>
                                      <td class="style17">
                                      Total Space:
                                          <asp:TextBox ID="txtbox_reservedtotalspace" runat="server" Height="20px" 
                                              Width="91px"></asp:TextBox>
                                      </td>
                                      <td class="style21">
                                      Unable Space:
                                          <asp:TextBox ID="txtbox_reservedunablespace" runat="server" Height="19px" 
                                              Width="83px"></asp:TextBox>
                                      </td>
                                      <td class="style19">
                                          <asp:Button ID="Rate_for_reserevd" runat="server" Text="Rate_reserved" 
                                              onclick="Rate_for_reserevd_Click" />
                                      </td>
                                          </td>
                                  </tr>
                                  <tr>
                                      <td class="style20">
                                         
                                              
                                          
                                          <asp:CheckBox ID="chbk_Random" runat="server" 
                                             Text="Random" />
                                         
                                              
                                          
                                      </td>
                                      <td class="style17">
                                      Total Space:
                                          <asp:TextBox ID="txtbox_randomtotalspace" runat="server" 
                                              ontextchanged="TextBox2_TextChanged" Height="21px" Width="89px"></asp:TextBox>
                                      </td>
                                      <td class="style21">
                                      Unable Space
                                          <asp:TextBox ID="txtbox_randomunablespace" runat="server" Height="20px" 
                                              Width="88px"></asp:TextBox>
                                      </td>
                                      <td class="style19">
                                      
                                      
                                          <asp:Button ID="_Random" runat="server" Text="Rate_Random" 
                                              onclick="_Random_Click" />
                                      
                                      
                                      </td>
                                  </tr>
                              </table>
                          
                      <tr>
                          <td class="style13" colspan="2">
                              <asp:Panel ID="Panel8" runat="server" Height="209px">
                                  <table class="style10">
                                      <tr>
                                          <td colspan="3">
                                              TAXES</td>
                                      </tr>
                                      <tr>
                            <td class="style5">
                                <span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">
                                Corporate Tax ID #<span class="Apple-converted-space">&nbsp;</span></span></td>
                            <td class="style15">
                                <asp:TextBox ID="txtbox_corporate_taxid" runat="server"></asp:TextBox>
                            </td>
                                          <td rowspan="5">
                                                <asp:DataList ID="dl_tax" runat="server" Width="629px" 
            onselectedindexchanged="DataList1_SelectedIndexChanged">
                   <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" Font-Size="Large" ForeColor="White"
                HorizontalAlign="Center" VerticalAlign="Middle" />
            <HeaderTemplate>
               <b>Tax Name</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Tax(%)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Sequence</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Delete</b></HeaderTemplate>
            <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
            <ItemTemplate>
                 <%#Eval("taxname")%>
                   <%#Eval("tax")%>
                     <%#Eval("sequence")%>
            <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete"  runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                </ItemTemplate>
                 </asp:DataList></td>
                        </tr>
                        <tr>
                            <td class="style5">
                                <span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">
                                Tax Name</span></td>
                            <td class="style15">
                                <asp:TextBox ID="txtbox_taxname" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style5">
                                <span style="color: rgb(68, 68, 68); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">
                                Tax %</span></td>
                            <td class="style15">
                                <asp:TextBox ID="txtbox_tax" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style5">
                                <span style="color: rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21.299999237060547px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);">
                                &nbsp;Sequence for Calculation</span></td>
                            <td class="style15">
                                <asp:DropDownList ID="ddl_squencecal" runat="server">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                                      <tr>
                                          <td>
                                              &nbsp;</td>
                                          <td class="style15">
                                              <asp:Button ID="btn_tax" runat="server" Text="ADD TAX" 
                                                  onclick="btn_tax_Click" />
                                          </td>
                                      </tr>


                                   
                                  </table>
                              </asp:Panel>

                          </td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                          <td>
                               <asp:Button ID="btn_MainAdd" runat="server" onclick="btn_MainAdd_Click" 
                                  Text="Next" Width="101px" />
                              <asp:Button ID="btn_FINISH_FINAL" runat="server" Height="25px" 
                                  onclick="btn_FINISH_FINAL_Click" style="margin-top: 0px" Text="Finish" 
                                  visible="false"/>
                          </td>
                      </tr>
                       </asp:View>
               
                 

</asp:MultiView>
                  
       
        </td>
      </tr>
    </table>



       

   



    



       





       

   



    



       




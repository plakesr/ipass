﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class wuc_lotmanager : System.Web.UI.UserControl
{

    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
       if (!IsPostBack)
        {

               clsInsert onj = new clsInsert();
                DataSet rec = onj.fetchrec("SELECT [TimeZoneTblId], [TimeZoneName], [TimeZoneId] FROM [tbl_TimeZoneMaster] ORDER BY [TimeZoneId]");
                DataTable t = new DataTable("timezone");
                t.Columns.Add("TimeZoneTblId", typeof(int));
                t.Columns.Add("TimeZoneName", typeof(string));

                for (int i = 0; i < rec.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = t.NewRow();
                    dr[0] = rec.Tables[0].Rows[i][0].ToString();
                    dr[1] = rec.Tables[0].Rows[i][2].ToString() + " [" + rec.Tables[0].Rows[i][1].ToString() + "]";
                    t.Rows.Add(dr);
                }


                ddl_timezone.DataSource = t;
                ddl_timezone.DataTextField = "TimeZoneName";
                ddl_timezone.DataValueField = "TimeZoneTblId";
                ddl_timezone.DataBind();
            
            btn_generaldetails.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
            try
            {

                dt.Columns.Add("taxname", typeof(string));
                dt.Columns.Add("tax", typeof(decimal));
                dt.Columns.Add("sequence", typeof(int));



                Session["datatable"] = dt;
            }
            catch
            {

            }



            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }

            

        }
    }
    protected void ddt_spaceavailable_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    
    protected void btn_add_Click(object sender, EventArgs e)
    {
       
    }
    protected void Tab1_Click(object sender, EventArgs e)
    {
        btn_generaldetails.CssClass = "Clicked";
        btn_contactDetails.CssClass= "Initial";
        btn_webDirection.CssClass= "Initial";
        btn_billing.CssClass = "Initial";
        //btn_finish.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void Tab2_Click(object sender, EventArgs e)
    {

        btn_generaldetails.CssClass = "Initial";
        btn_contactDetails.CssClass = "Clicked";
        btn_webDirection.CssClass = "Initial";
        btn_billing.CssClass = "Initial";
        //btn_finish.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
    protected void Tab3_Click(object sender, EventArgs e)
    {
        btn_generaldetails.CssClass = "Initial";
        btn_contactDetails.CssClass = "Initial";
        btn_webDirection.CssClass = "Clicked";
        btn_billing.CssClass = "Initial";
        //btn_finish.CssClass = "Initial";
        MainView.ActiveViewIndex = 2;
    }
    string approval = "true";
    string lotstatus = "true";
    protected void btn1_next_Click(object sender, EventArgs e)
    {
        Hashtable h1 = new Hashtable();
        h1.Clear();
        h1.Add("siteid",ddt_sitename.SelectedItem.Value);
        h1.Add("campusid", ddl_campusname.SelectedItem.Value);
        h1.Add("Lotname", txtbox_lotname.Text);
        h1.Add("Lotcode",txtbox_lotcode.Text);
        h1.Add("Effctivedate", txtbox_date.Text);

        h1.Add("Approval",approval);
        h1.Add("Timezone", ddl_timezone.SelectedItem.Value);
        h1.Add("Lothours", txtbox_hours.Text);
        if (txtbox_totalspace.Text == " ")
        {
            h1.Add("Lottotalspace", 0);

        }
        else
        {
            h1.Add("Lottotalspace",Convert.ToInt32( txtbox_totalspace.Text));
        }
        if (txtbox_unablespace.Text == " ")
        {
            h1.Add("lotunablespace", 0);
        }
        else
        {
            h1.Add("lotunablespace",Convert.ToInt32( txtbox_unablespace.Text));

        }

        Session["hasht1"] = h1;

        MainView.ActiveViewIndex = 1;

    }
    protected void btn2_next_Click(object sender, EventArgs e)
    {
       

        Hashtable h2 = new Hashtable();
        h2.Clear();
        h2.Add("address", address.Text);
        h2.Add("city", ddl_city.SelectedItem.Value);
        h2.Add("zip", txtbox_zip.Text);
        h2.Add("cellular", txtbox_cell.Text);
        h2.Add("officeno", txtbox_office.Text);

        h2.Add("email", txtbox_email.Text);
        Session["lat"] = latitude.Text;
        Session["long"] = longitude.Text;
        h2.Add("latitude", Session["lat"]);
        h2.Add("longitude", Session["long"]);
         Session["hasht2"] = h2;
        MainView.ActiveViewIndex = 2;

    }
    protected void next3_Click(object sender, EventArgs e)
    {
        Hashtable h3 = new Hashtable();
        h3.Clear();
        
        h3.Add("entryenglish", txtbox_enteranceinformationEnglish.Text);
        h3.Add("exitenglish", txtbox_exitinformationEnglish.Text);
        h3.Add("restrictionenglish", txtbox_restriction.Text);
        h3.Add("descriptionenglish", txtbox_Description.Text);
        h3.Add("entryfrench", txtbox_enteranceinformationFrench.Text);

        h3.Add("exitfrench", txtbox_exitinformationFrench.Text);
        h3.Add("restrictionfrench", txtbox_restrictionFrench.Text);
        h3.Add("descriptionfrench", txtbox_DescriptionFrench.Text);

        h3.Add("Lottype", ddl_lottype.SelectedItem.Value);
        h3.Add("otherdetails", txtbox_otherdetailsweb.Text);
        Session["hasht3"] = h3;
        MainView.ActiveViewIndex = 3;
    }







    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void ddlvehiclechargetype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtbox_name_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtn_yes_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void txtbox_reserved_Amt_TextChanged(object sender, EventArgs e)
    {

    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    DataTable dt = new DataTable();
    protected void btn_add_Click1(object sender, EventArgs e)
    {
       

    }
    protected void Tab4_Click(object sender, EventArgs e)
    {
        btn_generaldetails.CssClass = "Initial";
        btn_contactDetails.CssClass = "Initial";
        btn_webDirection.CssClass = "Initial";
        btn_billing.CssClass = "Clicked";
        //btn_finish.CssClass = "Initial";

        MainView.ActiveViewIndex = 3;
    }
    protected void btn_addtax_Click(object sender, EventArgs e)
    {
       
       
           
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
       
       
    }
    protected void txtbox_restrictionFrench_TextChanged(object sender, EventArgs e)
    {

    }
    string displayactivation = "";
    string displaydeposit = "";
    string parking_reserved = "";
    string parking_random = "";
    int lot_reserved_totalspace = 0;
    int lot_reserved_unablespace = 0;
    int lot_random_totalspace = 0;
    int lot_random_unablespace = 0;

    string LotTransaction_Daily;
    string LotTransaction_Monthly;
    protected void btn_MainAdd_Click(object sender, EventArgs e)
    {
        Hashtable h4 = new Hashtable();
        h4.Add("Billingentry", ddl_billingEntry.SelectedItem.Value);
        h4.Add("Billingexit", ddl_billingexit.SelectedItem.Value);
        h4.Add("paymentreminder", txtbox_paymentreminderdays.Text);
        h4.Add("interestrate", txtbox_interestrate.Text);
        h4.Add("Latefee", txtbox_latefee.Text);
        h4.Add("activationcharge", txtbox_Activation.Text);
        if (rbtn_yes.Checked == true)
        {
            displayactivation = "true";
            h4.Add("displayactivationcharge", displayactivation);
        }
        else if (rbtn_no.Checked == false)
        {
            displayactivation = "false";
            h4.Add("displayactivationcharge", displayactivation);
        }

        if (rbtn_yes_deposite.Checked == true)
        {
            displaydeposit = "true";
            h4.Add("displaydeposite", displaydeposit);
        }
        else if (rbtn_yes_deposite.Checked == false)
        {
            displaydeposit = "false";
            h4.Add("displaydeposite", displaydeposit);
        }
        int j = Convert.ToInt32(ddl_latepaymentaction.SelectedItem.Value);
        h4.Add("latepaymentaction",j);
        int k=Convert.ToInt32( ddl_creditcardpayment.SelectedItem.Value);
        h4.Add("creditcardpaymentaction",k);

        if(chk_monthly.Checked==true)
        {
            LotTransaction_Daily="true";
        h4.Add("LotTransaction_Daily", LotTransaction_Daily);

        }
        else
        {
             LotTransaction_Daily="false";
        h4.Add("LotTransaction_Daily", LotTransaction_Daily);
        }
        if(chk_daily.Checked==true)
        {
             LotTransaction_Monthly="true";
        h4.Add("LotTransaction_Monthly", LotTransaction_Monthly);
        }
        else
        {
            LotTransaction_Monthly="false";
        h4.Add("LotTransaction_Monthly", LotTransaction_Monthly);
        }

        if (chbk_Reserved.Checked == true)
        {
            parking_reserved = "true";
            h4.Add("tranasactiontype_reserved", parking_reserved);
            h4.Add("reserved_totalspace",Convert.ToInt32 (txtbox_reservedtotalspace.Text));
            h4.Add("reserved_unablespace",Convert.ToInt32 ( txtbox_reservedunablespace.Text));

        }
        else
        {
            parking_reserved = "false";
            h4.Add("tranasactiontype_reserved", parking_reserved);
            h4.Add("reserved_totalspace",lot_reserved_totalspace );
            h4.Add("reserved_unablespace", lot_reserved_unablespace);

        }



        if (chbk_Random.Checked == true)
        {
            parking_random = "true";
            h4.Add("tranasactiontype_random", parking_random);
            h4.Add("random_totalspace",Convert.ToInt32 ( txtbox_randomtotalspace.Text));
            h4.Add("random_unablespace", Convert.ToInt32 (txtbox_randomunablespace.Text));

        }
        else
        {
            parking_random = "false";
            h4.Add("tranasactiontype_random", parking_random);
            h4.Add("random_totalspace", lot_random_totalspace);
            h4.Add("random_unablespace", lot_random_unablespace);

        }
        
        Session["hasht4"] = h4;
        btn_FINISH_FINAL.Visible = true;
        btn_MainAdd.Visible = false;
       // MainView.ActiveViewIndex = 4;
    }
    
    protected void btn_tax_Click(object sender, EventArgs e)
    {
        try
        {

            dt = (DataTable)Session["datatable"];
            DataRow dr = dt.NewRow();
            dr[0] = txtbox_taxname.Text;
            dr[1] = txtbox_tax.Text;
            dr[2] = ddl_squencecal.SelectedItem.Text;

            dt.Rows.Add(dr);

            Session["datatable"] = dt;
            if (dt.Rows.Count > 0)
            {
                dl_tax.DataSource = dt;
                dl_tax.DataBind();
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please fill  details.')", true);
        }
    }
    
    
    protected void btn_reserved_Click(object sender, EventArgs e)
    {
        
    }
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {

    }
    int parkingtyp;
    protected void Rate_for_reserevd_Click(object sender, EventArgs e)
    {
        if (chbk_Reserved.Checked == true)
        {
            parkingtyp = 1;
            Session["reservedparkingtype"] = parkingtyp;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Lot_Reserved_RateManager.aspx','New Windows','height=380, width=300,location=no');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please mark the checkbox first.')", true);
        }

    }
    protected void _Random_Click(object sender, EventArgs e)
    {
        if (chbk_Random.Checked == true)
        {
            parkingtyp = 2;
            Session["randomparkingtype"] = parkingtyp;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Lot_Random_RateManager.aspx','New Windows','height=380, width=300,location=no');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please mark the checkbox first.')", true);
        }
    }

    public void insertrec()
    {

        
            Hashtable first = (Hashtable)Session["hasht1"];
            int Siteid = Convert.ToInt32(first["siteid"].ToString());
            int campusid = Convert.ToInt32(first["campusid"].ToString());
            string lotname = first["Lotname"].ToString();
            string lotcode = first["Lotcode"].ToString();
            DateTime effectivedate = Convert.ToDateTime(first["Effctivedate"].ToString());
            string approv = first["Approval"].ToString();
            int timezone = Convert.ToInt32(first["Timezone"].ToString());
            string lothours = first["Lothours"].ToString();
            int totalspace = Convert.ToInt32(first["Lottotalspace"].ToString());

            int unabelspace = Convert.ToInt32(first["lotunablespace"].ToString());

            Hashtable second = (Hashtable)Session["hasht2"];

            string address = second["address"].ToString();
            int city = Convert.ToInt32(second["city"].ToString());
            string zip = second["zip"].ToString();
            string cellular = second["cellular"].ToString();
            string office = second["officeno"].ToString();
            string email = second["email"].ToString();
            string lat = second["latitude"].ToString();
            string longi = second["longitude"].ToString();


            Hashtable third = (Hashtable)Session["hasht3"];
            string entryenglish = third["entryenglish"].ToString();
            string exitenglish = third["exitenglish"].ToString();
            string desceng = third["descriptionenglish"].ToString();
            string restrictionenglish = third["restrictionenglish"].ToString();

            string entryfrench = third["entryfrench"].ToString();
            string exitfrench = third["exitfrench"].ToString();
            string descfrnch = third["descriptionfrench"].ToString();
            string restrictionfrench = third["restrictionfrench"].ToString();

            int lottype = Convert.ToInt32(third["Lottype"].ToString());
            string otherdetails = third["otherdetails"].ToString();

            Hashtable four = (Hashtable)Session["hasht4"];
            int billingentry = Convert.ToInt32(four["Billingentry"].ToString());
            int billingexit = Convert.ToInt32(four["Billingexit"].ToString());
            int paymentrem = Convert.ToInt32(four["paymentreminder"].ToString());
            decimal interestrat = Convert.ToDecimal(four["interestrate"].ToString());
            decimal Latefee = Convert.ToDecimal(four["Latefee"].ToString());
            decimal activationcharge = Convert.ToDecimal(four["activationcharge"].ToString());
            string displayactivationcharge = four["displayactivationcharge"].ToString();
            string displaydeposite = four["displaydeposite"].ToString();

            int latepaymentaction = Convert.ToInt32(four["latepaymentaction"].ToString());
            int creditcardpaymentaction = Convert.ToInt32(four["creditcardpaymentaction"].ToString());


            string tranasactiontype_reserved = four["tranasactiontype_reserved"].ToString();
            int reserved_totalspace = Convert.ToInt32(four["reserved_totalspace"].ToString());
            int reserved_unablespace = Convert.ToInt32(four["reserved_unablespace"].ToString());
            string tranasactiontype_random = four["tranasactiontype_random"].ToString();
            int random_totalspace = Convert.ToInt32(four["random_totalspace"].ToString());
            int random_unablespace = Convert.ToInt32(four["random_unablespace"].ToString());
            string LotTransaction_Monthly1 = four["LotTransaction_Monthly"].ToString();
            string LotTransaction_Daily1 = four["LotTransaction_Daily"].ToString();
       
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("siteid", Siteid);
            hst.Add("campusid", campusid);

            hst.Add("lotname", lotname);
            hst.Add("lotcode", lotcode);
            hst.Add("effectvdate ", effectivedate);
            hst.Add("apprvl", approv);
            hst.Add("otherdetail", otherdetails);
            hst.Add("address", address);
            hst.Add("city", city);
            hst.Add("zip", zip);
            hst.Add("lot_latitude", lat);
            hst.Add("lot_longitude", longi);
            hst.Add("lot_cell", cellular);

            hst.Add("lot_office", office);
            hst.Add("lot_email", email);
            hst.Add("lottimezone", timezone);
            hst.Add("lothours", lothours);
            hst.Add("totalspaces", totalspace);
            hst.Add("unablespace", unabelspace);
            hst.Add("englishentrygateinformation", entryenglish);
            hst.Add("englishexitgateinformation", exitenglish);
            hst.Add("englishdescription", desceng);
            hst.Add("englishrestriction", restrictionenglish);
            hst.Add("frenchentrygateinformation", entryfrench);
            hst.Add("frenchexitgateinformation", exitfrench);
            hst.Add("frenchrestriction", restrictionfrench);
            hst.Add("frenchdescription", descfrnch);
            hst.Add("lottype", lottype);
            hst.Add("billingentry", billingentry);
            hst.Add("billingexit", billingexit);
            hst.Add("paymentrem", paymentrem);
            hst.Add("intrestrate", interestrat);
            hst.Add("latefees", Latefee);
                hst.Add("activationcharge", activationcharge);
                hst.Add("displayactivationcharge", displayactivationcharge);
                hst.Add("displaydeposite", displaydeposite);
                hst.Add("latepaymentaction", latepaymentaction);
                hst.Add("creditpaymentaction", creditcardpaymentaction);
                hst.Add("lottansaction_daily", LotTransaction_Daily1);
                hst.Add("lottransaction_monthly", LotTransaction_Monthly1);
               
             hst.Add("random_parking", tranasactiontype_random);
             hst.Add("random_parking_totalspace", random_totalspace);
             hst.Add("random_parking_unablespace", random_unablespace);
             hst.Add("reserved_parking", tranasactiontype_reserved);

             hst.Add("reserved_totalspace", reserved_totalspace);
             hst.Add("reserved_unablespace", reserved_unablespace);


             hst.Add("modifiedby", "lovey_modifier");
             hst.Add("operatorid", "lovey_operator");

             hst.Add("dateofchanging", DateTime.Now);
             hst.Add("lotstaus", lotstatus);



           


            int result = objData.ExecuteNonQuery("[sp_LotMaster]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }

        }
        catch
        {

        }

    }

   
    protected void btn_FINISH_FINAL_Click(object sender, EventArgs e)
    {
        //if (chbk_Finish.Checked == true)
        //{
            try
            {
                insertrec();

                clsInsert cs = new clsInsert();
                DataSet ds1 = cs.select_operation("select Lot_id from tbl_LotMaster where LotName='" + txtbox_lotname.Text + "'order by  Lot_id desc");
                // int lastid= ds1.table[0].rows[0][0].tostring();
                int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());

                try
                {
                    DataTable reservedparkingtype = (DataTable)Session["datatable_reserved"];

                    for (int i = 0; i < reservedparkingtype.Rows.Count; i++)
                    {

                        hst.Clear();

                        hst.Add("action","insert");
                        hst.Add("lotid", lastid);
                        hst.Add("parkingtype", Convert.ToInt32(reservedparkingtype.Rows[i]["typeofparking"].ToString()));
                        hst.Add("amount", Convert.ToDecimal(reservedparkingtype.Rows[i]["Amount_reserved"].ToString()));
                        hst.Add("quantity", Convert.ToDecimal(reservedparkingtype.Rows[i]["quantity_reserved"].ToString()));
                        hst.Add("chargeby", Convert.ToInt32(reservedparkingtype.Rows[i]["chargeby_reserved"].ToString()));

                        int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
                        if (result > 0)
                        {

                            //Response.Redirect("UserManager.aspx");

                        }
                        else
                        {
                            // lbl_error.Visible = true;

                        }
                    }

                }
                catch
                {
                }


                try
                {
                    DataTable randomparkingtype = (DataTable)Session["datatable_random"];

                    for (int i = 0; i < randomparkingtype.Rows.Count; i++)
                    {

                        hst.Clear();

                        hst.Add("action","insert");
                        hst.Add("lotid", lastid);
                        hst.Add("parkingtype", Convert.ToInt32(randomparkingtype.Rows[i]["typeofparking"].ToString()));
                        hst.Add("amount", Convert.ToDecimal(randomparkingtype.Rows[i]["Amount_random"].ToString()));
                        hst.Add("quantity", Convert.ToDecimal(randomparkingtype.Rows[i]["quantity_random"].ToString()));
                        hst.Add("chargeby", Convert.ToInt32(randomparkingtype.Rows[i]["chargeby_random"].ToString()));

                        int result = objData.ExecuteNonQuery("[sp_LotRate]", CommandType.StoredProcedure, hst);
                        if (result > 0)
                        {

                            //Response.Redirect("UserManager.aspx");

                        }
                        else
                        {
                            // lbl_error.Visible = true;

                        }
                    }

                }
                catch
                {
                }


                try
                {


                    DataTable datatable = (DataTable)Session["datatable"];

                    for (int i = 0; i < datatable.Rows.Count; i++)
                    {

                        hst.Clear();

                        hst.Add("action", "insert");
                        hst.Add("lot_id", lastid);
                        hst.Add("taxname", datatable.Rows[i]["taxname"].ToString());
                        hst.Add("taxrate", Convert.ToDecimal(datatable.Rows[i]["tax"].ToString()));
                        hst.Add("seqofcal", Convert.ToInt32(datatable.Rows[i]["sequence"].ToString()));


                        int result = objData.ExecuteNonQuery("[sp_LotTax]", CommandType.StoredProcedure, hst);
                        if (result > 0)
                        {

                            //Response.Redirect("UserManager.aspx");

                        }
                        else
                        {
                            // lbl_error.Visible = true;

                        }
                    }


                }
                catch
                {
                }

                Session["datatable"] = null;
                Session["datatable_random"] = null;
                Session["datatable_reserved"] = null;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);

                
                
            }
        
            catch
            {

            }

        
            
        }


    protected void txtbox_unablespace_TextChanged(object sender, EventArgs e)
    {
            }
}
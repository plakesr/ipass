﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_operator_Profile.ascx.cs" Inherits="WUC_wuc_operator_Profile" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
   
                    </cc1:ToolkitScriptManager>
                    <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style3
    {
        width: 101px;
    }
      .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
                        .style4
                        {
                            width: 381px;
                        }
                         .registration-area .Clicked{ text-decoration: none;
   border-radius:5px 5px 0 0;

  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; padding:8px 20px 7px;}
  
  .registration-area .Initial{text-decoration: none;
   border-radius:5px 5px 0 0;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #F9F9F9;
  color:#707070; padding:8px 20px; margin:0px;}
                    </style>

<table width="80%" align="center" class="registration-area">
      <tr>
        <td>
          <asp:Button Text="Details" BorderStyle="None" ID="Tab1" 
                CssClass="Initial" runat="server"
              OnClick="Tab1_Click" />
          <asp:Button Text="Authenticated Lots" BorderStyle="None" 
                ID="Tab2" CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Menu Access" BorderStyle="None" ID="Tab3" 
                CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
          <asp:MultiView ID="MainView" runat="server">
            <asp:View ID="View1" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                <tr>
                  <td>
                    <h3>
                      <span>

    
        <table  class="form-table">
        

            <tr>
                <td class="style3">
                    Name</td>
                <td>
                    <asp:TextBox ID="txtbox_Name" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ownername" runat="server" 
                        ControlToValidate="txtbox_Name" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    Adress1
                </td>
                <td>
                    <asp:TextBox ID="txtbox_adress" runat="server" ValidationGroup="a" TextMode="MultiLine" CssClass="twitterStyleTextbox" style="resize:none"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_owneradress1" runat="server" 
                        ControlToValidate="txtbox_adress" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style4" rowspan="5" valign="top">
                                
                   </td>
            </tr>
            <tr>
                <td class="style3">
                    DOB</td>
                <td>
                    <asp:TextBox ID="txtbox_dob" runat="server" CssClass=twitterStyleTextbox  ></asp:TextBox>
                   
                    
                    <cc1:CalendarExtender ID="txtbox_owneradress2_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtbox_dob"></cc1:CalendarExtender>
                   
                </td>
            </tr>
            <tr>
                <td class="style3">
                    Cellular
                </td>
                <td>
                    <asp:TextBox ID="txtbox_Cellular" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_OCellular" runat="server" 
                        ControlToValidate="txtbox_Cellular" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td class="style3">
                    City</td>
                <td>
                    <asp:TextBox ID="txtbox_city" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ownercity" runat="server" 
                        ControlToValidate="txtbox_city" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                   Email</td>
                <td>
                    <asp:TextBox ID="txtbox_email" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_Oemail" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="style3">
                    Designation</td>
                <td>
                    <asp:TextBox ID="txtbox_designation" runat="server" ValidationGroup="a" 
                        CssClass=twitterStyleTextbox Enabled="False"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ownerzip" runat="server" 
                        ControlToValidate="txtbox_designation" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td class="style4">
                    &nbsp;</td>
            </tr>

            <tr>
                <td class="style3">
                    Gender</td>
                <td>
                     <asp:RadioButton ID="rbMale" runat="server" GroupName="gender" Checked="true" />Male
                    <asp:RadioButton ID="rbFemale" runat="server"  GroupName="gender" />Female
                </td>
                <td class="style4">
                    &nbsp;</td>
            </tr>

            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtbox_ownerzip" ErrorMessage="*Please Enter the Zipcode" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>--%>
                </td>
                <td class="style4">
                    &nbsp;</td>
            </tr>
        </table><asp:Button ID="btn_save" runat="server" Text="Update" ValidationGroup="a" CssClass=button 
    onclick="btn_save_Click" Height="39px" />
     
<asp:Button ID="btn1_next" runat="server" Text="Next" onclick="btn1_next_Click" 
            ValidationGroup="a" CssClass=button></asp:Button>
   
    


                      </span>
                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View2" runat="server">
              <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                <tr>
                  <td>
                      <asp:Panel ID="Panel1" runat="server">
                          <fieldset>
                              <legend>Autentication Lots For You</legend>
                              <asp:Repeater ID="Repeater3" runat="server" DataSourceID="SqlDataSource1" 
                                  onitemcommand="Repeater3_ItemCommand">
                                  <HeaderTemplate>
                                      <table cellpadding="5" cellspacing="0" class="timezone-table" width="100%">
                                          <tr>
                                          
                                              <th>
                                                  Lot Name
                                              </th>
                                          </tr>
                                      </table>
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                   <table cellpadding="5" cellspacing="0" class="timezone-table" width="100%">
                                      <tr>
                                        
                                          <td>
                                              <%#Eval("LotName")%>
                                          </td>
                                      </tr>
                                      </table>
                                  </ItemTemplate>
                              </asp:Repeater>
                              <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                  ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                  SelectCommand="SELECT tbl_UserAuthenticatedlot.*,tbl_LotMaster.* FROM tbl_UserAuthenticatedlot INNER JOIN tbl_LotMaster ON tbl_UserAuthenticatedlot.lotid=tbl_LotMaster.Lot_id  WHERE ([username] = @username)">
                                  <SelectParameters>
                                      <asp:SessionParameter Name="username" SessionField="Login" />
                                  </SelectParameters>
                              </asp:SqlDataSource>
                          </fieldset>
                      </asp:Panel>
                      </td>
                      </tr>
                      </table>
                      <br />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <asp:Button ID="btn2_next" runat="server" CssClass="button" 
                          onclick="btn2_next_Click" Text="Next" ValidationGroup="b" />
                      &nbsp;&nbsp;&nbsp;
                      <asp:Button ID="btn_pre" runat="server" CssClass="button" 
                          onclick="btn_pre_Click" Text="Previous" />
                      <%--                   <table class="style1">
            <tr>
                <td class="style3">
                    UserName</td>
                <td>
                    <asp:TextBox ID="txtbox_username" runat="server" ValidationGroup="b" Enabled="false" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_username" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="b"></asp:RequiredFieldValidator>
                </td>
            </tr>
            
            <tr>
                <td class="style3">
                     Password</td>
                <td>
                    <asp:TextBox ID="txtbox_pass" runat="server" ValidationGroup="b" CssClass=twitterStyleTextbox></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtbox_pass" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="b"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                     &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_ownerzip" ErrorMessage="*Please Enter the Zipcode" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>--%><%-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn2_next" runat="server" Text="Next" onclick="btn2_next_Click" CssClass=button 
            ValidationGroup="b"></asp:Button>--%>
                      <h3>
                      </h3>
                      <h3>
                      </h3>
                 </asp:View>
            <asp:View ID="View3" runat="server">
              
                   <asp:Panel ID="Panel3" runat="server">
                            <fieldset>
                                <legend>Menu To access </legend>
                                <asp:Repeater ID="Repeater2" runat="server"
                                    onitemcommand="Repeater2_ItemCommand">
                                    <HeaderTemplate>
                                        <table cellpadding="5" cellspacing="0" class="timezone-table" width="100%">
                                            <tr>
                                                
                                                <th>
                                                    Menu Name
                                                </th>
                                                <th>
                                                    Add
                                                </th>
                                                <th>
                                                    Edit
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                                <th>
                                                    Read
                                                </th>
                                                <th>
                                                    Full
                                                </th>
                                            </tr>
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            
                                          
                                            <td>
                                            <%#Eval("menu")%>
                                            </td>
                                           <td>
                                                <asp:CheckBox ID="chbkadd" runat="server"  Checked='<%#Eval("Addpermssn")%>'  Enabled="False" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkedit" runat="server" Checked='<%#Eval("Editpermssn")%>'  Enabled="False" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkdelete" runat="server" Checked='<%#Eval("Deletepermssn")%>'  Enabled="False" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkread" runat="server" Checked='<%#Eval("Readpermssn")%>'  Enabled="False" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbkfull" runat="server" Checked='<%#Eval("Fullpermssn")%>'  Enabled="False" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                     </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                               <%-- <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                                    SelectCommand="SELECT * FROM [tbl_menus]"></asp:SqlDataSource>--%>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <%--  <table>
    <tr>
                <td class="style3">
                    Menu</td>
                <td>
                   <asp:CheckBoxList ID="chkmenu" runat="server">
</asp:CheckBoxList>
        
                </td>
                
            </tr>

            <tr>
            <td>
           
        <asp:Button ID="btn_save" runat="server" Text="Submit" ValidationGroup="a" 
    onclick="btn_save_Click"  CssClass=button/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_pre1" runat="server" Text="Previous" ValidationGroup="b" onclick="btn_pre1_Click" CssClass=button
                    />
        
            </td>
            </tr>
            
    </table>--%>
                            </fieldset>
                        </asp:Panel>




                      
    <%-- <panel>
    <fieldset>
    <legend>
        Menu To access 
    </legend>
    <table>
    <tr>
                <td class="style3">
                    Menu</td>
                <td>
                   <asp:CheckBoxList ID="chkmenu" runat="server">
</asp:CheckBoxList>--%>
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_ownerzip" ErrorMessage="*Please Enter the Zipcode" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>--%>
            <%--    </td>
            </tr>
            <tr>
            <td>--%>
           
 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" ValidationGroup="b" CssClass=button 
                       onclick="btn_cancel_Click"  />
<%--    <br />
            </td>
            </tr>
            
    </table>
   --%>
        



                    </h3>
                  </td>
                </tr>
              </table>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>

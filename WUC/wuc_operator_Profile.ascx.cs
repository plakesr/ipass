﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_operator_Profile : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    string stat;
    string userstat;
    string gender;
    string str;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if(!IsPostBack==true)
        {
           
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
            string username;
            username = Session["Login"].ToString();
            clsInsert obj1 = new clsInsert();
            DataTable lr = obj1.OperatorProfile(username);
            for (int j = 0; j < lr.Rows.Count; j++)
            {
                txtbox_Name.Text = lr.Rows[j][2].ToString();
                txtbox_adress.Text = lr.Rows[j][7].ToString();
                txtbox_dob.Text = lr.Rows[j][3].ToString();
                txtbox_Cellular.Text = lr.Rows[j][5].ToString();
                txtbox_city.Text = lr.Rows[j][10].ToString();
                txtbox_email.Text = lr.Rows[j][4].ToString();
                txtbox_designation.Text = lr.Rows[j][8].ToString();
            }
            DataTable dt3 = obj1.authenticatedlots(username);
            for (int r = 0; r < dt3.Rows.Count; r++)
            {
                //if (dt.Rows[r]["Menuname"].ToString() == "")
                //{
                //    CheckBox chkselect = Repeater2.Items[r].FindControl("chbkselect") as CheckBox;
                //    CheckBox chkadd = Repeater2.Items[r].FindControl("chbkadd") as CheckBox;
                //    CheckBox chkedit = Repeater2.Items[r].FindControl("chbkedit") as CheckBox;
                //    CheckBox chkread = Repeater2.Items[r].FindControl("chbkread") as CheckBox;
                //    CheckBox chkdelete = Repeater2.Items[r].FindControl("chbkdelete") as CheckBox;
                //    CheckBox chkfull = Repeater2.Items[r].FindControl("chbkfull") as CheckBox;
                //    chkselect.Checked = true;
                //    chkadd.Checked = true;
                //    chkedit.Checked = true;
                //    chkread.Checked = true;
                //    chkdelete.Checked = true;
                //    chkfull.Checked = true;


                //}
                for (int j = 0; j <= r; j++)
                {
                    Repeater2.DataSource = dt3;
                    Repeater2.DataBind();
                }
            }
        }
    }
    protected void btn1_next_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";

        MainView.ActiveViewIndex = 1;
    }

    protected void txtbox_email_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Tab1_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void Tab2_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
    protected void btn2_next_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;

    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
           
            
            if (rbMale.Checked == true)
            {
                gender = "male";
            }
            if (rbFemale.Checked == true)
            {
                gender = "Female";
            }
            UpdateUser();
            Response.Redirect("Operator_deshboard.aspx");
        }
        catch
        {
        }
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Operator_deshboard.aspx");
    }
    protected void btn_pre_Click(object sender, EventArgs e)
    {

        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;

    }
    protected void Tab3_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;
    }


    public void UpdateUser()
    {
        try
        {
            hst.Clear();

            string user = Session["Login"].ToString();
            hst.Add("action", "updatebyoperator");
            hst.Add("user", user);

            hst.Add("name", txtbox_Name.Text);
            hst.Add("dob", Convert.ToDateTime(txtbox_dob.Text));
            hst.Add("email ", txtbox_email.Text);
            hst.Add("city", txtbox_city.Text);
            hst.Add("mob", txtbox_Cellular.Text);
            hst.Add("gender", gender.ToString());
            hst.Add("address", txtbox_adress.Text);
            hst.Add("operator_id", "Self");
            hst.Add("modifiedby", "Self");
            hst.Add("dateofchanging", DateTime.Now);
            
           
            int result = objData.ExecuteNonQuery("[sp_User]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                // Response.Redirect("Admin_UserPermissionMaster.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }
    protected void Repeater3_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
}
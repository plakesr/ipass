﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_parkersAddbyCorporate : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable("check");
            dt.Columns.Add("a", typeof(string));

            for (int i = 0; i < int.Parse(txtbox_number.Text); i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = "";
                dt.Rows.Add(dr);
            }
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Enter Number First!')", true);
        }
    }
    int LOTIDForInserting;
    int Parkingtype;

    protected void btn_finish_Click(object sender, EventArgs e)
    {
        try
        {
            insert_parkerbycorporate();
            string msg = "recordsubmited";
            Response.Redirect("Deshboard.aspx?msg=" + msg);

            //Session["plantable"] = null;
            //Session["datatableforvehicle"] = null;
            Session["recordadd"] = "true";
        }
        catch
        {

        }
    }
    
    public void insert_parkerbycorporate()
    {
        try
        {
            for (int i = 0; i < Repeater1.Items.Count; i++)
            {
                TextBox firstname = Repeater1.Items[i].FindControl("txtbox_firstname") as TextBox;
                TextBox lastname = Repeater1.Items[i].FindControl("txtbox_lastname") as TextBox;
                //TextBox username = Repeater1.Items[i].FindControl("txtbox_username") as TextBox;
                TextBox date = Repeater1.Items[i].FindControl("txtbox_activationdate") as TextBox;
                string date2 = date.Text;
                DateTime date1 = Convert.ToDateTime(date2);
                try
                {
                    LOTIDForInserting = Convert.ToInt32(Session["lotid"].ToString());
                    Parkingtype = Convert.ToInt32(Session["ParkingType"].ToString());
                }
                catch
                {
                    Response.Redirect("Default.aspx");
                }
                string account_type = "Corporate_Parker";

                string address1 = Session["AdressLine1"].ToString();
                string adress2 = Session["AdressLine2"].ToString();

                string cardnumber = Session["CardNo"].ToString();
                int billingtype = Convert.ToInt32(Session["Billingwith"].ToString());

                string postal_parker = Session["Zip"].ToString();
                string country_parker = Session["Country"].ToString();
                string state_parker = Session["State"].ToString();
                string city_parker = Session["City"].ToString();
                string cellular_parker = Session["CellNo"].ToString();
                string phone_parker = Session["Phoneno"].ToString();
                string fax_parker = Session["Fax"].ToString();
                string email_parker = Session["Email"].ToString();
                string loginusername_parker = "Under Corporate Parker Account";

                int carddeliveryid = 0;

                try
                {
                    hst.Clear();


                    hst.Add("action", "adingparkerbycorporate");
                    hst.Add("Lotid", LOTIDForInserting);
                    hst.Add("parkingtype", Parkingtype);


                    hst.Add("accounttype", account_type);
                    hst.Add("UserName", loginusername_parker);
                    hst.Add("CardNo ", cardnumber);
                    hst.Add("firstname", firstname.Text);
                    hst.Add("lastname", lastname.Text);
                    hst.Add("AdressLine1", address1);
                    hst.Add("AddressLine2", adress2);
                    hst.Add("Zip_Parker", postal_parker);
                    hst.Add("Country_parker", country_parker);
                    hst.Add("State_Parker", state_parker);
                    hst.Add("city_parker", city_parker);

                    hst.Add("cell_parker", cellular_parker);
                    hst.Add("phone_parker", phone_parker);
                    hst.Add("fax", fax_parker);
                    hst.Add("email", email_parker);
                    hst.Add("billing", billingtype);
                 
                    hst.Add("activationdate", date1);
                    hst.Add("carddeliveryid", carddeliveryid);



                    DataTable plan = (DataTable)Session["plantable"];
                    for (int k = 0; k < plan.Rows.Count; k++)
                    {


                        if (int.Parse(plan.Rows[k]["Username"].ToString()) == i)
                        {

                            hst.Add("planrateid", plan.Rows[k]["Planrate"].ToString());
                            int result = objData.ExecuteNonQuery("[sp_Parker_Corporate]", CommandType.StoredProcedure, hst);
                            if (result > 0)
                            {

                                clsInsert cs = new clsInsert();
                                DataSet ds1 = cs.select_operation("select Parker_id from tbl_ParkerRegis where UserName='" + loginusername_parker + "'order by  Parker_id desc");
                                int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
                                try
                                {
                                    hst.Clear();


                                    hst.Add("action", "adingparkerbycorporateid");
                                    hst.Add("parkerparentid", Convert.ToInt64(Session["parkerparentid_corporate"].ToString()));
                                    hst.Add("parkerchildid", lastid);
                                    hst.Add("UserName", firstname.Text);

                                    int result1 = objData.ExecuteNonQuery("[sp_Parker_Corporate]", CommandType.StoredProcedure, hst);
                                    if (result1 > 0)
                                    {
                                    }
                                }
                                catch
                                {
                                }




                                DataTable dt = (DataTable)Session["datatableforvehicle"];

                                try
                                {



                                    for (int j = 0; j < dt.Rows.Count; j++)
                                    {



                                        DataSet ds2 = cs.select_operation("select Parker_id,UserName from tbl_ParkerRegis  order by  Parker_id desc");
                                        int lastid1 = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
                                        hst.Clear();
                                        hst.Add("action", "insert");

                                        hst.Add("parkeridforvehicle", lastid1);

                                        hst.Add("VehicelUserName", ds2.Tables[0].Rows[0]["Username"].ToString());
                                        hst.Add("Make", dt.Rows[j]["VehicalMake"].ToString());
                                        hst.Add("model", dt.Rows[j]["VehicalModel"].ToString());
                                        hst.Add("year", dt.Rows[j]["VehicalYear"].ToString());
                                        hst.Add("color", dt.Rows[j]["VehicalColor"].ToString());
                                        hst.Add("licenseno", dt.Rows[j]["VehicalLicensePlate"].ToString());

                                        if (int.Parse(dt.Rows[j]["Username"].ToString()) == i)
                                        {


                                            int vehicleresult = objData.ExecuteNonQuery("[sp_VehicleParker_byCoporate]", CommandType.StoredProcedure, hst);
                                            if (vehicleresult > 0)
                                            {



                                            }
                                            else
                                            {
                                                // lbl_error.Visible = true;

                                            }
                                        }
                                    }

                                }
                                catch
                                {


                                }




                            }

                        }
                        else
                        {
                            // lbl_error.Visible = true;

                        }
                    }
                }
                catch
                {

                }





            }
        }
        catch
        {

        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdAddvehicle")
        {
            //int ID = Convert.ToInt32(e.CommandArgument.ToString());
            TextBox username = Repeater1.Items[i].FindControl("txtbox_username") as TextBox;
            Session["usernameforvehicle"] = e.Item.ItemIndex.ToString ();
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('AddCorporate_vehicleparker.aspx','New Windows','height=380, width=300,location=no');", true);
           
        }

        if (e.CommandName == "cmdAddplan")
        {
            Session["usernameforplan"] = e.Item.ItemIndex.ToString();

          

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Corporate_plan.aspx','New Windows','height=380, width=300,location=center');", true);

            //Label l = rpt1.Items[i].FindControl("idLabel") as Label;


            //Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
            //Label l3 = rpt1.Items[i].FindControl("idlabel3") as Label;

            //Session["countryname"] = l.Text;

            //Session["countryabbr"] = l2.Text;

            //Session["Country_flag"] = l3.Text;

            //int id = Convert.ToInt32(e.CommandArgument.ToString());
            //Session["id"] = id;
            //Response.Redirect("Admin_EditCountryMaster.aspx");

        }
    }
}
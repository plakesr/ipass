﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_planmaster.ascx.cs" Inherits="WUC_wuc_planmaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">

    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
    resize:none;
    width:222px;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }


    .style2
    {
        width: 98px;
    }
    .style3
    {
        width: 382px;
    }
      .watermark
  { resize:none;  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
    font-family:Verdana;font-size:13px;
}
</style>

<%--<asp:Panel ID="Panel1" runat="server" >
<fieldset>
<legend> Plan Details  </legend>
<tableclass="form-table" width="60%">
    <tr>
        <td>
            Plan Name</td>
        <td>
            <asp:TextBox ID="txtbox_planname" runat="server" ValidationGroup="a"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_planname" runat="server" 
                ControlToValidate="txtbox_planname" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <br />
    <tr>
        <td>
            Plan Description</td>
        <td>
            <asp:TextBox ID="txtbox_plandesc" runat="server" TextMode="MultiLine" 
                ValidationGroup="a"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                ControlToValidate="txtbox_plandesc" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <br />
    <tr>
        <td>
            Vehicle Details</td>
        <td>
           <asp:CheckBox ID="chbk_state" runat="server" 
                oncheckedchanged="chbk_state_CheckedChanged" AutoPostBack="true" />
        </td>
    </tr>
</table>
</fieldset>
</asp:Panel>--%>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server" >
<fieldset>
<legend> Plan Details  </legend>
<table class="form-table">
    <tr><td>&nbsp;</td></tr>

    <tr>
        <td valign="top">
            Plan Name</td>
        <td>
            <asp:TextBox ID="txtbox_planname" runat="server" ValidationGroup="a" 
                CssClass="twitterStyleTextbox" Height="23px"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_planname_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_planname" WatermarkCssClass="watermark" WatermarkText="Enter Plan Name">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_planname" runat="server" 
                ControlToValidate="txtbox_planname" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td valign="top">
            Plan Description</td>
        <td>
            <asp:TextBox ID="txtbox_plandesc" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"
                ValidationGroup="a" ontextchanged="txtbox_plandesc_TextChanged"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtbox_plandesc_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txtbox_plandesc" WatermarkCssClass="watermark" WatermarkText="Enter Description">
            </cc1:TextBoxWatermarkExtender>
            <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                ControlToValidate="txtbox_plandesc" ErrorMessage="*" ForeColor="Red" 
                ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>

    <tr>
        <td valign="top">
            State</td>
        <td>
           <asp:CheckBox ID="chbk_state" runat="server" 
                oncheckedchanged="chbk_state_CheckedChanged" AutoPostBack="true" />
        </td>
    </tr>
</table>
</fieldset>
</asp:Panel>
 <br />
<asp:Panel ID="Panel2" runat="server" Visible="false" Height="300px" ScrollBars="Vertical">
<fieldset>

<legend> Vehicle Details  </legend>
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1" 
        onitemcommand="Repeater1_ItemCommand">

    <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="10">
                <tr>
                      <th>
                        Vehicle Type
                    </th>
                     <th>
                       Rate
                    </th>
                                 <th>
                        Plan Type
                    </th>
                   
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text='<%#Eval("v_id")%>' Visible="False"></asp:Label>     <%#Eval("vehicle_type")%>
                    </td><td>
                        <asp:TextBox ID="txtbox_rateforvehicle" runat="server" CssClass=twitterStyleTextbox></asp:TextBox>
                </td>
                
             
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass=twitterStyleTextbox>
                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                        <asp:ListItem Value="2">Daywise</asp:ListItem>
                        <asp:ListItem Value="3">Quarterly</asp:ListItem>
                        <asp:ListItem Value="4">HalfYearly</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                   <%-- <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Country_name") %>' 
                                        Visible="False" />
                                    <asp:Label ID="idlabel1" runat="server" Text='<%# Eval("Country_code") %>' 
                                        Visible="False"></asp:Label>
                    <asp:Label ID="idlabel2" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>
                                        <asp:Label ID="idlabel3" runat="server" Text='<%# Eval("Country_Abbreviation") %>' 
                                        Visible="False"></asp:Label>--%>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        SelectCommand="SELECT * FROM [tbl_Vehicle_Type]"></asp:SqlDataSource>
</fieldset>
</asp:Panel>

<asp:Panel ID="Panel3" runat="server" Visible="true">

<fieldset>
<legend> Money Details  </legend>
<table class="style1 form-table">
  <tr><td>&nbsp;</td></tr>
    <tr>
        <td>
           Rate for Normal</td>
        <td>
            <asp:TextBox ID="txbox_ratefornormal" runat="server" 
                CssClass=twitterStyleTextbox Height="24px"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txbox_ratefornormal_TextBoxWatermarkExtender" 
                runat="server" Enabled="True" TargetControlID="txbox_ratefornormal" WatermarkCssClass="watermark" WatermarkText="Enter Rate">
            </cc1:TextBoxWatermarkExtender>
        </td>
    </tr>
    <tr>
        <td>
            Plan type</td>
        <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        CssClass=twitterStyleTextbox Width="163px">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                        <asp:ListItem Value="2">Daywise</asp:ListItem>
                        <asp:ListItem Value="3">Quarterly</asp:ListItem>
                        <asp:ListItem Value="4">HalfYearly</asp:ListItem>
                    </asp:DropDownList>
                    <cc1:DropDownExtender ID="ddl_country_DropDownExtender" runat="server" 
            DynamicServicePath="" Enabled="True" TargetControlID="DropDownList1">
        </cc1:DropDownExtender>
                    </td>
    </tr>

    
</table>
</fieldset>
</asp:Panel>

<asp:Button ID="btn_submit" runat="server" onclick="btn_submit_Click"
    Text="Add" ValidationGroup="a" CssClass="button"/>
<%--<%#Eval("vehicle_type")%>--%>



<asp:Panel ID="Panel4" runat="server" Height="300px" ScrollBars="Vertical">
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="SELECT * FROM [tbl_PlanMaster]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table  width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Plan&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Plan&nbsp;&nbsp;Desc
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("Plan_Name")%>
                    </td><td>
                    <%#Eval("PlanDescription")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("Plan_id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("Plan_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />

                    </asp:LinkButton>
                    </td>
                     <asp:Label ID="id" runat="server" Text='<%# Eval("Plan_id") %>' 
                                        Visible="False" />
                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Plan_Name") %>' 
                                        Visible="False" />
  <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("PlanDescription") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="idLabel2" runat="server" Text='<%# Eval("RateforNormal") %>' 
                                        Visible="False"></asp:Label>  
    <%--<asp:Label ID="id" runat="server" Text='<%# Eval("StateId") %>' 
                                        Visible="False"></asp:Label> --%>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>






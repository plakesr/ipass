﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_planmaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       
    }
    public void delplan(int id, int id1)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("planid", id);
            hst.Add("planvehicleid", id1);


            int result = objData.ExecuteNonQuery("[sp_Plan]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                Response.Redirect("Admin_PlanMaster.aspx");

            }
            else
            {
                Response.Redirect("Admin_PlanMaster.aspx");
            }
        }
        catch
        {
            Response.Redirect("Admin_PlanMaster.aspx");

        }
    }
    
    int vehicletype;
    string rate;
    int plantype;
    protected void btn_submit_Click(object sender, EventArgs e)
    {
       
        try
        {



            if (chbk_state.Checked == true)
            {
                // state = "yes";
                Panel2.Visible = true;
                addplan1();


                clsInsert cs = new clsInsert();
                DataSet ds1 = cs.select_operation("select Plan_id from tbl_PlanMaster where Plan_Name='" + txtbox_planname.Text + "'order by  Plan_id desc");
                // int lastid= ds1.table[0].rows[0][0].tostring();
                int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
                //use this id in Looping
                for (int i = 0; i < Repeater1.Items.Count; i++)
                {
                    TextBox t = Repeater1.Items[i].FindControl("txtbox_rateforvehicle") as TextBox;
                    DropDownList ddl = Repeater1.Items[i].FindControl("DropDownList1") as DropDownList;
                    Label l = Repeater1.Items[i].FindControl("Label1") as Label;

                    vehicletype = Convert.ToInt32(l.Text);
                    rate = t.Text;
                    plantype = Convert.ToInt32(ddl.SelectedItem.Value);
                    hst.Clear();
                    hst.Add("action", "insert");
                    hst.Add("planforvehicle", txtbox_planname.Text);
                    //use Plan ID
                    hst.Add("vehicletype", vehicletype);
                    hst.Add("rateofvehicle", rate);
                    hst.Add("plantypeforvehicle", plantype);


                    hst.Add("v_modifiedby", "lovey");
                    hst.Add("v_operator", "lovey_operator");
                    hst.Add("v_dateofchanging", DateTime.Now);
                    hst.Add("planid", lastid);


                    int result1 = objData.ExecuteNonQuery("[sp_vehicel]", CommandType.StoredProcedure, hst);
                    if (result1 > 0)
                    {

                        //Response.Redirect("Country.aspx");

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }



                }

                //Response.Redirect("Admin_PlanMaster.aspx");
            }
            else
            {
                string msg = "1";
                addplan();
                Response.Redirect("Admin_PlanMaster.aspx?message=" + msg);

                //Response.Redirect("Admin_PlanMaster.aspx");
            }

          
            clear();
        }
        catch
        {

        }
        
    }
    public void clear()
    {
        txtbox_planname.Text = "";
        txtbox_plandesc.Text = "";
        txbox_ratefornormal.Text = "";
        chbk_state.Checked = false;
        DropDownList1.SelectedIndex = 0;
        for (int i = 0; i < Repeater1.Items.Count; i++)
        {
            TextBox t = Repeater1.Items[i].FindControl("txtbox_rateforvehicle") as TextBox;
            DropDownList ddl = Repeater1.Items[i].FindControl("DropDownList1") as DropDownList;
            
            t.Text = "";
            ddl.SelectedIndex=0;

        }
    }

    public void addplan()
    {
        try
        {
            hst.Clear();
           
            hst.Add("action", "insert");
            hst.Add("planname", txtbox_planname.Text);
            hst.Add("plandesc", txtbox_plandesc.Text);

            
               // state = "";
                hst.Add("RateforNormal", txbox_ratefornormal.Text);
                hst.Add("plantype", DropDownList1.SelectedItem.Value);
                
           
            hst.Add("modifiedby", "lovey");
            hst.Add("operator", "lovey_operator");
            hst.Add("dateofchanging", DateTime.Now);



            int result = objData.ExecuteNonQuery("[sp_Plan]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("Country.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }

    public void addplan1()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("planname", txtbox_planname.Text);
            hst.Add("plandesc", txtbox_plandesc.Text);


            // state = "";
            hst.Add("RateforNormal", "");
            hst.Add("plantype", 0);


            hst.Add("modifiedby", "lovey");
            hst.Add("operator", "lovey_operator");
            hst.Add("dateofchanging", DateTime.Now);



            int result = objData.ExecuteNonQuery("[sp_Plan]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                //Response.Redirect("Country.aspx");

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }

    }

    protected void chbk_state_CheckedChanged(object sender, EventArgs e)
    {
        if (chbk_state.Checked == true)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;

        }
        else
        {
            Panel2.Visible = false;
            Panel3.Visible = true;

        }
    }
    protected void SqlDataSource2_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            Label l = rpt1.Items[i].FindControl("idLabel") as Label;

            int id1 = Convert.ToInt32(e.CommandArgument.ToString());
            // delvehicleplan(id1);
            delplan(ID, id1);

        }
        if (e.CommandName == "cmdEdit")
        {

            Label l3 = rpt1.Items[i].FindControl("idLabel") as Label;
            Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;
            Label id = rpt1.Items[i].FindControl("id") as Label;
            Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;

            Session["PlanName"] = l3.Text;
            Session["Plandescription"] = l1.Text;
            Session["Ratefornormal"] = l2.Text;
            Session["id"] = id.Text;
            Response.Redirect("Admin_EditPlanMaster.aspx");
        }


    }


    protected void txtbox_plandesc_TextChanged(object sender, EventArgs e)
    {

    }
}
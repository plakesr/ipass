﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_randomrate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["randomparkingtype"].ToString();
            try
            {

                dt_random.Columns.Add("Amount_random", typeof(decimal));
                dt_random.Columns.Add("quantity_random", typeof(decimal));
                dt_random.Columns.Add("chargeby_random", typeof(int));
                dt_random.Columns.Add("typeofparking", typeof(int));
                dt_random.Columns.Add("chargebytext_random", typeof(string));



                Session["datatable_random"] = dt_random;

            }
            catch
            {

            }
        }
    }
    DataTable dt_random = new DataTable();
    protected void btn_reservedrate_Click(object sender, EventArgs e)
    {
        try
        {

            dt_random = (DataTable)Session["datatable_random"];
            DataRow dr_random = dt_random.NewRow();
            dr_random[0] = txtbox_randomamount.Text;
            dr_random[1] = txtbox_ranomquantity.Text;
            dr_random[2] = Convert.ToInt32(ddl_random.SelectedItem.Value);
            dr_random[3] = Convert.ToInt32(Session["randomparkingtype"].ToString());
            dr_random[4] = ddl_random.SelectedItem.Text;

            dt_random.Rows.Add(dr_random);

            Session["datatable_random"] = dt_random;
            if (dt_random.Rows.Count > 0)
            {
                rpt_random.DataSource = dt_random;
                rpt_random.DataBind();
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please fill  details.')", true);
        }
    }
}
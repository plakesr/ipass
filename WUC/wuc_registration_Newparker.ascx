﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_registration_Newparker.ascx.cs" Inherits="WUC_wuc_registration_Newparker" %>
 <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </cc1:ToolkitScriptManager>
                    <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
                <style type="text/css">
                    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
                    }

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb;
                        }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }


    
                </style>
<script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />
<%--<link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />--%><%--<script src="../js/Extension.min.js" type="text/javascript"></script>--%>
  <div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
   
 <table class="registration-area" >
      <tr>
        <td>
         
          <asp:Button Text="Personal Details" BorderStyle="None" ID="btn_personaldetails" 
                CssClass="Initial" runat="server"
              OnClick="Tab2_Click" />
          <asp:Button Text="Add Vehicle Details" BorderStyle="None" 
                ID="btn_addvehicledetails" CssClass="Initial" runat="server"
              OnClick="Tab3_Click" />
          <asp:Button Text="Total Charges" BorderStyle="None" ID="btn_totalcharges" 
                CssClass="Initial" runat="server"
              OnClick="Tab4_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              
              <asp:Label ID="Label1" runat="server" Text="You Purchase The Planrate Of" 
                Font-Bold="True" ForeColor="#3333FF" ></asp:Label>
            &nbsp;&nbsp; <asp:Label ID="Label3" runat="server" Text="$"  Font-Bold="True"  ForeColor="#33CC33"></asp:Label> 
            <asp:Label ID="lbl_amount" runat="server" Text=""  Font-Bold="True"  ForeColor="#3333FF" ></asp:Label>
            

            
          <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
        
            <asp:View ID="View2" runat="server">
              <table class="reg-form">
                <tr>
                  <td>
             
                      
  

<asp:Panel ID="Panel2" runat="server">
<fieldset>
<legend>Personal Information</legend>
   
    <table cellpadding="0" cellspacing="0" class="form-area">
         
        <caption>
            <br />
            <br />
            <tr>
                <td>First Name</td>
                <td ><asp:TextBox ID="txtBox_firstname" runat="server" 
                        CssClass="twitterStyleTextbox" placeholder="Enter the First Name" 
                        ValidationGroup="a" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_firstname" runat="server" 
                        ControlToValidate="txtBox_firstname" ErrorMessage="Enter First Name" 
                        ForeColor="#CC0000" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
                <td width="10%">
                </td>
                <td width="15%">Last Name</td>
                <td width="30%">
                    <asp:TextBox ID="txtbox_lastname" runat="server" CssClass="twitterStyleTextbox" 
                        placeholder="Enter the Last Name" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_lastname" runat="server" 
                        ControlToValidate="txtbox_lastname" ErrorMessage="Enter Last Name" 
                        ForeColor="#CC0000" ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><span ID="Grp_AccountInformation_lblContactName">Card Number(If Issued)</span></td>
                <td><asp:TextBox ID="txtbox_coupannumber" runat="server" 
                        CssClass="twitterStyleTextbox" placeholder="Enter the Card Number"></asp:TextBox>
                </td>
                <td width="50">
                </td>
                <td width="15%" >Zip Code</td>
                <td><asp:TextBox ID="txtbox_postal" runat="server" CssClass="twitterStyleTextbox" 
                        placeholder="Enter The Zip" ValidationGroup="a" ></asp:TextBox>
                   <%-- <asp:RequiredFieldValidator ID="rfv_postal" runat="server" 
                        ControlToValidate="txtbox_postal" 
                        ErrorMessage="Enter the Postal Code/Zip Code " ForeColor="#CC0000" 
                        ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator><br />--%>
                        <asp:RegularExpressionValidator ID="rgexp_postal" runat="server" 
                 ErrorMessage="Please Enter correct format" ControlToValidate="txtbox_postal" 
                 ValidationExpression="^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$" 
                 ValidationGroup="a" ForeColor="#FF3300"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>St #</td>
                <td><asp:TextBox ID="txtbox_address1" runat="server" CssClass="twitterStyleTextbox" 
                        placeholder="Enter the Street Address" style="resize: none;" 
                        TextMode="MultiLine" ValidationGroup="a"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_address2" runat="server" 
                        ControlToValidate="txtbox_address1" ErrorMessage="Enter the address" 
                        ForeColor="#CC0000" ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
                <td width="50">
                </td>
                <td width="15%">Street&nbsp;Name</td>
                <td><asp:TextBox ID="txtbox_address2" runat="server" CssClass="twitterStyleTextbox" 
                        placeholder="Enter The Area" style="resize: none;" TextMode="MultiLine"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtbox_address2" ErrorMessage="Enter the address" 
                        ForeColor="#CC0000" ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Country</td>
                <td><asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddl_Parkercountry" runat="server" AutoPostBack="True" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource5" 
                                DataTextField="Country_name" DataValueField="Country_id">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="ddl_Parkercountry" ErrorMessage="Enter country" ForeColor="#CC0000" 
                                ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                
                                SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country] ORDER BY [Country_name]">
                            </asp:SqlDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td width="50">
                </td>
                <td width="15%">Province</td>
                <td><asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="True" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource1" 
                                DataTextField="ProvinceName" DataValueField="ProvinceId" 
                                ValidationGroup="a" onselectedindexchanged="ddlstate_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfv_state0" runat="server" 
                                ControlToValidate="ddlstate" ErrorMessage="Enter Province" ForeColor="#CC0000" 
                                ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>City</td>
                <td><asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddl_Parkercity" runat="server" 
                                CssClass="twitterStyleTextbox" DataSourceID="SqlDataSource6" 
                                DataTextField="City_name" DataValueField="City_id">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ControlToValidate="ddl_Parkercity" ErrorMessage="Enter City" ForeColor="#CC0000" 
                                ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                
                                SelectCommand="SELECT * FROM [tbl_City] WHERE ([Province_id] = @Province_id) order by City_name">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlstate" Name="Province_id" 
                                        PropertyName="SelectedValue" Type="Int64" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td width="50">
                </td>
                <td width="15%">Email Id</td>
                <td><asp:TextBox ID="txtbox_Email" runat="server"  CssClass="twitterStyleTextbox" ValidationGroup="a"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rgexp_email" runat="server" 
                        ControlToValidate="txtbox_Email" 
                        ErrorMessage="*Please Enter Email In The Correct Format" ForeColor="Red" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <br />
                            <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                                ControlToValidate="txtbox_Email" Display="Dynamic" 
                                ErrorMessage="Enter The Email ID" ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                            
                </td>
            </tr>
            <tr>
                <td>Cellular No.</td>
                <td><asp:TextBox ID="txtbox_cellular" runat="server" CssClass="twitterStyleTextbox" 
                        ValidationGroup="a">


</asp:TextBox>
                
                    <asp:RequiredFieldValidator ID="rfv_cellular" runat="server" 
                        ControlToValidate="txtbox_cellular" 
                        ErrorMessage="Enter Cellular Number" ForeColor="#CC0000" 
                        ValidationGroup="a"  Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator     ID="RegularExpressionValidator2" runat="server"  ErrorMessage="Enter Correct Format"  ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
              ControlToValidate="txtbox_cellular" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" 
                 runat="server" Enabled="True" TargetControlID="txtbox_cellular" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                </td>
                <td width="50">
                </td>
                <td width="15%">Phone No.</td>
                <td width="15%"><asp:TextBox ID="txtbox_phone" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                   <%-- <asp:RequiredFieldValidator ID="rfv_phone" runat="server" 
                        ControlToValidate="txtbox_phone" Display="Dynamic" 
                        ErrorMessage="Enter Phone Number" ForeColor="#CC0000" ValidationGroup="a" ></asp:RequiredFieldValidator><br />--%>
                        <asp:RegularExpressionValidator
                                                           ID="RegularExpressionValidator1" runat="server" 
                 ErrorMessage="Enter Correct Format" 
                 ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" 
                 ControlToValidate="txtbox_phone" ValidationGroup="b" ForeColor="#FF3300" ></asp:RegularExpressionValidator>
                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" 
                 runat="server" Enabled="True" TargetControlID="txtbox_phone" WatermarkText="Enter In Format (123)123-4567" WatermarkCssClass="watermark">
             </cc1:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr>
                <td>Fax No.</td>
                <td><asp:TextBox ID="txt_fax" runat="server" CssClass="twitterStyleTextbox" 
                        Placeholder="Enter The Fax Number"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="rfv_fax" runat="server" 
                        ControlToValidate="txt_fax" Display="Dynamic" 
                        ErrorMessage="Enter Cellular Number" ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>--%>
                </td>
                <td width="50">
                </td>
                <td><asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                        SelectCommand="SELECT * FROM [tbl_Province] WHERE ([Country_id] = @Country_id) order by ProvinceName">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddl_Parkercountry" Name="Country_id" 
                                PropertyName="SelectedValue" Type="Int64" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td><%-- <asp:DropDownList ID="ddl_deliverymethod_corporate" runat="server" 
                        CssClass="twitterStyleTextbox" Height="38px" Visible="false" Width="300px">
                    </asp:DropDownList>--%>
                      <asp:RadioButton ID="rb_account" runat="server" Text="Individual" GroupName="a" 
                     ValidationGroup="b"  AutoPostBack="true" 
                    oncheckedchanged="rb_account_CheckedChanged" Visible="false"/> &nbsp; &nbsp; &nbsp;
                <asp:RadioButton ID="rb_account1" runat="server" Text="Corporate" GroupName="a"  
                     ValidationGroup="b" oncheckedchanged="rb_account1_CheckedChanged" AutoPostBack="true"  Visible="false"/>
                </td>
            </tr>
        </caption>
    </table>
   
    </fieldset>
</asp:Panel>
<div class="clearfix">&nbsp;</div>
<asp:Panel ID="Panel3" runat="server" Height="201px">
<fieldset>
<legend>&nbsp;Login Information</legend>
    <table>
        <tr>
            <td width="27%">User Name</td>
            <td><asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                <asp:TextBox ID="txtBox_username" runat="server" ValidationGroup="a" 
                    CssClass="twitterStyleTextbox" Placeholder="Enter The User Name" ontextchanged="txtBox_username_TextChanged1" AutoPostBack="true"
                    ></asp:TextBox>
                
                <asp:RequiredFieldValidator ID="rfv_username" runat="server" 
                    ControlToValidate="txtBox_username" ErrorMessage="Enter User Name"  ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:Label ID="lbl_usercheck" runat="server" Visible="False" 
                    ForeColor="#CC0000"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td width="27%">Password</td>
            <td><asp:TextBox ID="txtBox_Passwrd" runat="server" ValidationGroup="a" TextMode="Password" CssClass="twitterStyleTextbox" Placeholder="Enter The Password"></asp:TextBox>
                  <cc1:PasswordStrength ID="txtBox_Passwrd_PasswordStrength" runat="server" 
                    Enabled="True" MinimumUpperCaseCharacters="1" PreferredPasswordLength="10" 
                    StrengthIndicatorType="BarIndicator" TargetControlID="txtBox_Passwrd"  TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"></cc1:PasswordStrength>
                  <asp:RequiredFieldValidator ID="rfv_password" runat="server" 
                    ControlToValidate="txtBox_Passwrd" ErrorMessage="Enter the password" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="27%">Confirm Password</td>
            <td><asp:TextBox ID="txtBox_cnfrmpasswrd" runat="server" ValidationGroup="a" TextMode="Password" CssClass="twitterStyleTextbox" Placeholder="Retype the Password"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfv_confirmpassword" runat="server" 
                    ControlToValidate="txtBox_cnfrmpasswrd" ErrorMessage="Enter Confirm Password" ForeColor="#CC0000" 
                    ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cmp_confirmpwd" runat="server" 
                    ControlToCompare="txtBox_Passwrd" ControlToValidate="txtBox_cnfrmpasswrd" 
                    ErrorMessage="Password Does Not Match" ForeColor="#CC0000" ValidationGroup="a"></asp:CompareValidator>
                    
                &nbsp;&nbsp;
            </td>
        </tr>
    </table>
    </fieldset>
</asp:Panel>



                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div>


                                    <asp:Panel ID="Panel4" runat="server">
                                        <fieldset>
                                            <legend>Billing Information</legend>
                                            <br />
                                          
                                          
                                                <table class="style10">
                                                    <tr>
                                                        <td class="style13">
                                                            <asp:RadioButton ID="rbtn_creditcard" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_creditcard_CheckedChanged" 
                                                                Text="Auto Charge to Credit Card " />
                                                        </td>
                                                        <td class="style12">
                                                            <asp:RadioButton ID="rbtn_invoicebymail" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_invoicebymail0_CheckedChanged" 
                                                                Text="Invoice By Email" />
                                                        </td>
                                                        <td class="style11">
                                                            <asp:RadioButton ID="rbtn_invoicebyemail" runat="server" AutoPostBack="true" 
                                                                GroupName="s" oncheckedchanged="rbtn_invoicebyemail0_CheckedChanged" 
                                                                Text="Invoice By Mail " />
                                                        </td>
                                                        <td>
                                                            <asp:RadioButton ID="rbtn_ach" runat="server" AutoPostBack="true" GroupName="s" 
                                                                oncheckedchanged="rbtn_ach_CheckedChanged" Text="Auto Charge to ACH" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style13">
                                                        <br />
                                                            <asp:Button ID="btn_entercreditcard" runat="server" CssClass="button"  
                                                                onclick="btn_entercreditcard_Click" Text="Enter Credit Card" Visible="False" />
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;</td>
                                                        <td class="style11">
                                                            &nbsp;</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                           
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <br />
                                    <br />
                                    <asp:Panel ID="Panel5" runat="server"
                                        Visible="False">
                                        <fieldset>
                                            <legend>Direct&nbsp; Debit Bank Information</legend>
                                            <br />
                                            <table class="style10">
                                                <tr>
                                                    <td width="15%">
                                                        Customer Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_customername" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        Bank Account Type</td>
                                                    <td>
                                                    
                                                        <asp:DropDownList ID="ddl_accounttype" runat="server" CssClass="twitterStyleTextbox">
                                                         
                        <asp:ListItem Value="1">Saving</asp:ListItem>
                        <asp:ListItem Value="2">Current</asp:ListItem>
                        
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Bank Name</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        Branch</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_branch" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Zip/Postal</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankzip" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td width="15%">
                                                        City</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_city_bank" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Province</td>
                                                    <td class="style15">
                                                        <asp:DropDownList ID="ddl_bankstate" runat="server" DataSourceID="SqlDataSource4" 
                                                            DataTextField="ProvinceName" DataValueField="ProvinceId" CssClass="twitterStyleTextbox" >
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                                            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                                                            SelectCommand="SELECT * FROM [tbl_Province] order by ProvinceName" ></asp:SqlDataSource>
                                                    </td>
                                                    <td width="15%">
                                                        Bank Routing No</td>
                                                    <td>
                                                        <asp:TextBox ID="txtbox_routingno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%">
                                                        Bank Account No</td>
                                                    <td class="style15">
                                                        <asp:TextBox ID="txtbox_bankaccountno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td class="style14">
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                  

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

<asp:Button ID="btn_next2" runat="server" Text="Next"  onclick="btn_next2_Click" CssClass="button"  />

<asp:Button ID="btn_finish_corporate" runat="server" Text="Finish"  CssClass="button" Visible="false" 
                          onclick="btn_finish_corporate_Click" />


                        


                      
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View3" runat="server">
              <table class="reg-form">
                <tr>
                  <td>
                    <h3>
                      <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 150px;
    }
    .style3
    {
        width: 180px;
    }
    .style4
    {
        width: 181px;
    }
</style>

<div style="font-size:13px; color:#707070; font-weight:normal;">
    <table width="80%">
        <tr>
            <td width="15%">
                First Name :</td>
            <td  align="left">
                <asp:TextBox ID="txtbox_thirdFirstname" runat="server" ReadOnly="True" CssClass="twitterStyleTextbox"  Enabled="false"></asp:TextBox>
            </td>
            <td></td>
            <td width="15%" align="left">
                Last Name:</td>
            <td align="left">
                <asp:TextBox ID="txtbox_thirdLastname" runat="server" ReadOnly="True" CssClass="twitterStyleTextbox" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td><asp:Button ID="btn_addVehicle" runat="server" Text="Add/Edit vehicle" CssClass="button" onclick="btn_addVehicle_Click" 
                     /></td>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</div>
<asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table style="font-size:13px; color:#707070; font-weight:normal;"  width="80%">
                <tr>
                    <td width="15%">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td rowspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Activation Date :</td>
                    <td>
                        <asp:TextBox ID="txtbox_ActicationDate" runat="server" CssClass="disable_past_dates"></asp:TextBox>
                        <cc1:CalendarExtender ID="txtbox_ActicationDate_CalendarExtender" 
                            runat="server" Enabled="True" TargetControlID="txtbox_ActicationDate" ></cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rfv_activation_date" runat="server" 
                            ControlToValidate="txtbox_ActicationDate" Display="Dynamic" 
                            ErrorMessage="Enter Activation Date" ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>

      
      </ContentTemplate>
    </asp:UpdatePanel>
<asp:Panel ID="Panel6" runat="server">
<fieldset>
<legend>
    Select card Delivery Method
</legend>
    <br />
    <asp:DropDownList ID="ddl_deliverymethod" runat="server" Height="38px" CssClass="twitterStyleTextbox" 
        Width="300px">
    </asp:DropDownList>
    
</fieldset></asp:Panel>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    
<asp:Button ID="btn_prev3" runat="server" Text="Previous" onclick="btn_prev3_Click" CssClass="button" /> &nbsp; &nbsp; 
 <asp:Button ID="btn_next3" runat="server" Text="Next" onclick="btn_next3_Click" CssClass="button" />

                       
                  </td>
                </tr>
              </table>
            </asp:View>
            <asp:View ID="View4" runat="server">
              <table class="reg-form">
                <tr>
                  <td>
                    <h3>
                      <style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 129px;
    }
    .style3
    {
        width: 149px;
    }
    .style4
    {
        width: 77px;
    }
    .style5
    {
        width: 151px;
    }
    .style6
    {
        width: 68px;
    }
</style>
<asp:Panel ID="Panel7" runat="server">
<fieldset>
<legend>
    Total Charges</legend>


    <table width="100%" cellpadding="10" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="lbl_activationcharge" runat="server" CssClass="normaltext" 
                    Text="Total Activation Charges"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="activationcharge" runat="server"></asp:Label>
            </td>
            <td width="200">
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_totaldeliverycharge" runat="server" CssClass="normaltext" 
                    Text="Total Delivery Charges"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lbl_deliverycharge" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="total" runat="server" CssClass="normaltext" Text="Total"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lbl_total" runat="server"></asp:Label>
                <asp:Label ID="discount" runat="server" Text="Discount" Visible="false"></asp:Label>
                <asp:Label ID="lbl_discount" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            
            <td>
                <asp:Label ID="lbl_tax" runat="server" CssClass="normaltext" 
                    Text="Tax" Visible="True"></asp:Label>
            </td>
            <td align="right">

                <asp:Label ID="lbl_taxmoney" runat="server" ></asp:Label>
                <asp:Label ID="Label2" runat="server" Text="%"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lbl_tax_money" runat="server" Visible="false"></asp:Label></td>
            <td align="right">
                &nbsp;</td>
            <td>
                &nbsp;</td>

                <td style="border-bottom: 1pt dashed black; border-top: 1pt dashed black;" >
                <asp:Label ID="lb_grandtotal" runat="server" CssClass="normaltext" 
                    Text="Grand Total" Visible="False" BorderColor="Black" Font-Bold="True" 
                        ForeColor="Black"></asp:Label>
            </td>
            <td align="right" style="border-bottom: 1pt dashed black;border-top: 1pt dashed black;">
                <asp:Label ID="lbl_granttotal" runat="server" Font-Bold="True"></asp:Label>
            </td>

        </tr>
    </table>
</fieldset>
</asp:Panel>

<br />
<asp:Panel ID="Panel8" runat="server">
<fieldset>
<legend>
    Add Old Card Detail</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="normaltext">
                Credit Reference #</td>
            <td class="style3">
                <asp:TextBox ID="txtbox_creditreference" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="rfv_creditreference" runat="server" 
                    ControlToValidate="txtbox_creditreference" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>--%>
            </td>
            <td class="normaltext">
                Old Card #</td>
            <td class="style5">
                <asp:TextBox ID="txtbox_oldcard" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td class="normaltext">
                Balance</td>
            <td class="style5">
                <asp:TextBox ID="txtbox_balance" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_Add" runat="server" Text="Add" CssClass="button" />
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>

<br />
<asp:Panel ID="Panel9" runat="server">
<fieldset>
<legend>
    Old Card And Balances</legend>
    <asp:Repeater ID="rpt_oldcard" runat="server">
    </asp:Repeater>
    <br />
</fieldset>
    
</asp:Panel>

<br />
<asp:CheckBox ID="chbk_accept" runat="server" CssClass="normaltext" 
    
    Text="I confirm I have read and accept the terms &amp; conditions of service. I also accept the non-refundable fee detailed in the registration and the billing option selected." 
    ValidationGroup="a" />
                        <asp:Label ID="lbl_error" runat="server" ForeColor="Red" 
                            Text="* Please Accept Terms &amp; condition" Visible="False"></asp:Label>
    <br />
    <a href="Terms_condition.aspx" target="_blank">View Lot Terms and Condition</a><br /><br />
                     <asp:Button ID="btn_pre4" runat="server" Text="Previous" onclick="btn_pre4_Click" CssClass="button pull-left"   />
                        
                        <asp:Button ID="btn_add1" runat="server" CssClass="button pull-right" 
                            onclick="btn_add1_Click" Text="Submit" ValidationGroup="a" />
                       <asp:validationsummary id="vsmSummary" runat="server" ForeColor="DarkRed" HeaderText="Please Fill the Mendentry fields"
			 Font-Bold="True" ValidationGroup="a" ShowMessageBox="True"></asp:validationsummary>

                        </td>
                </tr>
              </table>
            </asp:View>
          </asp:MultiView>
        </td>
      </tr>
    </table>
    </div>
    </div>
    <div class="footer-container"></div>

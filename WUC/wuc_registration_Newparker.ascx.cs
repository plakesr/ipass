﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Mail;
using System.Net;
public partial class WUC_wuc_registration_Newparker : System.Web.UI.UserControl
{

    Hashtable hst = new Hashtable();
    clsInsert con = new clsInsert();
    clsData objData = new clsData();
    string billingtypeentry = "";
    string invoicenumber = "";
    decimal monthly_totalcharge = Convert.ToDecimal("0.00");
    int dateformatch = 15;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
               

            if (Session["PlanrateID"].ToString() != "" || Session["PlanrateID"].ToString() != null)
            {
                try
                {
                    DataSet ds_tax = new clsInsert().fetchrec("select * from tbl_LotTaxMaster where Sequenceofcalc=1 and LotId=" + Convert.ToInt32(Session["LOTIDForInserting"].ToString()));
                    if (ds_tax.Tables[0].Rows.Count > 0)
                    {
                        lbl_taxmoney.Text = ds_tax.Tables[0].Rows[0]["TaxRate"].ToString() ;
                    }
                }
                catch
                {
                }
                btn_personaldetails.CssClass = "Clicked";
                lbl_amount.Text = Session["amount_parking"].ToString();
                try
                {
                if (Session["Coupancode"].ToString() != null || Session["Coupancode"].ToString() != "")
                {
                    //txtbox_coupannumber.Text = Session["Coupancode"].ToString();
                    lbl_discount.Visible = true;
                    lbl_discount.ForeColor = Color.ForestGreen;
                    discount.CssClass = "normaltext";
                    lbl_discount.Text = Session["CoupanDiscount"].ToString();
                    discount.Visible = true;
                }
                }
                catch
                {
                    txtbox_coupannumber.Text = "";
                    lbl_discount.Visible = false;
                    discount.Visible = false;
                }
                bind_DeliveryMaster();
                rb_account.Checked = true;
                rbtn_creditcard.Checked = true;
                btn_entercreditcard.Visible = true;
            }
            }
            catch
            {
               
                    Response.Redirect("LotAddresssearch.aspx");
                 
            }
        }
        if (rb_account1.Checked == true)
        {
            // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Under Construction.')", true);
          btn_finish_corporate.Visible = true;
           btn_next2.Visible = false;
           //Delivery.Visible = true;
           //ddl_deliverymethod_corporate.Visible = true;
           //lbl_corporate_activationdate.Visible = true;
           //txtbox_corporateactivationdate.Visible = true;
        }
        else
        {
           btn_next2.Visible = true;
            //btn_finish_corporate.Visible = false;
            //Delivery.Visible = false;
            //ddl_deliverymethod_corporate.Visible = false;
            //lbl_corporate_activationdate.Visible = false;
            //txtbox_corporateactivationdate.Visible = false;
        }

        

       
    }
    decimal defaultdeposit = Convert.ToDecimal(0.00);
    int transactionid = 0;



  




    protected void Tab4_Click(object sender, EventArgs e)
    {

        try
        {
            SqlDataAdapter da1 = new SqlDataAdapter("SELECT Charges FROM [tbl_CardDeliveryMaster] where CardDeliveryId=" + ddl_deliverymethod.SelectedItem.Value, con.connString());
            DataSet ds1 = new DataSet();
            da1.Fill(ds1);
            lbl_deliverycharge.Text ="$"+ ds1.Tables[0].Rows[0][0].ToString() ;
            Session["DeleveryInv_billtable"] = ds1.Tables[0].Rows[0][0].ToString();

            Session["DeleveryInv"] = lbl_deliverycharge.Text;
            SqlDataAdapter da = new SqlDataAdapter("SELECT charges  FROM [tbl_extracharges] where lotid=" + Convert.ToInt32(Session["LOTIDForInserting"].ToString()) + "and chargeFor='New Card Registration'", con.connString());
            DataSet ds = new DataSet();
            da.Fill(ds);

            activationcharge.ForeColor = Color.Green;
            lbl_deliverycharge.ForeColor = Color.Green;
            activationcharge.Text = "$" + ds.Tables[0].Rows[0][0].ToString();
            Session["ActivationChargeInv_billtable"] = ds.Tables[0].Rows[0][0].ToString();

            Session["ActivationChargeInv"] = activationcharge.Text;

            int lotid = Convert.ToInt32(Session["LOTIDForInserting"].ToString());

            DataSet ds_billingentrytype = new clsInsert().fetchrec("select Billing_entry from tbl_LotMaster where Lot_id=" + lotid);
            if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "1")
            {
                billingtypeentry = "Full Charge";
            }

            if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "2")
            {
                billingtypeentry = "Prorate";

            }

            if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "3")
            {
                billingtypeentry = "Prorate By Fort Night";

            }

            if (billingtypeentry == "Full Charge")
            {
                monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text);

            }
            if (billingtypeentry == "Prorate")
            {
                DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
                int y = active.Year;
                int m = active.Month;
                int d = active.Day;
                int days = DateTime.DaysInMonth(y, m);
                int daysinmonthremaining = days - d;
                double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days);
                monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text) * Convert.ToDecimal(daystocharge);


            }
            if (billingtypeentry == "Prorate By Fort Night")
            {
                DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
                int d = active.Day;
                if (d > dateformatch)
                {
                    monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text);

                }
                else
                {
                    monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text) / 2;
                }
            }







            decimal total = Convert.ToDecimal(Convert.ToDecimal(ds1.Tables[0].Rows[0][0].ToString()) + Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString())) + Convert.ToDecimal(monthly_totalcharge);
            total = Math.Round(total, 2);

            lbl_total.ForeColor = Color.ForestGreen;
            lbl_total.Text = "$" + Convert.ToString(total);
            Session["totalchargeinv"] = total;
            try
            {
                if (Session["Coupancode"].ToString() != null || Session["Coupancode"].ToString() != "")
                {
                    decimal discount = Convert.ToDecimal(lbl_discount.Text);
                    decimal grandtotal = total - discount;
                    decimal tax_total = Convert.ToDecimal(0.00);
                    if (lbl_taxmoney.Text != "")
                    {
                        tax_total = total * ((Convert.ToDecimal(lbl_taxmoney.Text)) / 100);
                        lbl_tax_money.Text=Convert.ToString(tax_total);
                        grandtotal = tax_total + grandtotal;
                    }
                    lb_grandtotal.Visible = true;
                    lbl_granttotal.ForeColor = Color.Green;
                    grandtotal = Math.Round(grandtotal, 2);
                    lbl_granttotal.Text = "$ " + Convert.ToString(grandtotal);
                    Session["grandtotal"] = grandtotal;
                }

            }
            catch
            {
                lb_grandtotal.Visible = true;
                lbl_granttotal.ForeColor = Color.Green;
                decimal tax_total = Convert.ToDecimal(0.00);
                if (lbl_taxmoney.Text != "")
                {
                    tax_total = total * ((Convert.ToDecimal(lbl_taxmoney.Text)) / 100);
                    lbl_tax_money.Text=Convert.ToString(tax_total);

                    total = tax_total + total;
                }
                total = Math.Round(total, 2);
                lbl_granttotal.Text = "$ " + Convert.ToString(total);
                Session["grandtotal"] = total;

            }
            Hashtable h3 = new Hashtable();
            h3.Clear();
            h3.Add("Carddeliverycharge", ddl_deliverymethod.SelectedItem.Value);
            // h3.Add("PlanName", ddl_Plan.SelectedItem.Value);
            h3.Add("ActivationDate", Convert.ToDateTime(txtbox_ActicationDate.Text));



            Session["hasht3"] = h3;

            btn_totalcharges.Enabled = true;
            // btn_selectsite.Enabled = false;
            btn_addvehicledetails.CssClass = "Initial";
            btn_personaldetails.CssClass = "Initial";
            btn_totalcharges.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }
        catch
        {
            btn_totalcharges.Enabled = true;
            btn_addvehicledetails.CssClass = "Initial";
            btn_personaldetails.CssClass = "Initial";
            btn_totalcharges.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }








       // btn_selectsite.CssClass = "Initial";
        btn_addvehicledetails.CssClass = "Initial";
        btn_personaldetails.CssClass = "Initial";
        btn_totalcharges.CssClass = "Clicked";
        MainView.ActiveViewIndex = 2;
    }
    protected void Tab2_Click(object sender, EventArgs e)
    {
        //btn_selectsite.CssClass = "Initial";
        btn_addvehicledetails.CssClass = "Initial";
        btn_personaldetails.CssClass = "Clicked";
        btn_totalcharges.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
  
    protected void Tab3_Click(object sender, EventArgs e)
    {
        //btn_selectsite.CssClass = "Initial";
        try
        {
            txtbox_thirdFirstname.Text = txtBox_firstname.Text;
            txtbox_thirdLastname.Text = txtbox_lastname.Text;
            btn_addvehicledetails.Enabled = true;

            btn_personaldetails.Enabled = true;
            btn_addvehicledetails.CssClass = "Clicked";
            btn_personaldetails.CssClass = "Initial";
            btn_totalcharges.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
            Hashtable h2 = new Hashtable();
            h2.Clear();
            //h = (Hashtable )Session["hasht"];
            h2.Add("Account Type", "Individual");
            try
            {
                if (Session["PlanrateID"].ToString() != "" || Session["PlanrateID"].ToString() != null)
                {
                    h2.Add("PlanrateID", Convert.ToInt32(Session["PlanrateID"].ToString()));
                    h2.Add("LOTIDForInserting", Convert.ToInt32(Session["LOTIDForInserting"].ToString()));
                    h2.Add("Parkingtype", Convert.ToInt32(Session["parkinttype"].ToString()));
                }
            }
            catch
            {
                Response.Redirect("LotAddresssearch.aspx");
            }

            h2.Add("Firstname", txtBox_firstname.Text);
            h2.Add("LastName", txtbox_lastname.Text);
            h2.Add("CardNumber", txtbox_coupannumber.Text);
            h2.Add("Address1", txtbox_address1.Text);
            h2.Add("Address2", txtbox_address2.Text);
            h2.Add("Postal/Zip", txtbox_postal.Text);


            h2.Add("Parker_Country", ddl_Parkercountry.SelectedItem.Text);
            h2.Add("Parker_City", ddl_Parkercity.SelectedItem.Text);
            h2.Add("Parker_State", ddlstate.SelectedItem.Text);
            h2.Add("Parker_Cellular", txtbox_cellular.Text);
            h2.Add("Parker_Phone", txtbox_phone.Text);
            h2.Add("Parker_Fax", txt_fax.Text);
            h2.Add("Parker_Emailid", txtbox_Email.Text);
            Session["Usernameafterregis"] = txtBox_username.Text;
            Session["passwordforlogin"] = txtBox_Passwrd.Text;
            h2.Add("Parker_UserName", txtBox_username.Text);
            h2.Add("Parker_Password", Session["passwordforlogin"].ToString());
            if (rbtn_invoicebymail.Checked == true)
            {
                h2.Add("invoice type", 3);
            }
            if (rbtn_creditcard.Checked == true)
            {
                h2.Add("invoice type", 1);
            }

            if (rbtn_invoicebyemail.Checked == true)
            {
                h2.Add("invoice type", 2);
            }

            if (rbtn_ach.Checked == true)
            {

                h2.Add("invoice type", 4);
                h2.Add("CustomerName", txtbox_customername.Text);
                h2.Add("bankname", txtbox_bankname.Text);
                h2.Add("Bank_zip/postal", txtbox_bankzip.Text);
                h2.Add("state/postal", ddl_bankstate.SelectedItem.Text);
                h2.Add("BankAccountType", ddl_accounttype.SelectedItem.Text);

                h2.Add("bankaccountno", txtbox_bankaccountno.Text);
                h2.Add("branch", txtbox_branch.Text);
                h2.Add("city", txtbox_city_bank.Text);
                h2.Add("routing", txtbox_routingno.Text);


            }

            Session["hasht2"] = h2;


        }
        catch
        {
            txtbox_thirdFirstname.Text = txtBox_firstname.Text;
            txtbox_thirdLastname.Text = txtbox_lastname.Text;
            btn_addvehicledetails.Enabled = true;

            btn_personaldetails.Enabled = true;

            MainView.ActiveViewIndex = 1;
        }














        btn_addvehicledetails.CssClass = "Clicked";
        btn_personaldetails.CssClass = "Initial";
        btn_totalcharges.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
   

    public void bind_DeliveryMaster()
    {
        try
        {
            int lotiddd = Convert.ToInt32(Session["LOTIDForInserting"].ToString());
            //clsInsert cs = new clsInsert();
            //DataSet ds1 = cs.select_operation("select tbl_SiteMaster.SiteId ,tbl_LotMaster.Lot_id from tbl_LotMaster inner join tbl_SiteMaster on tbl_SiteMaster.SiteId=tbl_LotMaster.Site_Id where tbl_LotMaster.Lot_id='" + lotiddd + "'");

            //int site_id_carddelivery = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());

            SqlDataAdapter da = new SqlDataAdapter("select tbl_CardDeliveryID.*,tbl_CardDeliveryMaster.* from tbl_CardDeliveryMaster inner join tbl_CardDeliveryID on tbl_CardDeliveryID.card_delivery_id=tbl_CardDeliveryMaster.CardDeliveryId where tbl_CardDeliveryID.Lotid=" + lotiddd, con.connString());
            DataSet ds = new DataSet();
            da.Fill(ds);


            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                ListItem l = new ListItem(ds.Tables[0].Rows[i][4].ToString() + " [Price :" + ds.Tables[0].Rows[i][6].ToString() + "]", ds.Tables[0].Rows[i][3].ToString());
                ddl_deliverymethod.Items.Add(l);
                //ddl_deliverymethod_corporate.Items.Add(l);
            }


        }
        catch
        {
        }
    }


    protected void btn_next2_Click(object sender, EventArgs e)
    {
        try
        {

          
           
                txtbox_thirdFirstname.Text = txtBox_firstname.Text;
                txtbox_thirdLastname.Text = txtbox_lastname.Text;
                btn_addvehicledetails.Enabled = true;

                btn_personaldetails.Enabled = true;
                btn_addvehicledetails.CssClass = "Clicked";
                btn_personaldetails.CssClass = "Initial";
                btn_totalcharges.CssClass = "Initial";
                MainView.ActiveViewIndex = 1;
                Hashtable h2 = new Hashtable();
                h2.Clear();
                //h = (Hashtable )Session["hasht"];
                h2.Add("Account Type", "Individual");
                try
                {
                    if (Session["PlanrateID"].ToString() != "" || Session["PlanrateID"].ToString() != null)
                    {
                        h2.Add("PlanrateID", Convert.ToInt32(Session["PlanrateID"].ToString()));
                        h2.Add("LOTIDForInserting", Convert.ToInt32(Session["LOTIDForInserting"].ToString()));
                        h2.Add("Parkingtype", Convert.ToInt32(Session["parkinttype"].ToString()));
                    }
                }
                catch
                {
                    Response.Redirect("LotAddresssearch.aspx");
                }

                h2.Add("Firstname", txtBox_firstname.Text);
                h2.Add("LastName", txtbox_lastname.Text);
                h2.Add("CardNumber", txtbox_coupannumber.Text);
                h2.Add("Address1", txtbox_address1.Text);
                h2.Add("Address2", txtbox_address2.Text);
                h2.Add("Postal/Zip", txtbox_postal.Text);


                h2.Add("Parker_Country", ddl_Parkercountry.SelectedItem.Text);
                h2.Add("Parker_City", ddl_Parkercity.SelectedItem.Text);
                h2.Add("Parker_State", ddlstate.SelectedItem.Text);
                h2.Add("Parker_Cellular", txtbox_cellular.Text);
                h2.Add("Parker_Phone", txtbox_phone.Text);
                h2.Add("Parker_Fax", txt_fax.Text);
                h2.Add("Parker_Emailid", txtbox_Email.Text);
                Session["Usernameafterregis"] = txtBox_username.Text;
                Session["passwordforlogin"] = txtBox_Passwrd.Text;
                h2.Add("Parker_UserName", txtBox_username.Text);
                h2.Add("Parker_Password", Session["passwordforlogin"].ToString());

                if (rbtn_invoicebymail.Checked == true)
                {
                    h2.Add("invoice type", 3);
                }
                if (rbtn_creditcard.Checked == true)
                {
                    h2.Add("invoice type", 1);
                }

                if (rbtn_invoicebyemail.Checked == true)
                {
                    h2.Add("invoice type", 2);
                }

                if (rbtn_ach.Checked == true)
                {


                    h2.Add("invoice type", 4);
                    h2.Add("CustomerName", txtbox_customername.Text);
                    h2.Add("bankname", txtbox_bankname.Text);
                    h2.Add("Bank_zip/postal", txtbox_bankzip.Text);
                    h2.Add("state/postal", ddl_bankstate.SelectedItem.Text);
                    h2.Add("BankAccountType", ddl_accounttype.SelectedItem.Text);

                    h2.Add("bankaccountno", txtbox_bankaccountno.Text);
                    h2.Add("branch", txtbox_branch.Text);
                    h2.Add("city", txtbox_city_bank.Text);
                    h2.Add("routing", txtbox_routingno.Text);


                }

                Session["hasht2"] = h2;
            

        }
        catch
        {
            txtbox_thirdFirstname.Text = txtBox_firstname.Text;
            txtbox_thirdLastname.Text = txtbox_lastname.Text;
            btn_addvehicledetails.Enabled = true;

            btn_personaldetails.Enabled = true;

            MainView.ActiveViewIndex = 1;
        }
        

    }
    protected void btn_next3_Click(object sender, EventArgs e)
    {

        try
        {
            SqlDataAdapter da1 = new SqlDataAdapter("SELECT Charges FROM [tbl_CardDeliveryMaster] where CardDeliveryId="+ddl_deliverymethod.SelectedItem.Value, con.connString());
            DataSet ds1 = new DataSet();
            da1.Fill(ds1);
            lbl_deliverycharge.Text ="$"+ ds1.Tables[0].Rows[0][0].ToString();
            Session["DeleveryInv_billtable"] = ds1.Tables[0].Rows[0][0].ToString();

            Session["DeleveryInv"] = lbl_deliverycharge.Text;
            SqlDataAdapter da = new SqlDataAdapter("SELECT charges  FROM [tbl_extracharges] where lotid=" + Convert.ToInt32(Session["LOTIDForInserting"].ToString()) + "and chargeFor='New Card Registration'", con.connString());
            DataSet ds = new DataSet();
            da.Fill(ds);
            
            activationcharge.ForeColor = Color.Green;
            lbl_deliverycharge.ForeColor = Color.Green;
           activationcharge.Text ="$"+ ds.Tables[0].Rows[0][0].ToString();
           Session["ActivationChargeInv_billtable"] = ds.Tables[0].Rows[0][0].ToString();
           Session["ActivationChargeInv"] = activationcharge.Text;


            int lotid=Convert.ToInt32(Session["LOTIDForInserting"].ToString());

           DataSet ds_billingentrytype = new clsInsert().fetchrec("select Billing_entry from tbl_LotMaster where Lot_id=" + lotid);
           if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "1")
           {
               billingtypeentry = "Full Charge";
           }

           if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "2")
           {
               billingtypeentry = "Prorate";

           }

           if (ds_billingentrytype.Tables[0].Rows[0][0].ToString() == "3")
           {
               billingtypeentry = "Prorate By Fort Night";

           }

           if (billingtypeentry == "Full Charge")
           {
               monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text);

           }
           if (billingtypeentry == "Prorate")
           {
               DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
               int y = active.Year;
               int m = active.Month;
               int d = active.Day;
               int days = DateTime.DaysInMonth(y, m);
               int daysinmonthremaining = days - d;
               double daystocharge = Convert.ToDouble(daysinmonthremaining) / Convert.ToDouble(days);
               monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text) * Convert.ToDecimal(daystocharge);


           }
           if (billingtypeentry == "Prorate By Fort Night")
           {
               DateTime active = Convert.ToDateTime(txtbox_ActicationDate.Text);
               int d = active.Day;
               if (d > dateformatch)
               {
                   monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text);

               }
               else
               {
                   monthly_totalcharge = Convert.ToDecimal(lbl_amount.Text) / 2;
               }
           }





           decimal total = Convert.ToDecimal(Convert.ToDecimal(ds1.Tables[0].Rows[0][0].ToString()) + Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString())) + Convert.ToDecimal(monthly_totalcharge);
           total = Math.Round(total, 2);

            lbl_total.ForeColor = Color.ForestGreen;
            lbl_total.Text ="$"+ Convert.ToString(total) ;
            Session["totalchargeinv"] = total;
            try
            {
                if (Session["Coupancode"].ToString() != null || Session["Coupancode"].ToString() != "")
                {
                   // decimal discount = Convert.ToDecimal(lbl_discount.Text);
                    decimal grandtotal = total ;
                    decimal tax_total = Convert.ToDecimal(0.00);
                    if (lbl_taxmoney.Text != "")
                    {
                        Session["taxrate"] = lbl_taxmoney.Text;
                         tax_total = total * ((Convert.ToDecimal(lbl_taxmoney.Text)) / 100);
                         lbl_tax_money.Text=Convert.ToString(tax_total);

                         grandtotal = tax_total + grandtotal;
                    }
                    if (lbl_taxmoney.Text == "")
                    {
                    lbl_tax_money.Text=Convert.ToString("0.00");

                        Session["taxrate"] = "0.00";
                        
                    }
                  
                    lb_grandtotal.Visible = true;
                    lbl_granttotal.ForeColor = Color.Green;
                    grandtotal = Math.Round(grandtotal, 2);
                    lbl_granttotal.Text ="$ "+ Convert.ToString(grandtotal);
                    Session["grandtotal"] = grandtotal;
                }

            }
            catch
            {
                lb_grandtotal.Visible = true;
                lbl_granttotal.ForeColor = Color.Green;
                decimal tax_total = Convert.ToDecimal(0.00);
                if (lbl_taxmoney.Text != "")
                {
                    Session["taxrate"] = lbl_taxmoney.Text;
                    tax_total = total * ((Convert.ToDecimal(lbl_taxmoney.Text)) / 100);
                    lbl_tax_money.Text=Convert.ToString(tax_total);
                    total = tax_total + total;
                }
               
                if (lbl_taxmoney.Text == "")
                {
                    lbl_tax_money.Text=Convert.ToString("0.00");

                    Session["taxrate"] = "0.00";

                }
                total = Math.Round(total, 2);
                lbl_granttotal.Text ="$" +Convert.ToString(total)  ;
                Session["grandtotal"] = total;

            }
            Hashtable h3 = new Hashtable();
            h3.Clear();
            h3.Add("Carddeliverycharge", ddl_deliverymethod.SelectedItem.Value);
            // h3.Add("PlanName", ddl_Plan.SelectedItem.Value);
            h3.Add("ActivationDate", Convert.ToDateTime(txtbox_ActicationDate.Text));



            Session["hasht3"] = h3;

            btn_totalcharges.Enabled = true;
            // btn_selectsite.Enabled = false;
            btn_addvehicledetails.CssClass = "Initial";
            btn_personaldetails.CssClass = "Initial";
            btn_totalcharges.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }
        catch
        {
            btn_totalcharges.Enabled = true;
            btn_addvehicledetails.CssClass = "Initial";
            btn_personaldetails.CssClass = "Initial";
            btn_totalcharges.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }
    }
    
    protected void btn_prev3_Click(object sender, EventArgs e)
    {
       // btn_selectsite.Enabled = false;
        btn_personaldetails.Enabled = true;
        btn_addvehicledetails.CssClass = "Initial";
        btn_personaldetails.CssClass = "Clicked";
        btn_totalcharges.CssClass = "Initial";
        MainView.ActiveViewIndex = 0;
    }
    protected void btn_pre4_Click(object sender, EventArgs e)
    {
        //btn_selectsite.Enabled = false;
        
        btn_addvehicledetails.Enabled = true;
        btn_addvehicledetails.CssClass = "Clicked";
        btn_personaldetails.CssClass = "Initial";
        btn_totalcharges.CssClass = "Initial";
        MainView.ActiveViewIndex = 1;
    }
    protected void btn_entercreditcard_Click(object sender, EventArgs e)
    {

    }
    protected void rbtn_creditcard_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_creditcard.Checked == true)
        {
            btn_entercreditcard.Visible = true;
            Panel5.Visible = false;

            //Panel4.Visible = false;
        }
        else
        {
            btn_entercreditcard.Visible = false;
            // Panel4.Visible = false;
        }
        if (rbtn_ach.Checked == true)
        {
            Panel5.Visible = true;
            btn_entercreditcard.Visible = false;

        }
        else
        {

            // Panel4.Visible = false;
        }
       
        UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel2.Update();
   
    }
    protected void rbtn_ach_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_creditcard.Checked == true)
        {
            btn_entercreditcard.Visible = true;
            Panel5.Visible = false;

            //Panel4.Visible = false;
        }
        else
        {
            btn_entercreditcard.Visible = false;
            // Panel4.Visible = false;
        }
        if (rbtn_ach.Checked == true)
        {
            Panel5.Visible = true;
            btn_entercreditcard.Visible = false;
         
            UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
            UpdatePanel2.Update();

        }
        else
        {

            // Panel4.Visible = false;
        }
        UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel2.Update();
       
    }
      
    
    protected void rb_account1_CheckedChanged(object sender, EventArgs e)
    {
        if (rb_account1.Checked == true)
        {
            btn_addvehicledetails.Visible = false;
            btn_totalcharges.Visible = false;
        }
        else
        {
            btn_addvehicledetails.Visible = true;
            btn_totalcharges.Visible = true;
        }
        


    }
    protected void rbtn_invoicebymail0_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_creditcard.Checked == true)
        {
            btn_entercreditcard.Visible = true;
            Panel5.Visible = false;

            //Panel4.Visible = false;
        }
        else
        {
            Panel5.Visible = false;
            btn_entercreditcard.Visible = false;
        }
        if (rbtn_ach.Checked == true)
        {
            Panel5.Visible = true;
            btn_entercreditcard.Visible = false;

        }
        else
        {
            Panel5.Visible = false;
            btn_entercreditcard.Visible = false;

            // Panel4.Visible = false;
        }
        UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel2.Update();
    }
    protected void rbtn_invoicebyemail0_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtn_creditcard.Checked == true)
        {
            btn_entercreditcard.Visible = true;
            Panel5.Visible = false;
            

            //Panel4.Visible = false;
        }
        else
        {
            Panel5.Visible = false;
            btn_entercreditcard.Visible = false;
        }
        if (rbtn_ach.Checked == true)
        {
            Panel5.Visible = true;
            btn_entercreditcard.Visible = false;

        }
        else
        {
            Panel5.Visible = false;
            btn_entercreditcard.Visible = false;

            // Panel4.Visible = false;
        }
        UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel2.Update();
    }
    protected void btn_addVehicle_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('AddIndividua_vehicleparker.aspx','New Windows','height=600, width=900,location=no','titlebar= no; toolbar= no; statusbar=no');", true);
    }

    clsData obj = new clsData();
    DataTable dt;
    public void addrec()
    {
        insertrec();
        clsInsert cs = new clsInsert();
        DataSet ds1 = cs.select_operation("select Parker_id from tbl_ParkerRegis where UserName='" + txtBox_username.Text + "'order by  Parker_id desc");

        int lastid = Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString());
        try
        {



            for (int i = 0; i < dt.Rows.Count; i++)
            {

                hst.Clear();
                hst.Add("parkeridforvehicle", lastid);

                hst.Add("VehicelUserName", txtBox_username.Text);
                hst.Add("Make", dt.Rows[i]["VehicalMake"].ToString());
                hst.Add("model", dt.Rows[i]["VehicalModel"].ToString());
                hst.Add("year", dt.Rows[i]["VehicalYear"].ToString());
                hst.Add("color", dt.Rows[i]["VehicalColor"].ToString());
                hst.Add("licenseno", dt.Rows[i]["VehicalLicensePlate"].ToString());
                int result = objData.ExecuteNonQuery("[sp_VehicleParker]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {

                    //Response.Redirect("UserManager.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }

            Session["datatable"] = null;
            Session["hasht2"] = null;
            Session["hasht3"] = null;

        }
        catch
        {


        }


      
        


        try
        {
            if (Session["Coupancode"].ToString() != null || Session["Coupancode"].ToString() != "")
            {
                hst.Clear();
                hst.Add("couponcode", Session["Coupancode"].ToString());
                int result = objData.ExecuteNonQuery("[sp_updatestatus]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {

                    //Response.Redirect("UserManager.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }
        }
        catch
        {
            //txtbox_coupannumber.Text = "";
            //lbl_discount.Visible = false;
            //discount.Visible = false;
        }





    }

    protected void btn_add1_Click(object sender, EventArgs e)
    {
        try
        {

            if (chbk_accept.Checked == true)
            {
                try
                {


                    dt = (DataTable)Session["datatable"];


                    bool b = obj.mail(txtbox_Email.Text, "User Name :" + Session["Usernameafterregis"].ToString() + "\nPassword :" + Session["passwordforlogin"].ToString(), "Successfully Registerd on ipass" + System.DateTime.Now.ToString());
                    if (b == true)
                    {

                        addrec();


                        Session["registration"] = "True";

                        Response.Redirect("default.aspx");


                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Email Id not exist.')", true);

                    }


                }

                catch
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please Fill the details')", true);
                }

            }

            else
            {
                lbl_error.Visible = true;

                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Please accept the terms and condition.')", true);
            }
        }
        catch
        {
        }

    }
    string customername="";
    string bankaccounttype="";
    string bankname="" ;
    string branch="" ;
    string bank_zip = "";
    string bank_city = "";
    string state_bank = "";
    string routingno = "";
    string bankaccountno = "";
   
    public void insertrec()
    {
        

        Hashtable second = (Hashtable)Session["hasht2"];
        string account_type = second["Account Type"].ToString();
        int PlanrateID =Convert.ToInt32( second["PlanrateID"].ToString());
        int LOTIDForInserting =Convert.ToInt32( second["LOTIDForInserting"].ToString());
        int Parkingtype =Convert.ToInt32( second["Parkingtype"].ToString());

        string firstname = second["Firstname"].ToString();
        string lastname = second["LastName"].ToString();
        Session["fullname"] = firstname + "   " + lastname;
        string cardnumber;
        if (txtbox_coupannumber.Text == "")
        {
            cardnumber = "0";
        }
        else
        {

            cardnumber = second["CardNumber"].ToString();
        }

        string address1 = second["Address1"].ToString();
        string adress2 = second["Address2"].ToString();

        Session["Full Address"] = address1 +  adress2;
        string postal_parker = second["Postal/Zip"].ToString();
        string country_parker = second["Parker_Country"].ToString();
        string state_parker = second["Parker_State"].ToString();
        string city_parker = second["Parker_City"].ToString();
        string cellular_parker = second["Parker_Cellular"].ToString();
        string phone_parker = second["Parker_Phone"].ToString();
        string fax_parker = second["Parker_Fax"].ToString();
        string email_parker = second["Parker_Emailid"].ToString();
        string loginusername_parker = second["Parker_UserName"].ToString();
        string pass_parker = second["Parker_Password"].ToString();
        string type = "individual";
        string userstatus = "true";
        string oprator = "Self";
        string modifiedby = "Self";
        string status = "true";
        int invoicetype = Convert.ToInt32(second["invoice type"].ToString());
        if (invoicetype == 4)
        {
            customername = second["CustomerName"].ToString();
            bankaccounttype = second["BankAccountType"].ToString();
            bankname = second["bankname"].ToString();
            branch = second["branch"].ToString();
            bank_zip = second["Bank_zip/postal"].ToString();
            bank_city = second["city"].ToString();
            state_bank = second["state/postal"].ToString();
            routingno = second["routing"].ToString();
            bankaccountno = second["bankaccountno"].ToString();
        }
        Hashtable third = (Hashtable)Session["hasht3"];
       
        DateTime date = Convert.ToDateTime(third["ActivationDate"].ToString());
        int carddeliveryid = Convert.ToInt32(third["Carddeliverycharge"].ToString());
        clsInsert cs = new clsInsert();
        DataSet ds1 = cs.select_operation("SELECT * FROM tbl_CardDeliveryMaster where CardDeliveryId='" + carddeliveryid + "'");
        string chargedfor = ds1.Tables[0].Rows[0]["CardDeliveryName"].ToString();
        string charges = ds1.Tables[0].Rows[0]["Charges"].ToString();

        string deposite = "false";


        try
        {
            hst.Clear();


            hst.Add("action", "insert");
            hst.Add("Lotid", LOTIDForInserting);
            hst.Add("parkingtype", Parkingtype);
            hst.Add("planrateid", PlanrateID);

            hst.Add("accounttype", account_type);
            hst.Add("UserName", loginusername_parker);
            hst.Add("CardNo ", cardnumber);
            hst.Add("firstname", firstname);
            hst.Add("lastname", lastname);
            hst.Add("AdressLine1", address1);
            hst.Add("AddressLine2", adress2);
            hst.Add("Zip_Parker", postal_parker);
            hst.Add("Country_parker", country_parker);
            hst.Add("State_Parker", state_parker);
            hst.Add("city_parker", city_parker);

            hst.Add("cell_parker", cellular_parker);
            hst.Add("phone_parker", phone_parker);
            hst.Add("fax", fax_parker);
            hst.Add("email", email_parker);
            hst.Add("billing", invoicetype);
            //hst.Add("tariffplanid", tariifid);
            hst.Add("activationdate", date);
            hst.Add("carddeliveryid", carddeliveryid);

            hst.Add("login_username", loginusername_parker);
            hst.Add("pass", pass_parker);
            hst.Add("type", type);
            hst.Add("userstatus", userstatus);
            hst.Add("operatorid", oprator);
            hst.Add("modifiedby", modifiedby);
            hst.Add("dateofregis", DateTime.Now);

            hst.Add("Other_userName", loginusername_parker);
            hst.Add("ChargeFor", chargedfor);
            hst.Add("charges", charges);
            hst.Add("dateofchanging", DateTime.Now);
            hst.Add("deposite", deposite);

            if (invoicetype == 4)
            {
                hst.Add("AccountUserName", loginusername_parker);
                hst.Add("CustomerName", customername);
                hst.Add("BankAccountType", bankaccounttype);
                hst.Add("BranchName", branch);
                hst.Add("BankName", bankname);
                hst.Add("Bank_zip", bank_zip);
                hst.Add("Bank_city", bank_city);
                hst.Add("Bank_state", state_bank);
                hst.Add("bankrouting", routingno);
                hst.Add("bankAccountNo", bankaccountno);

            }


            int result = objData.ExecuteNonQuery("[sp_Parker]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               

            //decimal subtotal = (activationcharge + carddeliverycharges + monthly_totalcharge);
            //decimal total = (activationcharge + carddeliverycharges + monthly_totalcharge) * taxcharge;

            DateTime activedate = Convert.ToDateTime(txtbox_ActicationDate.Text);
            int year1 = activedate.Year;

            int month1 = activedate.Month;
            string month = "";
            if (month1 < 10)
            {
                month = "0" + Convert.ToString(month1);
            }
            if (month1 >= 10)
            {
                month = Convert.ToString(month1);

            }
            string yearmonth = Convert.ToString(year1) + Convert.ToString(month);
            DataSet dsinvoice = new clsInsert().fetchrec("select * from TBL_Invoice where Inv_num like'" + yearmonth + "%' order by Inv_num desc  ");
            if (dsinvoice.Tables[0].Rows.Count > 0)
            {
                string oFName = dsinvoice.Tables[0].Rows[0][1].ToString().Trim();
                string[] strArr = oFName.Split('_');
                int sLength = strArr.Length;
                int number = Convert.ToInt32(strArr[1]) + Convert.ToInt32(1);
                if (number < 10)
                {
                    invoicenumber = yearmonth + "_" + "00000" + Convert.ToString(number);
                }
                if (number >= 10 && number < 100)
                {
                    invoicenumber = yearmonth + "_" + "0000" + Convert.ToString(number);
                }
                if (number >= 100 && number < 1000)
                {
                    invoicenumber = yearmonth + "_" + "000" + Convert.ToString(number);
                }
                if (number >= 1000 && number < 10000)
                {
                    invoicenumber = yearmonth + "_" + "00" + Convert.ToString(number);
                }
            }
            else
            {
                invoicenumber = yearmonth + "_" + "000001";
            }
            Session["invoicenumber"] = invoicenumber;
                hst.Clear();

            hst.Add("action", "insert_billing_corporate");
            hst.Add("invoicenumber", invoicenumber);

            hst.Add("Parkerusername", txtBox_username.Text);

            hst.Add("startdate", Convert.ToDateTime(txtbox_ActicationDate.Text));

            DateTime today = Convert.ToDateTime(txtbox_ActicationDate.Text);
            int numberOfDaysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
            DateTime endOfMonth = new DateTime(today.Year, today.Month, numberOfDaysInMonth);


            // DateTime d1 = Convert.ToDateTime(txtbox_ActicationDate.Text).AddMonths(1);
            hst.Add("duedate", endOfMonth);
            hst.Add("subtotal", Convert.ToDecimal(lbl_total.Text.TrimStart('$')));
            hst.Add("tax", Convert.ToDecimal(lbl_tax_money.Text));
            hst.Add("details", "Invoice At The Time Of Registration");

            hst.Add("totalamount", Convert.ToDecimal(lbl_granttotal.Text.TrimStart('$')));

            decimal dueamount = Convert.ToDecimal(lbl_granttotal.Text.TrimStart('$'));
            hst.Add("dueamount ", dueamount);
            hst.Add("billingtype", 3);


            int result_billing = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
           
                 if (result_billing > 0)
                  {
                      //sp_billingfullinfo
                      hst.Clear();
                DataSet ds2 = new clsInsert().select_operation("select Inv_num from TBL_Invoice where Acct_Num='" + txtBox_username.Text + "'order by  ID desc");

                string lastid_bill = ds2.Tables[0].Rows[0][0].ToString();
                    if (Session["amount_parking"].ToString() != "null" || Session["amount_parking"].ToString() != "")
                      {
                        hst.Clear();
                         
                         hst.Add("action", "fulldetailsaboutbill");
                    hst.Add("billid", lastid_bill);
                    hst.Add("billtitle", "Monthly Charges");
                    hst.Add("amountparticular", Convert.ToDecimal(lbl_amount.Text));
                    hst.Add("date", DateTime.Now);
                    hst.Add("accountnumber", txtBox_username.Text);

                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        {
                        }

                      }
                      if (Session["ActivationChargeInv"].ToString() != null || Session["ActivationChargeInv"].ToString() != "")
                      {
                     
                           hst.Clear();
                           hst.Add("action", "fulldetailsaboutbill");
                           hst.Add("billid", lastid_bill);
                           hst.Add("billtitle", "Activation Charges");
                           hst.Add("amountparticular", Convert.ToDecimal(Session["ActivationChargeInv_billtable"].ToString()));
                           hst.Add("date", DateTime.Now);
                           hst.Add("accountnumber", txtBox_username.Text);

                    int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                        {
                        }
                      }
                      if (Session["DeleveryInv"].ToString() != null || Session["DeleveryInv"].ToString() != "")
                      {
                         
                           hst.Clear();
                           hst.Add("action", "fulldetailsaboutbill");
                           hst.Add("billid", lastid_bill);
                           hst.Add("billtitle", "Delivery Charges");
                           hst.Add("amountparticular", Convert.ToDecimal(Session["DeleveryInv_billtable"].ToString()));
                           hst.Add("date", DateTime.Now);
                           hst.Add("accountnumber", txtBox_username.Text);

                          int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                          {
                          }

                      }

                     
                      if (lbl_taxmoney.Text != "")
                      {
                       
                     

                           hst.Clear();
                           hst.Add("action", "insert");
                          hst.Add("billid", lastid_bill);
                          hst.Add("billtitle", "Tax Money");
                           if (lbl_taxmoney.Text != "")
                          {
                              decimal taxmoney = Convert.ToDecimal(lbl_total.Text.TrimStart('$')) * ((Convert.ToDecimal(lbl_taxmoney.Text)) / 100);
                              hst.Add("amountparticular", Convert.ToDecimal(taxmoney));

                          }
                          
                           hst.Add("date", DateTime.Now);
                           hst.Add("accountnumber", txtBox_username.Text);

                          int result_billing_full = objData.ExecuteNonQuery("[sp_corporateparkerbilling]", CommandType.StoredProcedure, hst);
                          {
                          }
                      }

                      if (Convert.ToString(defaultdeposit)=="0")
                      {
                         // @dateoftransaction,@creditamt,@reason,@status,@acctnumber
                          hst.Clear();
                          hst.Add("action", "insert");
                          hst.Add("acctnumber", txtBox_username.Text);
                          hst.Add("reason", "Advance At the Time Of Registration");
                          hst.Add("creditamt", Convert.ToDecimal(defaultdeposit));
                          hst.Add("dateoftransaction", DateTime.Now);
                          hst.Add("status", status);

                          int result_billing_full = objData.ExecuteNonQuery("[sp_CreaditNote]", CommandType.StoredProcedure, hst);
                          {
                          }

                      }

                  }

            }
            else
            {
                // lbl_error.Visible = true;

            }

        }
        catch
        {

        }

    }
    clsInsert cs = new clsInsert();

    int invoicetype_corporate;
    public void insertrec_corporate()
    {

     
        Hashtable second = (Hashtable)Session["hasht2"];
        string account_type = "Corporate";
        int PlanrateID = Convert.ToInt32(Session["PlanrateID"].ToString());
        int LOTIDForInserting = Convert.ToInt32(Session["LOTIDForInserting"].ToString());
        int Parkingtype = Convert.ToInt32(Session["parkinttype"].ToString());

        string firstname = txtBox_firstname.Text;
        string lastname = txtbox_lastname.Text;
        Session["fullname"] = firstname + "   " + lastname;
        string cardnumber;
        if (txtbox_coupannumber.Text == "")
        {
            cardnumber = "0";
        }
        else
        {

            cardnumber = txtbox_coupannumber.Text;
        }

        string address1 = txtbox_address1.Text;
        string adress2 = txtbox_address2.Text;

      //  Session["Full Address"] = address1 + adress2;
        string postal_parker = txtbox_postal.Text;
        string country_parker = ddl_Parkercountry.SelectedItem.Text;
        string state_parker = ddlstate.SelectedItem.Text;
        string city_parker = ddl_Parkercity.SelectedItem.Text;
        string cellular_parker =txtbox_cellular.Text;
        string phone_parker = txtbox_phone.Text;
        string fax_parker = txt_fax.Text;
        string email_parker = txtbox_Email.Text;
        string loginusername_parker = txtBox_username.Text;
        string pass_parker = txtBox_Passwrd.Text;
        string type = "Corporate";
        string userstatus = "true";
        string oprator = "Self";
        string modifiedby = "Self";




        
        if (rbtn_invoicebymail.Checked == true)
            {
                invoicetype_corporate = 3;
            }
            if (rbtn_creditcard.Checked == true)
            {
                invoicetype_corporate = 1;

              
            }

            if (rbtn_invoicebyemail.Checked == true)
            {
                invoicetype_corporate = 2;

                
            }

            if (rbtn_ach.Checked == true)
            {
                invoicetype_corporate = 4;

            }



            if (invoicetype_corporate == 4)
        {
           
            customername = txtbox_customername.Text;
            bankaccounttype = ddl_accounttype.SelectedItem.Text;
            bankname =txtbox_bankname.Text;
            branch = txtbox_branch.Text;
            bank_zip = txtbox_bankzip.Text;
            bank_city = txtbox_city_bank.Text;
            state_bank = ddl_bankstate.SelectedItem.Text;
            routingno = txtbox_routingno.Text;
            bankaccountno = txtbox_bankaccountno.Text;
        }
      
        //int carddeliveryid = Convert.ToInt32(ddl_deliverymethod_corporate.SelectedValue);




        clsInsert cs = new clsInsert();
        //DataSet ds1 = cs.select_operation("select * from tbl_carddeliverymaster where carddeliveryid='" + carddeliveryid + "'");
     //   string chargedfor = ds1.Tables[0].Rows[0]["carddeliveryname"].ToString();
       // string charges = ds1.Tables[0].Rows[0]["charges"].ToString();

        string deposite = "false";


        try
        {
            hst.Clear();


            hst.Add("action", "insert");
            hst.Add("Lotid", LOTIDForInserting);
            hst.Add("parkingtype", Parkingtype);
            hst.Add("planrateid", PlanrateID);

            hst.Add("accounttype", account_type);
            hst.Add("UserName", loginusername_parker);
            hst.Add("CardNo ", cardnumber);
            hst.Add("firstname", firstname);
            hst.Add("lastname", lastname);
            hst.Add("AdressLine1", address1);
            hst.Add("AddressLine2", adress2);
            hst.Add("Zip_Parker", postal_parker);
            hst.Add("Country_parker", country_parker);
            hst.Add("State_Parker", state_parker);
            hst.Add("city_parker", city_parker);

            hst.Add("cell_parker", cellular_parker);
            hst.Add("phone_parker", phone_parker);
            hst.Add("fax", fax_parker);
            hst.Add("email", email_parker);
            hst.Add("billing", invoicetype_corporate);
            //hst.Add("tariffplanid", tariifid);
           // hst.Add("activationdate",Convert.ToDateTime(txtbox_corporateactivationdate.Text));
           // hst.Add("carddeliveryid", carddeliveryid);

            hst.Add("login_username", loginusername_parker);
            hst.Add("pass", pass_parker);
            hst.Add("type", type);
            hst.Add("userstatus", userstatus);
            hst.Add("operatorid", oprator);
            hst.Add("modifiedby", modifiedby);
            hst.Add("dateofregis ", DateTime.Now);

            hst.Add("Other_userName", loginusername_parker);
        //    hst.Add("ChargeFor", chargedfor);
        //    hst.Add("charges", charges);
            hst.Add("dateofchanging", DateTime.Now);
            hst.Add("deposite", deposite);

            if (invoicetype_corporate == 4)
            {
                hst.Add("AccountUserName", loginusername_parker);
                hst.Add("CustomerName", customername);
                hst.Add("BankAccountType", bankaccounttype);
                hst.Add("BranchName", branch);
                hst.Add("BankName", bankname);
                hst.Add("Bank_zip", bank_zip);
                hst.Add("Bank_city", bank_city);
                hst.Add("Bank_state", state_bank);
                hst.Add("bankrouting", routingno);
                hst.Add("bankAccountNo", bankaccountno);

            }


            int result = objData.ExecuteNonQuery("[sp_Parker_Corporate]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {

                hst.Clear();





                SqlDataAdapter da1 = new SqlDataAdapter("SELECT Charges FROM [tbl_CardDeliveryMaster] where CardDeliveryId=" + ddl_deliverymethod.SelectedItem.Value, con.connString());
                DataSet cardcharge = new DataSet();
                da1.Fill(cardcharge);
             
                Session["DeleveryInv_billtable"] = cardcharge.Tables[0].Rows[0][0].ToString();

              
                SqlDataAdapter da = new SqlDataAdapter("SELECT charges  FROM [tbl_extracharges] where lotid=" + Convert.ToInt32(Session["LOTIDForInserting"].ToString()) + "and chargeFor='New Card Registration'", con.connString());
                DataSet ds_newregistrationcharges = new DataSet();
                da.Fill(ds_newregistrationcharges);
                Session["ActivationChargeInv_billtable"] = ds_newregistrationcharges.Tables[0].Rows[0][0].ToString();




                decimal total = Convert.ToDecimal(Convert.ToDecimal(cardcharge.Tables[0].Rows[0][0].ToString()) + Convert.ToDecimal(ds_newregistrationcharges.Tables[0].Rows[0][0].ToString())) + Convert.ToDecimal(lbl_amount.Text);
                
                Session["totalchargeinv"] = total;







                hst.Add("action", "insert");
                hst.Add("parkerusername", txtBox_username.Text);
               // hst.Add("startdate", Convert.ToDateTime(txtbox_corporateactivationdate.Text));
              //  DateTime d1 = Convert.ToDateTime(txtbox_corporateactivationdate.Text).AddMonths(1);
              //  hst.Add("duedate", d1);


                hst.Add("totalamount", Convert.ToDecimal(Session["totalchargeinv"].ToString()));
                hst.Add("depositamount", defaultdeposit);
                decimal dueamount = Convert.ToDecimal(Session["totalchargeinv"].ToString()) - defaultdeposit;
                hst.Add("dueamount ", dueamount);

                hst.Add("transactionid", transactionid);

                if (rbtn_invoicebymail.Checked == true)
                {
                    hst.Add("billingwith", 3);

                }
                if (rbtn_creditcard.Checked == true)
                {
                    hst.Add("billingwith", 1);

                }

                if (rbtn_invoicebyemail.Checked == true)
                {
                    hst.Add("billingwith", 2);


                }

                if (rbtn_ach.Checked == true)
                {
                    hst.Add("billingwith", 4);
                }

                int result_billing = objData.ExecuteNonQuery("[sp_billinginfo]", CommandType.StoredProcedure, hst);
                if (result_billing > 0)
                {
                    //sp_billingfullinfo
                    hst.Clear();
                    DataSet ds2 = cs.select_operation("select ID from tbl_ParkerBilling where Parker_UserName='" + txtBox_username.Text + "'order by  ID desc");

                    int lastid_bill = Convert.ToInt32(ds2.Tables[0].Rows[0][0].ToString());
                    if (Session["amount_parking"].ToString() != "null" || Session["amount_parking"].ToString() != "")
                    {

                        hst.Add("action", "insert");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Plan Charges");
                        hst.Add("amount_particular", Convert.ToDecimal(Session["amount_parking"].ToString()));
                        int result_billing_full = objData.ExecuteNonQuery("[sp_billingfullinfo]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Session["ActivationChargeInv_billtable"].ToString() != null || Session["ActivationChargeInv_billtable"].ToString() != "")
                    {
                        hst.Clear();
                        hst.Add("action", "insert");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Activation Charges");
                        hst.Add("amount_particular", Convert.ToDecimal(Session["ActivationChargeInv_billtable"].ToString()));
                        int result_billing_full = objData.ExecuteNonQuery("[sp_billingfullinfo]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }
                    if (Session["DeleveryInv_billtable"].ToString() != null || Session["DeleveryInv_billtable"].ToString() != "")
                    {
                        hst.Clear();
                        hst.Add("action", "insert");
                        hst.Add("billid", lastid_bill);
                        hst.Add("billtitle", "Delivery Charges");
                        hst.Add("amount_particular", Convert.ToDecimal(Session["DeleveryInv_billtable"].ToString()));
                        int result_billing_full = objData.ExecuteNonQuery("[sp_billingfullinfo]", CommandType.StoredProcedure, hst);
                        if (result_billing_full > 0)
                        {
                        }
                    }


                }

            }
            else
            {
                // lbl_error.Visible = true;

            }

        }
        catch
        {

        }


        try
        {
            if (Session["Coupancode"].ToString() != null || Session["Coupancode"].ToString() != "")
            {
                hst.Clear();
                hst.Add("couponcode", Session["Coupancode"].ToString());
                int result = objData.ExecuteNonQuery("[sp_updatestatus]", CommandType.StoredProcedure, hst);
                if (result > 0)
                {

                    //Response.Redirect("UserManager.aspx");

                }
                else
                {
                    // lbl_error.Visible = true;

                }
            }
        }
        catch
        {
            //txtbox_coupannumber.Text = "";
            //lbl_discount.Visible = false;
            //discount.Visible = false;
        }


    }

    public void clear()
    {
        txtBox_firstname.Text = "";
        txtbox_lastname.Text = "";
        txtbox_Email.Text = "";
        txtbox_coupannumber.Text="";
        txt_fax.Text = "";
        txtbox_address1.Text = "";
        txtbox_address2.Text = "";
        txtbox_balance.Text = "";
        txtbox_bankaccountno.Text = "";
       // ddl_Parkercity.SelectedIndex = 0;
        txtbox_cellular.Text = "";
        txtbox_postal.Text = "";
        txtbox_phone.Text = "";
        txtBox_username.Text = "";
        txtbox_bankname.Text = "";
        txtbox_bankzip.Text = "";
        txtbox_city_bank.Text = "";


    }


    protected void txtBox_username_TextChanged1(object sender, EventArgs e)
        {
        DataTable dt = new clsInsert().alreadyexist("usernamecheck",txtBox_username.Text.Trim());
       
        if (dt.Rows.Count > 0)
        {
            lbl_usercheck.Visible = true;
            lbl_usercheck.Text = "UserName already exist";
            txtBox_username.Text = "";
        }
        else
        {
            lbl_usercheck.Visible = false;
            txtBox_username.Text = txtBox_username.Text;
        }
    }
    protected void txtbox_Email_TextChanged(object sender, EventArgs e)
    {
        //DataTable dt = new clsInsert().alreadyexist("emailverify",txtbox_Email.Text.Trim());

        //if (dt.Rows.Count > 0)
        //{
        //    lbl_emailcheck.Visible = true;
        //    lbl_emailcheck.Text = "email already exist";
        //    txtbox_Email.Text = "";
        //}
        //else
        //{
        //    lbl_emailcheck.Visible = false;
        //    txtbox_Email.Text = txtbox_Email.Text;
        //}
    }
    protected void btn_finish_corporate_Click(object sender, EventArgs e)
    {
        try
        {



            bool b = obj.mail(txtbox_Email.Text, "User Name :" + txtBox_username.Text + "\nPassword :" + txtBox_Passwrd.Text, "Successfully Registerd on ipass" + System.DateTime.Now.ToString());
            if (b == true)
            {

                insertrec_corporate();
                clear();
              //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('registration successfully')", true);
              //  Session["registration"] = "True";
                Response.Redirect("default.aspx");
                

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Email Id not exist.')", true);

            }

           

        }
        catch
        {
            

        }

    }
    protected void rb_account_CheckedChanged(object sender, EventArgs e)
    {
        if (rb_account.Checked == true)
        {
            btn_addvehicledetails.Visible = true;
            btn_totalcharges.Visible = true;
           
        }
        else
        {
            btn_addvehicledetails.Visible = false;
            btn_totalcharges.Visible = false;
        }
    }
    protected void ddl_deliverymethod_corporate_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
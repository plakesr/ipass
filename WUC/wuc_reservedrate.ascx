﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_reservedrate.ascx.cs" Inherits="WUC_wuc_reservedrate" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 148px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Reserved Rate</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="style2">
                Amount :</td>
            <td>
                <asp:TextBox ID="txtbox_reservedamount" runat="server" ValidationGroup="a"  CssClass="twitterStyleTextbox"  onkeypress="return IsOneDecimalPoint(event);"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_reservedamount_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_reservedamount"  ValidChars="01234567890." >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Number &nbsp;&nbsp; :</td>
            <td>
                <asp:TextBox ID="txtbox_reservedquantity" runat="server" ValidationGroup="a" Height="22px"  CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtbox_reservedquantity_FilteredTextBoxExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_reservedquantity"  FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
                <asp:DropDownList ID="ddl_reserved" runat="server" ValidationGroup="a" 
                    DataSourceID="SqlDataSource1" DataTextField="chargebyname" 
                    DataValueField="chargebyid"  CssClass="twitterStyleTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                    SelectCommand="SELECT * FROM [tbl_Chargeby]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_reservedrate" runat="server" CssClass="button" 
                    onclick="btn_reservedrate_Click" Text="Add" />
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<asp:Panel ID="Panel2" runat="server">
<fieldset>
<asp:Repeater ID="rpt_reserved" runat="server">

<HeaderTemplate>
            <table width="100%" class="rounded-corner">
                <tr>
                 <th>
                        Paking Type ID
                    </th>
                      <th>
                       Amount
                    </th>
                    
                    <th>
                        Quantity
                    </th>
                    <th>
                      Charge Type
                    </th>
                    
                    
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("typeofparking")%>
            <td>
                    <%#Eval("Amount_reserved")%>
                    </td>
                <td>
                  <%#Eval("quantity_reserved")%>
                  </td>

                  <td>
                  <%#Eval("chargebytext_reserved")%>
                  </td>

                  <%--<td>
                  <%#Eval("VehicalLicensePlate")%>
                     
                    </td>
--%>
                <td>
                   <%-- <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("VehiColorid") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <%--<asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("VehiColorid") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>--%>
                    </td>
                   <%-- <asp:Label ID="idLabel" runat="server" Text='<%# Eval("VehiColorid") %>' 
                                        Visible="False" />
                    <asp:Label ID="idlabel1" runat="server" Text='<%# Eval("VehiColor") %>' 
                                        Visible="False"></asp:Label>--%>
                    
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
</fieldset>
</asp:Panel>

</ContentTemplate>
        
    </asp:UpdatePanel>
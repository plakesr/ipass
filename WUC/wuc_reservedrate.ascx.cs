﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WUC_wuc_reservedrate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["reservedparkingtype"].ToString();
            try
            {

                dt_reserved.Columns.Add("Amount_reserved", typeof(decimal));
                dt_reserved.Columns.Add("quantity_reserved", typeof(decimal));
                dt_reserved.Columns.Add("chargeby_reserved", typeof(int));
                dt_reserved.Columns.Add("typeofparking", typeof(int));
                dt_reserved.Columns.Add("chargebytext_reserved", typeof(string));



                Session["datatable_reserved"] = dt_reserved;

            }
            catch
            {

            }
        }
    }
    DataTable dt_reserved = new DataTable();
    protected void btn_reservedrate_Click(object sender, EventArgs e)
    {
        try
        {

            dt_reserved = (DataTable)Session["datatable_reserved"];
            DataRow dr_reserved = dt_reserved.NewRow();
            dr_reserved[0] = txtbox_reservedamount.Text;
            dr_reserved[1] = txtbox_reservedquantity.Text;
            dr_reserved[2] = Convert.ToInt32(ddl_reserved.SelectedItem.Value);
            dr_reserved[3] = Convert.ToInt32(Session["reservedparkingtype"].ToString());
            dr_reserved[4] = ddl_reserved.SelectedItem.Text;

            dt_reserved.Rows.Add(dr_reserved);

            Session["datatable_reserved"] = dt_reserved;
            if (dt_reserved.Rows.Count > 0)
            {
                rpt_reserved.DataSource = dt_reserved;
                rpt_reserved.DataBind();
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please fill  details.')", true);
        }
    }
}
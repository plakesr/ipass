﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_screen4.ascx.cs" Inherits="WUC_wuc_screen4" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 129px;
    }
    .style3
    {
        width: 149px;
    }
    .style4
    {
        width: 77px;
    }
    .style5
    {
        width: 151px;
    }
    .style6
    {
        width: 68px;
    }
</style>
<asp:Panel ID="Panel1" runat="server">
<fieldset>
<legend>
    Total Charges</legend>
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lbl_activationcharge" runat="server" 
        Text="Total Activation Charges"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lbl_totaldeliverycharge" runat="server" 
        Text="Total Delivery Charges"></asp:Label>
</fieldset>
</asp:Panel>

<br />
<asp:Panel ID="Panel2" runat="server">
<fieldset>
<legend>
    Add Old Card Detail</legend>
    <br />
    <table class="style1">
        <tr>
            <td class="style2">
                Credit Reference #</td>
            <td class="style3">
                <asp:TextBox ID="txtbox_creditreference" runat="server" ValidationGroup="a"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_creditreference" runat="server" 
                    ControlToValidate="txtbox_creditreference" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td class="style4">
                Old Card #</td>
            <td class="style5">
                <asp:TextBox ID="txtbox_oldcard" runat="server"></asp:TextBox>
            </td>
            <td class="style6">
                Balance</td>
            <td class="style5">
                <asp:TextBox ID="txtbox_balance" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_Add" runat="server" Text="Add" />
            </td>
        </tr>
    </table>
</fieldset>
</asp:Panel>

<br />
<asp:Panel ID="Panel3" runat="server">
<fieldset>
<legend>
    Old Card And Balances</legend>
    <asp:Repeater ID="rpt_oldcard" runat="server">
    </asp:Repeater>
    <br />
</fieldset>
    
</asp:Panel>

<br />
<asp:CheckBox ID="CheckBox1" runat="server" 
    
    Text="I confirm I have read and accept the terms &amp; conditions of service. I also accept the non-refundable fee detailed in the registration and the billing option selected." 
    ValidationGroup="a" />
    <br />

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn_add1" runat="server" Text="Add" onclick="Button1_Click" 
    ValidationGroup="a" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button ID="btn_pre" runat="server" Text="Previous" 
    onclick="Button1_Click" />
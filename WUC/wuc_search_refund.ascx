﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_search_refund.ascx.cs" Inherits="WUC_wuc_search_refund" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<link href="../stylesheet/CSS.css" rel="stylesheet" type="text/css" />
<script src="Extension.min.js" type="text/javascript"></script>
<link href="CSS.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .style5
    {
        width: 155px;
    }
    .style6
    {
        width: 158px;
    }
    .style7
    {
        width: 228px;
    }
    .style8
    {
        width: 170px;
    }
    .style9
    {
        width: 166px;
    }
    .style10
    {
        width: 60px;
    }
    .style11
    {
        width: 77px;
    }
</style>

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<div>
   <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
<table >
<tr>
<td class="style7">
    <asp:CheckBox ID="chk_CustomerName" runat="server" 
        oncheckedchanged="chk_CustomerName_CheckedChanged" Text="Customer Name" AutoPostBack="true" />
    </td>
<td class="style5">
    <asp:DropDownList ID="ddl_site" runat="server" style="width:194px;" AutoPostBack="True"  CssClass="twitterStyleTextbox"  DataTextField="SiteName" DataValueField="SiteId" 
        Enabled="False" >
    </asp:DropDownList>

    </td>
    <td class="style10">
        &nbsp;</td>
    <td class="style9">
        <asp:CheckBox ID="chk_CampusName" runat="server" AutoPostBack="true" 
            oncheckedchanged="chk_CampusName_CheckedChanged" Text="Campus Name" />
    </td>
    <td class="style8">
       <asp:DropDownList ID="ddl_campus" runat="server" style="width:194px;" AutoPostBack="True"  CssClass="twitterStyleTextbox" 
        DataTextField="Campus_Name" 
        DataValueField="CampusID" Enabled="False">
    </asp:DropDownList></td>
</tr>
<tr>
<td class="style7">
    <asp:CheckBox ID="chk_LotName" runat="server"  AutoPostBack="true"
        oncheckedchanged="chk_LotName_CheckedChanged" Text="Lot Name" />
    </td>
<td class="style5">
    <asp:DropDownList ID="ddl_lot" runat="server" style="width:194px;"   CssClass="twitterStyleTextbox"
        DataTextField="LotName" DataValueField="Lot_id" Enabled="False">
    </asp:DropDownList>
    </td>
    <td class="style10">
        &nbsp;</td>
    <td class="style9">
        <asp:CheckBox ID="chk_status" runat="server" AutoPostBack="true" 
            oncheckedchanged="chk_status_CheckedChanged" Text="Status" />
    </td>
    <td class="style8">
        <asp:DropDownList ID="ddl_status" runat="server" style="width:194px;" CssClass="twitterStyleTextbox" 
            Enabled="False">
            <asp:ListItem Value="1">Pending</asp:ListItem>
            <asp:ListItem Value="3">Approved</asp:ListItem>
            <asp:ListItem Value="4">Decline</asp:ListItem>
            <asp:ListItem Value="2">Processing</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="ddl_status1" runat="server" style="width:194px;" 
            CssClass="twitterStyleTextbox" Enabled="false" Visible="false">
            <asp:ListItem Value="2">Pending</asp:ListItem>
            <asp:ListItem Value="3">Approved</asp:ListItem>
            <asp:ListItem Value="4">Decline</asp:ListItem>
        </asp:DropDownList>
    </td>
</tr>
<tr>
<td class="style7">
    <asp:CheckBox ID="chk_name" runat="server" AutoPostBack="true" 
        oncheckedchanged="chk_name_CheckedChanged" Text="Parker Name" />
    </td>
<td class="style5">
    <asp:TextBox ID="txtbox_name" runat="server" Enabled="False" CssClass="twitterStyleTextbox" style=" width:170px;" ></asp:TextBox>
    </td>
    <td class="style10">
        &nbsp;</td>
    <td class="style9">
        <asp:CheckBox ID="chbk_refundtrackingid" runat="server" AutoPostBack="true" 
         Text="Refund Tracking ID" 
            oncheckedchanged="chbk_refundtrackingid_CheckedChanged" /></td>
    <td class="style8">
        <asp:TextBox ID="txtbox_refundtrackingid" runat="server" Enabled="False" CssClass="twitterStyleTextbox" style=" width:170px;"  ></asp:TextBox></td>
</tr>
<tr>
<td class="style7">
    <asp:CheckBox ID="chk_date" runat="server" Text="Date Of Submission" 
        oncheckedchanged="chk_date_CheckedChanged" AutoPostBack="true" />
    &nbsp;[From]&nbsp;</td>
<td>
   
    <asp:TextBox ID="txtbox_date" runat="server" CssClass="disable_future_dates" 
        Enabled="False" ontextchanged="txtbox_date_TextChanged" style=" width:182px; height:23px;   border: 1px solid #c4c4c4; font-size: 13px; padding: 4px 4px 4px 4px; border-radius: 4px;-moz-border-radius: 4px;-webkit-border-radius: 4px; box-shadow: 0px 0px 8px #d9d9d9;-moz-box-shadow: 0px 0px 8px #d9d9d9; -webkit-box-shadow: 0px 0px 8px #d9d9d9; margin-bottom: 0px; "></asp:TextBox>
    <cc1:CalendarExtender ID="txtbox_ActicationDate_CalendarExtender" 
        runat="server" DaysModeTitleFormat="yyyy,MMMM" Enabled="True" Format="d" 
        TargetControlID="txtbox_date">
    </cc1:CalendarExtender>
    </td>
    <td class="style10">
        &nbsp;</td>
    <td class="style9">
       
        &nbsp; [ To ]
      
    </td>
    <td class="style8">
      <asp:TextBox ID="txtbox_enddate" runat="server" CssClass="disable_future_dates" 
            Enabled="False" ontextchanged="txtbox_date_TextChanged" style=" width:182px; height:23px;   border: 1px solid #c4c4c4; font-size: 13px; padding: 4px 4px 4px 4px; border-radius: 4px;-moz-border-radius: 4px;-webkit-border-radius: 4px; box-shadow: 0px 0px 8px #d9d9d9;-moz-box-shadow: 0px 0px 8px #d9d9d9; -webkit-box-shadow: 0px 0px 8px #d9d9d9; margin-bottom: 0px; "></asp:TextBox>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
            DaysModeTitleFormat="yyyy,MMMM" Enabled="True" Format="d" 
            TargetControlID="txtbox_enddate">
        </cc1:CalendarExtender>
       </td>
</tr>
</table>
  </ContentTemplate>
                     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="chk_CustomerName" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="chk_CampusName" EventName="CheckedChanged" />
             <asp:AsyncPostBackTrigger ControlID="chk_LotName" EventName="CheckedChanged" />
                <asp:AsyncPostBackTrigger ControlID="chk_name" EventName="CheckedChanged" />
                   <asp:AsyncPostBackTrigger ControlID="chk_status" EventName="CheckedChanged" />
                      <asp:AsyncPostBackTrigger ControlID="chk_date" EventName="CheckedChanged" />
                </Triggers>
   </asp:UpdatePanel>

<table  class="form-table">
<tr>
<td class="style6">
               <asp:Label ID="Result" runat="server" Text="" Visible="false"></asp:Label></td>
<td class="style6">
               &nbsp;</td>
<td class="style11">
               &nbsp;</td>
<td>
    <asp:Button ID="btn_search" runat="server" CssClass="button" 
        onclick="btn_search_Click" Text="Search"  />
    </td>
<td>
    &nbsp;</td>
</tr>
</table>

    <%--<table>

<tr>
   
<td class="style1"> 
    <asp:RadioButton ID="rbtn_byname" runat="server" GroupName="l" 
        oncheckedchanged="rbtn_byname_CheckedChanged" Text="Search By Name"  AutoPostBack="true"/>
</td>
<td class="style3">
    <asp:RadioButton ID="rbtn_byticket" runat="server" GroupName="l"  AutoPostBack="true"
        oncheckedchanged="rbtn_byticket_CheckedChanged" 
        Text="Search By Ticket Number" />
</td>
<td class="style4">
    

</td>

<td class="style2">
    

   </td>

<td class="style2">
    

    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" CssClass="button" 
        onclick="btn_cancel_Click" Visible="false" />
    

</td>

</tr>
<tr>
<td class="style1"> 
    <asp:Label ID="lbltext" runat="server" Text="" Visible="false"></asp:Label>
    </td>
<td class="style3"><asp:TextBox ID="txtbox_search" runat="server"  CssClass="twitterStyleTextbox" Placeholder="Enter Keyword"
        Visible="False"></asp:TextBox>


   
 


</td>
<td class="style4">
  
</td>
<td class="style2">
    
        </td>
<td class="style2">
    <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button" 
        onclick="btn_search_Click" Visible="False" />

    </td>
</tr>

</table>--%>
    <asp:Panel ID="Panel1" runat="server">
    <fieldset>
        <asp:Repeater ID="Repeater1" runat="server"
            onitemcommand="Repeater1_ItemCommand">
           <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                 
               <%-- <th>
                        Machine Id
                    </th>--%>
                      <th>
                      Lot Name
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                    Amount Requested
                    </th>
                     <th>
                        Date of Submission
                    </th>
                   
                    <th>
                    Status
                    </th>
           
                    <th>
                     Detail
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
        <tr>
            
            <%--<td>
                    <%#Eval("DeviceMachineID")%>
                    
                </td>--%>
                <td>
                    <%#Eval("LotName")%>
                    
                </td>
                <td>
                    <%#Eval("F_name")%>  <%#Eval("L_name")%>
                    
                </td>

                <td>
                  $  <%#Eval("RefundAmountRequesting")%>
                    
                </td>
                  <td>
                    <%#Eval("DateofRequest")%>
                    
                </td>
                 <td>
                     <asp:Image ID="imgstatus" runat="server" />
                    <%--<asp:RadioButton ID="rbtn_panding" runat="server" GroupName="r" Enabled="False"  />--%>
                </td>
              <%--  <td>
                    
                    <asp:RadioButton ID="rbtn_processing" runat="server" GroupName="r" Enabled="False" />
                </td>
                <td>
                    
                    <asp:RadioButton ID="rbtn_approved" runat="server" GroupName="r" Enabled="False" />
                </td>
                <td>
                  <asp:RadioButton ID="rbtn_decline" runat="server" GroupName="r" Enabled="False"  />
                    </td>--%>
                    <td>
                    
                    <asp:Button ID="btn_detail" CommandName="detail" runat="server" Text="Detail"  CssClass="button" />
                </td>
                   
                    
            </tr>
             <asp:Label ID="id" runat="server" Text='<%# Eval("Refund_Customer_ID") %>' 
                                        Visible="False" />
             <asp:Label ID="REFUND_TRACKING_ID" runat="server" Text='<%# Eval("REFUND_TRACKING_ID") %>' 
                                        Visible="False" />
                                        
  <asp:Label ID="authorisationcode" runat="server" Text='<%# Eval("AuthorisationCode_CreditCard") %>' 
                                        Visible="False" />
                                        <asp:Label ID="amount" runat="server" Text='<%# Eval("RefundAmountRequesting") %>' 
                                        Visible="False" />
        </ItemTemplate>
            <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>





     <%--   <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
            SelectCommand="">
        </asp:SqlDataSource>--%>
    </fieldset>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
    <fieldset>
        <asp:Repeater ID="Repeater2" runat="server" 
            onitemcommand="Repeater2_ItemCommand">
          <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                
               <%-- <th>
                        Machine Id
                    </th>--%>
                      <th>
                      Lot Name
                    </th>
                    <th>
                        Name
                    </th>
                    
                     <th>
                    Amount Requested
                    
                    </th>
                     <th>
                        Date of Submission
                    </th>
                    
                 
                    <th>
                 Status
                    </th>
             <%--       <th>
           Processing
                    </th>
                    <th>
           Approved
                    </th>
                    <th>
           Decline
                    </th>--%>
                     <th>
                     Detail
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
          <tr>
           
           <%-- <td>
                    <%#Eval("DeviceMachineID")%>
                    
                </td>--%>
                <td>
                    <%#Eval("LotName")%>
                    
                </td>
                  <td>
                    <%#Eval("F_name")%>  <%#Eval("L_name")%>
                    
                </td>
                <td>
                   $ <%#Eval("RefundAmountRequesting")%>
                    
                </td>
              
                  <td>
                    <%#Eval("DateofRequest")%>
                    
                </td>
               
                
                       <td>
                           <asp:Image ID="imgstatus1" runat="server" />
                    <%--<asp:RadioButton ID="rbtn_panding1" runat="server" GroupName="r" Enabled="False" />--%>
                </td>
            <%--    <td>
                    
                    <asp:RadioButton ID="rbtn_processing1" runat="server" GroupName="r" Enabled="False" />
                </td>
                <td>
                    
                    <asp:RadioButton ID="rbtn_approved1" runat="server" GroupName="r" Enabled="False" />
                </td>
                <td>
                  <asp:RadioButton ID="rbtn_decline1" runat="server" GroupName="r" Enabled="False"  />
                    </td>--%>
                   
                     <td>
                    <asp:Label ID="id1" runat="server" Text='<%# Eval("Refund_Customer_ID") %>' 
                                        Visible="False" />
                                        <asp:Label ID="REFUND_TRACKING_ID1" runat="server" Text='<%# Eval("REFUND_TRACKING_ID") %>' 
                                        Visible="False" />
                                          <asp:Label ID="amount1" runat="server" Text='<%# Eval("RefundAmountRequesting") %>' 
                                        Visible="False" />

                                        <asp:Label ID="authorisationcode1" runat="server" Text='<%# Eval("AuthorisationCode_CreditCard") %>' 
                                        Visible="False" />
                    <asp:Button ID="btn_detail11" runat="server" Text="Detail" CommandName="Detail" CssClass="button" />
                </td>
            </tr>
        </ItemTemplate>
            <FooterTemplate>

            </table>
          <%--       <div id="dvNoRecords" runat="server" visible="false" style="padding:20px 20px; text-align:center; color:Red;">
        No records to display.
        </div>--%>
        </FooterTemplate>
        </asp:Repeater>
    </fieldset>
    </asp:Panel>
         
</div>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Globalization;
using System.Web.UI.HtmlControls;
public partial class WUC_wuc_search_refund : System.Web.UI.UserControl
{
    DataTable dtdetailsrefund = new DataTable();

    DataTable dt_search;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack == true)
        {
            try
            {
                if (Session["operatorid"].ToString() == "PM")
                {
                    ddl_status.Visible = true;
                    ddl_status1.Visible = false;
                }
                if (Session["operatorid"].ToString() == "PMO")
                {
                    ddl_status.Visible = true;
                    ddl_status1.Visible = false;
                }
                if (Session["operatorid"].ToString() == "Client Manager")
                {
                    ddl_status.Visible = false;
                    ddl_status1.Visible = true;
                }

                if (Session["operatorid"].ToString() == "Accounting")
                {
                    ddl_status.Visible = false;
                    ddl_status1.Visible = true;
                }
                if (Session["operatorid"].ToString() == "Data Center")
                {
                    ddl_status.Visible = false;
                    ddl_status1.Visible = true;
                }
                if (Session["operatorid"].ToString() == "PM" || Session["operatorid"].ToString() == "Client Manager")
                {
                    DataTable binddata_site = new clsInsert().binddata(Session["Login"].ToString());
                    DataTable binddata_campus = new clsInsert().binddata(Session["Login"].ToString());
                    DataTable binddata_lot = new clsInsert().binddata(Session["Login"].ToString());

                  
                    if (binddata_site.Rows.Count > 0)
                    {
                        //           DropDownList1.DataValueField = "Code";
                        //DropDownList1.DataTextField = "Name";
                        //DropDownList1.DataSource = countryQuery;
                        //DataBind()
                        RemoveDuplicateRows(binddata_site, "SiteId");
                        ddl_site.DataValueField = "SiteId";
                        ddl_site.DataTextField = "SiteName";
                        ddl_site.DataSource = binddata_site;
                        ddl_site.DataBind();

                        RemoveDuplicateRows(binddata_campus, "CampusID");

                        ddl_campus.DataValueField = "CampusID";
                        ddl_campus.DataTextField = "Campus_Name";
                        ddl_campus.DataSource = binddata_campus;
                        ddl_campus.DataBind();

                        RemoveDuplicateRows(binddata_lot, "lotid");

                        ddl_lot.DataValueField = "lotid";
                        ddl_lot.DataTextField = "LotName";
                        ddl_lot.DataSource = binddata_lot;
                        ddl_lot.DataBind();
                    }
                }
                else
                {


                    DataTable binddata_site = new clsInsert().allbind();
                    DataTable binddata_campus = new clsInsert().allbind();
                    DataTable binddata_lot = new clsInsert().allbind();

                    if (binddata_site.Rows.Count > 0)
                    {
                        //           DropDownList1.DataValueField = "Code";
                        //DropDownList1.DataTextField = "Name";
                        //DropDownList1.DataSource = countryQuery;
                        //DataBind()
                        RemoveDuplicateRows(binddata_site, "SiteId");
                        ddl_site.DataValueField = "SiteId";
                        ddl_site.DataTextField = "SiteName";
                        ddl_site.DataSource = binddata_site;
                        ddl_site.DataBind();

                        RemoveDuplicateRows(binddata_campus, "CampusID");

                        ddl_campus.DataValueField = "CampusID";
                        ddl_campus.DataTextField = "Campus_Name";
                        ddl_campus.DataSource = binddata_campus;
                        ddl_campus.DataBind();
                        
                        RemoveDuplicateRows(binddata_lot, "Lot_id");

                        ddl_lot.DataValueField = "Lot_id";
                        ddl_lot.DataTextField = "LotName";
                        ddl_lot.DataSource = binddata_lot;
                        ddl_lot.DataBind();
                    }

                }
                DataTable dv = new DataTable("ad");
                dv.Columns.Add("DeviceMachineID", typeof(string));
                dv.Columns.Add("LotName", typeof(string));
                dv.Columns.Add("F_name", typeof(string));
                dv.Columns.Add("L_name", typeof(string));
                dv.Columns.Add("RefundAmountRequesting", typeof(decimal));
                dv.Columns.Add("DateofRequest", typeof(DateTime));
                dv.Columns.Add("Refund_Customer_ID", typeof(int));
                dv.Columns.Add("REFUND_TRACKING_ID", typeof(int));
                dv.Columns.Add("requeststatus", typeof(int));
                dv.Columns.Add("AuthorisationCode_CreditCard", typeof(string));

                if (Session["operatorid"].ToString() == "PM")
                {
                    try
                    {
                        DataTable dtlotidforparker = new clsInsert().getlotsforPm(Session["Login"].ToString());
                        for (int i = 0; i < dtlotidforparker.Rows.Count; i++)
                        {
                            //  dtdetailsrefund = dtlotidforparker.Clone();
                            dtdetailsrefund = new clsInsert().getrefunddetailsforpmbylot(Convert.ToInt32(dtlotidforparker.Rows[i]["lotid"]));
                            if (dtdetailsrefund.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtdetailsrefund.Rows.Count; j++)
                                {
                                    DataRow dr = dv.NewRow();
                                    dr[0] = dtdetailsrefund.Rows[j]["DeviceMachineID"].ToString();
                                    dr[1] = dtdetailsrefund.Rows[j]["LotName"].ToString();
                                    dr[2] = dtdetailsrefund.Rows[j]["F_name"].ToString();
                                    dr[3] = dtdetailsrefund.Rows[j]["L_name"].ToString();
                                    dr[4] = dtdetailsrefund.Rows[j]["RefundAmountRequesting"].ToString();
                                    dr[5] = dtdetailsrefund.Rows[j]["DateofRequest"].ToString();
                                    dr[6] = dtdetailsrefund.Rows[j]["Refund_Customer_ID"].ToString();
                                    dr[7] = dtdetailsrefund.Rows[j]["REFUND_TRACKING_ID"].ToString();
                                    dr[8] = dtdetailsrefund.Rows[j]["requeststatus"].ToString();
                                    dr[9] = dtdetailsrefund.Rows[j]["AuthorisationCode_CreditCard"].ToString();
                                    
                                    dv.Rows.Add(dr);

                                      

                                }


                                Panel1.Visible = true;





                                Panel2.Visible = false;

                            }

                        }

                        if (dv.Rows.Count > 0)
                        {
                            Repeater1.DataSource = dv;
                            Repeater1.DataBind();

                            for (int i1 = 0; i1 < dv.Rows.Count; i1++)
                            {
                                Image img = Repeater1.Items[i1].FindControl("imgstatus") as Image;
                                if (dv.Rows[i1]["requeststatus"].ToString() == "1")
                                {
                                    img.ImageUrl = "~/images/pending.png";

                                }
                                if (dv.Rows[i1]["requeststatus"].ToString() == "2")
                                {
                                    img.ImageUrl = "~/images/processing.png";


                                }
                                if (dv.Rows[i1]["requeststatus"].ToString() == "3")
                                {
                                    img.ImageUrl = "~/images/approved.png";
                                }
                                if (dv.Rows[i1]["requeststatus"].ToString() == "4")
                                {
                                    img.ImageUrl = "~/images/decline.png";
                                }
                            }

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No New Refund Request Found.')", true);

                        }

                    }
                    catch
                    {
                    }
                    }
                    
                
                if (Session["operatorid"].ToString() == "Client Manager")
                {
                    try
                    {
                        DataTable dtlotidforparker = new clsInsert().getlotsforPm(Session["Login"].ToString());

                        for (int i = 0; i < dtlotidforparker.Rows.Count; i++)
                        {
                            //  dtdetailsrefund = dtlotidforparker.Clone();
                            dtdetailsrefund = new clsInsert().getdetailsforclientmanegerbylot(Convert.ToInt32(dtlotidforparker.Rows[i]["lotid"]));
                            if (dtdetailsrefund.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtdetailsrefund.Rows.Count; j++)
                                {
                                    DataRow dr = dv.NewRow();
                                    dr[0] = dtdetailsrefund.Rows[j]["DeviceMachineID"].ToString();
                                    dr[1] = dtdetailsrefund.Rows[j]["LotName"].ToString();
                                    dr[2] = dtdetailsrefund.Rows[j]["F_name"].ToString();
                                    dr[3] = dtdetailsrefund.Rows[j]["L_name"].ToString();
                                    dr[4] = dtdetailsrefund.Rows[j]["RefundAmountRequesting"].ToString();
                                    dr[5] = dtdetailsrefund.Rows[j]["DateofRequest"].ToString();
                                    dr[6] = dtdetailsrefund.Rows[j]["Refund_Customer_ID"].ToString();
                                    dr[7] = dtdetailsrefund.Rows[j]["REFUND_TRACKING_ID"].ToString();
                                    dr[8] = dtdetailsrefund.Rows[j]["requeststatus"].ToString();
                                    dr[9] = dtdetailsrefund.Rows[j]["AuthorisationCode_CreditCard"].ToString();

                                    dv.Rows.Add(dr);

                                }


                                Panel1.Visible = true;





                                Panel2.Visible = false;

                            }

                        }

                        //if (dv.Rows.Count > 0)
                        //{
                            Repeater1.DataSource = dv;
                            Repeater1.DataBind();


                            for (int i1 = 0; i1 < dv.Rows.Count; i1++)
                            {
                                Image img = Repeater1.Items[i1].FindControl("imgstatus") as Image;
                                if (dv.Rows[i1]["requeststatus"].ToString() == "1")
                                {
                                    img.ImageUrl = "~/images/pending.png";

                                }
                                if (dv.Rows[i1]["requeststatus"].ToString() == "2")
                                {
                                    img.ImageUrl = "~/images/pending.png";


                                }
                                if (dv.Rows[i1]["requeststatus"].ToString() == "3")
                                {
                                    img.ImageUrl = "~/images/approved.png";
                                }
                                if (dv.Rows[i1]["requeststatus"].ToString() == "4")
                                {
                                    img.ImageUrl = "~/images/decline.png";
                                }
                            }
                        //}
                        //else
                        //{

                        //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No New Refund Request Found.')", true);

                        //}
                       

                    }
                    catch
                    {
                    }
                    
                }
                if (Session["operatorid"].ToString() == "Accounting")
                {
                  dtdetailsrefund = new clsInsert().getdetailsforAccounting();
                    if (dtdetailsrefund.Rows.Count > 0)
                    {
                        Panel1.Visible = true;
                        Repeater1.DataSource = dtdetailsrefund;
                        Repeater1.DataBind();

                        for (int i = 0; i < dtdetailsrefund.Rows.Count; i++)
                        {
                            Image img = Repeater1.Items[i].FindControl("imgstatus") as Image;
                            //if (dtdetailsrefund.Rows[i]["Status_Paid"].ToString() == "")
                            //{
                            //    img.ImageUrl = "~/images/pending.png";

                            //}
                            if (dtdetailsrefund.Rows[i]["Status_Paid"].ToString() == "True")
                            {
                                img.ImageUrl = "~/images/approved.png";



                            }
                            if (dtdetailsrefund.Rows[i]["Status_Paid"].ToString() == "False")
                            {
                                img.ImageUrl = "~/images/pending.png";

                            }

                        }



                        Panel2.Visible = false;


                    }
                }
                if (Session["operatorid"].ToString() == "Data Center")
                {

                    dtdetailsrefund = new clsInsert().getdetailsforDatacenter();
                    if (dtdetailsrefund.Rows.Count > 0)
                    {
                        Panel1.Visible = true;
                        Repeater1.DataSource = dtdetailsrefund;
                        Repeater1.DataBind();

                        for (int i = 0; i < dtdetailsrefund.Rows.Count; i++)
                        {
                            Image img = Repeater1.Items[i].FindControl("imgstatus") as Image;
                            if (dtdetailsrefund.Rows[i]["DataCenter_Status"].ToString() == "")
                            {
                                img.ImageUrl = "~/images/pending.png";

                            }
                            if (dtdetailsrefund.Rows[i]["DataCenter_Status"].ToString() == "True")
                            {
                                img.ImageUrl = "~/images/approved.png";



                            }
                            if (dtdetailsrefund.Rows[i]["DataCenter_Status"].ToString() == "False")
                            {
                                img.ImageUrl = "~/images/decline.png";

                            }

                        }



                        Panel2.Visible = false;

                    }
                }

                if (Session["operatorid"].ToString() == "PMO")
                {

                    dtdetailsrefund = new clsInsert().getdetailsforpmo_pending();
                    if (dtdetailsrefund.Rows.Count > 0)
                    {
                        Panel1.Visible = true;
                        Repeater1.DataSource = dtdetailsrefund;
                        Repeater1.DataBind();

                        for (int i = 0; i < dtdetailsrefund.Rows.Count; i++)
                        {
                            Image img = Repeater1.Items[i].FindControl("imgstatus") as Image;
                            if (dtdetailsrefund.Rows[i]["DataCenter_Status"].ToString() == "")
                            {
                                img.ImageUrl = "~/images/pending.png";

                            }
                            if (dtdetailsrefund.Rows[i]["DataCenter_Status"].ToString() == "True")
                            {
                                img.ImageUrl = "~/images/approved.png";



                            }
                            if (dtdetailsrefund.Rows[i]["DataCenter_Status"].ToString() == "False")
                            {
                                img.ImageUrl = "~/images/decline.png";

                            }

                        }



                        Panel2.Visible = false;

                    }
                }
                
            }
            catch
            {
            }

            try
            {
                if( Session["SuccessFull"].ToString()== "true")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Succesfully Submited.')", true);
                    
                }
            }
                    catch
                    {

                    }
        }
    }
    DateTime date1,date2;
    string site, campus, lot, name, status, refundtrackingid_search;
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (chk_name.Checked == true)
        {
            name = txtbox_name.Text.TrimEnd();

        }
        else
        {
            name = "";

        }
        if (chbk_refundtrackingid.Checked == true)
        {
            refundtrackingid_search = txtbox_refundtrackingid.Text.TrimEnd();

        }
        else
        {
            refundtrackingid_search = "";

        }
        if (chk_LotName.Checked == true)
        {
            lot = ddl_lot.SelectedValue;

        }
        else
        {
            lot = "";
        }

        if (chk_CampusName.Checked == true)
        {
            campus = ddl_campus.SelectedValue;
        }
        else
        {
            campus = "";
        }

        if (chk_CustomerName.Checked == true)
        {
            site = ddl_site.SelectedValue;

        }
        else
        {
            site = "";
        }
        if (chk_status.Checked == true)
        {
            if (Session["operatorid"].ToString() == "PM")
            {
                status = ddl_status.SelectedValue;
            }
            if (Session["operatorid"].ToString() == "PMO")
            {
                status = ddl_status.SelectedValue;
            }
            if (Session["operatorid"].ToString() == "Client Manager")
            {
                status = ddl_status1.SelectedValue;

            }

            if (Session["operatorid"].ToString() == "Accounting")
            {
                status = ddl_status1.SelectedValue;

            }

            if (Session["operatorid"].ToString() == "Data Center")
            {
                status = ddl_status1.SelectedValue;

            }

        }
        else
        {
            if (Session["operatorid"].ToString() == "PM")
            {
                status = "";
            }
            if (Session["operatorid"].ToString() == "PMO")
            {
                status = "";
            }
            if (Session["operatorid"].ToString() == "Client Manager")
            {
                status = "";

            }

            if (Session["operatorid"].ToString() == "Accounting")
            {
                status = "";


            }

            if (Session["operatorid"].ToString() == "Data Center")
            {
                status = "";


            }
        }
        if (chk_date.Checked == true)
        {
            if (txtbox_enddate.Text == "")
            {
                DateTime d1 = DateTime.Parse(txtbox_date.Text);
                DateTime d2 = DateTime.Parse(txtbox_date.Text).AddDays(1);

                date1 = d1;
                date2 = d2;
            }

            if (txtbox_enddate.Text != "")
            {
                DateTime d1 = DateTime.Parse(txtbox_date.Text);
                DateTime d2 = DateTime.Parse(txtbox_enddate.Text);

                date1 = d1;
                date2 = d2;
            }
        }
        else
        {
           date1= DateTime.Parse("01/01/2001");
           date2 = System.DateTime.Now.AddDays (1);
        }
        if (Session["operatorid"].ToString() == "PM")
        {
            try
            {
                if (chk_status.Checked == true)
                {
                    if (ddl_status.SelectedValue == "1")
                    {
                        dt_search = new clsInsert().getrefundparker(name, lot, campus, site, Session["Login"].ToString(), status,date1,date2,refundtrackingid_search);

                    }

                    if (ddl_status.SelectedValue == "2")
                    {
                        dt_search = new clsInsert().getrefundparker1(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2,refundtrackingid_search);

                    } if (ddl_status.SelectedValue == "3")
                    {
                        dt_search = new clsInsert().getrefundparker1(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2,refundtrackingid_search);

                    } 
                    if (ddl_status.SelectedValue == "4")
                    {
                        dt_search = new clsInsert().getrefundparker1(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2,refundtrackingid_search);

                    }
                }
                else
                {
                    if (chk_CustomerName.Checked == false && chk_CampusName.Checked == false && chk_LotName.Checked == false && chk_name.Checked == false && chk_status.Checked == false && chk_date.Checked == false && chbk_refundtrackingid.Checked==true)
                    {
                        try
                        {
                            dt_search = new clsInsert().getrefundparkeronlyrefundtrackingid(Convert.ToInt32(refundtrackingid_search),Session["Login"].ToString());
                            
                        }
                        catch
                        {
                        }
                        
                    }
                    else
                    {
                        dt_search = new clsInsert().getrefundparker(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2, refundtrackingid_search);

                    }

                }
                    if (dt_search.Rows.Count > 0)
                    {
                        Panel2.Visible = true;
                        Panel1.Visible = false;
                        Repeater2.Visible = true;
                       
                        Repeater2.DataSource = dt_search;
                        Repeater2.DataBind();
                        for (int m = 0; m < dt_search.Rows.Count; m++)
                        {
                            Image img1 = Repeater2.Items[m].FindControl("imgstatus1") as Image;
                            if (dt_search.Rows[m]["requeststatus"].ToString() == "1")
                            {
                                img1.ImageUrl = "~/images/pending.png";

                            }
                            if (dt_search.Rows[m]["requeststatus"].ToString() == "2")
                            {
                                img1.ImageUrl = "~/images/processing.png";


                            }
                            if (dt_search.Rows[m]["requeststatus"].ToString() == "3")
                            {
                                img1.ImageUrl = "~/images/approved.png";
                            }
                            if (dt_search.Rows[m]["requeststatus"].ToString() == "4")
                            {
                                img1.ImageUrl = "~/images/decline.png";
                            }
                        }
                        Result.Visible = true;
                        Result.Text = dt_search.Rows.Count.ToString() + " " + "Records Found";



                    }
                    else
                    {

                        Result.Visible = false;
                        Repeater1.Visible = false;
                        Panel2.Visible = false;
                        // Repeater2.Visible = true;
                        
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found of this Match.')", true);


                    }
               




            }
            catch
            {
            }
        }





        if (Session["operatorid"].ToString() == "Client Manager")
        {
            try
            {
                if (chk_status.Checked == true)
                {
                    

                    if (ddl_status1.SelectedValue == "2")
                    {
                        dt_search = new clsInsert().getrefundparkercm(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status1.SelectedValue == "3")
                    {
                        dt_search = new clsInsert().getrefundparkercm1(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status1.SelectedValue == "4")
                    {
                        dt_search = new clsInsert().getrefundparkercm1(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2, refundtrackingid_search);

                    }
                }
                else
                {
                    if (chk_CustomerName.Checked == false && chk_CampusName.Checked == false && chk_LotName.Checked == false && chk_name.Checked == false && chk_status.Checked == false && chk_date.Checked == false && chbk_refundtrackingid.Checked == true)
                    {
                        try
                        {
                            dt_search = new clsInsert().getrefundparkeronlyrefundtrackingid(Convert.ToInt32(refundtrackingid_search), Session["Login"].ToString());

                        }
                        catch
                        {
                        }

                    }
                    else
                    {
                        dt_search = new clsInsert().getrefundparkercm(name, lot, campus, site, Session["Login"].ToString(), status, date1, date2, refundtrackingid_search);
                    }
                }
                if (dt_search.Rows.Count > 0)
                {
                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    Repeater2.Visible = true;
                    Repeater2.DataSource = dt_search;
                    Repeater2.DataBind();
                    for (int m = 0; m < dt_search.Rows.Count; m++)
                    {
                        Image img1 = Repeater2.Items[m].FindControl("imgstatus1") as Image;
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "1")
                        {
                            img1.ImageUrl = "~/images/pending.png";

                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "2")
                        {
                            img1.ImageUrl = "~/images/pending.png";


                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "3")
                        {
                            img1.ImageUrl = "~/images/approved.png";
                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "4")
                        {
                            img1.ImageUrl = "~/images/decline.png";
                        }
                    }
                    Result.Visible = true;
                    Result.Text = dt_search.Rows.Count.ToString() + " " + "Records Found";



                }
                else
                {

                    Result.Visible = false;
                    Repeater1.Visible = false;
                    Panel2.Visible = false;
                    // Repeater2.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found of this Match.')", true);

                }
               
            }
            catch
            {
            }
        }




        if (Session["operatorid"].ToString() == "Accounting")
        {
            try
            {
                if (chk_status.Checked == true)
                {


                    if (ddl_status1.SelectedValue == "2")
                    {
                        dt_search = new clsInsert().getrefundparkeraccounting(name, lot, campus, site, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status1.SelectedValue == "3")
                    {
                        dt_search = new clsInsert().getrefundparkeraccounting1(name, lot, campus, site, status, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status1.SelectedValue == "4")
                    {
                        dt_search = new clsInsert().getrefundparkeraccounting2(name, lot, campus, site, status, date1, date2, refundtrackingid_search);

                    }
                }
                else
                {

                    dt_search = new clsInsert().getrefundparkeraccounting(name, lot, campus, site, date1, date2, refundtrackingid_search);

                }
                if (dt_search.Rows.Count > 0)
                {
                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    Repeater2.Visible = true;
                    Repeater2.DataSource = dt_search;
                    Repeater2.DataBind();
                    for (int m = 0; m < dt_search.Rows.Count; m++)
                    {
                        Image img1 = Repeater2.Items[m].FindControl("imgstatus1") as Image;
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "1")
                        {
                            img1.ImageUrl = "~/images/pending.png";

                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "2")
                        {
                            img1.ImageUrl = "~/images/pending.png";


                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "3")
                        {

                            if (dt_search.Rows[m]["Status_Paid"].ToString() == "True")
                            {
                                img1.ImageUrl = "~/images/approved.png";
                            }

                            if (dt_search.Rows[m]["Status_Paid"].ToString() == "False")
                            {
                                img1.ImageUrl = "~/images/pending.png";
                            }
                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "4")
                        {
                            img1.ImageUrl = "~/images/decline.png";
                        }
                    }
                    Result.Visible = true;
                    Result.Text = dt_search.Rows.Count.ToString() + " " + "Records Found";



                }
                else
                {

                    Result.Visible = false;
                    Repeater1.Visible = false;
                    Panel2.Visible = false;
                    // Repeater2.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found of this Match.')", true);

                }

            }
            catch
            {
            }
        }




        if (Session["operatorid"].ToString() == "Data Center")
        {
            try
            {
                if (chk_status.Checked == true)
                {


                    if (ddl_status1.SelectedValue == "2")
                    {
                        dt_search = new clsInsert().getrefundparkerdatacenter(name, lot, campus, site, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status1.SelectedValue == "3")
                    {
                        dt_search = new clsInsert().getrefundparkerdatacenter1(name, lot, campus, site, status, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status1.SelectedValue == "4")
                    {
                        dt_search = new clsInsert().getrefundparkerdatacenter2(name, lot, campus, site, status, date1, date2, refundtrackingid_search);

                    }
                }
                else
                {

                    dt_search = new clsInsert().getrefundparkerdatacenter(name, lot, campus, site, date1, date2, refundtrackingid_search);

                }
                if (dt_search.Rows.Count > 0)
                {
                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    Repeater2.Visible = true;
                    Repeater2.DataSource = dt_search;
                    Repeater2.DataBind();
                    for (int m = 0; m < dt_search.Rows.Count; m++)
                    {
                        Image img1 = Repeater2.Items[m].FindControl("imgstatus1") as Image;
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "1")
                        {
                            img1.ImageUrl = "~/images/pending.png";

                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "2")
                        {
                            img1.ImageUrl = "~/images/pending.png";


                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "3")
                        {
                            if (dt_search.Rows[m]["Status_Paid"].ToString() == "True")
                            {
                                img1.ImageUrl = "~/images/approved.png";
                            }

                            if (dt_search.Rows[m]["Status_Paid"].ToString() == "False")
                            {
                                img1.ImageUrl = "~/images/pending.png";
                            }
                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "4")
                        {
                            img1.ImageUrl = "~/images/decline.png";
                        }
                    }
                    Result.Visible = true;
                    Result.Text = dt_search.Rows.Count.ToString() + " " + "Records Found";



                }
                else
                {

                    Result.Visible = false;
                    Repeater1.Visible = false;
                    Panel2.Visible = false;
                    // Repeater2.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found of this Match.')", true);

                }

            }
            catch
            {
            }
        }




        if (Session["operatorid"].ToString() == "PMO")
        {
            try
            {
                if (chk_status.Checked == true)
                {
                    if (ddl_status.SelectedValue == "1")
                    {
                        dt_search = new clsInsert().getrefundparker_pmo(name, lot, campus, site, status, date1, date2, refundtrackingid_search);

                    }

                    if (ddl_status.SelectedValue == "2")
                    {
                        dt_search = new clsInsert().getrefundparker_pmo1(name, lot, campus, site,  status, date1, date2, refundtrackingid_search);

                    } if (ddl_status.SelectedValue == "3")
                    {
                        dt_search = new clsInsert().getrefundparker_pmo1(name, lot, campus, site,  status, date1, date2, refundtrackingid_search);

                    }
                    if (ddl_status.SelectedValue == "4")
                    {
                        dt_search = new clsInsert().getrefundparker_pmo1(name, lot, campus, site,  status, date1, date2, refundtrackingid_search);

                    }
                }
                else
                {
                    if (chk_CustomerName.Checked == false && chk_CampusName.Checked == false && chk_LotName.Checked == false && chk_name.Checked == false && chk_status.Checked == false && chk_date.Checked == false && chbk_refundtrackingid.Checked == true)
                    {
                        try
                        {
                            dt_search = new clsInsert().getrefundparkeronlyrefundtrackingidbypmo(Convert.ToInt32(refundtrackingid_search));

                        }
                        catch
                        {
                        }

                    }
                    else
                    {
                        dt_search = new clsInsert().getrefundparker_pmo(name, lot, campus, site, status, date1, date2, refundtrackingid_search);

                    }

                }
                if (dt_search.Rows.Count > 0)
                {
                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    Repeater2.Visible = true;

                    Repeater2.DataSource = dt_search;
                    Repeater2.DataBind();
                    for (int m = 0; m < dt_search.Rows.Count; m++)
                    {
                        Image img1 = Repeater2.Items[m].FindControl("imgstatus1") as Image;
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "1")
                        {
                            img1.ImageUrl = "~/images/pending.png";

                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "2")
                        {
                            img1.ImageUrl = "~/images/processing.png";


                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "3")
                        {
                            img1.ImageUrl = "~/images/approved.png";
                        }
                        if (dt_search.Rows[m]["requeststatus"].ToString() == "4")
                        {
                            img1.ImageUrl = "~/images/decline.png";
                        }
                    }
                    Result.Visible = true;
                    Result.Text = dt_search.Rows.Count.ToString() + " " + "Records Found";



                }
                else
                {

                    Result.Visible = false;
                    Repeater1.Visible = false;
                    Panel2.Visible = false;
                    // Repeater2.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('No Record Found of this Match.')", true);


                }





            }
            catch
            {
            }
        }














    }
    


    //public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    //{
    //    Hashtable hTable = new Hashtable();
    //    ArrayList duplicateList = new ArrayList();
    //    foreach (DataRow dtRow in dTable.Rows)
    //    {
    //        if (hTable.Contains(dtRow[colName]))
    //            duplicateList.Add(dtRow);
    //        else
    //            hTable.Add(dtRow[colName], string.Empty);
    //    }
    //    foreach (DataRow dtRow in duplicateList)
    //        dTable.Rows.Remove(dtRow);
    //    return dTable;
    //}
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
         int i = e.Item.ItemIndex;
         if (e.CommandName == "detail")
         {
             Label l = Repeater1.Items[i].FindControl("id") as Label;
             Label l1 = Repeater1.Items[i].FindControl("REFUND_TRACKING_ID") as Label;
             Label amount = Repeater1.Items[i].FindControl("amount") as Label;
            
             Session["refund_customerid"] = l.Text;
             Session["REFUND_TRACKING_ID"] = l1.Text;
             Session["REFUND_Amount"] = amount.Text;
          
             
             if (Session["operatorid"].ToString() == "PM")
             {
                 DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                 if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "")
                 {

                     Response.Redirect("Operator_DetailOfRefundParker.aspx");

                 }
                 if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "True")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);


                 }
                 if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "False")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);


                 }
             }

             if (Session["operatorid"].ToString() == "Client Manager")
             {
                 DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                 if (dtredirectcheck.Rows[0]["ClientManager_Status"].ToString() == "")
                 {

                     Response.Redirect("Operator_DetailOfRefundParker.aspx");

                 }
                 if (dtredirectcheck.Rows[0]["ClientManager_Status"].ToString() == "True")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);


                 }
                 if (dtredirectcheck.Rows[0]["ClientManager_Status"].ToString() == "False")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);


                 }
             }
             if (Session["operatorid"].ToString() == "Accounting")
             {
                

                 Response.Redirect("Backoffice_Payment.aspx");
             }
             if (Session["operatorid"].ToString() == "Data Center")
             {
                 Label authorisationcode = Repeater1.Items[i].FindControl("authorisationcode") as Label;
                 Session["authorisationcode"] = authorisationcode.Text;

                 Response.Redirect("Backoffice_Payment.aspx");

             }
             if (Session["operatorid"].ToString() == "PMO")
             {
                 DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                 if (dtredirectcheck.Rows[0]["DataCenter_Status"].ToString() == "")
                 {
                     Session["approval"] = "Pending_PMO";
                     Response.Redirect("Operator_DetailOfRefundParker.aspx");

                 }
                 if (dtredirectcheck.Rows[0]["DataCenter_Status"].ToString() == "True")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);


                 }
                 if (dtredirectcheck.Rows[0]["DataCenter_Status"].ToString() == "False")
                 {
                     ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);


                 }

             }

         }
    }
    protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "Detail")
        {
            Label l = Repeater2.Items[i].FindControl("id1") as Label;
            Label l1 = Repeater2.Items[i].FindControl("REFUND_TRACKING_ID1") as Label;
            Label amount = Repeater2.Items[i].FindControl("amount1") as Label;

            Session["refund_customerid"] = l.Text;
            Session["REFUND_TRACKING_ID"] = l1.Text;
            Session["REFUND_Amount"] = amount.Text;

            if (Session["operatorid"].ToString() == "PM")
            {
                DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "")
                {

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");

                }
                if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "True")
                {
                  //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);

                    Session["approval"] = "true";
                      
                    Response.Redirect("Operator_DetailOfRefundParker.aspx");

                }
                if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "False")
                {
                    Session["approval"] = "false";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                  //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);


                }
            }

            if (Session["operatorid"].ToString() == "Client Manager")
            {
                DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                if (dtredirectcheck.Rows[0]["ClientManager_Status"].ToString() == "")
                {

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");

                }
                if (dtredirectcheck.Rows[0]["ClientManager_Status"].ToString() == "True")
                {
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);

                    Session["approval"] = "true";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                }
                if (dtredirectcheck.Rows[0]["ClientManager_Status"].ToString() == "False")
                {
                    Session["approval"] = "false";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                   // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);


                }
            }
            if (Session["operatorid"].ToString() == "Accounting")
            {

                DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                if (dtredirectcheck.Rows[0]["Accounting_status"].ToString() == "")
                {

                    Response.Redirect("Backoffice_Payment.aspx");

                }
                if (dtredirectcheck.Rows[0]["Accounting_status"].ToString() == "True")
                {
                   // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);
                    Session["approval"] = "true";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");

                }
                if (dtredirectcheck.Rows[0]["Accounting_status"].ToString() == "False")
                {
                   // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);

                    Session["approval"] = "false";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                }
               
            }
            if (Session["operatorid"].ToString() == "Data Center")
            {
               


                DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                if (dtredirectcheck.Rows[0]["DataCenter_Status"].ToString() == "")
                {
                    Label authorisationcode = Repeater2.Items[i].FindControl("authorisationcode") as Label;
                    Session["authorisationcode"] = authorisationcode.Text;
                    Response.Redirect("Backoffice_Payment.aspx");

                }
                if (dtredirectcheck.Rows[0]["DataCenter_Status"].ToString() == "True")
                {
                  //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);

                    Session["approval"] = "true";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                }
                if (dtredirectcheck.Rows[0]["DataCenter_Status"].ToString() == "False")
                {
                   // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);

                    Session["approval"] = "false";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                }


               // Response.Redirect("Backoffice_Payment.aspx");

            }




            if (Session["operatorid"].ToString() == "PMO")
            {
                DataTable dtredirectcheck = new clsInsert().getstatus_redirect(Convert.ToInt32(Session["REFUND_TRACKING_ID"].ToString()));

                if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "")
                {
                    Session["approval"] = "Pending_PMO";
                    Response.Redirect("Operator_DetailOfRefundParker.aspx");

                }
                if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "True")
                {
                    //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Approved.')", true);

                    Session["approval"] = "true";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");

                }
                if (dtredirectcheck.Rows[0]["PM_Status"].ToString() == "False")
                {
                    Session["approval"] = "false";

                    Response.Redirect("Operator_DetailOfRefundParker.aspx");
                    //  ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Declined.')", true);


                }
            }






        }
        //if (Repeater2.Items.Count < 1)
        //{
        //    if (e.Item.ItemType == ListItemType.Footer)
        //    {
        //        HtmlGenericControl dvNoRec = e.Item.FindControl("dvNoRecords") as HtmlGenericControl;
        //        if (dvNoRec != null)
        //        {
        //            dvNoRec.Visible = true;
        //        }
        //    }
        //}
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Operator_Search_RefundableByParker.aspx");
      //  btn_cancel.Visible = false;
    }
  
    protected void chk_CustomerName_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_CustomerName.Checked == true)
        {
            ddl_site.Enabled = true;
        }
        else
        {
            ddl_site.Enabled = false;
        }
    }
    protected void chk_CampusName_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_CampusName.Checked == true)
        {
            ddl_campus.Enabled = true;
        }
        else
        {
            ddl_campus.Enabled = false;
        }
    }

    protected void chk_LotName_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_LotName.Checked == true)
        {
            ddl_lot.Enabled = true;
        }
        else
        {
            ddl_lot.Enabled = false;
        }
    }
    protected void chk_name_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_name.Checked == true)
        {
            txtbox_name.Enabled = true;
        }
        else
        {
            txtbox_name.Text = "";
            txtbox_name.Enabled = false;
        }
    }
    protected void chk_date_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_date.Checked == true)
        {
            txtbox_date.Enabled = true;
            txtbox_enddate.Enabled = true;
        }
        else
        {
            txtbox_date.Text = "";
            txtbox_enddate.Text = "";
            txtbox_enddate.Enabled = false;
            txtbox_date.Enabled = false;
        }
    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();
        foreach (DataRow dtRow in dTable.Rows)
        {
            if (hTable.Contains(dtRow[colName]))
                duplicateList.Add(dtRow);
            else
                hTable.Add(dtRow[colName], string.Empty);
        }
        foreach (DataRow dtRow in duplicateList)
            dTable.Rows.Remove(dtRow);
        return dTable;
    }
    protected void chk_status_CheckedChanged(object sender, EventArgs e)
    {
       

        if (chk_status.Checked == true)
        {
                if (Session["operatorid"].ToString() == "PM")
                {

                ddl_status.Enabled = true;
                ddl_status1.Enabled = false;
                }
                if (Session["operatorid"].ToString() == "PMO")
                {

                    ddl_status.Enabled = true;
                    ddl_status1.Enabled = false;
                }
                if (Session["operatorid"].ToString() == "Client Manager")
                {
                    ddl_status.Enabled = false;
                    ddl_status1.Enabled = true;
                }
                if (Session["operatorid"].ToString() == "Accounting")
                {

                    ddl_status.Enabled = false;
                    ddl_status1.Enabled = true;
                }
                if (Session["operatorid"].ToString() == "Data Center")
                {
                    ddl_status.Visible = false;
                    ddl_status1.Visible = true;

                    ddl_status.Enabled = false;
                    ddl_status1.Enabled = true;
                    
                }
        }
        else
        {
            if (Session["operatorid"].ToString() == "PM")
            {

                ddl_status.Enabled = false;
                ddl_status1.Enabled = false;
            }

            if (Session["operatorid"].ToString() == "PMO")
            {

                ddl_status.Enabled = false;
                ddl_status1.Enabled = false;
            }
            if (Session["operatorid"].ToString() == "Client Manager")
            {
                ddl_status.Enabled = false;
                ddl_status1.Enabled = false;
            }



            if (Session["operatorid"].ToString() == "Accounting")
            {

                ddl_status.Enabled = false;
                ddl_status1.Enabled = false;
            }
            if (Session["operatorid"].ToString() == "Data Center")
            {
              

                ddl_status.Enabled = false;
                ddl_status1.Enabled = false;

            }
        }
    }

    protected void txtbox_date_TextChanged(object sender, EventArgs e)
    {

    }
    protected void chbk_refundtrackingid_CheckedChanged(object sender, EventArgs e)
    {
        if (chbk_refundtrackingid.Checked == true)
        {
            txtbox_refundtrackingid.Enabled = true;
           
        }
        else
        {
            txtbox_refundtrackingid.Text = "";
           
            txtbox_refundtrackingid.Enabled = false;
           
        }
    }
}
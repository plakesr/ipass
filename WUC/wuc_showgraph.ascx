﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_showgraph.ascx.cs" Inherits="WUC_wuc_showgraph" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Chart ID="Chart1" runat="server" Height="466px" 
    Palette="Pastel" Width="918px">
    <Series>
        <asp:Series Name="Series1" XValueMember="modifiedby" 
            YValueMembers="totalparker" ChartType="StackedColumn" >
        </asp:Series>
    </Series>
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1">
        <AxisY Title="Number Of Parkers"></AxisY><AxisX Title="Operator's Name"></AxisX></asp:ChartArea>
    </ChartAreas>
</asp:Chart>



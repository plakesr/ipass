﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_sitelotdetails.ascx.cs" Inherits="WUC_wuc_sitelotdetails" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"> </script>
<script type="text/javascript" src="js/script.js"></script>
<style type="text/css">
#backgroundPopup { 
	z-index:1;
	position: fixed;
	display:none;
	height:100%;
	width:100%;
	background:#000000;	
	top:0px;  
	left:0px;
}
.toPopup {
	font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
    background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #ccc;
    border-radius: 3px 3px 3px 3px;
    color: #333333;
    display: none;
	font-size: 14px;
    left: 50%;
    margin-left: -402px;
    position: fixed;
    top: 20%;
    width: 800px;
    z-index: 2;
}
div.loader {
    background: url("../img/loading.gif") no-repeat scroll 0 0 transparent;
    height: 32px;
    width: 32px;
	display: none;
	z-index: 9999;
	top: 40%;
	left: 50%;
	position: absolute;
	margin-left: -10px;
}
div.close {
    background: url("../img/closebox.png") no-repeat scroll 0 0 transparent;
    bottom: 24px;
    cursor: pointer;
    float: right;
    height: 30px;
    left: 27px;
    position: relative;
    width: 30px;
}
span.ecs_tooltip {
    background: none repeat scroll 0 0 #000000;
    border-radius: 2px 2px 2px 2px;
    color: #FFFFFF;
    display: none;
    font-size: 11px;
    height: 16px;
    opacity: 0.7;
    padding: 4px 3px 2px 5px;
    position: absolute;
    right: -62px;
    text-align: center;
    top: -51px;
    width: 93px;
}
span.arrow {
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 7px solid #000000;
    display: block;
    height: 1px;
    left: 40px;
    position: relative;
    top: 3px;
    width: 1px;
}
div#popup_content {
    margin: 4px 7px;
}
</style>
<asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Vertical">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Repeater ID="rptsite" runat="server">


            <HeaderTemplate>
            	<a href="#" class="topopup">Click Here Trigger</a>
    
    <div class="toPopup"> 
    	
        <div class="close"></div>
       	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
		<div id="popup_content"> <!--your content start-->
            <p>
            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, 
            feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi 
            vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, 
            commodo Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique 
            senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, 
            feugiat vitae, ultricies eget, tempor sit amet, ante. </p>
            <br />
            <p>
            Donec eu libero sit amet quam egestas semper. Aenean ultricies mi 
            vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, 
            commodo Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
            <p align="center"><a href="#" class="livebox">Click Here Trigger</a></p>
        </div> <!--your content end-->
    
    </div> <!--toPopup end-->
    
	<div class="loader"></div>
   	<div id="backgroundPopup"></div>

            <table width="100%" class="rounded-corner">
                <tr>
                      <th>
                        Lot Name
                    </th>
                     <th>
                      Lot Entry
                    </th>
                    <th>
                        Space Available
                    </th>
                    <%--<th>
                        Site Detail
                    </th> 
                    <th>
                        Tariff Detail
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            
            <td>
                <asp:Label ID="Label1" runat="server" Text='<%#Eval("LotName")%>' Visible="False"></asp:Label>     <%#Eval("LotName")%>
                    </td>
                    
                    <td>
                      <%--  <asp:TextBox ID="txtbox_rateforvehicle" runat="server" Text='<%#Eval("Address1")%>'></asp:TextBox>--%><%#Eval("EntryGateInformation")%>
                </td>
                
                <td>
                    <%#Eval("TotalSpaces") %>
                    
                </td>
                <td>
                
               <%-- <asp:LinkButton ID="lnkEdit" CommandName="cmdSiteDetails" CommandArgument='<%#Eval("SiteId") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                </td>
                <td>
                 <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("SiteId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>--%>

                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>



            </asp:Repeater>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
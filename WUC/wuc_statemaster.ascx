﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_statemaster.ascx.cs" Inherits="WUC_wuc_statemaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
    .style2
    {
        width: 181px;
    }
    .form-table
    {
        width: 66%;
    }
    .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
    <table class="form-table">
    <tr>
    <td>Country
    </td>
    <td style="width:261px;">

        <asp:DropDownList ID="ddl_country" runat="server" 
            DataSourceID="SqlDataSource1" DataTextField="Country_name" 
            DataValueField="Country_id" CssClass="twitterStyleTextbox" 
            AutoPostBack="true" 
            onselectedindexchanged="ddl_country_SelectedIndexChanged" 
            AppendDataBoundItems="true" >
              <asp:ListItem Selected = "True" Text = "---------Select Country---------" Value = "0"></asp:ListItem>
        </asp:DropDownList>
        <asp:Image ID="imgflag" runat="server" />
       
       

    </td>
    <td>
    
    </td>
    </tr>
        <tr>
            <td class="style2">
                Province Name</td>
            <td>
                <asp:TextBox ID="txtbox_StateName" runat="server" ValidationGroup="a" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_StateName_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_StateName" WatermarkCssClass="watermark" WatermarkText="Enter Province Name">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_statename" runat="server" 
                    ControlToValidate="txtbox_StateName" ErrorMessage="*" 
                    ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
          

            <td class="style2">
                Province Abbreviation</td>
            <td>
                <asp:TextBox ID="txtbox_StateAbbrv" runat="server" 
                    CssClass="twitterStyleTextbox" ValidationGroup="a"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_StateAbbrv_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txtbox_StateAbbrv" WatermarkCssClass="watermark" WatermarkText="Enter Province Abbreviation">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_stateabbr" runat="server" 
                    ControlToValidate="txtbox_StateAbbrv" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
        </tr>
       
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_submit" runat="server" Text="Add"
                    onclick="btn_submit_Click1" ValidationGroup="a" Height="35px" Width="79px" CssClass="button" />
            </td>
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server"  Height="300px" ScrollBars="Vertical">
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="select tbl_country.*,tbl_Province.* from tbl_Province
inner join tbl_country on tbl_Province.Country_id=tbl_country.Country_id">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString2 %>" 
                SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Province&nbsp;&nbsp;Name
                    </th>
                     <th>
                        Province&nbsp;&nbsp; Abbreviation
                    </th>
                    <th>
                        Country&nbsp;&nbsp;Name 
                    </th>
                    <th>
                       Flag
                    </th>
                    <th>
                        Edit
                    </th>
                  <%--  <th>
                      Delete
                    </th>--%>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("ProvinceName")%>
                    </td><td>
                    <%#Eval("ProvinceAbbreviation")%>
                </td>
                <td>
                <%#Eval("Country_name")%>
                </td>
                <td>
                 <asp:Image ID="Image1" runat="server" ImageUrl='<%#"~/FlagImage/Resized/"+Eval("Country_flag") %>' />
                </td>
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("ProvinceId") %>' runat="server"><img src="images/edit.png" /> </asp:LinkButton>
                    
                </td>
                <%--<td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("ProvinceId") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" /> </asp:LinkButton>
                    </td>--%>

                   
  <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("ProvinceName") %>' 
                                        Visible="False"></asp:Label>
<asp:Label ID="idLabel2" runat="server" Text='<%# Eval("ProvinceAbbreviation") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="id" runat="server" Text='<%# Eval("ProvinceId") %>' 
                                        Visible="False"></asp:Label>   
       
            <asp:Label ID="country_id" runat="server" Text='<%# Eval("Country_id") %>' 
                                        Visible="False"></asp:Label>                                   
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>

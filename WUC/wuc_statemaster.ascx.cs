﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_statemaster : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            DataTable dt = new clsInsert().getcuntryflag(Convert.ToInt32(2));

            if (dt.Rows.Count > 0)
            {

                imgflag.Height = 20;
                imgflag.Width = 30;
                imgflag.ImageUrl = "../FlagImage/Resized/" + dt.Rows[0]["Country_flag"].ToString().Trim();

            }

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }


            try
            {

                if (Session["lbladd"].ToString() == "True")
                {
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                }



            }
            catch
            {
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        // Response.Redirect("Deshboard.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        Response.Redirect("Operator_deshboard.aspx");

                    }
                }
                catch
                {
                    //  Response.Redirect("default.aspx");
                }
            }
        }
    }
    string msg = "";
    public void addState()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
           
            hst.Add("stateabbr", txtbox_StateAbbrv.Text);
            hst.Add("statename", txtbox_StateName.Text);
            hst.Add("country_id", Convert.ToInt32( ddl_country.SelectedItem.Value) );
            int result = objData.ExecuteNonQuery("[sp_State]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_StateMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_ProvinceMaster.aspx?message=" + msg);

                }

              

            }
            else
            {
                // lbl_error.Visible = true;

            }
        }
        catch
        {
            //lbl_error.Visible = true;

        }
    }


    public void clear()
    {
        txtbox_StateAbbrv.Text = "";
      
        txtbox_StateName.Text = "";
    }
    protected void btn_submit_Click1(object sender, EventArgs e)
    {
        try
        {
            
            addState();
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            int i = e.Item.ItemIndex;
            if (e.CommandName == "cmdDelete")
            {
                int ID = Convert.ToInt32(e.CommandArgument.ToString());
                try
                {
                    if (Session["Type"].ToString() == "admin")
                    {
                        delstate(ID);
                        Response.Redirect("Admin_StateMaster.aspx");
                    }
                    if (Session["Type"].ToString() == "operator")
                    {
                        if (Session["lbldelete"].ToString() == "False")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                        }

                        else
                        {
                            delstate(ID);
                            Response.Redirect("Operator_ProvinceMaster.aspx");
                        }
                    }

                }
                catch
                {
                    // Response.Redirect("default.aspx");
                }

            }

            if (e.CommandName == "cmdEdit")
            {

                Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

                Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
                Label l3 = rpt1.Items[i].FindControl("id") as Label;
                Label l4 = rpt1.Items[i].FindControl("country_id") as Label;


                try
                {

                    Session["StateName"] = l1.Text;
                    Session["StateAbbreviation"] = l2.Text;
                    Session["StateId"] = l3.Text;
                    Session["country_id"] = l4.Text;
                }
                catch
                {
                }
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_EditState.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbledit"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                    }
                    else
                    {
                        Response.Redirect("Operator_EditProvinceMaster.aspx");
                    }

                }


            }
        }
        catch
        {

        }
    }


    public void delstate(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);
            int id1 = id;
            hst.Add("city_provinceid", id1);

            int result = objData.ExecuteNonQuery("[sp_State]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
               // Response.Redirect("Admin_StateMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

                //Response.Redirect("Admin_StateMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

           //Response.Redirect("Admin_StateMaster.aspx");

        }


    }
    protected void ddl_country_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new clsInsert().getcuntryflag(Convert.ToInt32(ddl_country.SelectedItem.Value));

        if (dt.Rows.Count > 0)
        {

            imgflag.Height = 20;
            imgflag.Width = 30;
            imgflag.ImageUrl = "../FlagImage/Resized/" + dt.Rows[0]["Country_flag"].ToString().Trim();

        }
    }
}
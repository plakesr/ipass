﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;

public partial class WUC_wuc_tariff_plan : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ////btn_userdetails.CssClass = "Clicked";
            ////MainView.ActiveViewIndex = 0;
            this.PopulateMenus();

            //DataTable dt = new DataTable();
            //dt.Columns.Add("NAME", typeof(string));
            //dt.Columns.Add("NAME", typeof(string));
            //dt.Columns.Add("NAME", typeof(string));
            //dt.Columns.Add("NAME", typeof(string));
            //dt.Columns.Add("NAME", typeof(string));
            //dt.Columns.Add("NAME", typeof(string));

            //DataRow dr = dt.NewRow();
            //dr[0] = "";
            //dr[0] = "";
            //dr[0] = "";
            //dr[0] = "";
            //dr[0] = "";
            //dr[0] = "";

            //dt.Rows.Add(dr);


        }
    }
    protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    private void PopulateMenus()
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbl_SiteMaster";
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = sdr["SiteName"].ToString()+"["+sdr["Address1"].ToString()+"]";
                        item.Value = sdr["SiteId"].ToString();
                        //     item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chk_sitename.Items.Add(item);
                    }
                }
                conn.Close();
            }
        }
    }
    protected void btn_Add_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < chk_sitename.Items.Count; i++)
        {

            if (chk_sitename.Items[i].Selected)
            {

                try
                {
                    string Siteid = chk_sitename.Items[i].Value.ToString();
                    string Sitename = chk_sitename.Items[i].Text.ToString();

                    hst.Clear();


                    hst.Add("action", "insert");
                  
                    hst.Add("tariffplanname", txtbox_tariffname.Text);
                    hst.Add("tariifplandiscount",Convert.ToDecimal( txtxbox_discount.Text));
                    hst.Add("SiteName", Sitename);
                    hst.Add("siteid", Convert.ToInt32(Siteid));

                    hst.Add("modified", "modifiername");
                    hst.Add("operator", "lovey_operator");
                    hst.Add("dateofchanging", DateTime.Now);


                    int result = objData.ExecuteNonQuery("[Sp_TariffPlantToSite]", CommandType.StoredProcedure, hst);
                    if (result > 0)
                    {

                        Response.Redirect("Tariff_plan.aspx");
                        txtbox_tariffname.Text = "";
                        txtxbox_discount.Text = "";

                    }
                    else
                    {
                        // lbl_error.Visible = true;

                    }
                }
                catch
                {
                    //lbl_error.Visible = true;

                }



            }
        }


    }
}
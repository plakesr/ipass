﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_vehicleMak_Model.ascx.cs" Inherits="WUC_wuc_vehicleMak_Model" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
    .style2
    {
        width: 170px;
    }
     .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

          .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
.style2 {
    width: 140px;
}
</style>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel1" runat="server">

    <table  class="form-table" width="60%">
        <tr>
            <td class="style2" valign="top">
                Vehicle Make:</td>
            <td>
                <asp:TextBox ID="txtbox_make" runat="server" 
                    ontextchanged="TextBox1_TextChanged" ValidationGroup="b" 
                    CssClass=twitterStyleTextbox Width="156px"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="txtbox_make_TextBoxWatermarkExtender" WatermarkCssClass="watermark" WatermarkText="Enter Vehicle Make"
                    runat="server" Enabled="True" TargetControlID="txtbox_make">
                </cc1:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="rfv_make" runat="server" 
                    ControlToValidate="txtbox_make" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="b"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_add" runat="server" Text="Add" ValidationGroup="b" CssClass=button
                    onclick="btn_add_Click" />
            </td>
        </tr>
    </table>

    </asp:Panel>
<asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
                SelectCommand="SELECT * FROM [tbl_VehicleMakes]">
            </asp:SqlDataSource>
            <br />
            
            <asp:Repeater runat="server" id="rpt1" DataSourceID="SqlDataSource2" 
                onitemcommand="rpt1_ItemCommand">
            <HeaderTemplate>
            <table width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                      <th>
                        Vehicle&nbsp;&nbsp;Make
                    </th>
                    
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
            <td>
                    <%#Eval("VehiMake")%>
                    </td>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("VehiMakeID") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("VehiMakeID") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>
                    </td>

                   <%--<asp:Label ID="idLabel" runat="server" Text='<%# Eval("StateCode") %>' 
                                        Visible="False" />--%>
  <%--<asp:Label ID="idLabel1" runat="server" Text='<%# Eval("StateName") %>' 
                                        Visible="False"></asp:Label>--%>
<asp:Label ID="makename" runat="server" Text='<%# Eval("VehiMake") %>' 
                                        Visible="False"></asp:Label>  
    <asp:Label ID="id" runat="server" Text='<%# Eval("VehiMakeID") %>' 
                                        Visible="False"></asp:Label>  
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Panel>
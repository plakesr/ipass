﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_vehicleMak_Model : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel1.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        try
        {
          //  string msg = "1";

            addVehicleMake();
           // Response.Redirect("Admin_VehicleMakeModelMaster.aspx?message=" + msg);
           // ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            clear();

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    string msg;
    public void addVehicleMake()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("vehiclemake",txtbox_make.Text);
            hst.Add("operator_id", "admin");
            hst.Add("modifiedby", "admin");
            hst.Add("dateofchanging", DateTime.Now);
            int result = objData.ExecuteNonQuery("[sp_Vehicle_make]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleMakeModelMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_Vehicle Make_Model.aspx?message=" + msg);

                }
                
          
              //  Response.Redirect("Admin_VehicleMakeModelMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

                // lbl_error.Visible = true;

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            //lbl_error.Visible = true;

        }
    }
    public void clear()
    {
        txtbox_make.Text = "";
        
    }

    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delVehicleMake(ID);
                    Response.Redirect("Admin_VehicleMakeModelMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delVehicleMake(ID);
                        Response.Redirect("Operator_Vehicle Make_Model.aspx");
                    }
                }

            }
            catch
            {

                // Response.Redirect("default.aspx");
            }
           
        }

        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("makename") as Label;
            //Label l1 = rpt1.Items[i].FindControl("idLabel1") as Label;

            //Label l2 = rpt1.Items[i].FindControl("idLabel2") as Label;
            Label l3 = rpt1.Items[i].FindControl("id") as Label;

            try
            {

                Session["VehicleMake"] = l.Text;
                Session["id"] = l3.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");

            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditVehicleMakeMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditVehicle Make.aspx");
                }

            }
          

        }
    }
     public void delVehicleMake(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("id", id);


            int result = objData.ExecuteNonQuery("[sp_Vehicle_make]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              //  Response.Redirect("Admin_VehicleMakeModelMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

              //  Response.Redirect("Admin_VehicleMakeModelMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

            //Response.Redirect("Admin_VehicleMakeModelMaster.aspx");

        }


    }
}

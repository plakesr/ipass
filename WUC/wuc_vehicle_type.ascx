﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wuc_vehicle_type.ascx.cs" Inherits="WUC_wuc_vehicle_type" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style2
    {
        width: 262px;
    }
     .twitterStyleTextbox {
    border: 1px solid #c4c4c4;
    font-size: 13px;
    padding: 4px 4px 4px 4px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    box-shadow: 0px 0px 8px #d9d9d9;
    -moz-box-shadow: 0px 0px 8px #d9d9d9;
    -webkit-box-shadow: 0px 0px 8px #d9d9d9;
}

.twitterStyleTextbox:focus {
    outline: none;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
  border: none; }

.button {
  display: inline-block;
  padding: 0.5em 2em 0.55em;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  -webkit-border-radius: .5em;
  -moz-border-radius: .5em;
  border-radius: .5em;
  border: solid 1px #cccccc;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  border: solid 1px #2c67db;
  background-color: #3970dd;
  background: -webkit-gradient(linear, left top, left bottom, from(#7299e7), to(#3970dd));
  background: -moz-linear-gradient(top, #7299e7, #3970dd);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7299e7', endColorstr='#3970dd');
  color: #ebf0fb; }
  .button:hover {
    text-decoration: none; }
  .button:active {
    position: relative;
    top: 1px; }
  .button:hover {
    background-color: #2561d7;
    background: -webkit-gradient(linear, left top, left bottom, from(#658fe4), to(#2561d7));
    background: -moz-linear-gradient(top, #658fe4, #2561d7);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#658fe4', endColorstr='#2561d7'); }
  .button:active {
    background-color: #658fe4;
    background: -webkit-gradient(linear, left top, left bottom, from(#3970dd), to(#658fe4));
    background: -moz-linear-gradient(top, #3970dd, #658fe4);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3970dd', endColorstr='#658fe4');
    color: #c3d4f4; }
      .watermark
  {  
    
outline: none;
color:Gray;
    border: 1px solid #7bc1f7;
    box-shadow: 0px 0px 8px #7bc1f7;
    -moz-box-shadow: 0px 0px 8px #7bc1f7;
    -webkit-box-shadow: 0px 0px 8px #7bc1f7;
}
.style2 {
    width: 169px;
}
</style>

    
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<asp:Panel ID="Panel2" runat="server">
<table  class="form-table" width="60%">
            <tr>
                <td class="style2" valign="top">
                     Vehicle Type</td>
                <td>
                    <asp:TextBox ID="txtbox_vehicleType" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="txtbox_vehicleType_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_vehicleType" 
                        WatermarkCssClass="watermark" WatermarkText="Enter Vehicle Type">
                    </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_vehicle" runat="server" 
                        ControlToValidate="txtbox_vehicleType" ErrorMessage="*" Font-Bold="True" 
                        ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td class="style2" valign="top">
                 Description</td>
                <td>
                    
                    <asp:TextBox ID="txtbox_description" runat="server" ValidationGroup="a" CssClass=twitterStyleTextbox></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="txtbox_description_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_description" WatermarkCssClass="watermark" WatermarkText="Enter Vehicle Description">
                    </cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="rfv_description" runat="server" 
                        ControlToValidate="txtbox_description" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                    
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                <td>
                    <asp:Button ID="btn_submit" runat="server" Text="Add" ValidationGroup="a" CssClass=button
                        onclick="btn_submit_Click1" Height="40px" Width="90px" />
                </td>
            </tr>
        </table>
   </asp:Panel>

    <br />
    <br />
<asp:Panel ID="Panel1" runat="server"  Height="300px" ScrollBars="Vertical">
<fieldset>
        <asp:Repeater ID="rpt1" runat="server" DataSourceID="SqlDataSource1" 
            onitemcommand="rpt1_ItemCommand">
        <HeaderTemplate>
            <table  width="100%" class="timezone-table" cellpadding="5" cellspacing="0">
                <tr>
                     
                     <th>
                        Vehicle&nbsp;&nbsp;Name
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                      Delete
                    </th>
                     
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
           
                   <td>
                    <%#Eval("vehicle_type")%>
                </td>
                
                <td>
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("v_id") %>' runat="server"><img src="images/edit.png" />
                    </asp:LinkButton>
                    
                </td>
                <td>
                <asp:LinkButton ID="lnkDelete" CommandName="cmdDelete" CommandArgument='<%#Eval("v_id") %>' runat="server" OnClientClick="return confirmation();"><img src="images/trash.png" />
                    </asp:LinkButton>

                     <asp:Label ID="idLabel" runat="server" Text='<%# Eval("v_id") %>' 
                                        Visible="False" />
                    </td>
            </tr>
            
                        <asp:Label ID="lbl1" runat="server" Text='<%# Eval("v_id") %>' 
                                        Visible="False" />
            <asp:Label ID="code" runat="server" Text='<%# Eval("vehicle_type") %>' 
                                        Visible="False" />
 
        </ItemTemplate>
       
        <FooterTemplate>
            </table>
        </FooterTemplate>
</asp:Repeater>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
            SelectCommand="SELECT * FROM [tbl_Vehicle_Type]">
        </asp:SqlDataSource>
        </fieldset>
</asp:Panel>





﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class WUC_wuc_vehicle_type : System.Web.UI.UserControl
{
    clsData objData = new clsData();
    DataSet ds = new DataSet();
    Hashtable hst = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["message"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Inserted succesfully.')", true);
            }
            if (Request.QueryString["msg"] == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('update succesfully.')", true);
            }
            if (!IsPostBack == true)
            {
                try
                {

                    if (Session["lbladd"].ToString() == "True")
                    {
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = false;
                    }



                }
                catch
                {
                    try
                    {
                        if (Session["Type"].ToString() == "admin")
                        {
                            // Response.Redirect("Deshboard.aspx");
                        }
                        if (Session["Type"].ToString() == "operator")
                        {
                            Response.Redirect("Operator_deshboard.aspx");

                        }
                    }
                    catch
                    {
                        //  Response.Redirect("default.aspx");
                    }
                }
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
       
    }
    string msg;
    public void addvehicle()
    {
        try
        {
            hst.Clear();

            hst.Add("action", "insert");
            hst.Add("vehicle_type", txtbox_vehicleType.Text);

            int result = objData.ExecuteNonQuery("[sp_vehicle]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
                msg = "1";
                if (Session["Type"].ToString() == "admin")
                {
                    Response.Redirect("Admin_VehicleTypeMaster.aspx?message=" + msg);
                }
                if (Session["Type"].ToString() == "operator")
                {
                    Response.Redirect("Operator_VehicleTypeMaster.aspx?message=" + msg);

                }
                
                

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);

            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Inserted Data Please Try Again.')", true);


        }

    }
    public void clear()
    {
        txtbox_vehicleType.Text = "";
       
    }

    
    public void delvehicle(int id)
    {
        try
        {
            hst.Clear();
            hst.Add("action", "delete");
            hst.Add("v_id", id);
            hst.Add("operator_id", "lovey_operator");
            hst.Add("modifiedby", "modifier_id");
            hst.Add("dateofchanging", DateTime.Now);


            int result = objData.ExecuteNonQuery("[sp_vehicle]", CommandType.StoredProcedure, hst);
            if (result > 0)
            {
              //  Response.Redirect("Admin_VehicleTypeMaster.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

               // Response.Redirect("Admin_VehicleTypeMaster.aspx");
            }
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('Not Delate Data Please Try Again.')", true);

          //  Response.Redirect("Admin_VehicleTypeMaster.aspx");

        }


    }
    protected void btn_submit_Click1(object sender, EventArgs e)
    {
        try
        {
          //  string msg = "1";
            addvehicle();
           // Response.Redirect("Admin_VehicleTypeMaster.aspx?message=" + msg);

            clear();
           
        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('please try again.')", true);
        }
    }
    protected void rpt1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i = e.Item.ItemIndex;
        if (e.CommandName == "cmdDelete")
        {
            int ID = Convert.ToInt32(e.CommandArgument.ToString());
            try
            {
                if (Session["Type"].ToString() == "admin")
                {
                    delvehicle(ID);
                    Response.Redirect("Admin_VehicleTypeMaster.aspx");
                }
                if (Session["Type"].ToString() == "operator")
                {
                    if (Session["lbldelete"].ToString() == "False")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Deleting.')", true);

                    }

                    else
                    {
                        delvehicle(ID);
                        Response.Redirect("Operator_VehicleTypeMaster.aspx");
                    }
                }

            }
            catch
            {
                // Response.Redirect("default.aspx");
            }
       
        }
        if (e.CommandName == "cmdEdit")
        {
            Label l = rpt1.Items[i].FindControl("code") as Label;
            Label l1 = rpt1.Items[i].FindControl("lbl1") as Label;
            try
            {
                Session["vehicle_type"] = l.Text;
                Session["vehicleid"] = l1.Text;
            }
            catch
            {
                Response.Redirect("Default.aspx");
            }
            if (Session["Type"].ToString() == "admin")
            {
                Response.Redirect("Admin_EditVehicleTypeMaster.aspx");
            }
            if (Session["Type"].ToString() == "operator")
            {
                if (Session["lbledit"].ToString() == "False")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "1", "alert('You are not Authorised For Editing.')", true);

                }
                else
                {
                    Response.Redirect("Operator_EditVehicleTypeMaster.aspx");
                }

            }
            

        }
    }
}
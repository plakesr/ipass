﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="authnticate.aspx.cs" Inherits="authnticate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
 
        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1" 
            onitemcommand="Repeater1_ItemCommand">
        <ItemTemplate>
            <ul>
           
                    <asp:LinkButton ID="lnkEdit" CommandName="cmdEdit" CommandArgument='<%#Eval("id") %>' runat="server" > <%#Eval("menu") %> <asp:Label ID="idLabel" runat="server" Text='<%# Eval("navigationurl") %>' 
                                        Visible="False" />
                    </asp:LinkButton>
                    
                </ul>
        </ItemTemplate>

        </asp:Repeater>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" SelectCommand="select tblUserMenu.*,tbl_menus.id,tbl_menus.menuname as menu,tbl_menus.parentid,tbl_menus.navigationurl from tblUserMenu
inner join tbl_menus on tblUserMenu.Menuname=tbl_menus.id
where Username=@username and tbl_menus.parentid=1
">
            <SelectParameters>
                <asp:SessionParameter Name="username" SessionField="Login" />
            </SelectParameters>
        </asp:SqlDataSource>
 
    </div>
    </form>
</body>
</html>

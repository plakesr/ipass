﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="autocomplte.aspx.cs" Inherits="autocomplte" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <cc1:AutoCompleteExtender ID="TextBox1_AutoCompleteExtender" runat="server" TargetControlID="TextBox1"
       MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries">
    </cc1:AutoCompleteExtender>
    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    <cc1:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" 
        Enabled="True" TargetControlID="TextBox2">
    </cc1:CalendarExtender>
    <div>
    <%--<ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtCountry" 
MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries" >
</ajax:AutoCompleteExtender>--%>
    </div>
    </form>
</body>
</html>

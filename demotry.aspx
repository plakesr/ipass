﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="demotry.aspx.cs" Inherits="demotry" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    
    </div>
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" 
        Width="54px" />
    <asp:DataList ID="DataList1" runat="server">
        <ItemTemplate>
            <table class="style1">
                <tr>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%#Eval("a")%>'></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server" 
                            DataSourceID="SqlDataSource1" DataTextField="Country_name" 
                            DataValueField="Country_id">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server" ></asp:TextBox></td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
    <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="Button" />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        onselecting="SqlDataSource1_Selecting" 
        SelectCommand="SELECT [Country_id], [Country_name] FROM [tbl_country]">
    </asp:SqlDataSource>
    </form>
</body>
</html>

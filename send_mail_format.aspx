﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="send_mail_format.aspx.cs" Inherits="send_mail_format" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style2
        {
            text-decoration: underline;
            font-size: x-large;
        }
        .style6
        {
            width: 359px;
        }
        .style8
        {
            width: 471px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="clearfix"></div>
        <table style="  border:solid 1px #c1d5e5; padding:3px; border-collapse:collapse; width:1060px; height:300px; margin-top:15px; font-family:Arial; font-size:13px; margin:auto; "width="960" border="0" cellpadding="10" height="200" >
         <tr> <td colspan="4" style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;"><img border="0" alt="Salesforce" src="images/ipasslogo.png" /></td></tr>
            <tr>
                <td 
                    style="font-weight: 700; color: #3333FF; background-color: #FFFFFF;" 
                    class="style6">
                    
                    </td>
                <td colspan="2" 
                    style="font-weight: 700; color: #3333FF; background-color: #FFFFFF;">
                    
                    <span class="style2">&nbsp;BILLING INFORMATION&nbsp;</span></td>
                <td 
                    style="font-weight: 700; color: #3333FF; background-color: #FFFFFF;">
                    
                    &nbsp;</td>
            </tr>
            <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                    
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                    
                    Name</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_name" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                   
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                   
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   
                    User Name</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_username" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                    
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                    
                    &nbsp;&nbsp;&nbsp;
                    
                    Address</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_name1" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                  
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                  
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  
                    Lot Name</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_name2" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
             <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                   
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                   
                    Plan</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_name3" runat="server" Text="Label"></asp:Label>
                 </td>
            </tr>
             <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                    
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    Parking Type</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_name4" runat="server" Text="Label"></asp:Label>
                 </td>
            </tr>
             <tr>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;">
                 
                    &nbsp;</td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" class="style8">
                 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                    Total Due Amount </td>
                <td style=" background:#eaf3fa; border-right:solid 1px #c1d5e5;" colspan="2">
                    <asp:Label ID="lbl_name5" runat="server" Text="Label" ForeColor="#00CC66"></asp:Label>
                 </td>
            </tr>
        </table>
    
    </div><br />
    <div></div>
    </form>
</body>
</html>

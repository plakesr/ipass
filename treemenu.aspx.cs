﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class treemenu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindTreeViewControl();
        }
    }
    private void BindTreeViewControl()
    {
        try
        {
            DataSet ds = GetDataSet("select * from tbl_Menu inner join tbl_SubMenu on tbl_Menu.MenuID=tbl_SubMenu.MenuID");
            DataRow[] Rows = ds.Tables[0].Select("MenuID"); // Get all parents nodes
            for (int i = 0; i < Rows.Length; i++)
            {
                TreeNode root = new TreeNode(Rows[i]["MenuName"].ToString(), Rows[i]["MenuID"].ToString());
                root.SelectAction = TreeNodeSelectAction.Expand;
                CreateNode(root, ds.Tables[0]);
                treeviwExample.Nodes.Add(root);
            }
        }
        catch (Exception Ex) { throw Ex; }
    }

    public void CreateNode(TreeNode node, DataTable Dt)
    {
        DataRow[] Rows = Dt.Select("MenuID =" + node.Value);
        if (Rows.Length == 0) { return; }
        for (int i = 0; i < Rows.Length; i++)
        {
            TreeNode Childnode = new TreeNode(Rows[i]["Submenu"].ToString(), Rows[i]["SubMenuID"].ToString());
            Childnode.SelectAction = TreeNodeSelectAction.Expand;
            node.ChildNodes.Add(Childnode);
            CreateNode(Childnode, Dt);
        }
    }
    private DataSet GetDataSet(string Query)
    {
        DataSet Ds = new DataSet();
        try
        {
            string strCon = @"Data Source=.;Initial Catalog=db_IPASS;Integrated Security=True";
            SqlConnection Con = new SqlConnection(strCon);
            SqlDataAdapter Da = new SqlDataAdapter(Query, Con);
            Da.Fill(Ds);
        }
        catch (Exception) { }
        return Ds;
    }
   
}
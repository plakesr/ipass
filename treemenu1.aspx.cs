﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class treemenu1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fill_Tree2();
    }

    

    void fill_Tree2()
    {

        string name = Session["login"].ToString().Trim();

        DataSet PrSet = PDataset("Select * from tbl_Menu where username='"+name+"'");

        TreeView1.Nodes.Clear();

        foreach (DataRow dr in PrSet.Tables[0].Rows)
        {

            TreeNode tnParent = new TreeNode();

            tnParent.Text = dr["MenuName"].ToString();

            tnParent.Value = dr["MenuID"].ToString();

            tnParent.PopulateOnDemand = true;

            tnParent.ToolTip = "Click to get Child";

            tnParent.SelectAction = TreeNodeSelectAction.SelectExpand;

            tnParent.Expand();

            tnParent.Selected = true;

            TreeView1.Nodes.Add(tnParent);

            FillChild(tnParent, tnParent.Value);

        }



    }

    public void FillChild(TreeNode parent, string ParentId)
    {

        DataSet ds = PDataset("Select * from tbl_SubMenu where MenuID =" + ParentId);

        parent.ChildNodes.Clear();

        foreach (DataRow dr in ds.Tables[0].Rows)
        {

            TreeNode child = new TreeNode();

            child.Text = dr["Submenu"].ToString().Trim();

            child.Value = dr["SubMenuID"].ToString().Trim();

            child.NavigateUrl = dr["Navigation_URl"].ToString().Trim();

            if (child.ChildNodes.Count == 0)
            {

                child.PopulateOnDemand = true;

            }

            child.ToolTip = "Click to get Child";

            child.SelectAction = TreeNodeSelectAction.SelectExpand;

            child.CollapseAll();

            parent.ChildNodes.Add(child);

        }

    }



    protected DataSet PDataset(string Select_Statement)
    {

        SqlConnection SqlCon = new SqlConnection("Data Source=.;Initial Catalog=db_IPASS;Integrated Security=True");

        SqlDataAdapter ad = new SqlDataAdapter(Select_Statement, SqlCon);

        DataSet ds = new DataSet();

        ad.Fill(ds);

        return ds;



    }
}
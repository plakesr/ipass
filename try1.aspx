﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="try1.aspx.cs" Inherits="try1" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
        DeleteCommand="DELETE FROM [tbl_parkerVehicleDetails] WHERE [ParkerVehicleId] = @ParkerVehicleId" 
        InsertCommand="INSERT INTO [tbl_parkerVehicleDetails] ([Parker_Id], [Make], [Model], [Year], [Color], [LicenseNo], [UserName]) VALUES (@Parker_Id, @Make, @Model, @Year, @Color, @LicenseNo, @UserName)" 
        SelectCommand="SELECT * FROM [tbl_parkerVehicleDetails] ORDER BY [Parker_Id] DESC" 
        UpdateCommand="UPDATE [tbl_parkerVehicleDetails] SET [Parker_Id] = @Parker_Id, [Make] = @Make, [Model] = @Model, [Year] = @Year, [Color] = @Color, [LicenseNo] = @LicenseNo, [UserName] = @UserName WHERE [ParkerVehicleId] = @ParkerVehicleId">
        <DeleteParameters>
            <asp:Parameter Name="ParkerVehicleId" Type="Int64" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Parker_Id" Type="Int64" />
            <asp:Parameter Name="Make" Type="String" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="String" />
            <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="UserName" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Parker_Id" Type="Int64" />
            <asp:Parameter Name="Make" Type="String" />
            <asp:Parameter Name="Model" Type="String" />
            <asp:Parameter Name="Year" Type="String" />
            <asp:Parameter Name="Color" Type="String" />
            <asp:Parameter Name="LicenseNo" Type="String" />
            <asp:Parameter Name="UserName" Type="String" />
            <asp:Parameter Name="ParkerVehicleId" Type="Int64" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
                
                
        SelectCommand="SELECT [VehiMake] FROM [tbl_VehicleMakes] ORDER BY [VehiMake]">
    </asp:SqlDataSource>
    <asp:ListView ID="ListView1" runat="server" DataKeyNames="ParkerVehicleId" 
        DataSourceID="SqlDataSource1" InsertItemPosition="LastItem">
        <AlternatingItemTemplate>
            <tr style="background-color: #FAFAD2;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                </td>
                <td>
                    <asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />
                </td>
                <td>
                    <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                    <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />
                </td>
            </tr>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <tr style="background-color: #FFCC66;color: #000080;">
                <td>
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                        Text="Cancel" />
                </td>
                <td>
                    <asp:Label ID="ParkerVehicleIdLabel1" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />
                </td>
                <td>
                    <asp:TextBox ID="Parker_IdTextBox" runat="server" 
                        Text='<%# Bind("Parker_Id") %>' />
                </td>
                <td>
                 <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource2" DataTextField="VehiMake" 
                DataValueField="VehiMake" SelectedValue='<%# Bind("Make") %>'> </asp:DropDownList>

                    
                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                    <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                <td>
                    <asp:TextBox ID="UserNameTextBox" runat="server" 
                        Text='<%# Bind("UserName") %>' />
                </td>
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" 
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                        Text="Clear" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:TextBox ID="Parker_IdTextBox" runat="server" 
                        Text='<%# Bind("Parker_Id") %>' />
                </td>
                <td>
                    
                    <asp:DropDownList ID="MakeTextBox" runat="server" 
                DataSourceID="SqlDataSource2" DataTextField="VehiMake" 
                DataValueField="VehiMake" SelectedValue='<%# Bind("Make", "{0}") %>'> </asp:DropDownList>
                    

                </td>
                <td>
                    <asp:TextBox ID="ModelTextBox" runat="server" Text='<%# Bind("Model") %>' />
                </td>
                <td>
                    <asp:TextBox ID="YearTextBox" runat="server" Text='<%# Bind("Year") %>' />
                </td>
                <td>
                    <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />
                </td>
                <td>
                    <asp:TextBox ID="LicenseNoTextBox" runat="server" 
                        Text='<%# Bind("LicenseNo") %>' />
                </td>
                <td>
                    <asp:TextBox ID="UserNameTextBox" runat="server" 
                        Text='<%# Bind("UserName") %>' />
                </td>
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr style="background-color: #FFFBD6;color: #333333;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                </td>
                <td>
                    <asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />
                </td>
                <td>
                    <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                    <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="1" 
                            style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                            <tr runat="server" style="background-color: #FFFBD6;color: #333333;">
                                <th runat="server">
                                </th>
                                <th runat="server">
                                    ParkerVehicleId</th>
                                <th runat="server">
                                    Parker_Id</th>
                                <th runat="server">
                                    Make</th>
                                <th runat="server">
                                    Model</th>
                                <th runat="server">
                                    Year</th>
                                <th runat="server">
                                    Color</th>
                                <th runat="server">
                                    LicenseNo</th>
                                <th runat="server">
                                    UserName</th>
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" 
                        style="text-align: center;background-color: #FFCC66;font-family: Verdana, Arial, Helvetica, sans-serif;color: #333333;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                                    ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #FFCC66;font-weight: bold;color: #000080;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" 
                        Text="Delete" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                </td>
                <td>
                    <asp:Label ID="ParkerVehicleIdLabel" runat="server" 
                        Text='<%# Eval("ParkerVehicleId") %>' />
                </td>
                <td>
                    <asp:Label ID="Parker_IdLabel" runat="server" Text='<%# Eval("Parker_Id") %>' />
                </td>
                <td>
                    <asp:Label ID="MakeLabel" runat="server" Text='<%# Eval("Make") %>' />
                </td>
                <td>
                    <asp:Label ID="ModelLabel" runat="server" Text='<%# Eval("Model") %>' />
                </td>
                <td>
                    <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' />
                </td>
                <td>
                    <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                </td>
                <td>
                    <asp:Label ID="LicenseNoLabel" runat="server" Text='<%# Eval("LicenseNo") %>' />
                </td>
                <td>
                    <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>

    <%--<div>
   <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:db_IPASSConnectionString %>" 
            SelectCommand="SELECT * FROM [tbl_uploadlotTerms] WHERE ([LotId] = @LotId)">
                <SelectParameters>
                    <asp:Parameter DefaultValue="39" Name="LotId" Type="Int64" />
                </SelectParameters>
        </asp:SqlDataSource>
    <asp:repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
        
      
            
        

            <ItemTemplate>
              
            
                
<embed src='<%#"TermsForLots/"+Eval("LotTermPdfName") %>' width="100%" height="1024px"> </embed>
    <%--<asp:Image ID="Image1" runat="server" ImageUrl='<%#"~/FlagImage/Resized/"+Eval("Country_flag") %>' />--%>
              
       <%-- </ItemTemplate>
           <%-- <SelectParameters>
                <asp:SessionParameter Name="LotId" SessionField="Lotid" Type="Int64" />
            </SelectParameters>--%>
        <%--</asp:repeater>
    </div>--%>
    </form>
</body>
</html>


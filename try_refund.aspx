﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="try_refund.aspx.cs" Inherits="try_refund" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Refund Request</title>
    
    <%--<script src="Extension.min.js" type="text/javascript"></script>
    <link href="CSS.css" rel="stylesheet" type="text/css" />--%>
     <link href="stylesheet/main.css" rel="stylesheet" type="text/css" />
<link href="stylesheet/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="stylesheet/CSS.css" rel="stylesheet" type="text/css" />
 

    <link href="style/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.8.2.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        var date = new Date();
        var currentMonth = date.getMonth();
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
        $('#txtbox_transcationdate').datepicker({
            maxDate: new Date(currentYear, currentMonth, currentDate)
        });
    });
</script>







    
    <style type="text/css">
        .style2
        {
            width: 50px;
        }
        .style3
        {
            width: 295px;
        }
        .style4
        {
            width: 132px;
        }
        .style5
        {
            width: 133px;
        }
        .style6
        {
            width: 381px;
        }
        .style7
        {
            width: 189px;
        }
        .style8
        {
            width: 188px;
        }
        .style9
        {
            width: 176px;
        }
        .style10
        {
            width: 131px;
        }
        .style11
        {
            width: 129px;
        }
    </style>







    
</head>

<body>
<div class="wrapper">
<div class="container">
<div class="logo-area"><a href="#"><img border="0" alt="Salesforce" src="images/ipass-logo.png" /></a></div>
 <div class="body-container">
    <form id="form1" runat="server" >
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
        <asp:Panel ID="Panel1" runat="server">
        <fieldset><legend>Personal Detail</legend>
        
        <table class="form-table" cellpadding="0" cellspacing="0" >
            <tr>
                <td class="style5" width="150">
                    Enter Facility:
                </td>
                <td class="style14">
                    <asp:TextBox ID="search_lot" runat="server" 
                        ontextchanged="search_lot_TextChanged" CssClass="twitterStyleTextbox"  ></asp:TextBox>
                   <cc1:AutoCompleteExtender ID="TextBox1_AutoCompleteExtender" runat="server" TargetControlID="search_lot"
       MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCountries">
    </cc1:AutoCompleteExtender>
                </td>
                <td width="50"></td>
                <td class="style10">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style5">
                    Paystation #:</td>
                <td class="style14">
                     <asp:TextBox ID="txtbox_machine" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_machine" runat="server" 
                    ControlToValidate="txtbox_machine" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td>
                    <td width="50"></td>
                <td class="style10">
                    Receipt #:
                </td>
                <td>
                    <asp:TextBox ID="txtbox_ticketno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ticketno" runat="server" 
                        ControlToValidate="txtbox_ticketno" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>

          
            <tr>
                <td class="style5">
                    Amount Paid:</td>
                <td class="style14">
                      
                    <asp:TextBox ID="txtbox_givenamopunt" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_totalamountgiven" runat="server" 
                        ControlToValidate="txtbox_ticketno" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                   </td>
                   <td width="50"></td>
                <td class="style10">
                    Amount Requested to be refunded #:</td>
                <td>
                     <asp:TextBox ID="txtbox_refundamount" runat="server"  CssClass="twitterStyleTextbox"
                        ontextchanged="txtbox_refundamount_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_refundamount" runat="server" 
                        ControlToValidate="txtbox_refundamount" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblerr" runat="server" Text="" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td class="style5">
                    Date of Transaction #:</td>
                <td class="style14">
                    <asp:TextBox ID="txtbox_transcationdate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <%--<cc1:CalendarExtender ID="txtbox_transcationdate_CalendarExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_transcationdate">
                    </cc1:CalendarExtender>--%>
                <asp:RequiredFieldValidator ID="rfv_date" runat="server" 
                    ControlToValidate="txtbox_transcationdate" ErrorMessage="*" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td>
                    <td width="50"></td>
                <td class="style10">
                    Attach Original Receipt</td>
                <td>
                     <asp:FileUpload ID="originalrecepit" runat="server" />
                <asp:RequiredFieldValidator ID="rfv_attachoriginal" runat="server" 
                    ControlToValidate="originalrecepit" ErrorMessage="*Please Upload Original Receipt" ForeColor="#CC0000" 
                    ValidationGroup="a"></asp:RequiredFieldValidator></td>
            </tr>
            </table>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="form-table" cellpadding="0" cellspacing="0">
          
              <tr>

                <td class="style11">
                    Method of payment</td>
  
                <td class="style3">
              
                    <asp:RadioButton ID="rbt_cash" runat="server" AutoPostBack="true" GroupName="r" 
                    oncheckedchanged="rbt_cash_CheckedChanged" Text="CASH" />
                &nbsp;
                <asp:RadioButton ID="rbt_debit" runat="server" AutoPostBack="true" 
                    GroupName="r" oncheckedchanged="rbt_debit_CheckedChanged" Text="DEBIT" />
                &nbsp;
                <asp:RadioButton ID="rbt_credit" runat="server" AutoPostBack="true" 
                    GroupName="r" oncheckedchanged="rbt_credit_CheckedChanged" Text="CREDIT" />
       
                    
                    
                    </td>

        

                    <td class="style2"></td>
                <td class="style9">
                    <asp:Label ID="lbl_credit_authorization" runat="server" 
                    Text="Credit Card Authorization Code" Visible="False"></asp:Label></td>
                <td class="style6">
                   <asp:TextBox ID="txtbox_authorisation" runat="server" 
                    CssClass="twitterStyleTextbox" Visible="False"></asp:TextBox>
                    
         
                    
                    </td>
            </tr>
          
            </table>
            
        </ContentTemplate>
                     <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rbt_credit" EventName="CheckedChanged" />
                </Triggers>
   </asp:UpdatePanel>
            
            
             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table class="form-table" cellpadding="0" cellspacing="0">
            <tr>
       <td class="style13">
           Reason for refund request:</td>
       <td class="style4">
       
             <asp:DropDownList ID="ddl_reason" runat="server" AutoPostBack="True"  CssClass="twitterStyleTextbox"
                    onselectedindexchanged="ddl_reason_SelectedIndexChanged">
                    <asp:ListItem> Patient Passed away / Discharged / Staff no longer work</asp:ListItem>
                    <asp:ListItem> Bought Mistakenly wrong pass</asp:ListItem>
                    <asp:ListItem> Buying multiple tickets at pay and display</asp:ListItem>
                    <asp:ListItem>Using Credit Cards on way in but not on way out</asp:ListItem>
                    <asp:ListItem> Paid an old Ticket</asp:ListItem>
                    <asp:ListItem>Paystation Short changes with or with out credit note, hopper/dispenser error</asp:ListItem>
                    <asp:ListItem> P&amp;D Machines -Parker wrongly usese Max Button</asp:ListItem>
                    <asp:ListItem>Visitor wrongly purchased multi-use pass, but wanted to buy short term ticket</asp:ListItem>
                    <asp:ListItem> Appointment Cancelled</asp:ListItem>
                    <asp:ListItem>Waited the pay station to be serviced then went over time limit</asp:ListItem>
                    <asp:ListItem> Holiday rate not in effect / Programming error i.e.Holiday rate not programmed</asp:ListItem>
                    <asp:ListItem>Other</asp:ListItem>
                </asp:DropDownList>
       
           </td>
           <td width="50"></td>
           <td class="style4">
       
              <asp:TextBox ID="txtbox_reason" runat="server"   CssClass="twitterStyleTextbox" 
                    style=" resize:none;" TextMode="MultiLine" Visible="False" Height="28px"></asp:TextBox>
                     <cc1:TextBoxWatermarkExtender ID="txtbox_vehicleType_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txtbox_reason" 
                        WatermarkCssClass="watermark" WatermarkText="Enter Reason">
                    </cc1:TextBoxWatermarkExtender>
       
           </td>
           <td></td>
       </tr>
        
        </table>
                                                        </ContentTemplate>
                     <Triggers>
      
        <asp:AsyncPostBackTrigger ControlID="ddl_reason" EventName="SelectedIndexChanged" />
       

    </Triggers>
                </asp:UpdatePanel>
        </fieldset>
        </asp:Panel>
        <br />
        <asp:Panel ID="Panel2" runat="server">
        <fieldset><legend>Refund Detail</legend>

        <table class="form-table">
            <tr>
                <td class="style5" width="150">
                    First Name:</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_firstname" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_f_name" runat="server" 
                        ControlToValidate="txtbox_firstname" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td width="50"></td>
                <td class="style8" width="150">
                    Last Name:</td>
                <td>
                    <asp:TextBox ID="txtbox_lastname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv__l_name" runat="server" 
                        ControlToValidate="txtbox_lastname" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Apt/Suite#:</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_apt" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="50"></td>
                <td class="style8">
                    Street Address:</td>
                <td>
                    <asp:TextBox ID="txtbox_address" runat="server" CssClass="twitterStyleTextbox" 
                        style=" resize:none;" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    City</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_city" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="50"></td>
                <td class="style8">
                    Prov:</td>
                <td>
                    <asp:TextBox ID="txtbox_province" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Postal Code</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_postalcode" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td width="50"></td>
                <td class="style8">
                    Email:</td>
                <td>
                    <asp:TextBox ID="txtbox_email" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_email" runat="server" 
                        ControlToValidate="txtbox_email" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    Cell#</td>
                <td class="style7">
                    <asp:TextBox ID="txtbox_cellnumber" runat="server" 
                        CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_cellno" runat="server" 
                        ControlToValidate="txtbox_cellnumber" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td width="50"></td>
                <td class="style8">
                    Licence plate#:</td>
                <td>
                    <asp:TextBox ID="txtbox_licence" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_licence" runat="server" 
                        ControlToValidate="txtbox_licence" ErrorMessage="*" ForeColor="#CC0000" 
                        ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        </fieldset>        </asp:Panel>
           <table >
       <tr>
       <td class="style5">
           &nbsp;</td>
       <td class="style4">
       <asp:Button ID="btn_send" runat="server" Text="Send Request" CssClass="button" 
               ValidationGroup="a" onclick="btn_send_Click" />
           </td>
       </tr>
       </table>
    </form>
    </div>

    </div>

    </div>

        <div class="footer-container"></div>
</body>
</html>
